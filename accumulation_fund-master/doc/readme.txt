注意事项：
	1.先使用sql目录下的脚本建表并导入数据。
	2.在“application-config-datasource-project.properties”修改数据库的链接/ 用户名/ 密码。
	3.由于本项目用1.8jdk编译，所以只能在tomcat8.0（基于1.8的容器皆可）的容器中运行，如环境所限，需要在1.6/ 1.7的环境中运行，只需要用1.6/ 1.7的jdk重新编译即可。
	4.启动后的默认首页是“/project/manage/index.shtml”。