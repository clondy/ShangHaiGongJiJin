
/**
 *  字典分类表结构及初始化数据
 */ 
CREATE TABLE DICTIONARY_SORT
(
  ID           NUMBER(12)                       NOT NULL,
  TITLE        VARCHAR2(60 BYTE),
  DESCRIPTION  VARCHAR2(1000 BYTE),
  CREATE_TIME  DATE,
  ENABLED      NUMBER(1)                        DEFAULT 1
);
   
CREATE SEQUENCE SEQ_DICTIONARY_SORT
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;

  
/**
 *  字典项表结构及初始化数据
 */
CREATE TABLE DICTIONARIES
(
  ID            NUMBER(12)                       NOT NULL,
  SORT_ID       NUMBER(12),
  TITLE         VARCHAR2(60 BYTE),
  STANDARD_CODE VARCHAR2(20 BYTE),
  POSITION      NUMBER(12),
  DESCRIPTION   VARCHAR2(1000 BYTE),
  CREATE_TIME   DATE,
  ENABLED       NUMBER(1)                        DEFAULT 1
);

CREATE SEQUENCE SEQ_DICTIONARIES
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


create table REGIONS
(
  id            NUMBER(22,12) not null,
  parent_id     NUMBER(22,12),
  name_         VARCHAR2(60),
  rank          NUMBER(22,12),
  standard_code VARCHAR2(100),
  description   VARCHAR2(2000),
  enabled       CHAR(1) default 1,
  createtime    DATE,
  modifytime    DATE
);
   
create sequence SEQ_REGIONS
minvalue 1
maxvalue 999999999999999999999999999
start with 1001
increment by 1
cache 20;


create table SYSTEM_CONFIGS
(
  id    NUMBER,
  key   VARCHAR2(100),
  value CLOB
);
 
create sequence SEQ_SYSTEM_CONFIGS
minvalue 1
maxvalue 999999999999999999999999999
start with 2000
increment by 1
cache 20;


create table SYSTEM_RESOURCES
(
  id                  NUMBER(12) not null,
  parent_id           NUMBER(12),
  name                VARCHAR2(120),
  type                VARCHAR2(20),
  value               VARCHAR2(254),
  position            NUMBER(5),
  display             NUMBER(1),
  view_in_right_panel NUMBER(1) default 1,
  view_stamp          VARCHAR2(500),
  is_need_authorized  NUMBER(1) default 1 not null,
  view_stamp_id       NUMBER(12),
  is_category_menu    NUMBER(1) default 0
);

alter table SYSTEM_RESOURCES
  add primary key (ID)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
 
create sequence SEQ_RESOURCES
minvalue 1
maxvalue 999999999999999999999999999
start with 1000
increment by 1
cache 20;


create table SYSTEM_RESOURCES_ROLES
(
  resource_id NUMBER(12),
  role_id     NUMBER(12)
);


create table SYSTEM_ROLES
(
  id          NUMBER(12) not null,
  role_name   VARCHAR2(60),
  description VARCHAR2(254),
  enabled     NUMBER(1)
);

alter table SYSTEM_ROLES
  add primary key (ID)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
 
create sequence SEQ_ROLES
minvalue 1
maxvalue 999999999999999999999999999
start with 1000
increment by 1
cache 20;


create table SYSTEM_ROLES_VIEWSTAMPS
(
  role_id       NUMBER(12),
  view_stamp_id NUMBER(12)
);
 
create sequence SEQ_SYSTEM_VIEW_STAMPS
minvalue 1
maxvalue 9999999999999999999999999999
start with 1000
increment by 1
cache 20;


create table SYSTEM_USERS
(
  id            NUMBER(12) not null,
  user_name     VARCHAR2(32),
  password      VARCHAR2(32),
  nick_name     VARCHAR2(100),
  region_id     NUMBER(12),
  enabled       NUMBER(1) default 1
);

alter table SYSTEM_USERS
  add primary key (ID)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
 
create sequence SEQ_USERS
minvalue 1
maxvalue 999999999999999999999999999
start with 1000
increment by 1
cache 20;


create table SYSTEM_USERS_ROLES
(
  user_id NUMBER(12),
  role_id NUMBER(12)
);


create table SYSTEM_VIEW_STAMPS
(
  id          NUMBER(12) not null,
  name        VARCHAR2(90),
  description VARCHAR2(900),
  enabled     NUMBER(1)
);

comment on table SYSTEM_VIEW_STAMPS
  is '显示戳：用于控制在操作界面中，系统的相应权限资源（按钮/ 连接……）是否可被显示。';

comment on column SYSTEM_VIEW_STAMPS.id
  is 'ID';
comment on column SYSTEM_VIEW_STAMPS.name
  is '显示戳的名称（key）。作用于spring-security中的投票key。';
comment on column SYSTEM_VIEW_STAMPS.description
  is '显示戳的文字描述。';
comment on column SYSTEM_VIEW_STAMPS.enabled
  is '逻辑删除状态。';

alter table SYSTEM_VIEW_STAMPS
  add constraint SYSTEM_VIEW_STAMPS_PK primary key (ID)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
