create sequence SEQ_NOTICE
	minvalue 0
/

create table NOTICE
(
	ID NUMBER(19) not null
		primary key,
	TITLE NVARCHAR2(200) default ''  not null,
	CONTENT NVARCHAR2(4000) default ''  not null,
	RECEIVER_ID NUMBER(19)
		constraint NOTICE_SYSTEMUSER__FK
			references SYSTEM_USERS,
	RECEIVER_ROLE_ID NUMBER(19)
		constraint NOTICE_SYSTEMROLE__FK
			references SYSTEM_ROLES,
	PUBLISH_TIME DATE not null
)
/

comment on table NOTICE is '通知表'
/

comment on column NOTICE.ID is '主键'
/

comment on column NOTICE.TITLE is '标题'
/

comment on column NOTICE.CONTENT is '正文'
/

comment on column NOTICE.RECEIVER_ID is '接收者'
/

comment on column NOTICE.RECEIVER_ROLE_ID is '接收角色'
/

comment on column NOTICE.PUBLISH_TIME is '发布时间'
/



create sequence SEQ_NOTICE_READRECORD
	minvalue 0
/

create table NOTICE_READ_RECORD
(
	ID NUMBER(19) not null
		primary key,
	NOTICE_ID NUMBER(19) not null
		constraint NOTICE_READ_RECORD_NOTICE__FK
			references NOTICE,
	READER_ID NUMBER(19) not null
		constraint NOTICE_READ_RECORD_READER__FK
			references SYSTEM_USERS,
	READ_TIME DATE not null
)
/

comment on table NOTICE_READ_RECORD is '通知读取记录'
/

comment on column NOTICE_READ_RECORD.NOTICE_ID is '通知ID'
/

comment on column NOTICE_READ_RECORD.READER_ID is '读者'
/

comment on column NOTICE_READ_RECORD.READ_TIME is '读取时间'
/

