/*
 *    	@(#)@StringUtilsTest.java  2012-11-13
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.framework.util;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.capinfo.framework.util.StringUtils;

import static org.junit.Assert.*;

/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
@RunWith(JUnit4.class)
public class StringUtilsTest {

	@Test
	public void testReceiveMatchMap() {
		
		Map<String, String> matchMap = StringUtils.receiveMatchMap("(idCard eq 123)(sex ne 1)", "\\(([^\\)]*)\\)", 1);
		
		Assert.assertEquals(2, matchMap.size());
	}
	
	@Test
	public void testZeroFillToPrefix() {

		assertEquals("001", StringUtils.zeroFillToPrefix(3, 1));

		assertEquals("02", StringUtils.zeroFillToPrefix(2, 2));

		assertEquals("", StringUtils.zeroFillToPrefix(2, 100));

		assertEquals("99", StringUtils.zeroFillToPrefix(2, 99));

		assertEquals("", StringUtils.zeroFillToPrefix(0, 99));

		assertEquals("", StringUtils.zeroFillToPrefix(-1, 99));

		assertEquals("", StringUtils.zeroFillToPrefix(-1, -1));

		assertEquals("", StringUtils.zeroFillToPrefix(5, 0));

		assertEquals("000000000000000001", StringUtils.zeroFillToPrefix(18, 1));

		assertEquals("", StringUtils.zeroFillToPrefix(19, 1));
		assertEquals("01", StringUtils.zeroFillToPrefix(2, 1));
	}
}
