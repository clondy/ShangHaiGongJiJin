/**
 * 
 */
package test.com.capinfo;

import org.junit.FixMethodOrder;
//import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
//import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>有事物的baseTest——执行的数据库操作都会在执行结束后rollback。</p>
 * 
 * @author Lancelot
 *
 *	, "classpath:applicationContext-acegi-security.xml"
 *	, "classpath:application-web-callcenter-service.xml"
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:application-root-ecomm.xml"
								, "classpath:application-core-ecomm.xml"
								, "classpath:application-component-security.xml"
								, "classpath:application-commons-project-service.xml"
								, "classpath:application-component-security-extensions.xml"
								, "classpath:application-component-treemenu.xml"
								, "classpath:application-accumulation-project-service.xml"
								, "classpath:application-component-kafka.xml"
								/*, "file:src/main/resources/application-web-service.xml"
								, "file:src/main/resources/applicationContext-jms.xml"*/})
@Transactional(transactionManager="transactionManager4Ecomm", propagation = Propagation.REQUIRED)
@Rollback(value=true)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BaseTest extends AbstractTransactionalJUnit4SpringContextTests {
	
}
