/*
 *    	@(#)@SecurityEntityTest.java  2016年10月26日
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.commons.project.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.capinfo.commons.project.model.security.SystemViewStamp;
import com.capinfo.framework.service.GeneralService;

import test.com.capinfo.BaseTest;

/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class SecurityEntityTest extends BaseTest {

	@Autowired
	public GeneralService generalService;
	
	@Test
	public void testViewStemp() {
		
		System.out.println(generalService.getAllObjects(SystemViewStamp.class));
		System.out.println("test: " + generalService.getObjectById(SystemViewStamp.class, 3L));
	}
}
