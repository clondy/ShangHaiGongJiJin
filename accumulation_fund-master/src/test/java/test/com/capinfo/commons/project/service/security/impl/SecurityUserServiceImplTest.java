/*
 *    	@(#)@SecurityUserServiceImplTest.java  2017年9月19日
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.commons.project.service.security.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.parameter.security.SystemRoleParameter;
import com.capinfo.commons.project.parameter.security.SystemUserParameter;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.commons.project.service.security.SystemUserService;
import com.capinfo.framework.exception.ObjectNotFoundException;
import com.capinfo.framework.service.CommonsDataOperationService;

import test.com.capinfo.BaseTest;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class SecurityUserServiceImplTest extends CommonsDataOperationServiceTest<SystemUser, SystemUserParameter> {

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private SystemRoleService systemRoleService;
	
	private SystemUser persistedUser;
	
	private SystemUser transientUser;
	
	private SystemUserParameter persistedParameter;
	
	private SystemUserParameter transientParameter;

	@Before
	public void setup() {

		Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
		Assert.assertTrue(TestTransaction.isActive());
		
		SystemRole role = new SystemRole();
		
		role.setName("TEST_TEST_TEST_ROLE");
		role.setDescription("单元测试角色");
		
		SystemRoleParameter roleParameter = new SystemRoleParameter();
		
		roleParameter.setEntity(role);
		
		//初始化角色数据
		role = systemRoleService.saveOrUpdate(roleParameter);
		
		
		
		persistedUser = new SystemUser();
		
		persistedUser.setName("test_data_name");
		persistedUser.setLogonName("test_data_logonname");
		persistedUser.setPassword("123456");
		persistedUser.setRegionId(1000L);

		persistedParameter = new SystemUserParameter();
		persistedParameter.setEntity(persistedUser);
		persistedParameter.setRoleIds(new Long[]{role.getId()});

		//初始化用户数据
		systemUserService.saveOrUpdate(persistedParameter);
		
		
		
		transientUser = new SystemUser();
		
		transientUser.setName("test_data_transientname");
		transientUser.setLogonName("test_data_transientlogonname");
		transientUser.setPassword("123456");
		persistedUser.setRegionId(1000L);
		

		transientParameter = new SystemUserParameter();
		
		transientParameter.setEntity(transientUser);
	}
	
	@Test
	public void testGetSystemUserById() {
		
		Assert.assertNotNull(systemUserService.getSystemUserById(persistedUser.getId()));
		
		Assert.assertNull(systemUserService.getSystemUserById(transientUser.getId()));
	}

	@Test
	public void testValidatorLogonNameExists() {
		
		//同登录名同id则返回false——保证以存在的用户不会误报登录名重复。
		Assert.assertFalse(systemUserService.validatorLogonNameExists(persistedParameter));

		
		SystemUserParameter parameter = new SystemUserParameter();
		
		SystemUser user = new SystemUser();
		
		user.setName("test_data_name");
		user.setLogonName("test_data_logonname");
		user.setPassword("123456");
		
		parameter.setEntity(user);
		
		//同登录名不同id则返回true——重登录名的用户已存在。
		Assert.assertTrue(systemUserService.validatorLogonNameExists(parameter));
		
		
		Assert.assertFalse(systemUserService.validatorLogonNameExists(transientParameter));
	}
	
	@Test
	public void testModifyPassword() {

		persistedParameter.setOriginalPassword("123456");

		String originalPassword = persistedParameter.getEntity().getPassword();
		
		persistedParameter.setNewPassword("654321");
		persistedParameter.setConfirmPassword("654321");
		
		Assert.assertTrue(systemUserService.modifyPassword(persistedParameter));
		Assert.assertNotEquals(originalPassword, persistedParameter.getEntity().getPassword());
	}
	
	@Test
	public void testGetSystemUserByLogonName() {
		
		SystemUser user = systemUserService.getSystemUserByLogonName(persistedParameter.getEntity().getLogonName());
		
		Assert.assertNotNull(user);
		Assert.assertEquals(user, persistedParameter.getEntity());
		
		SystemUser user2 = null;
		try{
			
			user2 = systemUserService.getSystemUserByLogonName(transientParameter.getEntity().getLogonName());
		} catch(ObjectNotFoundException e) {
			
			e.printStackTrace();
		}
		Assert.assertNull(user2);
	}
	
	@Test
	public void testValidateLogonNameAndPassword() {
		
		Assert.assertTrue(systemUserService.validateLogonNameAndPassword(persistedParameter.getEntity().getLogonName(), "123456"));

		Assert.assertFalse(systemUserService.validateLogonNameAndPassword(transientParameter.getEntity().getLogonName(), "123456"));
	}
	
	@Test
	public void testGetSystemUserByLogonNameAndPassword() {

		SystemUser user = systemUserService.getSystemUserByLogonNameAndPassword(persistedParameter.getEntity().getLogonName(), "123456");
		
		Assert.assertNotNull(user);
		Assert.assertEquals(user, persistedParameter.getEntity());
		

		Assert.assertNull(systemUserService.getSystemUserByLogonNameAndPassword(transientParameter.getEntity().getLogonName(), "123456"));
	}
	
	@Test
	public void testModifyPassword2() {
		
		String originalPassword = persistedParameter.getEntity().getPassword();
		boolean result = systemUserService.modifyPassword(persistedParameter.getEntity().getLogonName(), "123456", "654321");
	
		Assert.assertTrue(result);
		
		SystemUser user = systemUserService.getSystemUserByLogonName(persistedParameter.getEntity().getLogonName());
		Assert.assertNotEquals(originalPassword, user.getPassword());
	}

	@Override
	public CommonsDataOperationService<SystemUser, SystemUserParameter> getCommonsDataOperationService() {
		// TODO Auto-generated method stub
		return systemUserService;
	}

	@Override
	public SystemUserParameter getDeleteParameter(boolean isTransientEntity) {
		// TODO Auto-generated method stub
		return ( isTransientEntity )? transientParameter: persistedParameter;
	}

	@Override
	public SystemUserParameter getListParameter(boolean isHit) {
		// TODO Auto-generated method stub
		return ( isHit )? persistedParameter: transientParameter;
	}

	@Override
	public SystemUserParameter getSaveOrUpdateParameter() {
		// TODO Auto-generated method stub
		return persistedParameter;
	}

	@Override
	public SystemUser getSaveOrUpdateEntity() {
		// TODO Auto-generated method stub
		return persistedUser;
	}

	@Override
	public SystemUserParameter getObjectByIdParameter() {
		// TODO Auto-generated method stub
		return persistedParameter;
	}
}
