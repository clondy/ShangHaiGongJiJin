package test.com.capinfo.commons.project.utils;

import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.utils.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/10.
 */
public class JsonUtilsTests {
	
	private Map map;
	
	private SystemUser user;
	
	@Before
	public void setup() {

		map = new HashMap();
		map.put("name", "测试");
		map.put("date", new Date());
		map.put("time", new Timestamp(new Date().getTime()));
		

		user = new SystemUser();
		
		user.setName("test_data_name");
		user.setLogonName("test_data_logonname");
		user.setPassword("123456");
		user.setRegionId(1000L);
	}

	@Test
	public void testToGlobleJsonObject() {
		
		Assert.assertTrue(JsonUtils.toGlobleJsonObject(map).toString().indexOf("-") > 0);
	}

	@Test
	public void testToJson() {
		
		Assert.assertEquals("{\"date\":\"2017-10-26\",\"name\":\"测试\",\"time\":\"2017-10-26\"}"
							, JsonUtils.toJson(map).toString());
		
		Assert.assertEquals("{\"date\":\"2017-10-26\",\"time\":\"2017-10-26\"}"
							, JsonUtils.toJson(map, "name").toString());
		
		Assert.assertEquals("{\"name\":\"测试\"}"
							, JsonUtils.toJson(map, "date", "time").toString());
	}
	
	@Test
	public void testToJson4Entity() {

		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"test_data_logonname\",\"name\":\"test_data_name\",\"password\":\"123456\",\"region\":null,\"regionId\":1000,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4Entity(user).toString());
		
		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"test_data_logonname\",\"name\":\"\",\"password\":\"123456\",\"region\":null,\"regionId\":1000,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4Entity(user, "name").toString());
		
		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"\",\"name\":\"\",\"password\":\"\",\"region\":null,\"regionId\":1000,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4Entity(user, "name", "logonName", "password").toString());
	}
	
	@Test
	public void testToJson4EntityByProperties() {

		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"\",\"name\":\"\",\"password\":\"\",\"region\":null,\"regionId\":0,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4EntityByProperties(user).toString());
		
		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"\",\"name\":\"test_data_name\",\"password\":\"\",\"region\":null,\"regionId\":0,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4EntityByProperties(user, "name").toString());
		
		Assert.assertEquals("{\"enabled\":true,\"id\":0,\"logonName\":\"test_data_logonname\",\"name\":\"test_data_name\",\"password\":\"123456\",\"region\":null,\"regionId\":0,\"roleIdString\":\"\",\"roleIds\":[],\"roles\":[]}"
							, JsonUtils.toJson4EntityByProperties(user, "name", "logonName", "password").toString());
	}
}
