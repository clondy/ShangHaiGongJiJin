/*
 *    	@(#)@SystemResourceServiceImpl4EasyUITest.java  2017年9月4日
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.commons.project.service.security.impl;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.capinfo.commons.project.service.security.SystemResourceService;

import test.com.capinfo.BaseTest;

/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class SecurityResourceServiceImpl4EasyUITest extends BaseTest {

	@Resource(name = "securityResourceServiceImpl4EasyUI")
	private SystemResourceService service;

	@Test
	public void testBuildResource() {
		
		Assert.assertNotNull(service.buildResourceJson());
	}
}
