package test.com.capinfo.commons.project.utils.excel;
//package test.com.capinfo.commons.project.utils.excel;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.poi.ss.usermodel.Workbook;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.ClassPathResource;
//
//import test.com.capinfo.BaseTest;
//
//
//import com.capinfo.assistant.platform.model.SpecialIssue;
//import com.capinfo.assistant.platform.service.business.special.issue.SpecialIssueService;
//import com.capinfo.commons.project.utils.excel.*;
//import com.capinfo.framework.dao.SearchCriteriaBuilder;
//import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
//import com.capinfo.framework.service.GeneralService;
//
///**
// * <p>
// * 导出Excel--Test
// * 
// * </p>
// * 
// * @version 1.0
// * @author dinglei
// */
//public class ExcelBuilderTest extends BaseTest{
//	
//	private ExcelBuilder excelBuilder;
//	
//	@Autowired
//	private GeneralService generalService;
//	
//	
//	String absolutePath = null;
//	
//	@Before
//	public void initial(){
//		excelBuilder = new ExcelBuilder();
//	
//		try {
//			absolutePath = new ClassPathResource("", this.getClass()).getURL().getPath();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		this.setSqlScriptEncoding("UTF-8");
//		this.executeSqlScript(
//				"file:" + absolutePath + "specialissue.sql",
//				false);
//	}
//	
//	/**
//	 * 一个Sheet，无特殊格式要求的
//	 * @author dinglei
//	 */
//	@Test
//	public void testOneSheetGeneral() {
//		Workbook wb = excelBuilder.getWorkbook();
//		Theme theme = this.getTheme(wb);
//		excelBuilder.setExcelSheetList(this.getExcelSheetList());
//		excelBuilder.setTheme(theme);
//		try {
//			//注意：这里采用的是文件方式测试，在实际使用中调用writeToStream方法
//			excelBuilder.writeToFile(absolutePath +"testOneSheetGeneral.xlsx");
//
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	/**
//	 * 多个Sheet，无特殊格式要求的，这里以两个sheet为例
//	 * @author dinglei
//	 */
//	@Test
//	public void testMoreSheetGeneral() {
//		Workbook wb = excelBuilder.getWorkbook();
//		Theme theme = this.getTheme(wb);
//		excelBuilder.setExcelSheetList(this.getExcelSheetListSecond());
//		excelBuilder.setTheme(theme);
//		try {
//			//注意：这里采用的是文件方式测试，在实际使用中调用writeToStream方法
//			excelBuilder.writeToFile(absolutePath +"testMoreSheetGeneral.xlsx");
//
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	/**
//	 * 有特殊格式要求的，这里以一个sheet为例，如果想加入多个sheet,请参照testMoreSheetGeneral
//	 * @author dinglei
//	 */
//	@Test
//	public void testOneSheetSpecial() {
//		Workbook wb = excelBuilder.getWorkbook();
//		Theme theme = this.getTheme(wb);
//		excelBuilder.setExcelSheetList(this.getExcelSheetListThird());
//		excelBuilder.setTheme(theme);
//		try {
//			//注意：这里采用的是文件方式测试，在实际使用中调用writeToStream方法
//			excelBuilder.writeToFile(absolutePath +"testOneSheetSpecial.xlsx");
//
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * 一个Sheet，无特殊格式要求的
//	 * @author dinglei
//	 */
//	private List<ExcelSheet> getExcelSheetList(){
//		List<ExcelSheet> excelSheetList = new ArrayList<ExcelSheet>();
//		ExcelSheet excelSheet = new ExcelSheet();
//		excelSheet.setSchema(this.getSchema());
//		excelSheet.setSheetName("特刊");
//		excelSheet.setDataList(this.getDataList());
//		
//		excelSheetList.add(excelSheet);
//
//		return excelSheetList;
//	}
//	/**
//	 * 多个Sheet，无特殊格式要求的，这里以两个sheet为例
//	 * @author dinglei
//	 */
//	private List<ExcelSheet> getExcelSheetListSecond(){
//		List<ExcelSheet> excelSheetList = new ArrayList<ExcelSheet>();
//		
//		//第一个sheet
//		ExcelSheet excelSheet = new ExcelSheet();
//		excelSheet.setSchema(this.getSchema());
//		excelSheet.setSheetName("特刊");
//		excelSheet.setDataList(this.getDataList());
//		
//		//第二个sheet
//		ExcelSheet excelSheet2 = new ExcelSheet();
//		excelSheet2.setSchema(this.getSchema());
//		excelSheet2.setSheetName("特刊2");
//		excelSheet2.setDataList(this.getDataList());
//		
//		excelSheetList.add(excelSheet);
//		excelSheetList.add(excelSheet2);
//		return excelSheetList;
//	}
//	/**
//	 * 有特殊格式要求的，这里以一个sheet为例，如果想加入多个sheet,请参照testMoreSheetGeneral
//	 * 这里主要是Schema的不同，主要是配置Schema
//	 * @author dinglei
//	 */
//	private List<ExcelSheet> getExcelSheetListThird(){
//		List<ExcelSheet> excelSheetList = new ArrayList<ExcelSheet>();
//
//		ExcelSheet excelSheet = new ExcelSheet();
//		excelSheet.setSchema(this.getSchemaTwo());
//		excelSheet.setSheetName("特刊3");
//		excelSheet.setDataList(this.getDataList());
//		
//		excelSheetList.add(excelSheet);
//		return excelSheetList;
//	}
//	
//	/**
//	 * 以下为相关配置方法
//	 */
//	private Schema getSchema(){
//		Schema schema = new Schema();
//		schema.setTitle("小帮手特刊促销信息");
//		schema.setDefaultRowHeight(1000);
//		schema.setColumnList(this.getColumnList());
//		schema.addColHeaderCell(this.getColHeaderCellList());
//		return schema;
//	}
//	/**
//	 * 特殊格式的schema配置
//	 * @return
//	 */
//	private Schema getSchemaTwo(){
//		Schema schema = new Schema();
//		schema.setTitle("小帮手特刊促销信息--特殊格式");
//		schema.setDefaultRowHeight(1000);
//		schema.setColumnList(this.getColumnListTwo());
//		schema.addColHeaderCell(getColHeaderCellListParent());
//		schema.addColHeaderCell(this.getColHeaderCellListTwo());
//		schema.addColFooterCell(getColFooterCell());
//		return schema;
//	}
//	
//	/**
//	 * 表头
//	 */
//	private List<ColHeaderCell> getColHeaderCellListParent(){
//		List<ColHeaderCell> colHeaderCellList = new ArrayList<ColHeaderCell>();
//		ColHeaderCell colHeaderCell = null;
//		for(int i=0;i<3;i++){
//			colHeaderCell = new ColHeaderCell();
//			colHeaderCell.setLabel(this.getColHeaderCellLabelParent().get(i));
//			//横向合并单元格
//			colHeaderCell.setColSpan(2);
//			colHeaderCellList.add(colHeaderCell);
//		}
//		
//		return colHeaderCellList;
//	}
//	private List<ColHeaderCell> getColHeaderCellList(){
//		List<ColHeaderCell> colHeaderCellList = new ArrayList<ColHeaderCell>();
//		ColHeaderCell colHeaderCell = null;
//		for(int i=0;i<7;i++){
//			colHeaderCell = new ColHeaderCell();
//			colHeaderCell.setLabel(this.getColHeaderCellLabel().get(i));
//			colHeaderCellList.add(colHeaderCell);
//		}
//		
//		return colHeaderCellList;
//	}
//	private List<ColHeaderCell> getColHeaderCellListTwo(){
//		List<ColHeaderCell> colHeaderCellList = new ArrayList<ColHeaderCell>();
//		ColHeaderCell colHeaderCell = null;
//		for(int i=0;i<6;i++){
//			colHeaderCell = new ColHeaderCell();
//			colHeaderCell.setLabel(this.getColHeaderCellLabelTwo().get(i));
//			colHeaderCellList.add(colHeaderCell);
//		}
//		
//		return colHeaderCellList;
//	}
//	/**
//	 * 表头名
//	 */
//	private Map<Integer,String> getColHeaderCellLabelParent(){
//		Map<Integer,String> colHeaderCell = new HashMap<Integer,String>();
//		colHeaderCell.put(0, "地区");
//		colHeaderCell.put(1, "商品");
//		colHeaderCell.put(2, "价格");
//		return colHeaderCell;
//	}
//	
//	private Map<Integer,String> getColHeaderCellLabel(){
//		Map<Integer,String> colHeaderCell = new HashMap<Integer,String>();
//		colHeaderCell.put(0, "所属区");
//		colHeaderCell.put(1, "所属街道");
//		colHeaderCell.put(2, "商户名称");
//		colHeaderCell.put(3, "促销商品名称");
//		colHeaderCell.put(4, "商品原价");
//		colHeaderCell.put(5, "商品促销价");
//		colHeaderCell.put(6, "商品图片");
//		return colHeaderCell;
//	}
//	
//	private Map<Integer,String> getColHeaderCellLabelTwo(){
//		Map<Integer,String> colHeaderCell = new HashMap<Integer,String>();
//		colHeaderCell.put(0, "所属区");
//		colHeaderCell.put(1, "所属街道");
//		colHeaderCell.put(2, "商户名称");
//		colHeaderCell.put(3, "促销商品名称");
//		colHeaderCell.put(4, "商品原价");
//		colHeaderCell.put(5, "商品促销价");
//		return colHeaderCell;
//	}
//	
//	/**
//	 * 列属性
//	 * @return
//	 */
//	private List<Column> getColumnList(){
//		List<Column> columnList = new ArrayList<Column>();
//		Column column = new Column();
//		column.setColumnName(this.getColumnName().get(0));
//		column.setWidth(3000);
//		column.setIsAutoSize(false);
//		column.setIsGroupColumn(true);
//		column.setIsIndexColumn(false);
//		columnList.add(column);
//		for(int i=1;i<4;i++){
//			column = new Column();
//			column.setColumnName(this.getColumnName().get(i));
//			column.setWidth(3000);
//			column.setIsAutoSize(false);
//			column.setIsGroupColumn(true);
//			column.setIsIndexColumn(false);
//			columnList.add(column);
//		}
//		for(int i=4;i<6;i++){
//			column = new Column();
//			column.setColumnName(this.getColumnName().get(i));
//			column.setWidth(3000);
//			column.setIsAutoSize(false);
//			column.setIsGroupColumn(false);
//			column.setIsIndexColumn(false);
//			columnList.add(column);
//		}
//		column = new Column();
//		column.setWidth(5000);
//		column.setColumnName(this.getColumnName().get(6));
//		column.setIsAutoSize(false);
//		column.setIsGroupColumn(false);
//		column.setIsIndexColumn(false);
//		columnList.add(column);
//		return columnList;
//	}
//	
//	private List<Column> getColumnListTwo(){
//		List<Column> columnList = new ArrayList<Column>();
//		Column column = new Column();
//		column.setColumnName(this.getColumnNameTwo().get(0));
//		column.setWidth(3000);
//		column.setIsAutoSize(false);
//		column.setIsGroupColumn(true);
//		column.setIsIndexColumn(false);
//		columnList.add(column);
//		for(int i=1;i<4;i++){
//			column = new Column();
//			column.setColumnName(this.getColumnNameTwo().get(i));
//			column.setWidth(3000);
//			column.setIsAutoSize(false);
//			column.setIsGroupColumn(true);
//			column.setIsIndexColumn(false);
//			columnList.add(column);
//		}
//		for(int i=4;i<6;i++){
//			column = new Column();
//			column.setColumnName(this.getColumnNameTwo().get(i));
//			column.setWidth(3000);
//			column.setIsAutoSize(false);
//			column.setIsGroupColumn(false);
//			column.setIsIndexColumn(false);
//			columnList.add(column);
//		}
//		return columnList;
//	}
//	/**
//	 * 添加表尾
//	 */
//	private ColFooterCell getColFooterCell(){
//		ColFooterCell colFooterCell = new ColFooterCell();
//		colFooterCell.setCellName("合计:总共1000元");
//		return colFooterCell;
//	}
//	
//	/**
//	 * 每列字段名，对应映射类中要显示的属性
//	 */
//	private Map<Integer,String> getColumnName(){
//		Map<Integer,String> columnTitle = new HashMap<Integer,String>();
//		columnTitle.put(0, "provider.county.name");
//		columnTitle.put(1, "provider.street.name");
//		columnTitle.put(2, "provider.shopName");
//		columnTitle.put(3, "title");
//		columnTitle.put(4, "originPrice");
//		columnTitle.put(5, "discountPrice");
//		columnTitle.put(6, "image.content");
//		return columnTitle;
//	}
//	private Map<Integer,String> getColumnNameTwo(){
//		Map<Integer,String> columnTitle = new HashMap<Integer,String>();
//		columnTitle.put(0, "provider.county.name");
//		columnTitle.put(1, "provider.street.name");
//		columnTitle.put(2, "provider.shopName");
//		columnTitle.put(3, "title");
//		columnTitle.put(4, "originPrice");
//		columnTitle.put(5, "discountPrice");
//		return columnTitle;
//	}
//	/**
//	 * 数据
//	 * @return
//	 */
//	private List<SpecialIssue> getDataList(){
//		
//		SearchCriteriaBuilder<SpecialIssue> builder = new SearchCriteriaBuilder<SpecialIssue>(SpecialIssue.class);
//		builder.addQueryCondition("createTime", RestrictionExpression.GREATER_EQUALS_OP, "2013-12-1 00:00:00");
//		builder.addQueryCondition("createTime", RestrictionExpression.LESS_EQUALS_OP, "2013-12-1 23:59:59");
//		List<SpecialIssue> specialIssueList = generalService.getObjects(builder.build());
//		return specialIssueList;
//	}
//	/**
//	 * 主题
//	 * @param wb
//	 * @return
//	 */
//	private Theme getTheme(Workbook wb){
//		Theme theme = new DefaultTheme(wb);
//		return theme;
//	}
//}
