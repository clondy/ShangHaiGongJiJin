package test.com.capinfo.mybist;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.capinfo.accumulation.dao.GB_JZPZXXDao;
import com.capinfo.accumulation.model.accounting.GB_JZPZXX;
import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.GB_JZPZXXServiceI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import test.com.capinfo.BaseCommitDBTest;

public class DaoTest extends BaseCommitDBTest {
	
	@Autowired
	private GB_JZPZXXDao dao;
	
	@Autowired
	private GB_JZPZXXServiceI gB_JZPZXXService; 
		
	
	@Test
	public void insertTest(){
		GB_JZPZXX gb_JZPZXX = new GB_JZPZXX();
		gb_JZPZXX.setDFFSE(100.0);
		gb_JZPZXX.setDFFSE(101.0);
		gB_JZPZXXService.insert(gb_JZPZXX);
	}
	
	@Test
	public void updateTest(){
		GB_JZPZXX gb_JZPZXX = new GB_JZPZXX();
		gb_JZPZXX.setID(1L);
		gb_JZPZXX.setDFFSE(1000.0);
		//dao.updateByPrimaryKeySelective(gb_JZPZXX);
		gB_JZPZXXService.updateByPrimaryKeySelective(gb_JZPZXX);
	}
	
	@Test
	public void gridTest(){
		Map map = new HashMap();
		map.put("currentPieceNum", 1);
		map.put("perPieceSize", 1);
		map.put("sort", "ID");
		map.put("order", "DESC");
//		map.put("JZRQ_START_DATE_yyyyMMdd", "20171225");
//		map.put("JZRQ_END_DATE_yyyyMMdd", "20171225");
		map.put("KMBH_EQ_STR", "5");
		DataGrid dataGrid = gB_JZPZXXService.findbyByGrid(map);
		System.out.println(JSON.toJSONString(dataGrid));
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		System.out.println(gson.toJson(dataGrid));
	}
}
