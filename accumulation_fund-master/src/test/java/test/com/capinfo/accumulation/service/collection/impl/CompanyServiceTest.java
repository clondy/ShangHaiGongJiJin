package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.service.collection.CompanyHistoryService;
import com.capinfo.accumulation.service.collection.CompanyService;
import com.capinfo.framework.service.CommonsDataOperationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

/**
 * Created by thinkpadwyl on 2017/9/28.
 */
public class CompanyServiceTest extends CommonsDataOperationServiceTest<Company,CompanyParameter> {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyHistoryService historyService;

    private Company persistedCompany;

    private Company transientCompany;

    private CompanyAccountFund persistedCompanyAccountFund;

    private CompanyParameter persistedParameter;

    private CompanyParameter transientParameter;

    private CompanyParameter auditParameter;

    private CompanyHistory persistedCompanyHistory;

    private CompanyAccountHistory persistedCompanyAccountHistory;

    private CompanyHistoryParameter persistedHistoryParameter;



    @Before
    public void setup() {
        Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
        Assert.assertTrue(TestTransaction.isActive());
        Company company = new Company();

        company.setGbOrganizationCode("20170929001");


        CompanyParameter companyParameter = new CompanyParameter();

        companyParameter.setEntity(company);

        //初始化角色数据
        company = companyService.saveOrUpdate(companyParameter);
        Assert.assertNotNull(company.getId());

        persistedParameter = new CompanyParameter();
        persistedCompany = new Company();
        persistedCompany.setGbOrganizationCode("20170929002");
        persistedCompany.setGbCompanyName("国电电力");
        persistedCompany.setCompetentCode("123456789");

        persistedParameter.setCompanyCode("123456789");
        persistedParameter.setEntity(persistedCompany);

        persistedCompanyAccountFund = new CompanyAccountFund();
        persistedCompanyAccountFund.setCompanyAccount("gb2312");

        persistedParameter.setCompanyAccount(persistedCompanyAccountFund);

        companyService.saveOrUpdate(persistedParameter);

        transientParameter = new CompanyParameter();
        transientCompany = new Company();
        transientCompany.setGbOrganizationCode("20170929003");

        transientParameter.setEntity(transientCompany);

        persistedCompanyHistory = new CompanyHistory();
        persistedCompanyHistory.setGbCompanyAccount("886282802205");
        persistedCompanyHistory.setGbCompanyName("首都信息发展股份有限公司");
        persistedCompanyHistory.setNatureAccountId(123L);
        persistedCompanyHistory.setRegisteredAddress("上海市公积金中心");

        //时间是否需要格式化
        persistedCompanyHistory.setCompanyAddress("海淀区量子银座7层");
        persistedCompanyHistory.setGbCompanyEconomicType("私营");
        persistedCompanyHistory.setLegalPersonality(true);

        persistedCompanyAccountHistory = new CompanyAccountHistory();
        persistedCompanyAccountHistory.setBankCode("1qazxsw23edc");
        persistedCompanyAccountHistory.setCompanyAccount("cde32wsxzaq1");

        persistedCompanyHistory.getCompanyAccountHistories().add(persistedCompanyAccountHistory);

        persistedHistoryParameter = new CompanyHistoryParameter();

        persistedHistoryParameter.setEntity(persistedCompanyHistory);
        persistedHistoryParameter.setCompanyAccount(persistedCompanyAccountHistory);

        persistedCompanyHistory = historyService.saveOrUpdate(persistedHistoryParameter);
        Assert.assertNotNull(persistedCompanyHistory.getId());

        auditParameter = new CompanyParameter();
        Company subCompany = new Company();
        subCompany.setId(persistedCompanyHistory.getId());
        auditParameter.setConfirmerId("987654321");

        auditParameter.setEntity(subCompany);

    }

    @Test
    public void testSaveOrUpdate(){
        CompanyParameter companyParameter = new CompanyParameter();
        Company company = new Company();
        CompanyAccountFund companyAccountFund = new CompanyAccountFund();

        BeanUtils.copyProperties(persistedCompany,company,
                new String[]{"companyMessages","companyHistories",
                        "companyAccountFunds","companyRecords",
                        "delegationTransfers","operators",
                "entrustedContracts","persons","personHistories"});

        BeanUtils.copyProperties(persistedCompanyAccountFund,companyAccountFund,
                new String[]{
                "adjustmentRecord","pauseRecoveryRecord"
                        ,"payininfo","personAccountFund","USER_GGJ",
                        "transactionRecordDetail","payinSupplementInfo",
                        "transactionRecord","companyAnnualBill","companyAccountHistory"
        });

        companyAccountFund.setDelegationPaymentId(true);
        companyAccountFund.setId(persistedCompanyAccountFund.getId());
        companyAccountFund.setBankCode("88888");

        company.setGbCompanyName("test");
        company.setLegalPersonality(true);
        company.setId(persistedCompany.getId());

        companyParameter.setEntity(company);
        companyParameter.setCompanyAccount(companyAccountFund);

        Company theCompany = companyService.saveOrUpdate(companyParameter);

        Assert.assertNotNull(theCompany);
        Assert.assertEquals("test", theCompany.getGbCompanyName());
        Assert.assertEquals("123456789", theCompany.getCompetentCode());

        Assert.assertNotNull(theCompany.getCompanyAccountFunds());
        Assert.assertEquals("88888", theCompany.getCompanyAccountFunds().get(0).getBankCode());

    }

    @Test
    public void testIsExist(){

        Assert.assertNotNull(companyService.isExist(persistedParameter));
        Assert.assertNull(companyService.isExist(transientParameter));
    }

    @Test
    public void testAudit(){
        Assert.assertTrue(companyService.audit(auditParameter));
        Assert.assertFalse(companyService.audit(persistedParameter));
    }

    @Test
    public void getCompany(){
        CompanyParameter parameter = new CompanyParameter();
        parameter.getEntity().setGbCompanyAccount("cde32wsxzaq1");
        Company company = companyService.getCompany(parameter);
//        Assert.assertNotNull(company);
        if(company != null)
            Assert.assertEquals("cde32wsxzaq1",company.getGbCompanyAccount());
    }

    @Test
    public void testGetCompanyById(){

        Assert.assertNotNull(companyService.getCompanyById(persistedCompany.getId()));

        Assert.assertNull(companyService.getCompanyById(transientCompany.getId()));
    }

    @Override
    public CommonsDataOperationService<Company, CompanyParameter> getCommonsDataOperationService() {
        return companyService;
    }

    @Override
    public CompanyParameter getDeleteParameter(boolean isTransientEntity) {
        return ( isTransientEntity )? transientParameter: persistedParameter;
    }

    @Override
    public CompanyParameter getListParameter(boolean isHit) {
        return ( isHit )? persistedParameter: transientParameter;
    }

    @Override
    public CompanyParameter getSaveOrUpdateParameter() {
        return persistedParameter;
    }

    @Override
    public Company getSaveOrUpdateEntity() {
        return persistedCompany;
    }

    @Override
    public CompanyParameter getObjectByIdParameter() {
        return persistedParameter;
    }

}
