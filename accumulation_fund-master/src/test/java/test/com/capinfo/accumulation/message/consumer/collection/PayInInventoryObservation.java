/*
 *    	@(#)@PayInInventoryObservation.java  2017-10-27
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.accumulation.message.consumer.collection;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class PayInInventoryObservation {

	/**
	 * <p>
	 * 
	 * </p>
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-root-ecomm.xml"
																					, "application-core-ecomm.xml"
																					, "application-component-security.xml"
																					, "application-commons-project-service.xml"
																					, "application-component-security-extensions.xml"
																					, "application-component-treemenu.xml"
																					, "application-accumulation-project-service.xml"
																					, "application-component-kafka.xml");
		
		//SimpleMessageObserver<String, LogMessage> messageObserver = (SimpleMessageObserver<String, LogMessage>) context.getBean("simpleMessageObserver");
		System.out.println("启动！！！");

		context.start();
		
		System.in.read();
	}

}
