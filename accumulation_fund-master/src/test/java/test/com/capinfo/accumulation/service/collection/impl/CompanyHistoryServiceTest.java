package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;

import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.accumulation.service.collection.CompanyHistoryService;
import com.capinfo.accumulation.service.collection.CompanyUtilService;
import com.capinfo.accumulation.web.Constants;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.service.GeneralService;

import java.util.Random;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import test.com.capinfo.BaseCommitDBTest;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

/**
 * Created by Rexxxar on 2017/9/30.
 */

public class CompanyHistoryServiceTest
		extends /*BaseCommitDBTest*/ CommonsDataOperationServiceTest<CompanyHistory, CompanyHistoryParameter> {
	/**
	 * @Author: Rexxxar
	 * @Description
	 * @Date: 2017/9/30 11:30
	 */

	@Autowired
	private CompanyHistoryService companyHistoryService;

	@Autowired
	private GeneralService generalService;

	@Autowired
	private CompanyUtilService companyUtilService;

	private CompanyHistory persistedCompanyHistory;

	private CompanyHistory transientCompanyHistory;

	private CompanyHistoryParameter persistedParameter;

	private CompanyHistoryParameter transientParameter;

	@Before
	public void setup() {
		// 初始化
		Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
		Assert.assertTrue(TestTransaction.isActive());

		Long siteId = new Long(RandomUtils.nextInt(100));
		Constants.THREAD_LOCAL_DATA_SCOPE_ID.set(siteId);

		CompanyHistory companyHistory = new CompanyHistory();
		// companyHistory.setId(6282802205L);
		/* companyHistory.setGbOrganizationCode("91310106MA1FY1RF54"); */
		// 统一社会信用代码和组织机构代码 如果有 存入数据库
		// 若没有，写入虚拟组织机构代码 存入数据库
		// companyHistory.setCreditCode("91310106MA1FY1RF54");
		// companyHistory.setRegionId(2L);
		companyHistory.setGbCompanyAccount("886282802205");
		companyHistory.setGbCompanyName("首都信息发展股份有限公司");
		companyHistory.setNatureAccountId(567L);

		// companyHistory.setCompetentCode(5L);
		companyHistory.setRegisteredAddress("上海市公积金中心");
		// companyHistory.setRegisteredPostCode(100000L);

		// 时间是否需要格式化
		companyHistory.setCompanyAddress("海淀区量子银座7层");
		// companyHistory.setGbCompanyPostCode("000000L");
		companyHistory.setGbCompanyEconomicType("私营");
		// companyHistory.setCompanyRelatoinships(1L);
		companyHistory.setLegalPersonality(true);

		// 关于单位公积金历史的数据
		CompanyAccountHistory companyAccountHistory = new CompanyAccountHistory();
		companyAccountHistory.setBankCode("1qazxsw23edc");
		companyAccountHistory.setCompanyAccount("cde32wsxzaq1");

		persistedParameter = new CompanyHistoryParameter();
		// persistedParameter.setFlag(0L);
		persistedParameter.setFlag(1L);
		persistedCompanyHistory = companyHistory;
		persistedParameter.setEntity(companyHistory);
		persistedParameter.setCompanyAccount(companyAccountHistory);

		companyHistoryService.saveOrUpdate(persistedParameter);

		transientParameter = new CompanyHistoryParameter();
		transientCompanyHistory = new CompanyHistory();
		transientCompanyHistory.setGbCompanyName("首都信息发展股份有限公司");
		transientCompanyHistory.setCreditCode("dwead2423424");
		// transientCompany.setLegalPersonality(false);
		transientParameter.setEntity(transientCompanyHistory);
		/* Assert.assertNotNull(companyHistory.getId()); */
		/* Assert.assertNull(companyHistory.getId()); */

	}

	@Test
	public void saveOrUpdate() {
		// 关于单位历史的数据
		CompanyHistory companyHistory = new CompanyHistory();
		// companyHistory.setId(6282802205L);
		/* companyHistory.setGbOrganizationCode("91310106MA1FY1RF54"); */
		// 统一社会信用代码和组织机构代码 如果有 存入数据库
		// 若没有，写入虚拟组织机构代码 存入数据库
		// companyHistory.setCreditCode("91310106MA1FY1RF54");
		// companyHistory.setRegionId(2L);
		companyHistory.setGbCompanyAccount("886282802205");
		companyHistory.setGbCompanyName("首都信息发展股份有限公司");
		companyHistory.setNatureAccountId(2432424L);
		// companyHistory.setCompetentCode(5L);
		companyHistory.setRegisteredAddress("上海市公积金中心");
		// companyHistory.setRegisteredPostCode(100000L);

		// 时间是否需要格式化
		companyHistory.setCompanyAddress("海淀区量子银座7层");
		// companyHistory.setGbCompanyPostCode("000000L");
		companyHistory.setGbCompanyEconomicType("私营");
		// companyHistory.setCompanyRelatoinships(1L);
		companyHistory.setLegalPersonality(true);

		// 关于单位公积金历史的数据
		CompanyAccountHistory companyAccountHistory = new CompanyAccountHistory();
		companyAccountHistory.setBankCode("1qazxsw23edc");
		companyAccountHistory.setCompanyAccount("cde32wsxzaq1");

		CompanyHistoryParameter companyHistoryParameter = new CompanyHistoryParameter();

		companyHistoryParameter.setEntity(companyHistory);
		companyHistoryParameter.setCompanyAccount(companyAccountHistory);

		companyHistory = companyHistoryService.saveOrUpdate(companyHistoryParameter);

		Assert.assertNotNull(companyHistory.getId());

		companyHistoryParameter.getEntity().setGbCompanyName("test");

		companyHistoryService.saveOrUpdate(companyHistoryParameter);
		
		Assert.assertEquals("test", companyHistory.getGbCompanyName());
		Assert.assertEquals("1qazxsw23edc", companyAccountHistory.getBankCode());
		Assert.assertEquals("cde32wsxzaq1", companyAccountHistory.getCompanyAccount());

	}

	@Test
	public void auditInfo() {
		// 没有ID，直接退出
		// 有ID，先插入值，然后把值从数据库取出，看是否相等

		CompanyAccountHistory companyAccountHistory = new CompanyAccountHistory();
		companyAccountHistory.setCompanyHistoryId(persistedCompanyHistory.getId());

		Long id = persistedCompanyHistory.getId();

		companyHistoryService.auditInfo(id, 666666L, 7777777L);
		
		persistedCompanyHistory = generalService.getObjectById(CompanyHistory.class, id);

		generalService.fetchLazyProperty(persistedCompanyHistory, "companyAccountHistories");
		Assert.assertEquals((Long) 7777777L,
				persistedCompanyHistory.getCompanyAccountHistories().get(0).getConfirmerId());
		Assert.assertEquals((Long) 666666L, persistedCompanyHistory.getCompanyId());
		Assert.assertEquals((Long) null, persistedCompanyHistory.getOperatorId());

	}

	@Override
	public CommonsDataOperationService<CompanyHistory, CompanyHistoryParameter> getCommonsDataOperationService() {
		return companyHistoryService;
	}

	@Override
	public CompanyHistoryParameter getDeleteParameter(boolean isTransientEntity) {
		return (isTransientEntity) ? transientParameter : persistedParameter;
	}

	@Override
	public CompanyHistoryParameter getListParameter(boolean isHit) {
		return (isHit) ? persistedParameter : transientParameter;
	}

	@Override
	public CompanyHistoryParameter getSaveOrUpdateParameter() {
		return persistedParameter;
	}

	@Override
	public CompanyHistory getSaveOrUpdateEntity() {
		return persistedCompanyHistory;
	}

	@Override
	public CompanyHistoryParameter getObjectByIdParameter() {
		return persistedParameter;
	}

}
