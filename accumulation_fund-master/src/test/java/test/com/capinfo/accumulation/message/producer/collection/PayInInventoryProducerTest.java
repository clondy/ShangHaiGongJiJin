/*
 *    	@(#)@PayInInventoryProducerTest.java  2017年10月27日
 *     
 *      @COPYRIGHT@
 */
package test.com.capinfo.accumulation.message.producer.collection;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.capinfo.accumulation.message.model.collection.PayInInventoryMessage;
import com.capinfo.components.kafka.producer.SimpleProducer;

import test.com.capinfo.BaseTest;


/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class PayInInventoryProducerTest extends BaseTest {

	@Autowired
	private SimpleProducer<Object, PayInInventoryMessage> payInInventoryProducer;
	
	@Test
	public void testSend() {

//		payInInventoryProducer.send("com_capinfo_accumulation_fund_business_collection_payin_inventory_topic", "172.20.53.201", new PayInInventoryMessage(1343L));
//
//		payInInventoryProducer.send(1, new PayInInventoryMessage(1343L));
//           payInInventoryProducer.send(new PayInInventoryMessage(1343L));
	 payInInventoryProducer.send(1,new PayInInventoryMessage(19849l,"123",262l));
	}
}
