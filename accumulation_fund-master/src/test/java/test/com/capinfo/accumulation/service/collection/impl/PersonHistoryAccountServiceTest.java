package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.model.collection.PersonHistoryAccountFund;
import com.capinfo.accumulation.parameter.collection.PersonHistoryAccountFundParameter;
import com.capinfo.accumulation.service.collection.PersonAccountFundService;
import com.capinfo.accumulation.service.collection.PersonHistoryAccountFundService;
import com.capinfo.framework.service.GeneralService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.capinfo.BaseCommitDBTest;

/**
 * Created by Rexxxar on 2017/9/30.
 */

public class PersonHistoryAccountServiceTest
        extends BaseCommitDBTest    // CommonsDataOperationServiceTest<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> {
{
    /**
     * @Author: Rexxxar
     * @Description
     * @Date: 2017/9/30 11:30
     */

    @Autowired
    private PersonHistoryAccountFundService personHistoryAccountFundService;

    @Autowired
    private PersonAccountFundService personAccountFundService;


    @Autowired
    private GeneralService generalService;


    private PersonHistoryAccountFund personHistoryAccountFund = new PersonHistoryAccountFund();

    private PersonHistory personHistory = new PersonHistory();

    private PersonHistoryAccountFundParameter persistedParameter = new PersonHistoryAccountFundParameter();

    private PersonHistoryAccountFundParameter transientParameter = new PersonHistoryAccountFundParameter();

    private PersonAccountFund personAccountFund;

    @Before
    public void setup() {
        // 初始化

//        personHistory.setPersonalAccount("1212121212w");
//        personHistory.setName("小红");
//        personHistory.setIdCardTypeId(47L);
//        personHistory.setIdCardNumber("111111112222222222");
//        personHistory.setBirthYearMonth(new Date());
//        personHistory.setSexId(62L);
//        personHistory.setCompanyId(1361l);//恒安-心相印
//        personHistory.setWageIncome(new BigDecimal(11.1));
//        personHistory.setKoseki(2L);
//        BigDecimal bigDecimal = new BigDecimal(23.2);
//        bigDecimal.setScale(2, 5);
//        personHistoryAccountFund.setMonthPay(bigDecimal);
//        personHistoryAccountFund.setCompanyPaymentRatio(3l);    //单位缴存比例
//        personHistoryAccountFund.setPersonPaymentRatio(2l);        //个人缴存比例
//
//        personHistoryAccountFund.setCompanyAccountFundId(262L);
//        persistedParameter.setEntity(personHistoryAccountFund);
//        persistedParameter.setEntityPerson(personHistory);
//
//        PersonHistoryAccountFund personHistoryAccountFund = personHistoryAccountFundService.saveOrUpdate(persistedParameter);
//        Long id = personHistoryAccountFund.getId();
//
//        //审核
//        PersonAccountFundParameter personAccountFundParameter = new PersonAccountFundParameter();
//        personAccountFundParameter.getEntity().setId(id);
//        personAccountFund = personAccountFundService.audit(personAccountFundParameter);
//

    }

    @Test
    public void saveOrUpdate() {
        //基本
        //初始化数据保存

//       PersonHistoryAccountFund personHistoryAccountFund = personHistoryAccountFundService.saveOrUpdate(persistedParameter);
        Assert.assertNotNull(personHistoryAccountFund);
        Assert.assertEquals("小红", persistedParameter.getEntityPerson().getName());
        personHistory.setName("小刚");
        personHistoryAccountFundService.saveOrUpdate(persistedParameter);
        Assert.assertEquals("小刚", persistedParameter.getEntityPerson().getName());


    }

    /**
     * 补充
     */
    @Test
    public void supplement() {

        PersonHistoryAccountFundParameter parameter = new PersonHistoryAccountFundParameter();
        parameter.getEntity().setId(personAccountFund.getId());
        parameter.setExtra("extra");
        PersonHistoryAccountFund personHistoryAccountFund = personHistoryAccountFundService.saveOrUpdate(parameter);
        Assert.assertEquals(personAccountFund.getCompanyAccountFundId(), personHistoryAccountFund.getCompanyAccountFundId());
    }

    /**
     * 变更保存
     */
    @Test
    public void changeSaveOrUpdate(){
        PersonHistoryAccountFundParameter parameter = new PersonHistoryAccountFundParameter();
        parameter.getEntityPerson().setId(149L);
        parameter.getEntityPerson().setName("小绿");
        PersonHistory personHistory = personHistoryAccountFundService.changeSaveOrUpdate(parameter);
        Assert.assertEquals("小绿",personHistory.getName());
    }
    /**
     * 变更审核
     */
    @Test
    public void changeAudit(){
        PersonHistoryAccountFundParameter parameter = new PersonHistoryAccountFundParameter();
        parameter.setPreviousId(288L);
        parameter.getEntity().setId(288L);
        Person person = personHistoryAccountFundService.changeAudit(parameter);
        Assert.assertEquals("变更1",personHistory.getName());
    }



//    @Override
//    public CommonsDataOperationService<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> getCommonsDataOperationService() {
//        return personHistoryAccountFundService;
//    }
//
//    @Override
//    public PersonHistoryAccountFundParameter getDeleteParameter(boolean isTransientEntity) {
//        return (isTransientEntity) ? transientParameter : persistedParameter;
//    }
//
//    @Override
//    public PersonHistoryAccountFundParameter getListParameter(boolean isHit) {
//        Assert.assertTrue(!this.getCommonsDataOperationService().getList(this.getListParameter(true), this.getCommonsDataOperationService().getTotalCount(this.getListParameter(true)), 1).isEmpty());
//        Assert.assertTrue(this.getCommonsDataOperationService().getList(this.getListParameter(false), this.getCommonsDataOperationService().getTotalCount(this.getListParameter(false)), 1).isEmpty());
//        return (isHit) ? persistedParameter : transientParameter;
//    }
//
//    @Override
//    public PersonHistoryAccountFundParameter getSaveOrUpdateParameter() {
//        return persistedParameter;
//    }
//
//    @Override
//    public PersonHistoryAccountFund getSaveOrUpdateEntity() {
//        return personHistoryAccountFund;
//    }
//
//    @Override
//    public PersonHistoryAccountFundParameter getObjectByIdParameter() {
//        return persistedParameter;
//    }

}
