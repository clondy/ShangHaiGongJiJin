package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.accumulation.parameter.collection.PersonAccountFundParameter;
import com.capinfo.accumulation.service.collection.PersonAccountFundService;
import com.capinfo.accumulation.service.collection.PersonHistoryAccountFundService;
import com.capinfo.framework.service.GeneralService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.capinfo.BaseCommitDBTest;

/**
 * Created by Rexxxar on 2017/9/30.
 */

public class PersonAccountServiceTest
		extends BaseCommitDBTest //CommonsDataOperationServiceTest<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> {
{
	/**
	 * @Author: Rexxxar
	 * @Description
	 * @Date: 2017/9/30 11:30
	 */

	@Autowired
	private PersonAccountFundService personAccountFundService;
	@Autowired
	private PersonHistoryAccountFundService personhsitoryAccountFundService;

	@Autowired
	private GeneralService generalService;



	private PersonAccountFund personHistoryAccountFund = new PersonAccountFund();

	private Person personHistory = new Person();

	private PersonAccountFundParameter persistedParameter = new PersonAccountFundParameter();

	private PersonAccountFundParameter transientParameter = new PersonAccountFundParameter();

	@Before
	public void setup() {
		// 初始化

//		personHistory.setPersonalAccount("1212121212w");
//		personHistory.setName("小红");
//		personHistory.setIdCardTypeId(47L);
//		personHistory.setIdCardNumber("111111112222222222");
//		personHistory.setBirthYearMonth(new Date());
//		personHistory.setSexId(62L);
//		personHistory.setCompanyId(1361l);//恒安-心相印
//		personHistory.setWageIncome(new BigDecimal(11.1));
//		personHistory.setKoseki(2L);
//		BigDecimal bigDecimal=new BigDecimal(23.2);
//		bigDecimal.setScale(2,5);
//		personHistoryAccountFund.setMonthPay(bigDecimal);
//		personHistoryAccountFund.setCompanyPaymentRatio(3l);	//单位缴存比例
//		personHistoryAccountFund.setPersonPaymentRatio(2l);		//个人缴存比例
//
//		personHistoryAccountFund.setCompanyAccountFundId(262L);
//		persistedParameter.setEntity(personHistoryAccountFund);
//		persistedParameter.setEntityPerson(personHistory);



//		personHistoryAccountFundService.saveOrUpdate(persistedParameter);

//		transientParameter.setEntity(personHistoryAccountFund);
//		transientParameter.setEntityPerson(personHistory);

	}

	@Test
	public void saveOrUpdate() {

        PersonAccountFundParameter parameter = new PersonAccountFundParameter();
        parameter.getEntity().setId(125L);
        PersonAccountFund personAccount = personAccountFundService.audit(parameter);

        Assert.assertNotNull(personAccount);

	}

	@Test
	public void getPersonAccount(){
        PersonAccountFundParameter personAccountFundParameter = new PersonAccountFundParameter();
        personAccountFundParameter.getEntity().setId(92L);
        PersonAccountFund personAccountFund = personAccountFundService.getPersonAccountFund(personAccountFundParameter);
        Assert.assertEquals("262",personAccountFund.getCompanyAccountFundId()+"");
    }


//
//	@Override
//	public CommonsDataOperationService<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> getCommonsDataOperationService() {
//		return personHistoryAccountFundService;
//	}
//
//	@Override
//	public PersonHistoryAccountFundParameter getDeleteParameter(boolean isTransientEntity) {
//		return (isTransientEntity) ? transientParameter : persistedParameter;
//	}
//
//	@Override
//	public PersonHistoryAccountFundParameter getListParameter(boolean isHit) {
//		return (isHit) ? persistedParameter : transientParameter;
//	}
//
//	@Override
//	public PersonHistoryAccountFundParameter getSaveOrUpdateParameter() {
//		return persistedParameter;
//	}
//
//	@Override
//	public PersonHistoryAccountFund getSaveOrUpdateEntity() {
//		return personHistoryAccountFund;
//	}
//
//	@Override
//	public PersonHistoryAccountFundParameter getObjectByIdParameter() {
//		return persistedParameter;
//	}

}
