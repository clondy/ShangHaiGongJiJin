package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.service.collection.CompanyUtilService;
import com.capinfo.accumulation.service.collection.impl.CompanyUtilServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.capinfo.BaseTest;

/**
 * Created by xiaopeng on 2017/10/9.
 */
public class CompanyUtilServiceTest extends BaseTest {
    @Autowired
    private CompanyUtilService companyUtilService;
    @Before
    public void setup() {
        //companyUtilService=new CompanyUtilServiceImpl();
    }
    @Test
    public void testGenerateCompanyCode() {
        String baseCode=companyUtilService.generateBaseCompanyCode();
        Company company=new Company();
        company.setGbCompanyAccount(baseCode);
        String supportCode=companyUtilService.generateSupplyCompanyCode(company);
        Assert.assertTrue(baseCode.substring(1,9).equals(supportCode.substring(3,11)));
        Assert.assertTrue(baseCode.length()==12);
        Assert.assertTrue(supportCode.length()==12);
        //Assert.assertTrue(baseCode.startsWith("N"));
        //Assert.assertTrue(baseCode.startsWith("N"));
        Assert.assertTrue(baseCode.endsWith("205"));
        Assert.assertTrue(supportCode.startsWith("209"));
    }
    @Test
    public void testGetLegalPersonInfo() {
        Company company=companyUtilService.getLegalPersonInfo("123");
        Company company2=companyUtilService.getLegalPersonInfo("12345678");
        Assert.assertTrue(company.getGbOrganizationCode()==null);
        Assert.assertTrue(company2.getGbOrganizationCode()!=null);
    }
}
