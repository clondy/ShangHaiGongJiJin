package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.service.collection.PayininfoService;
import com.capinfo.accumulation.service.collection.PayinitemService;
import com.capinfo.accumulation.service.collection.PersonAccountFundService;
import com.capinfo.accumulation.service.collection.PersonService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import test.com.capinfo.BaseCommitDBTest;
import test.com.capinfo.BaseTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wcj on 2017/10/30.
 */
public class PayinitemServiceTest extends BaseTest {
    @Autowired
    private PersonAccountFundService personAccountFundService;
    @Autowired
    private PayinitemService payinitemService;
    @Autowired
    private PersonService personService;

    @Autowired
    private PayininfoService payininfoService;

    private Payininfo persistedPayininfo;

    private  List<PersonAccountFund> personAccountFunds=new ArrayList<>();

    private PersonAccountFund persistedPersonAccountFund;



    private Person persistedPerson;


    @Before
    public void setup(){
//        SearchCriteriaBuilder<PersonAccountFund> criteriaBuilder = new SearchCriteriaBuilder<>(
//                PersonAccountFund.class);
//
//        criteriaBuilder.addQueryCondition("companyAccountFundId", RestrictionExpression.EQUALS_OP,
//               262l);
//
//        criteriaBuilder.addQueryCondition("isPayment", RestrictionExpression.EQUALS_OP, 1l);
//        personAccountFunds= personAccountFundService.getPersonAccountBycri(criteriaBuilder);
        Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
        Assert.assertTrue(TestTransaction.isActive());
        Person person = new Person();
        person.setWageIncome(new BigDecimal(100));
        persistedPerson= personService.saveOrUpdate(person);
        PersonAccountFund personAccountFund = new PersonAccountFund();
        personAccountFund.setPersonPaymentRatio(10l);
        personAccountFund.setCompanyPaymentRatio(10l);
        personAccountFund.setRegularBalance(new BigDecimal(100));
        personAccountFund.setCurrentBalance(new BigDecimal(100));
        personAccountFund.setPerson(persistedPerson);
        persistedPersonAccountFund=personAccountFundService.saveOrUpdate(personAccountFund);
        personAccountFunds.add(persistedPersonAccountFund);
        Payininfo payininfo = new Payininfo();
        payininfo.setId(1000000l);
        persistedPayininfo=payininfoService.saveOrUpdate(payininfo);
    }
    @Test
    public void testsaveOrUpdate(){

        Boolean batch = payinitemService.saveOrUpdateBatch(personAccountFunds, "123456789999", persistedPayininfo.getId());
        Assert.assertTrue(batch);
    }
}
