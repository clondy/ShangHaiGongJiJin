package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.service.collection.*;
import com.capinfo.framework.service.CommonsDataOperationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import test.com.capinfo.BaseTest;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

import java.util.Date;
import java.util.List;

/**
 * Created by wcj on 2017/10/29.
 */
public class PayininfoServiceTest extends BaseTest {
    @Autowired
    private PayininfoService payininfoService;

    @Autowired
    private CompanyAccountFundService companyAccountService;
    @Autowired
    private PersonAccountFundService personAccountService;



    private Payininfo persistedPayininfo;

    private Payininfo transientPayininfo;

    private CompanyAccountFund persistedCompanyAccountFund;

    private PayininfoParameter persistedParameter;

    private PayininfoParameter transientParameter;


    private CompanyParameter auditParameter;






   @Before
   public void setup() {
        Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
        Assert.assertTrue(TestTransaction.isActive());
       Payininfo payininfo = new Payininfo();
       payininfo.setCompanyAccount("123");
       payininfo.setIsfinished(true);
       payininfo.setVerificationOptions(87l);


   }

    @Test
    public void testConfirm(){
        PayininfoParameter payininfoParameter = new PayininfoParameter();
        payininfoParameter.setCompanyAccot("123");
        payininfoParameter.setVerificationOptions(87l);
        payininfoParameter.setRemitDate(new Date());
        boolean confirm = payininfoService.confirm(payininfoParameter);

        Assert.assertTrue(confirm);


    }

    @Test
    public void testsearchPayinfouncheck(){
        PayininfoParameter payininfoParameter = new PayininfoParameter();
        payininfoParameter.setCompanyAccot("123");
        payininfoParameter.setVerificationOptions(87l);
        List<Payininfo> payininfos = payininfoService.searchPayinfouncheck(payininfoParameter);
        Assert.assertNotNull(payininfos);
    }

    @Test
    public void testbuildPayininfoQc(){
        Payininfo payininfo = new Payininfo();
        payininfo.setCompanyAccount("123");
        payininfo.setVerificationOptions(87l);

        PayininfoParameter payininfoParameter = new PayininfoParameter();
        payininfoParameter.setStartPaymentDate("201710");
        payininfoParameter.setEndPaymentDate("201712");

        payininfoParameter.setEntity(payininfo);
        boolean payininfoQc = payininfoService.buildPayininfoQc(payininfoParameter, "admin");
        Assert.assertTrue(payininfoQc);

    }
    @Test
    public void testgetCompanyAccount(){
        PayininfoParameter payininfoParameter = new PayininfoParameter();
        payininfoParameter.setCompanyAccot("123");
        CompanyAccountFund companyAccount = payininfoService.getCompanyAccount(payininfoParameter);
        Assert.assertEquals("国资委",companyAccount.getUnitName());
    }

}
