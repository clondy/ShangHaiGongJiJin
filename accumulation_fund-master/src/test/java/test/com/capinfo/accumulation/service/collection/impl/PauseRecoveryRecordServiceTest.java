package test.com.capinfo.accumulation.service.collection.impl;


import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.accumulation.model.collection.PauseRecoveryRecord;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryRecordParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.accumulation.service.collection.PauseRecoveryRecordService;
import com.capinfo.framework.service.CommonsDataOperationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.com.capinfo.BaseCommitDBTest;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

import java.math.BigDecimal;

/**
 * Created by Rexxxar on 2017/10/19.
 */
public class PauseRecoveryRecordServiceTest extends BaseCommitDBTest/*CommonsDataOperationServiceTest*//*<PauseRecoveryRecord,PauseRecoveryRecordParameter>*/ {


    @Autowired
    PauseRecoveryRecordService pauseRecoveryRecordService;

    @Autowired
    CompanyAccountFundService companyAccountFundService;


    private PauseRecoveryRecordService persistedService;

    private PauseRecoveryRecordService transientService;

    private PauseRecoveryRecordParameter persistedParameter;

    private PauseRecoveryRecordParameter transientParameter;

    private PauseRecoveryInventory pauseRecoveryInventory;

    private PauseRecoveryRecord pauseRecoveryRecord;



    @Before
    public void setup(){
        //初始化

        //停复缴清册表
        PauseRecoveryInventory pauseRecoveryInventory = new PauseRecoveryInventory();
        pauseRecoveryInventory.setId(12L);
        pauseRecoveryInventory.setCompanyAccountFundId(262L);


        //停复缴记录表
        PauseRecoveryRecord pauseRecoveryRecord = new PauseRecoveryRecord();
        pauseRecoveryRecord.setCompanyAccountFundId(262L);
        pauseRecoveryRecord.setPersonAccountFundId(20L);
        //pauseRecoveryRecord.setPauseRecoveryInventoryId();//流水号
        pauseRecoveryRecord.setReasonId(3L);
        //pauseRecoveryRecord.setStopTo();//停缴至
        BigDecimal bigDecimal=new BigDecimal(26783.28);
        bigDecimal.setScale(2,5);
        pauseRecoveryRecord.setWage(bigDecimal);
        pauseRecoveryRecord.setCompanyPaymentRatio(7);
        pauseRecoveryRecord.setPersonPaymentRatio(5);
        BigDecimal monthPayment=new BigDecimal(266543783.28);
        bigDecimal.setScale(2,5);
        pauseRecoveryRecord.setMonthPayment(monthPayment);
        BigDecimal balance=new BigDecimal(2.28);
        bigDecimal.setScale(2,5);
        pauseRecoveryRecord.setBalance(balance);
        pauseRecoveryRecord.setReasonId(1L);
        pauseRecoveryRecord.setSiteId(5L);
        pauseRecoveryRecord.setStatus(2);


/*
        persistedParameter.setEntityMessage(pauseRecoveryInventory);
        persistedParameter.setEntity(pauseRecoveryRecord);

        transientParameter.setEntityMessage(pauseRecoveryInventory);
        transientParameter.setEntity(pauseRecoveryRecord);
*/


    }

    @Test
    public void saveOrUpdate(){
        //停复缴记录表

        PauseRecoveryRecord pauseRecoveryRecord = new PauseRecoveryRecord();
        pauseRecoveryRecord.setCompanyAccountFundId(262L);
        pauseRecoveryRecord.getCompanyAccountFund().setId(2L);
        pauseRecoveryRecord.getPauseRecoveryInventory().setId(2L);
        pauseRecoveryRecord.setPersonAccountFundId(20L);
        pauseRecoveryRecord.getPersonAccountFund().setId(2L);
        pauseRecoveryRecord.setPauseRecoveryInventoryId(1L);//流水号
        pauseRecoveryRecord.setReasonId(3L);
        //pauseRecoveryRecord.setStopTo();//停缴至
        BigDecimal bigDecimal=new BigDecimal(26783.28);
        bigDecimal.setScale(2,5);
        pauseRecoveryRecord.setWage(bigDecimal);
        pauseRecoveryRecord.setCompanyPaymentRatio(7);
        pauseRecoveryRecord.setPersonPaymentRatio(5);
        BigDecimal monthPayment=new BigDecimal(266543783.28);
        monthPayment.setScale(2,5);
        pauseRecoveryRecord.setMonthPayment(monthPayment);
        BigDecimal balance=new BigDecimal(2.28);
        balance.setScale(2,5);
        pauseRecoveryRecord.setBalance(balance);
        pauseRecoveryRecord.setOperatorId(1L);
        pauseRecoveryRecord.setReasonId(1L);
        pauseRecoveryRecord.setSiteId(5L);
        pauseRecoveryRecord.setStatus(2);


        // 停复缴记录详细表
/*        PauseRecoveryRecordMessage pauseRecoveryRecordMessage = new PauseRecoveryRecordMessage();
        pauseRecoveryRecordMessage.setBankCode("1qazxsw23edc");
        pauseRecoveryRecordMessage.setSerialNumber("cde32wsxzaq1");*/

        PauseRecoveryRecordParameter pauseRecoveryRecordParameter = new PauseRecoveryRecordParameter();

        pauseRecoveryRecordParameter.setEntity(pauseRecoveryRecord);
        /*pauseRecoveryRecordParameter.setEntityMessage(pauseRecoveryRecordMessage);
*/
        pauseRecoveryRecordService.saveOrUpdate(pauseRecoveryRecordParameter);

        Assert.assertNotNull(pauseRecoveryRecord.getId());
        Assert.assertEquals((Long)5L,pauseRecoveryRecord.getSiteId());


        /*pauseRecoveryRecordParameter.getEntity().setBankCode("ding");

        pauseRecoveryRecordService.saveOrUpdateMessage(pauseRecoveryRecordParameter);*/


        /*Assert.assertEquals((Long) 888L,pauseRecoveryRecord.getSiteId());*/

    }

   /* @Test
    public void saveOrUpdateRecord(){

        //停复缴记录表
        PauseRecoveryRecord pauseRecoveryRecord = new PauseRecoveryRecord();
        pauseRecoveryRecord.setCompanyAccountFundId(262L);
        pauseRecoveryRecord.getCompanyAccountFund().setId(2L);
        pauseRecoveryRecord.getPauseRecoveryInventory().setId(2L);
        pauseRecoveryRecord.setPersonAccountFundId(20L);
        pauseRecoveryRecord.getPersonAccountFund().setId(2L);
        pauseRecoveryRecord.setPauseRecoveryInventoryId(1L);//流水号
        pauseRecoveryRecord.setReasonId(3L);
        //pauseRecoveryRecord.setStopTo();//停缴至
        BigDecimal bigDecimal=new BigDecimal(26783.28);
        bigDecimal.setScale(2,5);
        pauseRecoveryRecord.setWage(bigDecimal);
        pauseRecoveryRecord.setCompanyPaymentRatio(7);
        pauseRecoveryRecord.setPersonPaymentRatio(5);
        BigDecimal monthPayment=new BigDecimal(266543783.28);
        monthPayment.setScale(2,5);
        pauseRecoveryRecord.setMonthPayment(monthPayment);
        BigDecimal balance=new BigDecimal(2.28);
        balance.setScale(2,5);
        pauseRecoveryRecord.setBalance(balance);
        pauseRecoveryRecord.setOperatorId(1L);
        pauseRecoveryRecord.setReasonId(1L);
        pauseRecoveryRecord.setSiteId(5L);
        pauseRecoveryRecord.setStatus(2);


        PauseRecoveryRecordParameter parameter = new PauseRecoveryRecordParameter();
        parameter.setEntity(pauseRecoveryRecord);


        pauseRecoveryRecord = pauseRecoveryRecordService.saveOrUpdateRecord(parameter);
*//*        Assert.assertNotNull(pauseRecoveryInventory.getId());*//*

        parameter.getEntity().setWage(BigDecimal.valueOf(12L));

        pauseRecoveryRecordService.saveOrUpdateRecord(parameter);


        Assert.assertEquals((Long) 5L,pauseRecoveryRecord.getSiteId());



    }*/


   /* @Test
    public void SearchCriteriaBuilder(){

        PauseRecoveryRecord pauseRecoveryRecord = new PauseRecoveryRecord();
        *//*pauseRecoveryRecord.setId(6282802205L);*//*

        PauseRecoveryRecordParameter pauseRecoveryRecordParameter = new PauseRecoveryRecordParameter();
        pauseRecoveryRecordParameter.setEntity(pauseRecoveryRecord);
        *//*pauseRecoveryRecordParameter.setPerson(Person);*//*

        persistedParameter = new PauseRecoveryRecordParameter();


        pauseRecoveryRecord = pauseRecoveryRecordService.saveOrUpdate(pauseRecoveryRecordParameter);
        Assert.assertNotNull(pauseRecoveryRecord.getId());


    }*/

    @Test
    public void recoveryCheckAccount(){
        CompanyAccountFund companyAccountFund = new CompanyAccountFund();
        companyAccountFund.setCompanyAccount("L1234rty");

        pauseRecoveryRecordService.recoveryCheckAccount("L1234rty");

    }

 /*   @Test
    public void openresonjson(){

    }
*/












}
