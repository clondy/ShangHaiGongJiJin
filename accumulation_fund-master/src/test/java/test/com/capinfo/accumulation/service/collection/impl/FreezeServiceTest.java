package test.com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Freeze;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.FreezeParameter;
import com.capinfo.accumulation.parameter.collection.PersonParameter;
import com.capinfo.accumulation.service.collection.CompanyService;
import com.capinfo.accumulation.service.collection.FreezeService;
import com.capinfo.accumulation.service.collection.PersonService;
import com.capinfo.framework.service.CommonsDataOperationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import test.com.capinfo.framework.service.CommonsDataOperationServiceTest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by thinkpadwyl on 2017/10/23.
 */
public class FreezeServiceTest extends CommonsDataOperationServiceTest<Freeze,FreezeParameter> {

    @Autowired
    private FreezeService freezeService;

    @Autowired
    private PersonService personService;

    private Freeze persistedFreeze;

    private Freeze transientFreeze;

    private FreezeParameter persistedParameter;

    private FreezeParameter transientParameter;

    private FreezeParameter auditParameter;

    @Before
    public void setup(){
        Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
        Assert.assertTrue(TestTransaction.isActive());
        Long l_personId = 513L;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();


        Freeze freeze = new Freeze();
        freeze.setPersonId(613L);
        freeze.setCourtNameId("1");
        freeze.setAmount(BigDecimal.valueOf(12L));
        freeze.setRegionId(5L);
        freeze.setState(3L);
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_YEAR, -1);
        Date aDate = c.getTime();
        freeze.setStartTime(aDate);
        freezeService.saveOrUpdate(freeze);

        persistedFreeze = new Freeze();
        persistedFreeze.setPersonId(l_personId);
        persistedFreeze.setCourtNameId("1");
        persistedFreeze.setAmount(BigDecimal.valueOf(12L));
        persistedFreeze.setRegionId(5L);
        persistedFreeze.setState(3L);


        c.setTime(new Date());
        c.add(Calendar.YEAR, -2);
        Date y = c.getTime();

        c.setTime(new Date());
        c.add(Calendar.YEAR, 10);
        Date w = c.getTime();

        persistedFreeze.setStartTime(y );
        persistedFreeze.setEndTime(w);




        persistedParameter = new FreezeParameter();
        persistedParameter.setEntity(persistedFreeze);

        freezeService.saveOrUpdate(persistedFreeze);

        transientFreeze = new Freeze();
        transientFreeze.setCourtNameId("2");
        transientFreeze.setRegionId(6L);
        transientFreeze.setAmount(BigDecimal.valueOf(13L));

        //transientFreeze.setStartTime(y);


        transientParameter = new FreezeParameter();

        transientParameter.setEntity(transientFreeze);

        auditParameter = new FreezeParameter();
        Freeze auditFreeze = new Freeze();
        auditFreeze.setId(persistedFreeze.getId());
        auditParameter.setEntity(auditFreeze);



    }

    @Test
    public void testSaveOrUpdate(){
        Freeze freeze = new Freeze();
        FreezeParameter freezeParameter = new FreezeParameter();

        BeanUtils.copyProperties(persistedFreeze,freeze,new String[]{"id"});
        freeze.setCourt("test001");

        freezeParameter.setEntity(freeze);
        freeze = freezeService.saveOrUpdate(freezeParameter);
        Assert.assertNotNull(freeze);
        Assert.assertEquals("test001",freeze.getCourt());
    }

    @Test
    public void testFreezeAudit(){
        Assert.assertTrue(freezeService.freezeAudit(auditParameter));
        Assert.assertFalse(freezeService.freezeAudit(transientParameter));
    }

    @Test
    public void testUnfreezeAudit(){
        Assert.assertTrue(freezeService.unfreezeAudit(auditParameter));
        Assert.assertFalse(freezeService.unfreezeAudit(transientParameter));
    }

    @Test
    public void testUnfreezeJob(){
        freezeService.unfreezeJob();
        Person unPerson = new Person();
        unPerson.setIdCardNumber("121332188009093215");
        PersonParameter personParameter = new PersonParameter();
        personParameter.setEntity(unPerson);
        Person person = personService.getPerson(personParameter);
        if(person != null) {
            if(person.getIsFreeze() != null)
            Assert.assertEquals("0", person.getIsFreeze().toString());
        }
    }

    @Test
    public void testDelay(){
        Assert.assertTrue(freezeService.delay(auditParameter));
        Assert.assertFalse(freezeService.delay(transientParameter));
    }

    @Test
    public void testUnfreeze(){
        Assert.assertTrue(freezeService.unfreeze(auditParameter));
        Assert.assertFalse(freezeService.unfreeze(transientParameter));
    }

    @Override
    public CommonsDataOperationService<Freeze, FreezeParameter> getCommonsDataOperationService() {
        return freezeService;
    }

    @Override
    public FreezeParameter getDeleteParameter(boolean isTransientEntity) {
        return ( isTransientEntity )? transientParameter: persistedParameter;
    }

    @Override
    public FreezeParameter getListParameter(boolean isHit) {
        return ( isHit )? persistedParameter: transientParameter;
    }

    @Override
    public FreezeParameter getSaveOrUpdateParameter() {
        return persistedParameter;
    }

    @Override
    public Freeze getSaveOrUpdateEntity() {
        return persistedFreeze;
    }

    @Override
    public FreezeParameter getObjectByIdParameter() {
        return persistedParameter;
    }

}
