package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.*;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.accumulation.service.collection.CompanyService;
import com.capinfo.accumulation.service.collection.PersonAccountFundService;
import com.capinfo.accumulation.service.collection.PersonService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.utils.JsonUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.WebRequestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Administrator on 2017/10/31.
 */
@Controller
@RequestMapping("/business/collection/overall_transfer")
public class OverallTransferController extends CommonsDataManageController<PersonParameter, Person> {

    private static final Log logger = LogFactory.getLog(Person.class);
    @Autowired
    private PersonService personService;
    @Autowired
    private CompanyAccountFundService companyAccountFundService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PersonAccountFundService personAccountFundService;
    @Override
    public CommonsDataOperationService<Person, PersonParameter> getCommonsDataOperationService() {

        return personService;
    }

    @Override
    public Map<String, Object> search(PersonParameter parameter) {
        return super.search(parameter);
    }

    /*@RequestMapping("/get_localhost_turnOutCompanyAccount.shtml")
    @ResponseBody
    public String getCompanyInfo( CompanyParameter parameter){

        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        CompanyParameter companyParameter=new CompanyParameter();
        companyParameter.getEntity().setGbCompanyAccount(parameter.getGbCompanyName());
        Company company= companyService.getCompany(companyParameter);
        company.getCompanyAccountFunds().get(0);
        object.put("gbCompanyName",company.getGbCompanyName());
        object.put("paymentSite",company.getPaymentSite());
        return object.toString();
    }

*/
    /**
     * 通过CompanyService查询本地转出单位信息
     */
    @RequestMapping("/get_localhost_turnOutCompanyAccount.shtml")
    @ResponseBody
    public String QueryTurnOutCompanyAccount(NativeWebRequest request, CompanyParameter parameter,OutputStream stream){
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);
        CompanyParameter companyParameter=new CompanyParameter();
        //查询出

        List<CompanyAccountFund> list=companyAccountFundService.getCompanyAccountByCretier(parameter.getTurnInCompanyAccount(),18L);
        PersonAccountFundParameter personAccountFundParameter=new PersonAccountFundParameter();
        personAccountFundParameter.getEntity().setCompanyAccountFundId(list.get(0).getId());
        PersonAccountFund personAccountFund=new PersonAccountFund();
        personAccountFund= personAccountFundService.getPersonAccountFund(personAccountFundParameter);
        /*//转化日期格式
        String sformat = new SimpleDateFormat("yyyy-MM").format(companyAccountFund.getCompanyPaymentDate());*/
        JSONObject object =  new JSONObject();
      /*  object.put("turnOutCompanyName",company.getGbCompanyName());
        object.put("turnOutPaymentDate",companyAccountFund.getCompanyPaymentDate());*/
        /*int count=0;
        if (company!=null) {
//            count = company.size();
        }
        Map map =new HashMap<String,Object>();
        map.put("count",count);
        try {
            IOUtils.outputByStream(stream, JsonUtils.toGlobleJsonObject(map).toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            LogUtils.debugException(logger,e);
        }*/
        return object.toString();
    }




    /**
     * 通过CompanyService查询本地转入单位信息
     */
    @RequestMapping("/get_localhost_turnInCompanyAccount.shtml")
    @ResponseBody
    public String QueryTurnInCompanyAccount( PersonTransferInfoParameter parameter){
        CompanyParameter companyParameter=new CompanyParameter();
        companyParameter.setCompanyCode(parameter.getCompanyAccount());

        Company company = companyService.getCompany(companyParameter);//获取company所有信息
        CompanyAccountFund companyAccountFund=company.getCompanyAccountFunds().get(0);
        JSONObject object =  new JSONObject();
        object.put("turnInCompanyName",company.getGbCompanyName());
        object.put("turnInPaymentDate",companyAccountFund.getCompanyPaymentDate());
        return object.toString();
    }

    @RequestMapping("/save_insert.shtml")
    public void buildPainfo(NativeWebRequest request, PersonTransferItemParameter parameter){
        WebRequestUtils.setContentType(request, "application/json;charset=UTF-8");

        Object nativeResponse = request.getNativeResponse();
        if (nativeResponse instanceof ServletResponse) {

            HttpServletResponse response = (HttpServletResponse) nativeResponse;
            OutputStream stream = null;
            SystemUser user  = (SystemUser)WebRequestUtils.retrieveSessionAttribute( request, com.capinfo.framework.web.Constants.ADMIN_ATTRIBUTE_KEY);
            try {
                stream = response.getOutputStream();
               boolean b=true;
                if (b) {
                    JSONObject json = new JSONObject();
                    json.put("result", "success");

                    IOUtils.outputByStream(stream, json.toString().getBytes("UTF-8"));
                } else {

                    IOUtils.outputByStream(stream, Boolean.FALSE.toString().getBytes());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
