package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryInventoryParameter;
import com.capinfo.accumulation.service.collection.PauseRecoveryInventoryService;
import com.capinfo.accumulation.service.collection.PauseRecoveryRecordService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.Constants;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.util.WebRequestUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpSession;
import java.io.Writer;
import java.util.*;

/**
 * Created by capinfo on 2017/10/31.
 */
@Controller
@RequestMapping("/business/collection/pause_recovery_inventory")
public class PauseRecoveryInventoryController extends CommonsDataManageController<PauseRecoveryInventoryParameter,PauseRecoveryInventory> {

    private static final Log logger = LogFactory.getLog(PauseRecoveryInventory.class);

    @Autowired
    private PauseRecoveryInventoryService pauseRecoveryInventoryService;

    @Autowired
    private PauseRecoveryRecordService pauseRecoveryRecordService;


    /**
     * 清单列表
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/26 14:17
     */
    @RequestMapping("/inventory_list.shtml")
    public Map<String, Object> list(NativeWebRequest request, PauseRecoveryInventoryParameter parameter) {
        Map<String, Object> viewModel = ControllerUtils.buildViewModel();
        if(this.listBeforeInvoke(request, parameter)) {
            List< PauseRecoveryInventory> entities = new ArrayList();
            long totalCount = this.getCommonsDataOperationService().getTotalCount(parameter);
            if(0L != totalCount) {
                entities = this.getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
            }

            viewModel.putAll((new DataListViewModel(parameter.getEntityClazz().getSimpleName() + "List", (Collection)entities, totalCount, this.getCommonsDataOperationService(), parameter)).buildViewModel());
        }
        return this.listAfterInvoke(request, parameter, viewModel);


    }

    @Override
    public CommonsDataOperationService<PauseRecoveryInventory, PauseRecoveryInventoryParameter> getCommonsDataOperationService() {

        return pauseRecoveryInventoryService;
    }

    /**
     * 弹出添加清册dialog
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/26 14:17
     */
    @RequestMapping("/inventory_form.shtml")
    public Map<String, Object> initial(NativeWebRequest request, PauseRecoveryInventoryParameter parameter) {

        Map<String, Object> viewModel = new HashMap<String, Object>();

        return viewModel;
    }

    @RequestMapping("/save_or_update_inventory.shtml")
    public void saveOrUpdate(NativeWebRequest request, PauseRecoveryInventoryParameter parameter, Writer writer){
        boolean flag = pauseRecoveryRecordService.recoveryCheckAccount(parameter.getCompanyAccountFund().getCompanyAccount());
        if(flag){
            parameter = this.saveOrUpdateBeforeInvoke(request, parameter);
            if(parameter == null) {
                ControllerUtils.outputByWriter(request, writer, false);
            } else {
                SystemUser user  = (SystemUser)WebRequestUtils.retrieveSessionAttribute(request, Constants.ADMIN_ATTRIBUTE_KEY);
                parameter.getEntity().setOperatorId(user.getId());
                PauseRecoveryInventory entity = pauseRecoveryInventoryService.saveOrUpdateInventory(parameter);
                this.saveOrUpdateAfterInvoke(request, parameter, entity);
                if(entity!=null){
                    JSONObject json = new JSONObject();
                    WebRequestUtils.storeSessionAttribute(request,"pauseRecoveryInventoryId",entity.getId());

                    WebRequestUtils.storeSessionAttribute(request,"inventoryFund",parameter.getCompanyAccountFund().getCompanyAccount());
                    json.put("result", true);

                    ControllerUtils.outputByWriter(request, writer, json.toString());
                }else{
                    ControllerUtils.outputByWriter(request, writer, entity != null);
                }

            }
        }
    }

    /**
     * 弹出确认名单
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/26 14:17
     */
    @RequestMapping("/confirm.shtml")
    public Map<String, Object> confirm(PauseRecoveryInventoryParameter parameter) {
        return ControllerUtils.buildViewModel(parameter);
    }




}
