/*
 *    	@(#)@Constants.java  2017年10月23日
 *     
 *      @COPYRIGHT@
 */
package com.capinfo.accumulation.web;

/**
 * <p>
 * 
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class Constants extends com.capinfo.framework.web.Constants {

	public static String COMPANY_BINDING_ATTRIBUTE_KEY = "company_binding";

	public static final String COOKIE_NAME_CLIENT_INFO = "cookie_name_client_info";
	
	public static final ThreadLocal<Long> THREAD_LOCAL_DATA_SCOPE_ID = new ThreadLocal<Long>();
}
