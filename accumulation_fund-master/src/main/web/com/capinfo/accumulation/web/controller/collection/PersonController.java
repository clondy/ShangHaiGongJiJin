package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.parameter.collection.PersonParameter;
import com.capinfo.accumulation.service.collection.PersonService;
import com.capinfo.commons.project.service.security.SystemUserService;
import com.capinfo.commons.project.web.controller.manage.region.RegionManageController;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Map;
import java.util.Set;

/**
 * Created by wangyulin on 2017/9/27.
 */


@Controller
@RequestMapping("/business/collection/person")
public class PersonController extends CommonsDataManageController<PersonParameter, Person> {

    private static final Log logger = LogFactory.getLog(RegionManageController.class);
    @Autowired
    private PersonService personService;

    @Autowired
    private SystemUserService systemUserService;

    @Override
    public CommonsDataOperationService<Person, PersonParameter> getCommonsDataOperationService() {

        return personService;
    }
    @Override
    protected Map<String, Object> formAfterInvoke(NativeWebRequest request, PersonParameter parameter, Map<String, Object> viewModel) {
        Set<Dictionary> regionIdList = personService.getDictionariesBySortName("区县");
        viewModel.put("regionIdList",regionIdList);
        Set<Dictionary> legalPersonIdCardTypeIdList = personService.getDictionariesBySortName("证件类型");
        viewModel.put("legalPersonIdCardTypeIdList",legalPersonIdCardTypeIdList);
        Set<Dictionary> sexList = personService.getDictionariesBySortName("性别");
        viewModel.put("sexList",sexList);
        Set<Dictionary> depositRateMarkIdList = personService.getDictionariesBySortName("缴存率标志");
        viewModel.put("depositRateMarkIdList",depositRateMarkIdList);
        if(parameter.getDeposit()=="deposit"){   //判断是否为封存专户
            Company depositCompany = personService.getDepositPerson();
            viewModel.put("depositCompany",depositCompany);
        }

        return super.formAfterInvoke(request, parameter, viewModel);
    }


    /**获取个人信息
     * 张旭
     * 2017年10月24日19:14:39
     * @param request
     * @param parameter 个人账号 身份证号码
     * @return
     */
    @RequestMapping("/get_person.shtml")
    @ResponseBody
    public String getCompany(NativeWebRequest request, PersonParameter parameter) {
        JSONObject personJson = new JSONObject();
            Person getPerson = personService.getPerson(parameter);
            String a = parameter.getEntity().getIdCardNumber();
            if (getPerson != null) {
                personJson.put("result", true);
                personJson.put("person.id", getPerson.getId());
                personJson.put("personId",getPerson.getId());
                personJson.put("person.name", getPerson.getName());
                personJson.put("person.sex", getPerson.getSexId());
                personJson.put("person.birthYearMonth", getPerson.getBirthYearMonth());
                personJson.put("person.idCardTypeId", getPerson.getIdCardTypeId());
                personJson.put("person.idCardNumber", getPerson.getIdCardNumber());
                //个人账号
                personJson.put("person.personalAccount", getPerson.getPersonalAccount());
                //单位账号
                personJson.put("person.company.gbCompanyAccount", getPerson.getCompany().getGbCompanyAccount());
                //单位名称
                personJson.put("person.company.gbCompanyName", getPerson.getCompany().getGbCompanyName());


            }else{
                personJson.put("result", false);
            }
        return personJson.toString();

    }

}
