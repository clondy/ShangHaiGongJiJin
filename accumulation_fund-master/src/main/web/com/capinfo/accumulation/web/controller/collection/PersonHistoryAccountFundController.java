package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.model.collection.PersonHistoryAccountFund;
import com.capinfo.accumulation.parameter.collection.PersonHistoryAccountFundParameter;
import com.capinfo.accumulation.service.collection.PersonHistoryAccountFundService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.service.security.SystemUserService;
import com.capinfo.commons.project.web.controller.manage.region.RegionManageController;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.Constants;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.WebRequestUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by wangyulin on 2017/9/27.
 */


@Controller
@RequestMapping("/business/collection/person_history_account_fund")
public class PersonHistoryAccountFundController extends CommonsDataManageController<PersonHistoryAccountFundParameter, PersonHistoryAccountFund> {

    private static final Log logger = LogFactory.getLog(RegionManageController.class);
    @Autowired
    private PersonHistoryAccountFundService personHistoryAccountFundService;

    @Autowired
    private SystemUserService systemUserService;


    @Override
    public CommonsDataOperationService<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> getCommonsDataOperationService() {

        return personHistoryAccountFundService;
    }

    /**
     * 数据确认 保存
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/9/28 10:32
     */
    @RequestMapping("/history/save_or_update.shtml")
    public void saveOrUpdate(NativeWebRequest request, PersonHistoryAccountFundParameter parameter, OutputStream stream){

        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);
        JSONObject jsonObject  = new JSONObject();
        SystemUser user  = (SystemUser) WebRequestUtils.retrieveSessionAttribute( request, Constants.ADMIN_ATTRIBUTE_KEY);
        try {
            parameter.getEntity().setOperatorId(user.getId());  //添加操作员
            parameter.getEntity().setCreateName(user.getName());    //添加创建人姓名
            PersonHistoryAccountFund personAccount = personHistoryAccountFundService.saveOrUpdate(parameter);
            jsonObject.put("id", personAccount.getId());
            jsonObject.put("isFirstSave", parameter.getIsFirstSave());
            jsonObject.put("result", "true");

        }catch (Exception e) {

            jsonObject.put("result", "false");
            LogUtils.debugException(logger, e);
        }

        try {

            IOUtils.outputByStream(stream , jsonObject.toString().getBytes("UTF-8"));

        }catch (UnsupportedEncodingException e) {

            LogUtils.debugException(logger, e);
        }

    }

    @RequestMapping("/history/change_save_or_update.shtml")
    public void changeSaveOrUpdate(NativeWebRequest request, PersonHistoryAccountFundParameter Parameter, OutputStream stream){
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);

        JSONObject jsonObject  = new JSONObject();

        SystemUser user  = (SystemUser) WebRequestUtils.retrieveSessionAttribute( request, Constants.ADMIN_ATTRIBUTE_KEY);

        try {
            Parameter.getEntity().setOperatorId(user.getId());
            PersonHistory personHistory = personHistoryAccountFundService.changeSaveOrUpdate(Parameter);
            jsonObject.put("id", Parameter.getEntityPerson().getId());
            jsonObject.put("personHistoryId",personHistory.getId());
            jsonObject.put("isFirstSave",Parameter.getIsFirstSave());
            jsonObject.put("result", "true");

        }catch (Exception e) {

            jsonObject.put("result", "false");
            LogUtils.debugException(logger, e);
        }

        try {

            IOUtils.outputByStream(stream , jsonObject.toString().getBytes("UTF-8"));

        }catch (UnsupportedEncodingException e) {

            LogUtils.debugException(logger, e);
        }
    }

    @RequestMapping("/person_change_audit.shtml")
    public Map<String,Object> changeAudit(NativeWebRequest request, PersonHistoryAccountFundParameter parameter,OutputStream stream){
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);

        JSONObject jsonObject  = new JSONObject();

        try {
            Person person= personHistoryAccountFundService.changeAudit(parameter);
            jsonObject.put("id", parameter.getEntity().getId());
//            jsonObject.put("personHistoryId",personHistory.getId());
            jsonObject.put("result", "true");

        }catch (Exception e) {

            jsonObject.put("result", "false");
            LogUtils.debugException(logger, e);
        }

        try {

            IOUtils.outputByStream(stream , jsonObject.toString().getBytes("UTF-8"));

        }catch (UnsupportedEncodingException e) {

            LogUtils.debugException(logger, e);
        }

        return null;
    }
}
