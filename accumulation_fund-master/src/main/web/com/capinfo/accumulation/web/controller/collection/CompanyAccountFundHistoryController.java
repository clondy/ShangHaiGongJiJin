package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundHistoryParameter;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundHistoryService;
import com.capinfo.accumulation.service.collection.CompanyHistoryService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.Constants;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.WebRequestUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;


/**
 * @Comment 单位信息历史表
 *
 * Created by Rexxxar on 2017/9/27.
 */
@Controller
@RequestMapping("/business/collection/company_account_fund_history")
public class CompanyAccountFundHistoryController extends CommonsDataManageController<CompanyAccountFundHistoryParameter,CompanyAccountHistory> {

    private static final Log logger = LogFactory.getLog(CompanyAccountHistory.class);
    @Autowired
    private CompanyAccountFundHistoryService companyAccountFundHistoryService;

    /**
     * 数据确认 保存
     *@Author: cairunbin
     *@Description
     *@Date: 2017/10/30 11:00
     */
    @RequestMapping("/history/save_or_update.shtml")
    public void saveOrUpdate(NativeWebRequest request, CompanyAccountFundHistoryParameter companyAccountFundHistoryParameter, OutputStream stream){

        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);

        JSONObject jsonObject  = new JSONObject();
        SystemUser user  = (SystemUser)WebRequestUtils.retrieveSessionAttribute( request, Constants.ADMIN_ATTRIBUTE_KEY);
        try {
            companyAccountFundHistoryParameter.getEntity().setOperatorId(user.getId());
            companyAccountFundHistoryParameter.getEntity().setCompanyHistoryId(companyAccountFundHistoryParameter.getCompanyId());
            //System. out.println("********////"+companyAccountFundHistoryParameter.getCompanyId());
            companyAccountFundHistoryParameter.getEntityCompany().setOperatorId(user.getId());



            CompanyAccountHistory companyAccountHistory = companyAccountFundHistoryService.saveOrUpdate(companyAccountFundHistoryParameter);
            if (companyAccountHistory !=null) {
                if (companyAccountFundHistoryParameter.getEntity().getStartPaymentDate().getTime()<=companyAccountHistory.getStartPaymentDate().getTime()){
                    jsonObject.put("id", companyAccountFundHistoryParameter.getEntity().getId());
                    jsonObject.put("result", true);
                }else {
                    jsonObject.put("result", false);
                    jsonObject.put("flag", "one");
                    jsonObject.put("errMsg", "补充公积金缴存年月不能超过基本公积金缴存年月！");
                }
            }else{
                jsonObject.put("result", false);
                jsonObject.put("flag", "two");
                jsonObject.put("errMsg", "单位缴存比例/个人缴存比例超出正常允许范围！");
            }
        }catch (Exception e) {

            jsonObject.put("result", false);
            LogUtils.debugException(logger, e);
        }

        try {

            IOUtils.outputByStream(stream , jsonObject.toString().getBytes("UTF-8"));

        }catch (UnsupportedEncodingException e) {

            LogUtils.debugException(logger, e);
        }

    }
}
