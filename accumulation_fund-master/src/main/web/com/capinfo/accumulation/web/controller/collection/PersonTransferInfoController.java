package com.capinfo.accumulation.web.controller.collection;


import com.capinfo.accumulation.model.collection.PersonTransferInfo;
import com.capinfo.accumulation.parameter.collection.PersonTransferInfoParameter;
import com.capinfo.accumulation.service.collection.PersonTransferInfoService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Lee on 2017/10/19.
 */
@Controller
@RequestMapping("/business/collection/persontransferinfo")
    public class PersonTransferInfoController extends CommonsDataManageController<PersonTransferInfoParameter, PersonTransferInfo> {

    private static final Log logger = LogFactory.getLog(PersonTransferInfo.class);
    @Autowired
    private PersonTransferInfoService personTransferInfoService;
    @Override
    public CommonsDataOperationService<PersonTransferInfo, PersonTransferInfoParameter> getCommonsDataOperationService() {

        return personTransferInfoService;
    }

}
