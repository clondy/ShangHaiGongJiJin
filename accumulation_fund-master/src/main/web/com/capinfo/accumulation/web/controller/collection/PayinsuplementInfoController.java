package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PayinSupplementInfo;
import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryRecordParameter;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.parameter.collection.PayinsuplementInfoParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.accumulation.service.collection.PayinsuplementInfoService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.utils.JsonUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.util.WebRequestUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wcj on 2017/10/31.
 */
@Controller
@RequestMapping("/business/collection/payinsuplemetinfo")
public class PayinsuplementInfoController extends CommonsDataManageController<PayinsuplementInfoParameter,PayinSupplementInfo> {
    private static final Log logger = LogFactory.getLog(PayinsuplementInfoController.class);
    @Autowired
    private PayinsuplementInfoService payinsuplementInfoService;



    @Autowired
    private CompanyAccountFundService companyAccountFundService;

    @Override
    public CommonsDataOperationService<PayinSupplementInfo, PayinsuplementInfoParameter> getCommonsDataOperationService() {
        return payinsuplementInfoService;
    }
    @Override
    public Map<String, Object> search(PayinsuplementInfoParameter parameter) {

        Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
        Set<Dictionary> siteList = payinsuplementInfoService.getDictionariesBySortName("缴存网点");
        viewModel.put("siteList", siteList);
        Set<Dictionary> hdItemList = payinsuplementInfoService.getDictionariesBySortName("公积金账户类型");
        viewModel.put("bjItemList",hdItemList);
        Set<Dictionary> bjReasonList = payinsuplementInfoService.getDictionariesBySortName("补缴原因");
        viewModel.put("bjReasonList",bjReasonList);
        return  viewModel;
    }

    @Override
    protected Map<String, Object> formAfterInvoke(NativeWebRequest request, PayinsuplementInfoParameter parameter, Map<String, Object> viewModel) {
        Set<Dictionary> hdItemList = payinsuplementInfoService.getDictionariesBySortName("公积金账户类型");
        viewModel.put("bjItemList",hdItemList);
        Set<Dictionary> siteList = payinsuplementInfoService.getDictionariesBySortName("缴存网点");
        viewModel.put("siteList", siteList);
        Set<Dictionary> bjReasonList = payinsuplementInfoService.getDictionariesBySortName("补缴原因");
        viewModel.put("bjReasonList",bjReasonList);
        return super.formAfterInvoke(request, parameter, viewModel);
    }

    @RequestMapping("/showuname.shtml")
    public void getCompanyAccount(NativeWebRequest request, PayinsuplementInfoParameter parameter,OutputStream stream){
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);
        Map map = new HashMap<String, Object>();
        List<CompanyAccountFund> accountFunds = companyAccountFundService.getCompanyAccountByCretier(parameter.getCompanyAccot(), null);
        if(!accountFunds.isEmpty()){
            map.put("uname", accountFunds.get(0).getUnitName());
        }

        try {
            IOUtils.outputByStream(stream, JsonUtils.toGlobleJsonObject(map).toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            LogUtils.debugException(logger, e);
        }
    }

    @RequestMapping("/payinsuplemetinfo_build.shtml")
    public void buildPayinsupplement(NativeWebRequest request,PayinsuplementInfoParameter parameter,OutputStream stream){
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);


            SystemUser user  = (SystemUser)WebRequestUtils.retrieveSessionAttribute( request, com.capinfo.framework.web.Constants.ADMIN_ATTRIBUTE_KEY);

        Map map =new HashMap<String,Object>();
        PayinSupplementInfo payinSupplementInfo = payinsuplementInfoService.buildPayinSupplementInfoQc(parameter, user.getLogonName(), user.getId());
        if(payinSupplementInfo!=null){

            map.put("result",1);

            map.put("paysupleId",payinSupplementInfo.getId());
            try {
                IOUtils.outputByStream(stream, JsonUtils.toGlobleJsonObject(map).toString().getBytes("UTF-8"));
                // IOUtils.outputByStream(stream, JSONObject.fromObject(map,jsonConfig).toString().getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                LogUtils.debugException(logger,e);
            }
        }else{
            map.put("result",2);
        }

        }


    @RequestMapping({"/confirm.shtml"})
    public Map<String, Object> confirm(PayinsuplementInfoParameter parameter) {

        return ControllerUtils.buildViewModel(parameter);

    }
    @RequestMapping({"/add.shtml"})
    public Map<String, Object> toAddForm(PayinsuplementInfoParameter parameter) {

        return ControllerUtils.buildViewModel(parameter);

    }

}
