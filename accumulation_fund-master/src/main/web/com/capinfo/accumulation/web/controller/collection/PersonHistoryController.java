package com.capinfo.accumulation.web.controller.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.parameter.collection.PersonHistoryParameter;
import com.capinfo.accumulation.service.collection.PersonHistoryService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.LogUtils;
import com.capinfo.framework.web.Constants;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.WebRequestUtils;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;


/**
 * @Comment 单位信息历史表
 *
 * Created by Rexxxar on 2017/9/27.
 */
@Controller
@RequestMapping("/business/collection/person_history")
public class PersonHistoryController extends CommonsDataManageController<PersonHistoryParameter,PersonHistory> {

    private static final Log logger = LogFactory.getLog(CompanyHistory.class);

    @Autowired
    private PersonHistoryService personHistoryService;


    /**
     * 数据确认
     * 2017年10月23日20:26:38
     */
    @RequestMapping("/save_or_update.shtml")
    public void saveOrUpdate(NativeWebRequest request, PersonHistoryParameter parameter, Writer writer, OutputStream stream) {
        WebRequestUtils.setCharacterEncodingForUTF8(request);
        WebRequestUtils.setContentTypeForHTML(request);
        JSONObject jsonObject  = new JSONObject();

        SystemUser user  = (SystemUser)WebRequestUtils.retrieveSessionAttribute( request, Constants.ADMIN_ATTRIBUTE_KEY);
        try {
            parameter.getEntity().setOperatorId(user.getId());  //设置操作人
            personHistoryService.saveOrUpdate(parameter);
            jsonObject.put("id", parameter.getEntity().getId());
            jsonObject.put("creditCode",parameter.getEntity().getPersonalAccount());
            jsonObject.put("result", "true");
        }catch (Exception e) {
            jsonObject.put("result", "false");
            LogUtils.debugException(logger, e);
        }

        try {

            IOUtils.outputByStream(stream , jsonObject.toString().getBytes("UTF-8"));

        }catch (UnsupportedEncodingException e) {

            LogUtils.debugException(logger, e);
        }

    }

    /**
     * 返回的字典项
     * @param request
     * @param parameter
     * @param viewModel
     * @return
     */

}
