package com.capinfo.accounting.web;


import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.accumulation.parameter.accounting.ZflzParameter;
import com.capinfo.accumulation.service.accounting.ZflzService;
import com.capinfo.accumulation.util.ExcelUtil;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

/**
 * Created by cairunbin on 2017/10/24.
 */

	@Controller
	@RequestMapping("/business/accounting/zflz")
	public class ZflzController extends CommonsDataManageController<ZflzParameter, Zflz> {

	@Autowired
	private ZflzService zflzService;

	@Override
	public CommonsDataOperationService<Zflz, ZflzParameter> getCommonsDataOperationService() {
		return zflzService;
	}
	 
	@RequestMapping({"/search.shtml"})
	public Map<String, Object> search(ZflzParameter parameter) {
	    Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	    Set<Dictionary> kjndList = zflzService.getDictionariesBySortName("会计年度");
	    viewModel.put("kjndList",kjndList);
	    List<Kmszjbsj> kmmcList = zflzService.findKmmc();
		viewModel.put("kmmcList", kmmcList);
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		viewModel.put("time",sdf.format(date));
		
	    return viewModel;
	}
	
	@Override
	@RequestMapping("/list.shtml")
	public Map<String, Object> list(NativeWebRequest request, ZflzParameter parameter) {
		// TODO Auto-generated method stub
		return super.list(request, parameter);
	}
	
	/**
	 * excel导出
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/excels.shtml" })
	@ResponseBody
	public void excels(HttpServletRequest request,HttpServletResponse response,ZflzParameter parameter) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Zflz> entities = new ArrayList<Zflz>();
		long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
		if (0 != totalCount) {
			entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
		}
		Map beanParams = new HashMap();
		beanParams .put("liste", entities);
		//Excel名称
		String excelName = "总分类账.xls";
		ExcelUtil.createExcel(ExcelUtil.readValueMB() + excelName, beanParams, ExcelUtil.readValueSC() + excelName);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(excelName);
	}

}
