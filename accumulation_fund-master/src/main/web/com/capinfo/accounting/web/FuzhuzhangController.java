package com.capinfo.accounting.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capinfo.accumulation.model.accounting.Fuzhuzhang;
import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Kemuhzb;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.FuzhuzhangParameter;
import com.capinfo.accumulation.parameter.accounting.KemuhzbParameter;
import com.capinfo.accumulation.service.accounting.FuzhuzhangService;
import com.capinfo.accumulation.util.ExcelUtil;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/fuzhumxz")
public class FuzhuzhangController extends CommonsDataManageController<FuzhuzhangParameter,Fuzhuzhang>{
	
	@Autowired
	private FuzhuzhangService fuzhuzhangService;
	
	@Override
	public CommonsDataOperationService<Fuzhuzhang, FuzhuzhangParameter> getCommonsDataOperationService() {
		return fuzhuzhangService;
	}
	
	@RequestMapping({"/search.shtml"})
	public Map<String, Object> search(FuzhuzhangParameter parameter) {
	    Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	    List<Kmszjbsj> kmmcList = fuzhuzhangService.findKmmc();
		viewModel.put("kmmcList", kmmcList);
		
	    List<Fzlxsz> fzlbList = fuzhuzhangService.findFzlb();
		viewModel.put("fzlbList", fzlbList);
		
		List<Fzhsxmjbsj> hsxmmcList = fuzhuzhangService.findHsxmmc();
		viewModel.put("hsxmmcList", hsxmmcList);
		return viewModel;
	}
	
	/**
	 * excel导出
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/excels.shtml" })
	@ResponseBody
	public void excels(HttpServletRequest request,HttpServletResponse response,FuzhuzhangParameter parameter) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Fuzhuzhang> entities = new ArrayList<Fuzhuzhang>();
		long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
		if (0 != totalCount) {
			entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
		}
		Map beanParams = new HashMap();
		beanParams .put("liste", entities);
		//Excel名称
		String excelName = "辅助明细账.xls";
		ExcelUtil.createExcel(ExcelUtil.readValueMB() + excelName, beanParams, ExcelUtil.readValueSC() + excelName);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(excelName);
	}
}
