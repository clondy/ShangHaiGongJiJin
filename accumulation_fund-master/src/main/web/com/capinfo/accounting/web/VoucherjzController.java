package com.capinfo.accounting.web;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.model.collection.Freeze;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.parameter.collection.FreezeParameter;
import com.capinfo.accumulation.service.accounting.VoucherService;
import com.capinfo.accumulation.service.accounting.VoucherjzService;
import com.capinfo.accumulation.service.collection.FreezeService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/pingzhengjizhang")
public class VoucherjzController extends CommonsDataManageController<VoucherParameter, Voucher> {
	
	 @Autowired
	  private VoucherjzService voucherjzService;
	
	  

	  @Autowired
	  public CommonsDataOperationService<Voucher, VoucherParameter> getCommonsDataOperationService() {
	        return voucherjzService;
	    }

		@RequestMapping("/site_list.shtml")
		 public ModelAndView index(NativeWebRequest request, VoucherParameter parameter) {
	        Map<String, Object> viewModel = ControllerUtils.buildViewModel();
	        ModelAndView mav = new ModelAndView();
	        mav.setViewName("/business/accounting/pingzhengjizhang/site_list");
	        mav.addAllObjects(viewModel);
	        return mav;
	    }
	
		@RequestMapping("/site_record.shtml")
		 public ModelAndView site_record(NativeWebRequest request, VoucherParameter parameter) {
	        Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	        ModelAndView mav = new ModelAndView();
	        viewModel.put("VoucherList", voucherjzService.jzpzcx(parameter));
	        mav.setViewName("/business/accounting/pingzhengjizhang/site_record");
	        mav.addAllObjects(viewModel);
	        return mav;
	    }
		
		@RequestMapping("/site_record2.shtml")
		 public ModelAndView site_record2(NativeWebRequest request, VoucherParameter parameter) {
	        Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	        ModelAndView mav = new ModelAndView();
	        viewModel.put("VoucherList", voucherjzService.jzpzcx(parameter));
	        mav.setViewName("/business/accounting/pingzhengjizhang/site_record2");
	        mav.addAllObjects(viewModel);
	        return mav;
	    }
		
		@RequestMapping("/site_record3.shtml")
		 public ModelAndView site_record3(NativeWebRequest request, VoucherParameter parameter) {
	        Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	        ModelAndView mav = new ModelAndView();
	        viewModel.put("VoucherList", voucherjzService.jzpzcx(parameter));
	        mav.setViewName("/business/accounting/pingzhengjizhang/site_record3");
	        mav.addAllObjects(viewModel);
	        return mav;
	    }
}