package com.capinfo.accounting.web;


import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Ztsplssj;
import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.ZtsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.ZtsplssjService;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.accumulation.service.accounting.ZtxglssjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
@Controller
@RequestMapping("/business/accounting/ztspHistory")
public class ZtsplssjController extends CommonsDataManageController<ZtsplssjParameter, Ztsplssj>{
	 @Autowired
	  private ZtsplssjService ztsplssjService;
	 @Autowired
	  private ZtxglssjService ztxglssjService;
	 @Autowired
	  private ZtszService ztszService;


	  @Override
	  public CommonsDataOperationService<Ztsplssj, ZtsplssjParameter> getCommonsDataOperationService() {
	      return ztsplssjService;
	  }

	  @RequestMapping({ "/form2.shtml" })
		public final Map<String, Object> form2(NativeWebRequest request, ZtsplssjParameter parameter) {
			Map<String, Object> viewModel = new HashMap<String, Object>();
			viewModel.put("entity", parameter.getEntity());
			return listAfterInvoke(request, parameter, viewModel );
		}
	  
	  
	  @RequestMapping({ "/saveOrupdate2.shtml" })
	  public void saveOrUpdate2(NativeWebRequest request, ZtsplssjParameter parameter, Writer writer) {
		  
		  ZtxglssjParameter xgPrameter=new ZtxglssjParameter();
		  xgPrameter.getEntity().setId(parameter.getEntity().getZtls());
		  Ztxglssj ztxglssj = ztxglssjService.getObjectById(xgPrameter);
	  
	      parameter.getEntity().setCzy(ztxglssj.getCzy());
	      parameter.getEntity().setCzsj(ztxglssj.getCrsj());
	      //初审不通过 = 3
	      parameter.getEntity().setSpzt(SpztEnums.初审不通过.getToken());
	      parameter.getEntity().setSjzt(SjztEnums.停用审批中.getToken());
      
	      Date d = new Date();
	      parameter.getEntity().setSpsj(d);
          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	      parameter.getEntity().setSpr(user.getLogonName());
  
          super.saveOrUpdate(request, parameter, writer);
          ztxglssj.setSpzt(SpztEnums.初审不通过.getToken());
          ztxglssj.setSjzt(SjztEnums.停用审批中.getToken());
          ztxglssjService.saveOrUpdate(ztxglssj);
		  
	  }
	  
	  
	  
	  @Override
     	public void saveOrUpdate(NativeWebRequest request, ZtsplssjParameter parameter, Writer writer) {
		  
		  
				  ZtxglssjParameter xgPrameter=new ZtxglssjParameter();
				  xgPrameter.getEntity().setId(parameter.getEntity().getZtls());
				  Ztxglssj ztxglssj = ztxglssjService.getObjectById(xgPrameter);
			  
			      parameter.getEntity().setCzy(ztxglssj.getCzy());
			      parameter.getEntity().setCzsj(ztxglssj.getCrsj());
			      //初审通过 = 2
			      parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
			      parameter.getEntity().setSjzt(SjztEnums.正常使用.getToken());
		      
			      Date d = new Date();
			      parameter.getEntity().setSpsj(d);
		          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	    	      parameter.getEntity().setSpr(user.getLogonName());
		  
	              super.saveOrUpdate(request, parameter, writer);
              
	              //正在使用=1，新增审批中=2，停用审批中=3，禁用中=4
	              //待审核 = 1,初审通过=2,初审不通过=3
	              
	              ztxglssj.setSpzt(SpztEnums.初审通过.getToken());
	              ztxglssj.setSjzt(SjztEnums.正常使用.getToken());
	              ztxglssjService.saveOrUpdate(ztxglssj);
	              
	              if(parameter.getEntity().getSpzt()== 2){
	            	   ZtszParameter ztszEntity= new ZtszParameter();
	            	   ztszEntity.getEntity().setId(ztxglssj.getZtid());
	            	   Ztsz ztsz= ztszService.getObjectById(ztszEntity);
	            	   ztsz.setSpzt(SpztEnums.初审通过.getToken());
	            	   ztsz.setSjzt(SjztEnums.正常使用.getToken());
	            	   ztsz.setHsdw(ztxglssj.getHsdw());
	            	   ztsz.setZjlx(ztxglssj.getZjlx());
	            	   ztsz.setZzjg(ztxglssj.getZzjg());
	            	   ztsz.setZtdm(ztxglssj.getZtdm());
	            	   ztsz.setZtmc(ztxglssj.getZtmc());
	            	   ztsz.setZtms(ztxglssj.getZtms());
	            	   ztsz.setCrsj(ztxglssj.getCrsj());
	            	   
	            	   ztszService.saveOrUpdate(ztsz);
	            	   parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
	            	   ztsplssjService.saveOrUpdate(parameter);
	            	   
	            	   
	              }
	           
	              
	}

}
