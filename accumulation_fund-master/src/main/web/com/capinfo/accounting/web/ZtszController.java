package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.accumulation.service.accounting.ZtxglssjService;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

/**
 * Created by cairunbin on 2017/10/24.
 */

@Controller
@RequestMapping("/business/accounting/ZTSZ")
public class ZtszController extends CommonsDataManageController<ZtszParameter, Ztsz> {

	@Autowired
	private ZtszService ztszService;

	@Autowired
	private ZtxglssjService ztxglssjService;

	@Override
	public CommonsDataOperationService<Ztsz, ZtszParameter> getCommonsDataOperationService() {
		return ztszService;
	}

	@RequestMapping({ "/search.shtml"})
	public Map<String, Object> search(ZtszParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		Set<Dictionary> hsdwList = ztszService.getDictionariesBySortName("核算单位");
		viewModel.put("hsdwList", hsdwList);
		return viewModel;
	}

	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, ZtszParameter parameter,
			Map<String, Object> viewModel) {
		Set<Dictionary> hsdwList = ztszService.getDictionariesBySortName("核算单位");
		viewModel.put("hsdwList", hsdwList);
		Set<Dictionary> zzjgList = ztszService.getDictionariesBySortName("组织机构");
		viewModel.put("zzjgList", zzjgList);
		Set<Dictionary> zjlxList = ztszService.getDictionariesBySortName("资金类型");
		viewModel.put("zjlxList", zjlxList);

		return super.formAfterInvoke(request, parameter, viewModel);
	}
	
	@RequestMapping("/deletexg.shtml")
	public final void deletexg(NativeWebRequest request, ZtszParameter parameter, Writer writer) {
		
		ControllerUtils.outputByWriter(request, writer, getCommonsDataOperationService().delete(parameter));
		
	}
	
	
}
