package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;
import com.capinfo.accumulation.service.accounting.KmszxgService;
import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;




@Controller
@RequestMapping("/business/accounting/kemushezhilssj")
public class KmszxgController  extends CommonsDataManageController<KmszxgParameter,Kmszxglssj> {
	   private static final Log logger = LogFactory.getLog(Kmszxglssj.class);
		
	   
	   @Autowired
	   private KmszxgService kmszxgService;
	   
	   @Autowired
		  public CommonsDataOperationService<Kmszxglssj, KmszxgParameter> getCommonsDataOperationService() {
		        return kmszxgService;
		    }

	   @RequestMapping("/search1.shtml")
	   public ModelAndView search1(NativeWebRequest request, KmszxgParameter parameter,Long tempid) {
	       Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	       ModelAndView mav = new ModelAndView();
	       parameter.getEntity().setKmszid(tempid);
	       mav.setViewName("/business/accounting/kemushezhilssj/search1");
	       mav.addAllObjects(viewModel);
	       return mav;
	   }
	   @RequestMapping("/site_list.shtml")
	   public Map<String, Object> site_list(NativeWebRequest request, KmszxgParameter parameter) {

			Map<String, Object> viewModel = ControllerUtils.buildViewModel();
			
			if (listBeforeInvoke(request, parameter)) {
				List<Kmszxglssj> entities = new ArrayList<Kmszxglssj>();

				long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
				if (0 != totalCount) {
					
					entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
				}

				viewModel.putAll(new DataListViewModel<Kmszxglssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
			}
			
			return listAfterInvoke(request, parameter, viewModel);
		}

			protected boolean listBeforeInvoke(NativeWebRequest request, KmszxgParameter parameter) {
			return true;
		}

		protected Map<String, Object> listAfterInvoke(NativeWebRequest request, KmszxgParameter parameter, Map<String, Object> viewModel) {
			return viewModel;
		}
		


		  @RequestMapping("/jinyong.shtml")
		public void jinyong(NativeWebRequest request, KmszxgParameter parameter, Writer writer) 
		  {
			 Kmszxglssj kmjy= kmszxgService.getObjectById(parameter);
			  KmszxgParameter km =new KmszxgParameter();
			  	BeanUtils.copyProperties(kmjy, km.getEntity());
			  	km.getEntity().setZt(SjztEnums.停用审批中.getToken());
			  	km.getEntity().setSpzt(SpztEnums.待审批.getToken());
			  	km.getEntity().setId(null);
			  	ControllerUtils.outputByWriter(request, writer, (null != kmszxgService.saveOrUpdate(km)));
		}
		  @RequestMapping("/jinyongjingban.shtml")
		  public void jinyongjingban(NativeWebRequest request, KmszxgParameter parameter, Writer writer) {
				 Kmszxglssj kmjy= kmszxgService.getObjectById(parameter);
				 kmjy.setZt(SjztEnums.禁用中.getToken());
				 kmjy.setSpzt(SpztEnums.初审通过.getToken());
				 parameter.setEntity(kmjy);
				  	super.saveOrUpdate(request, parameter, writer);
			}
		
		
		
		
}
