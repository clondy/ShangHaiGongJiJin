package com.capinfo.accounting.web;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Fzyeb;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.FzyebParameter;
import com.capinfo.accumulation.service.accounting.FzyebService;
import com.capinfo.accumulation.util.ExcelUtil;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

/**
 * 辅助余额账表控制器
 * @author
 *
 */
@Controller
@RequestMapping("/business/accounting/fzyeb")
public class FzyebController extends CommonsDataManageController<FzyebParameter, Fzyeb> {

	@Autowired
	private FzyebService fzyebService;

	@Override
	public CommonsDataOperationService<Fzyeb, FzyebParameter> getCommonsDataOperationService() {
		return fzyebService;
	}

	@RequestMapping({ "/search.shtml" })
	public Map<String, Object> search(FzyebParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		List<Kmszjbsj> list = fzyebService.findAllFzlxhsfz();
		viewModel.put("fzyebList", list);
		return viewModel;
	}
	
	/**
	 * 查询核算类型
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/hslx.shtml" })
	@ResponseBody
	public String hslx(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Fzlxsz> list = fzyebService.findAllhslx();
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(JSON.toJSONString(list));
		return null;
	}
	
	/**
	 * 通过核算类型级联下拉核算项目
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/hsxm.shtml" })
	@ResponseBody
	public String hsxm(HttpServletRequest request,HttpServletResponse response,String id) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Fzhsxmjbsj> list = fzyebService.findAllHsxmmc(id);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(JSON.toJSONString(list));
		return null;
	}
	
	/**
	 * excel导出
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/excels.shtml" })
	@ResponseBody
	public void excels(HttpServletRequest request,HttpServletResponse response,FzyebParameter parameter) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Fzyeb> entities = new ArrayList<Fzyeb>();
		long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
		if (0 != totalCount) {
			entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
		}
		Map beanParams = new HashMap();
		beanParams.put("liste", entities);
		//Excel名称
		String excelName = "辅助余额账表.xls";
		ExcelUtil.createExcel(ExcelUtil.readValueMB() + excelName, beanParams, ExcelUtil.readValueSC() + excelName);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(excelName);
	}
	
}
