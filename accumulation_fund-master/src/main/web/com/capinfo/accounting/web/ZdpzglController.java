package com.capinfo.accounting.web;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.capinfo.accumulation.model.accounting.GB_JZPZXX;
import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.GB_JZPZXXServiceI;




@Controller
@RequestMapping("/business/accounting/zdpzgl")
public class ZdpzglController  {
//	private static Logger log=LoggerFactory.getLogger(ZdpzglController.class);  
	@Autowired
	GB_JZPZXXServiceI gB_JZPZXXService;
	
	@RequestMapping("/search.shtml")
	public String toIndex(HttpServletRequest request){  
		
		
        return "/business/accounting/zdpzgl/search";  
	
	}
	
	@RequestMapping(value="/loadlist")  
	@ResponseBody  
	public void search (HttpServletRequest request,HttpServletResponse response) throws IOException  {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String jzrq=(String) request.getParameter("JZRQ");
		   Map map = new HashMap();
		   map.put("currentPieceNum", 1);
		   map.put("perPieceSize", 10);
		   map.put("sort", "ID");
		   map.put("order", "DESC");
		   
		   if(jzrq!=null){
			   map.put("JZRQ_START_DATE_yyyyMMdd", jzrq);
			   map.put("JZRQ_END_DATE_yyyyMMdd", jzrq);
		   }
		   DataGrid dataGrid=  gB_JZPZXXService.findbyByGrid(map);
		   Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		   gson.toJson(dataGrid);
		   PrintWriter pw;
		   pw = response.getWriter();
		   pw.write(gson.toJson(dataGrid));
	}
	@RequestMapping(value="/hsdw")  
	@ResponseBody  
	public void hsdw (HttpServletRequest request,HttpServletResponse response) throws IOException  {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		 Gson gson = new Gson();
		   Map map = new HashMap();
		   List<Map<String,Object>>hsdw= gB_JZPZXXService.hsdw();
		   PrintWriter pw;
		   pw = response.getWriter();
		   pw.write(gson.toJson(hsdw));
	   }
	
	
	@RequestMapping(value="/cwzt")  
	@ResponseBody  
	public void cwzt (HttpServletRequest request,HttpServletResponse response) throws IOException  {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		 Gson gson = new Gson();
		   Map map = new HashMap();
		   List<Map<String,Object>>cwzt= gB_JZPZXXService.cwzt();
		   PrintWriter pw;
		   pw = response.getWriter();
		   pw.write(gson.toJson(cwzt));
	   }
	
	@RequestMapping(value="/chakan")  
public String chakan(HttpServletRequest request){  
		
		
        return "/business/accounting/zdpzgl/chakan";  
	
	}
	
	@RequestMapping(value="/jizhangpingzheng")  
	public String jizhangpingzheng(HttpServletRequest request){  
			
			
	        return null;  
		
		}
	
	
}
