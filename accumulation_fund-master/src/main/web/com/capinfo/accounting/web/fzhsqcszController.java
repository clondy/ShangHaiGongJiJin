package com.capinfo.accounting.web;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.FUZHU_HSQCSZLSSJServiceI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



@Controller
@RequestMapping("/business/accounting/fzhsqcsz")
public class fzhsqcszController{
	@Autowired
	
     private FUZHU_HSQCSZLSSJServiceI fuzhu_hsqcszlssjServiceI;
	
	
	
	@RequestMapping("search.shtml")
	public String toIndex(HttpServletRequest request){  
		
		
        return "business/accounting/fzhsqcsz/search";  
	
	}
	
	
	@RequestMapping(value="/QClist")      
	@ResponseBody	 
     public String search (HttpServletRequest request,HttpServletResponse response,String kjnd) throws IOException  {
 		response.setHeader("Content-type", "textml;charset=UTF-8");
 		response.setCharacterEncoding("UTF-8");
 		String str= request.getParameter("KJND");
 		   Map map = new HashMap();
 		   map.put("currentPieceNum", 1);
 		   map.put("perPieceSize", 10);
 		   map.put("sort", "ID");
 		   map.put("order", "DESC");
 		   if(str!= null){
 		   map.put("KJND_EQ_NUM",str);
 		   }
 		  
 		   DataGrid dataGrid=  fuzhu_hsqcszlssjServiceI.findbyByGrid(map);
 		   Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
 		   gson.toJson(dataGrid);
 		   PrintWriter pw;
 		   pw = response.getWriter();
 		   pw.write(gson.toJson(dataGrid));
 	       return null;
 	   }
	
	
}