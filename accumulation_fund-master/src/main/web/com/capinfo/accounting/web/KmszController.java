package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.KmszParameter;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.accumulation.service.accounting.KmszService;
import com.capinfo.accumulation.service.accounting.KmszsplssjService;
import com.capinfo.accumulation.service.accounting.KmszxgService;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.accumulation.service.accounting.mybits.GB_JZPZXXServiceI;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;



@Controller
@RequestMapping("/business/accounting/kemushezhi")
public class KmszController extends CommonsDataManageController<KmszParameter, Kmszjbsj> {
	 private static final Log logger = LogFactory.getLog(Kmszjbsj.class);


	  @Autowired
	  private KmszService kmszService;
	
	  @Autowired
	  private KmszxgService kmszxgService;
	  
	  @Autowired
	  private KmszsplssjService kmszsplssjService;
	  
	  @Autowired
	  private GB_JZPZXXServiceI gB_JZPZXXService; 
	  
	  @Autowired
		private ZtszService ztszService;
	  @Autowired
	  public CommonsDataOperationService<Kmszjbsj, KmszParameter> getCommonsDataOperationService() {
	        return kmszService;
	    }

	
	@RequestMapping("/search.shtml")
	public Map<String, Object> search(KmszParameter parameter ) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
        Set<Dictionary> paymentSiteList = kmszService.getDictionariesBySortName("会计年度");
        Set<Dictionary> paymentSiteList1 = kmszService.getDictionariesBySortName("科目属性");
        Set<Dictionary> paymentSiteList2 = kmszService.getDictionariesBySortName("台账标记");
        Set<Dictionary> paymentSiteList3 = kmszService.getDictionariesBySortName("日记账类型");
        Set<Dictionary> paymentSiteList4 = kmszService.getDictionariesBySortName("余额方向");
        viewModel.put("paymentSiteList",paymentSiteList);
        viewModel.put("paymentSiteList1",paymentSiteList1);
        viewModel.put("paymentSiteList2",paymentSiteList2);
        viewModel.put("paymentSiteList3",paymentSiteList3);
        viewModel.put("paymentSiteList4",paymentSiteList4);
		return viewModel;
	}
	/**
	 * 增加非一级科目
	 * @param request
	 * @param parameter
	 * @return
	 */
	@RequestMapping("/site_record.shtml")
	public ModelAndView site_record(NativeWebRequest request, KmszParameter parameter) {
	  Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
      ModelAndView mav = new ModelAndView();
      Set<Dictionary> paymentSiteList   = kmszService.getDictionariesBySortName("余额方向");
      Set<Dictionary> paymentSiteList1 = kmszService.getDictionariesBySortName("科目属性");
      Set<Dictionary> paymentSiteList2 = kmszService.getDictionariesBySortName("台账标记");
      Set<Dictionary> paymentSiteList3 = kmszService.getDictionariesBySortName("日记账类型");
      List<Ztsz> paymentSiteList4 = ztszService.getZtsz();
      Set<Dictionary> paymentSiteList5 = kmszService.getDictionariesBySortName("末级标记");
      Set<Dictionary> paymentSiteList6 = kmszService.getDictionariesBySortName("会计年度");
      viewModel.put("paymentSiteList",paymentSiteList);
      viewModel.put("paymentSiteList1",paymentSiteList1);
      viewModel.put("paymentSiteList2",paymentSiteList2);
      viewModel.put("paymentSiteList3",paymentSiteList3);
      viewModel.put("paymentSiteList4",paymentSiteList4);
      viewModel.put("paymentSiteList5",paymentSiteList5);
      viewModel.put("paymentSiteList6",paymentSiteList6);
	  viewModel.put("KmszjbsjList",  kmszService.dantiaochaxun(parameter));
	  mav.setViewName("/business/accounting/kemushezhi/site_record");
	  mav.addAllObjects(viewModel);
	  return mav;
   }
	/**
	 * 增加一级科目
	 * @param request
	 * @param parameter
	 * @return
	 */
	@RequestMapping("/site_record1.shtml")
	public ModelAndView site_record1(NativeWebRequest request, KmszParameter parameter) {
      Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
      ModelAndView mav = new ModelAndView();
      Set<Dictionary> paymentSiteList   = kmszService.getDictionariesBySortName("余额方向");
      Set<Dictionary> paymentSiteList1 = kmszService.getDictionariesBySortName("科目属性");
      Set<Dictionary> paymentSiteList2 = kmszService.getDictionariesBySortName("台账标记");
      Set<Dictionary> paymentSiteList3 = kmszService.getDictionariesBySortName("日记账类型");
      List<Ztsz> paymentSiteList4 = ztszService.getZtsz();
      Set<Dictionary> paymentSiteList5 = kmszService.getDictionariesBySortName("末级标记");
      Set<Dictionary> paymentSiteList6 = kmszService.getDictionariesBySortName("会计年度");
      viewModel.put("paymentSiteList",paymentSiteList);
      viewModel.put("paymentSiteList1",paymentSiteList1);
      viewModel.put("paymentSiteList2",paymentSiteList2);
      viewModel.put("paymentSiteList3",paymentSiteList3);
      viewModel.put("paymentSiteList4",paymentSiteList4);
      viewModel.put("paymentSiteList5",paymentSiteList5);
      viewModel.put("paymentSiteList6",paymentSiteList6);
      mav.setViewName("/business/accounting/kemushezhi/site_record1");
      mav.addAllObjects(viewModel);
      return mav;
  }
	
	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, KmszParameter parameter,
			Map<String, Object> viewModel) {
		Set<Dictionary> paymentSiteList   = kmszService.getDictionariesBySortName("余额方向");
	      Set<Dictionary> paymentSiteList1 = kmszService.getDictionariesBySortName("科目属性");
	      Set<Dictionary> paymentSiteList2 = kmszService.getDictionariesBySortName("台账标记");
	      Set<Dictionary> paymentSiteList8 = kmszService.getDictionariesBySortName("日记账类型");
	      List<Ztsz> paymentSiteList4 = ztszService.getZtsz();
	      Set<Dictionary> paymentSiteList5 = kmszService.getDictionariesBySortName("末级标记");
	      Set<Dictionary> paymentSiteList6 = kmszService.getDictionariesBySortName("会计年度");
	      viewModel.put("paymentSiteList",paymentSiteList);
	      viewModel.put("paymentSiteList1",paymentSiteList1);
	      viewModel.put("paymentSiteList2",paymentSiteList2);
	      viewModel.put("paymentSiteList8",paymentSiteList8);
	      viewModel.put("paymentSiteList4",paymentSiteList4);
	      viewModel.put("paymentSiteList5",paymentSiteList5);
	      viewModel.put("paymentSiteList6",paymentSiteList6);
	      Map<String, Object> map = super.formAfterInvoke(request, parameter, viewModel);
	      KmszParameter kmszParameter =  (KmszParameter)map.get("command");
	     String kmbh = kmszParameter.getEntity().getKmbh();
	     String pkmbh="";
	     if(kmszParameter.getEntity().getPKMBH()!=null){
	    	  pkmbh = kmszParameter.getEntity().getPKMBH().getKmbh();
	     }
	     kmbh = kmbh.substring(pkmbh.length());
	     kmszParameter.getEntity().setKmbh(kmbh);
		return map;
	}
	
	@Override
	public void saveOrUpdate(NativeWebRequest request, KmszParameter parameter, Writer writer) {
		int zt=parameter.getEntity().getZt();
		Long id=parameter.getLsbid();
		Long ids=parameter.getEntity().getId();
		Date time = new Date();
		SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		String czy=user.getLogonName();
		if(zt==2){
			 KmszxgParameter km=new KmszxgParameter();
			 km.getEntity().setKmszid(ids);
		     km.getEntity().setCzy(czy);
			 BeanUtils.copyProperties(parameter.getEntity(), km.getEntity());
			 km.getEntity().setId(id);
			 km.getEntity().setCrsj(time);
			 kmszxgService.saveOrUpdate(km);
//			super.saveOrUpdate(request, parameter, writer);
		}
	}


	@RequestMapping("/save.shtml")
	public void save(NativeWebRequest request, KmszParameter parameter, Writer writer) {
		    Date time = new Date();
		    parameter.getEntity().setZt(SjztEnums.新增审批中.getToken());
	        parameter.getEntity().setSpzt(SpztEnums.待审批.getToken());
	        parameter.getEntity().setKjkmlb("凭证记账");
		    parameter.getEntity().setJbjg("上海市住房公积金");
		    parameter.getEntity().setCrsj(time);
			parameter.getEntity().setKmbh(parameter.getEntity().getPKMBH().getKmbh()+parameter.getEntity().getKmbh());
			parameter.getEntity().setId(null);
			super.saveOrUpdate(request, parameter, writer);
			Long ids=parameter.getEntity().getId();
			KmszxgParameter km=new KmszxgParameter();
			BeanUtils.copyProperties(parameter.getEntity(), km.getEntity());
			km.getEntity().setKmszid(ids);
			km.getEntity().setId(null);
			SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
			String czy=user.getLogonName();
		    km.getEntity().setCzy(czy);
		    kmszxgService.saveOrUpdate(km);
	}
	@RequestMapping("/save1.shtml")
	public void save1(NativeWebRequest request, KmszParameter parameter, Writer writer) {
		 Date time = new Date();
		 parameter.getEntity().setZt(SjztEnums.新增审批中.getToken());
	     parameter.getEntity().setSpzt(SpztEnums.待审批.getToken());
	     parameter.getEntity().setKjkmlb("凭证记账");
		 parameter.getEntity().setJbjg("上海市住房公积金");
		 parameter.getEntity().setCrsj(time);
	     super.saveOrUpdate(request, parameter, writer);
	     Long ids=parameter.getEntity().getId();
		 KmszxgParameter km=new KmszxgParameter();
		 BeanUtils.copyProperties(parameter.getEntity(), km.getEntity());
		 km.getEntity().setKmszid(ids);
		 km.getEntity().setId(null);
		 SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		 String czy=user.getLogonName();
	     km.getEntity().setCzy(czy);
	     kmszxgService.saveOrUpdate(km);
	}
	@RequestMapping("/yzkmbh.shtml")
	public void yzkmbh(NativeWebRequest request, KmszParameter parameter, Writer writer) {
		ControllerUtils.outputByWriter(request, writer, kmszService.yzkmbh(parameter));
	}


	@Override
	public Map<String, Object> list(NativeWebRequest request, KmszParameter parameter) {
		// TODO Auto-generated method stub
		return super.list(request, parameter);
	}

	}

