package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Fzlxhsfz;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.FzlxszParameter;
import com.capinfo.accumulation.parameter.accounting.FzlxxglssjParameter;
import com.capinfo.accumulation.service.accounting.FzlxhsfzService;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.accumulation.service.accounting.FzlxxglssjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/fuzhuleixingsz")
public class FzlxszController extends CommonsDataManageController<FzlxszParameter, Fzlxsz> {
	@Autowired
	private FzlxszService fzlxszService;
	//在这加载Service ，它会自动的匹配它bean中id名与本bean的set**相同的，并自动装载
	@Autowired
	private FzlxhsfzService fzlxhsfzService;
	@Autowired
	private FzlxxglssjService fzlxxglssjService;
	@Autowired
	public CommonsDataOperationService<Fzlxsz, FzlxszParameter> getCommonsDataOperationService() {
		return fzlxszService;
	}

	/**
	 * 下面是添加的方法
	 */
/*	@Override
	public void saveOrUpdate(NativeWebRequest request, FzlxszParameter parameter, Writer writer) {
		Date date = new Date();
		// 插入默认的状态编码2   1正常使用 2新增审批中 3停用审批中 4停用中
		long mrzt = 2;
		parameter.getEntity().setSjzt(mrzt);
		// 插入当前时间
		parameter.getEntity().setCrsj(date);
		
		super.saveOrUpdate(request, parameter, writer);
		
		
	}*/
	//这个是下拉框验证的方法
	@RequestMapping("/yzhslxbm.shtml")
	public void yzhslxbm(NativeWebRequest request, FzlxszParameter parameter, Writer writer){
		System.out.println(parameter);
		List<Fzlxsz> list = fzlxszService.getList(parameter, 1, 0);
		Map map = new HashMap();
		if(list!= null && list .size() >0){
			ControllerUtils.outputByWriter(request, writer,false);
		}else{
			ControllerUtils.outputByWriter(request, writer,true);
		}
	}
  //插入界面的那个下拉框
	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, FzlxszParameter parameter,
			Map<String, Object> viewModel) {
		//这个是表关联的核算类型分组说明的下拉框
		List<Fzlxhsfz> fzlxhsfzList = fzlxhsfzService.findAllFzlxhsfz();
		viewModel.put("fzlxhsfzList", fzlxhsfzList);
		//这个是核算单位字典那个下拉框
		Set<Dictionary> hsdwList = fzlxszService.getDictionariesBySortName("核算单位");
		viewModel.put("hsdwList", hsdwList);
		return super.formAfterInvoke(request, parameter, viewModel);
	}
	
	//主界面下的核算单位下拉框
	@RequestMapping({ "/search.shtml"})
	public Map<String, Object> search(FzlxszParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		Set<Dictionary> hsdwList = fzlxszService.getDictionariesBySortName("核算单位");
		viewModel.put("hsdwList", hsdwList);
		return viewModel;
	}
	
	
	@Override
	public void saveOrUpdate(NativeWebRequest request, FzlxszParameter parameter, Writer writer) {
		
//		if((parameter.getEntity().getSpzt()) == 1){
//			FzlxxglssjParameter entity=new FzlxxglssjParameter();
//			BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
//			fzlxxglssjService.saveOrUpdate(entity);
//
//			
//		}else if ((parameter.getEntity().getSpzt())==3) {
//			
//			FzlxxglssjParameter entity=new FzlxxglssjParameter();
//			BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity()); 
//			entity.getEntity().setZtdm(parameter.getEntity().getZtdm());
//			entity.getEntity().setId(null);
//			entity.getEntity().setSpzt(SpztEnums.待审批.getToken());
//			entity.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
//			fzlxxglssjService.saveOrUpdate(entity);
//		}else{
			FzlxszParameter fzlxszParameter = new FzlxszParameter();
			
			BeanUtils.copyProperties(parameter.getEntity(), fzlxszParameter.getEntity());
			//正在使用=1，新增审批中=2，停用审批中=3，禁用中=4
			int mrsjzt=2;
			Date d = new Date();
			fzlxszParameter.getEntity().setCrsj(d);
			fzlxszParameter.getEntity().setSjzt(mrsjzt);
			//待审核 = 1,初审通过=2,初审不通过=3
			int spzt=1;
			fzlxszParameter.getEntity().setSpzt(spzt);
			
			fzlxszService.saveOrUpdate(fzlxszParameter);
			
			
			FzlxxglssjParameter entity=new FzlxxglssjParameter();
	    	
	    
	    	SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	    	entity.getEntity().setCzy(user.getLogonName());
			
			BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
			entity.getEntity().setSpzt(fzlxszParameter.getEntity().getSjzt());
			entity.getEntity().setSjzt(fzlxszParameter.getEntity().getSjzt());
			entity.getEntity().setFzlxszid(fzlxszParameter.getEntity().getId());
			entity.getEntity().setCrsj(fzlxszParameter.getEntity().getCrsj());
			entity.getEntity().setId(null);
			fzlxxglssjService.saveOrUpdate(entity);

//		}
		
	}
	 

	
	
	
}