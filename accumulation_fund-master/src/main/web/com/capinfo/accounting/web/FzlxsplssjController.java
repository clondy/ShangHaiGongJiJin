package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Fzlxsplssj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Fzlxxglssj;
import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.FzlxsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.FzlxszParameter;
import com.capinfo.accumulation.parameter.accounting.FzlxxglssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.FzlxsplssjService;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.accumulation.service.accounting.FzlxxglssjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;

@Controller
@RequestMapping("/business/accounting/fuzhuleixingspls")
public class FzlxsplssjController extends CommonsDataManageController<FzlxsplssjParameter, Fzlxsplssj> {

	
	
	@Autowired
	private FzlxsplssjService fzlxsplssjService;
	@Autowired
	private FzlxxglssjService fzlxxglssjService;
	@Autowired
	private FzlxszService fzlxszService;
	
	
	@Autowired
	public CommonsDataOperationService<Fzlxsplssj, FzlxsplssjParameter> getCommonsDataOperationService() {
		return fzlxsplssjService;
	}
	
	 @RequestMapping({ "/form2.shtml" })
		public final Map<String, Object> form2(NativeWebRequest request, FzlxsplssjParameter parameter) {
			Map<String, Object> viewModel = new HashMap<String, Object>();
			viewModel.put("entity", parameter.getEntity());
			return listAfterInvoke(request, parameter, viewModel );
		}
	 
	 
	 
	  
	  @RequestMapping({ "/saveOrupdate2.shtml" })
	  public void saveOrUpdate2(NativeWebRequest request, FzlxsplssjParameter parameter, Writer writer) {
		  
		  FzlxxglssjParameter xgPrameter=new FzlxxglssjParameter();
		  xgPrameter.getEntity().setId(parameter.getEntity().getHsspid());
		  Fzlxxglssj fzlxxglssj = fzlxxglssjService.getObjectById(xgPrameter);
	  
	      parameter.getEntity().setCzy(fzlxxglssj.getCzy());
	      parameter.getEntity().setCzsj(fzlxxglssj.getCrsj());
	      //初审不通过 = 3
	      parameter.getEntity().setSpzt(SpztEnums.初审不通过.getToken());
	      parameter.getEntity().setSjzt(SjztEnums.停用审批中.getToken());
	      
	      Date d = new Date();
	      parameter.getEntity().setSpsj(d);
          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	      parameter.getEntity().setSpr(user.getLogonName());
 
         super.saveOrUpdate(request, parameter, writer);
         fzlxxglssj.setSpzt(SpztEnums.初审不通过.getToken());
         fzlxxglssj.setSjzt(SjztEnums.停用审批中.getToken());
         fzlxxglssjService.saveOrUpdate(fzlxxglssj);
		  
	  }
	  
	  
	  
	  @Override
    	public void saveOrUpdate(NativeWebRequest request, FzlxsplssjParameter parameter, Writer writer) {
		  
		  
				  FzlxxglssjParameter xgPrameter=new FzlxxglssjParameter();
				  xgPrameter.getEntity().setId(parameter.getEntity().getHsspid());
				  Fzlxxglssj fzlxxglssj = fzlxxglssjService.getObjectById(xgPrameter);
			  
			      parameter.getEntity().setCzy(fzlxxglssj.getCzy());
			      parameter.getEntity().setCzsj(fzlxxglssj.getCrsj());
			      //初审通过 = 2
			      parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
			      parameter.getEntity().setSjzt(SjztEnums.正常使用.getToken());
		      
			      Date d = new Date();
			      parameter.getEntity().setSpsj(d);
		          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	    	      parameter.getEntity().setSpr(user.getLogonName());
		  
	              super.saveOrUpdate(request, parameter, writer);
             
	              //正在使用=1，新增审批中=2，停用审批中=3，禁用中=4
	              //待审核 = 1,初审通过=2,初审不通过=3
	              
	              fzlxxglssj.setSpzt(SpztEnums.初审通过.getToken());
	              fzlxxglssj.setSjzt(SjztEnums.正常使用.getToken());
	              fzlxxglssjService.saveOrUpdate(fzlxxglssj);
	              
	              if(parameter.getEntity().getSpzt()== 2){
	            	   FzlxszParameter fzlxszEntity= new FzlxszParameter();
	            	   fzlxszEntity.getEntity().setId(fzlxxglssj.getFzlxszid());
	            	   Fzlxsz fzlxsz= fzlxszService.getObjectById(fzlxszEntity);
	            	   fzlxsz.setSpzt(SpztEnums.初审通过.getToken());
	            	   fzlxsz.setSjzt(SjztEnums.正常使用.getToken());
	            	   
	            	   //核算单位
	            	   fzlxsz.setHsdw(fzlxxglssj.getHsdw());
	            	   fzlxsz.setHslxbm(fzlxxglssj.getHslxbm());
	            	   fzlxsz.setHslxmc(fzlxxglssj.getHslxmc());
	            	   fzlxsz.setHsfzbm(fzlxxglssj.getHsfzbm());
	            	   fzlxsz.setZtbh(fzlxxglssj.getZtbh());
	            	   fzlxsz.setHsfl(fzlxxglssj.getHsfl());
	            	   fzlxsz.setCrsj(fzlxxglssj.getCrsj());
	            	   
	            	   fzlxszService.saveOrUpdate(fzlxsz);
	            	   parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
	            	   fzlxsplssjService.saveOrUpdate(parameter);
	            	   
	            	   
	              }
	           
	              
	}
	
	
	 

}
