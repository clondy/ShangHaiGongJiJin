package com.capinfo.accounting.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Sanlanmxz;
import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.accumulation.parameter.accounting.SanlanmxzParameter;
import com.capinfo.accumulation.parameter.accounting.ZflzParameter;
import com.capinfo.accumulation.service.accounting.SanlanmxzService;
import com.capinfo.accumulation.util.ExcelUtil;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/sanlanmxz")
public class SanlanmxzController extends CommonsDataManageController<SanlanmxzParameter, Sanlanmxz>{

	@Autowired
	private SanlanmxzService sanlanmxzService;
	@Override
	public CommonsDataOperationService<Sanlanmxz, SanlanmxzParameter> getCommonsDataOperationService() {
		return sanlanmxzService;
	}
	@RequestMapping({"/search.shtml"})
	public Map<String, Object> search(SanlanmxzParameter parameter) {
	    Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	    List<Kmszjbsj> kmmcList = sanlanmxzService.findKmmc();
		viewModel.put("kmmcList", kmmcList);
	    return viewModel;
	}
	
	/**
	 * excel导出
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/excels.shtml" })
	@ResponseBody
	public void excels(HttpServletRequest request,HttpServletResponse response,SanlanmxzParameter parameter) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Sanlanmxz> entities = new ArrayList<Sanlanmxz>();
		long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
		if (0 != totalCount) {
			entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
		}
		Map beanParams = new HashMap();
		beanParams .put("liste", entities);
		//Excel名称
		String excelName = "三栏明细账.xls";
		ExcelUtil.createExcel(ExcelUtil.readValueMB() + excelName, beanParams, ExcelUtil.readValueSC() + excelName);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(excelName);
	}
}
