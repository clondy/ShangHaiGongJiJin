package com.capinfo.accounting.web;



import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Fzlxxglssj;
import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.FzlxxglssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.FzlxsplssjService;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.accumulation.service.accounting.FzlxxglssjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;

@Controller
@RequestMapping("/business/accounting/fuzhuleixingxgls")
public class FzlxxglssjController extends CommonsDataManageController<FzlxxglssjParameter, Fzlxxglssj>{
	@Autowired
	private FzlxxglssjService fzlxxglssjService;
	
	@Autowired
	private FzlxszService fzlxszService;
	
	
	@Autowired
	public CommonsDataOperationService<Fzlxxglssj, FzlxxglssjParameter> getCommonsDataOperationService() {
		return fzlxxglssjService;
	}
	
	@RequestMapping("/splx.shtml")
	public ModelAndView splx(NativeWebRequest request, FzlxxglssjParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel();
		if (listBeforeInvoke(request, parameter)) {
			List<Fzlxxglssj> entities = new ArrayList<Fzlxxglssj>();
			long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
			if (0 != totalCount) {
				entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
			}
			viewModel.putAll(new DataListViewModel<Fzlxxglssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
		}
		 ModelAndView mav = new ModelAndView();
		  mav.setViewName("/business/accounting/fuzhuleixingxgls/splx");
	       mav.addAllObjects(viewModel);
	       return mav;
	}
	
	

	 

}
