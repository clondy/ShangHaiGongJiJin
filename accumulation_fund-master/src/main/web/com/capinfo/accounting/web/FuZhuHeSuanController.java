package com.capinfo.accounting.web;


import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.parameter.accounting.FzhsxmjbsjParameter;
import com.capinfo.accumulation.service.accounting.FzhsxglssjService;
import com.capinfo.accumulation.service.accounting.FzhsxmjbsjService;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/fuzhuhesuan")
public class FuZhuHeSuanController extends CommonsDataManageController<FzhsxmjbsjParameter,Fzhsxmjbsj>{
    @Autowired
	private FzhsxmjbsjService fzhsxmjbsjService;
    @Autowired
   	private FzlxszService fzlxszService;
    @Autowired
   	private FzhsxglssjService fzhsxglssjService;

	@Override
	public CommonsDataOperationService<Fzhsxmjbsj, FzhsxmjbsjParameter> getCommonsDataOperationService() {
		return fzhsxmjbsjService;
	}
	@RequestMapping({ "/search.shtml"})
	public Map<String, Object> search(FzhsxmjbsjParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		List< Fzlxsz>  fzlxszList = fzlxszService.getFzlxsz();
		viewModel.put("fzlxszList", fzlxszList);
		return viewModel;
	}
	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, FzhsxmjbsjParameter parameter,
	    Map<String, Object> viewModel) {
		Set<Dictionary> kjndList = fzhsxmjbsjService.getDictionariesBySortName("会计年度");
		viewModel.put("kjndList", kjndList);
		List< Fzlxsz>  fzlxszList = fzlxszService.getFzlxsz();
		viewModel.put("fzlxszList", fzlxszList);
		return super.formAfterInvoke(request, parameter, viewModel);
	}
	@RequestMapping("/deletexg.shtml")
	public final void deletexg(NativeWebRequest request, FzhsxmjbsjParameter parameter, Writer writer) {
		ControllerUtils.outputByWriter(request, writer, getCommonsDataOperationService().delete(parameter));
		
		
	}
}
