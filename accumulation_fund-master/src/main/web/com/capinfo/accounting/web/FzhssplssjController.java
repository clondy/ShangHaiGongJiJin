package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Fzhssplssj;
import com.capinfo.accumulation.model.accounting.Fzhsxglssj;
import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.FzhssplssjParameter;
import com.capinfo.accumulation.parameter.accounting.FzhsxglssjParameter;
import com.capinfo.accumulation.parameter.accounting.FzhsxmjbsjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtsplssjParameter;
import com.capinfo.accumulation.service.accounting.FzhssplssjService;
import com.capinfo.accumulation.service.accounting.FzhsxglssjService;
import com.capinfo.accumulation.service.accounting.FzhsxmjbsjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;




@Controller
@RequestMapping("/business/accounting/fuzhusplssj")
public class FzhssplssjController extends CommonsDataManageController<FzhssplssjParameter, Fzhssplssj>{
	@Autowired
	private FzhssplssjService fzhssplssjService;
	@Autowired
	private FzhsxglssjService fzhsxglssjService;
	@Autowired
	private FzhsxmjbsjService fzhsxmjbsjService;
	@Override
	public CommonsDataOperationService<Fzhssplssj, FzhssplssjParameter> getCommonsDataOperationService() {
		return fzhssplssjService;
	}
	  @RequestMapping({ "/form2.shtml" })
		public final Map<String, Object> form2(NativeWebRequest request, FzhssplssjParameter parameter) {
			Map<String, Object> viewModel = new HashMap<String, Object>();
			viewModel.put("entity", parameter.getEntity());
			return listAfterInvoke(request, parameter, viewModel );
		}
	  @RequestMapping({ "/saveOrupdate2.shtml" })
	  public void saveOrUpdate2(NativeWebRequest request, FzhssplssjParameter parameter, Writer writer) {
		  FzhsxglssjParameter xgpar=new FzhsxglssjParameter();
		  xgpar.getEntity().setId(parameter.getEntity().getHsspid());
		  Fzhsxglssj fzhsxglssj =fzhsxglssjService.getObjectById(xgpar);
		  
		  parameter.getEntity().setCzsj(fzhsxglssj.getCrsj());
		  parameter.getEntity().setSpzt(SpztEnums.初审不通过.getToken());
	      Date d = new Date();
	      parameter.getEntity().setSpsj(d);
          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	      parameter.getEntity().setSpr(user.getLogonName());
          super.saveOrUpdate(request, parameter, writer);
          fzhsxglssj.setSpzt(SpztEnums.初审不通过.getToken());
          fzhsxglssjService.saveOrUpdate(fzhsxglssj);
		  
	  }
	  
	  
	  @Override
	public void saveOrUpdate(NativeWebRequest request, FzhssplssjParameter parameter, Writer writer) {
		  FzhsxglssjParameter xgpar=new FzhsxglssjParameter();
		  xgpar.getEntity().setId(parameter.getEntity().getHsspid());
		  Fzhsxglssj fzhsxglssj =fzhsxglssjService.getObjectById(xgpar);
		  parameter.getEntity().setCzsj(xgpar.getEntity().getCrsj());
		  parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
		  Date d = new Date();
	      parameter.getEntity().setSpsj(d);
          SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
	      parameter.getEntity().setSpr(user.getLogonName());
  
          super.saveOrUpdate(request, parameter, writer);
          fzhsxglssj.setSpzt(SpztEnums.初审通过.getToken());
          fzhsxglssjService.saveOrUpdate(fzhsxglssj);
          
          
          if(parameter.getEntity().getSpzt() == 2 ){
        	   
        	  FzhsxmjbsjParameter entity=new FzhsxmjbsjParameter();
        	  
        	  entity.getEntity().setId(fzhsxglssj.getHsid());
        	  
        	  Fzhsxmjbsj fzhsxmjbsj=fzhsxmjbsjService.getObjectById(entity);
        	  
        	  fzhsxmjbsj.setCrsj(fzhsxglssj.getCrsj());
        	  fzhsxmjbsj.setHslxbm(fzhsxglssj.getHslxbm());
        	  fzhsxmjbsj.setHsxmbm(fzhsxglssj.getHsxmbm());
        	  fzhsxmjbsj.setHsxmmc(fzhsxglssj.getHsxmmc());
        	  fzhsxmjbsj.setKjnd(fzhsxglssj.getKjnd());
        	  fzhsxmjbsj.setSfdjmx(fzhsxglssj.getSfdjmx());
        	  fzhsxmjbsj.setSpzt(SpztEnums.初审通过.getToken());
        	  fzhsxmjbsjService.saveOrUpdate(fzhsxmjbsj);
        	  
        	  
        	  parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
        	  fzhssplssjService.saveOrUpdate(parameter);
        	  
        	  
          }
		  
	}
}
