package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Zhaiyao;
import com.capinfo.accumulation.parameter.accounting.ZhaiyaoParameter;
import com.capinfo.accumulation.service.accounting.ZhaiyaoService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.web.controller.manage.region.RegionManageController;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;

@Controller
@RequestMapping("/business/accounting/zhaiyao")
public class ZhaiyaoController extends CommonsDataManageController<ZhaiyaoParameter, Zhaiyao>{

	

	private static final Log logger = LogFactory.getLog(RegionManageController.class);
	@Autowired
	private ZhaiyaoService zhaiyaoService;
    @Override
    public CommonsDataOperationService<Zhaiyao, ZhaiyaoParameter> getCommonsDataOperationService() {
        return zhaiyaoService;
    }
   
    @Override
    protected Map<String, Object> formAfterInvoke(NativeWebRequest request, ZhaiyaoParameter parameter, Map<String, Object> viewModel) {
        Set<Dictionary> hsdwList = zhaiyaoService.getDictionariesBySortName("核算单位");
        viewModel.put("hsdwList", hsdwList);
        Set<Dictionary> zylxList = zhaiyaoService.getDictionariesBySortName("摘要类型");
        viewModel.put("zylxList", zylxList);
        Set<Dictionary> ztbhList = zhaiyaoService.getDictionariesBySortName("账套编号");
        viewModel.put("ztbhList", ztbhList);
        return super.formAfterInvoke(request, parameter, viewModel);
    }
    
    @RequestMapping({"/search.shtml"})
    public Map<String, Object> search(ZhaiyaoParameter parameter) {

        Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
        Set<Dictionary> hsdwList = zhaiyaoService.getDictionariesBySortName("核算单位");
        viewModel.put("hsdwLists",hsdwList);
        Set<Dictionary> ztbhList = zhaiyaoService.getDictionariesBySortName("账套编号");
        viewModel.put("ztbhLists",ztbhList);
        return viewModel;
    }
    
    @Override
   	@RequestMapping("/save_or_update.shtml")
   	public void saveOrUpdate(NativeWebRequest request, ZhaiyaoParameter parameter, Writer writer) {
   		// TODO Auto-generated method stub

    	Date d = new Date();
    	parameter.getEntity().setCrsj(d);
    	
    	SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
    	parameter.getEntity().setCzr(user.getLogonName());
   	
		super.saveOrUpdate(request, parameter, writer);
   	}

	@Override
	@RequestMapping("/list.shtml")
	public Map<String, Object> list(NativeWebRequest request, ZhaiyaoParameter parameter) {
		// TODO Auto-generated method stub
		return super.list(request, parameter);
	}
	
}
