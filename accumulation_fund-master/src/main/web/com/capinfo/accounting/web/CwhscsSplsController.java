package com.capinfo.accounting.web;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.CwhscsSpls;
import com.capinfo.accumulation.model.accounting.CwhscsXgls;
import com.capinfo.accumulation.parameter.accounting.CwhscsSplsParameter;
import com.capinfo.accumulation.parameter.accounting.CwhscsXglsParameter;
import com.capinfo.accumulation.parameter.accounting.KmszsplssjParameter;
import com.capinfo.accumulation.service.accounting.CwhscsSplsService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;


/**
 * 
 * 财务核算参数设置---审批流水
 */
@Controller
@RequestMapping("/business/accounting/cwhscs_spls")
public class CwhscsSplsController extends CommonsDataManageController<CwhscsSplsParameter,CwhscsSpls> {
	   
	  @RequestMapping("/search1.shtml")
	   public ModelAndView search1(NativeWebRequest request, CwhscsSplsParameter parameter,Long cwhscsxgls) {
	       Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	       ModelAndView mav = new ModelAndView();
	       parameter.getEntity().setCwhscsxgls(cwhscsxgls);
	       mav.setViewName("/business/accounting/cwhscs_spls/search1");
	       mav.addAllObjects(viewModel);
	       return mav;
	   }


	@Autowired
	   private CwhscsSplsService cwhscssplsservice;
	   
	   
	   @Autowired
	   public CommonsDataOperationService<CwhscsSpls, CwhscsSplsParameter> getCommonsDataOperationService() {
		        return cwhscssplsservice;
		    }
	   @RequestMapping("/site_list.shtml")
	   public Map<String, Object> site_list(NativeWebRequest request, CwhscsSplsParameter parameter) {
			Map<String, Object> viewModel = ControllerUtils.buildViewModel();
			
			if (listBeforeInvoke(request, parameter)) {
				List<CwhscsSpls> entities = new ArrayList<CwhscsSpls>();

				long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
				if (0 != totalCount) {
					
					entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
				}

				viewModel.putAll(new DataListViewModel<CwhscsSpls>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
			}
			
			return listAfterInvoke(request, parameter, viewModel);
		}

			protected boolean listBeforeInvoke(NativeWebRequest request, CwhscsSplsParameter parameter) {
			return true;
		}

		protected Map<String, Object> listAfterInvoke(NativeWebRequest request, CwhscsSplsParameter parameter, Map<String, Object> viewModel) {
			return viewModel;
		}
}
