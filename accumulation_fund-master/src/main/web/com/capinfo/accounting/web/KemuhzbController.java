package com.capinfo.accounting.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capinfo.accumulation.model.accounting.Kemuhzb;
import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.accumulation.parameter.accounting.KemuhzbParameter;
import com.capinfo.accumulation.parameter.accounting.ZflzParameter;
import com.capinfo.accumulation.service.accounting.KemuhzbService;
import com.capinfo.accumulation.util.ExcelUtil;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;

@Controller
@RequestMapping("/business/accounting/kemuhzb")
public class KemuhzbController extends CommonsDataManageController<KemuhzbParameter, Kemuhzb> {
	@Autowired
	private KemuhzbService kemuhzbservice;
	
	@Override
	public CommonsDataOperationService<Kemuhzb, KemuhzbParameter> getCommonsDataOperationService() {
		return kemuhzbservice;
	}
	
	/**
	 * excel导出
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping({ "/excels.shtml" })
	@ResponseBody
	public void excels(HttpServletRequest request,HttpServletResponse response,KemuhzbParameter parameter) throws IOException {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		List<Kemuhzb> entities = new ArrayList<Kemuhzb>();
		long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
		if (0 != totalCount) {
			entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
		}
		Map beanParams = new HashMap();
		beanParams .put("liste", entities);
		//Excel名称
		String excelName = "科目汇总表.xls";
		ExcelUtil.createExcel(ExcelUtil.readValueMB() + excelName, beanParams, ExcelUtil.readValueSC() + excelName);
		PrintWriter pw;
		pw = response.getWriter();
		pw.write(excelName);
	}
}
