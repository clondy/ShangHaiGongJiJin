package com.capinfo.accounting.web;




import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.accumulation.service.accounting.ZtxglssjService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;

@Controller
@RequestMapping("/business/accounting/ztxgHistory")
public class ZtxglssjController extends CommonsDataManageController<ZtxglssjParameter, Ztxglssj>  {

	  @Autowired
	  private ZtxglssjService ztxglssjService;
	  @Autowired
	  private ZtszService ztszService;

	  @Override
	  public CommonsDataOperationService<Ztxglssj, ZtxglssjParameter> getCommonsDataOperationService() {
	      return ztxglssjService;
	  }
	  

		
		 @RequestMapping("/splx.shtml")
		public ModelAndView splx(NativeWebRequest request, ZtxglssjParameter parameter) {
			Map<String, Object> viewModel = ControllerUtils.buildViewModel();
			if (listBeforeInvoke(request, parameter)) {
				List<Ztxglssj> entities = new ArrayList<Ztxglssj>();
				long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
				if (0 != totalCount) {
					entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
				}
				viewModel.putAll(new DataListViewModel<Ztxglssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
			}
			 ModelAndView mav = new ModelAndView();
			  mav.setViewName("/business/accounting/ztxgHistory/splx");
		       mav.addAllObjects(viewModel);
		       return mav;
		}
		 
		 
		@RequestMapping("/formxg.shtml")
		public final ModelAndView formxg(NativeWebRequest request, ZtxglssjParameter parameter) {

			if (formBeforeInvoke(request, parameter)) {
				getCommonsDataOperationService().populateParameter(parameter);
			}
			
			Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
			Set<Dictionary> hsdwList = ztxglssjService.getDictionariesBySortName("核算单位");
			viewModel.put("hsdwList", hsdwList);
			Set<Dictionary> zzjgList = ztxglssjService.getDictionariesBySortName("组织机构");
			viewModel.put("zzjgList", zzjgList);
			Set<Dictionary> zjlxList = ztxglssjService.getDictionariesBySortName("资金类型");
			viewModel.put("zjlxList", zjlxList);
			
			
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/business/accounting/ZTSZ/form");
		    mav.addAllObjects(viewModel);
		    return mav;
		}
		
		@Override
		public void saveOrUpdate(NativeWebRequest request, ZtxglssjParameter parameter, Writer writer) {
			
			if((parameter.getEntity().getSpzt()) == 1){
				ZtxglssjParameter entity=new ZtxglssjParameter();
				BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
				ztxglssjService.saveOrUpdate(entity);

				
			}else if ((parameter.getEntity().getSpzt())==3) {
				
				ZtxglssjParameter entity=new ZtxglssjParameter();
				BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
				entity.getEntity().setZtdm(parameter.getEntity().getZtdm());
				entity.getEntity().setId(null);
				entity.getEntity().setSpzt(SpztEnums.待审批.getToken());
				entity.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
				ztxglssjService.saveOrUpdate(entity);
			}else{
				ZtszParameter ztszParameter = new ZtszParameter();
				
				BeanUtils.copyProperties(parameter.getEntity(), ztszParameter.getEntity());
				//正在使用=1，新增审批中=2，停用审批中=3，禁用中=4
				ztszParameter.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
				//待审核 = 1,初审通过=2,初审不通过=3
				ztszParameter.getEntity().setSpzt(SpztEnums.待审批.getToken());
				
				ztszService.saveOrUpdate(ztszParameter);
				
				
				ZtxglssjParameter entity=new ZtxglssjParameter();
		    	Date d = new Date();
		    	parameter.getEntity().setCrsj(d);
		    	
		    	SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		    	parameter.getEntity().setCzy(user.getLogonName());
				
				BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
				entity.getEntity().setSpzt(ztszParameter.getEntity().getSpzt());
				entity.getEntity().setSjzt(ztszParameter.getEntity().getSjzt());
				entity.getEntity().setZtid(ztszParameter.getEntity().getId());
				entity.getEntity().setId(null);
				ztxglssjService.saveOrUpdate(entity);

			}
			
		}
		 
	

		
}
