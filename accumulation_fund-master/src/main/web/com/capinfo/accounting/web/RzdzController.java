package com.capinfo.accounting.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;

/**
 * Created by cairunbin on 2017/10/24.
 */

@Controller
@RequestMapping("/business/accounting/rzdz")
public class RzdzController extends CommonsDataManageController<ZtszParameter, Ztsz> {

	@Autowired
	private ZtszService ztszService;

	@Override
	public CommonsDataOperationService<Ztsz, ZtszParameter> getCommonsDataOperationService() {
		return ztszService;
	}

//	@RequestMapping("/list2.shtml")
//	public Map<String, Object> list(NativeWebRequest request, ZtszParameter parameter) {
//
//		Map<String, Object> viewModel = ControllerUtils.buildViewModel();
//		
//		if (listBeforeInvoke(request, parameter)) {
//			
//			List<Ztsz> entities = new ArrayList<Ztsz>();
//
//			long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
//			if (0 != totalCount) {
//				
//				entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
//			}
//
//			viewModel.putAll(new DataListViewModel<Ztsz>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
//		}
//		
//		return listAfterInvoke(request, parameter, viewModel);
//	}
//
//	protected boolean listBeforeInvoke(NativeWebRequest request, ZtszParameter parameter) {
//		return true;
//	}
//
//	protected Map<String, Object> listAfterInvoke(NativeWebRequest request, ZtszParameter parameter, Map<String, Object> viewModel) {
//		return viewModel;
//	}


}
