package com.capinfo.accounting.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.service.accounting.VoucherjzService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;

@Controller
@RequestMapping("/business/accounting/pingzhengchaxun")
public class VouchercxController extends CommonsDataManageController<VoucherParameter, Voucher> {
	
	  @Autowired
	  private VoucherjzService voucherjzService;
	
	  @Autowired
	  public CommonsDataOperationService<Voucher, VoucherParameter> getCommonsDataOperationService() {
	        return voucherjzService;
	    }
		@RequestMapping("/site_record.shtml")
		 public ModelAndView site_record(NativeWebRequest request, VoucherParameter parameter) {
			 Map<String, Object> viewModel = ControllerUtils.buildViewModel();
				if (listBeforeInvoke(request, parameter)) {
					List<Voucher> entities = new ArrayList<Voucher>();
					long totalCount = voucherjzService.getTotalCount(parameter);
					if (0 != totalCount) {
						entities = voucherjzService.getList(parameter, totalCount, parameter.getCurrentPieceNum());
					}
					viewModel.putAll(new DataListViewModel<Voucher>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
				}
				ModelAndView mav = new ModelAndView();
		        mav.setViewName("/business/accounting/pingzhengchaxun/site_record");
		        mav.addAllObjects(viewModel);
		        return mav;
	    }
		
		
		 @RequestMapping("/pzhz.shtml")
		   public ModelAndView pzhz(VoucherParameter parameter) {
				Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		        ModelAndView mav = new ModelAndView();
		        mav.setViewName("/business/accounting/pingzhenghuizong/search");
		        mav.addAllObjects(viewModel);
		        return mav;
			}
}