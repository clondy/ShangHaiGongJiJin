package com.capinfo.accounting.web;

import org.springframework.beans.factory.annotation.Autowired;

import com.capinfo.accumulation.model.accounting.Fzhsxglssj;
import com.capinfo.accumulation.parameter.accounting.FzhsxglssjParameter;
import com.capinfo.accumulation.service.accounting.FzhsxglssjService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;

public class FzxglssjController extends CommonsDataManageController<FzhsxglssjParameter,Fzhsxglssj>{
	@Autowired
	private FzhsxglssjService fzhsxglssjService;

	@Override
	public CommonsDataOperationService<Fzhsxglssj, FzhsxglssjParameter> getCommonsDataOperationService() {
		return fzhsxglssjService;
	}
}

