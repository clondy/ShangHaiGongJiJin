package com.capinfo.accounting.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;

/**
 * Created by cairunbin on 2017/10/24.
 */

@Controller
@RequestMapping("/business/accounting/zdpzmb")
public class ZdpzmbController extends CommonsDataManageController<ZtszParameter, Ztsz> {

	@Autowired
	private ZtszService ztszService;

	@Override
	public CommonsDataOperationService<Ztsz, ZtszParameter> getCommonsDataOperationService() {
		return ztszService;
	}



}
