package com.capinfo.accounting.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.accumulation.parameter.accounting.CwhscsParameter;
import com.capinfo.framework.web.controller.CommonsDataManageController;

@Controller
@RequestMapping("/business/accounting/zhangdanweihu")
public class ZhangDanWeiHuController extends CommonsDataManageController<CwhscsParameter,Cwhscs>{
	
}