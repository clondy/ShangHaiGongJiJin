package com.capinfo.accounting.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.accumulation.model.accounting.CwhscsXgls;
import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.parameter.accounting.CwhscsParameter;
import com.capinfo.accumulation.parameter.accounting.CwhscsXglsParameter;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.accumulation.service.accounting.CwhscsXglsService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;


/**
 * 
 * 财务核算参数设置——修改流水
 */
@Controller
@RequestMapping("/business/accounting/cwhscs_xgls")
public class CwhscsXglsController extends CommonsDataManageController<CwhscsXglsParameter,CwhscsXgls> {
	
	
	@Autowired
	private CwhscsXglsService cwhscsxglsService;
	
	@Override
	public CommonsDataOperationService<CwhscsXgls, CwhscsXglsParameter> getCommonsDataOperationService() {
		return cwhscsxglsService;
	}


	   @RequestMapping("/search1.shtml")
	   public ModelAndView search1(NativeWebRequest request, CwhscsXglsParameter parameter,Long tempid) {
		   System.out.println(tempid);
	       Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	       ModelAndView mav = new ModelAndView();
	       parameter.getEntity().setCwhscsid(tempid);
	       mav.setViewName("/business/accounting/cwhscs_xgls/search1");
	       mav.addAllObjects(viewModel);
	       return mav;
	   }
	   @RequestMapping("/site_list.shtml")
	   public Map<String, Object> site_list(NativeWebRequest request, CwhscsXglsParameter parameter) {
		   Long id = parameter.getEntity().getId();
			Map<String, Object> viewModel = ControllerUtils.buildViewModel();
			
			if (listBeforeInvoke(request, parameter)) {
				List<CwhscsXgls> entities = new ArrayList<CwhscsXgls>();

				long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
				if (0 != totalCount) {
					
					entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
				}

				viewModel.putAll(new DataListViewModel<CwhscsXgls>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
			}
			
			return listAfterInvoke(request, parameter, viewModel);
		}

			protected boolean listBeforeInvoke(NativeWebRequest request, CwhscsXglsParameter parameter) {
			return true;
		}

		protected Map<String, Object> listAfterInvoke(NativeWebRequest request, CwhscsXglsParameter parameter, Map<String, Object> viewModel) {
			return viewModel;
		}
}
