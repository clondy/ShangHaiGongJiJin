package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.service.accounting.VoucherService;
import com.capinfo.accumulation.service.accounting.VoucherjzService;
import com.capinfo.accumulation.util.ArithUtil;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;




@Controller
@RequestMapping("/business/accounting/pingzhengfuhe")
public class VoucherfhController extends CommonsDataManageController<VoucherParameter, Voucher>  {
	  @Autowired
	  private VoucherService voucherService;
	  @Autowired
	  private VoucherjzService voucherjzService;
	  @Autowired
	  public CommonsDataOperationService<Voucher, VoucherParameter> getCommonsDataOperationService() {
	        return voucherService;
	    }
	  
	   @RequestMapping("/search.shtml")
	   public Map<String, Object> search(VoucherParameter parameter) {
			Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	        Set<Dictionary> pzlbList = voucherService.getDictionariesBySortName("凭证类别");
	        viewModel.put("pzlbList",pzlbList);
			return viewModel;
		}
	  
	    @RequestMapping("/jzpzfh.shtml")
		public void jzpzfh(VoucherParameter parameter,NativeWebRequest request,  Writer writer) {
	    	voucherService.jzpzfh(parameter);
	    	ControllerUtils.outputByWriter(request, writer, "复核成功");
		}

		@Override
		public Map<String, Object> list(NativeWebRequest request, VoucherParameter parameter) {
			Map<String, Object> map = super.list(request, parameter);	
			Map<String, Object> retmap  =  new HashMap<String, Object> ();
			List<Voucher> list =  (List<Voucher>)map.get("VoucherList");
			List<Voucher> list2 =  new ArrayList<>();
			Map<String, Voucher> map22 = new HashMap<String, Voucher>();
			if(!list.isEmpty())
			for(int i = 0 ; i<list.size() ; i++){
				Voucher voucher = list.get(i);
				if(map22.get("pzh_"+voucher.getXspzh())!=null){
					Voucher vouchertmp = map22.get("pzh_"+voucher.getXspzh());
					if(voucher.getDffse()!=0)
					vouchertmp.setDffse(ArithUtil.add(vouchertmp.getDffse(), voucher.getDffse()));
					if(voucher.getJffse()!=0)
					vouchertmp.setJffse(ArithUtil.add(vouchertmp.getJffse(),voucher.getJffse()));
				}else{
					map22.put("pzh_"+voucher.getXspzh(), voucher);
				}
			}
			for (Map.Entry<String, Voucher> entry : map22.entrySet()) {  
			    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  
			    list2.add(entry.getValue());
			}  
			retmap.put("VoucherList", list2);
			retmap.put("DataTotalCount", list2.size());
			return retmap;
		}
	
		 @RequestMapping("/shuju.shtml")
		   public ModelAndView shuju(VoucherParameter parameter) {
				Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		        ModelAndView mav = new ModelAndView();
		        viewModel.put("VoucherList", voucherjzService.jzpzcx(parameter));
		        mav.setViewName("/business/accounting/pingzhengchaxun/shuju");
		        mav.addAllObjects(viewModel);
		        return mav;
			}
	    
}
