package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.parameter.accounting.CwhscsParameter;
import com.capinfo.accumulation.parameter.accounting.CwhscsXglsParameter;
import com.capinfo.accumulation.service.accounting.CwhscsService;
import com.capinfo.accumulation.service.accounting.CwhscsXglsService;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;


/**
 * 
 *  财务核算参数设置
 */
@Controller
@RequestMapping("/business/accounting/cwhscs")
public class CwhscsController extends CommonsDataManageController<CwhscsParameter,Cwhscs> {
	@Autowired
	private CwhscsService cwhscsService;

	@Autowired
	private ZtszService ztszService;

	
	@Autowired
	private CwhscsXglsService cwhscsxglsService;
	
	@Override
	public CommonsDataOperationService<Cwhscs, CwhscsParameter> getCommonsDataOperationService() {
		return cwhscsService;
	}

	@Override
	public Map<String, Object> list(NativeWebRequest request, CwhscsParameter parameter) {
		
		return super.list(request, parameter);
	}
	@RequestMapping("/search.shtml")
	public Map<String, Object> search(CwhscsParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
        Set<Dictionary> paymentSiteList = cwhscsService.getDictionariesBySortName("会计年度");
        List<Ztsz> paymentSiteList7 = ztszService.getZtsz();
	    viewModel.put("paymentSiteList7",paymentSiteList7);
        viewModel.put("paymentSiteList",paymentSiteList);
		return viewModel;
	}
	@RequestMapping("/site_record1.shtml")
	 public ModelAndView site_record1(NativeWebRequest request, CwhscsParameter parameter) {
	      Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	      ModelAndView mav = new ModelAndView();
	      Set<Dictionary> paymentSiteList6 = cwhscsService.getDictionariesBySortName("会计年度");
	      viewModel.put("paymentSiteList6",paymentSiteList6);
	      Set<Dictionary> paymentSiteList5 = cwhscsService.getDictionariesBySortName("核算单位");
	      viewModel.put("paymentSiteList5",paymentSiteList5);
	      List<Ztsz> paymentSiteList7 = ztszService.getZtsz();
	      viewModel.put("paymentSiteList7",paymentSiteList7);
//	      mav.setViewName("/business/accounting/cwhscs/site_record1");
	      mav.addAllObjects(viewModel);
	      return mav;
	  }
	
	@RequestMapping("/save.shtml")
	public void save1(NativeWebRequest request, CwhscsParameter parameter, Writer writer) {
		 Date time = new Date();
		 parameter.getEntity().setCrsj(time);
		 parameter.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
		 Cwhscs entity = cwhscsService.saveOrUpdate(parameter.getEntity());
		 CwhscsXglsParameter cc=new CwhscsXglsParameter();
		 Long cwhscsid = parameter.getEntity().getId();
		 BeanUtils.copyProperties(parameter.getEntity(), cc.getEntity());
		 cc.getEntity().setCwhscsid(cwhscsid);
		 cc.getEntity().setId(null);
		 SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		 String czy=user.getLogonName();
		 cc.getEntity().setCzy(czy);
		 cc.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
		 cwhscsxglsService.saveOrUpdate(cc);
	     ControllerUtils.outputByWriter(request, writer, (null != entity));
	}
	
	
	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, CwhscsParameter parameter,
			Map<String, Object> viewModel) {
		 Set<Dictionary> paymentSiteList6 = cwhscsService.getDictionariesBySortName("会计年度");
	      viewModel.put("paymentSiteList6",paymentSiteList6);
	      Set<Dictionary> paymentSiteList5 = cwhscsService.getDictionariesBySortName("核算单位");
	      viewModel.put("paymentSiteList5",paymentSiteList5);
	      List<Ztsz> paymentSiteList7 = ztszService.getZtsz();
	      viewModel.put("paymentSiteList7",paymentSiteList7);
	      List paymentSiteList8 = new ArrayList();
	      SHIF  shif = new SHIF("是","1");
	      paymentSiteList8.add(shif);
	      SHIF  shif2 = new SHIF("否","0");
	      paymentSiteList8.add(shif2);
	      viewModel.put("paymentSiteList8",paymentSiteList8);
		return super.formAfterInvoke(request, parameter, viewModel);
	}

	@Override
	public void saveOrUpdate(NativeWebRequest request, CwhscsParameter parameter, Writer writer) {
		Long idd= parameter.getEntity().getId();
		Long id = parameter.getLsid();
		Date time = new Date();
		SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		String czy=user.getLogonName();
		CwhscsXglsParameter cx=new CwhscsXglsParameter();
		BeanUtils.copyProperties(parameter.getEntity(), cx.getEntity());
		cx.getEntity().setCwhscsid(idd);
		cx.getEntity().setId(id);
		cx.getEntity().setCrsj(time);
		cx.getEntity().setCzy(czy);
		cwhscsxglsService.saveOrUpdate(cx);
		
		}
	}

	class SHIF{
		private String name;
		private String id;
		
		public SHIF(String name, String id) {
			super();
			this.name = name;
			this.id = id;
		}
		
		
		public SHIF() {
			super();
		}


		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		
		
	}
	
	
