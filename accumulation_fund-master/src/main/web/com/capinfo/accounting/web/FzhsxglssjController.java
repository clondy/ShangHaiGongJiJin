package com.capinfo.accounting.web;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Fzhsxglssj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.parameter.accounting.FzhsxglssjParameter;
import com.capinfo.accumulation.parameter.accounting.FzhsxmjbsjParameter;
import com.capinfo.accumulation.service.accounting.FzhsxglssjService;
import com.capinfo.accumulation.service.accounting.FzhsxmjbsjService;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.framework.model.dictionary.Dictionary;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;


@Controller
@RequestMapping("/business/accounting/fuzhuxglssj")
public class FzhsxglssjController extends CommonsDataManageController<FzhsxglssjParameter, Fzhsxglssj>{
	@Autowired
	private FzhsxglssjService fzhsxglssjService;
	@Autowired
	private FzhsxmjbsjService fzhsxmjbsjService;
	@Autowired
	private FzlxszService fzlxszService;
	@Override
	public CommonsDataOperationService<Fzhsxglssj, FzhsxglssjParameter> getCommonsDataOperationService() {
		return fzhsxglssjService;
	}
	@RequestMapping("/formxg.shtml")
	public final ModelAndView formxg(NativeWebRequest request, FzhsxglssjParameter parameter) {

		if (formBeforeInvoke(request, parameter)) {
			getCommonsDataOperationService().populateParameter(parameter);
		}
		
		Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		Set<Dictionary> kjndList = fzhsxglssjService.getDictionariesBySortName("会计年度");
		viewModel.put("kjndList", kjndList);
		List< Fzlxsz>  fzlxszList = fzlxszService.getFzlxsz();
		viewModel.put("fzlxszList", fzlxszList);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/business/accounting/fuzhuhesuan/form");
	    mav.addAllObjects(viewModel);
	    return mav;
	}
		 
	
	 @RequestMapping("/splx.shtml")
	public ModelAndView splx(NativeWebRequest request,FzhsxglssjParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel();
		if (listBeforeInvoke(request, parameter)) {
			List<Fzhsxglssj> entities = new ArrayList<Fzhsxglssj>();
			long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
			if (0 != totalCount) {
				entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
			}
			viewModel.putAll(new DataListViewModel<Fzhsxglssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
		}
		 ModelAndView mav = new ModelAndView();
		  mav.setViewName("/business/accounting/fuzhusplssj/splx");
	       mav.addAllObjects(viewModel);
	       return mav;
	}
	 
	 @RequestMapping("/spjl.shtml")
	public ModelAndView spjl(NativeWebRequest request,FzhsxglssjParameter parameter) {
		Map<String, Object> viewModel = ControllerUtils.buildViewModel();
		if (listBeforeInvoke(request, parameter)) {
			List<Fzhsxglssj> entities = new ArrayList<Fzhsxglssj>();
			long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
			if (0 != totalCount) {
				entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
			}
			viewModel.putAll(new DataListViewModel<Fzhsxglssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
		}
		 ModelAndView mav = new ModelAndView();
		  mav.setViewName("/business/accounting/fuzhusplssj/spjl");
	       mav.addAllObjects(viewModel);
	       return mav;
	}
	 
	@Override
	public void saveOrUpdate(NativeWebRequest request, FzhsxglssjParameter parameter, Writer writer) {
		
		if(parameter.getEntity().getId() != null){
			
			FzhsxglssjParameter entity=new FzhsxglssjParameter();
			BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
			
			fzhsxglssjService.saveOrUpdate(entity);	
			
		}else{
			
			FzhsxmjbsjParameter fzhsxmjbsjParameter=new FzhsxmjbsjParameter();
			BeanUtils.copyProperties(parameter.getEntity(), fzhsxmjbsjParameter.getEntity());
			Date d = new Date();
			fzhsxmjbsjParameter.getEntity().setCrsj(d);
			fzhsxmjbsjService.saveOrUpdate(fzhsxmjbsjParameter);
			
			FzhsxglssjParameter entity=new FzhsxglssjParameter();
			BeanUtils.copyProperties(parameter.getEntity(), entity.getEntity());
			entity.getEntity().setCrsj(fzhsxmjbsjParameter.getEntity().getCrsj());
			entity.getEntity().setId(null);
			entity.getEntity().setHsid(fzhsxmjbsjParameter.getEntity().getId());
			
			fzhsxglssjService.saveOrUpdate(entity);
			
			
		}
		
	}
	
	
	
}