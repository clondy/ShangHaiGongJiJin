package com.capinfo.accounting.web;

import java.io.Writer;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Kmszsplssj;
import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.model.enums.SpztEnums;
import com.capinfo.accumulation.parameter.accounting.KmszParameter;
import com.capinfo.accumulation.parameter.accounting.KmszsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.accumulation.service.accounting.KmszService;
import com.capinfo.accumulation.service.accounting.KmszsplssjService;
import com.capinfo.accumulation.service.accounting.KmszxgService;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.ControllerUtils;
import com.capinfo.framework.web.view.model.DataListViewModel;

@Controller
@RequestMapping("/business/accounting/kemushezhisplssj")
public class KmszsplssjController extends CommonsDataManageController<KmszsplssjParameter,Kmszsplssj> {

	 private static final Log logger = LogFactory.getLog(Kmszsplssj.class);
	  
	   @Autowired
	   private KmszsplssjService kmszsplssjService;
	   
	   @Autowired
	   private KmszxgService kmszxgService;
	  
	   @Autowired
	   private KmszService kmszService;
	   @Autowired
		  public CommonsDataOperationService<Kmszsplssj, KmszsplssjParameter> getCommonsDataOperationService() {
		        return kmszsplssjService;
		    }
	   @RequestMapping("/search1.shtml")
	   public ModelAndView search1(NativeWebRequest request, KmszsplssjParameter parameter,Long kmszls) {
	       Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
	       ModelAndView mav = new ModelAndView();
	       parameter.getEntity().setKmszls(kmszls);
	       mav.setViewName("/business/accounting/kemushezhisplssj/search1");
	       mav.addAllObjects(viewModel);
	       return mav;
	   }
	   @RequestMapping("/site_list.shtml")
	   public Map<String, Object> site_list(NativeWebRequest request, KmszsplssjParameter parameter) {

			Map<String, Object> viewModel = ControllerUtils.buildViewModel();
			
			if (listBeforeInvoke(request, parameter)) {
				kmszsplssjService.dantiaochaxun(parameter);
				List<Kmszsplssj> entities = new ArrayList<Kmszsplssj>();

				long totalCount = getCommonsDataOperationService().getTotalCount(parameter);
				if (0 != totalCount) {
					
					entities = getCommonsDataOperationService().getList(parameter, totalCount, parameter.getCurrentPieceNum());
				}

				viewModel.putAll(new DataListViewModel<Kmszsplssj>(parameter.getEntityClazz().getSimpleName() + "List", entities, totalCount, getCommonsDataOperationService(), parameter).buildViewModel());
			}
			
			return listAfterInvoke(request, parameter, viewModel);
		}

		protected boolean listBeforeInvoke(NativeWebRequest request, KmszsplssjParameter parameter) {
			return true;
		}

		protected Map<String, Object> listAfterInvoke(NativeWebRequest request, KmszsplssjParameter parameter, Map<String, Object> viewModel) {
			return viewModel;
		}

		
		
		@RequestMapping("/shenpi.shtml")
		public void shenpi(NativeWebRequest request, KmszsplssjParameter parameter, Writer writer) {
			Date time = new Date();
			parameter.getEntity().getSpyj();
			parameter.getEntity().setSpsj(time);
			SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
			String spr=user.getLogonName();
			parameter.getEntity().setSpr(spr);
			parameter.getEntity().setSpzt(SpztEnums.初审通过.getToken());
			parameter.getEntity().setSjzt(SjztEnums.正常使用.getToken());
			Long ids =parameter.getEntity().getKmszls();
			KmszxgParameter parameter2 = new KmszxgParameter();
			parameter2.getEntity().setId(ids);
			Kmszxglssj kmszxg = kmszxgService.getObjectById(parameter2 );
			parameter.getEntity().setCzsj(kmszxg.getCrsj());
			parameter.getEntity().setCzy(kmszxg.getCzy());
			kmszxg.setZt(SjztEnums.正常使用.getToken());
			kmszxg.setSpzt(SpztEnums.初审通过.getToken());
			super.saveOrUpdate(request, parameter, writer);
			kmszxgService.saveOrUpdate(kmszxg);
			KmszParameter parameter3 = new KmszParameter();
			parameter2.setEntity(kmszxg);
			Long idd=parameter2.getEntity().getKmszid();
			parameter3.getEntity().setId(idd);
			BeanUtils.copyProperties(parameter2.getEntity(), parameter3.getEntity(),new String[]{"id"});
			kmszService.saveOrUpdate(parameter3);
		}

		@RequestMapping("/shenpiyijian.shtml")
		 public ModelAndView shenpiyijian(NativeWebRequest request, KmszsplssjParameter parameter) {
		      Map<String, Object> viewModel = ControllerUtils.buildViewModel(parameter);
		      ModelAndView mav = new ModelAndView();
			  mav.setViewName("/business/accounting/kemushezhisplssj/shenpi");
			  mav.addAllObjects(viewModel);
			  return mav;		
			}
		@RequestMapping("/shenpino.shtml")
		public void shenpino(NativeWebRequest request, KmszsplssjParameter parameter, Writer writer) {
			Date time = new Date();
			parameter.getEntity().getSpyj();
			parameter.getEntity().setSpsj(time);
			SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
			String spr=user.getLogonName();
			parameter.getEntity().setSpr(spr);
			parameter.getEntity().setSpzt(SpztEnums.初审不通过.getToken());
			parameter.getEntity().setSjzt(SjztEnums.新增审批中.getToken());
			Long ids =parameter.getEntity().getKmszls();
			KmszxgParameter parameter2 = new KmszxgParameter();
			parameter2.getEntity().setId(ids);
			Kmszxglssj kmszxg = kmszxgService.getObjectById(parameter2 );
			parameter.getEntity().setCzsj(kmszxg.getCrsj());
			parameter.getEntity().setCzy(kmszxg.getCzy());
			kmszxg.setZt(SjztEnums.禁用中.getToken());;
			kmszxg.setSpzt(SpztEnums.初审不通过.getToken());
			super.saveOrUpdate(request, parameter, writer);
			kmszxgService.saveOrUpdate(kmszxg);
			KmszParameter parameter3 = new KmszParameter();
			parameter2.setEntity(kmszxg);
			Long idd=parameter2.getEntity().getKmszid();
			parameter3.getEntity().setId(idd);
			BeanUtils.copyProperties(parameter2.getEntity(), parameter3.getEntity(),new String[]{"id"});
			kmszService.saveOrUpdate(parameter3);
		}
		

}
