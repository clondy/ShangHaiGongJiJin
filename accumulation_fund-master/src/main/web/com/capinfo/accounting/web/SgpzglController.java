package com.capinfo.accounting.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.KmszParameter;
import com.capinfo.accumulation.service.accounting.KmszService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;


@Controller
@RequestMapping("/business/accounting/sgpzgl")
public class SgpzglController extends CommonsDataManageController<KmszParameter, Kmszjbsj>  {
	
	
	
	
	 @Autowired
	  private KmszService kmszService;
	
	  

	  @Autowired
	  public CommonsDataOperationService<Kmszjbsj, KmszParameter> getCommonsDataOperationService() {
	        return kmszService;
	    }

}
