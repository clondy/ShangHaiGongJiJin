package com.capinfo.accounting.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.KjkmqcszServiceI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@RequestMapping("/business/accounting/kjkmqcsz")
public class KjkmqcszController{
	
	@Autowired
	private KjkmqcszServiceI kjkmqcszServiceI;
	
	@RequestMapping("search.shtml")
	public String toIndex(HttpServletRequest request){  
		return "business/accounting/kjkmqcsz/search";  
	}
	
	@RequestMapping(value="/list")      
	@ResponseBody	 
     public String select (HttpServletRequest request,HttpServletResponse response) throws IOException  {
 		response.setHeader("Content-type", "textml;charset=UTF-8");
 		response.setCharacterEncoding("UTF-8");
 		   Map map = new HashMap();
 		   map.put("currentPieceNum", 1);
 		   map.put("perPieceSize", 10);
 		   List<Kmszjbsj> kjkmqcszList = kjkmqcszServiceI.selectBySfmj();
 		  
 		   Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
 		   gson.toJson(kjkmqcszList);
 		   PrintWriter pw;
 		   pw = response.getWriter();
 		   pw.write(gson.toJson(kjkmqcszList));
 	       return null;
 	   }
	 
	@RequestMapping("form.shtml")      
	@ResponseBody	 
    public String selectByid (HttpServletRequest request,HttpServletResponse response) throws IOException  {
 		response.setHeader("Content-type", "textml;charset=UTF-8");
 		response.setCharacterEncoding("UTF-8");
 		String id=request.getParameter("id");
 		Kmszjbsj kmszjbsj = kjkmqcszServiceI.selectByPrimaryKey(id);
 		
// 		kmszjbsj.setBnljdf(kmszjbsj.getBnljdf());
// 		kmszjbsj.setBnljjf(kmszjbsj.getBnljjf());
// 		kmszjbsj.setYe(kmszjbsj.getYe());
// 		kmszjbsj.setNcye(kmszjbsj.getNcye());
// 		
// 		kjkmqcszServiceI.updateYesz(id);
 		 		  
	    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
	    gson.toJson(kmszjbsj);
	    PrintWriter pw;
	    pw = response.getWriter();
	    pw.write(gson.toJson(kmszjbsj));
	    return null; 
    }
	
		
}
