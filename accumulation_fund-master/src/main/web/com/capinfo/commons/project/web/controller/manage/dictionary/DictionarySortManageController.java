package com.capinfo.commons.project.web.controller.manage.dictionary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capinfo.commons.project.model.dictionary.DictionarySort;
import com.capinfo.commons.project.parameter.dictionary.DictionarySortParameter;
import com.capinfo.commons.project.service.dictionary.DictionarySortService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:   字典分类管理</p>
 * @author  xuxianping
 * @version 0.1
 */
@Controller
@RequestMapping(value = "/manage/dictionary/sort")
public class DictionarySortManageController extends CommonsDataManageController<DictionarySortParameter, DictionarySort>{

	@Autowired
	private DictionarySortService dictionarySortManageService;

	@Override
	public CommonsDataOperationService<DictionarySort, DictionarySortParameter> getCommonsDataOperationService() {
		
		return dictionarySortManageService;
	}
	
}
