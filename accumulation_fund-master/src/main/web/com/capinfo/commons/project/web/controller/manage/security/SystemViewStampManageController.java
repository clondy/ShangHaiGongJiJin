package com.capinfo.commons.project.web.controller.manage.security;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.model.security.SystemViewStamp;
import com.capinfo.commons.project.parameter.security.SystemViewStampParameter;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.commons.project.service.security.SystemViewStampService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;

/**
 * <p>
 * Title: SystemViewStampManageController
 * </p>
 * <p>
 * Description: 视图标记管理
 * </p>
 * 
 */
@Controller
@RequestMapping("/manage/systemViewStamp")
public class SystemViewStampManageController extends CommonsDataManageController<SystemViewStampParameter, SystemViewStamp> {

	@Autowired
	private SystemViewStampService systemViewStampService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * <p>描述: 验证角色名是否存在</p>
	 * @param request
	 * @param parameter
	 * @param writer
	 * @author: 张名扬
	 * @update:
	 */
	@RequestMapping("/validator.shtml")
	public void validator(NativeWebRequest request, SystemViewStampParameter parameter, Writer writer) {
		
		IOUtils.outputByWriter(writer, this.systemViewStampService.validatorNameExists(parameter));
	}
	
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, SystemViewStampParameter parameter, Map<String, Object> viewModel) {

		SystemViewStamp systemViewStamp = parameter.getEntity();
		if(null != systemViewStamp && null != systemViewStamp.getId()) {
			List<SystemRole> roleList = new ArrayList<SystemRole>(systemViewStamp.getRoles());
			Long[] roleIds = new Long[roleList.size()];
			for(int i=0; i<roleList.size(); i++){
				roleIds[i] = roleList.get(i).getId();
			}
			parameter.setRoleIds(roleIds);
		}
		viewModel.put("roles", systemRoleService.getAllRoles());
		
		return viewModel;
	}
	
	@Override
	public CommonsDataOperationService<SystemViewStamp, SystemViewStampParameter> getCommonsDataOperationService() {

		return systemViewStampService;
	}
	
	
}
