package com.capinfo.commons.project.web.controller.manage.print;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 2017/9/25.
 */
@Controller
public class PrintController {
    @RequestMapping("/print/index.shtml")
    public String index(HttpServletRequest request,Model model) {
        return "/print/index";
    }
}
