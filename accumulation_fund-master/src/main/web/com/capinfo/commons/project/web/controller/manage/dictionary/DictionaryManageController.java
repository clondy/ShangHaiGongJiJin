package com.capinfo.commons.project.web.controller.manage.dictionary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.parameter.dictionary.DictionaryParameter;
import com.capinfo.commons.project.service.dictionary.DictionaryService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.web.controller.CommonsDataManageController;

/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:   字典管理</p>
 * @author  xuxianping
 * @version 0.1
 */
@Controller
@RequestMapping(value = "/manage/dictionary")
public class DictionaryManageController extends CommonsDataManageController<DictionaryParameter, Dictionary>{

	@Autowired
	private DictionaryService dictionaryManageService;

	
	@Override
	public CommonsDataOperationService<Dictionary, DictionaryParameter> getCommonsDataOperationService() {
		
		return dictionaryManageService;
	}
	
}
