package com.capinfo.commons.project.web.controller.manage.security;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.commons.project.model.security.SystemResource;
import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.parameter.security.SystemResourceParameter;
import com.capinfo.commons.project.service.security.SystemResourceService;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.commons.project.service.security.SystemViewStampService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.web.util.WebRequestUtils;

@Controller
@RequestMapping("/manage/systemResource/")
public class SystemResourceManageController {

	@Resource(name = "securityResourceServiceImpl4EasyUI")
	private SystemResourceService securityResourceService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	@Autowired
	private SystemViewStampService systemViewStampService;
	
	
	@RequestMapping("tree.shtml")
	public Map<String, Object> tree(NativeWebRequest request, SystemResourceParameter parameter) {

		Map<String, Object> viewModel = new HashMap<String, Object>();
		//parameter.getEntity().setId(Constants.KONOWLEDGE_CATALOGS);
		//viewModel.put("treeRootId", parameter.getEntity().getId());
		//viewModel.put("command",parameter);
		return viewModel;
	}
	
	
	@RequestMapping("form.shtml")
	public Map<String, Object> form(NativeWebRequest request, SystemResourceParameter parameter) {

		Map<String, Object> viewModel = new HashMap<String, Object>();
		Long id  = parameter.getEntity().getId();
		if(id != null){
			SystemResource resource = securityResourceService.getResourceById(id);
			Set<SystemRole> roles = resource.getRoles();
			Long roleIds[] = new Long[roles.size()];
			int i = 0;
			for(SystemRole role:roles) {
				roleIds[i] = role.getId();
				i++;
			}
			
			parameter.setRoleIds(roleIds);
			parameter.setEntity(resource);
		}
		
		viewModel.put("command", parameter);
		
		viewModel.put("viewStamps", systemViewStampService.getAllViewStamps());
		viewModel.put("roles", systemRoleService.getAllRoles());
		
		return viewModel;
	}
	
	

	@RequestMapping("saveOrUpdate.shtml")
	public void saveOrUpdate(NativeWebRequest request, SystemResourceParameter parameter, OutputStream stream) throws UnsupportedEncodingException {

		WebRequestUtils.setContentType(request, "application/json;charset=UTF-8");
		
		SystemResource resource = securityResourceService.saveOrUpdate(parameter);
		boolean token = resource == null ? false :true;
		if( token ) {

			JSONObject json = new JSONObject();
			json.put("id", resource.getId());
			json.put("name", resource.getName());
			IOUtils.outputByStream(stream, json.toString().getBytes("UTF-8"));
		} else {

			IOUtils.outputByStream(stream, Boolean.FALSE.toString().getBytes());
		}
	}
	
	@RequestMapping("delete.shtml")
	public void delete(NativeWebRequest request, SystemResourceParameter parameter, Writer writer) {

		boolean token = securityResourceService.delete(parameter);
		
		IOUtils.outputByWriter(writer, token);
	}
	
	@RequestMapping("validate.shtml")
	public void validate(NativeWebRequest request, SystemResourceParameter parameter , Writer writer) {
		
		boolean token = securityResourceService.validateResourceExists(parameter);
		IOUtils.outputByWriter(writer, token);
		
	}
	
	@RequestMapping("move.shtml")
	public void move(NativeWebRequest request, SystemResourceParameter parameter , Writer writer) {
		
		boolean token = securityResourceService.move(parameter);
		IOUtils.outputByWriter(writer, token);
		
	}
	
	@RequestMapping("resource.shtml")
	public void resource(NativeWebRequest request,SystemResourceParameter parameter, @ModelAttribute("eccomm_admin")SystemUser user, OutputStream stream) {

		WebRequestUtils.setCharacterEncodingForUTF8(request);
		WebRequestUtils.setContentTypeForHTML(request);
		
		IOUtils.outputByStream(stream , securityResourceService.retrieveResource(parameter));
	}
	
	@RequestMapping("rebuildResource.shtml")
	public void rebuildResource(NativeWebRequest request,SystemResourceParameter parameter, @ModelAttribute("eccomm_admin") SystemUser user, Writer writer) {
		
		boolean token = securityResourceService.rebuildResourceToDataBase(parameter);
		IOUtils.outputByWriter(writer, token);
	}
	
	@RequestMapping("choose_tree.shtml")
	public void chooseTree() {}
}
