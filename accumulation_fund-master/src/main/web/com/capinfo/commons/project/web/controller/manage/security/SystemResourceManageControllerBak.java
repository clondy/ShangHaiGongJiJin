package com.capinfo.commons.project.web.controller.manage.security;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.commons.project.model.security.SystemResource;
import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.parameter.security.SystemResourceParameter;
import com.capinfo.commons.project.service.security.SystemResourceService;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;
import com.capinfo.framework.web.util.WebRequestUtils;


/**
 * 作成日期： 2013-12-26 
 * <p>功能描述:  资源管理</p>
 * @author  xuxianping
 * @version 0.1
 */
//@Controller
//@RequestMapping("/manage/systemResource/")
public class SystemResourceManageControllerBak extends CommonsDataManageController<SystemResourceParameter, SystemResource>{

	@Autowired
	private SystemResourceService systemResourceService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * <p>描述: 初始化页面</p>
	 * @param request
	 * @param parameter
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("initial.shtml")
	public Map<String, Object> initial(NativeWebRequest request, SystemResourceParameter parameter) {

		Map<String, Object> viewModel = new HashMap<String, Object>();
		
		return viewModel;
	}
	
	/**
	 * <p>描述: form方法回调函数，将所有角色以及资源已配置角色分别放入参数中，以供表单页面使用</p>
	 * @param request
	 * @param parameter
	 * @param viewModel
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	@Override
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, SystemResourceParameter parameter, Map<String, Object> viewModel) {
		SystemResource entity = parameter.getEntity();
		
		if(null != entity.getId()){
			Set<SystemRole> systemRoles = entity.getRoles();
			Long roleIds[] = new Long[systemRoles.size()];
			
			int i = 0; 
			for(SystemRole systemRole: systemRoles){
				roleIds[i++] = systemRole.getId();
			}
			parameter.setRoleIds(roleIds);
		}
		
		viewModel.put("command", parameter);
		
		viewModel.put("roles", systemRoleService.getAllRoles());
		
		return super.formAfterInvoke(request, parameter, viewModel);
	}

	
	/**
	 * <p>描述: 新增或修改资源，返回新增或修改的资源JSON</p>
	 * @param request
	 * @param parameter
	 * @param stream
	 * @throws UnsupportedEncodingException
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("saveOrUpdate.shtml")
	public void saveOrUpdate(NativeWebRequest request, SystemResourceParameter parameter, OutputStream stream) throws UnsupportedEncodingException {

		SystemResource resource = systemResourceService.saveOrUpdate(parameter);
		JSONObject json = new JSONObject();
		json.put("id", resource.getId());
		json.put("name", resource.getName());
		IOUtils.outputByStream(stream , json.toString().getBytes("UTF-8"));
	}
	
	/**
	 * <p>描述: 验证资源访问值使用已存在，已确保访问值的唯一性</p>
	 * @param request
	 * @param parameter
	 * @param writer
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("validate.shtml")
	public void validate(NativeWebRequest request, SystemResourceParameter parameter , Writer writer) {
		
		boolean token = systemResourceService.validateResourceExists(parameter);
		IOUtils.outputByWriter(writer, token);
		
	}
	
	/**
	 * <p>描述: 资源移动位置时调用</p>
	 * @param request
	 * @param parameter
	 * @param writer
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("move.shtml")
	public void move(NativeWebRequest request, SystemResourceParameter parameter , Writer writer) {
		
		boolean token = systemResourceService.move(parameter);
		IOUtils.outputByWriter(writer, token);
	}
	
	/**
	 * <p>描述: 获取资源数据， 返回JSON</p>
	 * @param request
	 * @param parameter
	 * @param user
	 * @param stream
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("resource.shtml")
	public void resource(NativeWebRequest request,SystemResourceParameter parameter, OutputStream stream) {

		WebRequestUtils.setCharacterEncodingForUTF8(request);
		WebRequestUtils.setContentTypeForHTML(request);
		
		IOUtils.outputByStream(stream , systemResourceService.retrieveResource(parameter));
	}
	
	
	/**
	 * <p>描述: 重建资源缓存，将生成的数据存入数据库，使查询资源数据时不用每次都查询数据库以及生产JSON而影响性能</p>
	 * @param request
	 * @param parameter
	 * @param user
	 * @param writer
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("rebuildResource.shtml")
	public void rebuildResource(NativeWebRequest request,SystemResourceParameter parameter, Writer writer) {
		
		boolean token = systemResourceService.rebuildResourceToDataBase(parameter);
		IOUtils.outputByWriter(writer, token);
	}
	
	@Override
	public CommonsDataOperationService<SystemResource, SystemResourceParameter> getCommonsDataOperationService() {
		return systemResourceService;
	}
	
	
}
