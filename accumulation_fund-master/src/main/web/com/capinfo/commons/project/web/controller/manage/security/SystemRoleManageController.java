package com.capinfo.commons.project.web.controller.manage.security;

import java.io.Writer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.parameter.security.SystemRoleParameter;
import com.capinfo.commons.project.parameter.security.SystemUserParameter;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:  角色管理</p>
 * @author  xuxianping
 * @version 0.1
 */
@Controller
@RequestMapping("/manage/systemRole")
public class SystemRoleManageController extends CommonsDataManageController<SystemRoleParameter, SystemRole> {

	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * <p>描述: 验证角色名是否存在</p>
	 * @param request
	 * @param parameter
	 * @param writer
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("/validator.shtml")
	public void validator(NativeWebRequest request, SystemRoleParameter parameter, Writer writer) {
		
		IOUtils.outputByWriter(writer, this.systemRoleService.validatorNameExists(parameter));
	}
	
	@Override	
	public CommonsDataOperationService<SystemRole, SystemRoleParameter> getCommonsDataOperationService() {

		return systemRoleService;
	}
}
