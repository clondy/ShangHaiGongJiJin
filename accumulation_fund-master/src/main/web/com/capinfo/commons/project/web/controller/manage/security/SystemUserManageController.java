package com.capinfo.commons.project.web.controller.manage.security;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.NativeWebRequest;

import com.capinfo.commons.project.Constants;
import com.capinfo.commons.project.config.ApplicationConfigBeanForProject;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.parameter.security.SystemUserParameter;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.commons.project.service.security.SystemUserService;
import com.capinfo.commons.project.utils.RegionUtils;
import com.capinfo.components.security.web.utils.WebRequestUtils;
import com.capinfo.framework.service.CommonsDataOperationService;
import com.capinfo.framework.util.IOUtils;
import com.capinfo.framework.util.TokenSignatureUtils;
import com.capinfo.framework.web.controller.CommonsDataManageController;

@Controller
@RequestMapping("/manage/systemUser")
@SessionAttributes("eccomm_admin")
public class SystemUserManageController extends CommonsDataManageController<SystemUserParameter, SystemUser> {
	
	@Autowired
	private SystemUserService systemUserService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	@Autowired
	private ApplicationConfigBeanForProject applicationConfigBeanForProject;
	
	
	/**
	 * <p>描述: 查询页面</p>
	 * @param parameter
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("/initial.shtml")
	public Map<String,Object> initial(NativeWebRequest request, SystemUserParameter parameter){
		
		Map<String,Object> viewModel = new HashMap<String,Object>();
		
		SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		RegionUtils.initScopeIds(viewModel, user.getRegion());
		
		viewModel.put("command", parameter);

		viewModel.put("regionClassName", applicationConfigBeanForProject.getRegionClassName());
		
		return viewModel;
	}
		
	protected Map<String, Object> formAfterInvoke(NativeWebRequest request, SystemUserParameter parameter, Map<String, Object> viewModel) {

		SystemUser user = parameter.getEntity();
		if(user != null) {
			parameter.setRoleIds(user.getRoleIds());
		}
		viewModel.put("roles", systemRoleService.getAllRoles());
		
		SystemUser user2;
		if ( user.getId() == null ) {
			
			user2 = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		} else {
			user2 = user;
		}

		RegionUtils.initScopeIds(viewModel, user2.getRegion());

		viewModel.put("regionClassName", applicationConfigBeanForProject.getRegionClassName());
		
		return viewModel;
	}
	
	
	protected boolean listBeforeInvoke(NativeWebRequest request, SystemUserParameter parameter) {
		SystemUser user = parameter.getEntity();
		if (user.getRegionId() == null) {
			Long regionId = ((SystemUser)WebRequestUtils.retrieveSecureUser(request)).getRegionId();
			
			user.setRegionId(regionId);
		}
		return true;
	}
	
	@RequestMapping("/validator.shtml")
	public void validator(NativeWebRequest request, SystemUserParameter parameter, Writer writer) {
		
		IOUtils.outputByWriter(writer, this.systemUserService.validatorLogonNameExists(parameter));
	}
	
	//修改密码页面
	@RequestMapping("/modify_password_form.shtml")
	public Map<String, Object> modifyPasswordPage(SystemUserParameter parameter) {
		Map<String, Object> viewModel = new HashMap<String, Object>();
		Long id  = parameter.getId();
		if(id != null) {
			parameter.setEntity(this.systemUserService.getSystemUserById(id));
		}
		viewModel.put("command", parameter);
		return viewModel;
	}
	
	//修改密码
	@RequestMapping("/modify_password.shtml")
	public void modifyPassword(NativeWebRequest request, SystemUserParameter parameter, Writer writer) {
		boolean token = systemUserService.modifyPassword(parameter);
		IOUtils.outputByWriter(writer, token);
	}
	
	//重置密码
	@RequestMapping("/reset_password.shtml")
	public void resetPassword(NativeWebRequest request, SystemUserParameter parameter, Writer writer) {
		
		parameter.setNewPassword(Constants.RESET_PASSWORD);
		parameter.setConfirmPassword(Constants.RESET_PASSWORD);
		boolean token = systemUserService.modifyPassword(parameter);
		IOUtils.outputByWriter(writer, token);
	}
	
	/**
	 * <p>描述: 获得当前用户的凭证条</p>
	 * @param request
	 * @param parameter
	 * @param writer
	 * @author: xuxianping
	 * @update:
	 */
	@RequestMapping("/getToken.shtml")
	public void getToken(NativeWebRequest request, SystemUserParameter parameter, OutputStream stream) throws UnsupportedEncodingException{
		SystemUser user = (SystemUser)WebRequestUtils.retrieveSecureUser(request);
		
		String token = TokenSignatureUtils.encode(user.getLogonName(), user.getPassword(), applicationConfigBeanForProject.getTokenSignatureLifetimeSeconds(), applicationConfigBeanForProject.getTokenSignatureKey());
		
		com.capinfo.framework.web.util.WebRequestUtils.setCharacterEncodingForUTF8(request);
		com.capinfo.framework.web.util.WebRequestUtils.setContentTypeForHTML(request);
		
		IOUtils.outputByStream(stream, token.getBytes("UTF-8"));
	}
	
	public CommonsDataOperationService<SystemUser, SystemUserParameter> getCommonsDataOperationService() {
		
		return systemUserService;
	}
}
