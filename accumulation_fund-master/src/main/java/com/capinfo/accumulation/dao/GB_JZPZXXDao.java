package com.capinfo.accumulation.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;
import com.capinfo.accumulation.model.accounting.GB_JZPZXX;
/**
 *  @author zxl
 */
public interface GB_JZPZXXDao {
    int deleteByPrimaryKey(String id);

    int insert(GB_JZPZXX record);

    int insertSelective(GB_JZPZXX record);

    GB_JZPZXX selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(GB_JZPZXX record);

    int updateByPrimaryKey(GB_JZPZXX record);
	
	List<GB_JZPZXX> findbyByGrid(Map map);

	List<Map<String, Object>> hsdw();
	
	List<Map<String, Object>> cwzt();
}


