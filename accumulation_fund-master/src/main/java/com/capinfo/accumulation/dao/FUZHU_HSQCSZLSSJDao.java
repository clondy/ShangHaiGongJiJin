package com.capinfo.accumulation.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;
import com.capinfo.accumulation.model.accounting.FUZHU_HSQCSZLSSJ;
/**
 *  @author zxl
 */
public interface FUZHU_HSQCSZLSSJDao {
    int deleteByPrimaryKey(String id);

    int insert(FUZHU_HSQCSZLSSJ record);

    int insertSelective(FUZHU_HSQCSZLSSJ record);

    FUZHU_HSQCSZLSSJ selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(FUZHU_HSQCSZLSSJ record);

    int updateByPrimaryKey(FUZHU_HSQCSZLSSJ record);
	
	List<FUZHU_HSQCSZLSSJ> findbyByGrid(Map map);
	
	List aaa(Map map);
	
	List findBySelect(Map map);
}


