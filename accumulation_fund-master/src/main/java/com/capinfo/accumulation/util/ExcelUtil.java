package com.capinfo.accumulation.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Properties;

import net.sf.jxls.transformer.XLSTransformer;

/**
 * Excel生成工具类.  
 * @author
 *
 */
public class ExcelUtil {
	
    /** 
     * 根据模板生成Excel文件. 
     * @param templateFileName 模板文件. 
     * @param list 模板中存放的数据. 
     * @param resultFileName 生成的文件. 
     * @throws UnsupportedEncodingException 
     */  
    public static void createExcel(String templateFileName, Map beanParams, String resultFileName) throws UnsupportedEncodingException{
    	XLSTransformer former = new XLSTransformer();
		try {
			former.transformXLS(templateFileName, beanParams, resultFileName);
		} catch (Exception e) {
			System.err.println("文件未正常生产");
		}
    }
    
    /**
     * 读取模板文件路径
     * @return
     * @throws IOException
     */
    public static String readValueMB() throws IOException {
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("freemarker.properties");
        Properties properties = new Properties();
        properties.load(in);
        String str = properties.getProperty("excelMB");
        str = URLDecoder.decode(str, "UTF-8");
		return str;
	}
    
    /**
     * 读取生成文件路径
     * @return
     * @throws IOException
     */
    public static String readValueSC() throws IOException {
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("freemarker.properties");
        Properties properties = new Properties();
        properties.load(in);
        String str = properties.getProperty("excelSC");
        str = URLDecoder.decode(str, "UTF-8");
		return str;
	}
}  