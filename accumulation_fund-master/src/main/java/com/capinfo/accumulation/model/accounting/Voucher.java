package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.capinfo.framework.model.BaseEntity;

@Entity
@Table(name = "GB_JZPZXX")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_ACCOUNT_BANK_SPECIALT", allocationSize = 1)
public class Voucher implements BaseEntity {

	
	private Long id;
	private String jzpzh;
	private String  zhaiyao;
	private String kmbh;
	private Double jffse;
	private Double dffse;
	private Long fjdjs;
	private Date jzrq;
	private Long sfscmxz;
	private String ywlx;
	private String zjywlx;
	private String kmmc;
	private String zd;
	private Date zdrq;
	private String fh;
	private Date fhrq;
	private String pzzt;
	private String hzpzh;
	private String xspzh;
	private String pzlb;
	private String wtsm;
	private String jz;
	private Date pzrq;
	

	@Basic
    @Column(name = "JZPZH")
	public String getJzpzh() {
		return jzpzh;
	}


	public void setJzpzh(String jzpzh) {
		this.jzpzh = jzpzh;
	}

	@Basic
    @Column(name = "ZHAIYAO")
	public String getZhaiyao() {
		return zhaiyao;
	}


	public void setZhaiyao(String zhaiyao) {
		this.zhaiyao = zhaiyao;
	}

	@Basic
    @Column(name = "KMBH")
	public String getKmbh() {
		return kmbh;
	}


	public void setKmbh(String kmbh) {
		this.kmbh = kmbh;
	}

	@Basic
    @Column(name = "JFFSE")
	public Double getJffse() {
		return jffse;
	}


	public void setJffse(Double jffse) {
		this.jffse = jffse;
	}

	@Basic
    @Column(name = "DFFSE")
	public Double getDffse() {
		return dffse;
	}


	public void setDffse(Double dffse) {
		this.dffse = dffse;
	}

	@Basic
    @Column(name = "FJDJS")
	public Long getFjdjs() {
		return fjdjs;
	}


	public void setFjdjs(Long fjdjs) {
		this.fjdjs = fjdjs;
	}

	@Basic
    @Column(name = "JZRQ")
	public Date getJzrq() {
		return jzrq;
	}


	public void setJzrq(Date jzrq) {
		this.jzrq = jzrq;
	}

	@Basic
    @Column(name = "SFSCMXZ")
	public Long getSfscmxz() {
		return sfscmxz;
	}


	public void setSfscmxz(Long sfscmxz) {
		this.sfscmxz = sfscmxz;
	}

	@Basic
    @Column(name = "YWLX")
	public String getYwlx() {
		return ywlx;
	}


	public void setYwlx(String ywlx) {
		this.ywlx = ywlx;
	}

	@Basic
    @Column(name = "ZJYWLX")
	public String getZjywlx() {
		return zjywlx;
	}


	public void setZjywlx(String zjywlx) {
		this.zjywlx = zjywlx;
	}

	@Basic
    @Column(name = "KMMC")
	public String getKmmc() {
		return kmmc;
	}


	public void setKmmc(String kmmc) {
		this.kmmc = kmmc;
	}

	@Basic
    @Column(name = "ZD")
	public String getZd() {
		return zd;
	}


	public void setZd(String zd) {
		this.zd = zd;
	}

	@Basic
    @Column(name = "ZDRQ")
	public Date getZdrq() {
		return zdrq;
	}


	public void setZdrq(Date zdrq) {
		this.zdrq = zdrq;
	}

	@Basic
    @Column(name = "FH")
	public String getFh() {
		return fh;
	}


	public void setFh(String fh) {
		this.fh = fh;
	}

	@Basic
    @Column(name = "FHRQ")
	public Date getFhrq() {
		return fhrq;
	}


	public void setFhrq(Date fhrq) {
		this.fhrq = fhrq;
	}

	@Basic
    @Column(name = "PZZT")
	public String getPzzt() {
		return pzzt;
	}


	public void setPzzt(String pzzt) {
		this.pzzt = pzzt;
	}

	@Basic
    @Column(name = "HZPZH")
	public String getHzpzh() {
		return hzpzh;
	}


	public void setHzpzh(String hzpzh) {
		this.hzpzh = hzpzh;
	}

	@Basic
    @Column(name = "XSPZH")
	public String getXspzh() {
		return xspzh;
	}


	public void setXspzh(String xspzh) {
		this.xspzh = xspzh;
	}

	@Basic
    @Column(name = "PZLB")
	public String getPzlb() {
		return pzlb;
	}


	public void setPzlb(String pzlb) {
		this.pzlb = pzlb;
	}

	@Basic
    @Column(name = "WTSM")
	public String getWtsm() {
		return wtsm;
	}


	public void setWtsm(String wtsm) {
		this.wtsm = wtsm;
	}

	@Basic
    @Column(name = "JZ")
	public String getJz() {
		return jz;
	}


	public void setJz(String jz) {
		this.jz = jz;
	}

	@Basic
    @Column(name = "PZRQ")
	public Date getPzrq() {
		return pzrq;
	}


	public void setPzrq(Date pzrq) {
		this.pzrq = pzrq;
	}
	

	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
}
