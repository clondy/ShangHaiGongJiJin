package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.HadSiteEntity;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 单位公积金历史
 * @Author: zhangxu
 * @Date 2017/9/22 18:12
 */
@Entity
@Table(name = "COMPANY_ACCOUNT_HISTORY")
@SequenceGenerator(name = "seqCompanyAccountHistory", sequenceName = "SEQ_COMPANY_ACCOUNT_HISTORY", allocationSize = 1)

public class CompanyAccountHistory implements BaseEntity, HadSiteEntity {
    private Long id;
    //单位公积金账户_id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund;

    //单位_id
    private Long companyHistoryId;
    private CompanyHistory companyHistory;

    //单位账号 （贯标）dwzh
    private String companyAccount;
    //单位缴存人数 （贯标） dwjcrs
    private Long companyPaymentNumber;
    //单位账户状态(贯标)
    private String gbdwzhzt;
    //单位账户状态
    //单位账户状态
    private Long companyAccountStateId;
    private Dictionary companyAccountState;
    //单位销户日期 （贯标）dwxhrq
    private Date companyCloseDate;
    //单位账户余额 （贯标） dwzhye
    private BigDecimal companyAccountBalance;
    //个人缴存比例 （贯标） grjcbl
    private Long personPaymentRatio;
    //单位封存人数 （贯标）dwfcrs
    private Long companyDepositNumber;
    //缴至年月 （贯标）jzny
    private Date companyPaymentDate;
    //单位缴存比例 （贯标） dwjcbl
    private Long companyPaymentRatio;
    //单位销户原因
    private Long companyCloseReasonId;
    private Dictionary companyCloseReason;
    //单位销户原因（贯标）
    private String gbCompanyCloseReason;
    //启缴年月
    private Date startPaymentDate = new Date();
    //缴交日
    private String paymentDate;
    //缴存率标志
    private Long depositRateMarkId;
    private Dictionary depositRateMark;
    //缴存精度
    private String paymentPrecision;
    //备注
    private String notes;
    //公积金账户类型
    private Long accumulationFundAccountTypeId;
    private Dictionary accumulationFundAccountType;
    //是否委托扣款
    private Boolean delegationPaymentId = true;
    //上年末余额
    private BigDecimal preTotalAmount;
    //累计利息
    private BigDecimal totalInterest;
    //当年利息
    private BigDecimal interest;
    //上月的最后汇缴月份
    private Date preMonthPayMonth;
    //当前的最后汇缴月份
    private Date lastPayMonth;
    //开户日期
    private Date openDate = new Date();
    //开始汇交日期
    private Date startPayMonth ;
    //发生时间
    private Date dateTime = new Date();
    //银行编码
    private String bankCode;
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime;
    //渠道
    private Long channels;
    //创建时间
    private String createName;

    //单位补充公积金账户历史ID
    private Long companyAccountFundHistoryId;

    //单位名称
    private String unitName;

    @Column(name = "UnitName")
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Column(name = "COMPANYACCOUNTFUNDHISTORYID")
    public Long getCompanyAccountFundHistoryId() {
        return companyAccountFundHistoryId;
    }

    public void setCompanyAccountFundHistoryId(Long companyAccountFundHistoryId) {
        this.companyAccountFundHistoryId = companyAccountFundHistoryId;
    }

    public CompanyAccountHistory(Long id) {
        this.id = id;
    }

    public CompanyAccountHistory() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCompanyAccountHistory")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @ManyToOne(targetEntity = com.capinfo.accumulation.model.collection.CompanyAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public com.capinfo.accumulation.model.collection.CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @ManyToOne(targetEntity = CompanyHistory.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_HISTORY_ID", insertable = false, updatable = false)
    public CompanyHistory getCompanyHistory() {
        return companyHistory;
    }

    public void setCompanyHistory(CompanyHistory companyHistory) {
        this.companyHistory = companyHistory;
    }


    @Basic
    @Column(name = "COMPANY_HISTORY_ID")
    public Long getCompanyHistoryId() {
        return companyHistoryId;
    }

    public void setCompanyHistoryId(Long companyHistoryId) {
        this.companyHistoryId = companyHistoryId;
    }


    @Basic
    @Column(name = "DWZH")

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    @Basic
    @Column(name = "DWJCRS")

    public Long getCompanyPaymentNumber() {
        return companyPaymentNumber;
    }

    public void setCompanyPaymentNumber(Long companyPaymentNumber) {
        this.companyPaymentNumber = companyPaymentNumber;
    }


    @Basic
    @Column(name = "DWZHZT")

    public String getGbdwzhzt() {
        return gbdwzhzt;
    }

    public void setGbdwzhzt(String gbdwzhzt) {
        this.gbdwzhzt = gbdwzhzt;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_STATE")

    public Long getCompanyAccountStateId() {
        return companyAccountStateId;
    }

    public void setCompanyAccountStateId(Long companyAccountStateId) {
        this.companyAccountStateId = companyAccountStateId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_STATE", insertable = false, updatable = false)

    public Dictionary getCompanyAccountState() {
        return companyAccountState;
    }

    public void setCompanyAccountState(Dictionary companyAccountState) {
        this.companyAccountState = companyAccountState;
    }




    @Basic
    @Column(name = "DWXHRQ")
    public Date getCompanyCloseDate() {
        return companyCloseDate;
    }

    public void setCompanyCloseDate(Date companyCloseDate) {
        this.companyCloseDate = companyCloseDate;
    }

    @Basic
    @Column(name = "DWZHYE")
    public BigDecimal getCompanyAccountBalance() {
        return companyAccountBalance;
    }

    public void setCompanyAccountBalance(BigDecimal companyAccountBalance) {
        this.companyAccountBalance = companyAccountBalance;
    }

    @Basic
    @Column(name = "GRJCBL")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    @Basic
    @Column(name = "DWFCRS")
    public Long getCompanyDepositNumber() {
        return companyDepositNumber;
    }

    public void setCompanyDepositNumber(Long companyDepositNumber) {
        this.companyDepositNumber = companyDepositNumber;
    }

    @Basic
    @Column(name = "JZNY")
    public Date getCompanyPaymentDate() {
        return companyPaymentDate;
    }

    public void setCompanyPaymentDate(Date companyPaymentDate) {
        this.companyPaymentDate = companyPaymentDate;
    }

    @Basic
    @Column(name = "DWJCBL")
    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "COMPANY_CLOSE_REASON")
    public Long getCompanyCloseReasonId() {
        return companyCloseReasonId;
    }

    public void setCompanyCloseReasonId(Long companyCloseReasonId) {
        this.companyCloseReasonId = companyCloseReasonId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_CLOSE_REASON", insertable = false, updatable = false)
    public Dictionary getCompanyCloseReason() {
        return companyCloseReason;
    }

    public void setCompanyCloseReason(Dictionary companyCloseReason) {
        this.companyCloseReason = companyCloseReason;
    }

    @Basic
    @Column(name = "DWXHYY")
    public String getGbCompanyCloseReason() {
        return gbCompanyCloseReason;
    }

    public void setGbCompanyCloseReason(String gbCompanyCloseReason) {
        this.gbCompanyCloseReason = gbCompanyCloseReason;
    }

    @Basic
    @Column(name = "START_PAYMENT_DATE")
    public Date getStartPaymentDate() {
        return startPaymentDate;
    }

    public void setStartPaymentDate(Date startPaymentDate) {
        this.startPaymentDate = startPaymentDate;
    }

    @Basic
    @Column(name = "PAYMENT_DATE")
    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "DEPOSIT_RATE_MARK")

    public Long getDepositRateMarkId() {
        return depositRateMarkId;
    }

    public void setDepositRateMarkId(Long depositRateMarkId) {
        this.depositRateMarkId = depositRateMarkId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "DEPOSIT_RATE_MARK", insertable = false, updatable = false)


    public Dictionary getDepositRateMark() {
        return depositRateMark;
    }

    public void setDepositRateMark(Dictionary depositRateMark) {
        this.depositRateMark = depositRateMark;
    }

    @Basic
    @Column(name = "PAYMENT_PRECISION")
    public String getPaymentPrecision() {
        return paymentPrecision;
    }

    public void setPaymentPrecision(String paymentPrecision) {
        this.paymentPrecision = paymentPrecision;
    }

    @Basic
    @Column(name = "NOTES")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "ACCUMULATION_FUND_ACCOUNT_TYPE")

    public Long getAccumulationFundAccountTypeId() {
        return accumulationFundAccountTypeId;
    }

    public void setAccumulationFundAccountTypeId(Long accumulationFundAccountTypeId) {
        this.accumulationFundAccountTypeId = accumulationFundAccountTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ACCUMULATION_FUND_ACCOUNT_TYPE", insertable = false, updatable = false)

    public Dictionary getAccumulationFundAccountType() {
        return accumulationFundAccountType;
    }

    public void setAccumulationFundAccountType(Dictionary accumulationFundAccountType) {
        this.accumulationFundAccountType = accumulationFundAccountType;
    }

    @Basic
    @Column(name = "IS_DELEGATION_PAYMENT")
    public Boolean getDelegationPaymentId() {
        return delegationPaymentId;
    }

    public void setDelegationPaymentId(Boolean delegationPaymentId) {
        this.delegationPaymentId = delegationPaymentId;
    }
    
    @Basic
    @Column(name = "PRE_TOTAL_AMOUNT")
    public BigDecimal getPreTotalAmount() {
        return preTotalAmount;
    }

    public void setPreTotalAmount(BigDecimal preTotalAmount) {
        this.preTotalAmount = preTotalAmount;
    }

    @Basic
    @Column(name = "TOTAL_INTEREST")
    public BigDecimal getTotalInterest() {
        return totalInterest;
    }

    public void setTotalInterest(BigDecimal totalInterest) {
        this.totalInterest = totalInterest;
    }


    @Basic
    @Column(name = "INTEREST")
    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }


    @Basic
    @Column(name = "PRE_MONTH_PAY_MONTH")
    public Date getPreMonthPayMonth() {
        return preMonthPayMonth;
    }

    public void setPreMonthPayMonth(Date preMonthPayMonth) {
        this.preMonthPayMonth = preMonthPayMonth;
    }

    @Basic
    @Column(name = "LAST_PAY_MONTH")
    public Date getLastPayMonth() {
        return lastPayMonth;
    }

    public void setLastPayMonth(Date lastPayMonth) {
        this.lastPayMonth = lastPayMonth;
    }


    @Basic
    @Column(name = "OPEN_DATE")
    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    @Basic
    @Column(name = "START_PAY_MONTH")
    public Date getStartPayMonth() {
        return startPayMonth;
    }

    public void setStartPayMonth(Date startPayMonth) {
        this.startPayMonth = startPayMonth;
    }


    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }


    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }


    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof CompanyAccountHistory))
            return false;

        CompanyAccountHistory castOther = (CompanyAccountHistory) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyId", getCompanyHistoryId())
                .append("companyAccountState", getCompanyAccountState()).toString();
    }

}
