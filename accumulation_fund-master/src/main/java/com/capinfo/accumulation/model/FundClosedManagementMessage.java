package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import javax.persistence.*;

import java.util.Date;

/**
 * Comment 资金封闭管理信息
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "FUND_CLOSED_MANAGEMENT_MESSAGE")
@SequenceGenerator(name = "seqFUND_CLOSED_MANAGEMENT_MESSAGE", sequenceName = "SEQ_FUND_CLOSED_MANAGEMENT_MESSAGE", allocationSize = 1)
public class FundClosedManagementMessage implements BaseEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long zjjgzhqcye;
    private String xmbh;
    private Date fsrq;
    private Long jffse;
    private String jydszhhm;
    private String ywlsh;
    private String zjlrly;
    private String zfyt;
    private String qmyefx;
    private String zjjgzhhm;
    private Long zjjgzhqmye;
    private String jydszhyhdm;
    private Long dffse;
    private String jydszhmc;
    private String qcyefx;


    public FundClosedManagementMessage() {
    }

    public FundClosedManagementMessage(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqFUND_CLOSED_MANAGEMENT_MESSAGE")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ZJJGZHQCYE")
    public Long getZjjgzhqcye() {
        return zjjgzhqcye;
    }

    public void setZjjgzhqcye(Long zjjgzhqcye) {
        this.zjjgzhqcye = zjjgzhqcye;
    }

    @Basic
    @Column(name = "XMBH")
    public String getXmbh() {
        return xmbh;
    }

    public void setXmbh(String xmbh) {
        this.xmbh = xmbh;
    }

    @Basic
    @Column(name = "FSRQ")
    public Date getFsrq() {
        return fsrq;
    }

    public void setFsrq(Date fsrq) {
        this.fsrq = fsrq;
    }

    @Basic
    @Column(name = "JFFSE")
    public Long getJffse() {
        return jffse;
    }

    public void setJffse(Long jffse) {
        this.jffse = jffse;
    }

    @Basic
    @Column(name = "JYDSZHHM")
    public String getJydszhhm() {
        return jydszhhm;
    }

    public void setJydszhhm(String jydszhhm) {
        this.jydszhhm = jydszhhm;
    }

    @Basic
    @Column(name = "YWLSH")
    public String getYwlsh() {
        return ywlsh;
    }

    public void setYwlsh(String ywlsh) {
        this.ywlsh = ywlsh;
    }

    @Basic
    @Column(name = "ZJLRLY")
    public String getZjlrly() {
        return zjlrly;
    }

    public void setZjlrly(String zjlrly) {
        this.zjlrly = zjlrly;
    }

    @Basic
    @Column(name = "ZFYT")
    public String getZfyt() {
        return zfyt;
    }

    public void setZfyt(String zfyt) {
        this.zfyt = zfyt;
    }

    @Basic
    @Column(name = "QMYEFX")
    public String getQmyefx() {
        return qmyefx;
    }

    public void setQmyefx(String qmyefx) {
        this.qmyefx = qmyefx;
    }

    @Basic
    @Column(name = "ZJJGZHHM")
    public String getZjjgzhhm() {
        return zjjgzhhm;
    }

    public void setZjjgzhhm(String zjjgzhhm) {
        this.zjjgzhhm = zjjgzhhm;
    }

    @Basic
    @Column(name = "ZJJGZHQMYE")
    public Long getZjjgzhqmye() {
        return zjjgzhqmye;
    }

    public void setZjjgzhqmye(Long zjjgzhqmye) {
        this.zjjgzhqmye = zjjgzhqmye;
    }

    @Basic
    @Column(name = "JYDSZHYHDM")
    public String getJydszhyhdm() {
        return jydszhyhdm;
    }

    public void setJydszhyhdm(String jydszhyhdm) {
        this.jydszhyhdm = jydszhyhdm;
    }

    @Basic
    @Column(name = "DFFSE")
    public Long getDffse() {
        return dffse;
    }

    public void setDffse(Long dffse) {
        this.dffse = dffse;
    }

    @Basic
    @Column(name = "JYDSZHMC")
    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    @Basic
    @Column(name = "QCYEFX")
    public String getQcyefx() {
        return qcyefx;
    }

    public void setQcyefx(String qcyefx) {
        this.qcyefx = qcyefx;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof FundClosedManagementMessage))
            return false;
        FundClosedManagementMessage castOther = (FundClosedManagementMessage)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
