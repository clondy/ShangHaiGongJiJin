package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.sql.Time;

/**
 * 财务银行存款日记账（贯标）未涉及业务
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "FINANCIAL_CASH_IN_BANK_JOURNAL")
public class FinancialCashInBankJournal {
    private long id;
    private String yhzhhm;
    private Time jzrq;
    private long jffse;
    private Time pzssny;
    private long yue;
    private String zjywlx;
    private String jzpzh;
    private String cnlsh;
    private String yejdfx;
    private Time rzrq;
    private long dffse;
    private String zhaiyao;
    private String rzzt;
    private String czbz;
    private String yhjslsh;

    public FinancialCashInBankJournal(long id) {
        this.id = id;
    }

    public FinancialCashInBankJournal() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "YHZHHM")
    public String getYhzhhm() {
        return yhzhhm;
    }

    public void setYhzhhm(String yhzhhm) {
        this.yhzhhm = yhzhhm;
    }

    @Basic
    @Column(name = "JZRQ")
    public Time getJzrq() {
        return jzrq;
    }

    public void setJzrq(Time jzrq) {
        this.jzrq = jzrq;
    }

    @Basic
    @Column(name = "JFFSE")
    public long getJffse() {
        return jffse;
    }

    public void setJffse(long jffse) {
        this.jffse = jffse;
    }

    @Basic
    @Column(name = "PZSSNY")
    public Time getPzssny() {
        return pzssny;
    }

    public void setPzssny(Time pzssny) {
        this.pzssny = pzssny;
    }

    @Basic
    @Column(name = "YUE")
    public long getYue() {
        return yue;
    }

    public void setYue(long yue) {
        this.yue = yue;
    }

    @Basic
    @Column(name = "ZJYWLX")
    public String getZjywlx() {
        return zjywlx;
    }

    public void setZjywlx(String zjywlx) {
        this.zjywlx = zjywlx;
    }

    @Basic
    @Column(name = "JZPZH")
    public String getJzpzh() {
        return jzpzh;
    }

    public void setJzpzh(String jzpzh) {
        this.jzpzh = jzpzh;
    }

    @Basic
    @Column(name = "CNLSH")
    public String getCnlsh() {
        return cnlsh;
    }

    public void setCnlsh(String cnlsh) {
        this.cnlsh = cnlsh;
    }

    @Basic
    @Column(name = "YEJDFX")
    public String getYejdfx() {
        return yejdfx;
    }

    public void setYejdfx(String yejdfx) {
        this.yejdfx = yejdfx;
    }

    @Basic
    @Column(name = "RZRQ")
    public Time getRzrq() {
        return rzrq;
    }

    public void setRzrq(Time rzrq) {
        this.rzrq = rzrq;
    }

    @Basic
    @Column(name = "DFFSE")
    public long getDffse() {
        return dffse;
    }

    public void setDffse(long dffse) {
        this.dffse = dffse;
    }

    @Basic
    @Column(name = "ZHAIYAO")
    public String getZhaiyao() {
        return zhaiyao;
    }

    public void setZhaiyao(String zhaiyao) {
        this.zhaiyao = zhaiyao;
    }

    @Basic
    @Column(name = "RZZT")
    public String getRzzt() {
        return rzzt;
    }

    public void setRzzt(String rzzt) {
        this.rzzt = rzzt;
    }

    @Basic
    @Column(name = "CZBZ")
    public String getCzbz() {
        return czbz;
    }

    public void setCzbz(String czbz) {
        this.czbz = czbz;
    }

    @Basic
    @Column(name = "YHJSLSH")
    public String getYhjslsh() {
        return yhjslsh;
    }

    public void setYhjslsh(String yhjslsh) {
        this.yhjslsh = yhjslsh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialCashInBankJournal that = (FinancialCashInBankJournal) o;

        if (id != that.id) return false;
        if (jffse != that.jffse) return false;
        if (yue != that.yue) return false;
        if (dffse != that.dffse) return false;
        if (yhzhhm != null ? !yhzhhm.equals(that.yhzhhm) : that.yhzhhm != null) return false;
        if (jzrq != null ? !jzrq.equals(that.jzrq) : that.jzrq != null) return false;
        if (pzssny != null ? !pzssny.equals(that.pzssny) : that.pzssny != null) return false;
        if (zjywlx != null ? !zjywlx.equals(that.zjywlx) : that.zjywlx != null) return false;
        if (jzpzh != null ? !jzpzh.equals(that.jzpzh) : that.jzpzh != null) return false;
        if (cnlsh != null ? !cnlsh.equals(that.cnlsh) : that.cnlsh != null) return false;
        if (yejdfx != null ? !yejdfx.equals(that.yejdfx) : that.yejdfx != null) return false;
        if (rzrq != null ? !rzrq.equals(that.rzrq) : that.rzrq != null) return false;
        if (zhaiyao != null ? !zhaiyao.equals(that.zhaiyao) : that.zhaiyao != null) return false;
        if (rzzt != null ? !rzzt.equals(that.rzzt) : that.rzzt != null) return false;
        if (czbz != null ? !czbz.equals(that.czbz) : that.czbz != null) return false;
        if (yhjslsh != null ? !yhjslsh.equals(that.yhjslsh) : that.yhjslsh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (yhzhhm != null ? yhzhhm.hashCode() : 0);
        result = 31 * result + (jzrq != null ? jzrq.hashCode() : 0);
        result = 31 * result + (int) (jffse ^ (jffse >>> 32));
        result = 31 * result + (pzssny != null ? pzssny.hashCode() : 0);
        result = 31 * result + (int) (yue ^ (yue >>> 32));
        result = 31 * result + (zjywlx != null ? zjywlx.hashCode() : 0);
        result = 31 * result + (jzpzh != null ? jzpzh.hashCode() : 0);
        result = 31 * result + (cnlsh != null ? cnlsh.hashCode() : 0);
        result = 31 * result + (yejdfx != null ? yejdfx.hashCode() : 0);
        result = 31 * result + (rzrq != null ? rzrq.hashCode() : 0);
        result = 31 * result + (int) (dffse ^ (dffse >>> 32));
        result = 31 * result + (zhaiyao != null ? zhaiyao.hashCode() : 0);
        result = 31 * result + (rzzt != null ? rzzt.hashCode() : 0);
        result = 31 * result + (czbz != null ? czbz.hashCode() : 0);
        result = 31 * result + (yhjslsh != null ? yhjslsh.hashCode() : 0);
        return result;
    }
}
