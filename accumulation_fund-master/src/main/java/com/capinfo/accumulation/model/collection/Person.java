package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.*;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 个人信息
 * </p>
 */
@Entity
@Table(name = "PERSON")
@SequenceGenerator(name = "seqPerson", sequenceName = "SEQ_PERSON", allocationSize = 1)
public class Person implements BaseEntity{
    private static final long serialVersionUID = 1L;
    private Long id;
    //单位_id
    private Long companyId;
    //(贯标)个人账号 grzh
    private String personalAccount;
    //(贯标)性别 xingbie
    private String gbSex;
    //(贯标)姓名全拼 xmqp
    private String nameAllPinYin;
    //姓名 xingming
    private String name;
    //性别
    private Long sexId;
    private Dictionary sex = new Dictionary();
//    (贯标)出生年月  csny
    private Date birthYearMonth;
//    （贯标）婚姻状况  hyzk
    private String gbmaritalStatus;
//    婚姻状况
    private Long maritalStatusId;
    private Dictionary maritalStatus;
//    （贯标）邮政编码 yzbm
    private String postCode;
//    (贯标)证件类型    zjlx
    private String gbidCardType;
//    证件类型
    private Long idCardTypeId;
    private Dictionary idCardType;
// （贯标）证件号码  zjhm
    private String idCardNumber ;
//(贯标)学历  xueli
    private String gbeducation;
//    学历
    private Long educationId;
    private Dictionary education;
//    （贯标）户籍
    private Long koseki;
//    （贯标）家庭住址  jtzz
    private String homeAddress;
//    （贯标）固定电话号码 gddhhm
    private String fixedTelephoneNumber;
//    （贯标）手机号码  sjhm
    private String mobilePhone;
//    (贯标)职业    zhiye
    private String gbprofession;
//    职业
    private Long profession;
//    (贯标)职务  zhiwu
    private String gbtitle;
//    职务
    private Long title;
//    (贯标)职称 zhichen
    private String gbpositionalTitle;
//    职称
    private Long positionalTitleId;
    private Dictionary positionalTitle;
//    工资收入
    private BigDecimal wageIncome;
//    （贯标）家庭月收入  jtysr
    private BigDecimal homeMonthlyIncome;
//    是否冻结
    private Long isFreeze;
//    贷款否
    private Long loanState;
//    交款时限控制
    private String timeControl;
//    备用
    private String reserve;
//    新开户标志
    private Long newFlag;
//    客户编号
    private String customerNumber;
//    社保卡号
    private String societyCardNo;
//    管理状态
    private Long manageState;
//    单位交存比例
    private Long companyPaymentRatio;
//    个人交存比例
    private Long personPaymentRatio;
//个人历史最新id
    private Long historyId;

    private List<PersonHistory> personHistorieList = new ArrayList<PersonHistory>(); //个人历史信息集合

    private PersonHistory personHistory = new PersonHistory(); //个人历史信息集合


    private List<PersonAccountFund> personAccountFundList= new ArrayList<PersonAccountFund>(); //个人公积金账户

    private List<TransactionContract> transactionContractList =new ArrayList<TransactionContract>();  //交易合同
    private List<PersonMessage> personMessageList = new ArrayList<PersonMessage>();  //反馈员工信息
    private List<PersonTransferItem> personTransferItemList = new ArrayList<PersonTransferItem>(); //清册项（员工转移关联的员工信息）
    private List<CompanyRecord>  companyRecordList = new ArrayList<CompanyRecord>();  //机关/ 企事业单位的员工的就业履历
    private List<PersonSupplement> personSupplementList = new ArrayList<PersonSupplement>(); //个人补充信息
    private Company company;  //单位信息

    public Person() {
    }

    public Person(Long id) {

        this.id = id;
    }
    @OneToMany(targetEntity=PersonSupplement.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<PersonSupplement> getPersonSupplementList() {
        return personSupplementList;
    }

    public void setPersonSupplementList(List<PersonSupplement> personSupplementList) {
        this.personSupplementList = personSupplementList;
    }

    @OneToMany(targetEntity=CompanyRecord.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<CompanyRecord> getCompanyRecordList() {
        return companyRecordList;
    }

    public void setCompanyRecordList(List<CompanyRecord> companyRecordList) {
        this.companyRecordList = companyRecordList;
    }

    @OneToMany(targetEntity=PersonAccountFund.class, cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<PersonAccountFund> getPersonAccountFundList() {
        return personAccountFundList;
    }

    public void setPersonAccountFundList(List<PersonAccountFund> personAccountFundList) {
        this.personAccountFundList = personAccountFundList;
    }

    @OneToMany(targetEntity=TransactionContract.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<TransactionContract> getTransactionContractList() {
        return transactionContractList;
    }

    public void setTransactionContractList(List<TransactionContract> transactionContractList) {
        this.transactionContractList = transactionContractList;
    }

    @OneToMany(targetEntity=PersonMessage.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<PersonMessage> getPersonMessageList() {
        return personMessageList;
    }

    public void setPersonMessageList(List<PersonMessage> personMessageList) {
        this.personMessageList = personMessageList;
    }


    @OneToMany(targetEntity=PersonTransferItem.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<PersonTransferItem> getPersonTransferItemList() {
        return personTransferItemList;
    }

    public void setPersonTransferItemList(List<PersonTransferItem> personTransferItemList) {
        this.personTransferItemList = personTransferItemList;
    }

    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }




    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPerson")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    @Basic
    @Column(name = "GRZH")
    public String getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(String personalAccount) {
        this.personalAccount = personalAccount;
    }

    @Basic
    @Column(name = "XINGBIE")
    public String getGbSex() {
        return gbSex;
    }

    public void setGbSex(String gbSex) {
        this.gbSex = gbSex;
    }

    @Basic
    @Column(name = "XMQP")
    public String getNameAllPinYin() {
        return nameAllPinYin;
    }

    public void setNameAllPinYin(String nameAllPinYin) {
        this.nameAllPinYin = nameAllPinYin;
    }

    @Basic
    @Column(name = "XINGMING")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "SEX")
    public Long getSexId() {
        return sexId;
    }

    public void setSexId(Long sexId) {
        this.sexId = sexId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SEX", insertable = false, updatable = false)
    public Dictionary getSex() {
        return sex;
    }

    public void setSex(Dictionary sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "CSNY")
    public Date getBirthYearMonth() {
        return birthYearMonth;
    }

    public void setBirthYearMonth(Date birthYearMonth) {
        this.birthYearMonth = birthYearMonth;
    }

    @Basic
    @Column(name = "HYZK")
    public String getGbmaritalStatus() {
        return gbmaritalStatus;
    }

    public void setGbmaritalStatus(String gbmaritalStatus) {
        this.gbmaritalStatus = gbmaritalStatus;
    }



    @Basic
    @Column(name = "MARITAL_STATUS")
    public Long getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(Long maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY )
    @JoinColumn(name = "MARITAL_STATUS", insertable = false, updatable = false)
    public Dictionary getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Dictionary maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Basic
    @Column(name = "YZBM")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Basic
    @Column(name = "ZJLX")
    public String getGbidCardType() {
        return gbidCardType;
    }

    public void setGbidCardType(String gbidCardType) {
        this.gbidCardType = gbidCardType;
    }

    @Basic
    @Column(name = "ID_CARD_TYPE")
    public Long getIdCardTypeId() {
        return idCardTypeId;
    }

    public void setIdCardTypeId(Long idCardTypeId) {
        this.idCardTypeId = idCardTypeId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(Dictionary idCardType) {
        this.idCardType = idCardType;
    }

    @Basic
    @Column(name = "ZJHM")
    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    @Basic
    @Column(name = "XUELI")
    public String getGbeducation() {
        return gbeducation;
    }

    public void setGbeducation(String gbeducation) {
        this.gbeducation = gbeducation;
    }

    @Basic
    @Column(name = "EDUCATION")
    public Long getEducationId() {
        return educationId;
    }

    public void setEducationId(Long educationId) {
        this.educationId = educationId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "EDUCATION", insertable = false, updatable = false)
    public Dictionary getEducation() {
        return education;
    }

    public void setEducation(Dictionary education) {
        this.education = education;
    }

    @Basic
    @Column(name = "KOSEKI")
    public Long getKoseki() {
        return koseki;
    }

    public void setKoseki(Long koseki) {
        this.koseki = koseki;
    }

    @Basic
    @Column(name = "JTZZ")
    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    @Basic
    @Column(name = "GDDHHM")
    public String getFixedTelephoneNumber() {
        return fixedTelephoneNumber;
    }

    public void setFixedTelephoneNumber(String fixedTelephoneNumber) {
        this.fixedTelephoneNumber = fixedTelephoneNumber;
    }

    @Basic
    @Column(name = "SJHM")
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Basic
    @Column(name = "ZHIYE")
    public String getGbprofession() {
        return gbprofession;
    }

    public void setGbprofession(String gbprofession) {
        this.gbprofession = gbprofession;
    }




    @Basic
    @Column(name = "PROFESSION")
    public Long getProfession() {
        return profession;
    }

    public void setProfession(Long profession) {
        this.profession = profession;
    }


    @Basic
    @Column(name = "ZHIWU")
    public String getGbtitle() {
        return gbtitle;
    }

    public void setGbtitle(String gbtitle) {
        this.gbtitle = gbtitle;
    }




    @Basic
    @Column(name = "TITLE")
    public Long getTitle() {
        return title;
    }

    public void setTitle(Long title) {
        this.title = title;
    }

    @Basic
    @Column(name = "ZHICHEN")
    public String getGbpositionalTitle() {
        return gbpositionalTitle;
    }

    public void setGbpositionalTitle(String gbpositionalTitle) {
        this.gbpositionalTitle = gbpositionalTitle;
    }

    @Basic
    @Column(name = "POSITIONAL_TITLE")
    public Long getPositionalTitleId() {
        return positionalTitleId;
    }

    public void setPositionalTitleId(Long positionalTitleId) {
        this.positionalTitleId = positionalTitleId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "POSITIONAL_TITLE", insertable = false, updatable = false)
    public Dictionary getPositionalTitle() {
        return positionalTitle;
    }

    public void setPositionalTitle(Dictionary positionalTitle) {
        this.positionalTitle = positionalTitle;
    }




    @Basic
    @Column(name = "WAGE_INCOME")
    public BigDecimal getWageIncome() {
        return wageIncome;
    }

    public void setWageIncome(BigDecimal wageIncome) {
        this.wageIncome = wageIncome;
    }

    @Basic
    @Column(name = "JTYSR")
    public BigDecimal getHomeMonthlyIncome() {
        return homeMonthlyIncome;
    }

    public void setHomeMonthlyIncome(BigDecimal homeMonthlyIncome) {
        this.homeMonthlyIncome = homeMonthlyIncome;
    }




    @Basic
    @Column(name = "IS_FREEZE")
    public Long getIsFreeze() {
        return isFreeze;
    }

    public void setIsFreeze(Long isFreeze) {
        this.isFreeze = isFreeze;
    }

    @Basic
    @Column(name = "LOAN_STATE")
    public Long getLoanState() {
        return loanState;
    }

    public void setLoanState(Long loanState) {
        this.loanState = loanState;
    }

    @Basic
    @Column(name = "TIME_CONTROL")
    public String getTimeControl() {
        return timeControl;
    }

    public void setTimeControl(String timeControl) {
        this.timeControl = timeControl;
    }

    @Basic
    @Column(name = "RESERVE")
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Basic
    @Column(name = "NEW_FLAG")
    public Long getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(Long newFlag) {
        this.newFlag = newFlag;
    }

    @Basic
    @Column(name = "CUSTOMER_NUMBER")
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @Basic
    @Column(name = "SOCIETY_CARD_NO")
    public String getSocietyCardNo() {
        return societyCardNo;
    }

    public void setSocietyCardNo(String societyCardNo) {
        this.societyCardNo = societyCardNo;
    }

    @Basic
    @Column(name = "MANAGE_STATE")
    public Long getManageState() {
        return manageState;
    }

    public void setManageState(Long manageState) {
        this.manageState = manageState;
    }

    @Basic
    @Column(name = "COMPANY_PAYMENT_RATIO")
    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "PERSON_PAYMENT_RATIO")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof Person))
            return false;

        Person castOther = (Person) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id",getId())
                .append("companyId",getCompanyId())
                .append("NameAllPinYin",getNameAllPinYin())
                .append("name",getName()).toString();
    }
    @Column(name = "HISTORY_ID")
    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    @OneToMany(targetEntity=PersonHistory.class, cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="PERSON_ID")
    @OrderBy(value="id desc")
    public List<PersonHistory> getPersonHistorieList() {
        return personHistorieList;
    }

    public void setPersonHistorieList(List<PersonHistory> personHistorieList) {
        this.personHistorieList = personHistorieList;
    }
}
