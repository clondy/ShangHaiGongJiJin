/*
 *    	@(#)@HadSiteEntity.java  2017年10月24日
 *     
 *      @COPYRIGHT@
 */
package com.capinfo.accumulation.model;

/**
 * <p>
 * 用于标识包含siteId属性实体类的接口。
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public interface HadSiteEntity {
	
	public Long getSiteId();
	
	public void setSiteId(Long siteId);
}
