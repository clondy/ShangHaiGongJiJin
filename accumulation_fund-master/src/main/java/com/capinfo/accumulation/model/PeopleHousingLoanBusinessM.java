package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import javax.persistence.*;
import java.sql.Time;

/**
 * <p>
 * 个人住房贷款业务明细信息
 * </p>
 */
@Entity
@Table(name = "PEOPLE_HOUSING_LOAN_BUSINESS_M")
@SequenceGenerator(name = "seqPeopleHousingLoanBusinessM", sequenceName = "SEQ_PEO_HOU_LOAN_BUSINESS_M", allocationSize = 1)
public class PeopleHousingLoanBusinessM implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //（贯标）罚息金额 Penalty_interest_amount
    private long fxje;
    //(贯标)逾期转正常本金金额
    private long yqzzcbjje;
    //(贯标)利息金额
    private long lxje;
    //(贯标)业务流水号
    private String ywlsh;
    //(贯标)当期期次
    private long dqqc;
    //(贯标)业务发生日期
    private Time ywfsrq;
    //(贯标)正常转逾期本金金额
    private long zzzyqbjje;
    //(贯标)贷款业务明细类型
    private String dkywmxlx;
    //(贯标)发生额
    private long fse;
    //(贯标)本金金额
    private long bjje;
    //(贯标)贷款银行代码
    private String dkyhdm;
    //(贯标)记账日期
    private Time jzrq;
    //(贯标)贷款账号
    private String dkzh;

    public PeopleHousingLoanBusinessM(Long id) {
        this.id = id;
    }

    public PeopleHousingLoanBusinessM() {
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "FXJE")
    public long getFxje() {
        return fxje;
    }

    public void setFxje(long fxje) {
        this.fxje = fxje;
    }

    @Basic
    @Column(name = "YQZZCBJJE")
    public long getYqzzcbjje() {
        return yqzzcbjje;
    }

    public void setYqzzcbjje(long yqzzcbjje) {
        this.yqzzcbjje = yqzzcbjje;
    }

    @Basic
    @Column(name = "LXJE")
    public long getLxje() {
        return lxje;
    }

    public void setLxje(long lxje) {
        this.lxje = lxje;
    }

    @Basic
    @Column(name = "YWLSH")
    public String getYwlsh() {
        return ywlsh;
    }

    public void setYwlsh(String ywlsh) {
        this.ywlsh = ywlsh;
    }

    @Basic
    @Column(name = "DQQC")
    public long getDqqc() {
        return dqqc;
    }

    public void setDqqc(long dqqc) {
        this.dqqc = dqqc;
    }

    @Basic
    @Column(name = "YWFSRQ")
    public Time getYwfsrq() {
        return ywfsrq;
    }

    public void setYwfsrq(Time ywfsrq) {
        this.ywfsrq = ywfsrq;
    }

    @Basic
    @Column(name = "ZZZYQBJJE")
    public long getZzzyqbjje() {
        return zzzyqbjje;
    }

    public void setZzzyqbjje(long zzzyqbjje) {
        this.zzzyqbjje = zzzyqbjje;
    }

    @Basic
    @Column(name = "DKYWMXLX")
    public String getDkywmxlx() {
        return dkywmxlx;
    }

    public void setDkywmxlx(String dkywmxlx) {
        this.dkywmxlx = dkywmxlx;
    }

    @Basic
    @Column(name = "FSE")
    public long getFse() {
        return fse;
    }

    public void setFse(long fse) {
        this.fse = fse;
    }

    @Basic
    @Column(name = "BJJE")
    public long getBjje() {
        return bjje;
    }

    public void setBjje(long bjje) {
        this.bjje = bjje;
    }

    @Basic
    @Column(name = "DKYHDM")
    public String getDkyhdm() {
        return dkyhdm;
    }

    public void setDkyhdm(String dkyhdm) {
        this.dkyhdm = dkyhdm;
    }

    @Basic
    @Column(name = "JZRQ")
    public Time getJzrq() {
        return jzrq;
    }

    public void setJzrq(Time jzrq) {
        this.jzrq = jzrq;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PeopleHousingLoanBusinessM))
            return false;

        PeopleHousingLoanBusinessM castOther = (PeopleHousingLoanBusinessM) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .toString();
    }
}
