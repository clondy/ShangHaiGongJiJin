package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 交易明细（收/ 付款）
 * @Author: zhangxu
 * @Date 2017/9/20 17:10
 */
@Entity
@Table(name = "TRANSACTION_RECORD_DETAIL")
@SequenceGenerator(name = "seqTransactionRecordDetail", sequenceName = "SEQ_TRANSACTION_RECORD_DETAIL", allocationSize = 1)

public class TransactionRecordDetail implements BaseEntity {
    private static final long serialVersionUID = 7061670634075878286L;
    private Long id;
    //交易流水id
    private Long transactionRecordId;
    private TransactionRecord transactionRecord;
    //单位公积金id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund;
    //员工公积金id
    private Long personAccountFundId;
    private PersonAccountFund personAccountFund;


    //金额
    private BigDecimal amount;
    //记账日期  (贯标)  JZRQ
    private Date accountDate;
    //发生利息额 (贯标) FSLXE
    private BigDecimal accrualInterest;
    //业务流水号(贯标)  YWLSH
    private String businessNumber;
    //归集和提取业务类型 (贯标)gjhtqywlx
    private String gbPersonCollectionAndDraw;
    //归集和提取业务类型
    private Long personCollectionAndDrawId;
    private Dictionary personCollectionAndDraw;
    //个人账号 (贯标) GRZH
    private String personalAccount;
    //当年归集发生额 (贯标) DNGJFSE
    private BigDecimal collection;
    //上年结转发生额(贯标)  SNJZFSE
    private BigDecimal indirectBalanceLastYear;
    //冲账标识(贯标)  CZBZ
    private String gbReverse;
    //冲账标识
    private Long reverseId;
    private Dictionary reverse;
    //提取方式(贯标) TQFS
    private String gbDrawMode;
    //提取方式
    private Long drawModeId;
    private Dictionary drawMode;
    //提取原因(贯标)  TQYY
    private String gbDrawReaso;
    //提取原因
    private Long drawReasoId;
    private Dictionary drawReaso;
    //发生额(贯标)   FSE
    private BigDecimal Accrual;
    //单位账号(贯标)  DWZH
    private String companyAccount;
    //发生人数(贯标)  FSRS
    private Long accrualPeopleNumber;
    //业务明细类型(贯标) YWMXLX
    private String gbBusinessType;
    //业务明细类型
    private Long businessTypeId;
    private Dictionary businessType;
    //汇补缴年月(贯标) HBJNY
    private Date remit;

    public TransactionRecordDetail(Long id) {
        this.id = id;
    }

    public TransactionRecordDetail() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqTransactionRecordDetail")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TRANSACTION_RECORD_ID")
    public Long getTransactionRecordId() {
        return transactionRecordId;
    }

    public void setTransactionRecordId(Long transactionRecordId) {
        this.transactionRecordId = transactionRecordId;
    }

    @ManyToOne(targetEntity = TransactionRecord.class , cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "TRANSACTION_RECORD_ID", insertable = false, updatable = false)
    public TransactionRecord getTransactionRecord() {
        return transactionRecord;
    }

    public void setTransactionRecord(TransactionRecord transactionRecord) {
        this.transactionRecord = transactionRecord;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @ManyToOne(targetEntity = CompanyAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }

    @ManyToOne(targetEntity = PersonAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    @Basic
    @Column(name = "AMOUNT")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "JZRQ")
    public Date getAccountDate() {
        return accountDate;
    }

    public void setAccountDate(Date accountDate) {
        this.accountDate = accountDate;
    }

    @Basic
    @Column(name = "FSLXE")
    public BigDecimal getAccrualInterest() {
        return accrualInterest;
    }

    public void setAccrualInterest(BigDecimal accrualInterest) {
        this.accrualInterest = accrualInterest;
    }

    @Basic
    @Column(name = "YWLSH")
    public String getBusinessNumber() {
        return businessNumber;
    }

    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    @Basic
    @Column(name = "GJHTQYWLX")
    public String getGbPersonCollectionAndDraw() {
        return gbPersonCollectionAndDraw;
    }

    public void setGbPersonCollectionAndDraw(String gbPersonCollectionAndDraw) {
        this.gbPersonCollectionAndDraw = gbPersonCollectionAndDraw;
    }


    @Basic
    @Column(name = "PERSON_COLLECTION_AND_DRAW")
    public Long getPersonCollectionAndDrawId() {
        return personCollectionAndDrawId;
    }

    public void setPersonCollectionAndDrawId(Long personCollectionAndDrawId) {
        this.personCollectionAndDrawId = personCollectionAndDrawId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_COLLECTION_AND_DRAW", insertable = false, updatable = false )
    public Dictionary getPersonCollectionAndDraw() {
        return personCollectionAndDraw;
    }

    public void setPersonCollectionAndDraw(Dictionary personCollectionAndDraw) {
        this.personCollectionAndDraw = personCollectionAndDraw;
    }

    @Basic
    @Column(name = "GRZH")
    public String getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(String personalAccount) {
        this.personalAccount = personalAccount;
    }

    @Basic
    @Column(name = "DNGJFSE")
    public BigDecimal getCollection() {
        return collection;
    }

    public void setCollection(BigDecimal collection) {
        this.collection = collection;
    }

    @Basic
    @Column(name = "SNJZFSE")
    public BigDecimal getIndirectBalanceLastYear() {
        return indirectBalanceLastYear;
    }

    public void setIndirectBalanceLastYear(BigDecimal indirectBalanceLastYear) {
        this.indirectBalanceLastYear = indirectBalanceLastYear;
    }

    @Basic
    @Column(name = "CZBZ")
    public String getGbReverse() {
        return gbReverse;
    }

    public void setGbReverse(String gbReverse) {
        this.gbReverse = gbReverse;
    }

    @Basic
    @Column(name = "REVERSE")
    public Long getReverseId() {
        return reverseId;
    }

    public void setReverseId(Long reverseId) {
        this.reverseId = reverseId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REVERSE" , insertable = false, updatable = false)
    public Dictionary getReverse() {
        return reverse;
    }

    public void setReverse(Dictionary reverse) {
        this.reverse = reverse;
    }

    @Basic
    @Column(name = "TQFS")
    public String getGbDrawMode() {
        return gbDrawMode;
    }

    public void setGbDrawMode(String gbDrawMode) {
        this.gbDrawMode = gbDrawMode;
    }

    @Basic
    @Column(name = "DRAW_MODE")
    public Long getDrawModeId() {
        return drawModeId;
    }

    public void setDrawModeId(Long drawModeId) {
        this.drawModeId = drawModeId;
    }

    @ManyToOne(targetEntity = Dictionary.class , cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "DRAW_MODE", insertable = false, updatable = false )
    public Dictionary getDrawMode() {
        return drawMode;
    }

    public void setDrawMode(Dictionary drawMode) {
        this.drawMode = drawMode;
    }

    @Basic
    @Column(name = "TQYY")
    public String getGbDrawReaso() {
        return gbDrawReaso;
    }

    public void setGbDrawReaso(String gbDrawReaso) {
        this.gbDrawReaso = gbDrawReaso;
    }

    @Basic
    @Column(name = "DRAW_REASO")
    public Long getDrawReasoId() {
        return drawReasoId;
    }

    public void setDrawReasoId(Long drawReasoId) {
        this.drawReasoId = drawReasoId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "DRAW_REASO", insertable = false, updatable = false )
    public Dictionary getDrawReaso() {
        return drawReaso;
    }

    public void setDrawReaso(Dictionary drawReaso) {
        this.drawReaso = drawReaso;
    }

    @Basic
    @Column(name = "FSE")
    public BigDecimal getAccrual() {
        return Accrual;
    }

    public void setAccrual(BigDecimal accrual) {
        Accrual = accrual;
    }

    @Basic
    @Column(name = "DWZH")
    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    @Basic
    @Column(name = "FSRS")
    public Long getAccrualPeopleNumber() {
        return accrualPeopleNumber;
    }

    public void setAccrualPeopleNumber(Long accrualPeopleNumber) {
        this.accrualPeopleNumber = accrualPeopleNumber;
    }

    @Basic
    @Column(name = "YWMXLX")
    public String getGbBusinessType() {
        return gbBusinessType;
    }

    public void setGbBusinessType(String gbBusinessType) {
        this.gbBusinessType = gbBusinessType;
    }

    @Basic
    @Column(name = "BUSINESS_TYPE")
    public Long getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(Long businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "BUSINESS_TYPE", insertable = false, updatable = false )
    public Dictionary getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Dictionary businessType) {
        this.businessType = businessType;
    }

    @Basic
    @Column(name = "HBJNY")
    public Date getRemit() {
        return remit;
    }

    public void setRemit(Date remit) {
        this.remit = remit;
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("交易流水id", this.getTransactionRecordId())
                .append("个人公积金_id", this.getBusinessNumber())
                .append("金额", this.getAmount())
                .append("记账日期(贯标)_JZRQ", this.getAccountDate())
                .append("发生利息额 (贯标) FSLXE", this.getAccrualInterest())
                .append("业务流水号(贯标)  YWLSH", this.getBusinessNumber())
                .append("归集和提取业务类型 (贯标)gjhtqywlx", this.getGbPersonCollectionAndDraw())
                .append("归集和提取业务类型", this.getPersonCollectionAndDrawId())
                .append("个人账号 (贯标) GRZH", this.getPersonalAccount())
                .append("当年归集发生额 (贯标) DNGJFSE", this.getCollection())
                .append("上年结转发生额(贯标)  SNJZFSE", this.getIndirectBalanceLastYear())
                .append("冲账标识(贯标)  CZBZ", this.getGbReverse())
                .append("冲账标识", this.getReverseId())
                .append("提取方式(贯标) TQFS", this.getGbDrawMode())
                .append("提取方式", this.getDrawModeId())
                .append("提取原因(贯标)  TQYY", this.getGbDrawReaso())
                .append("提取原因", this.getDrawReasoId())
                .append("发生额(贯标)   FSE", this.getAccrual())
                .append("单位账号(贯标)  DWZH", this.getCompanyAccount())
                .append("发生人数(贯标)  FSRS", this.getAccrualPeopleNumber())
                .append("业务明细类型(贯标) YWMXLX", this.getGbBusinessType())
                .append("业务明细类型", this.getBusinessTypeId())
                .append("汇补缴年月(贯标) HBJNY", this.getRemit())
                .toString();
    }

    public boolean equals(Object other) {
        if ((this == other)) {

            return true;
        }
        if (!(other instanceof TransactionRecordDetail)) {

            return false;
        }

        TransactionRecordDetail caseOther = (TransactionRecordDetail) other;
        return new EqualsBuilder().append(this.getId(),
                caseOther.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
