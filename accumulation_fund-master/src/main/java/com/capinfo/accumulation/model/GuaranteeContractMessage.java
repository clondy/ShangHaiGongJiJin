package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Comment 担保合同信息
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "GUARANTEE_CONTRACT_MESSAGE")
@SequenceGenerator(name = "seqGUARANTEE_CONTRACT_MESSAGE", sequenceName = "SEQ_GUARANTEE_CONTRACT_MESSAGE", allocationSize = 1)
public class GuaranteeContractMessage implements BaseEntity{

    private static final long serialVersionUID = 1L;
    private Long id;
    private String bzjgmc;
    private String dbjgmc;
    private String dkdblx;
    private Long dkbzj;
    private Date fhbzjrq;
    private Date zyhtjsrq;
    private String zywmc;
    private String bzhtbh;
    private String jkhtbh;
    private Date zyhtksrq;
    private String zywbh;
    private String zyhtbh;
    private String dywfwzl;
    private Date dyqjcrq;
    private Long zywjz;
    private String dywtxqzh;
    private String dbhtbh;
    private String dywqzh;
    private Long dywpgjz;
    private Date dyqjlrq;


    public GuaranteeContractMessage(){

    }

    public GuaranteeContractMessage(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BZJGMC")
    public String getBzjgmc() {
        return bzjgmc;
    }

    public void setBzjgmc(String bzjgmc) {
        this.bzjgmc = bzjgmc;
    }

    @Basic
    @Column(name = "DBJGMC")
    public String getDbjgmc() {
        return dbjgmc;
    }

    public void setDbjgmc(String dbjgmc) {
        this.dbjgmc = dbjgmc;
    }

    @Basic
    @Column(name = "DKDBLX")
    public String getDkdblx() {
        return dkdblx;
    }

    public void setDkdblx(String dkdblx) {
        this.dkdblx = dkdblx;
    }

    @Basic
    @Column(name = "DKBZJ")
    public Long getDkbzj() {
        return dkbzj;
    }

    public void setDkbzj(Long dkbzj) {
        this.dkbzj = dkbzj;
    }

    @Basic
    @Column(name = "FHBZJRQ")
    public Date getFhbzjrq() {
        return fhbzjrq;
    }

    public void setFhbzjrq(Date fhbzjrq) {
        this.fhbzjrq = fhbzjrq;
    }

    @Basic
    @Column(name = "ZYHTJSRQ")
    public Date getZyhtjsrq() {
        return zyhtjsrq;
    }

    public void setZyhtjsrq(Date zyhtjsrq) {
        this.zyhtjsrq = zyhtjsrq;
    }

    @Basic
    @Column(name = "ZYWMC")
    public String getZywmc() {
        return zywmc;
    }

    public void setZywmc(String zywmc) {
        this.zywmc = zywmc;
    }

    @Basic
    @Column(name = "BZHTBH")
    public String getBzhtbh() {
        return bzhtbh;
    }

    public void setBzhtbh(String bzhtbh) {
        this.bzhtbh = bzhtbh;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "ZYHTKSRQ")
    public Date getZyhtksrq() {
        return zyhtksrq;
    }

    public void setZyhtksrq(Date zyhtksrq) {
        this.zyhtksrq = zyhtksrq;
    }

    @Basic
    @Column(name = "ZYWBH")
    public String getZywbh() {
        return zywbh;
    }

    public void setZywbh(String zywbh) {
        this.zywbh = zywbh;
    }

    @Basic
    @Column(name = "ZYHTBH")
    public String getZyhtbh() {
        return zyhtbh;
    }

    public void setZyhtbh(String zyhtbh) {
        this.zyhtbh = zyhtbh;
    }

    @Basic
    @Column(name = "DYWFWZL")
    public String getDywfwzl() {
        return dywfwzl;
    }

    public void setDywfwzl(String dywfwzl) {
        this.dywfwzl = dywfwzl;
    }

    @Basic
    @Column(name = "DYQJCRQ")
    public Date getDyqjcrq() {
        return dyqjcrq;
    }

    public void setDyqjcrq(Date dyqjcrq) {
        this.dyqjcrq = dyqjcrq;
    }

    @Basic
    @Column(name = "ZYWJZ")
    public Long getZywjz() {
        return zywjz;
    }

    public void setZywjz(Long zywjz) {
        this.zywjz = zywjz;
    }

    @Basic
    @Column(name = "DYWTXQZH")
    public String getDywtxqzh() {
        return dywtxqzh;
    }

    public void setDywtxqzh(String dywtxqzh) {
        this.dywtxqzh = dywtxqzh;
    }

    @Basic
    @Column(name = "DBHTBH")
    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    @Basic
    @Column(name = "DYWQZH")
    public String getDywqzh() {
        return dywqzh;
    }

    public void setDywqzh(String dywqzh) {
        this.dywqzh = dywqzh;
    }

    @Basic
    @Column(name = "DYWPGJZ")
    public Long getDywpgjz() {
        return dywpgjz;
    }

    public void setDywpgjz(Long dywpgjz) {
        this.dywpgjz = dywpgjz;
    }

    @Basic
    @Column(name = "DYQJLRQ")
    public Date getDyqjlrq() {
        return dyqjlrq;
    }

    public void setDyqjlrq(Date dyqjlrq) {
        this.dyqjlrq = dyqjlrq;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof GuaranteeContractMessage))
            return false;
        GuaranteeContractMessage castOther = (GuaranteeContractMessage)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
