package com.capinfo.accumulation.model.enums;

/**
 * Created by Rivex on 2017/9/20.
 */
public enum Channel {
	
	网厅(1)
	, 微信(2)
	, 中心网点(3)
	, 银行网点(4);

	// 成员变量
	private int token;
	private Channel name;

	// 构造方法
	private Channel(int token) {
		
		this.token = token;
	}

	public int getToken() {
		
		return token;
	}

	public Channel getName() {
		
		name= Channel.getObject(this.token);
		return name;
	}

	public static Channel getObject(int token) {

		switch (token) {

		case 1:
			
			return 网厅;
		case 2:
			
			return 微信;
		case 3:
			
			return 中心网点;
		case 4:
			
			return 银行网点;
		}
		
		return null;
	}
}
