package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.sql.Time;

/**
 * 财务总账信息（贯标）未涉及业务
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "FINANCIAL_GENERAL_LEDGER")
public class FinancialGeneralLedger {
    private long id;
    private Time jzrq;
    private long jffse;
    private long dffse;
    private String kmmc;
    private String qmyefx;
    private String zhaiyao;
    private long qmye;
    private long qcye;
    private String kmbh;
    private String qcyefx;

    public FinancialGeneralLedger(long id) {
        this.id = id;
    }

    public FinancialGeneralLedger() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "JZRQ")
    public Time getJzrq() {
        return jzrq;
    }

    public void setJzrq(Time jzrq) {
        this.jzrq = jzrq;
    }

    @Basic
    @Column(name = "JFFSE")
    public long getJffse() {
        return jffse;
    }

    public void setJffse(long jffse) {
        this.jffse = jffse;
    }

    @Basic
    @Column(name = "DFFSE")
    public long getDffse() {
        return dffse;
    }

    public void setDffse(long dffse) {
        this.dffse = dffse;
    }

    @Basic
    @Column(name = "KMMC")
    public String getKmmc() {
        return kmmc;
    }

    public void setKmmc(String kmmc) {
        this.kmmc = kmmc;
    }

    @Basic
    @Column(name = "QMYEFX")
    public String getQmyefx() {
        return qmyefx;
    }

    public void setQmyefx(String qmyefx) {
        this.qmyefx = qmyefx;
    }

    @Basic
    @Column(name = "ZHAIYAO")
    public String getZhaiyao() {
        return zhaiyao;
    }

    public void setZhaiyao(String zhaiyao) {
        this.zhaiyao = zhaiyao;
    }

    @Basic
    @Column(name = "QMYE")
    public long getQmye() {
        return qmye;
    }

    public void setQmye(long qmye) {
        this.qmye = qmye;
    }

    @Basic
    @Column(name = "QCYE")
    public long getQcye() {
        return qcye;
    }

    public void setQcye(long qcye) {
        this.qcye = qcye;
    }

    @Basic
    @Column(name = "KMBH")
    public String getKmbh() {
        return kmbh;
    }

    public void setKmbh(String kmbh) {
        this.kmbh = kmbh;
    }

    @Basic
    @Column(name = "QCYEFX")
    public String getQcyefx() {
        return qcyefx;
    }

    public void setQcyefx(String qcyefx) {
        this.qcyefx = qcyefx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialGeneralLedger that = (FinancialGeneralLedger) o;

        if (id != that.id) return false;
        if (jffse != that.jffse) return false;
        if (dffse != that.dffse) return false;
        if (qmye != that.qmye) return false;
        if (qcye != that.qcye) return false;
        if (jzrq != null ? !jzrq.equals(that.jzrq) : that.jzrq != null) return false;
        if (kmmc != null ? !kmmc.equals(that.kmmc) : that.kmmc != null) return false;
        if (qmyefx != null ? !qmyefx.equals(that.qmyefx) : that.qmyefx != null) return false;
        if (zhaiyao != null ? !zhaiyao.equals(that.zhaiyao) : that.zhaiyao != null) return false;
        if (kmbh != null ? !kmbh.equals(that.kmbh) : that.kmbh != null) return false;
        if (qcyefx != null ? !qcyefx.equals(that.qcyefx) : that.qcyefx != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (jzrq != null ? jzrq.hashCode() : 0);
        result = 31 * result + (int) (jffse ^ (jffse >>> 32));
        result = 31 * result + (int) (dffse ^ (dffse >>> 32));
        result = 31 * result + (kmmc != null ? kmmc.hashCode() : 0);
        result = 31 * result + (qmyefx != null ? qmyefx.hashCode() : 0);
        result = 31 * result + (zhaiyao != null ? zhaiyao.hashCode() : 0);
        result = 31 * result + (int) (qmye ^ (qmye >>> 32));
        result = 31 * result + (int) (qcye ^ (qcye >>> 32));
        result = 31 * result + (kmbh != null ? kmbh.hashCode() : 0);
        result = 31 * result + (qcyefx != null ? qcyefx.hashCode() : 0);
        return result;
    }
}
