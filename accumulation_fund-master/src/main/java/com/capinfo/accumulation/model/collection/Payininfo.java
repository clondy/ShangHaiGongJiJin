package com.capinfo.accumulation.model.collection;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Comment 汇缴核定（汇缴信息 )
 *  上个月的汇缴+这个月的变更
 *  核定汇缴清册
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "PAYININFO")
@SequenceGenerator(name = "seqPAYININFO", sequenceName = "SEQ_PAYININFO", allocationSize = 1)
public class Payininfo implements BaseEntity {

    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    private Long companyAccountFundId;

    //
    private CompanyAccountFund companyAccountFund;

    //流水号
    private String serialNumber;

    //单位账号
    private String companyAccount;

    //核定选项
    private Long verificationOptions;

    //缴存年月
    private String payMonthly;

    //核定日期
    private Date receivedDate;

    //汇缴金额
    private BigDecimal remitAmount;

    //汇缴人数
    private Long remitNumber;

    //发生时间
    private Date dateTime;

    //银行编码
    private String bankCode;

    //操作员
    private Long operatorId;

    //区县
    private Long regionId;

    //网点
    private Long siteId;

    //复核人
    private Long confirmerId;

    //复核时间
    private Date confirmationTime;

    //渠道
    private Long channels;

    //创建人名称
    private String createName;

    //关联Payinitems
    private List<Payinitem> payinitems=new ArrayList<>();

    //上月汇缴金额LASTREMIT_AMOUNT
    private BigDecimal lastRemitAmount;

    //上月汇缴人数LASTREMIT_NUMBER
    private Long lastRemitNumber;

    //本月增加金额INCREASEREMIT_AMOUNT
    private BigDecimal increaseRemitAmount;

    //本月增加人数INCREASEREMIT_NUMBER
    private Long increaseRemitNumber;

    //本月减少金额REDUCEREMIT_AMOUNT
    private BigDecimal reduceRemitAmount;

    //本月减少人数REDUCEREMIT_NUMBER
    private Long reduceRemitNumber;

    //完成状态ISFINISHED
    private Boolean isfinished;

    private PersonAccountFund personAccountFund;


    public Payininfo(){

    }

    public Payininfo(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPAYININFO")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @Basic
    @Column(name = "LASTREMIT_AMOUNT")
    public BigDecimal getLastRemitAmount() {
        return lastRemitAmount;
    }
    public void setLastRemitAmount(BigDecimal lastRemitAmount) {
        this.lastRemitAmount = lastRemitAmount;
    }
    @Basic
    @Column(name = "LASTREMIT_NUMBER")
    public Long getLastRemitNumber() {
        return lastRemitNumber;
    }

    public void setLastRemitNumber(Long lastRemitNumber) {
        this.lastRemitNumber = lastRemitNumber;
    }
    @Basic
    @Column(name = "INCREASEREMIT_AMOUNT")
    public BigDecimal getIncreaseRemitAmount() {
        return increaseRemitAmount;
    }

    public void setIncreaseRemitAmount(BigDecimal increaseRemitAmount) {
        this.increaseRemitAmount = increaseRemitAmount;
    }
    @Basic
    @Column(name = "INCREASEREMIT_NUMBER")
    public Long getIncreaseRemitNumber() {
        return increaseRemitNumber;
    }

    public void setIncreaseRemitNumber(Long increaseRemitNumber) {
        this.increaseRemitNumber = increaseRemitNumber;
    }
    @Basic
    @Column(name = "REDUCEREMIT_AMOUNT")
    public BigDecimal getReduceRemitAmount() {
        return reduceRemitAmount;
    }

    public void setReduceRemitAmount(BigDecimal reduceRemitAmount) {
        this.reduceRemitAmount = reduceRemitAmount;
    }
    @Basic
    @Column(name = "REDUCEREMIT_NUMBER")
    public Long getReduceRemitNumber() {
        return reduceRemitNumber;
    }

    public void setReduceRemitNumber(Long reduceRemitNumber) {
        this.reduceRemitNumber = reduceRemitNumber;
    }
    @Basic
    @Column(name = "ISFINISHED")
    public Boolean getIsfinished() {
        return isfinished;
    }

    public void setIsfinished(Boolean isfinished) {
        this.isfinished = isfinished;
    }


    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "SERIAL_NUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT")
    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    @Basic
    @Column(name = "VERIFICATION_OPTIONS")
    public Long getVerificationOptions() {
        return verificationOptions;
    }

    public void setVerificationOptions(Long verificationOptions) {
        this.verificationOptions = verificationOptions;
    }

    @Basic
    @Column(name = "PAY_MONTHLY")
    public String getPayMonthly() {
        return payMonthly;
    }

    public void setPayMonthly(String payMonthly) {
        this.payMonthly = payMonthly;
    }

    @Basic
    @Column(name = "RECEIVED_DATE")
    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    @Basic
    @Column(name = "REMIT_AMOUNT")
    public BigDecimal getRemitAmount() {
        return remitAmount;
    }

    public void setRemitAmount(BigDecimal remitAmount) {
        this.remitAmount = remitAmount;
    }

    @Basic
    @Column(name = "REMIT_NUMBER")
    public Long getRemitNumber() {
        return remitNumber;
    }

    public void setRemitNumber(Long remitNumber) {
        this.remitNumber = remitNumber;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @OneToMany(mappedBy="payininfo", targetEntity=Payinitem.class,cascade=javax.persistence.CascadeType.ALL)
    public List<Payinitem> getPayinitems() {
        return payinitems;
    }

    public void setPayinitems(List<Payinitem> payinitems) {
        this.payinitems = payinitems;
    }
    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof Payininfo))
            return false;
        Payininfo castOther = (Payininfo)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
