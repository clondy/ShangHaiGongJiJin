package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.framework.model.BaseEntity;



@Entity
@Table(name = "KMSZJBSJ")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_ACCOUNT_BANK_SPECIALT", allocationSize = 1)

public class Kmszjbsj implements BaseEntity {
 
	private Long id;
	private String hsdw;
	private Ztsz ztdm;
	private String kmbh;
	private String  kmmc;
	private Long kmjb;
	private String kmsx;
	private String kjkmlb;
	private String kmyefx;
	private Long sfmjkm;
	private Double ncye;
	private Double ye;
	private Double bnljjf;
	private Double bnljdf;
	private Long tzbj;
	private Long rjzlx;
	private String fzhslx;
	private int zt;
	private Date crsj;
	private String kjnd;
	private String jbjg;
	private int spzt;
	private Kmszjbsj PKMBH;
	@Basic
    @Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}

	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}

	private List<Kmszxglssj> kmszxglssjList;

	@Basic
    @Column(name = "JBJG")
	    public String getJbjg() {
		return jbjg;
	}

	public void setJbjg(String jbjg) {
		this.jbjg = jbjg;
	}

		@Id
	    @Column(name = "ID")
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
	public Long getId() {
		return id;
	}
		@Basic
	    @Column(name = "KJND")
	public String getKjnd() {
			return kjnd;
		}

		public void setKjnd(String kjnd) {
			this.kjnd = kjnd;
		}

	public void setId(Long id) {
		this.id = id;
	}
	@Basic
    @Column(name = "HSDW")
	public String getHsdw() {
		return hsdw;
	}

	public void setHsdw(String hsdw) {
		this.hsdw = hsdw;
	}
	
	@Basic
    @Column(name = "KMMC")
	public String getKmmc() {
		return kmmc;
	}

	public void setKmmc(String kmmc) {
		this.kmmc = kmmc;
	}
	@Basic
    @Column(name = "KMJB")
	public Long getKmjb() {
		return kmjb;
	}

	public void setKmjb(Long kmjb) {
		this.kmjb = kmjb;
	}
	@Basic
    @Column(name = "KMSX")
	public String getKmsx() {
		return kmsx;
	}

	public void setKmsx(String kmsx) {
		this.kmsx = kmsx;
	}
	@Basic
    @Column(name = "KJKMLB")
	public String getKjkmlb() {
		return kjkmlb;
	}

	public void setKjkmlb(String kjkmlb) {
		this.kjkmlb = kjkmlb;
	}
	@Basic
    @Column(name = "KMYEFX")
	public String getKmyefx() {
		return kmyefx;
	}

	public void setKmyefx(String kmyefx) {
		this.kmyefx = kmyefx;
	}
	@Basic
    @Column(name = "SFMJKM")
	public Long getSfmjkm() {
		return sfmjkm;
	}

	public void setSfmjkm(Long sfmjkm) {
		this.sfmjkm = sfmjkm;
	}
	@Basic
    @Column(name = "TZBJ")
	public Long getTzbj() {
		return tzbj;
	}

	public void setTzbj(Long tzbj) {
		this.tzbj = tzbj;
	}
	@Basic
    @Column(name = "RJZLX")
	public Long getRjzlx() {
		return rjzlx;
	}

	public void setRjzlx(Long rjzlx) {
		this.rjzlx = rjzlx;
	}
	@Basic
    @Column(name = "FZHSLX")
	public String getFzhslx() {
		return fzhslx;
	}

	public void setFzhslx(String fzhslx) {
		this.fzhslx = fzhslx;
	}
	@Basic
    @Column(name = "ZT")
	public int getZt() {
		return zt;
	}

	public void setZt(int zt) {
		this.zt = zt;
	}
	@Basic
    @Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}

	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	
	@Basic
    @Column(name = "KMBH")
	public String getKmbh() {
		return kmbh;
	}

	public void setKmbh(String kmbh) {
		this.kmbh = kmbh;
	}
	
	@OneToMany(mappedBy="kmszid" ,fetch =FetchType.LAZY )   
	@OrderBy(clause="ID DESC")
	public List<Kmszxglssj> getKmszxglssjList() {
		return kmszxglssjList;
	}

	public void setKmszxglssjList(List<Kmszxglssj> kmszxglssjList) {
		this.kmszxglssjList = kmszxglssjList;
	}
	@Basic
    @Column(name = "NCYE")
	public Double getNcye() {
		return ncye;
	}

	public void setNcye(Double ncye) {
		this.ncye = ncye;
	}
	@Basic
    @Column(name = "YE")
	public Double getYe() {
		return ye;
	}

	public void setYe(Double ye) {
		this.ye = ye;
	}
	@Basic
    @Column(name = "BNLJJF")
	public Double getBnljjf() {
		return bnljjf;
	}

	public void setBnljjf(Double bnljjf) {
		this.bnljjf = bnljjf;
	}
	@Basic
    @Column(name = "BNLJDF")
	public Double getBnljdf() {
		return bnljdf;
	}

	public void setBnljdf(Double bnljdf) {
		this.bnljdf = bnljdf;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ztdm" ,referencedColumnName="ztdm" )
	public Ztsz getZtdm() {
		return ztdm;
	}

	public void setZtdm(Ztsz ztdm) {
		this.ztdm = ztdm;
	}
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PKMBH" ,referencedColumnName="KMBH" )
	public Kmszjbsj getPKMBH() {
		return PKMBH;
	}

	public void setPKMBH(Kmszjbsj pKMBH) {
		PKMBH = pKMBH;
	}
	
}
