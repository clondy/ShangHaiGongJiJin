package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "ZTSPLSSj")
@SequenceGenerator(name = "SEQZTSPLSSJ", sequenceName = "SEQ_ZTSPLSSJ", allocationSize = 1)
public class Ztsplssj implements BaseEntity {
	private Long id;
	private Long ztls;
	//数据状态
	private int sjzt;
	//审批状态
	private int spzt;
	//审批意见
	private String spyj;
	//操作员
	private String czy;
	//操作时间
	private Date czsj;
	//审批人
	private String spr;
	//审批时间
	private Date spsj;
	
	
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQZTSPLSSJ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	@Basic
	@Column(name = "ZTLS")
	public Long getZtls() {
		return ztls;
	}

	public void setZtls(Long ztls) {
		this.ztls = ztls;
	}
	@Basic
	@Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}

	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}
	@Basic
	@Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}

	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	@Basic
	@Column(name = "SPYJ")
	public String getSpyj() {
		return spyj;
	}

	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}
	@Basic
	@Column(name = "CZY")
	public String getCzy() {
		return czy;
	}

	public void setCzy(String czy) {
		this.czy = czy;
	}
	@Basic
	@Column(name = "CZSJ")
	public Date getCzsj() {
		return czsj;
	}

	public void setCzsj(Date czsj) {
		this.czsj = czsj;
	}
	@Basic
	@Column(name = "SPR")
	public String getSpr() {
		return spr;
	}

	public void setSpr(String spr) {
		this.spr = spr;
	}
	@Basic
	@Column(name = "SPSJ")
	public Date getSpsj() {
		return spsj;
	}

	public void setSpsj(Date spsj) {
		this.spsj = spsj;
	}



}
