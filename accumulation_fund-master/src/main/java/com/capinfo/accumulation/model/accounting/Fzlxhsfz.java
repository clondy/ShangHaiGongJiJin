package com.capinfo.accumulation.model.accounting;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.capinfo.framework.model.BaseEntity;
/**
 * 辅助类型设置分组说明
 * @author Think
 *
 */
@Entity
@Table(name = "FZLXHSFZ")
@SequenceGenerator(name = "SEQ_FZLXHSFZ",sequenceName = "SEQ_FZLXHSFZ",allocationSize = 1)
public class Fzlxhsfz implements BaseEntity{
	private Long id;
	private String hsfzbm;
	private String hsfzsm;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_FZLXHSFZ")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Basic
	@Column(name = "HSFZBM")
	public String getHsfzbm() {
		return hsfzbm;
	}
	public void setHsfzbm(String hsfzbm) {
		this.hsfzbm = hsfzbm;
	}
	@Basic
	@Column(name = "HSFZSM")
	public String getHsfzsm() {
		return hsfzsm;
	}
	public void setHsfzsm(String hsfzsm) {
		this.hsfzsm = hsfzsm;
	}
	

}
