package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * 辅助核算项目基本数据实体
 * @author
 *
 */
@Entity
@Table(name = "FZHSXMJBSJ")
@SequenceGenerator(name = "SEQ_FZHSXMJBSJ", sequenceName = "SEQ_FZHSXMJBSJ", allocationSize = 1)
public class Fzhsxmjbsj implements BaseEntity {

	private Long id;
	//核算类型编码
	private Fzlxsz  hslxbm;
	//核算项目编码
	private String hsxmbm;
	//核算项目名称
	private String hsxmmc;
	//是否低级明细
	private String sfdjmx;
	//二元辅助
	private String eyfz;
	//插入时间
	private Date crsj;
	private Dictionary kjnd;
	private List<Fzhsxglssj>  fzhsxglssj;
	
	private int spzt;
	

	
	@Basic
	@Column(name = "SPZT")

	public int getSpzt() {
		return spzt;
	}
	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FZHSXMJBSJ")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="HSLXBM" ,referencedColumnName="hslxbm")
	public Fzlxsz getHslxbm() {
		return hslxbm;
	}
	public void setHslxbm(Fzlxsz hslxbm) {
		this.hslxbm = hslxbm;
	}
	@Basic
	@Column(name = "HSXMBM")
	public String getHsxmbm() {
		return hsxmbm;
	}
	public void setHsxmbm(String hsxmbm) {
		this.hsxmbm = hsxmbm;
	}
	
	@Basic
	@Column(name = "HSXMMC")
	public String getHsxmmc() {
		return hsxmmc;
	}
	public void setHsxmmc(String hsxmmc) {
		this.hsxmmc = hsxmmc;
	}
	
	@Basic
	@Column(name = "SFDJMX")
	public String getSfdjmx() {
		return sfdjmx;
	}
	public void setSfdjmx(String sfdjmx) {
		this.sfdjmx = sfdjmx;
	}
	
	@Basic
	@Column(name = "EYFZ")
	public String getEyfz() {
		return eyfz;
	}
	public void setEyfz(String eyfz) {
		this.eyfz = eyfz;
	}
	
	
	@Basic
	@Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}
	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="KJND" ,referencedColumnName="ID" )
	public Dictionary getKjnd() {
		return kjnd;
	}
	public void setKjnd(Dictionary kjnd) {
		this.kjnd = kjnd;
	}
	@OneToMany(mappedBy="hsid" ,fetch =FetchType.LAZY )   
	@OrderBy(clause="id DESC")
	public List<Fzhsxglssj> getFzhsxglssj() {
		return fzhsxglssj;
	}
	public void setFzhsxglssj(List<Fzhsxglssj> fzhsxglssj) {
		this.fzhsxglssj = fzhsxglssj;
	}
}
