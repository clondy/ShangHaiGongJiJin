package com.capinfo.accumulation.model.accounting;

import java.util.List;
import java.util.Date;
/**
 *  @author zxl
 */
public class GB_JZPZXX {
	/**
	 * 主键
	 */	
    private Long ID;
	/**
	 * 记账凭证号
	 */	
    private String JZPZH;
	/**
	 * 摘要
	 */	
    private String ZHAIYAO;
	/**
	 * 科目编号
	 */	
    private String KMBH;
	/**
	 * 借方发生额
	 */	
    private Double JFFSE;
	/**
	 * 贷方发生额
	 */	
    private Double DFFSE;
	/**
	 * 附件单据数
	 */	
    private Long FJDJS;
	/**
	 * 记账日期
	 */	
    private java.util.Date JZRQ;
	/**
	 * 是否生成明细账
	 */	
    private Long SFSCMXZ;
	/**
	 * 业务类型
	 */	
    private String YWLX;
	/**
	 * 资金业务类型
	 */	
    private String ZJYWLX;
	/**
	 * 科目名称
	 */	
    private String KMMC;
	/**
	 * 制单
	 */	
    private String ZD;
	/**
	 * 制单日期
	 */	
    private java.util.Date ZDRQ;
	/**
	 * 复核
	 */	
    private String FH;
	/**
	 * 复核日期
	 */	
    private java.util.Date FHRQ;
	/**
	 * 凭证状态
	 */	
    private String PZZT;
	/**
	 * 汇总凭证号
	 */	
    private String HZPZH;
	/**
	 * 显示凭证号
	 */	
    private String XSPZH;
	/**
	 * 凭证类别
	 */	
    private String PZLB;
	/**
	 * 问题说明
	 */	
    private String WTSM;
	/**
	 * 记账
	 */	
    private String JZ;
	/**
	 * 凭证日期
	 */	
    private java.util.Date PZRQ;
    /**
	 * 凭证状态标识
	 */	
    private String PZZTBS;
    /**
	 * 凭证日期制作标识
	 */	
    private String ZZBS;
    
    /**
   	 * 核算单位
   	 */	
       private Long HSDW;
       /**
   	 * 凭证日期制作标识
   	 */	
       private Long ZTDM;
    
       public void setHSDW(Long HSDW){
           this.HSDW = HSDW;
       }
       public Long getHSDW(){
           return this.HSDW;
       }
       
       public void setZTDM(Long ZTDM){
           this.ZTDM = ZTDM;
       }
       public Long getZTDM(){
           return this.ZTDM;
       }
    public void setPZZTBS(String PZZTBS){
        this.PZZTBS = PZZTBS;
    }
    public String getPZZTBS(){
        return this.PZZTBS;
    }
    public void setZZBS(String ZZBS){
        this.ZZBS = ZZBS;
    }
    public String getZZBS(){
        return this.ZZBS;
    }
    public void setID(Long ID){
        this.ID = ID;
    }
    public Long getID(){
        return this.ID;
    }
    public void setJZPZH(String JZPZH){
        this.JZPZH = JZPZH;
    }
    public String getJZPZH(){
        return this.JZPZH;
    }
    public void setZHAIYAO(String ZHAIYAO){
        this.ZHAIYAO = ZHAIYAO;
    }
    public String getZHAIYAO(){
        return this.ZHAIYAO;
    }
    public void setKMBH(String KMBH){
        this.KMBH = KMBH;
    }
    public String getKMBH(){
        return this.KMBH;
    }
    public void setJFFSE(Double JFFSE){
        this.JFFSE = JFFSE;
    }
    public Double getJFFSE(){
        return this.JFFSE;
    }
    public void setDFFSE(Double DFFSE){
        this.DFFSE = DFFSE;
    }
    public Double getDFFSE(){
        return this.DFFSE;
    }
    public void setFJDJS(Long FJDJS){
        this.FJDJS = FJDJS;
    }
    public Long getFJDJS(){
        return this.FJDJS;
    }
    public void setJZRQ(java.util.Date JZRQ){
        this.JZRQ = JZRQ;
    }
    public java.util.Date getJZRQ(){
        return this.JZRQ;
    }
    public void setSFSCMXZ(Long SFSCMXZ){
        this.SFSCMXZ = SFSCMXZ;
    }
    public Long getSFSCMXZ(){
        return this.SFSCMXZ;
    }
    public void setYWLX(String YWLX){
        this.YWLX = YWLX;
    }
    public String getYWLX(){
        return this.YWLX;
    }
    public void setZJYWLX(String ZJYWLX){
        this.ZJYWLX = ZJYWLX;
    }
    public String getZJYWLX(){
        return this.ZJYWLX;
    }
    public void setKMMC(String KMMC){
        this.KMMC = KMMC;
    }
    public String getKMMC(){
        return this.KMMC;
    }
    public void setZD(String ZD){
        this.ZD = ZD;
    }
    public String getZD(){
        return this.ZD;
    }
    public void setZDRQ(java.util.Date ZDRQ){
        this.ZDRQ = ZDRQ;
    }
    public java.util.Date getZDRQ(){
        return this.ZDRQ;
    }
    public void setFH(String FH){
        this.FH = FH;
    }
    public String getFH(){
        return this.FH;
    }
    public void setFHRQ(java.util.Date FHRQ){
        this.FHRQ = FHRQ;
    }
    public java.util.Date getFHRQ(){
        return this.FHRQ;
    }
    public void setPZZT(String PZZT){
        this.PZZT = PZZT;
    }
    public String getPZZT(){
        return this.PZZT;
    }
    public void setHZPZH(String HZPZH){
        this.HZPZH = HZPZH;
    }
    public String getHZPZH(){
        return this.HZPZH;
    }
    public void setXSPZH(String XSPZH){
        this.XSPZH = XSPZH;
    }
    public String getXSPZH(){
        return this.XSPZH;
    }
    public void setPZLB(String PZLB){
        this.PZLB = PZLB;
    }
    public String getPZLB(){
        return this.PZLB;
    }
    public void setWTSM(String WTSM){
        this.WTSM = WTSM;
    }
    public String getWTSM(){
        return this.WTSM;
    }
    public void setJZ(String JZ){
        this.JZ = JZ;
    }
    public String getJZ(){
        return this.JZ;
    }
    public void setPZRQ(java.util.Date PZRQ){
        this.PZRQ = PZRQ;
    }
    public java.util.Date getPZRQ(){
        return this.PZRQ;
    }
}