package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;

import javax.persistence.*;
import java.sql.Time;

/**财务记账凭证信息（贯标）未涉及业务
 * Created by Rexxxar on 2017/9/19.
 */
@Entity

@SequenceGenerator(name = "seqFinancialAccountingDocument", sequenceName = "SEQ_FINANCIAL_ACCOUNTING_DOCUMENT", allocationSize=1)
@Table(name = "FINANCIAL_ACCOUNTING_DOCUMENT")
public class FinancialAccountingDocument implements BaseEntity {
    private Long id;
    private Time jzrq;
    private long jffse;
    private long dffse;
    private String zhaiyao;
    private short fjdjs;
    private String kmbh;
    private String jzpzh;

    public FinancialAccountingDocument(Long id) {
        this.id = id;
    }

    public FinancialAccountingDocument() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqFinancialAccountingDocument")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "JZRQ")
    public Time getJzrq() {
        return jzrq;
    }

    public void setJzrq(Time jzrq) {
        this.jzrq = jzrq;
    }

    @Basic
    @Column(name = "JFFSE")
    public long getJffse() {
        return jffse;
    }

    public void setJffse(long jffse) {
        this.jffse = jffse;
    }

    @Basic
    @Column(name = "DFFSE")
    public long getDffse() {
        return dffse;
    }

    public void setDffse(long dffse) {
        this.dffse = dffse;
    }

    @Basic
    @Column(name = "ZHAIYAO")
    public String getZhaiyao() {
        return zhaiyao;
    }

    public void setZhaiyao(String zhaiyao) {
        this.zhaiyao = zhaiyao;
    }

    @Basic
    @Column(name = "FJDJS")
    public short getFjdjs() {
        return fjdjs;
    }

    public void setFjdjs(short fjdjs) {
        this.fjdjs = fjdjs;
    }

    @Basic
    @Column(name = "KMBH")
    public String getKmbh() {
        return kmbh;
    }

    public void setKmbh(String kmbh) {
        this.kmbh = kmbh;
    }

    @Basic
    @Column(name = "JZPZH")
    public String getJzpzh() {
        return jzpzh;
    }

    public void setJzpzh(String jzpzh) {
        this.jzpzh = jzpzh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialAccountingDocument that = (FinancialAccountingDocument) o;

        if (id != that.id) return false;
        if (jffse != that.jffse) return false;
        if (dffse != that.dffse) return false;
        if (fjdjs != that.fjdjs) return false;
        if (jzrq != null ? !jzrq.equals(that.jzrq) : that.jzrq != null) return false;
        if (zhaiyao != null ? !zhaiyao.equals(that.zhaiyao) : that.zhaiyao != null) return false;
        if (kmbh != null ? !kmbh.equals(that.kmbh) : that.kmbh != null) return false;
        if (jzpzh != null ? !jzpzh.equals(that.jzpzh) : that.jzpzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (jzrq != null ? jzrq.hashCode() : 0);
        result = 31 * result + (int) (jffse ^ (jffse >>> 32));
        result = 31 * result + (int) (dffse ^ (dffse >>> 32));
        result = 31 * result + (zhaiyao != null ? zhaiyao.hashCode() : 0);
        result = 31 * result + (int) fjdjs;
        result = 31 * result + (kmbh != null ? kmbh.hashCode() : 0);
        result = 31 * result + (jzpzh != null ? jzpzh.hashCode() : 0);
        return result;
    }
}
