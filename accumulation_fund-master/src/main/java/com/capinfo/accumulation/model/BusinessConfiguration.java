package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.beans.beancontext.BeanContext;

/**
 * Created by Rexxxar on 2017/9/19.
 * 业务配置
 * @author fengjingsen
 */
@Entity
@Table(name = "BUSINESS_CONFIGURATION")
@SequenceGenerator(name = "seqBusinessConfiguration", sequenceName = "SEQ_BUSINESS_CONFIGURATION", allocationSize = 1)
public class BusinessConfiguration implements BaseEntity {
    private Long id;

    public BusinessConfiguration(Long id) {
        this.id = id;
    }

    public BusinessConfiguration() {
    }
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqBusinessConfiguration")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof BusinessConfiguration))
            return false;
        BusinessConfiguration otherBusinessConfiguration = (BusinessConfiguration) other;
        return new EqualsBuilder().append(this.getId(),
                otherBusinessConfiguration.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
