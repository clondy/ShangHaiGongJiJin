package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 项目贷款业务明细信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:37
 */
@Entity
@Table(name = "PROJECT_LOAN_BUSINESS_MESSAGE")
public class ProjectLoanBusinessMessage {
    private Long id;
    private Long lxje;
    private String ywlsh;
    private Long nyfxje;
    private Long fse;
    private Long bjje;
    private Long yqfxje;
    private String ywmxlx;
    private Date jzrq;
    private String dkzh;

    public ProjectLoanBusinessMessage(Long id) {
        this.id = id;
    }

    public ProjectLoanBusinessMessage() {
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "LXJE")
    public Long getLxje() {
        return lxje;
    }

    public void setLxje(Long lxje) {
        this.lxje = lxje;
    }

    @Basic
    @Column(name = "YWLSH")
    public String getYwlsh() {
        return ywlsh;
    }

    public void setYwlsh(String ywlsh) {
        this.ywlsh = ywlsh;
    }

    @Basic
    @Column(name = "NYFXJE")
    public Long getNyfxje() {
        return nyfxje;
    }

    public void setNyfxje(Long nyfxje) {
        this.nyfxje = nyfxje;
    }

    @Basic
    @Column(name = "FSE")
    public Long getFse() {
        return fse;
    }

    public void setFse(Long fse) {
        this.fse = fse;
    }

    @Basic
    @Column(name = "BJJE")
    public Long getBjje() {
        return bjje;
    }

    public void setBjje(Long bjje) {
        this.bjje = bjje;
    }

    @Basic
    @Column(name = "YQFXJE")
    public Long getYqfxje() {
        return yqfxje;
    }

    public void setYqfxje(Long yqfxje) {
        this.yqfxje = yqfxje;
    }

    @Basic
    @Column(name = "YWMXLX")
    public String getYwmxlx() {
        return ywmxlx;
    }

    public void setYwmxlx(String ywmxlx) {
        this.ywmxlx = ywmxlx;
    }

    @Basic
    @Column(name = "JZRQ")
    public Date getJzrq() {
        return jzrq;
    }

    public void setJzrq(Date jzrq) {
        this.jzrq = jzrq;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectLoanBusinessMessage that = (ProjectLoanBusinessMessage) o;

        if (id != that.id) return false;
        if (lxje != that.lxje) return false;
        if (fse != that.fse) return false;
        if (bjje != that.bjje) return false;
        if (ywlsh != null ? !ywlsh.equals(that.ywlsh) : that.ywlsh != null) return false;
        if (nyfxje != null ? !nyfxje.equals(that.nyfxje) : that.nyfxje != null) return false;
        if (yqfxje != null ? !yqfxje.equals(that.yqfxje) : that.yqfxje != null) return false;
        if (ywmxlx != null ? !ywmxlx.equals(that.ywmxlx) : that.ywmxlx != null) return false;
        if (jzrq != null ? !jzrq.equals(that.jzrq) : that.jzrq != null) return false;
        if (dkzh != null ? !dkzh.equals(that.dkzh) : that.dkzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (lxje ^ (lxje >>> 32));
        result = 31 * result + (ywlsh != null ? ywlsh.hashCode() : 0);
        result = 31 * result + (nyfxje != null ? nyfxje.hashCode() : 0);
        result = 31 * result + (int) (fse ^ (fse >>> 32));
        result = 31 * result + (int) (bjje ^ (bjje >>> 32));
        result = 31 * result + (yqfxje != null ? yqfxje.hashCode() : 0);
        result = 31 * result + (ywmxlx != null ? ywmxlx.hashCode() : 0);
        result = 31 * result + (jzrq != null ? jzrq.hashCode() : 0);
        result = 31 * result + (dkzh != null ? dkzh.hashCode() : 0);
        return result;
    }
}
