package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;


/**
 * <p>
 * 个人住房贷款逾期登记信息
 * </p>
 */
@Entity
@Table(name = "PEOPLE_HOUSING_LOAN_OVERDUE_ME")
@SequenceGenerator(name = "seqPeopleHousingLoanOverdueMe", sequenceName = "SEQ_PEO_HOU_LOAN_OVERDUE_ME", allocationSize = 1)
public class PeopleHousingLoanOverdueMe implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //逾期罚息
    private long yqfx;
    //逾期利息
    private long yqlx;
    //实收日期
    private Date ssrq;
    //逾期期次
    private long yqqc;
    //实收逾期本金金额
    private long ssyqbjje;
    //实收逾期罚息金额
    private long ssyqfxje;
    //逾期本金
    private long yqbj;
    //实收逾期利息金额
    private long ssyqlxje;
    //还款期次
    private long hkqc;
    //贷款账号
    private String dkzh;

    public PeopleHousingLoanOverdueMe(long id) {
        this.id = id;
    }

    public PeopleHousingLoanOverdueMe() {
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "YQFX")
    public long getYqfx() {
        return yqfx;
    }

    public void setYqfx(long yqfx) {
        this.yqfx = yqfx;
    }

    @Basic
    @Column(name = "YQLX")
    public long getYqlx() {
        return yqlx;
    }

    public void setYqlx(long yqlx) {
        this.yqlx = yqlx;
    }

    @Basic
    @Column(name = "SSRQ")
    public Date getSsrq() {
        return ssrq;
    }

    public void setSsrq(Date ssrq) {
        this.ssrq = ssrq;
    }

    @Basic
    @Column(name = "YQQC")
    public long getYqqc() {
        return yqqc;
    }

    public void setYqqc(long yqqc) {
        this.yqqc = yqqc;
    }

    @Basic
    @Column(name = "SSYQBJJE")
    public long getSsyqbjje() {
        return ssyqbjje;
    }

    public void setSsyqbjje(long ssyqbjje) {
        this.ssyqbjje = ssyqbjje;
    }

    @Basic
    @Column(name = "SSYQFXJE")
    public long getSsyqfxje() {
        return ssyqfxje;
    }

    public void setSsyqfxje(long ssyqfxje) {
        this.ssyqfxje = ssyqfxje;
    }

    @Basic
    @Column(name = "YQBJ")
    public long getYqbj() {
        return yqbj;
    }

    public void setYqbj(long yqbj) {
        this.yqbj = yqbj;
    }

    @Basic
    @Column(name = "SSYQLXJE")
    public long getSsyqlxje() {
        return ssyqlxje;
    }

    public void setSsyqlxje(long ssyqlxje) {
        this.ssyqlxje = ssyqlxje;
    }

    @Basic
    @Column(name = "HKQC")
    public long getHkqc() {
        return hkqc;
    }

    public void setHkqc(long hkqc) {
        this.hkqc = hkqc;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PeopleHousingLoanOverdueMe))
            return false;

        PeopleHousingLoanOverdueMe castOther = (PeopleHousingLoanOverdueMe) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .toString();
    }
}
