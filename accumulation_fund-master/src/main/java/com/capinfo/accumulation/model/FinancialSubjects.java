package com.capinfo.accumulation.model;

import javax.persistence.*;

/**财务科目信息（贯标） 未涉及业务
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "FINANCIAL_SUBJECTS")
public class FinancialSubjects {
    private long id;
    private String kmjb;
    private String kmyefx;
    private String kmsx;
    private String kmmc;
    private String dwzh;

    public FinancialSubjects(long id) {
        this.id = id;
    }

    public FinancialSubjects() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "KMJB")
    public String getKmjb() {
        return kmjb;
    }

    public void setKmjb(String kmjb) {
        this.kmjb = kmjb;
    }

    @Basic
    @Column(name = "KMYEFX")
    public String getKmyefx() {
        return kmyefx;
    }

    public void setKmyefx(String kmyefx) {
        this.kmyefx = kmyefx;
    }

    @Basic
    @Column(name = "KMSX")
    public String getKmsx() {
        return kmsx;
    }

    public void setKmsx(String kmsx) {
        this.kmsx = kmsx;
    }

    @Basic
    @Column(name = "KMMC")
    public String getKmmc() {
        return kmmc;
    }

    public void setKmmc(String kmmc) {
        this.kmmc = kmmc;
    }

    @Basic
    @Column(name = "DWZH")
    public String getDwzh() {
        return dwzh;
    }

    public void setDwzh(String dwzh) {
        this.dwzh = dwzh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialSubjects that = (FinancialSubjects) o;

        if (id != that.id) return false;
        if (kmjb != null ? !kmjb.equals(that.kmjb) : that.kmjb != null) return false;
        if (kmyefx != null ? !kmyefx.equals(that.kmyefx) : that.kmyefx != null) return false;
        if (kmsx != null ? !kmsx.equals(that.kmsx) : that.kmsx != null) return false;
        if (kmmc != null ? !kmmc.equals(that.kmmc) : that.kmmc != null) return false;
        if (dwzh != null ? !dwzh.equals(that.dwzh) : that.dwzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (kmjb != null ? kmjb.hashCode() : 0);
        result = 31 * result + (kmyefx != null ? kmyefx.hashCode() : 0);
        result = 31 * result + (kmsx != null ? kmsx.hashCode() : 0);
        result = 31 * result + (kmmc != null ? kmmc.hashCode() : 0);
        result = 31 * result + (dwzh != null ? dwzh.hashCode() : 0);
        return result;
    }
}
