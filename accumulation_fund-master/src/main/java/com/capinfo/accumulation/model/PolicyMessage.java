package com.capinfo.accumulation.model;

import javax.persistence.*;

/**
 * @Description: 政策信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:25
 */
@Entity
@Table(name = "POLICY_MESSAGE")
public class PolicyMessage {
    private long id;
    //个人住房贷款最高贷款比例 grzfdkzgdkbl
    private long grzfdkzgdkbl;
    //单位缴存比例上限
    private long dwjcblsx;
    //项目贷款最长年限
    private long xmdkzcnx;
    //单位缴存比例下限
    private long dwjcblxx;
    //个人缴存比例上限
    private long grjcblsx;
    //项目贷款最高贷款比例
    private long xmdkzgdkbl;
    //个人缴存比例下限
    private long grjcblxx;
    //月缴存额下限
    private long yjcexx;
    //个人住房贷款最高额度
    private long grzfdkzged;
    //利率类型
    private String lllx;
    //月缴存额上限
    private long yjcesx;
    //个人住房贷款最长年限
    private long grzfdkzcnx;
    //执行利率
    private long zxll;

    public PolicyMessage(long id) {
        this.id = id;
    }

    public PolicyMessage() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "GRZFDKZGDKBL")
    public long getGrzfdkzgdkbl() {
        return grzfdkzgdkbl;
    }

    public void setGrzfdkzgdkbl(long grzfdkzgdkbl) {
        this.grzfdkzgdkbl = grzfdkzgdkbl;
    }

    @Basic
    @Column(name = "DWJCBLSX")
    public long getDwjcblsx() {
        return dwjcblsx;
    }

    public void setDwjcblsx(long dwjcblsx) {
        this.dwjcblsx = dwjcblsx;
    }

    @Basic
    @Column(name = "XMDKZCNX")
    public long getXmdkzcnx() {
        return xmdkzcnx;
    }

    public void setXmdkzcnx(long xmdkzcnx) {
        this.xmdkzcnx = xmdkzcnx;
    }

    @Basic
    @Column(name = "DWJCBLXX")
    public long getDwjcblxx() {
        return dwjcblxx;
    }

    public void setDwjcblxx(long dwjcblxx) {
        this.dwjcblxx = dwjcblxx;
    }

    @Basic
    @Column(name = "GRJCBLSX")
    public long getGrjcblsx() {
        return grjcblsx;
    }

    public void setGrjcblsx(long grjcblsx) {
        this.grjcblsx = grjcblsx;
    }

    @Basic
    @Column(name = "XMDKZGDKBL")
    public long getXmdkzgdkbl() {
        return xmdkzgdkbl;
    }

    public void setXmdkzgdkbl(long xmdkzgdkbl) {
        this.xmdkzgdkbl = xmdkzgdkbl;
    }

    @Basic
    @Column(name = "GRJCBLXX")
    public long getGrjcblxx() {
        return grjcblxx;
    }

    public void setGrjcblxx(long grjcblxx) {
        this.grjcblxx = grjcblxx;
    }

    @Basic
    @Column(name = "YJCEXX")
    public long getYjcexx() {
        return yjcexx;
    }

    public void setYjcexx(long yjcexx) {
        this.yjcexx = yjcexx;
    }

    @Basic
    @Column(name = "GRZFDKZGED")
    public long getGrzfdkzged() {
        return grzfdkzged;
    }

    public void setGrzfdkzged(long grzfdkzged) {
        this.grzfdkzged = grzfdkzged;
    }

    @Basic
    @Column(name = "LLLX")
    public String getLllx() {
        return lllx;
    }

    public void setLllx(String lllx) {
        this.lllx = lllx;
    }

    @Basic
    @Column(name = "YJCESX")
    public long getYjcesx() {
        return yjcesx;
    }

    public void setYjcesx(long yjcesx) {
        this.yjcesx = yjcesx;
    }

    @Basic
    @Column(name = "GRZFDKZCNX")
    public long getGrzfdkzcnx() {
        return grzfdkzcnx;
    }

    public void setGrzfdkzcnx(long grzfdkzcnx) {
        this.grzfdkzcnx = grzfdkzcnx;
    }

    @Basic
    @Column(name = "ZXLL")
    public long getZxll() {
        return zxll;
    }

    public void setZxll(long zxll) {
        this.zxll = zxll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PolicyMessage that = (PolicyMessage) o;

        if (id != that.id) return false;
        if (grzfdkzgdkbl != that.grzfdkzgdkbl) return false;
        if (dwjcblsx != that.dwjcblsx) return false;
        if (xmdkzcnx != that.xmdkzcnx) return false;
        if (dwjcblxx != that.dwjcblxx) return false;
        if (grjcblsx != that.grjcblsx) return false;
        if (xmdkzgdkbl != that.xmdkzgdkbl) return false;
        if (grjcblxx != that.grjcblxx) return false;
        if (yjcexx != that.yjcexx) return false;
        if (grzfdkzged != that.grzfdkzged) return false;
        if (yjcesx != that.yjcesx) return false;
        if (grzfdkzcnx != that.grzfdkzcnx) return false;
        if (zxll != that.zxll) return false;
        if (lllx != null ? !lllx.equals(that.lllx) : that.lllx != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (grzfdkzgdkbl ^ (grzfdkzgdkbl >>> 32));
        result = 31 * result + (int) (dwjcblsx ^ (dwjcblsx >>> 32));
        result = 31 * result + (int) (xmdkzcnx ^ (xmdkzcnx >>> 32));
        result = 31 * result + (int) (dwjcblxx ^ (dwjcblxx >>> 32));
        result = 31 * result + (int) (grjcblsx ^ (grjcblsx >>> 32));
        result = 31 * result + (int) (xmdkzgdkbl ^ (xmdkzgdkbl >>> 32));
        result = 31 * result + (int) (grjcblxx ^ (grjcblxx >>> 32));
        result = 31 * result + (int) (yjcexx ^ (yjcexx >>> 32));
        result = 31 * result + (int) (grzfdkzged ^ (grzfdkzged >>> 32));
        result = 31 * result + (lllx != null ? lllx.hashCode() : 0);
        result = 31 * result + (int) (yjcesx ^ (yjcesx >>> 32));
        result = 31 * result + (int) (grzfdkzcnx ^ (grzfdkzcnx >>> 32));
        result = 31 * result + (int) (zxll ^ (zxll >>> 32));
        return result;
    }
}
