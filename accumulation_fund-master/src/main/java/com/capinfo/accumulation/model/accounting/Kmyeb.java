package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;

/**
 * 科目余额实体
 * @author
 *
 */
@Entity
@Table(name = "KMYEB")
public class Kmyeb implements BaseEntity {
	
	private Long id;
	private String ncjd;
	private double ncs;
	private String qcjd;
	private double qcye;
	private double bqj;
	private double bqd;
	private double bnj;
	private double bnd;
	private String yejd;
	private double ye;
	private Date inserttime;
	
	//科目名称
	private Kmszjbsj kmbh;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="kmbh" ,referencedColumnName="kmbh")
	public Kmszjbsj getKmbh() {
		return kmbh;
	}

	public void setKmbh(Kmszjbsj kmbh) {
		this.kmbh = kmbh;
	}

	@Id
	@Column(name = "ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "NCJD")
	public String getNcjd() {
		return ncjd;
	}

	public void setNcjd(String ncjd) {
		this.ncjd = ncjd;
	}

	@Basic
	@Column(name = "NCS")
	public double getNcs() {
		return ncs;
	}

	public void setNcs(double ncs) {
		this.ncs = ncs;
	}

	@Basic
	@Column(name = "QCJD")
	public String getQcjd() {
		return qcjd;
	}

	public void setQcjd(String qcjd) {
		this.qcjd = qcjd;
	}

	@Basic
	@Column(name = "QCYE")
	public double getQcye() {
		return qcye;
	}

	public void setQcye(double qcye) {
		this.qcye = qcye;
	}

	@Basic
	@Column(name = "BQJ")
	public double getBqj() {
		return bqj;
	}

	public void setBqj(double bqj) {
		this.bqj = bqj;
	}

	@Basic
	@Column(name = "BQD")
	public double getBqd() {
		return bqd;
	}

	public void setBqd(double bqd) {
		this.bqd = bqd;
	}

	@Basic
	@Column(name = "BNJ")
	public double getBnj() {
		return bnj;
	}

	public void setBnj(double bnj) {
		this.bnj = bnj;
	}

	@Basic
	@Column(name = "BND")
	public double getBnd() {
		return bnd;
	}

	public void setBnd(double bnd) {
		this.bnd = bnd;
	}

	@Basic
	@Column(name = "YEJD")
	public String getYejd() {
		return yejd;
	}

	public void setYejd(String yejd) {
		this.yejd = yejd;
	}

	@Basic
	@Column(name = "YE")
	public double getYe() {
		return ye;
	}

	public void setYe(double ye) {
		this.ye = ye;
	}

	@Basic
	@Column(name = "INSERTTIME")
	public Date getInserttime() {
		return inserttime;
	}

	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}
}
