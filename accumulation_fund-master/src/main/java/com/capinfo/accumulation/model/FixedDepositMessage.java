package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.sql.Time;

/**
 * 定期存款明细信息（贯标）未涉及业务
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "FIXED_DEPOSIT_MESSAGE")
public class FixedDepositMessage {
    private long id;
    private String zhhm;
    private String zqqk;
    private long lxsr;
    private Time dqrq;
    private Time qkrq;
    private String dqckbh;
    private long lilv;
    private Time crrq;
    private long bjje;
    private String khyhmc;
    private short ckqx;
    private String zhmc;

    public FixedDepositMessage(long id) {
        this.id = id;
    }

    public FixedDepositMessage() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ZHHM")
    public String getZhhm() {
        return zhhm;
    }

    public void setZhhm(String zhhm) {
        this.zhhm = zhhm;
    }

    @Basic
    @Column(name = "ZQQK")
    public String getZqqk() {
        return zqqk;
    }

    public void setZqqk(String zqqk) {
        this.zqqk = zqqk;
    }

    @Basic
    @Column(name = "LXSR")
    public long getLxsr() {
        return lxsr;
    }

    public void setLxsr(long lxsr) {
        this.lxsr = lxsr;
    }

    @Basic
    @Column(name = "DQRQ")
    public Time getDqrq() {
        return dqrq;
    }

    public void setDqrq(Time dqrq) {
        this.dqrq = dqrq;
    }

    @Basic
    @Column(name = "QKRQ")
    public Time getQkrq() {
        return qkrq;
    }

    public void setQkrq(Time qkrq) {
        this.qkrq = qkrq;
    }

    @Basic
    @Column(name = "DQCKBH")
    public String getDqckbh() {
        return dqckbh;
    }

    public void setDqckbh(String dqckbh) {
        this.dqckbh = dqckbh;
    }

    @Basic
    @Column(name = "LILV")
    public long getLilv() {
        return lilv;
    }

    public void setLilv(long lilv) {
        this.lilv = lilv;
    }

    @Basic
    @Column(name = "CRRQ")
    public Time getCrrq() {
        return crrq;
    }

    public void setCrrq(Time crrq) {
        this.crrq = crrq;
    }

    @Basic
    @Column(name = "BJJE")
    public long getBjje() {
        return bjje;
    }

    public void setBjje(long bjje) {
        this.bjje = bjje;
    }

    @Basic
    @Column(name = "KHYHMC")
    public String getKhyhmc() {
        return khyhmc;
    }

    public void setKhyhmc(String khyhmc) {
        this.khyhmc = khyhmc;
    }

    @Basic
    @Column(name = "CKQX")
    public short getCkqx() {
        return ckqx;
    }

    public void setCkqx(short ckqx) {
        this.ckqx = ckqx;
    }

    @Basic
    @Column(name = "ZHMC")
    public String getZhmc() {
        return zhmc;
    }

    public void setZhmc(String zhmc) {
        this.zhmc = zhmc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FixedDepositMessage that = (FixedDepositMessage) o;

        if (id != that.id) return false;
        if (lxsr != that.lxsr) return false;
        if (lilv != that.lilv) return false;
        if (bjje != that.bjje) return false;
        if (ckqx != that.ckqx) return false;
        if (zhhm != null ? !zhhm.equals(that.zhhm) : that.zhhm != null) return false;
        if (zqqk != null ? !zqqk.equals(that.zqqk) : that.zqqk != null) return false;
        if (dqrq != null ? !dqrq.equals(that.dqrq) : that.dqrq != null) return false;
        if (qkrq != null ? !qkrq.equals(that.qkrq) : that.qkrq != null) return false;
        if (dqckbh != null ? !dqckbh.equals(that.dqckbh) : that.dqckbh != null) return false;
        if (crrq != null ? !crrq.equals(that.crrq) : that.crrq != null) return false;
        if (khyhmc != null ? !khyhmc.equals(that.khyhmc) : that.khyhmc != null) return false;
        if (zhmc != null ? !zhmc.equals(that.zhmc) : that.zhmc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (zhhm != null ? zhhm.hashCode() : 0);
        result = 31 * result + (zqqk != null ? zqqk.hashCode() : 0);
        result = 31 * result + (int) (lxsr ^ (lxsr >>> 32));
        result = 31 * result + (dqrq != null ? dqrq.hashCode() : 0);
        result = 31 * result + (qkrq != null ? qkrq.hashCode() : 0);
        result = 31 * result + (dqckbh != null ? dqckbh.hashCode() : 0);
        result = 31 * result + (int) (lilv ^ (lilv >>> 32));
        result = 31 * result + (crrq != null ? crrq.hashCode() : 0);
        result = 31 * result + (int) (bjje ^ (bjje >>> 32));
        result = 31 * result + (khyhmc != null ? khyhmc.hashCode() : 0);
        result = 31 * result + (int) ckqx;
        result = 31 * result + (zhmc != null ? zhmc.hashCode() : 0);
        return result;
    }
}
