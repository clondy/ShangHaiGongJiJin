package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * <P>
 * 职工结存单（政府/ 企事业单位职工账户年度账单）
 * </p>
 */
@Entity
@Table(name = "PERSON_ANNUALBILL")
@SequenceGenerator(name = "seqPersonAnnualbill", sequenceName = "SEQ_PERSON_ANNUALBILL", allocationSize = 1)
public class PersonAnnualbill implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //个人账号
    private Long personAccumulationFundId;

    public PersonAnnualbill(Long id) {
        this.id = id;
    }

    public PersonAnnualbill() {
    }

    private PersonAccountFund personAccountFund; //个人公积金账号
    @ManyToOne(targetEntity = PersonAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCUMULATION_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund curPersonAccountFund) {
        this.personAccountFund = curPersonAccountFund;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPersonAnnualbill")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ACCUMULATION_FUND_ID")
    public Long getPersonAccumulationFundId() {
        return personAccumulationFundId;
    }

    public void setPersonAccumulationFundId(Long personAccumulationFundId) {
        this.personAccumulationFundId = personAccumulationFundId;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonAnnualbill))
            return false;

        PersonAnnualbill castOther = (PersonAnnualbill) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("personAccumulationFundId", getPersonAccumulationFundId())
                .toString();
    }


}
