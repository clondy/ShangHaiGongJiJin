package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 *  数据范围项
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@SequenceGenerator(name = "seqDataScopeItem", sequenceName = "SEQ_DATA_SCOPE_ITEM", allocationSize=1)
@Table(name = "DATA_SCOPE_ITEM")
public class DataScopeItem implements BaseEntity{
    private static final long serialVersionUID = 1L;
    private Long id;
    //父id
    private Long parentId;
    //标题
    private String title;
    //描述
    private String description;
    //是否有效
    private Boolean EnableId=true;

    private List<DataScopeSort> datascopesort;

    public DataScopeItem() {
    }

    public DataScopeItem(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqDataScopeItem")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PARENT_ID")
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Basic
    @Column(name = "IS_ENABLE")
    public Boolean getEnableId() {
        return EnableId;
    }

    public void setEnableId(Boolean enableId) {
        EnableId = enableId;
    }


    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof DataScopeItem))
            return false;

        DataScopeItem castOther = (DataScopeItem) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("title", getTitle())
                .append("description", getDescription())
                .append("EnableId", getEnableId()).toString();
    }

    @ManyToMany(targetEntity=DataScopeItem.class, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="DATE_SCOPE_SORT_ITEM", joinColumns={@JoinColumn(name="DATE_SCOPE_ITEM_ID")},inverseJoinColumns={@JoinColumn(name="DATE_SCOPE_SORT_ID")})
    @OrderBy(value = "id asc")
    public List<DataScopeSort> getDatascopesort() {
        return datascopesort;
    }

    public void setDatascopesort(List<DataScopeSort> datascopesort) {
        this.datascopesort = datascopesort;
    }
}
