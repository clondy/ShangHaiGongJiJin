package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 国债明细信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 17:10
 */
@Entity
@Table(name = "TREASURY_MESSAGE")
@SequenceGenerator(name = "seqTreasuryMessage", sequenceName = "SEQ_TREASURY_MESSAGE", allocationSize = 1)
public class TreasuryMessage implements BaseEntity {
    private Long id;
    private Date gmrq;
    private Long gmje;
    private Date qxrq;
    private String gzmc;
    private String styhdm;
    private Date dqrq;
    private Long dfbj;
    private String gzpzh;
    private String gzbh;
    private Long shuliang;
    private Long lilv;
    private Long mianzhi;
    private String gzzl;
    private String yhzhzh;
    private Long qixian;
    private Long lxsr;
    private String gmqd;
    private Date dfrq;

    public TreasuryMessage(Long id) {
        this.id = id;
    }

    public TreasuryMessage() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqTreasuryMessage")
    @Column(name = "ID")
    public Long getId() {
        //change id for finance
        //@TODO
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "GMRQ")
    public Date getGmrq() {
        return gmrq;
    }

    public void setGmrq(Date gmrq) {
        this.gmrq = gmrq;
    }

    @Basic
    @Column(name = "GMJE")
    public Long getGmje() {
        return gmje;
    }

    public void setGmje(Long gmje) {
        this.gmje = gmje;
    }

    @Basic
    @Column(name = "QXRQ")
    public Date getQxrq() {
        return qxrq;
    }

    public void setQxrq(Date qxrq) {
        this.qxrq = qxrq;
    }

    @Basic
    @Column(name = "GZMC")
    public String getGzmc() {
        return gzmc;
    }

    public void setGzmc(String gzmc) {
        this.gzmc = gzmc;
    }

    @Basic
    @Column(name = "STYHDM")
    public String getStyhdm() {
        return styhdm;
    }

    public void setStyhdm(String styhdm) {
        this.styhdm = styhdm;
    }

    @Basic
    @Column(name = "DQRQ")
    public Date getDqrq() {
        return dqrq;
    }

    public void setDqrq(Date dqrq) {
        this.dqrq = dqrq;
    }

    @Basic
    @Column(name = "DFBJ")
    public Long getDfbj() {
        return dfbj;
    }

    public void setDfbj(Long dfbj) {
        this.dfbj = dfbj;
    }

    @Basic
    @Column(name = "GZPZH")
    public String getGzpzh() {
        return gzpzh;
    }

    public void setGzpzh(String gzpzh) {
        this.gzpzh = gzpzh;
    }

    @Basic
    @Column(name = "GZBH")
    public String getGzbh() {
        return gzbh;
    }

    public void setGzbh(String gzbh) {
        this.gzbh = gzbh;
    }

    @Basic
    @Column(name = "SHULIANG")
    public Long getShuliang() {
        return shuliang;
    }

    public void setShuliang(Long shuliang) {
        this.shuliang = shuliang;
    }

    @Basic
    @Column(name = "LILV")
    public Long getLilv() {
        return lilv;
    }

    public void setLilv(Long lilv) {
        this.lilv = lilv;
    }

    @Basic
    @Column(name = "MIANZHI")
    public Long getMianzhi() {
        return mianzhi;
    }

    public void setMianzhi(Long mianzhi) {
        this.mianzhi = mianzhi;
    }

    @Basic
    @Column(name = "GZZL")
    public String getGzzl() {
        return gzzl;
    }

    public void setGzzl(String gzzl) {
        this.gzzl = gzzl;
    }

    @Basic
    @Column(name = "YHZHZH")
    public String getYhzhzh() {
        return yhzhzh;
    }

    public void setYhzhzh(String yhzhzh) {
        this.yhzhzh = yhzhzh;
    }

    @Basic
    @Column(name = "QIXIAN")
    public Long getQixian() {
        return qixian;
    }

    public void setQixian(Long qixian) {
        this.qixian = qixian;
    }

    @Basic
    @Column(name = "LXSR")
    public Long getLxsr() {
        return lxsr;
    }

    public void setLxsr(Long lxsr) {
        this.lxsr = lxsr;
    }

    @Basic
    @Column(name = "GMQD")
    public String getGmqd() {
        return gmqd;
    }

    public void setGmqd(String gmqd) {
        this.gmqd = gmqd;
    }

    @Basic
    @Column(name = "DFRQ")
    public Date getDfrq() {
        return dfrq;
    }

    public void setDfrq(Date dfrq) {
        this.dfrq = dfrq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreasuryMessage that = (TreasuryMessage) o;

        if (id != that.id) return false;
        if (gmje != that.gmje) return false;
        if (dfbj != that.dfbj) return false;
        if (shuliang != that.shuliang) return false;
        if (lilv != that.lilv) return false;
        if (mianzhi != that.mianzhi) return false;
        if (qixian != that.qixian) return false;
        if (lxsr != that.lxsr) return false;
        if (gmrq != null ? !gmrq.equals(that.gmrq) : that.gmrq != null) return false;
        if (qxrq != null ? !qxrq.equals(that.qxrq) : that.qxrq != null) return false;
        if (gzmc != null ? !gzmc.equals(that.gzmc) : that.gzmc != null) return false;
        if (styhdm != null ? !styhdm.equals(that.styhdm) : that.styhdm != null) return false;
        if (dqrq != null ? !dqrq.equals(that.dqrq) : that.dqrq != null) return false;
        if (gzpzh != null ? !gzpzh.equals(that.gzpzh) : that.gzpzh != null) return false;
        if (gzbh != null ? !gzbh.equals(that.gzbh) : that.gzbh != null) return false;
        if (gzzl != null ? !gzzl.equals(that.gzzl) : that.gzzl != null) return false;
        if (yhzhzh != null ? !yhzhzh.equals(that.yhzhzh) : that.yhzhzh != null) return false;
        if (gmqd != null ? !gmqd.equals(that.gmqd) : that.gmqd != null) return false;
        if (dfrq != null ? !dfrq.equals(that.dfrq) : that.dfrq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gmrq != null ? gmrq.hashCode() : 0);
        result = 31 * result + (int) (gmje ^ (gmje >>> 32));
        result = 31 * result + (qxrq != null ? qxrq.hashCode() : 0);
        result = 31 * result + (gzmc != null ? gzmc.hashCode() : 0);
        result = 31 * result + (styhdm != null ? styhdm.hashCode() : 0);
        result = 31 * result + (dqrq != null ? dqrq.hashCode() : 0);
        result = 31 * result + (int) (dfbj ^ (dfbj >>> 32));
        result = 31 * result + (gzpzh != null ? gzpzh.hashCode() : 0);
        result = 31 * result + (gzbh != null ? gzbh.hashCode() : 0);
        result = 31 * result + (int) (shuliang ^ (shuliang >>> 32));
        result = 31 * result + (int) (lilv ^ (lilv >>> 32));
        result = 31 * result + (int) (mianzhi ^ (mianzhi >>> 32));
        result = 31 * result + (gzzl != null ? gzzl.hashCode() : 0);
        result = 31 * result + (yhzhzh != null ? yhzhzh.hashCode() : 0);
        result = 31 * result + (int) (qixian ^ (qixian >>> 32));
        result = 31 * result + (int) (lxsr ^ (lxsr >>> 32));
        result = 31 * result + (gmqd != null ? gmqd.hashCode() : 0);
        result = 31 * result + (dfrq != null ? dfrq.hashCode() : 0);
        return result;
    }
}
