package com.capinfo.accumulation.model.collection;

import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

import java.util.Date;

/**
 * 冻结表
 * author：lishuai
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@SequenceGenerator(name = "seqFreeze", sequenceName = "SEQ_FREEZE", allocationSize=1)
@Table(name = "FREEZE")
public class Freeze  implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //个人id
    private Long personId;

    private Person person;
    //冻结类型
    private Long typeId;
    private Dictionary type;
    //冻结开始时间
    private Date startTime=new Date();
    //冻结结束时间
    private Date endTime=new Date();
    //冻结金额
    private BigDecimal amount;
    //外地法院名称
    private String court;
    //本地法院名称
    private String courtNameId;
    private Dictionary courtName;
    //法官名称
    private String judgeName;
    //联系方式
    private String contactInfo;
    //冻结原因
    private Long freezeReasonId;
    private Dictionary freezeReason;
    //解冻原因
    private Long unfreezeReasonId;
    private Dictionary unfreezeReason;
    //冻结期限
    private Long freezePeriod;
    //备注
    private String remark;
    //案件号
    private String caseNumber;
    //发生时间
    private Date dateTime=new Date();
    //银行编码
    private Long bankCode;
    //操作员
    private String operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime=new Date();
    //渠道
    private Long channels;
    //创建时间
    private String createName;
    //状态 0-申请状态；1-冻结状态；2-解冻状态； 3-解冻审核
    private Long state;

    public Freeze(Long id) {
        this.id = id;
    }

    public Freeze() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqFreeze")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


    @Basic
    @Column(name = "TYPE")
    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "TYPE", insertable = false, updatable = false)
    public Dictionary getType() {
        return type;
    }

    public void setType(Dictionary type) {
        this.type = type;
    }

    @Basic
    @Column(name = "START_TIME")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "END_TIME")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "AMOUNT")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "COURT")
    public String getCourt() {
        return court;
    }

    public void setCourt(String court) {
        this.court = court;
    }

    @Basic
    @Column(name = "COURT_NAME")
    public String getCourtNameId() {
        return courtNameId;
    }

    public void setCourtNameId(String courtNameId) {
        this.courtNameId = courtNameId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COURT_NAME", insertable = false, updatable = false)
    public Dictionary getCourtName() {
        return courtName;
    }

    public void setCourtName(Dictionary courtName) {
        this.courtName = courtName;
    }

    @Basic
    @Column(name = "JUDGE_NAME")
    public String getJudgeName() {
        return judgeName;
    }

    public void setJudgeName(String judgeName) {
        this.judgeName = judgeName;
    }
    @Basic
    @Column(name = "CONTACT_INFO")
    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }
    @Basic
    @Column(name = "FREEZE_REASON")
    public Long getFreezeReasonId() {
        return freezeReasonId;
    }

    public void setFreezeReasonId(Long freezeReasonId) {
        this.freezeReasonId = freezeReasonId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "FREEZE_REASON", insertable = false, updatable = false)
    public Dictionary getFreezeReason() {
        return freezeReason;
    }

    public void setFreezeReason(Dictionary freezeReason) {
        this.freezeReason = freezeReason;
    }

    @Basic
    @Column(name = "UNFREEZE_REASON")
    public Long getUnfreezeReasonId() {
        return unfreezeReasonId;
    }

    public void setUnfreezeReasonId(Long unfreezeReasonId) {
        this.unfreezeReasonId = unfreezeReasonId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "UNFREEZE_REASON", insertable = false, updatable = false)
    public Dictionary getUnfreezeReason() {
        return unfreezeReason;
    }

    public void setUnfreezeReason(Dictionary unfreezeReason) {
        this.unfreezeReason = unfreezeReason;
    }

    @Basic
    @Column(name = "FREEZE_PERIOD")
    public Long getFreezePeriod() {
        return freezePeriod;
    }

    public void setFreezePeriod(Long freezePeriod) {
        this.freezePeriod = freezePeriod;
    }
    @Basic
    @Column(name = "REMARK")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "CASE_NUMBER")
    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }
    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Basic
    @Column(name = "STATE")
    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof Freeze))
            return false;

        Freeze castOther = (Freeze) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("personId", getPersonId())
                .append("typeId", getTypeId())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .append("amount", getAmount())
                .append("court", getCourt())
                .append("courtName", getCourtName())
                .append("caseNumber", getCaseNumber())
                .append("dateTime", getDateTime())
                .append("bankCode", getId())
                .append("operatorId", getOperatorId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("confirmationTime", getConfirmationTime())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();
    }
}
