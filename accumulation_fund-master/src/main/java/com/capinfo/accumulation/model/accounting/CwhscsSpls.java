package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.capinfo.framework.model.BaseEntity;

/**
 * CwhscsSpls entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CWHSCS_SPLS", schema = "ECA")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_ACCOUNT_BANK_SPECIALT", allocationSize = 1)
public class CwhscsSpls implements BaseEntity {

	// Fields

	private Long id;
	private Long cwhscsxgls;
	private int sjzt;
	private String spzt;
	private String spyj;
	private String czy;
	private Date czsj;
	private String spr;
	private Date spsj;
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Basic
    @Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}
	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}
	
	@Basic
    @Column(name = "CWHSCSXGLS")
	public Long getCwhscsxgls() {
		return cwhscsxgls;
	}
	public void setCwhscsxgls(Long cwhscsxgls) {
		this.cwhscsxgls = cwhscsxgls;
	}
	@Basic
    @Column(name = "SPZT")
	public String getSpzt() {
		return spzt;
	}
	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}
	@Basic
    @Column(name = "SPYJ")
	public String getSpyj() {
		return spyj;
	}
	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}
	@Basic
    @Column(name = "CZY")
	public String getCzy() {
		return czy;
	}
	public void setCzy(String czy) {
		this.czy = czy;
	}
	@Basic
    @Column(name = "CZSJ")
	public Date getCzsj() {
		return czsj;
	}
	public void setCzsj(Date czsj) {
		this.czsj = czsj;
	}
	@Basic
    @Column(name = "SPR")
	public String getSpr() {
		return spr;
	}
	public void setSpr(String spr) {
		this.spr = spr;
	}
	@Basic
    @Column(name = "SPSJ")
	public Date getSpsj() {
		return spsj;
	}
	public void setSpsj(Date spsj) {
		this.spsj = spsj;
	}

	
}