package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "FZLXXGLSSJ")
@SequenceGenerator(name = "SEQ_FZLXXGLSSJ",sequenceName = "SEQ_FZLXXGLSSJ",allocationSize = 1)
public class Fzlxxglssj implements BaseEntity{
	
		//主键ID
		private Long id;
		//核算类型编码
		private String hslxbm;
		//核算类型名称
		private String hslxmc;
		//核算分组编码 跟核算分组里的核算分组编码关联
		private Fzlxhsfz hsfzbm;
		//数据状态
		private int sjzt;
		//插入时间
		private Date crsj;
		//核算分类
		private String hsfl;
		//帐套编号
		private String ztbh;
		//审批状态
		private int spzt;
		//核算单位
		private Dictionary hsdw;
		
		
		//操作员
		private String czy;
		//外键核算辅助类型ID
		private Long fzlxszid;
		//辅助核算类型审批流水数据对象
		private List<Fzlxsplssj> fzlxsplssjList;
		
		
		@Id
		@Column(name = "ID")
		@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_FZLXXGLSSJ")
		public Long getId() {
			return id;
		}
		
		public void setId(Long id) {
			this.id = id;
		}
		
		
	

		@Basic
		@Column(name = "HSLXMC")
		public String getHslxmc() {
			return hslxmc;
		}
		
		public void setHslxmc(String hslxmc) {
			this.hslxmc = hslxmc;
		}
		
		
		@Basic
		@Column(name = "SJZT")
		public int getSjzt() {
			return sjzt;
		}
		
		public void setSjzt(int sjzt) {
			this.sjzt = sjzt;
		}
		
		@Basic
		@Column(name = "CRSJ")
		public Date getCrsj() {
			return crsj;
		}
		
		public void setCrsj(Date crsj) {
			this.crsj = crsj;
		}
		
		@Basic
		@Column(name = "HSFL")
		public String getHsfl() {
			return hsfl;
		}
		

		public void setHsfl(String hsfl) {
			this.hsfl = hsfl;
		}
		
		@Basic
		@Column(name = "HSLXBM")
		public String getHslxbm() {
			return hslxbm;
		}

		public void setHslxbm(String hslxbm) {
			this.hslxbm = hslxbm;
		}
		//多对一的关联注解
		@ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name="hsfzbm" ,referencedColumnName="hsfzbm" )
		public Fzlxhsfz getHsfzbm() {
			return hsfzbm;
		}

		public void setHsfzbm(Fzlxhsfz hsfzbm) {
			this.hsfzbm = hsfzbm;
		}
		@Basic
		@Column(name = "ZTBH")
		public String getZtbh() {
			return ztbh;
		}

		public void setZtbh(String ztbh) {
			this.ztbh = ztbh;
		}
		@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	    @JoinColumn(name="HSDW" ,referencedColumnName="ID" )
		public Dictionary getHsdw() {
			return hsdw;
		}

		public void setHsdw(Dictionary hsdw) {
			this.hsdw = hsdw;
		}

		@Basic
		@Column(name = "CZY")
		public String getCzy() {
			return czy;
		}

		public void setCzy(String czy) {
			this.czy = czy;
		}

		@Basic
		@Column(name = "FZLXSZID")
		public Long getFzlxszid() {
			return fzlxszid;
		}

		public void setFzlxszid(Long fzlxszid) {
			this.fzlxszid = fzlxszid;
		}
		
		@Basic
		@Column(name = "SPZT")
		public int getSpzt() {
			return spzt;
		}

		public void setSpzt(int i) {
			this.spzt = i;
		}
		//一对多的关联
		@OneToMany(mappedBy="hsspid" ,fetch =FetchType.LAZY )   
		@OrderBy(clause="id DESC")
		public List<Fzlxsplssj> getFzlxsplssjList() {
			return fzlxsplssjList;
		}

		public void setFzlxsplssjList(List<Fzlxsplssj> fzlxsplssjList) {
			this.fzlxsplssjList = fzlxsplssjList;
		}



}
