package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "FZLXSPLSSJ")
@SequenceGenerator(name = "SEQ_FZLXSPLSSJ",sequenceName = "SEQ_FZLXSPLSSJ",allocationSize = 1)
public class Fzlxsplssj implements BaseEntity{
	//主键ID
	private Long id;
	//外键核算审批ID
	private Long hsspid;
	//数据状态
	private int sjzt;
	//审批状态
	private int spzt;
	//审批意见
	private String spyj;
	//操作员
	private String czy;
	//操作时间
	private Date czsj;
	//审批人
	private String spr;
	//审批时间
	private Date spsj;
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_FZLXSPLSSJ")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Basic
	@Column(name = "FZLXXGID")
	public Long getHsspid() {
		return hsspid;
	}
	public void setHsspid(Long hsspid) {
		this.hsspid = hsspid;
	}
	@Basic
	@Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}
	public void setSjzt(int i) {
		this.sjzt = i;
	}
	@Basic
	@Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}
	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	@Basic
	@Column(name = "SPYJ")
	public String getSpyj() {
		return spyj;
	}
	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}
	@Basic
	@Column(name = "CZY")
	public String getCzy() {
		return czy;
	}
	public void setCzy(String czy) {
		this.czy = czy;
	}
	@Basic
	@Column(name = "CZSJ")
	public Date getCzsj() {
		return czsj;
	}
	public void setCzsj(Date czsj) {
		this.czsj = czsj;
	}
	@Basic
	@Column(name = "SPR")
	public String getSpr() {
		return spr;
	}
	public void setSpr(String spr) {
		this.spr = spr;
	}
	@Basic
	@Column(name = "SPSJ")
	public Date getSpsj() {
		return spsj;
	}
	public void setSpsj(Date spsj) {
		this.spsj = spsj;
	}
	
	

}
