package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;

/**
 * 
 * 科目汇总表
 *
 */
@Entity
@Table(name = "KEMUHZB" )
@SequenceGenerator(name = "seqkemuhzb", sequenceName = "SEQ_KEMUHZB", allocationSize = 1)
public class Kemuhzb implements BaseEntity{
	//ID
	private Long id;
	//科目名称
	private Kmszjbsj kmmc;
	//期初余额
	private double qcye;
	//借方发生额
	private double jffse;
	//贷方发生额
	private double dffse;
	//余额
	private double yue;
	//统计年月
	private Date riqi;
	
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqkemuhzb")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="kmmc" ,referencedColumnName="kmbh")
	public Kmszjbsj getKmmc() {
		return kmmc;
	}
	public void setKmmc(Kmszjbsj kmmc) {
		this.kmmc = kmmc;
	}
	@Basic
    @Column(name = "QCYE")
	public double getQcye() {
		return qcye;
	}
	public void setQcye(double qcye) {
		this.qcye = qcye;
	}
	@Basic
    @Column(name = "JFFSE")
	public double getJffse() {
		return jffse;
	}
	public void setJffse(double jffse) {
		this.jffse = jffse;
	}
	@Basic
    @Column(name = "DFFSE")
	public double getDffse() {
		return dffse;
	}
	public void setDffse(double dffse) {
		this.dffse = dffse;
	}
	@Basic
    @Column(name = "YUE")
	public double getYue() {
		return yue;
	}
	public void setYue(double yue) {
		this.yue = yue;
	}
	@Basic
    @Column(name = "RIQI")
	public Date getRiqi() {
		return riqi;
	}
	public void setRiqi(Date riqi) {
		this.riqi = riqi;
	}
}
