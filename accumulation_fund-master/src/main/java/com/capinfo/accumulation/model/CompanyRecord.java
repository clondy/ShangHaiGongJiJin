package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Rexxxar on 2017/9/19.
 *机关/ 企事业单位的员工的就业履历
 * @author fengjingsen
 */
@Entity
@Table(name = "COMPANY_RECORD")
@SequenceGenerator(name = "seqCompanyRecord", sequenceName = "SEQ_COMPANY_RECORD", allocationSize = 1)
public class CompanyRecord implements BaseEntity {
    //ID
    private Long id;
    //单位_ID
    private Long companyId;
    //个人信息_id
    private Long personId;
    //迁移日期
    private Date migrationDate=new Date();
    //发生时间
    private Date dateTime=new Date();
    //银行编码
    private Long bankCode;
    //操作员
    private String operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime=new Date();
    //渠道
    private Long channels;
    //创建人名称
    private String createName;

    private Company company;

    private Person person;

    public CompanyRecord(Long id) {
        this.id = id;
    }

    public CompanyRecord() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCompanyRecord")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "MIGRATION_DATE")
    public Date getMigrationDate() {
        return migrationDate;
    }

    public void setMigrationDate(Date migrationDate) {
        this.migrationDate = migrationDate;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyId", getCompanyId())
                .append("personId", getPersonId())
                .append("migrationDate", getMigrationDate())
                .append("dateTime", getDateTime())
                .append("operatorId", getOperatorId())
                .append("regionId", getRegionId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("confirmationTime", getConfirmationTime())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof CompanyRecord))
            return false;
        CompanyRecord otherCompanyRecord = (CompanyRecord) other;
        return new EqualsBuilder().append(this.getId(),
                otherCompanyRecord.getId()).isEquals();
    }

    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
