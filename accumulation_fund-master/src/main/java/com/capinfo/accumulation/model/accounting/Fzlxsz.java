package com.capinfo.accumulation.model.accounting;
/**
 * 辅助类型设置基本数据
 */
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "FZLXJBSJ")
@SequenceGenerator(name = "SEQ_FZLXJBSJ", sequenceName = "SEQ_FZLXJBSJ", allocationSize = 1)
public class Fzlxsz implements BaseEntity {
	//主键ID
	private Long id;
	//核算类型编码
	private String hslxbm;
	//核算类型名称
	private String hslxmc;
	//核算分组编码 跟核算分组里的核算分组编码关联
	private Fzlxhsfz hsfzbm;
	//数据状态
	private int sjzt;
	//插入时间
	private Date crsj;
	//核算分类
	private String hsfl;
	//帐套编号
	private String ztbh;
	//审批状态
	private int spzt;
	//核算单位
	private Dictionary hsdw;
	
	//核算辅助类型修改流水数据对象
	private List<Fzlxxglssj> fzlxxglssjList;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_FZLXJBSJ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	@Basic
	@Column(name = "HSLXMC")
	public String getHslxmc() {
		return hslxmc;
	}
	
	public void setHslxmc(String hslxmc) {
		this.hslxmc = hslxmc;
	}
	
	
	@Basic
	@Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}
	
	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}
	
	@Basic
	@Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}
	
	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	
	@Basic
	@Column(name = "HSFL")
	public String getHsfl() {
		return hsfl;
	}
	
	public void setHsfl(String hsfl) {
		this.hsfl = hsfl;
	}
	
	@Basic
	@Column(name = "HSLXBM")
	public String getHslxbm() {
		return hslxbm;
	}

	public void setHslxbm(String hslxbm) {
		this.hslxbm = hslxbm;
	}
	//多对一的关联注解
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hsfzbm" ,referencedColumnName="hsfzbm" )
	public Fzlxhsfz getHsfzbm() {
		return hsfzbm;
	}

	public void setHsfzbm(Fzlxhsfz hsfzbm) {
		this.hsfzbm = hsfzbm;
	}
	@Basic
	@Column(name = "ZTBH")
	public String getZtbh() {
		return ztbh;
	}

	public void setZtbh(String ztbh) {
		this.ztbh = ztbh;
	}
	//这是多对一的关联
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="HSDW" ,referencedColumnName="ID" )
	public Dictionary getHsdw() {
		return hsdw;
	}

	public void setHsdw(Dictionary hsdw) {
		this.hsdw = hsdw;
	}
	@Basic
	@Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}

	public void setSpzt(int i) {
		this.spzt = i;
	}

	//一对多的关联
	@OneToMany(mappedBy="fzlxszid" ,fetch =FetchType.LAZY )   
	@OrderBy(clause="id DESC")
	public List<Fzlxxglssj> getFzlxxglssjList() {
		return fzlxxglssjList;
	}

	public void setFzlxxglssjList(List<Fzlxxglssj> fzlxxglssjList) {
		this.fzlxxglssjList = fzlxxglssjList;
	}


	
}
