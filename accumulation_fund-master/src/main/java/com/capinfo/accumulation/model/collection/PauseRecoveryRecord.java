package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.enums.Channel;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Comment 停缴/ 启封登记（复缴/ 停缴记录）
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "PAUSE_RECOVERY_RECORD")
@SequenceGenerator(name = "seqPAUSE_RECOVERY_RECORD", sequenceName = "SEQ_PAUSE_RECOVERY_RECORD", allocationSize = 1)

public class PauseRecoveryRecord implements BaseEntity {


    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    //个人公积金_id
    private Long personAccountFundId;
    private PersonAccountFund personAccountFund = new PersonAccountFund();

    //单位公积金_id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund = new CompanyAccountFund();

    //停复缴清册ID
    private Long pauseRecoveryInventoryId;
    private PauseRecoveryInventory pauseRecoveryInventory = new PauseRecoveryInventory();

    //业务类型 1:停缴 2:复缴
    private int status;

    //停缴启封原因
    private Long reasonId;

    //停缴启封原因字典
    private Dictionary reason;

    //停缴自
    private Date stopFrom;

    //停缴至
    private Date stopTo;

    //*工资
    private BigDecimal wage;

    //*单位缴存比例
    private int companyPaymentRatio;

    //*个人缴存比例
    private int personPaymentRatio;

    //*月缴存额
    private BigDecimal monthPayment;

    //*余额
    private BigDecimal balance;

    //创建时间
    private Date createTime = new Date();

    //操作员
    private Long operatorId;

    private SystemUser operator;

    //区县
    private Long regionId;

    //网点
    private Long siteId;

    //渠道
    private int channels = Channel.中心网点.getToken();

    public PauseRecoveryRecord(){

    }

    public PauseRecoveryRecord(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPAUSE_RECOVERY_RECORD")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }

    @ManyToOne(targetEntity = PersonAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)

    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }



    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }



    @ManyToOne(targetEntity = CompanyAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "PAUSE_RECOVERY_INVENTORY_ID")
    public Long getPauseRecoveryInventoryId() {
        return pauseRecoveryInventoryId;
    }

    public void setPauseRecoveryInventoryId(Long pauseRecoveryInventoryId) {
        this.pauseRecoveryInventoryId = pauseRecoveryInventoryId;
    }

    @ManyToOne(targetEntity = PauseRecoveryInventory.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PAUSE_RECOVERY_INVENTORY_ID",insertable = false, updatable = false)
    public PauseRecoveryInventory getPauseRecoveryInventory() {
        return pauseRecoveryInventory;
    }

    public void setPauseRecoveryInventory(PauseRecoveryInventory pauseRecoveryInventory) {
        this.pauseRecoveryInventory = pauseRecoveryInventory;
    }

    @Basic
    @Column(name = "STATUS")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    @Basic
    @Column(name = "REASON_ID")
    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REASON_ID", insertable = false, updatable = false)
    public Dictionary getReason() {
        return reason;
    }

    public void setReason(Dictionary reason) {
        this.reason = reason;
    }



    @Basic
    @Column(name = "STOP_FROM")
    public Date getStopFrom() {
        return stopFrom;
    }

    public void setStopFrom(Date stopFrom) {
        this.stopFrom = stopFrom;
    }

    @Basic
    @Column(name = "STOP_TO")
    public Date getStopTo() {
        return stopTo;
    }

    public void setStopTo(Date stopTo) {
        this.stopTo = stopTo;
    }


    @Basic
    @Column(name = "WAGE")
    public BigDecimal getWage() {
        return wage;
    }

    public void setWage(BigDecimal wage) {
        this.wage = wage;
    }


    @Basic
    @Column(name = "COMPANY_PAYMENT_RATIO")
    public int getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(int companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "PERSON_PAYMENT_RATIO")
    public int getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(int personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }






    @Basic
    @Column(name = "MONTH_PAYMENT")
    public BigDecimal getMonthPayment() {
        return monthPayment;
    }


    public void setMonthPayment(BigDecimal monthPayment) {
        this.monthPayment = monthPayment;
    }

    @Basic
    @Column(name = "BALANCE")
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Basic
    @Column(name = "CREATE_TIME")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "CHANNELS")
    public int getChannels() {
        return channels;
    }

    public void setChannels(int channels) {
        this.channels = channels;
    }





    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof PauseRecoveryRecord))
            return false;
        PauseRecoveryRecord castOther = (PauseRecoveryRecord)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }


}
