package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 交易合同（个人与公积金中心签订的各种合同） 参考 委托收款,自愿缴费
 * @Author: zhangxu
 * @Date 2017/9/20 14:38
 */
@Entity
@Table(name = "TRANSACTION_CONTRACT")
@SequenceGenerator(name = "seqTransactionContract", sequenceName = "SEQ_TRANSACTION_CONTRACT", allocationSize = 1)

public class TransactionContract implements BaseEntity {
    private static final long serialVersionUID = 6078810692082392636L;
    private Long id;
    //个人信息
    private Long personId;
    private Person person;
    //合同类型
    private Long contractTypeId;
    private Dictionary contractType;
    //操作时间
    private Date dateTime;
    //银行编码
    private Long bankCode;
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //渠道
    private Long channels;
    //创建人名称
    private String createName;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime;

    public TransactionContract(Long id) {
        this.id = id;
    }

    public TransactionContract() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqTransactionContract")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Basic
    @Column(name = "CONTRACT_TYPE")
    public Long getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Long contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

      @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONTRACT_TYPE" , insertable = false, updatable = false)
    public Dictionary getContractType() {
        return contractType;
    }

    public void setContractType(Dictionary contractType) {
        this.contractType = contractType;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID" , insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }


    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("个人信息Id", this.getPersonId())
                .append("合同类型Id", this.getBankCode())
                .append("复核人", this.getConfirmerId())
                .append("复核时间", this.getConfirmationTime())
                .append("操作时间", this.getDateTime())
                .append("银行编码", this.getBankCode())
                .append("操作员Id", this.getOperatorId())
                .append("区县id", this.getRegionId())
                .append("网点id", this.getSiteId())
                .append("渠道", this.getChannels())
                .append("创建人名称", this.getCreateName())


                .toString();
    }

    public boolean equals(Object other) {
        if ((this == other)) {

            return true;
        }
        if (!(other instanceof TransactionContract)) {

            return false;
        }

        TransactionContract caseOther = (TransactionContract) other;
        return new EqualsBuilder().append(this.getId(),
                caseOther.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
