package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.PayinSupplementInfo;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.sql.Time;

/**
 * <p>
 * 补交信息与员工公积金的关联信息
 * </p>
 */
@Entity
@Table(name = "PAYIN_SUPPLEMENT_ITEM")
@SequenceGenerator(name = "seqPayinSupplementItem", sequenceName = "SEQ_PAYIN_SUPPLEMENT_ITEM", allocationSize = 1)
public class PayinSupplementItem {
    private long id;
    //个人公积金_id
    private Long personAccountFundId;

    private PersonAccountFund personAccountFund;

    //补缴核定_id
    private Long payinSupplementInfoId;

    private PayinSupplementInfo payinSupplementInfo;

    //补缴金额
    private Long amount;
    //补缴月份
    private Time month;

    public PayinSupplementItem(long id) {
        this.id = id;
    }

    public PayinSupplementItem() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }


    @ManyToOne(targetEntity = PersonAccountFund.class)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    @Basic
    @Column(name = "PAYIN_SUPPLEMENT_INFO_ID")
    public Long getPayinSupplementInfoId() {
        return payinSupplementInfoId;
    }

    public void setPayinSupplementInfoId(Long payinSupplementInfoId) {
        this.payinSupplementInfoId = payinSupplementInfoId;
    }

    @ManyToOne(targetEntity = PayinSupplementInfo.class)
    @JoinColumn(name = "PAYIN_SUPPLEMENT_INFO_ID", insertable = false, updatable = false)
    public PayinSupplementInfo getPayinSupplementInfo() {
        return payinSupplementInfo;
    }

    public void setPayinSupplementInfo(PayinSupplementInfo payinSupplementInfo) {
        this.payinSupplementInfo = payinSupplementInfo;
    }

    @Basic
    @Column(name = "AMOUNT")
    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "MONTH")
    public Time getMonth() {
        return month;
    }

    public void setMonth(Time month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PayinSupplementItem that = (PayinSupplementItem) o;

        if (id != that.id) return false;
        if (personAccountFundId != null ? !personAccountFundId.equals(that.personAccountFundId) : that.personAccountFundId != null)
            return false;
        if (payinSupplementInfoId != null ? !payinSupplementInfoId.equals(that.payinSupplementInfoId) : that.payinSupplementInfoId != null)
            return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (personAccountFundId != null ? personAccountFundId.hashCode() : 0);
        result = 31 * result + (payinSupplementInfoId != null ? payinSupplementInfoId.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (month != null ? month.hashCode() : 0);
        return result;
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .toString();
    }
}
