package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 *数据范围分类
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@SequenceGenerator(name = "seqDataScopeSort", sequenceName = "SEQ_DATA_SCOPE_SORT", allocationSize=1)
@Table(name = "DATA_SCOPE_SORT")
public class DataScopeSort implements BaseEntity{
    private static final long serialVersionUID = 1L;
    private Long id;
    //标题
    private String title;
    //描述
    private String description;
    //是否有效
    private Boolean EnableId=true;
    private List<DataScopeItem> datascopeitem;

    public DataScopeSort(Long id) {
        this.id = id;
    }

    public DataScopeSort() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqDataScopeSort")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "IS_ENABLE")

    public Boolean getEnableId() {
        return EnableId;
    }

    public void setEnableId(Boolean enableId) {
        this.EnableId = enableId;
    }


    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof DataScopeSort))
            return false;

        DataScopeSort castOther = (DataScopeSort) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("title", getTitle())
                .append("description", getDescription())
                .append("EnableId", getEnableId()).toString();
    }
    @ManyToMany(targetEntity=DataScopeSort.class, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="DATE_SCOPE_SORT_ITEM", joinColumns={@JoinColumn(name="DATE_SCOPE_SORT_ID")},inverseJoinColumns={@JoinColumn(name="DATE_SCOPE_ITEM_ID")})
    @OrderBy(value = "id asc")
    public List<DataScopeItem> getDatascopeitem() {
        return datascopeitem;
    }

    public void setDatascopeitem(List<DataScopeItem> datascopeitem) {
        this.datascopeitem = datascopeitem;
    }
}
