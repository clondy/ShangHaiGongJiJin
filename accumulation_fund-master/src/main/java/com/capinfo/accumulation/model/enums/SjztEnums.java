package com.capinfo.accumulation.model.enums;

/**
 * 数据状态
 */
public enum SjztEnums {
	新增审批中(1), 正常使用(2), 停用审批中(3), 禁用中(4);
	// 成员变量
	private int token;
	private SjztEnums name;

	// 构造方法
	private SjztEnums(int token) {
		
		this.token = token;
	}

	public int getToken() {
		
		return token;
	}

	public SjztEnums getName() {
		
		name= SjztEnums.getObject(this.token);
		return name;
	}

	public static SjztEnums getObject(int token) {

		switch (token) {
		case 1:
			return 新增审批中;
		case 2:
			return 正常使用;
		case 3:
			return 停用审批中;
		case 4:
			return 禁用中;
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(SjztEnums.新增审批中.getToken());
	}
}