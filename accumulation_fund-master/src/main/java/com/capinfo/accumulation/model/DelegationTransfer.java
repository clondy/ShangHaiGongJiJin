package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;


/**
 * 委托转移信息表
 * author：lishuai
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@SequenceGenerator(name = "seqDelegationTransfer", sequenceName = "SEQ_DELEGATION_TRANSFER", allocationSize=1)
@Table(name = "DELEGATION_TRANSFER")
public class DelegationTransfer implements BaseEntity{
    private static final long serialVersionUID = 1L;
    private Long id;
    //单位id
    private Long companyId;
    private Company company;
    //个人id
    private Long personId;
    private Person person;
    //委托转移原因
    private String reasonId;
    private Dictionary reason;
    //办理日期
    private Date dateTime=new Date();
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //复核人
    private Long confirmerId;;
    private SystemUser confirmer;
    //复核时间
    private Date reviewTime=new Date();
    //网点
    private Long siteId;
    //银行编码
    private String bankCode;
    //区县
    private Long regionId;
    private Region region;
    //渠道
    private Long channels;
    //创建人姓名
    private String createName;

    public DelegationTransfer(Long id) {
        this.id = id;
    }

    public DelegationTransfer() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqDelegationTransfer")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "REASON")

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "IS_ENABLE", insertable = false, updatable = false)
    public Dictionary getReason() {
        return reason;
    }
    public void setReason(Dictionary reason) {
        this.reason = reason;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SYSTEMUSER_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SYSTEMUSER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "REVIEW_TIME")
    public Date getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Date reviewTime) {
        this.reviewTime = reviewTime;
    }

    @Basic
    @Column(name = "SITE_ID")

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }



    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof DelegationTransfer))
            return false;

        DelegationTransfer castOther = (DelegationTransfer) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyId", getCompanyId())
                .append("personId", getPersonId())
                .append("reasonId", getReasonId())
                .append("dateTime", getDateTime())
                .append("operatorId", getOperatorId())
                .append("confirmerId", getConfirmerId())
                .append("reviewTime", getReviewTime())
                .append("siteId", getSiteId())
                .append("bankCode", getBankCode())
                .append("regionId", getReasonId())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();

    }
    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }
}
