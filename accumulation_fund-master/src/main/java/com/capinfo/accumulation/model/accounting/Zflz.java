package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * 
 * 总分类账
 *
 */
@Entity
@Table(name = "ZFLZ" )
@SequenceGenerator(name = "seqZflz", sequenceName = "SEQ_ZFLZ", allocationSize = 1)
public class Zflz implements BaseEntity{
	//ID
	private Long id;
	//科目
	private Kmszjbsj kmmc;
	//日期
	private Date riqi;
	//摘要
	private String zhaiyao;
	//借方发生额
	private double jffse;
	//贷方发生额
	private double dffse;
	//余额方向
	private Dictionary yefx;
	//余额
	private double yue;
	//年度
	private Dictionary kjnd;
	

	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="KJND" ,referencedColumnName="ID" )
 	public Dictionary getKjnd() {
		return kjnd;
	}

	public void setKjnd(Dictionary kjnd) {
		this.kjnd = kjnd;
	}


	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqZflz")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	@Basic
    @Column(name = "RIQI")
	public Date getRiqi() {
		return riqi;
	}

	public void setRiqi(Date riqi) {
		this.riqi = riqi;
	}

	@Basic
    @Column(name = "ZHAIYAO")
	public String getZhaiyao() {
		return zhaiyao;
	}

	public void setZhaiyao(String zhaiyao) {
		this.zhaiyao = zhaiyao;
	}

	@Basic
    @Column(name = "JFFSE")
	public double getJffse() {
		return jffse;
	}

	public void setJffse(double jffse) {
		this.jffse = jffse;
	}

	@Basic
    @Column(name = "DFFSE")
	public double getDffse() {
		return dffse;
	}

	public void setDffse(double dffse) {
		this.dffse = dffse;
	}

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="YEFX" ,referencedColumnName="ID" )
	public Dictionary getYefx() {
		return yefx;
	}

	public void setYefx(Dictionary yefx) {
		this.yefx = yefx;
	}

	@Basic
    @Column(name = "YUE")
	public double getYue() {
		return yue;
	}

	public void setYue(double yue) {
		this.yue = yue;
	}
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="kmmc" ,referencedColumnName="kmbh" )
	public Kmszjbsj getKmmc() {
		return kmmc;
	}

	public void setKmmc(Kmszjbsj kmmc) {
		this.kmmc = kmmc;
	}
	
	

}
