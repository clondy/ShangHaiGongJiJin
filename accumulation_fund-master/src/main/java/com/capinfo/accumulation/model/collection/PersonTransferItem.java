package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * @Description: 清册项
 * @Author: zhangxu
 * @Date 2017/9/20 14:03
 */
@Entity
@Table(name = "PERSON_TRANSFER_ITEM")
@SequenceGenerator(name = "seqPersonTransferItem", sequenceName = "SEQ_PERSON_TRANSFER_ITEM", allocationSize = 1)

public class PersonTransferItem implements BaseEntity {
    private static final long serialVersionUID = -6033786823239263638L;
    private Long id;
    //个人id
    private Long personId;
    private Person person;
    //清册id
    private Long personTransferInfoId;
    private PersonTransferInfo personTransferInfo;

    //转出单位账号
    private String turnOutCompanyAccount;
    //转入单位账号
    private String turnInCompanyAccount;


    public PersonTransferItem(Long id) {
        this.id = id;
    }

    public PersonTransferItem() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPersonTransferItem")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Basic
    @Column(name = "PERSON_TRANSFER_INFO_ID")
    public Long getPersonTransferInfoId() {
        return personTransferInfoId;
    }

    public void setPersonTransferInfoId(Long personTransferInfoId) {
        this.personTransferInfoId = personTransferInfoId;
    }

    @ManyToOne(targetEntity = PersonTransferInfo.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_TRANSFER_INFO_ID", insertable = false, updatable = false)
    public PersonTransferInfo getPersonTransferInfo() {
        return personTransferInfo;
    }

    public void setPersonTransferInfo(PersonTransferInfo personTransferInfo) {
        this.personTransferInfo = personTransferInfo;
    }
    @Basic
    @Column(name = "TURN_OUT_COMPANYACCOUNT")
    public String getTurnOutCompanyAccount() {
        return turnOutCompanyAccount;
    }

    public void setTurnOutCompanyAccount(String turnOutCompanyAccount) {
        this.turnOutCompanyAccount = turnOutCompanyAccount;
    }
    @Basic
    @Column(name = "TURN_IN_COMPANYACCOUNT")
    public String getTurnInCompanyAccount() {
        return turnInCompanyAccount;
    }

    public void setTurnInCompanyAccount(String turnInCompanyAccount) {
        this.turnInCompanyAccount = turnInCompanyAccount;
    }


    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("个人id", getPersonId())
                .append("清册id", getPersonTransferInfoId())
                .append("转出单位账号", getTurnOutCompanyAccount())
                .append("转入单位账号", getTurnInCompanyAccount())
                .toString();
    }

    public boolean equals(Object other) {
        if ((this == other)) {

            return true;
        }
        if (!(other instanceof PersonTransferItem)) {

            return false;
        }

        PersonTransferItem caseOther = (PersonTransferItem) other;
        return new EqualsBuilder().append(this.getId(),
                caseOther.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }


}
