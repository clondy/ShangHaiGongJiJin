package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @Description: USER_GGJ
 * @Author: zhangxu
 * @Date 2017/9/20 18:31
 */
@Entity
@Table(name = "USER_GJJ")
@SequenceGenerator(name = "seqUserGjj", sequenceName = "SEQ_USER_GJJ", allocationSize = 1)

public class USER_GGJ implements BaseEntity {
    private static final long serialVersionUID = 7157609492200356930L;
    private Long id;
    //交易流水id
    private Long transactionRecordId;
    private TransactionRecord transactionRecord;
    //单位公积金id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund;
    //员工公积金id
    private Long personAccountFundId;
    private PersonAccountFund personAccountFund;
    //金额
    private BigDecimal amount;

    public USER_GGJ(Long id) {
        this.id = id;
    }

    public USER_GGJ() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqUserGjj")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TRANSACTION_RECORD_ID")
    public Long getTransactionRecordId() {
        return transactionRecordId;
    }

    public void setTransactionRecordId(Long transactionRecordId) {
        this.transactionRecordId = transactionRecordId;
    }

    @ManyToOne(targetEntity = TransactionRecord.class , cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "TRANSACTION_RECORD_ID", insertable = false, updatable = false)
    public TransactionRecord getTransactionRecord() {
        return transactionRecord;
    }

    public void setTransactionRecord(TransactionRecord transactionRecord) {
        this.transactionRecord = transactionRecord;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @ManyToOne(targetEntity = CompanyAccountFund.class , cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }

    @ManyToOne(targetEntity = PersonAccountFund.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    @Basic
    @Column(name = "AMOUNT")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("交易流水id", this.getCompanyAccountFundId())
                .append("单位公积金id", this.getPersonAccountFundId())
                .append("员工公积金id", this.getTransactionRecordId())
                .append("金额", this.getAmount())
                .toString();
    }

    public boolean equals(Object other) {
        if ((this == other)) {

            return true;
        }
        if (!(other instanceof USER_GGJ)) {

            return false;
        }

        USER_GGJ caseOther = (USER_GGJ) other;
        return new EqualsBuilder().append(this.getId(),
                caseOther.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
