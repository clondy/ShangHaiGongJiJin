package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * 单位公积金 author:lishuai Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "ZTJBSJ")
@SequenceGenerator(name = "SEQ_ZTJBSJ", sequenceName = "SEQ_ZTJBSJ", allocationSize = 1)
public class Ztsz implements BaseEntity {
	// id
	private Long id;
	//账套名称
	private String ztmc;
	//账套代码
	private Long ztdm;
	//账套描述
	private String ztms;
	//数据状态
	private int sjzt;
	//插入时间
	private Date crsj;
	//核算单位
	private Dictionary hsdw;
	//资金类型
	private Dictionary zjlx;
	//组织机构
	private Dictionary zzjg;
	//审批状态
	private int spzt;
	
	private List<Ztxglssj> ztxglssjList;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="HSDW" ,referencedColumnName="ID" )
	public Dictionary getHsdw() {
		return hsdw;
	}

	public void setHsdw(Dictionary hsdw) {
		this.hsdw = hsdw;
	}

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ZJLX" ,referencedColumnName="ID" )
	public Dictionary getZjlx() {
		return zjlx;
	}

	public void setZjlx(Dictionary zjlx) {
		this.zjlx = zjlx;
	}

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ZZJG" ,referencedColumnName="ID" )
	public Dictionary getZzjg() {
		return zzjg;
	}

	public void setZzjg(Dictionary zzjg) {
		this.zzjg = zzjg;
	}
    
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ZTJBSJ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	@Basic
	@Column(name = "ZTMC")
	public String getZtmc() {
		return ztmc;
	}

	public void setZtmc(String ztmc) {
		this.ztmc = ztmc;
	}

	@Basic
	@Column(name = "ZTDM")
	public Long getZtdm() {
		return ztdm;
	}

	public void setZtdm(Long ztdm) {
		this.ztdm = ztdm;
	}

	@Basic
	@Column(name = "ZTMS")
	public String getZtms() {
		return ztms;
	}

	public void setZtms(String ztms) {
		this.ztms = ztms;
	}
	@Basic
	@Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}

	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}

	@Basic
	@Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}

	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	@Basic
	@Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}

	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	
	@OneToMany(mappedBy="ztid" ,fetch =FetchType.LAZY )   
	@OrderBy(clause="id DESC")
	public List<Ztxglssj> getZtxglssjList() {
		return ztxglssjList;
	}
	
	public void setZtxglssjList(List<Ztxglssj> ztxglssjList) {
		this.ztxglssjList = ztxglssjList;
	}
	
	
}
