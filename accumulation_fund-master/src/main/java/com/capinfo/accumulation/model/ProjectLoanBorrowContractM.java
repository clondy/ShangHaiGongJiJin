package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 项目贷款借款合同信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:36
 */
@Entity
@Table(name = "PROJECT_LOAN_BORROW_CONTRACT_M")
public class ProjectLoanBorrowContractM {
    private long id;
    private String dkfffs;
    private String jkr;
    private long nyfxll;
    private String xmbh;
    private long dkje;
    private String zjjgzhhm;
    private Date wtrqysj;
    private String jkhtbh;
    private String yhdm;
    private long dkqx;
    private Date dkrqysj;
    private long yqfxll;
    private String dkrqydb;
    private String dkr;
    private Date jkrqysj;
    private String jkrqydb;
    private String wtrqydb;
    private String wtr;
    private String dkhbfs;
    private long dkll;

    public ProjectLoanBorrowContractM(long id) {
        this.id = id;
    }

    public ProjectLoanBorrowContractM() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DKFFFS")
    public String getDkfffs() {
        return dkfffs;
    }

    public void setDkfffs(String dkfffs) {
        this.dkfffs = dkfffs;
    }

    @Basic
    @Column(name = "JKR")
    public String getJkr() {
        return jkr;
    }

    public void setJkr(String jkr) {
        this.jkr = jkr;
    }

    @Basic
    @Column(name = "NYFXLL")
    public long getNyfxll() {
        return nyfxll;
    }

    public void setNyfxll(long nyfxll) {
        this.nyfxll = nyfxll;
    }

    @Basic
    @Column(name = "XMBH")
    public String getXmbh() {
        return xmbh;
    }

    public void setXmbh(String xmbh) {
        this.xmbh = xmbh;
    }

    @Basic
    @Column(name = "DKJE")
    public long getDkje() {
        return dkje;
    }

    public void setDkje(long dkje) {
        this.dkje = dkje;
    }

    @Basic
    @Column(name = "ZJJGZHHM")
    public String getZjjgzhhm() {
        return zjjgzhhm;
    }

    public void setZjjgzhhm(String zjjgzhhm) {
        this.zjjgzhhm = zjjgzhhm;
    }

    @Basic
    @Column(name = "WTRQYSJ")
    public Date getWtrqysj() {
        return wtrqysj;
    }

    public void setWtrqysj(Date wtrqysj) {
        this.wtrqysj = wtrqysj;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "YHDM")
    public String getYhdm() {
        return yhdm;
    }

    public void setYhdm(String yhdm) {
        this.yhdm = yhdm;
    }

    @Basic
    @Column(name = "DKQX")
    public long getDkqx() {
        return dkqx;
    }

    public void setDkqx(long dkqx) {
        this.dkqx = dkqx;
    }

    @Basic
    @Column(name = "DKRQYSJ")
    public Date getDkrqysj() {
        return dkrqysj;
    }

    public void setDkrqysj(Date dkrqysj) {
        this.dkrqysj = dkrqysj;
    }

    @Basic
    @Column(name = "YQFXLL")
    public long getYqfxll() {
        return yqfxll;
    }

    public void setYqfxll(long yqfxll) {
        this.yqfxll = yqfxll;
    }

    @Basic
    @Column(name = "DKRQYDB")
    public String getDkrqydb() {
        return dkrqydb;
    }

    public void setDkrqydb(String dkrqydb) {
        this.dkrqydb = dkrqydb;
    }

    @Basic
    @Column(name = "DKR")
    public String getDkr() {
        return dkr;
    }

    public void setDkr(String dkr) {
        this.dkr = dkr;
    }

    @Basic
    @Column(name = "JKRQYSJ")
    public Date getJkrqysj() {
        return jkrqysj;
    }

    public void setJkrqysj(Date jkrqysj) {
        this.jkrqysj = jkrqysj;
    }

    @Basic
    @Column(name = "JKRQYDB")
    public String getJkrqydb() {
        return jkrqydb;
    }

    public void setJkrqydb(String jkrqydb) {
        this.jkrqydb = jkrqydb;
    }

    @Basic
    @Column(name = "WTRQYDB")
    public String getWtrqydb() {
        return wtrqydb;
    }

    public void setWtrqydb(String wtrqydb) {
        this.wtrqydb = wtrqydb;
    }

    @Basic
    @Column(name = "WTR")
    public String getWtr() {
        return wtr;
    }

    public void setWtr(String wtr) {
        this.wtr = wtr;
    }

    @Basic
    @Column(name = "DKHBFS")
    public String getDkhbfs() {
        return dkhbfs;
    }

    public void setDkhbfs(String dkhbfs) {
        this.dkhbfs = dkhbfs;
    }

    @Basic
    @Column(name = "DKLL")
    public long getDkll() {
        return dkll;
    }

    public void setDkll(long dkll) {
        this.dkll = dkll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectLoanBorrowContractM that = (ProjectLoanBorrowContractM) o;

        if (id != that.id) return false;
        if (nyfxll != that.nyfxll) return false;
        if (dkje != that.dkje) return false;
        if (dkqx != that.dkqx) return false;
        if (yqfxll != that.yqfxll) return false;
        if (dkll != that.dkll) return false;
        if (dkfffs != null ? !dkfffs.equals(that.dkfffs) : that.dkfffs != null) return false;
        if (jkr != null ? !jkr.equals(that.jkr) : that.jkr != null) return false;
        if (xmbh != null ? !xmbh.equals(that.xmbh) : that.xmbh != null) return false;
        if (zjjgzhhm != null ? !zjjgzhhm.equals(that.zjjgzhhm) : that.zjjgzhhm != null) return false;
        if (wtrqysj != null ? !wtrqysj.equals(that.wtrqysj) : that.wtrqysj != null) return false;
        if (jkhtbh != null ? !jkhtbh.equals(that.jkhtbh) : that.jkhtbh != null) return false;
        if (yhdm != null ? !yhdm.equals(that.yhdm) : that.yhdm != null) return false;
        if (dkrqysj != null ? !dkrqysj.equals(that.dkrqysj) : that.dkrqysj != null) return false;
        if (dkrqydb != null ? !dkrqydb.equals(that.dkrqydb) : that.dkrqydb != null) return false;
        if (dkr != null ? !dkr.equals(that.dkr) : that.dkr != null) return false;
        if (jkrqysj != null ? !jkrqysj.equals(that.jkrqysj) : that.jkrqysj != null) return false;
        if (jkrqydb != null ? !jkrqydb.equals(that.jkrqydb) : that.jkrqydb != null) return false;
        if (wtrqydb != null ? !wtrqydb.equals(that.wtrqydb) : that.wtrqydb != null) return false;
        if (wtr != null ? !wtr.equals(that.wtr) : that.wtr != null) return false;
        if (dkhbfs != null ? !dkhbfs.equals(that.dkhbfs) : that.dkhbfs != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (dkfffs != null ? dkfffs.hashCode() : 0);
        result = 31 * result + (jkr != null ? jkr.hashCode() : 0);
        result = 31 * result + (int) (nyfxll ^ (nyfxll >>> 32));
        result = 31 * result + (xmbh != null ? xmbh.hashCode() : 0);
        result = 31 * result + (int) (dkje ^ (dkje >>> 32));
        result = 31 * result + (zjjgzhhm != null ? zjjgzhhm.hashCode() : 0);
        result = 31 * result + (wtrqysj != null ? wtrqysj.hashCode() : 0);
        result = 31 * result + (jkhtbh != null ? jkhtbh.hashCode() : 0);
        result = 31 * result + (yhdm != null ? yhdm.hashCode() : 0);
        result = 31 * result + (int) (dkqx ^ (dkqx >>> 32));
        result = 31 * result + (dkrqysj != null ? dkrqysj.hashCode() : 0);
        result = 31 * result + (int) (yqfxll ^ (yqfxll >>> 32));
        result = 31 * result + (dkrqydb != null ? dkrqydb.hashCode() : 0);
        result = 31 * result + (dkr != null ? dkr.hashCode() : 0);
        result = 31 * result + (jkrqysj != null ? jkrqysj.hashCode() : 0);
        result = 31 * result + (jkrqydb != null ? jkrqydb.hashCode() : 0);
        result = 31 * result + (wtrqydb != null ? wtrqydb.hashCode() : 0);
        result = 31 * result + (wtr != null ? wtr.hashCode() : 0);
        result = 31 * result + (dkhbfs != null ? dkhbfs.hashCode() : 0);
        result = 31 * result + (int) (dkll ^ (dkll >>> 32));
        return result;
    }
}
