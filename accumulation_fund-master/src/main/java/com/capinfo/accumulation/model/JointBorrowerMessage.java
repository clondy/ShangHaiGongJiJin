package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;

import javax.persistence.*;

/**
 * Comment 共同借款人信息
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "JOINT_BORROWER_MESSAGE")
public class JointBorrowerMessage implements BaseEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String gddhhm;
    private String sjhm;
    private String gtjkrzjhm;
    private String gtjkrgjjzh;
    private String gtjkrxm;
    private String cdgx;
    private String gtjkrzjlx;
    private String jkhtbh;
    private Long ysr;


    public JointBorrowerMessage(){

    }

    public JointBorrowerMessage(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "GDDHHM")
    public String getGddhhm() {
        return gddhhm;
    }

    public void setGddhhm(String gddhhm) {
        this.gddhhm = gddhhm;
    }

    @Basic
    @Column(name = "SJHM")
    public String getSjhm() {
        return sjhm;
    }

    public void setSjhm(String sjhm) {
        this.sjhm = sjhm;
    }

    @Basic
    @Column(name = "GTJKRZJHM")
    public String getGtjkrzjhm() {
        return gtjkrzjhm;
    }

    public void setGtjkrzjhm(String gtjkrzjhm) {
        this.gtjkrzjhm = gtjkrzjhm;
    }

    @Basic
    @Column(name = "GTJKRGJJZH")
    public String getGtjkrgjjzh() {
        return gtjkrgjjzh;
    }

    public void setGtjkrgjjzh(String gtjkrgjjzh) {
        this.gtjkrgjjzh = gtjkrgjjzh;
    }

    @Basic
    @Column(name = "GTJKRXM")
    public String getGtjkrxm() {
        return gtjkrxm;
    }

    public void setGtjkrxm(String gtjkrxm) {
        this.gtjkrxm = gtjkrxm;
    }

    @Basic
    @Column(name = "CDGX")
    public String getCdgx() {
        return cdgx;
    }

    public void setCdgx(String cdgx) {
        this.cdgx = cdgx;
    }

    @Basic
    @Column(name = "GTJKRZJLX")
    public String getGtjkrzjlx() {
        return gtjkrzjlx;
    }

    public void setGtjkrzjlx(String gtjkrzjlx) {
        this.gtjkrzjlx = gtjkrzjlx;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "YSR")
    public Long getYsr() {
        return ysr;
    }

    public void setYsr(Long ysr) {
        this.ysr = ysr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JointBorrowerMessage that = (JointBorrowerMessage) o;

        if (id != that.id) return false;
        if (ysr != that.ysr) return false;
        if (gddhhm != null ? !gddhhm.equals(that.gddhhm) : that.gddhhm != null) return false;
        if (sjhm != null ? !sjhm.equals(that.sjhm) : that.sjhm != null) return false;
        if (gtjkrzjhm != null ? !gtjkrzjhm.equals(that.gtjkrzjhm) : that.gtjkrzjhm != null) return false;
        if (gtjkrgjjzh != null ? !gtjkrgjjzh.equals(that.gtjkrgjjzh) : that.gtjkrgjjzh != null) return false;
        if (gtjkrxm != null ? !gtjkrxm.equals(that.gtjkrxm) : that.gtjkrxm != null) return false;
        if (cdgx != null ? !cdgx.equals(that.cdgx) : that.cdgx != null) return false;
        if (gtjkrzjlx != null ? !gtjkrzjlx.equals(that.gtjkrzjlx) : that.gtjkrzjlx != null) return false;
        if (jkhtbh != null ? !jkhtbh.equals(that.jkhtbh) : that.jkhtbh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (gddhhm != null ? gddhhm.hashCode() : 0);
        result = 31 * result + (sjhm != null ? sjhm.hashCode() : 0);
        result = 31 * result + (gtjkrzjhm != null ? gtjkrzjhm.hashCode() : 0);
        result = 31 * result + (gtjkrgjjzh != null ? gtjkrgjjzh.hashCode() : 0);
        result = 31 * result + (gtjkrxm != null ? gtjkrxm.hashCode() : 0);
        result = 31 * result + (cdgx != null ? cdgx.hashCode() : 0);
        result = 31 * result + (gtjkrzjlx != null ? gtjkrzjlx.hashCode() : 0);
        result = 31 * result + (jkhtbh != null ? jkhtbh.hashCode() : 0);
        result = 31 * result + (int) (ysr ^ (ysr >>> 32));
        return result;
    }
}
