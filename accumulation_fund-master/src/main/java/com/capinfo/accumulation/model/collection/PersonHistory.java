package com.capinfo.accumulation.model.collection;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <p>
 *     个人信息历史表
 * </p>
 */
@Entity
@Table(name = "PERSON_HISTORY")
@SequenceGenerator(name = "seqPersonHistory", sequenceName = "SEQ_PERSON_HISTORY", allocationSize = 1)
public class PersonHistory implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //单位_id
    private Long companyId;
    //个人_ID
    private Long personId;
    //个人账号(贯标) grzh
    private String personalAccount;
    //姓名(贯标)  xingming
    private String name;
    //姓名全拼 xmqp
    private String nameAllPinYin;
    //性别(贯标) xingbie
    private String gbsex;
    //性别
    private Long sexId;
    private Dictionary sex;
    //出生年月(贯标) csny
    private Date birthYearMonth = new Date();
    //婚姻状况(贯标)  hyzk
    private String gbmaritalStatus;
    //婚姻状况
    private Long maritalStatus;
    //邮政编码(贯标)  yzbm
    private String postCode;
    //证件类型(贯标)  zjlx
    private String gbidCardType;
    //证件类型
    private Long idCardTypeId;
    private Dictionary idCardType;
    //证件号码(贯标)  zjhm
    private String idCardNumber;
    //学历(贯标) xueli
    private String gbeducation;
    //学历
    private Long educationId;
    private Dictionary education;
    //户籍
    private Long koseki;
    //家庭住址(贯标) jtzz
    private String homeAddress;
    //固定电话号码(贯标)  gddhhm
    private String fixedTelephoneNumber;
    //手机号码(贯标)  sjhm
    private String mobilePhone;
    //职业(贯标)  zhiye
    private String gbprofession;
    //职业
    private Long profession;
    //职务(贯标) zhiwu
    private String gbtitle;
    //职务
    private Long title;
    //职称(贯标) zhichen
    private String gbpositionalTitle;
    //职称
    private Long positionalTitle;
    //工资收入
    private BigDecimal wageIncome;
    //家庭月收入  jtysr
    private BigDecimal homeMonthlyIncome;
    //是否冻结
    private Long isFreeze;
    //贷款否
    private Long loanState;
    //交款时限控制
    private String timeControl;
    //备用
    private String reserve;
    //新开户标志
    private Long newFlag;
    //客户编号
    private String customerNumber;
    //社保卡号
    private String societyCardNo;
    //管理状态
    private Long manageState;
    //单位交存比例
    private Long companyPaymentRatio;
    //个人交存比例
    private Long personPaymentRatio;
    //发生时间
    private Date dateTime= new Date();
    //银行编码
    private String bankCode;
    //操作员
    private Long operatorId;
    //区县
    private Long regionId;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    //复核时间
    private Date confirmationTime= new Date();
    //渠道
    private Long channels;
    //创建人名称
    private String createName;


    private Person person; //个人信息
    private Company company; //单位信息
    //上一个历史id
    private Long previousId;

    private List<PersonHistorySupplement> personHistorySupplement = new ArrayList<PersonHistorySupplement>(); //个人历史补充信息


    //个人公积金历史id
    private Long personHisttoryAccountId;

    //关联单位公积金历史
    List<PersonHistoryAccountFund> personHistoryAccountFunds = new ArrayList<>();




    public PersonHistory(Long id) {
        this.id = id;
    }

    public PersonHistory() {
    }

    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company curCompany) {
        this.company = curCompany;
    }
    @OneToMany(mappedBy="personHistory", targetEntity=PersonHistorySupplement.class)
    public List<PersonHistorySupplement> getPersonHistorySupplement() {
        return personHistorySupplement;
    }

    public void setPersonHistorySupplement(List<PersonHistorySupplement> personHistorySupplementLst) {
        this.personHistorySupplement = personHistorySupplementLst;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person curPerson) {
        this.person = curPerson;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPersonHistory")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "GRZH")
    public String getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(String personalAccount) {
        this.personalAccount = personalAccount;
    }
    @Basic
    @Column(name = "XINGMING")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "XMQP")
    public String getNameAllPinYin() {
        return nameAllPinYin;
    }

    public void setNameAllPinYin(String nameAllPinYin) {
        this.nameAllPinYin = nameAllPinYin;
    }



    @Basic
    @Column(name = "XINGBIE")
    public String getGbsex() {
        return gbsex;
    }

    public void setGbsex(String gbsex) {
        this.gbsex = gbsex;
    }

    @Basic
    @Column(name = "SEX")
    public Long getSexId() {
        return sexId;
    }

    public void setSexId(Long sexId) {
        this.sexId = sexId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SEX", insertable = false, updatable = false)
    public Dictionary getSex() {
        return sex;
    }

    public void setSex(Dictionary sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "CSNY")
    public Date getBirthYearMonth() {
        return birthYearMonth;
    }

    public void setBirthYearMonth(Date birthYearMonth) {
        this.birthYearMonth = birthYearMonth;
    }


    @Basic
    @Column(name = "HYZK")
    public String getGbmaritalStatus() {
        return gbmaritalStatus;
    }

    public void setGbmaritalStatus(String gbmaritalStatus) {
        this.gbmaritalStatus = gbmaritalStatus;
    }


    @Basic
    @Column(name = "MARITAL_STATUS")
    public Long getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Long maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    @Basic
    @Column(name = "YZBM")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    @Basic
    @Column(name = "ZJLX")
    public String getGbidCardType() {
        return gbidCardType;
    }

    public void setGbidCardType(String gbidCardType) {
        this.gbidCardType = gbidCardType;
    }

    @Basic
    @Column(name = "ID_CARD_TYPE")
    public Long getIdCardTypeId() {
        return idCardTypeId;
    }

    public void setIdCardTypeId(Long idCardTypeId) {
        this.idCardTypeId = idCardTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(Dictionary idCardType) {
        this.idCardType = idCardType;
    }

    @Basic
    @Column(name = "ZJHM")
    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    @Basic
    @Column(name = "XUELI")
    public String getGbeducation() {
        return gbeducation;
    }

    public void setGbeducation(String gbeducation) {
        this.gbeducation = gbeducation;
    }

    @Basic
    @Column(name = "EDUCATION")
    public Long getEducationId() {
        return educationId;
    }

    public void setEducationId(Long educationId) {
        this.educationId = educationId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "EDUCATION", insertable = false, updatable = false)
    public Dictionary getEducation() {
        return education;
    }

    @Basic
    @Column(name = "PREVIOUS_ID")
    public  Long getPreviousId() {
        return previousId;
    }

    public void setPreviousId(Long previousId) {
        this.previousId = previousId;
    }

    public void setEducation(Dictionary education) {
        this.education = education;
    }




    @Basic
    @Column(name = "KOSEKI")
    public Long getKoseki() {
        return koseki;
    }

    public void setKoseki(Long koseki) {
        this.koseki = koseki;
    }

    @Basic
    @Column(name = "JTZZ")
    public String getHomeAddress() {
        return homeAddress;
    }
    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }




    @Basic
    @Column(name = "GDDHHM")
    public String getFixedTelephoneNumber() {
        return fixedTelephoneNumber;
    }

    public void setFixedTelephoneNumber(String fixedTelephoneNumber) {
        this.fixedTelephoneNumber = fixedTelephoneNumber;
    }

    @Basic
    @Column(name = "SJHM")
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Basic
    @Column(name = "ZHIYE")
    public String getGbprofession() {
        return gbprofession;
    }

    public void setGbprofession(String gbprofession) {
        this.gbprofession = gbprofession;
    }




    @Basic
    @Column(name = "PROFESSION")
    public Long getProfession() {
        return profession;
    }

    public void setProfession(Long profession) {
        this.profession = profession;
    }

    @Basic
    @Column(name = "ZHIWU")
    public String getGbtitle() {
        return gbtitle;
    }

    public void setGbtitle(String gbtitle) {
        this.gbtitle = gbtitle;
    }


    @Basic
    @Column(name = "TITLE")
    public Long getTitle() {
        return title;
    }

    public void setTitle(Long title) {
        this.title = title;
    }
    @Basic
    @Column(name = "ZHICHEN")
    public String getGbpositionalTitle() {
        return gbpositionalTitle;
    }

    public void setGbpositionalTitle(String gbpositionalTitle) {
        this.gbpositionalTitle = gbpositionalTitle;
    }




    @Basic
    @Column(name = "POSITIONAL_TITLE")
    public Long getPositionalTitle() {
        return positionalTitle;
    }

    public void setPositionalTitle(Long positionalTitle) {
        this.positionalTitle = positionalTitle;
    }

    @Basic
    @Column(name = "WAGE_INCOME")
    public BigDecimal getWageIncome() {
        return wageIncome;
    }

    public void setWageIncome(BigDecimal wageIncome) {
        this.wageIncome = wageIncome;
    }

    @Basic
    @Column(name = "JTYSR")
    public BigDecimal getHomeMonthlyIncome() {
        return homeMonthlyIncome;
    }

    public void setHomeMonthlyIncome(BigDecimal homeMonthlyIncome) {
        this.homeMonthlyIncome = homeMonthlyIncome;
    }




    @Basic
    @Column(name = "IS_FREEZE")
    public Long getIsFreeze() {
        return isFreeze;
    }

    public void setIsFreeze(Long isFreeze) {
        this.isFreeze = isFreeze;
    }

    @Basic
    @Column(name = "LOAN_STATE")
    public Long getLoanState() {
        return loanState;
    }

    public void setLoanState(Long loanState) {
        this.loanState = loanState;
    }

    @Basic
    @Column(name = "TIME_CONTROL")
    public String getTimeControl() {
        return timeControl;
    }

    public void setTimeControl(String timeControl) {
        this.timeControl = timeControl;
    }

    @Basic
    @Column(name = "RESERVE")
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Basic
    @Column(name = "NEW_FLAG")
    public Long getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(Long newFlag) {
        this.newFlag = newFlag;
    }

    @Basic
    @Column(name = "CUSTOMER_NUMBER")
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @Basic
    @Column(name = "SOCIETY_CARD_NO")
    public String getSocietyCardNo() {
        return societyCardNo;
    }

    public void setSocietyCardNo(String societyCardNo) {
        this.societyCardNo = societyCardNo;
    }

    @Basic
    @Column(name = "MANAGE_STATE")
    public Long getManageState() {
        return manageState;
    }

    public void setManageState(Long manageState) {
        this.manageState = manageState;
    }

    @Basic
    @Column(name = "COMPANY_PAYMENT_RATIO")
    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "PERSON_PAYMENT_RATIO")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonHistory))
            return false;

        PersonHistory castOther = (PersonHistory) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id ",getId())
                .append("companyId",getCompanyId ())
                .append("personId ",getPersonId())
                .append("personalAccount",getPersonalAccount ())
                .append("name ",getName())
                .append("NameAllPinYin",getNameAllPinYin ())
                .append("sexId",getSexId ())
                .append("birthYearMonth ",getBirthYearMonth())
                .append("maritalStatus",getMaritalStatus ())
                .append("postCode ",getPostCode())
                .append("idCardTypeId ",getIdCardTypeId())
                .append("idCardNumber ",getIdCardNumber())
                .append("educationId",getEducationId ())
                .append("koseki ",getKoseki())
                .append("homeAddress",getHomeAddress ())
                .append("fixedTelephoneNumber ",getFixedTelephoneNumber())
                .append("mobilePhone",getMobilePhone ())
                .append("profession ",getProfession())
                .append("title",getTitle ())
                .append("positionalTitle",getPositionalTitle ())
                .append("wageIncome ",getWageIncome())
                .append("homeMonthlyIncome",getHomeMonthlyIncome ())
                .append("isFreeze ",getIsFreeze())
                .append("loanState",getLoanState ())
                .append("timeControl",getTimeControl ())
                .append("reserve",getReserve ())
                .append("newFlag",getNewFlag ())
                .append("customerNumber ",getCustomerNumber())
                .append("societyCardNo",getSocietyCardNo ())
                .append("manageState",getManageState ())
                .append("companyPaymentRatio",getCompanyPaymentRatio ())
                .append("personPaymentRatio ",getPersonPaymentRatio())
                .append("dateTime ",getDateTime())
                .append("bankCode ",getBankCode())
                .append("operatorId ",getOperatorId())
                .append("regionId ",getRegionId())
                .append("siteId ",getSiteId())
                .append("confirmerId",getConfirmerId ())
                .append("confirmationTime ",getConfirmationTime())
                .append("channels ",getChannels())
                .append("createName ",getCreateName())
                .toString();
    }
    @Column(name = "PERSON_HISTORY_ACCOUNT_ID")
    public Long getPersonHisttoryAccountId() {
        return personHisttoryAccountId;
    }

    public void setPersonHisttoryAccountId(Long personHisttoryAccountId) {
        this.personHisttoryAccountId = personHisttoryAccountId;
    }


    @OneToMany(targetEntity=PersonHistoryAccountFund.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PERSON_HISTORY_ID")
    @OrderBy(value="id desc")
    public List<PersonHistoryAccountFund> getPersonHistoryAccountFunds() {
        return personHistoryAccountFunds;
    }

    public void setPersonHistoryAccountFunds(List<PersonHistoryAccountFund> personHistoryAccountFunds) {
        this.personHistoryAccountFunds = personHistoryAccountFunds;
    }

    @Transient
    public void addAccountHistory(PersonHistoryAccountFund accountHistory) {
        accountHistory.setPersonHistory(this);
        getPersonHistoryAccountFunds().add(accountHistory);
    }

    @Transient
    public void removeAccountHistory(PersonHistoryAccountFund accountHistory) {
        accountHistory.setPersonHistory(null);
        getPersonHistoryAccountFunds().remove(accountHistory);
    }

}
