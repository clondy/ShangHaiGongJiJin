package com.capinfo.accumulation.model.notice;

import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by xiaopeng on 2017/10/20.
 *发送的消息的读取记录
 *
 */
@Entity
@Table(name = "NOTICEREADRECORD")
@SequenceGenerator(name = "seqReadRecord", sequenceName = "SEQ_NOTICE_READRECORD", allocationSize = 1)
public class ReadRecord implements BaseEntity {
    //主键
    private Long id;
    //消息
    private Notice notice;
    //读者
    private SystemUser reader;
    //读取时间
    private Date readTime;

    @Basic
    @Column(name = "NOTICE")
    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "READER_ID", insertable = false, updatable = false)
    public SystemUser getReader() {
        return reader;
    }

    public void setReader(SystemUser reader) {
        this.reader = reader;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "READ_TIME")
    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }
}
