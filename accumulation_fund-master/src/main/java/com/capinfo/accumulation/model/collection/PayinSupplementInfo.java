package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.enums.Channel;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Comment  补缴核定（补缴信息）
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "PAYIN_SUPPLEMENT_INFO")
@SequenceGenerator(name = "seqPAYIN_SUPPLEMENT_INFO", sequenceName = "SEQ_PAYIN_SUPPLEMENT_INFO", allocationSize = 1)
public class PayinSupplementInfo implements BaseEntity{

    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    //单位公积金_id
    private Long companyAccountFundId;

    //
    private CompanyAccountFund companyAccountFund;

    //补缴选项
    private Long paymentOptions;

    //补缴原因
    private Long paymentReason;

    //补缴总金额
    private BigDecimal paymentAmount;

    //核定日期
    private Date receivedDate;

    //到账日期
    private Date approvalDate;

    //流水号
    private String serialNumber;

    //补缴起始年月
    private String  startTime;

    //补缴结束年月
    private String  endTime;

    //补缴人数
    private Long numberOfPeople;

    //发生时间
    private Date dateTime;

    //银行编码
    private String bankCode;

    //操作员
    private Long operatorId;

    //区县
    private Long regionId;

    //网点
    private Long siteId;

    //复核人
    private Long confirmerId;

    //复核时间
    private Date confirmationTime;

    //渠道
    private Long channels=new Long(Channel.中心网点.getToken());

    //创建人名称
    private String createName;

    private List<PayinSupplementItem> payinSupplementItems=new ArrayList<>();

    public PayinSupplementInfo(){

    }

    public PayinSupplementInfo(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPAYIN_SUPPLEMENT_INFO")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_Fund_id", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @OneToMany(mappedBy="payinSupplementInfo", targetEntity=PayinSupplementItem.class,cascade=javax.persistence.CascadeType.ALL)
    public List<PayinSupplementItem> getPayinSupplementItems() {
        return payinSupplementItems;
    }

    public void setPayinSupplementItems(List<PayinSupplementItem> payinSupplementItems) {
        this.payinSupplementItems = payinSupplementItems;
    }



    @Basic
    @Column(name = "PAYMENT_OPTIONS")
    public Long getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(Long paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    @Basic
    @Column(name = "PAYMENT_REASON")
    public Long getPaymentReason() {
        return paymentReason;
    }

    public void setPaymentReason(Long paymentReason) {
        this.paymentReason = paymentReason;
    }

    @Basic
    @Column(name = "PAYMENT_AMOUNT")
    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    @Basic
    @Column(name = "RECEIVED_DATE")
    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    @Basic
    @Column(name = "APPROVAL_DATE")
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Basic
    @Column(name = "SERIAL_NUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Basic
    @Column(name = "START_TIME")
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "END_TIME")
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "NUMBER_OF_PEOPLE")
    public Long getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Long numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof PayinSupplementInfo))
            return false;
        PayinSupplementInfo castOther = (PayinSupplementInfo)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
