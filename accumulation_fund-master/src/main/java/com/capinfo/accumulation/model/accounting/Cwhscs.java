package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OrderBy;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * Cwhscs entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CWHSCS")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_CWHSCS", allocationSize = 1)
public class Cwhscs implements BaseEntity {

	// Fields

	private Long id;
	//核算单位
	private Dictionary hsdw;
	private Ztsz ztdm;
	private Long sfyxdxtz;
	private Long sfyxpzwk;
	private Long pzbhscfs;
	private Long sfyxkpzbh;
	private Long pzbhcd;
	private Long pzbhscgz;
	private String zdpzzdr;	
	private String cwnd;
	private Long dfyxpzfjz;
	private Long sfyxfyj;
	private Long sfyxqzyj;
	private int sjzt;
	private Date crsj;
	private List<CwhscsXgls> cwhscsxgls;

	// Constructors
	@OneToMany(mappedBy="cwhscsid" ,fetch =FetchType.LAZY )   
	@OrderBy(clause="ID DESC")
	public List<CwhscsXgls> getCwhscsxgls() {
		return cwhscsxgls;
	}

	public void setCwhscsxgls(List<CwhscsXgls> cwhscsxgls) {
		this.cwhscsxgls = cwhscsxgls;
	}
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Column(name = "SFYXDXTZ", precision = 22, scale = 0)
	public Long getSfyxdxtz() {
		return this.sfyxdxtz;
	}

	public void setSfyxdxtz(Long sfyxdxtz) {
		this.sfyxdxtz = sfyxdxtz;
	}

	@Column(name = "SFYXPZWK", precision = 22, scale = 0)
	public Long getSfyxpzwk() {
		return this.sfyxpzwk;
	}

	public void setSfyxpzwk(Long sfyxpzwk) {
		this.sfyxpzwk = sfyxpzwk;
	}

	@Column(name = "PZBHSCFS", precision = 22, scale = 0)
	public Long getPzbhscfs() {
		return this.pzbhscfs;
	}

	public void setPzbhscfs(Long pzbhscfs) {
		this.pzbhscfs = pzbhscfs;
	}

	@Column(name = "SFYXKPZBH", precision = 22, scale = 0)
	public Long getSfyxkpzbh() {
		return this.sfyxkpzbh;
	}

	public void setSfyxkpzbh(Long sfyxkpzbh) {
		this.sfyxkpzbh = sfyxkpzbh;
	}

	@Column(name = "PZBHCD", precision = 22, scale = 0)
	public Long getPzbhcd() {
		return this.pzbhcd;
	}

	public void setPzbhcd(Long pzbhcd) {
		this.pzbhcd = pzbhcd;
	}

	@Column(name = "PZBHSCGZ", precision = 22, scale = 0)
	public Long getPzbhscgz() {
		return this.pzbhscgz;
	}

	public void setPzbhscgz(Long pzbhscgz) {
		this.pzbhscgz = pzbhscgz;
	}

	@Column(name = "ZDPZZDR", length = 120)
	public String getZdpzzdr() {
		return this.zdpzzdr;
	}

	public void setZdpzzdr(String zdpzzdr) {
		this.zdpzzdr = zdpzzdr;
	}

	@Column(name = "DFYXPZFJZ", precision = 22, scale = 0)
	public Long getDfyxpzfjz() {
		return this.dfyxpzfjz;
	}

	public void setDfyxpzfjz(Long dfyxpzfjz) {
		this.dfyxpzfjz = dfyxpzfjz;
	}

	@Column(name = "SFYXFYJ", precision = 22, scale = 0)
	public Long getSfyxfyj() {
		return this.sfyxfyj;
	}

	public void setSfyxfyj(Long sfyxfyj) {
		this.sfyxfyj = sfyxfyj;
	}

	@Column(name = "SFYXQZYJ", precision = 22, scale = 0)
	public Long getSfyxqzyj() {
		return this.sfyxqzyj;
	}

	public void setSfyxqzyj(Long sfyxqzyj) {
		this.sfyxqzyj = sfyxqzyj;
	}

	@Column(name = "SJZT", length = 10)
	public int getSjzt() {
		return this.sjzt;
	}

	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CRSJ", length = 7)
	public Date getCrsj() {
		return this.crsj;
	}

	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ztdm" ,referencedColumnName="ztdm" )
	public Ztsz getZtdm() {
		return ztdm;
	}

	
	public void setZtdm(Ztsz ztdm) {
		this.ztdm = ztdm;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hsdw" ,referencedColumnName="id" )
	public Dictionary getHsdw() {
		return hsdw;
	}


	public void setHsdw(Dictionary hsdw) {
		this.hsdw = hsdw;
	}

	@Column(name = "CWND")
	public String getCwnd() {
		return cwnd;
	}


	public void setCwnd(String cwnd) {
		this.cwnd = cwnd;
	}
	
	
	
	
}