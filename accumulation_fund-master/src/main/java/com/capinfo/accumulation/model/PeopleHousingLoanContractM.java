package com.capinfo.accumulation.model;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.sql.Time;

/**
 * <p>
 * 个人住房贷款借款合同信息（贯标）
 * </p>
 */
@Entity
@Table(name = "PEOPLE_HOUSING_LOAN_CONTRACT_M")
@SequenceGenerator(name = "seqPeopleHousingLoanContractM", sequenceName = "SEQ_PEO_HOU_LOAN_CONTRACT_M", allocationSize = 1)
public class PeopleHousingLoanContractM implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //(贯标)账户开户银行代码
    private String zhkhyhdm;
    //(贯标)约定到期日期
    private Time yddqrq;
    //(贯标)约定放款日期
    private Time ydfkrq;
    //(贯标)受委托银行代码
    private String swtyhdm;
    //(贯标)贷款担保类型
    private String dkdblx;
    //(贯标)贷款期数
    private long dkqs;
    //(贯标)售房人开户银行名称
    private String sfrkhyhmc;
    //(贯标)售房人名称
    private String sfrmc;
    //(贯标)房屋性质
    private String fwxz;
    //(贯标)房屋坐落
    private String fwzl;
    //(贯标)贷款类型
    private String dklx;
    //(贯标)借款合同签订日期
    private Time jkhtqdrq;
    //(贯标)约定还款日
    private Time ydhkr;
    //(贯标)受委托银行名称
    private String swtyhmc;
    //(贯标)还款账号
    private String hkzh;
    //(贯标)借款人证件类型
    private String jkrzjlx;
    //(贯标)房屋建筑面积
    private long fwjzmj;
    //(贯标)借款合同编号
    private String jkhtbh;
    //(贯标)借款人公积金账号
    private String jkrrgjjzh;
    //(贯标)房屋套内面积
    private long fwtnmj;
    //(贯标)购房首付款
    private long gfsfk;
    //(贯标)售房人开户银行代码
    private String sfrkhyhdm;
    //(贯标)账户开户银行名称
    private String zhkhyhmc;
    //(贯标)合同贷款金额
    private long htdkje;
    //(贯标)利率浮动比例
    private Long llfdbl;
    //(贯标)购房合同编号
    private String gfhtbh;
    //(贯标) 借款人姓名
    private String jkrxm;
    //(贯标)贷款还款方式
    private Time dkhkfs;
    //(贯标)借款合同利率
    private Long jkhtll;
    //(贯标)房屋总价
    private long fwzj;
    //(贯标)售房人账户号码
    private String sfrzhhm;
    //(贯标)借款人证件号
    private String jkrzjh;

    public PeopleHousingLoanContractM(long id) {
        this.id = id;
    }

    public PeopleHousingLoanContractM() {
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ZHKHYHDM")
    public String getZhkhyhdm() {
        return zhkhyhdm;
    }

    public void setZhkhyhdm(String zhkhyhdm) {
        this.zhkhyhdm = zhkhyhdm;
    }

    @Basic
    @Column(name = "YDDQRQ")
    public Time getYddqrq() {
        return yddqrq;
    }

    public void setYddqrq(Time yddqrq) {
        this.yddqrq = yddqrq;
    }

    @Basic
    @Column(name = "YDFKRQ")
    public Time getYdfkrq() {
        return ydfkrq;
    }

    public void setYdfkrq(Time ydfkrq) {
        this.ydfkrq = ydfkrq;
    }

    @Basic
    @Column(name = "SWTYHDM")
    public String getSwtyhdm() {
        return swtyhdm;
    }

    public void setSwtyhdm(String swtyhdm) {
        this.swtyhdm = swtyhdm;
    }

    @Basic
    @Column(name = "DKDBLX")
    public String getDkdblx() {
        return dkdblx;
    }

    public void setDkdblx(String dkdblx) {
        this.dkdblx = dkdblx;
    }

    @Basic
    @Column(name = "DKQS")
    public long getDkqs() {
        return dkqs;
    }

    public void setDkqs(long dkqs) {
        this.dkqs = dkqs;
    }

    @Basic
    @Column(name = "SFRKHYHMC")
    public String getSfrkhyhmc() {
        return sfrkhyhmc;
    }

    public void setSfrkhyhmc(String sfrkhyhmc) {
        this.sfrkhyhmc = sfrkhyhmc;
    }

    @Basic
    @Column(name = "SFRMC")
    public String getSfrmc() {
        return sfrmc;
    }

    public void setSfrmc(String sfrmc) {
        this.sfrmc = sfrmc;
    }

    @Basic
    @Column(name = "FWXZ")
    public String getFwxz() {
        return fwxz;
    }

    public void setFwxz(String fwxz) {
        this.fwxz = fwxz;
    }

    @Basic
    @Column(name = "FWZL")
    public String getFwzl() {
        return fwzl;
    }

    public void setFwzl(String fwzl) {
        this.fwzl = fwzl;
    }

    @Basic
    @Column(name = "DKLX")
    public String getDklx() {
        return dklx;
    }

    public void setDklx(String dklx) {
        this.dklx = dklx;
    }

    @Basic
    @Column(name = "JKHTQDRQ")
    public Time getJkhtqdrq() {
        return jkhtqdrq;
    }

    public void setJkhtqdrq(Time jkhtqdrq) {
        this.jkhtqdrq = jkhtqdrq;
    }

    @Basic
    @Column(name = "YDHKR")
    public Time getYdhkr() {
        return ydhkr;
    }

    public void setYdhkr(Time ydhkr) {
        this.ydhkr = ydhkr;
    }

    @Basic
    @Column(name = "SWTYHMC")
    public String getSwtyhmc() {
        return swtyhmc;
    }

    public void setSwtyhmc(String swtyhmc) {
        this.swtyhmc = swtyhmc;
    }

    @Basic
    @Column(name = "HKZH")
    public String getHkzh() {
        return hkzh;
    }

    public void setHkzh(String hkzh) {
        this.hkzh = hkzh;
    }

    @Basic
    @Column(name = "JKRZJLX")
    public String getJkrzjlx() {
        return jkrzjlx;
    }

    public void setJkrzjlx(String jkrzjlx) {
        this.jkrzjlx = jkrzjlx;
    }

    @Basic
    @Column(name = "FWJZMJ")
    public long getFwjzmj() {
        return fwjzmj;
    }

    public void setFwjzmj(long fwjzmj) {
        this.fwjzmj = fwjzmj;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "JKRRGJJZH")
    public String getJkrrgjjzh() {
        return jkrrgjjzh;
    }

    public void setJkrrgjjzh(String jkrrgjjzh) {
        this.jkrrgjjzh = jkrrgjjzh;
    }

    @Basic
    @Column(name = "FWTNMJ")
    public long getFwtnmj() {
        return fwtnmj;
    }

    public void setFwtnmj(long fwtnmj) {
        this.fwtnmj = fwtnmj;
    }

    @Basic
    @Column(name = "GFSFK")
    public long getGfsfk() {
        return gfsfk;
    }

    public void setGfsfk(long gfsfk) {
        this.gfsfk = gfsfk;
    }

    @Basic
    @Column(name = "SFRKHYHDM")
    public String getSfrkhyhdm() {
        return sfrkhyhdm;
    }

    public void setSfrkhyhdm(String sfrkhyhdm) {
        this.sfrkhyhdm = sfrkhyhdm;
    }

    @Basic
    @Column(name = "ZHKHYHMC")
    public String getZhkhyhmc() {
        return zhkhyhmc;
    }

    public void setZhkhyhmc(String zhkhyhmc) {
        this.zhkhyhmc = zhkhyhmc;
    }

    @Basic
    @Column(name = "HTDKJE")
    public long getHtdkje() {
        return htdkje;
    }

    public void setHtdkje(long htdkje) {
        this.htdkje = htdkje;
    }

    @Basic
    @Column(name = "LLFDBL")
    public Long getLlfdbl() {
        return llfdbl;
    }

    public void setLlfdbl(Long llfdbl) {
        this.llfdbl = llfdbl;
    }

    @Basic
    @Column(name = "GFHTBH")
    public String getGfhtbh() {
        return gfhtbh;
    }

    public void setGfhtbh(String gfhtbh) {
        this.gfhtbh = gfhtbh;
    }

    @Basic
    @Column(name = "JKRXM")
    public String getJkrxm() {
        return jkrxm;
    }

    public void setJkrxm(String jkrxm) {
        this.jkrxm = jkrxm;
    }

    @Basic
    @Column(name = "DKHKFS")
    public Time getDkhkfs() {
        return dkhkfs;
    }

    public void setDkhkfs(Time dkhkfs) {
        this.dkhkfs = dkhkfs;
    }

    @Basic
    @Column(name = "JKHTLL")
    public Long getJkhtll() {
        return jkhtll;
    }

    public void setJkhtll(Long jkhtll) {
        this.jkhtll = jkhtll;
    }

    @Basic
    @Column(name = "FWZJ")
    public long getFwzj() {
        return fwzj;
    }

    public void setFwzj(long fwzj) {
        this.fwzj = fwzj;
    }

    @Basic
    @Column(name = "SFRZHHM")
    public String getSfrzhhm() {
        return sfrzhhm;
    }

    public void setSfrzhhm(String sfrzhhm) {
        this.sfrzhhm = sfrzhhm;
    }

    @Basic
    @Column(name = "JKRZJH")
    public String getJkrzjh() {
        return jkrzjh;
    }

    public void setJkrzjh(String jkrzjh) {
        this.jkrzjh = jkrzjh;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PeopleHousingLoanContractM))
            return false;

        PeopleHousingLoanContractM castOther = (PeopleHousingLoanContractM) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .toString();
    }
}
