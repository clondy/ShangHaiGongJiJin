package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;


/**
 * <p>
 *发送给个人（员工）的消息（通知）/ 反馈信息……
 * </p>
 */
@Entity
@Table(name = "PERSON_MESSAGE")
@SequenceGenerator(name = "seqPersonMessage", sequenceName = "SEQ_PERSON_MESSAGE", allocationSize = 1)
public class PersonMessage  implements BaseEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    //个人_id
    private Long personId;
    //时间
    private Date feedbackTime= new Date();
    //类型
    private Long feedbackType;
    //原因
    private String feedbackReason;
    //标题
    private String feedbackTitle;
    //内容
    private String feedbackContent;
    //是否已读
    private Long haveRead;

    private Person person;  //个人信息


    public PersonMessage() {
    }

    public PersonMessage(Long id) {

        this.id = id;
    }
    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person curPerson) {
        this.person = curPerson;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPersonMessage")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "FEEDBACK_TIME")
    public Date getFeedbackTime() {
        return feedbackTime;
    }

    public void setFeedbackTime(Date feedbackTime) {
        this.feedbackTime = feedbackTime;
    }

    @Basic
    @Column(name = "FEEDBACK_TYPE")
    public Long getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(Long feedbackType) {
        this.feedbackType = feedbackType;
    }

    @Basic
    @Column(name = "FEEDBACK_REASON")
    public String getFeedbackReason() {
        return feedbackReason;
    }

    public void setFeedbackReason(String feedbackReason) {
        this.feedbackReason = feedbackReason;
    }

    @Basic
    @Column(name = "FEEDBACK_TITLE")
    public String getFeedbackTitle() {
        return feedbackTitle;
    }

    public void setFeedbackTitle(String feedbackTitle) {
        this.feedbackTitle = feedbackTitle;
    }

    @Basic
    @Column(name = "FEEDBACK_CONTENT")
    public String getFeedbackContent() {
        return feedbackContent;
    }

    public void setFeedbackContent(String feedbackContent) {
        this.feedbackContent = feedbackContent;
    }

    @Basic
    @Column(name = "HAVE_READ")
    public Long getHaveRead() {
        return haveRead;
    }

    public void setHaveRead(Long haveRead) {
        this.haveRead = haveRead;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonMessage))
            return false;

        PersonMessage castOther = (PersonMessage) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id",getId())
                .append("personId",getPersonId())
                .append("feedbackTime",getFeedbackTime())
                .append("feedbackType",getFeedbackType())
                .append("feedbackReason",getFeedbackReason())
                .append("feedbackTitle",getFeedbackTitle())
                .append("feedbackContent",getFeedbackContent())
                .append("haveRead",getHaveRead())
                .toString();
    }
}
