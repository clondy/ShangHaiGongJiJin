package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 项目贷款抵押合同信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:37
 */
@Entity
@Table(name = "PROJECT_LOAN_MORTGAGE_CONTRACT")
public class ProjectLoanMortgageContract {
    private long id;
    private String dywmc;
    private String dyhtbh;
    private Date dyqjcrq;
    private Date dyqjlrq;
    private long dyl;
    private String jkhtbh;
    private long ydyjz;
    private String dywqzh;
    private String dywlx;
    private String dywcs;
    private long dywpgjz;

    public ProjectLoanMortgageContract(long id) {
        this.id = id;
    }

    public ProjectLoanMortgageContract() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DYWMC")
    public String getDywmc() {
        return dywmc;
    }

    public void setDywmc(String dywmc) {
        this.dywmc = dywmc;
    }

    @Basic
    @Column(name = "DYHTBH")
    public String getDyhtbh() {
        return dyhtbh;
    }

    public void setDyhtbh(String dyhtbh) {
        this.dyhtbh = dyhtbh;
    }

    @Basic
    @Column(name = "DYQJCRQ")
    public Date getDyqjcrq() {
        return dyqjcrq;
    }

    public void setDyqjcrq(Date dyqjcrq) {
        this.dyqjcrq = dyqjcrq;
    }

    @Basic
    @Column(name = "DYQJLRQ")
    public Date getDyqjlrq() {
        return dyqjlrq;
    }

    public void setDyqjlrq(Date dyqjlrq) {
        this.dyqjlrq = dyqjlrq;
    }

    @Basic
    @Column(name = "DYL")
    public long getDyl() {
        return dyl;
    }

    public void setDyl(long dyl) {
        this.dyl = dyl;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "YDYJZ")
    public long getYdyjz() {
        return ydyjz;
    }

    public void setYdyjz(long ydyjz) {
        this.ydyjz = ydyjz;
    }

    @Basic
    @Column(name = "DYWQZH")
    public String getDywqzh() {
        return dywqzh;
    }

    public void setDywqzh(String dywqzh) {
        this.dywqzh = dywqzh;
    }

    @Basic
    @Column(name = "DYWLX")
    public String getDywlx() {
        return dywlx;
    }

    public void setDywlx(String dywlx) {
        this.dywlx = dywlx;
    }

    @Basic
    @Column(name = "DYWCS")
    public String getDywcs() {
        return dywcs;
    }

    public void setDywcs(String dywcs) {
        this.dywcs = dywcs;
    }

    @Basic
    @Column(name = "DYWPGJZ")
    public long getDywpgjz() {
        return dywpgjz;
    }

    public void setDywpgjz(long dywpgjz) {
        this.dywpgjz = dywpgjz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectLoanMortgageContract that = (ProjectLoanMortgageContract) o;

        if (id != that.id) return false;
        if (dyl != that.dyl) return false;
        if (ydyjz != that.ydyjz) return false;
        if (dywpgjz != that.dywpgjz) return false;
        if (dywmc != null ? !dywmc.equals(that.dywmc) : that.dywmc != null) return false;
        if (dyhtbh != null ? !dyhtbh.equals(that.dyhtbh) : that.dyhtbh != null) return false;
        if (dyqjcrq != null ? !dyqjcrq.equals(that.dyqjcrq) : that.dyqjcrq != null) return false;
        if (dyqjlrq != null ? !dyqjlrq.equals(that.dyqjlrq) : that.dyqjlrq != null) return false;
        if (jkhtbh != null ? !jkhtbh.equals(that.jkhtbh) : that.jkhtbh != null) return false;
        if (dywqzh != null ? !dywqzh.equals(that.dywqzh) : that.dywqzh != null) return false;
        if (dywlx != null ? !dywlx.equals(that.dywlx) : that.dywlx != null) return false;
        if (dywcs != null ? !dywcs.equals(that.dywcs) : that.dywcs != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (dywmc != null ? dywmc.hashCode() : 0);
        result = 31 * result + (dyhtbh != null ? dyhtbh.hashCode() : 0);
        result = 31 * result + (dyqjcrq != null ? dyqjcrq.hashCode() : 0);
        result = 31 * result + (dyqjlrq != null ? dyqjlrq.hashCode() : 0);
        result = 31 * result + (int) (dyl ^ (dyl >>> 32));
        result = 31 * result + (jkhtbh != null ? jkhtbh.hashCode() : 0);
        result = 31 * result + (int) (ydyjz ^ (ydyjz >>> 32));
        result = 31 * result + (dywqzh != null ? dywqzh.hashCode() : 0);
        result = 31 * result + (dywlx != null ? dywlx.hashCode() : 0);
        result = 31 * result + (dywcs != null ? dywcs.hashCode() : 0);
        result = 31 * result + (int) (dywpgjz ^ (dywpgjz >>> 32));
        return result;
    }
}
