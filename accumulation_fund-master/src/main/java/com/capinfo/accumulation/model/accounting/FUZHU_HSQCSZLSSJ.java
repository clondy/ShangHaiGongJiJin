package com.capinfo.accumulation.model.accounting;

import java.util.List;
import java.util.Date;
/**
 *  @author zxl
 */
public class FUZHU_HSQCSZLSSJ {
	/**
	 * ID
	 */	
    private Long ID;
	/**
	 * 会计年度
	 */	
    private Long KJND;
	/**
	 * 核算类型
	 */	
    private Long HSLX;
	/**
	 * 财务科目
	 */	
    private String CWKM;
	/**
	 * 年初余额
	 */	
    private Long NCYE;
	/**
	 * 余额
	 */	
    private Long YUE;
	/**
	 * 本年累计借方
	 */	
    private Long BNLJJF;
	/**
	 * 本年累计贷方
	 */	
    private Long BNLJDF;
	/**
	 * 操作员id
	 */	
    private String CZYID;
	/**
	 * 插入时间
	 */	
    private java.util.Date CRSJ;

    public void setID(Long ID){
        this.ID = ID;
    }
    public Long getID(){
        return this.ID;
    }
    public void setKJND(Long KJND){
        this.KJND = KJND;
    }
    public Long getKJND(){
        return this.KJND;
    }
    public void setHSLX(Long HSLX){
        this.HSLX = HSLX;
    }
    public Long getHSLX(){
        return this.HSLX;
    }
    public void setCWKM(String CWKM){
        this.CWKM = CWKM;
    }
    public String getCWKM(){
        return this.CWKM;
    }
    public void setNCYE(Long NCYE){
        this.NCYE = NCYE;
    }
    public Long getNCYE(){
        return this.NCYE;
    }
    public void setYUE(Long YUE){
        this.YUE = YUE;
    }
    public Long getYUE(){
        return this.YUE;
    }
    public void setBNLJJF(Long BNLJJF){
        this.BNLJJF = BNLJJF;
    }
    public Long getBNLJJF(){
        return this.BNLJJF;
    }
    public void setBNLJDF(Long BNLJDF){
        this.BNLJDF = BNLJDF;
    }
    public Long getBNLJDF(){
        return this.BNLJDF;
    }
    public void setCZYID(String CZYID){
        this.CZYID = CZYID;
    }
    public String getCZYID(){
        return this.CZYID;
    }
    public void setCRSJ(java.util.Date CRSJ){
        this.CRSJ = CRSJ;
    }
    public java.util.Date getCRSJ(){
        return this.CRSJ;
    }
}