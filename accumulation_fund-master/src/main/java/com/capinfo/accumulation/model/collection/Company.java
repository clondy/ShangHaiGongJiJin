package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.*;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rexxxar on 2017/9/19.
 * 单位信息
 * @author fengjingsen
 */
@Entity
@Table(name = "COMPANY" )
@SequenceGenerator(name = "seqCompany", sequenceName = "SEQ_COMPANY", allocationSize = 1)
public class Company implements BaseEntity {
    //ID
    private Long id;
    //单位名称(贯标)dwmc
    private String gbCompanyName;
    //组织机构代码(贯标) zzjgdm
    private String gbOrganizationCode;
    //统一社会信用代码
    private String creditCode;
    //主管机构代码
    private String competentCode;
    //单位账号(贯标) dwzh
    private String gbCompanyAccount;
    //账户性质
    private Long natureAccountId;
    private Dictionary natureAccount;
    //单位内部分类
    private Long companyInsideClassificationId;
    private Dictionary companyInsideClassification;
    //机构类型
    private Long organizationTypeId;
    private Dictionary organizationType;
    //单位经济类型(贯标)  dwjjlx
    private String gbCompanyEconomicType;
    //单位经济类型
    private Long companyEconomicTypeId;
    private Dictionary companyEconomicType;
    //单位所属行业(贯标) dwsshy
    private String gbCompanyIndustries;
    //单位所属行业
    private Long companyIndustriesId;
    private Dictionary companyIndustries;
    //单位隶属关系(贯标) dwlsgx
    private String gbCompanyRelationships;
    //单位隶属关系
    private Long companyRelatoinshipsId;
    private Dictionary companyRelatoinships;
    //单位地址(贯标) dwdz
    private String gbCompanyAddress;
    //注册地址
    private String registeredAddress;
    //注册地邮编
    private Long registeredPostCode;
    //单位邮编(贯标) dwyb
    private String gbCompanyPostCode;
    //单位电子信箱(贯标) dwdzxx
    private String gbCompanyEmail;
    //单位设立日期(贯标) dwslrq
    private Date gbCompanyCreationDate=new Date();
    //单位联系地址
    private String companyAddress;
    //是否具备法人资格
    private Boolean legalPersonality = true;
    //单位法人代表证件类型(贯标) dwfrdbzjlx
    private String gbLegalPersonIdCardType;
    //单位法人代表证件类型
    private Long legalPersonIdCardTypeId;
    private Dictionary legalPersonIdCardType;
    //单位法人代表姓名(贯标) dwfrdbxm
    private String gbLegalPersonName;
    //单位法人代表证件号(贯标) dwfrdbzjhm
    private String gbLegalPersonIdCardNumber;
    //法定代表人移动电话
    private Long legalPersonMobilePhone;
    //单位开户日期(贯标) dwkhrq
    private Date companyOpenedDate = new Date();
    //单位发薪日(贯标) dwfxr
    private String gbCompanyPayDay;
    //经办人姓名(贯标) jbrxm
    private String gbAgentName;
    //经办人证件类型(贯标) jbrzjlx
    private String gbAgentIdCardType;
    //经办人证件类型
    private Long agentIdCardTypeId;
    private Dictionary agentIdCardType;
    //经办人证件号码(贯标) jbrzjhm
    private String gbAgentIdCardNumber;
    //经办人手机号码(贯标) jbrsjhm
    private String gbAgentMobilePhone;
    //经办人固定电话号码(贯标) jbrgddhhm
    private String gbAgentTelephone;
    //受托银行名称(贯标) styhmc
    private String gbTrusteeBank;
    //受托银行代码(贯标) styhdm
    private String gbTrusteeBankCode;
    //受托支行名称
    private String delegationBranch;
    //受托支行代码
    private String delegationBranchCode;
    //缴存网点
    private String paymentSite;
    //区县
    private Long regionId;
    private Region region;
    //联系人移动电话
    private Long contactMobilePhone;
    //联系人姓名
    private String contactName;
    //联系人证件号码
    private String contactIdCardNumber;
    //联系人证件类型
    private Long contactIdCardTypeId;
    private Dictionary contactIdCardType;
    //联系地址
    private String contactAddress;
    //单位代码
    private String unitCode;
    //休息日
    private String restDay;
    //主管区县局代码
    private String responCode;
    //新开户代码
    private String newFlag;
    //客户编码
    private String custNo;
    //中心控制的账户管理状态
    private Long manageState;
    //每月交缴日
    private String payDay;
    //单位信息历史ID
    private Long companyHistoryId;
   

    //关联反馈信息表
    List<CompanyMessage> companyMessages = new ArrayList<>();
    //关联单位信息历史
    List<CompanyHistory> companyHistories = new ArrayList<>();
    //关联单位公积金
    List<CompanyAccountFund> companyAccountFunds = new ArrayList<>();
    //关联  机关/ 企事业单位的员工的就业履历
    List<CompanyRecord> companyRecords = new ArrayList<>();
    //关联  委托转移信息表
    List<DelegationTransfer> delegationTransfers = new ArrayList<>();
    //关联  经办人
    List<Operator> operators = new ArrayList<>();
    //关联  委托收款合同
    List<EntrustedContract> entrustedContracts = new ArrayList<>();
    //关联  个人信息
    List<Person> persons = new ArrayList<>();
    //关联  个人信息历史表
    List<PersonHistory> personHistories = new ArrayList<>();
    //关联  转移信息表
    List<PersonTransferInfo> personTransferInfo = new ArrayList<>();

    public Company(Long id) {
        this.id = id;
    }

    public Company() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCompany")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DWMC")
    public String getGbCompanyName() {
        return gbCompanyName;
    }

    public void setGbCompanyName(String gbCompanyName) {
        this.gbCompanyName = gbCompanyName;
    }

    @Basic
    @Column(name = "ZZJGDM")
    public String getGbOrganizationCode() {
        return gbOrganizationCode;
    }

    public void setGbOrganizationCode(String gbOrganizationCode) {
        this.gbOrganizationCode = gbOrganizationCode;
    }

    @Basic
    @Column(name = "DWZH")
    public String getGbCompanyAccount() {
        return gbCompanyAccount;
    }

    public void setGbCompanyAccount(String gbCompanyAccount) {
        this.gbCompanyAccount = gbCompanyAccount;
    }

    @Basic
    @Column(name = "DWJJLX")
    public String getGbCompanyEconomicType() {
        return gbCompanyEconomicType;
    }

    public void setGbCompanyEconomicType(String gbCompanyEconomicType) {
        this.gbCompanyEconomicType = gbCompanyEconomicType;
    }

    @Basic
    @Column(name = "DWSSHY")
    public String getGbCompanyIndustries() {
        return gbCompanyIndustries;
    }

    public void setGbCompanyIndustries(String gbCompany_Industries) {
        this.gbCompanyIndustries = gbCompany_Industries;
    }

    @Basic
    @Column(name = "DWLSGX")
    public String getGbCompanyRelationships() {
        return gbCompanyRelationships;
    }

    public void setGbCompanyRelationships(String gbCompanyRelationships) {
        this.gbCompanyRelationships = gbCompanyRelationships;
    }

    @Basic
    @Column(name = "DWDZ")
    public String getGbCompanyAddress() {
        return gbCompanyAddress;
    }

    public void setGbCompanyAddress(String gbCompanyAddress) {
        this.gbCompanyAddress = gbCompanyAddress;
    }

    @Basic
    @Column(name = "DWYB")
    public String getGbCompanyPostCode() {
        return gbCompanyPostCode;
    }

    public void setGbCompanyPostCode(String gbCompanyPostCode) {
        this.gbCompanyPostCode = gbCompanyPostCode;
    }

    @Basic
    @Column(name = "DWDZXX")
    public String getGbCompanyEmail() {
        return gbCompanyEmail;
    }

    public void setGbCompanyEmail(String gbCompanyEmail) {
        this.gbCompanyEmail = gbCompanyEmail;
    }

    @Basic
    @Column(name = "DWSLRQ")
    public Date getGbCompanyCreationDate() {
        return gbCompanyCreationDate;
    }

    public void setGbCompanyCreationDate(Date gbCompanyCreationDate) {
        this.gbCompanyCreationDate = gbCompanyCreationDate;
    }

    @Basic
    @Column(name = "DWFRDBZJLX")
    public String getGbLegalPersonIdCardType() {
        return gbLegalPersonIdCardType;
    }

    public void setGbLegalPersonIdCardType(String gbLegalPersonIdCardType) {
        this.gbLegalPersonIdCardType = gbLegalPersonIdCardType;
    }

    @Basic
    @Column(name = "DWFRDBXM")
    public String getGbLegalPersonName() {
        return gbLegalPersonName;
    }

    public void setGbLegalPersonName(String gbLegalPersonName) {
        this.gbLegalPersonName = gbLegalPersonName;
    }

    @Basic
    @Column(name = "DWFRDBZJHM")
    public String getGbLegalPersonIdCardNumber() {
        return gbLegalPersonIdCardNumber;
    }

    public void setGbLegalPersonIdCardNumber(String gbLegalPersonIdCardNumber) {
        this.gbLegalPersonIdCardNumber = gbLegalPersonIdCardNumber;
    }

    @Basic
    @Column(name = "DWKHRQ")
    public Date getCompanyOpenedDate() {
        return companyOpenedDate;
    }

    public void setCompanyOpenedDate(Date companyOpenedDate) {
        this.companyOpenedDate = companyOpenedDate;
    }

    @Basic
    @Column(name = "DWFXR")

    public String getGbCompanyPayDay() {
        return gbCompanyPayDay;
    }

    public void setGbCompanyPayDay(String gbCompanyPayDay) {
        this.gbCompanyPayDay = gbCompanyPayDay;
    }

    @Basic
    @Column(name = "JBRXM")
    public String getGbAgentName() {
        return gbAgentName;
    }

    public void setGbAgentName(String gbAgentName) {
        this.gbAgentName = gbAgentName;
    }

    @Basic
    @Column(name = "JBRZJLX")
    public String getGbAgentIdCardType() {
        return gbAgentIdCardType;
    }

    public void setGbAgentIdCardType(String gbAgentIdCardType) {
        this.gbAgentIdCardType = gbAgentIdCardType;
    }

    @Basic
    @Column(name = "JBRZJHM")
    public String getGbAgentIdCardNumber() {
        return gbAgentIdCardNumber;
    }

    public void setGbAgentIdCardNumber(String gbAgentIdCardNumber) {
        this.gbAgentIdCardNumber = gbAgentIdCardNumber;
    }

    @Basic
    @Column(name = "JBRSJHM")
    public String getGbAgentMobilePhone() {
        return gbAgentMobilePhone;
    }

    public void setGbAgentMobilePhone(String gbAgentMobilePhone) {
        this.gbAgentMobilePhone = gbAgentMobilePhone;
    }

    @Basic
    @Column(name = "JBRGDDHHM")
    public String getGbAgentTelephone() {
        return gbAgentTelephone;
    }

    public void setGbAgentTelephone(String gbAgentTelephone) {
        this.gbAgentTelephone = gbAgentTelephone;
    }

    @Basic
    @Column(name = "STYHMC")
    public String getGbTrusteeBank() {
        return gbTrusteeBank;
    }

    public void setGbTrusteeBank(String gbTrusteeBank) {
        this.gbTrusteeBank = gbTrusteeBank;
    }

    @Basic
    @Column(name = "STYHDM")
    public String getGbTrusteeBankCode() {
        return gbTrusteeBankCode;
    }

    public void setGbTrusteeBankCode(String gbTrusteeBankCode) {
        this.gbTrusteeBankCode = gbTrusteeBankCode;
    }




    @Basic
    @Column(name = "CREDIT_CODE")
    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    @Basic
    @Column(name = "COMPETENT_CODE")
    public String getCompetentCode() {
        return competentCode;
    }

    public void setCompetentCode(String competentCode) {
        this.competentCode = competentCode;
    }

    @Basic
    @Column(name = "NATURE_ACCOUNT")
    public Long getNatureAccountId() {
        return natureAccountId;
    }

    public void setNatureAccountId(Long natureAccountId) {
        this.natureAccountId = natureAccountId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "NATURE_ACCOUNT", insertable = false, updatable = false)
    public Dictionary getNatureAccount() {
        return natureAccount;
    }

    public void setNatureAccount(Dictionary natureAccount) {
        this.natureAccount = natureAccount;
    }

    @Basic
    @Column(name = "COMPANY_INSIDE_CLASSIFICATION")
    public Long getCompanyInsideClassificationId() {
        return companyInsideClassificationId;
    }

    public void setCompanyInsideClassificationId(Long companyInsideClassificationId) {
        this.companyInsideClassificationId = companyInsideClassificationId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "NATURE_ACCOUNT", insertable = false, updatable = false)
    public Dictionary getCompanyInsideClassification() {
        return companyInsideClassification;
    }

    public void setCompanyInsideClassification(Dictionary companyInsideClassification) {
        this.companyInsideClassification = companyInsideClassification;
    }

    @Basic
    @Column(name = "ORGANIZATION_TYPE")
    public Long getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(Long organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    @Basic
    @Column(name = "COMPANY_ECONOMIC_TYPE")
    public Long getCompanyEconomicTypeId() {
        return companyEconomicTypeId;
    }

    public void setCompanyEconomicTypeId(Long companyEconomicTypeId) {
        this.companyEconomicTypeId = companyEconomicTypeId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ECONOMIC_TYPE", insertable = false, updatable = false)
    public Dictionary getCompanyEconomicType() {
        return companyEconomicType;
    }

    public void setCompanyEconomicType(Dictionary companyEconomicType) {
        this.companyEconomicType = companyEconomicType;
    }

    @Basic
    @Column(name = "COMPANY_INDUSTRIES")
    public Long getCompanyIndustriesId() {
        return companyIndustriesId;
    }

    public void setCompanyIndustriesId(Long companyIndustriesId) {
        this.companyIndustriesId = companyIndustriesId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_INDUSTRIES", insertable = false, updatable = false)
    public Dictionary getCompanyIndustries() {
        return companyIndustries;
    }

    public void setCompanyIndustries(Dictionary companyIndustries) {
        this.companyIndustries = companyIndustries;
    }

    @Basic
    @Column(name = "COMPANY_RELATOINSHIPS")
    public Long getCompanyRelatoinshipsId() {
        return companyRelatoinshipsId;
    }

    public void setCompanyRelatoinshipsId(Long companyRelatoinshipsId) {
        this.companyRelatoinshipsId = companyRelatoinshipsId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "NATURE_ACCOUNT", insertable = false, updatable = false)
    public Dictionary getCompanyRelatoinships() {
        return companyRelatoinships;
    }

    public void setCompanyRelatoinships(Dictionary companyRelatoinships) {
        this.companyRelatoinships = companyRelatoinships;
    }

    @Basic
    @Column(name = "REGISTERED_ADDRESS")
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    @Basic
    @Column(name = "REGISTERED_POST_CODE")
    public Long getRegisteredPostCode() {
        return registeredPostCode;
    }

    public void setRegisteredPostCode(Long registeredPostCode) {
        this.registeredPostCode = registeredPostCode;
    }

    @Basic
    @Column(name = "COMPANY_ADDRESS")
    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    @Basic
    @Column(name = "IS_LEGAL_PERSONALITY")
    public Boolean getLegalPersonality() {
        return legalPersonality;
    }

    public void setLegalPersonality(Boolean legalPersonality) {
        this.legalPersonality = legalPersonality;
    }

    @Basic
    @Column(name = "LEGAL_PERSON_ID_CARD_TYPE")
    public Long getLegalPersonIdCardTypeId() {
        return legalPersonIdCardTypeId;
    }

    public void setLegalPersonIdCardTypeId(Long legalPersonIdCardTypeId) {
        this.legalPersonIdCardTypeId = legalPersonIdCardTypeId;
    }

    @Basic
    @Column(name = "LEGAL_PERSON_MOBILE_PHONE")
    public Long getLegalPersonMobilePhone() {
        return legalPersonMobilePhone;
    }

    public void setLegalPersonMobilePhone(Long legalPersonMobilePhone) {
        this.legalPersonMobilePhone = legalPersonMobilePhone;
    }

    @Basic
    @Column(name = "AGENT_ID_CARD_TYPE")
    public Long getAgentIdCardTypeId() {
        return agentIdCardTypeId;
    }

    public void setAgentIdCardTypeId(Long agentIdCardTypeId) {
        this.agentIdCardTypeId = agentIdCardTypeId;
    }

    @Basic
    @Column(name = "DELEGATION_BRANCH")
    public String getDelegationBranch() {
        return delegationBranch;
    }

    public void setDelegationBranch(String delegationBranch) {
        this.delegationBranch = delegationBranch;
    }

    @Basic
    @Column(name = "DELEGATION_BRANCH_CODE")
    public String getDelegationBranchCode() {
        return delegationBranchCode;
    }

    public void setDelegationBranchCode(String delegationBranchCode) {
        this.delegationBranchCode = delegationBranchCode;
    }




    @Basic
    @Column(name = "PAYMENT_SITE")
    public String getPaymentSite() {
        return paymentSite;
    }

    public void setPaymentSite(String paymentSite) {
        this.paymentSite = paymentSite;
    }
    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "CONTACT_MOBILE_PHONE")
    public Long getContactMobilePhone() {
        return contactMobilePhone;
    }

    public void setContactMobilePhone(Long contactMobilePhone) {
        this.contactMobilePhone = contactMobilePhone;
    }

    @Basic
    @Column(name = "CONTACT_NAME")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "CONTACT_ID_CARD_NUMBER")
    public String getContactIdCardNumber() {
        return contactIdCardNumber;
    }

    public void setContactIdCardNumber(String contactIdCardNumber) {
        this.contactIdCardNumber = contactIdCardNumber;
    }

    @Basic
    @Column(name = "CONTACT_ID_CARD_TYPE")
    public Long getContactIdCardTypeId() {
        return contactIdCardTypeId;
    }

    public void setContactIdCardTypeId(Long contactIdCardTypeId) {
        this.contactIdCardTypeId = contactIdCardTypeId;
    }

    @Basic
    @Column(name = "CONTACT_ADDRESS")
    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @Basic
    @Column(name = "UNIT_CODE")
    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    @Basic
    @Column(name = "REST_DAY")
    public String getRestDay() {
        return restDay;
    }

    public void setRestDay(String restDay) {
        this.restDay = restDay;
    }

    @Basic
    @Column(name = "RESPON_CODE")
    public String getResponCode() {
        return responCode;
    }

    public void setResponCode(String responCode) {
        this.responCode = responCode;
    }

    @Basic
    @Column(name = "NEW_FLAG")
    public String getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(String newFlag) {
        this.newFlag = newFlag;
    }

    @Basic
    @Column(name = "CUST_NO")
    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    @Basic
    @Column(name = "MANAGE_STATE")
    public Long getManageState() {
        return manageState;
    }

    public void setManageState(Long manageState) {
        this.manageState = manageState;
    }

    @Basic
    @Column(name = "PAY_DAY")
    public String getPayDay() {
        return payDay;
    }

    public void setPayDay(String payDay) {
        this.payDay = payDay;
    }

    @Basic
    @Column(name = "COMPANY_HISTORY_ID")
    public Long getCompanyHistoryId() {
        return companyHistoryId;
    }

    public void setCompanyHistoryId(Long companyHistoryId) {
        this.companyHistoryId = companyHistoryId;
    }


    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ORGANIZATION_TYPE", insertable = false, updatable = false)
    public Dictionary getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(Dictionary organizationType) {
        this.organizationType = organizationType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "LEGAL_PERSON_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getLegalPersonIdCardType() {
        return legalPersonIdCardType;
    }

    public void setLegalPersonIdCardType(Dictionary legalPersonIdCardType) {
        this.legalPersonIdCardType = legalPersonIdCardType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "AGENT_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getAgentIdCardType() {
        return agentIdCardType;
    }

    public void setAgentIdCardType(Dictionary agentIdCardType) {
        this.agentIdCardType = agentIdCardType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getContactIdCardType() {
        return contactIdCardType;
    }

    public void setContactIdCardType(Dictionary contactIdCardType) {
        this.contactIdCardType = contactIdCardType;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("gbCompanyName", getGbCompanyName())
                .append("gbOrganizationCode", getGbOrganizationCode()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof Company))
            return false;
        Company otherCompany = (Company) other;
        return new EqualsBuilder().append(this.getId(),
                otherCompany.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @OneToMany(mappedBy="company", targetEntity=CompanyMessage.class, fetch = FetchType.LAZY)
    public List<CompanyMessage> getCompanyMessages() {
        return companyMessages;
    }

    public void setCompanyMessages(List<CompanyMessage> companyMessages) {
        this.companyMessages = companyMessages;
    }



    @OneToMany(mappedBy="company", targetEntity=PersonTransferInfo.class, fetch = FetchType.LAZY)
    public List<PersonTransferInfo> getPersonTransferInfo() {
        return personTransferInfo;
    }

    public void setPersonTransferInfo(List<PersonTransferInfo> personTransferInfo) {
       this.personTransferInfo = personTransferInfo;
    }

    @OneToMany(mappedBy="company", targetEntity=CompanyHistory.class, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    public List<CompanyHistory> getCompanyHistories() {
        return companyHistories;
    }

    public void setCompanyHistories(List<CompanyHistory> companyHistories) {
        this.companyHistories = companyHistories;
    }

    @OneToMany(targetEntity=CompanyAccountFund.class, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="COMPANY_ID")
    @OrderBy(value="id desc")
    public List<CompanyAccountFund> getCompanyAccountFunds() {
        return this.companyAccountFunds;
    }

    public void setCompanyAccountFunds(List<CompanyAccountFund> companyAccountFunds) {
        this.companyAccountFunds = companyAccountFunds;
    }

    @OneToMany(mappedBy="company", targetEntity=CompanyRecord.class, fetch = FetchType.LAZY)
    public List<CompanyRecord> getCompanyRecords() {
        return this.companyRecords;
    }

    public void setCompanyRecords(List<CompanyRecord> companyRecords) {
        this.companyRecords = companyRecords;
    }

    @OneToMany(mappedBy="company", targetEntity=DelegationTransfer.class, fetch = FetchType.LAZY)
    public List<DelegationTransfer> getDelegationTransfers() {
        return this.delegationTransfers;
    }

    public void setDelegationTransfers(List<DelegationTransfer> delegationTransfers) {
        this.delegationTransfers = delegationTransfers;
    }

    @OneToMany(mappedBy="company", targetEntity=Operator.class, fetch = FetchType.LAZY)
    public List<Operator> getOperators() {
        return this.operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

    @OneToMany(mappedBy="company", targetEntity=EntrustedContract.class, fetch = FetchType.LAZY)
    public List<EntrustedContract> getEntrustedContracts() {
        return this.entrustedContracts;
    }

    public void setEntrustedContracts(List<EntrustedContract> entrustedContracts) {
        this.entrustedContracts = entrustedContracts;
    }

    @OneToMany(mappedBy="company", targetEntity=Person.class, fetch = FetchType.LAZY)
    public List<Person> getPersons() {
        return this.persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @OneToMany(mappedBy="company", targetEntity=PersonHistory.class, fetch = FetchType.LAZY)
    public List<PersonHistory> getPersonHistories() {
        return this.personHistories;
    }

    public void setPersonHistories(List<PersonHistory> personHistories) {
        this.personHistories = personHistories;
    }

}
