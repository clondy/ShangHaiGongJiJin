package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 项目贷款账户信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:36
 */
@Entity
@Table(name = "PROJECT_LOAN_ACCOUNT_MESSAGE")
public class ProjectLoanAccountMessage {
    private long id;
    private Long nyfxll;
    private Date dkjqrq;
    private long dkye;
    private Long yqlx;
    private long hslxze;
    private Long tqhkbjze;
    private long hsbjze;
    private String jkhtbh;
    private Long yqbj;
    private Long fxze;
    private Long yqfxll;
    private String dkfxdj;
    private String dkzh;
    private long dkll;

    public ProjectLoanAccountMessage(long id) {
        this.id = id;
    }

    public ProjectLoanAccountMessage() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NYFXLL")
    public Long getNyfxll() {
        return nyfxll;
    }

    public void setNyfxll(Long nyfxll) {
        this.nyfxll = nyfxll;
    }

    @Basic
    @Column(name = "DKJQRQ")
    public Date getDkjqrq() {
        return dkjqrq;
    }

    public void setDkjqrq(Date dkjqrq) {
        this.dkjqrq = dkjqrq;
    }

    @Basic
    @Column(name = "DKYE")
    public long getDkye() {
        return dkye;
    }

    public void setDkye(long dkye) {
        this.dkye = dkye;
    }

    @Basic
    @Column(name = "YQLX")
    public Long getYqlx() {
        return yqlx;
    }

    public void setYqlx(Long yqlx) {
        this.yqlx = yqlx;
    }

    @Basic
    @Column(name = "HSLXZE")
    public long getHslxze() {
        return hslxze;
    }

    public void setHslxze(long hslxze) {
        this.hslxze = hslxze;
    }

    @Basic
    @Column(name = "TQHKBJZE")
    public Long getTqhkbjze() {
        return tqhkbjze;
    }

    public void setTqhkbjze(Long tqhkbjze) {
        this.tqhkbjze = tqhkbjze;
    }

    @Basic
    @Column(name = "HSBJZE")
    public long getHsbjze() {
        return hsbjze;
    }

    public void setHsbjze(long hsbjze) {
        this.hsbjze = hsbjze;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "YQBJ")
    public Long getYqbj() {
        return yqbj;
    }

    public void setYqbj(Long yqbj) {
        this.yqbj = yqbj;
    }

    @Basic
    @Column(name = "FXZE")
    public Long getFxze() {
        return fxze;
    }

    public void setFxze(Long fxze) {
        this.fxze = fxze;
    }

    @Basic
    @Column(name = "YQFXLL")
    public Long getYqfxll() {
        return yqfxll;
    }

    public void setYqfxll(Long yqfxll) {
        this.yqfxll = yqfxll;
    }

    @Basic
    @Column(name = "DKFXDJ")
    public String getDkfxdj() {
        return dkfxdj;
    }

    public void setDkfxdj(String dkfxdj) {
        this.dkfxdj = dkfxdj;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    @Basic
    @Column(name = "DKLL")
    public long getDkll() {
        return dkll;
    }

    public void setDkll(long dkll) {
        this.dkll = dkll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectLoanAccountMessage that = (ProjectLoanAccountMessage) o;

        if (id != that.id) return false;
        if (dkye != that.dkye) return false;
        if (hslxze != that.hslxze) return false;
        if (hsbjze != that.hsbjze) return false;
        if (dkll != that.dkll) return false;
        if (nyfxll != null ? !nyfxll.equals(that.nyfxll) : that.nyfxll != null) return false;
        if (dkjqrq != null ? !dkjqrq.equals(that.dkjqrq) : that.dkjqrq != null) return false;
        if (yqlx != null ? !yqlx.equals(that.yqlx) : that.yqlx != null) return false;
        if (tqhkbjze != null ? !tqhkbjze.equals(that.tqhkbjze) : that.tqhkbjze != null) return false;
        if (jkhtbh != null ? !jkhtbh.equals(that.jkhtbh) : that.jkhtbh != null) return false;
        if (yqbj != null ? !yqbj.equals(that.yqbj) : that.yqbj != null) return false;
        if (fxze != null ? !fxze.equals(that.fxze) : that.fxze != null) return false;
        if (yqfxll != null ? !yqfxll.equals(that.yqfxll) : that.yqfxll != null) return false;
        if (dkfxdj != null ? !dkfxdj.equals(that.dkfxdj) : that.dkfxdj != null) return false;
        if (dkzh != null ? !dkzh.equals(that.dkzh) : that.dkzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nyfxll != null ? nyfxll.hashCode() : 0);
        result = 31 * result + (dkjqrq != null ? dkjqrq.hashCode() : 0);
        result = 31 * result + (int) (dkye ^ (dkye >>> 32));
        result = 31 * result + (yqlx != null ? yqlx.hashCode() : 0);
        result = 31 * result + (int) (hslxze ^ (hslxze >>> 32));
        result = 31 * result + (tqhkbjze != null ? tqhkbjze.hashCode() : 0);
        result = 31 * result + (int) (hsbjze ^ (hsbjze >>> 32));
        result = 31 * result + (jkhtbh != null ? jkhtbh.hashCode() : 0);
        result = 31 * result + (yqbj != null ? yqbj.hashCode() : 0);
        result = 31 * result + (fxze != null ? fxze.hashCode() : 0);
        result = 31 * result + (yqfxll != null ? yqfxll.hashCode() : 0);
        result = 31 * result + (dkfxdj != null ? dkfxdj.hashCode() : 0);
        result = 31 * result + (dkzh != null ? dkzh.hashCode() : 0);
        result = 31 * result + (int) (dkll ^ (dkll >>> 32));
        return result;
    }
}
