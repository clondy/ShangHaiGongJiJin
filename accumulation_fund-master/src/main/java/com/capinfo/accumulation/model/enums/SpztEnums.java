package com.capinfo.accumulation.model.enums;

/**
 * 审批状态
 *
 */
public enum SpztEnums {
	  待审批(1) , 初审通过(2), 初审不通过(3);
	// 成员变量
	private int token;
	private SpztEnums name;

	// 构造方法
	private SpztEnums(int token) {
		
		this.token = token;
	}

	public int getToken() {
		
		return token;
	}

	public SpztEnums getName() {
		
		name= SpztEnums.getObject(this.token);
		return name;
	}

	public static SpztEnums getObject(int token) {

		switch (token) {
		case 1:
			return 待审批;
		case 2:
			return 初审通过;
		case 3:
			return 初审不通过;
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(SpztEnums.初审不通过.getToken());
	}
}