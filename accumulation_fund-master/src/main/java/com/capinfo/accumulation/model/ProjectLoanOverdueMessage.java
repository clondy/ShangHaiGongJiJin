package com.capinfo.accumulation.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @Description: 项目贷款逾期登记信息(贯标)
 * @Author: zhangxu
 * @Date 2017/9/20 14:37
 */
@Entity
@Table(name = "PROJECT_LOAN_OVERDUE_MESSAGE")
public class ProjectLoanOverdueMessage {
    private long id;
    private Long yqfx;
    private Long yqlx;
    private Date ssrq;
    private Long ssyqlx;
    private Long ssyqbj;
    private Long yqbj;
    private Long ssyqfx;
    private String dkzh;

    public ProjectLoanOverdueMessage(long id) {
        this.id = id;
    }

    public ProjectLoanOverdueMessage() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "YQFX")
    public Long getYqfx() {
        return yqfx;
    }

    public void setYqfx(Long yqfx) {
        this.yqfx = yqfx;
    }

    @Basic
    @Column(name = "YQLX")
    public Long getYqlx() {
        return yqlx;
    }

    public void setYqlx(Long yqlx) {
        this.yqlx = yqlx;
    }

    @Basic
    @Column(name = "SSRQ")
    public Date getSsrq() {
        return ssrq;
    }

    public void setSsrq(Date ssrq) {
        this.ssrq = ssrq;
    }

    @Basic
    @Column(name = "SSYQLX")
    public Long getSsyqlx() {
        return ssyqlx;
    }

    public void setSsyqlx(Long ssyqlx) {
        this.ssyqlx = ssyqlx;
    }

    @Basic
    @Column(name = "SSYQBJ")
    public Long getSsyqbj() {
        return ssyqbj;
    }

    public void setSsyqbj(Long ssyqbj) {
        this.ssyqbj = ssyqbj;
    }

    @Basic
    @Column(name = "YQBJ")
    public Long getYqbj() {
        return yqbj;
    }

    public void setYqbj(Long yqbj) {
        this.yqbj = yqbj;
    }

    @Basic
    @Column(name = "SSYQFX")
    public Long getSsyqfx() {
        return ssyqfx;
    }

    public void setSsyqfx(Long ssyqfx) {
        this.ssyqfx = ssyqfx;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectLoanOverdueMessage that = (ProjectLoanOverdueMessage) o;

        if (id != that.id) return false;
        if (yqfx != null ? !yqfx.equals(that.yqfx) : that.yqfx != null) return false;
        if (yqlx != null ? !yqlx.equals(that.yqlx) : that.yqlx != null) return false;
        if (ssrq != null ? !ssrq.equals(that.ssrq) : that.ssrq != null) return false;
        if (ssyqlx != null ? !ssyqlx.equals(that.ssyqlx) : that.ssyqlx != null) return false;
        if (ssyqbj != null ? !ssyqbj.equals(that.ssyqbj) : that.ssyqbj != null) return false;
        if (yqbj != null ? !yqbj.equals(that.yqbj) : that.yqbj != null) return false;
        if (ssyqfx != null ? !ssyqfx.equals(that.ssyqfx) : that.ssyqfx != null) return false;
        if (dkzh != null ? !dkzh.equals(that.dkzh) : that.dkzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (yqfx != null ? yqfx.hashCode() : 0);
        result = 31 * result + (yqlx != null ? yqlx.hashCode() : 0);
        result = 31 * result + (ssrq != null ? ssrq.hashCode() : 0);
        result = 31 * result + (ssyqlx != null ? ssyqlx.hashCode() : 0);
        result = 31 * result + (ssyqbj != null ? ssyqbj.hashCode() : 0);
        result = 31 * result + (yqbj != null ? yqbj.hashCode() : 0);
        result = 31 * result + (ssyqfx != null ? ssyqfx.hashCode() : 0);
        result = 31 * result + (dkzh != null ? dkzh.hashCode() : 0);
        return result;
    }
}
