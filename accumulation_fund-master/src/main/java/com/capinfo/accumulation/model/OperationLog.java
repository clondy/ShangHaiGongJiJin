package com.capinfo.accumulation.model;


import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;


/**
 * Comment 系统的操作日志
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "OPERATION_LOG")
@SequenceGenerator(name = "seqOPERATION_LOG", sequenceName = "SEQ_OPERATION_LOG", allocationSize = 1)
public class OperationLog implements BaseEntity {

    private static final long serialVersionUID = 1L;

    //ID
    private Long id;

    //访问地址
    private String requestAddress;

    //请求内容
    private String requestContent;

    //用户ID
    private Long userId;

    //session_id
    private String sessionId;

    //远程地址
    private String remoteIp;

    //请求头
    private String requestHead;

    //操作时间
    private Date operateTime;

    //操作时间
    private Date dateTime;

    //银行编码
    private Long bankCode;

    //操作员
    private String operatorId;

    //区县
    private Long regionId;

    //网点
    private Long siteId;

    //复核人
    private Long confirmerId;

    //复核时间
    private Date confirmationTime;

    //渠道
    private Long channels;

    //创建人名称
    private String createName;


    public OperationLog(){

    }

    public OperationLog(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqOPERATION_LOG")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "REQUEST_ADDRESS")
    public String getRequestAddress() {
        return requestAddress;
    }

    public void setRequestAddress(String requestAddress) {
        this.requestAddress = requestAddress;
    }

    @Basic
    @Column(name = "REQUEST_CONTENT")
    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    @Basic
    @Column(name = "USER_ID")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "SESSION_ID")
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Basic
    @Column(name = "REMOTE_IP")
    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    @Basic
    @Column(name = "REQUEST_HEAD")
    public String getRequestHead() {
        return requestHead;
    }

    public void setRequestHead(String requestHead) {
        this.requestHead = requestHead;
    }

    @Basic
    @Column(name = "OPERATE_TIME")
    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof OperationLog))
            return false;
        OperationLog castOther = (OperationLog)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
