package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: 交易流水（收/ 付款）银行结算流水信息
 * @Author: zhangxu
 * @Date 2017/9/20 18:33
 */
@Entity
@Table(name = "TRANSACTION_RECORD")
@SequenceGenerator(name = "seqTransactionRecord", sequenceName = "SEQ_TRANSACTION_RECORD", allocationSize = 1)
public class TransactionRecord implements BaseEntity {

    private static final long serialVersionUID = -3312925183075041885L;
    private Long id;
    //银行专户id
    private Long accountBankSpecialId;
    private AccountBankSpecial accountBankSpecial;
    //单位公积金_id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund;
    //个人公积金_id
    private Long personAccountFundId;
    private PersonAccountFund personAccountFund;
    //收款银行代码(贯标) SKYHDM
    private String gbCollectionBankCode;
    //收款银行代码
    private Long collectionBankCodeId;
    private Dictionary collectionBankCode;
    //收款账户名称(贯标)    SKZHMC
    private String collectionBankName;
    //收款账户号码(贯标)    SKZHHM
    private String collectionBankNum;
    //付款银行代码(贯标)    FKYHDM
    private String gbPaymentBankCode;
    //付款银行代码
    private Long paymentBankCodeId;
    private Dictionary paymentBankCode;
    //付款账户名称(贯标) FKZHMC
    private String paymentBankName;
    //付款账户号码(贯标) FKZHHM
    private String paymentBankNum;
    //银行结算流水号(贯标)   YHJSLSH
    private String bankJournalNumber;
    //结算发生日期(贯标)    JSFSRQ
    private Date journalDate;
    //结算银行代码(贯标)    JSYHDM
    private String gbJournalBankCode;
    //结算银行代码

    private Long journalBankCodeId;
    private Dictionary journalBankCode;

    //资金业务类型(贯标)    ZJYWLX
    private String gbFundBusinessType;
    //资金业务类型
    private Long fundBusinessTypeId;
    private Dictionary fundBusinessType;
    //业务凭证号码(贯标)    YWPZHM
    private String serviceDocumentNum;
    //业务流水号(贯标) YWLSH
    private String businessNumber;
    //发生额(贯标)   FSE
    private BigDecimal accrua;
    //摘要(贯标)    ZHAIYAO
    private String digest;
    //结算模式
    private Long settlementPatternId;
    private Dictionary settlementPattern;
    //结算类别
    private Long settlementCategoryId;
    private Dictionary settlementCategory;
    //批量项目编号
    private String batchProjectNumber;
    //账户类别
    private Long draweeAccountTypeId;
    private Dictionary draweeAccountType;
    //备注
    private String content;
    //付息户账号
    private Long negativeInterestAccount;
    //付息户户名
    private String negativeInterestName;
    //付息账户类别
    private Long negativeInterestTypeId;
    private Dictionary negativeInterestType;
    //利息收方账号
    private Long interestPayeeAccount;
    //付方联行号
    private Long draweeLineNumber;
    //付方账户行别
    private Long bankId;
    private Dictionary bank;
    //收方行名
    private String payeeBankName;
    //收方联行号
    private Long payeeLineNumber;
    //多方协议号
    private String protocolNumber;
    //金额
    private BigDecimal amount;
    //本金发生额
    private BigDecimal accrualPrincipal;
    //利息发生额
    private BigDecimal accrualInterest;
    //合计金额
    private BigDecimal totalAmount;
    //业务明细账号
    private Long businessDetails;
    //业务明细流水号
    private Long businessDetailsNumber;
    //业务明细流水号1
    private Long businessDetailsNumber1;
    //业务明细流水号2
    private Long businessDetailsNumber2;
    //付款利息金额
    private BigDecimal paymentInterests;
    //总笔数
    private Integer allAmout;
    //总金额
    private BigDecimal allAmount;
    //文件列表
    private String fileList;
    //发生时间
    private Date dateTime;


    List<USER_GGJ> userGjjs = new ArrayList<>();
    List<TransactionRecordDetail> transactionRecordDetails = new ArrayList<>();

    public TransactionRecord(Long id) {
        this.id = id;
    }

    public TransactionRecord() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqTransactionRecord")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ACCOUNT_BANK_SPECIAL_ID")
    public Long getAccountBankSpecialId() {
        return accountBankSpecialId;
    }

    public void setAccountBankSpecialId(Long accountBankSpecialId) {
        this.accountBankSpecialId = accountBankSpecialId;
    }

    @ManyToOne(targetEntity = AccountBankSpecial.class)
    @JoinColumn(name = "ACCOUNT_BANK_SPECIAL_ID", insertable = false, updatable = false)
    public AccountBankSpecial getAccountBankSpecial() {
        return accountBankSpecial;
    }

    public void setAccountBankSpecial(AccountBankSpecial accountBankSpecial) {
        this.accountBankSpecial = accountBankSpecial;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }
    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }
    @ManyToOne(targetEntity = PersonAccountFund.class)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    @Basic
    @Column(name = "SKYHDM")
    public String getGbCollectionBankCode() {
        return gbCollectionBankCode;
    }

    public void setGbCollectionBankCode(String gbCollectionBankCode) {
        this.gbCollectionBankCode = gbCollectionBankCode;
    }

    @Basic
    @Column(name = "COLLECTION_BANK_CODE")
    public Long getCollectionBankCodeId() {
        return collectionBankCodeId;
    }

    public void setCollectionBankCodeId(Long collectionBankCodeId) {
        this.collectionBankCodeId = collectionBankCodeId;
    }

    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "COLLECTION_BANK_CODE", insertable = false, updatable = false)
    public Dictionary getCollectionBankCode() {
        return collectionBankCode;
    }

    public void setCollectionBankCode(Dictionary collectionBankCode) {
        this.collectionBankCode = collectionBankCode;
    }

    @Basic
    @Column(name = "SKZHMC")
    public String getCollectionBankName() {
        return collectionBankName;
    }

    public void setCollectionBankName(String collectionBankName) {
        this.collectionBankName = collectionBankName;
    }

    @Basic
    @Column(name = "SKZHHM")
    public String getCollectionBankNum() {
        return collectionBankNum;
    }

    public void setCollectionBankNum(String collectionBankNum) {
        this.collectionBankNum = collectionBankNum;
    }

    @Basic
    @Column(name = "FKYHDM")
    public String getGbPaymentBankCode() {
        return gbPaymentBankCode;
    }

    public void setGbPaymentBankCode(String gbPaymentBankCode) {
        this.gbPaymentBankCode = gbPaymentBankCode;
    }

    @Basic
    @Column(name = "PAYMENT_BANK_CODE")
    public Long getPaymentBankCodeId() {
        return paymentBankCodeId;
    }

    public void setPaymentBankCodeId(Long paymentBankCodeId) {
        this.paymentBankCodeId = paymentBankCodeId;
    }

    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "PAYMENT_BANK_CODE", insertable = false, updatable = false)
    public Dictionary getPaymentBankCode() {
        return paymentBankCode;
    }

    public void setPaymentBankCode(Dictionary paymentBankCode) {
        this.paymentBankCode = paymentBankCode;
    }

    @Basic
    @Column(name = "FKZHMC")
    public String getPaymentBankName() {
        return paymentBankName;
    }

    public void setPaymentBankName(String paymentBankName) {
        this.paymentBankName = paymentBankName;
    }

    @Basic
    @Column(name = "FKZHHM")
    public String getPaymentBankNum() {
        return paymentBankNum;
    }

    public void setPaymentBankNum(String paymentBankNum) {
        this.paymentBankNum = paymentBankNum;
    }

    @Basic
    @Column(name = "YHJSLSH")
    public String getBankJournalNumber() {
        return bankJournalNumber;
    }

    public void setBankJournalNumber(String bankJournalNumber) {
        this.bankJournalNumber = bankJournalNumber;
    }

    @Basic
    @Column(name = "JSFSRQ")
    public Date getJournalDate() {
        return journalDate;
    }

    public void setJournalDate(Date journalDate) {
        this.journalDate = journalDate;
    }

    @Basic
    @Column(name = "JSYHDM")
    public String getGbJournalBankCode() {
        return gbJournalBankCode;
    }

    public void setGbJournalBankCode(String gbJournalBankCode) {
        this.gbJournalBankCode = gbJournalBankCode;
    }

    @Basic
    @Column(name = "JOURNAL_BANK_CODE")
    public Long getJournalBankCodeId() {
        return journalBankCodeId;
    }

    public void setJournalBankCodeId(Long journalBankCodeId) {
        this.journalBankCodeId = journalBankCodeId;
    }

    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "JOURNAL_BANK_CODE", insertable = false, updatable = false)
    public Dictionary getJournalBankCode() {
        return journalBankCode;
    }

    public void setJournalBankCode(Dictionary journalBankCode) {
        this.journalBankCode = journalBankCode;
    }

    @Basic
    @Column(name = "ZJYWLX")
    public String getGbFundBusinessType() {
        return gbFundBusinessType;
    }

    public void setGbFundBusinessType(String gbFundBusinessType) {
        this.gbFundBusinessType = gbFundBusinessType;
    }

    @Basic
    @Column(name = "fund_Business_Type")
    public Long getFundBusinessTypeId() {
        return fundBusinessTypeId;
    }

    public void setFundBusinessTypeId(Long fundBusinessTypeId) {
        this.fundBusinessTypeId = fundBusinessTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "fund_Business_Type", insertable = false, updatable = false)
    public Dictionary getFundBusinessType() {
        return fundBusinessType;
    }

    public void setFundBusinessType(Dictionary fundBusinessType) {
        this.fundBusinessType = fundBusinessType;
    }

    @Basic
    @Column(name = "YWPZHM")
    public String getServiceDocumentNum() {
        return serviceDocumentNum;
    }

    public void setServiceDocumentNum(String serviceDocumentNum) {
        this.serviceDocumentNum = serviceDocumentNum;
    }

    @Basic
    @Column(name = "YWLSH")
    public String getBusinessNumber() {
        return businessNumber;
    }

    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    @Basic
    @Column(name = "FSE")
    public BigDecimal getAccrua() {
        return accrua;
    }

    public void setAccrua(BigDecimal accrua) {
        this.accrua = accrua;
    }

    @Basic
    @Column(name = "ZHAIYAO")
    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    @Basic
    @Column(name = "SETTLEMENT_PATTERN")
    public Long getSettlementPatternId() {
        return settlementPatternId;
    }

    public void setSettlementPatternId(Long settlementPatternId) {
        this.settlementPatternId = settlementPatternId;
    }
    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "SETTLEMENT_PATTERN", insertable = false, updatable = false)
    public Dictionary getSettlementPattern() {
        return settlementPattern;
    }

    public void setSettlementPattern(Dictionary settlementPattern) {
        this.settlementPattern = settlementPattern;
    }
    @Basic
    @Column(name = "SETTLEMENT_CATEGORY")
    public Long getSettlementCategoryId() {
        return settlementCategoryId;
    }

    public void setSettlementCategoryId(Long settlementCategoryId) {
        this.settlementCategoryId = settlementCategoryId;
    }
    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "SETTLEMENT_CATEGORY", insertable = false, updatable = false)
    public Dictionary getSettlementCategory() {
        return settlementCategory;
    }

    public void setSettlementCategory(Dictionary settlementCategory) {
        this.settlementCategory = settlementCategory;
    }

    @Basic
    @Column(name = "BATCH_PROJECT_NUMBER")
    public String getBatchProjectNumber() {
        return batchProjectNumber;
    }

    public void setBatchProjectNumber(String batchProjectNumber) {
        this.batchProjectNumber = batchProjectNumber;
    }

    @Basic
    @Column(name = "DRAWEE_ACCOUNT_TYPE")
    public Long getDraweeAccountTypeId() {
        return draweeAccountTypeId;
    }

    public void setDraweeAccountTypeId(Long draweeAccountTypeId) {
        this.draweeAccountTypeId = draweeAccountTypeId;
    }
    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "DRAWEE_ACCOUNT_TYPE", insertable = false, updatable = false)
    public Dictionary getDraweeAccountType() {
        return draweeAccountType;
    }

    public void setDraweeAccountType(Dictionary draweeAccountType) {
        this.draweeAccountType = draweeAccountType;
    }

    @Basic
    @Column(name = "CONTENT")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "NEGATIVE_INTEREST_ACCOUNT")
    public Long getNegativeInterestAccount() {
        return negativeInterestAccount;
    }

    public void setNegativeInterestAccount(Long negativeInterestAccount) {
        this.negativeInterestAccount = negativeInterestAccount;
    }

    @Basic
    @Column(name = "NEGATIVE_INTEREST_NAME")
    public String getNegativeInterestName() {
        return negativeInterestName;
    }

    public void setNegativeInterestName(String negativeInterestName) {
        this.negativeInterestName = negativeInterestName;
    }

    @Basic
    @Column(name = "NEGATIVE_INTEREST_TYPE")
    public Long getNegativeInterestTypeId() {
        return negativeInterestTypeId;
    }

    public void setNegativeInterestTypeId(Long negativeInterestTypeId) {
        this.negativeInterestTypeId = negativeInterestTypeId;
    }
    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "NEGATIVE_INTEREST_TYPE", insertable = false, updatable = false)
    public Dictionary getNegativeInterestType() {
        return negativeInterestType;
    }

    public void setNegativeInterestType(Dictionary negativeInterestType) {
        this.negativeInterestType = negativeInterestType;
    }

    @Basic
    @Column(name = "INTEREST_PAYEE_ACCOUNT")
    public Long getInterestPayeeAccount() {
        return interestPayeeAccount;
    }

    public void setInterestPayeeAccount(Long interestPayeeAccount) {
        this.interestPayeeAccount = interestPayeeAccount;
    }

    @Basic
    @Column(name = "drawee_Line number")
    public Long getDraweeLineNumber() {
        return draweeLineNumber;
    }

    public void setDraweeLineNumber(Long draweeLineNumber) {
        this.draweeLineNumber = draweeLineNumber;
    }

    @Basic
    @Column(name = "IS_BANK")
    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }
    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "IS_BANK", insertable = false, updatable = false)
    public Dictionary getBank() {
        return bank;
    }

    public void setBank(Dictionary bank) {
        this.bank = bank;
    }

    @Basic
    @Column(name = "PAYEE_BANK_NAME")
    public String getPayeeBankName() {
        return payeeBankName;
    }

    public void setPayeeBankName(String payeeBankName) {
        this.payeeBankName = payeeBankName;
    }

    @Basic
    @Column(name = "payee_Line number")
    public Long getPayeeLineNumber() {
        return payeeLineNumber;
    }

    public void setPayeeLineNumber(Long payeeLineNumber) {
        this.payeeLineNumber = payeeLineNumber;
    }

    @Basic
    @Column(name = "PROTOCOL_NUMBER")
    public String getProtocolNumber() {
        return protocolNumber;
    }

    public void setProtocolNumber(String protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    @Basic
    @Column(name = "AMOUNT")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "ACCRUAL_PRINCIPAL")
    public BigDecimal getAccrualPrincipal() {
        return accrualPrincipal;
    }

    public void setAccrualPrincipal(BigDecimal accrualPrincipal) {
        this.accrualPrincipal = accrualPrincipal;
    }

    @Basic
    @Column(name = "ACCRUAL_INTEREST")
    public BigDecimal getAccrualInterest() {
        return accrualInterest;
    }

    public void setAccrualInterest(BigDecimal accrualInterest) {
        this.accrualInterest = accrualInterest;
    }

    @Basic
    @Column(name = "TOTAL_AMOUNT")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "BUSINESS_DETAILS")
    public Long getBusinessDetails() {
        return businessDetails;
    }

    public void setBusinessDetails(Long businessDetails) {
        this.businessDetails = businessDetails;
    }

    @Basic
    @Column(name = "BUSINESS_DETAILS_NUMBER")
    public Long getBusinessDetailsNumber() {
        return businessDetailsNumber;
    }

    public void setBusinessDetailsNumber(Long businessDetailsNumber) {
        this.businessDetailsNumber = businessDetailsNumber;
    }

    @Basic
    @Column(name = "BUSINESS_DETAILS_NUMBER_1")
    public Long getBusinessDetailsNumber1() {
        return businessDetailsNumber1;
    }

    public void setBusinessDetailsNumber1(Long businessDetailsNumber1) {
        this.businessDetailsNumber1 = businessDetailsNumber1;
    }

    @Basic
    @Column(name = "BUSINESS_DETAILS_NUMBER_2")
    public Long getBusinessDetailsNumber2() {
        return businessDetailsNumber2;
    }

    public void setBusinessDetailsNumber2(Long businessDetailsNumber2) {
        this.businessDetailsNumber2 = businessDetailsNumber2;
    }

    @Basic
    @Column(name = "PAYMENT_INTERESTS")
    public BigDecimal getPaymentInterests() {
        return paymentInterests;
    }

    public void setPaymentInterests(BigDecimal paymentInterests) {
        this.paymentInterests = paymentInterests;
    }

    @Basic
    @Column(name = "ALL_AMOUT")
    public Integer getAllAmout() {
        return allAmout;
    }

    public void setAllAmout(Integer allAmout) {
        this.allAmout = allAmout;
    }

    @Basic
    @Column(name = "ALL_AMOUNT")
    public BigDecimal getAllAmount() {
        return allAmount;
    }

    public void setAllAmount(BigDecimal allAmount) {
        this.allAmount = allAmount;
    }

    @Basic
    @Column(name = "FILE_LIST")
    public String getFileList() {
        return fileList;
    }

    public void setFileList(String fileList) {
        this.fileList = fileList;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }



    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("银行专户id", getAccountBankSpecialId())
                .append("单位公积金_id", this.getCompanyAccountFundId())
                .append("个人公积金_id", this.getPersonAccountFundId())
                .append("收款银行代码(贯标) SKYHDM", this.getGbCollectionBankCode())
                .append("收款银行代码", this.getCollectionBankCodeId())
                .append("收款账户名称(贯标)    SKZHMC", this.getCollectionBankName())
                .append("收款账户号码(贯标)    SKZHHM", this.getCollectionBankNum())
                .append("付款银行代码(贯标)    FKYHDM", this.getGbPaymentBankCode())
                .append("付款银行代码", this.getPaymentBankCodeId())
                .append("付款账户名称(贯标) FKZHMC", this.getPaymentBankName())
                .append("付款账户号码(贯标) FKZHHM", this.getPaymentBankNum())
                .append("银行结算流水号(贯标)   YHJSLSH", this.getBankJournalNumber())
                .append("结算发生日期(贯标)    JSFSRQ", this.getJournalDate())
                .append("结算银行代码(贯标)    JSYHDM", this.getGbJournalBankCode())
                .append("结算银行代码", this.getJournalBankCodeId())
                .append("资金业务类型(贯标)    ZJYWLX", this.getGbFundBusinessType())
                .append("资金业务类型", this.getFundBusinessTypeId())
                .append("业务凭证号码(贯标)    YWPZHM", this.getServiceDocumentNum())
                .append("业务流水号(贯标) YWLSH", this.getBusinessNumber())
                .append("发生额(贯标)   FSE", this.getAccrua())
                .append("摘要(贯标)    ZHAIYAO", this.getDigest())
                .append("结算模式", this.getSettlementPattern())
                .append("结算类别", this.getSettlementCategory())
                .append("批量项目编号", this.getBatchProjectNumber())
                .append("账户类别", this.getDraweeAccountType())
                .append("备注", this.getContent())
                .append("付息户账号", this.getNegativeInterestAccount())
                .append("付息户户名", this.getNegativeInterestName())
                .append("付息账户类别", this.getNegativeInterestType())
                .append("利息收方账号", this.getInterestPayeeAccount())
                .append("付方联行号", this.getDraweeLineNumber())
                .append("付方账户行别", this.getBank())
                .append("收方行名", this.getPayeeBankName())
                .append("收方联行号", this.getPayeeLineNumber())
                .append("多方协议号", this.getProtocolNumber())
                .append("金额", this.getAmount())
                .append("本金发生额", this.getAccrualPrincipal())
                .append("利息发生额", this.getAccrualInterest())
                .append("合计金额", this.getTotalAmount())
                .append("业务明细账号", this.getBusinessDetails())
                .append("业务明细流水号", this.getBusinessDetailsNumber())
                .append("业务明细流水号1", this.getBusinessDetailsNumber1())
                .append("业务明细流水号2", this.getBusinessDetailsNumber2())
                .append("付款利息金额", this.getPaymentInterests())
                .append("总笔数", this.getAllAmout())
                .append("总金额", this.getAllAmount())
                .append("文件列表", this.getFileList())
                .append("操作时间", this.getDateTime())
                .toString();
    }

    public boolean equals(Object other) {
        if ((this == other)) {

            return true;
        }
        if (!(other instanceof TransactionRecord)) {

            return false;
        }

        TransactionRecord caseOther = (TransactionRecord) other;
        return new EqualsBuilder().append(this.getId(),
                caseOther.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @OneToMany(mappedBy="transactionRecord", targetEntity=USER_GGJ.class)
    public List<USER_GGJ> getUserGjjs() {
        return userGjjs;
    }

    public void setUserGjjs(List<USER_GGJ> userGjjs) {
        this.userGjjs = userGjjs;
    }
    @OneToMany(mappedBy="transactionRecord", targetEntity=TransactionRecordDetail.class)
    public List<TransactionRecordDetail> getTransactionRecordDetails() {
        return transactionRecordDetails;
    }

    public void setTransactionRecordDetails(List<TransactionRecordDetail> transactionRecordDetails) {
        this.transactionRecordDetails = transactionRecordDetails;
    }
}
