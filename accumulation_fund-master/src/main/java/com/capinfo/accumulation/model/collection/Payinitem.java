package com.capinfo.accumulation.model.collection;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Comment 汇缴信息与员工公积金的关联信息
 *
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "PAYINITEM")
@SequenceGenerator(name = "seqPAYINITEM", sequenceName = "SEQ_PAYINITEM", allocationSize = 1)
public class Payinitem implements BaseEntity {

    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    //个人公积金_id
    private Long personAccountFundId;
    //
    private PersonAccountFund personAccountFund;

    //汇缴核定_id
    private Long payininfoId;

    //
    private Payininfo payininfo;

    //单位账号
    private String companyAccount;



    //个人缴存额
    private BigDecimal peopleMonthSealed;

    //单位缴存额
    private BigDecimal companyMonthSealed;

    //到账日期
    private Date receivedDate;

    //账户总金额
    private BigDecimal amountAccount;

    //发生时间(核定年月)
    private Date dateTime;


    public Payinitem(){

    }
    public Payinitem(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPAYINITEM")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @ManyToOne(targetEntity = PersonAccountFund.class)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)

    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }





    @ManyToOne(targetEntity = Payininfo.class)
    @JoinColumn(name = "PayInInfo_id", insertable = false, updatable = false)

    public Payininfo getPayininfo() {
        return payininfo;
    }

    public void setPayininfo(Payininfo payininfo) {
        this.payininfo = payininfo;
    }



    @Basic
    @Column(name = "PayInInfo_ID")
    public Long getPayininfoId() {
        return payininfoId;
    }

    public void setPayininfoId(Long payininfoId) {
        this.payininfoId = payininfoId;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT")
    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }



    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }

    @Basic
    @Column(name = "PEOPLE_MONTH_SEALED")
    public BigDecimal getPeopleMonthSealed() {
        return peopleMonthSealed;
    }

    public void setPeopleMonthSealed(BigDecimal peopleMonthSealed) {
        this.peopleMonthSealed = peopleMonthSealed;
    }

    @Basic
    @Column(name = "COMPANY_MONTH_SEALED")
    public BigDecimal getCompanyMonthSealed() {
        return companyMonthSealed;
    }

    public void setCompanyMonthSealed(BigDecimal companyMonthSealed) {
        this.companyMonthSealed = companyMonthSealed;
    }

    @Basic
    @Column(name = "RECEIVED_DATE")
    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    @Basic
    @Column(name = "AMOUNT_ACCOUNT")
    public BigDecimal getAmountAccount() {
        return amountAccount;
    }

    public void setAmountAccount(BigDecimal amountAccount) {
        this.amountAccount = amountAccount;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof Payinitem))
            return false;
        Payinitem castOther = (Payinitem)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
