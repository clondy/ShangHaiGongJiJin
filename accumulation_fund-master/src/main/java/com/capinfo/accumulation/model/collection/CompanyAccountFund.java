package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.*;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 单位公积金
 * author:lishuai
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "COMPANY_ACCOUNT_FUND")
@SequenceGenerator(name = "seqCompanyAccountFund", sequenceName = "SEQ_COMPANY_ACCOUNT_FUND", allocationSize = 1)
public class CompanyAccountFund implements BaseEntity {
    private static final long serialVersionUID = 1L;

    private Long id;
    //单位_id
    private Long companyId;
    private Company company;
    //单位账号 （贯标）dwzh
    private String companyAccount;
    //单位缴存人数 （贯标） dwjcrs
    private Long companyPaymentNumber;
    //单位账户状态(贯标)
    private String gbdwzhzt;
    //单位账户状态
    private Long companyAccountStateId;
    private Dictionary companyAccountState;

    //单位销户日期 （贯标）dwxhrq
    private Date companyCloseDate = new Date();
    //单位账户余额 （贯标） dwzhye
    private BigDecimal companyAccountBalance;
    //个人缴存比例 （贯标） grjcbl
    private Long personPaymentRatio;
    //单位封存人数 （贯标）dwfcrs
    private Long companyDepositNumber;
    //缴至年月 （贯标）jzny
    private Date companyPaymentDate = new Date();
    //单位缴存比例 （贯标） dwjcbl
    private Long companyPaymentRatio;
    //单位销户原因
    private Long companyCloseReasonId;
    private Dictionary companyCloseReason;
    //单位销户原因（贯标）
    private String gbCompanyCloseReason;
    //启缴年月
    private Date startPaymentDate = new Date();
    //缴交日
    private String paymentDate;
    //缴存率标志
    private Long depositRateMarkId;
    private Dictionary depositRateMark;
    //缴存精度
    private String paymentPrecision;
    //备注
    private String notes;
    //公积金账户类型
    private Long accumulationFundAccountTypeId;

    private Dictionary accumulationFundAccountType;
    //是否委托扣款
    private Boolean DelegationPaymentId = false;

    //单位代码
    private String unitCode;
    //单位名称
    private String unitName;
    //合计人数
    private Long totalWorkerNum;
    //当前实交人数
    private Long payWorkerNum;
    //合计月缴额
    private BigDecimal totalMonthPay;
    //当前月实交金额
    private BigDecimal totalMonth;
    //上年末余额
    private BigDecimal preTotalAmount;
    //累计利息
    private BigDecimal totalInterest;
    //当年转入额
    private BigDecimal transIn;
    //当年转入利息额
    private BigDecimal transInLx;
    //当年转出额
    private BigDecimal transOut;
    //当年转出利息额
    private BigDecimal transOutLx;
    //当年交存额
    private BigDecimal payThisYear;
    //当年补交额
    private BigDecimal latePay;
    //当年支取额
    private BigDecimal drawOut;
    //当年销户额
    private BigDecimal useOut;
    //当年开户人数
    private Long openNum;
    //当年转入人数
    private Long transInNum;
    //当年转出人数
    private Long transOutNum;
    //当年汇缴人数
    private Long payNum;
    //当年补交人数
    private Long latePayNum;
    //当年支取人数
    private Long drawNum;
    //当年销户人数
    private Long useNum;
    //当年利息
    private BigDecimal interest;
    //累计转入额
    private BigDecimal totalTransIn;
    //累计转出额
    private BigDecimal totalTransOut;
    //累计交存额
    private BigDecimal totalPay;
    //累计补交额
    private BigDecimal totalLatePay;
    //累计支取额
    private BigDecimal totalDrawOut;
    //累计销户额
    private BigDecimal totalUseOut;
    //累计开户人数
    private Long totalOpenNum;
    //累计转入人数
    private Long totalTransInNum;
    //累计转出人数
    private Long totalTransOutNum;
    //累计交存人数
    private Long totalPayNum;
    //累计补交人数
    private Long totalLatepayNum;
    //累计支取人数
    private Long totalDrawNum;
    //累计销户人数
    private Long totalUseNum;
    //上月的最后汇缴月份
    private Date preMonthPayMonth = new Date();
    //当前的最后汇缴月份
    private Date lastPayMonth = new Date();
    //最后汇缴月交存总额
    private BigDecimal premonPayAmount;
    //最后汇缴月交存人数
    private Long premonPayNum;
    //开户日期
    private Date openDate = new Date();
    //开始汇交日期
    private Date startPayMonth = new Date();
    //新开户标志
    private String newFlag;
    //销户原因代码
    private Long closeCode;
    //发生时间
    private Date dateTime = new Date();
    //银行编码
    private String bankCode;
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime = new Date();
    //渠道
    private Long channels;
    //创建时间
    private String createName;
    //单位补充公积金账户历史ID


    private Long companyAccountFundHistoryId;
    @Column(name = "COMPANYACCOUNTFUNDHISTORYID")
    public Long getCompanyAccountFundHistoryId() {
        return companyAccountFundHistoryId;
    }

    public void setCompanyAccountFundHistoryId(Long companyAccountFundHistoryId) {
        this.companyAccountFundHistoryId = companyAccountFundHistoryId;
    }


    private List<AdjustmentRecord> adjustmentRecord = new ArrayList<>();
    private List<PauseRecoveryRecord> pauseRecoveryRecord = new ArrayList<>();
    private List<Payininfo> payininfo = new ArrayList<>();
    private List<PersonAccountFund> personAccountFund = new ArrayList<>();
    private List<USER_GGJ> USER_GGJ = new ArrayList<>();
    private List<TransactionRecordDetail> transactionRecordDetail = new ArrayList<>();
    private List<PayinSupplementInfo> payinSupplementInfo = new ArrayList<>();
    private List<TransactionRecord> transactionRecord = new ArrayList<>();
    private List<CompanyAnnualBill> companyAnnualBill = new ArrayList<>();
    private List<CompanyAccountHistory> companyAccountHistory = new ArrayList<>();

    public CompanyAccountFund() {
    }

    public CompanyAccountFund(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCompanyAccountFund")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "DWZH")

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    @Basic
    @Column(name = "DWJCRS")

    public Long getCompanyPaymentNumber() {
        return companyPaymentNumber;
    }

    public void setCompanyPaymentNumber(Long companyPaymentNumber) {
        this.companyPaymentNumber = companyPaymentNumber;
    }


    @Basic
    @Column(name = "DWZHZT")

    public String getGbdwzhzt() {
        return gbdwzhzt;
    }

    public void setGbdwzhzt(String gbdwzhzt) {
        this.gbdwzhzt = gbdwzhzt;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_STATE")

    public Long getCompanyAccountStateId() {
        return companyAccountStateId;
    }

    public void setCompanyAccountStateId(Long companyAccountStateId) {
        this.companyAccountStateId = companyAccountStateId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_STATE", insertable = false, updatable = false)

    public Dictionary getCompanyAccountState() {
        return companyAccountState;
    }

    public void setCompanyAccountState(Dictionary companyAccountState) {
        this.companyAccountState = companyAccountState;
    }

    @Basic
    @Column(name = "DWXHRQ")
    public Date getCompanyCloseDate() {
        return companyCloseDate;
    }

    public void setCompanyCloseDate(Date companyCloseDate) {
        this.companyCloseDate = companyCloseDate;
    }

    @Basic
    @Column(name = "DWZHYE")

    public BigDecimal getCompanyAccountBalance() {
        return companyAccountBalance;
    }

    public void setCompanyAccountBalance(BigDecimal companyAccountBalance) {
        this.companyAccountBalance = companyAccountBalance;
    }


    @Basic
    @Column(name = "GRJCBL")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    @Basic
    @Column(name = "DWFCRS")


    public Long getCompanyDepositNumber() {
        return companyDepositNumber;
    }

    public void setCompanyDepositNumber(Long companyDepositNumber) {
        this.companyDepositNumber = companyDepositNumber;
    }

    @Basic
    @Column(name = "JZNY")
    public Date getCompanyPaymentDate() {
        return companyPaymentDate;
    }

    public void setCompanyPaymentDate(Date companyPaymentDate) {
        this.companyPaymentDate = companyPaymentDate;
    }

    @Basic
    @Column(name = "DWJCBL")

    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "COMPANY_CLOSE_REASON")
    public Long getCompanyCloseReasonId() {
        return companyCloseReasonId;
    }

    public void setCompanyCloseReasonId(Long companyCloseReasonId) {
        this.companyCloseReasonId = companyCloseReasonId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_CLOSE_REASON", insertable = false, updatable = false)
    public Dictionary getCompanyCloseReason() {
        return companyCloseReason;
    }

    public void setCompanyCloseReason(Dictionary companyCloseReason) {
        this.companyCloseReason = companyCloseReason;
    }

    @Basic
    @Column(name = "DWXHYY")
    public String getGbCompanyCloseReason() {
        return gbCompanyCloseReason;
    }

    public void setGbCompanyCloseReason(String gbCompanyCloseReason) {
        this.gbCompanyCloseReason = gbCompanyCloseReason;
    }

    @Basic
    @Column(name = "START_PAYMENT_DATE")
    public Date getStartPaymentDate() {
        return startPaymentDate;
    }

    public void setStartPaymentDate(Date startPaymentDate) {
        this.startPaymentDate = startPaymentDate;
    }

    @Basic
    @Column(name = "PAYMENT_DATE")
    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Basic
    @Column(name = "DEPOSIT_RATE_MARK")
    public Long getDepositRateMarkId() {
        return depositRateMarkId;
    }

    public void setDepositRateMarkId(Long depositRateMarkId) {
        this.depositRateMarkId = depositRateMarkId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPOSIT_RATE_MARK", insertable = false, updatable = false)
    public Dictionary getDepositRateMark() {
        return depositRateMark;
    }

    public void setDepositRateMark(Dictionary depositRateMark) {
        this.depositRateMark = depositRateMark;
    }

    @Basic
    @Column(name = "PAYMENT_PRECISION")
    public String getPaymentPrecision() {
        return paymentPrecision;
    }

    public void setPaymentPrecision(String paymentPrecision) {
        this.paymentPrecision = paymentPrecision;
    }

    @Basic
    @Column(name = "NOTES")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "ACCUMULATION_FUND_ACCOUNT_TYPE")
    public Long getAccumulationFundAccountTypeId() {
        return accumulationFundAccountTypeId;
    }

    public void setAccumulationFundAccountTypeId(Long accumulationFundAccountTypeId) {
        this.accumulationFundAccountTypeId = accumulationFundAccountTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCUMULATION_FUND_ACCOUNT_TYPE", insertable = false, updatable = false)
    public Dictionary getAccumulationFundAccountType() {
        return accumulationFundAccountType;
    }

    public void setAccumulationFundAccountType(Dictionary accumulationFundAccountType) {
        this.accumulationFundAccountType = accumulationFundAccountType;
    }

    @Basic
    @Column(name = "IS_DELEGATION_PAYMENT")
    public Boolean getDelegationPaymentId() {
        return DelegationPaymentId;
    }

    public void setDelegationPaymentId(Boolean delegationPaymentId) {
        this.DelegationPaymentId = delegationPaymentId;
    }

    @Basic
    @Column(name = "UNIT_CODE")
    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    @Basic
    @Column(name = "UNIT_NAME")
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Basic
    @Column(name = "TOTAL_WORKER_NUM")
    public Long getTotalWorkerNum() {
        return totalWorkerNum;
    }

    public void setTotalWorkerNum(Long totalWorkerNum) {
        this.totalWorkerNum = totalWorkerNum;
    }

    @Basic
    @Column(name = "PAY_WORKER_NUM")
    public Long getPayWorkerNum() {
        return payWorkerNum;
    }

    public void setPayWorkerNum(Long payWorkerNum) {
        this.payWorkerNum = payWorkerNum;
    }

    @Basic
    @Column(name = "TOTAL_MONTH_PAY")
    public BigDecimal getTotalMonthPay() {
        return totalMonthPay;
    }

    public void setTotalMonthPay(BigDecimal totalMonthPay) {
        this.totalMonthPay = totalMonthPay;
    }

    @Basic
    @Column(name = "TOTAL_MONTH")
    public BigDecimal getTotalMonth() {
        return totalMonth;
    }

    public void setTotalMonth(BigDecimal totalMonth) {
        this.totalMonth = totalMonth;
    }

    @Basic
    @Column(name = "PRE_TOTAL_AMOUNT")
    public BigDecimal getPreTotalAmount() {
        return preTotalAmount;
    }

    public void setPreTotalAmount(BigDecimal preTotalAmount) {
        this.preTotalAmount = preTotalAmount;
    }

    @Basic
    @Column(name = "TOTAL_INTEREST")
    public BigDecimal getTotalInterest() {
        return totalInterest;
    }

    public void setTotalInterest(BigDecimal totalInterest) {
        this.totalInterest = totalInterest;
    }

    @Basic
    @Column(name = "TRANS_IN")
    public BigDecimal getTransIn() {
        return transIn;
    }

    public void setTransIn(BigDecimal transIn) {
        this.transIn = transIn;
    }

    @Basic
    @Column(name = "TRANS_IN_LX")
    public BigDecimal getTransInLx() {
        return transInLx;
    }

    public void setTransInLx(BigDecimal transInLx) {
        this.transInLx = transInLx;
    }

    @Basic
    @Column(name = "TRANS_OUT")
    public BigDecimal getTransOut() {
        return transOut;
    }

    public void setTransOut(BigDecimal transOut) {
        this.transOut = transOut;
    }

    @Basic
    @Column(name = "TRANS_OUT_LX")
    public BigDecimal getTransOutLx() {
        return transOutLx;
    }

    public void setTransOutLx(BigDecimal transOutLx) {
        this.transOutLx = transOutLx;
    }

    @Basic
    @Column(name = "PAY_THIS_YEAR")
    public BigDecimal getPayThisYear() {
        return payThisYear;
    }

    public void setPayThisYear(BigDecimal payThisYear) {
        this.payThisYear = payThisYear;
    }

    @Basic
    @Column(name = "LATE_PAY")
    public BigDecimal getLatePay() {
        return latePay;
    }

    public void setLatePay(BigDecimal latePay) {
        this.latePay = latePay;
    }

    @Basic
    @Column(name = "DRAW_OUT")
    public BigDecimal getDrawOut() {
        return drawOut;
    }

    public void setDrawOut(BigDecimal drawOut) {
        this.drawOut = drawOut;
    }

    @Basic
    @Column(name = "USE_OUT")
    public BigDecimal getUseOut() {
        return useOut;
    }

    public void setUseOut(BigDecimal useOut) {
        this.useOut = useOut;
    }

    @Basic
    @Column(name = "OPEN_NUM")
    public Long getOpenNum() {
        return openNum;
    }

    public void setOpenNum(Long openNum) {
        this.openNum = openNum;
    }

    @Basic
    @Column(name = "TRANS_IN_NUM")
    public Long getTransInNum() {
        return transInNum;
    }

    public void setTransInNum(Long transInNum) {
        this.transInNum = transInNum;
    }

    @Basic
    @Column(name = "TRANS_OUT_NUM")
    public Long getTransOutNum() {
        return transOutNum;
    }

    public void setTransOutNum(Long transOutNum) {
        this.transOutNum = transOutNum;
    }

    @Basic
    @Column(name = "PAY_NUM")
    public Long getPayNum() {
        return payNum;
    }

    public void setPayNum(Long payNum) {
        this.payNum = payNum;
    }

    @Basic
    @Column(name = "LATE_PAY_NUM")
    public Long getLatePayNum() {
        return latePayNum;
    }

    public void setLatePayNum(Long latePayNum) {
        this.latePayNum = latePayNum;
    }

    @Basic
    @Column(name = "DRAW_NUM")
    public Long getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(Long drawNum) {
        this.drawNum = drawNum;
    }

    @Basic
    @Column(name = "USE_NUM")
    public Long getUseNum() {
        return useNum;
    }

    public void setUseNum(Long useNum) {
        this.useNum = useNum;
    }

    @Basic
    @Column(name = "INTEREST")
    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    @Basic
    @Column(name = "TOTAL_TRANS_IN")
    public BigDecimal getTotalTransIn() {
        return totalTransIn;
    }

    public void setTotalTransIn(BigDecimal totalTransIn) {
        this.totalTransIn = totalTransIn;
    }

    @Basic
    @Column(name = "TOTAL_TRANS_OUT")
    public BigDecimal getTotalTransOut() {
        return totalTransOut;
    }

    public void setTotalTransOut(BigDecimal totalTransOut) {
        this.totalTransOut = totalTransOut;
    }

    @Basic
    @Column(name = "TOTAL_PAY")
    public BigDecimal getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(BigDecimal totalPay) {
        this.totalPay = totalPay;
    }

    @Basic
    @Column(name = "TOTAL_LATE_PAY")
    public BigDecimal getTotalLatePay() {
        return totalLatePay;
    }

    public void setTotalLatePay(BigDecimal totalLatePay) {
        this.totalLatePay = totalLatePay;
    }

    @Basic
    @Column(name = "TOTAL_DRAW_OUT")
    public BigDecimal getTotalDrawOut() {
        return totalDrawOut;
    }

    public void setTotalDrawOut(BigDecimal totalDrawOut) {
        this.totalDrawOut = totalDrawOut;
    }

    @Basic
    @Column(name = "TOTAL_USE_OUT")
    public BigDecimal getTotalUseOut() {
        return totalUseOut;
    }

    public void setTotalUseOut(BigDecimal totalUseOut) {
        this.totalUseOut = totalUseOut;
    }

    @Basic
    @Column(name = "TOTAL_OPEN_NUM")
    public Long getTotalOpenNum() {
        return totalOpenNum;
    }

    public void setTotalOpenNum(Long totalOpenNum) {
        this.totalOpenNum = totalOpenNum;
    }

    @Basic
    @Column(name = "TOTAL_TRANS_IN_NUM")
    public Long getTotalTransInNum() {
        return totalTransInNum;
    }

    public void setTotalTransInNum(Long totalTransInNum) {
        this.totalTransInNum = totalTransInNum;
    }

    @Basic
    @Column(name = "TOTAL_TRANS_OUT_NUM")
    public Long getTotalTransOutNum() {
        return totalTransOutNum;
    }

    public void setTotalTransOutNum(Long totalTransOutNum) {
        this.totalTransOutNum = totalTransOutNum;
    }

    @Basic
    @Column(name = "TOTAL_PAY_NUM")
    public Long getTotalPayNum() {
        return totalPayNum;
    }

    public void setTotalPayNum(Long totalPayNum) {
        this.totalPayNum = totalPayNum;
    }

    @Basic
    @Column(name = "TOTAL_LATEPAY_NUM")
    public Long getTotalLatepayNum() {
        return totalLatepayNum;
    }

    public void setTotalLatepayNum(Long totalLatepayNum) {
        this.totalLatepayNum = totalLatepayNum;
    }

    @Basic
    @Column(name = "TOTAL_DRAW_NUM")
    public Long getTotalDrawNum() {
        return totalDrawNum;
    }

    public void setTotalDrawNum(Long totalDrawNum) {
        this.totalDrawNum = totalDrawNum;
    }

    @Basic
    @Column(name = "TOTAL_USE_NUM")
    public Long getTotalUseNum() {
        return totalUseNum;
    }

    public void setTotalUseNum(Long totalUseNum) {
        this.totalUseNum = totalUseNum;
    }

    @Basic
    @Column(name = "PRE_MONTH_PAY_MONTH")
    public Date getPreMonthPayMonth() {
        return preMonthPayMonth;
    }

    public void setPreMonthPayMonth(Date preMonthPayMonth) {
        this.preMonthPayMonth = preMonthPayMonth;
    }

    @Basic
    @Column(name = "LAST_PAY_MONTH")
    public Date getLastPayMonth() {
        return lastPayMonth;
    }

    public void setLastPayMonth(Date lastPayMonth) {
        this.lastPayMonth = lastPayMonth;
    }

    @Basic
    @Column(name = "PREMON_PAY_AMOUNT")
    public BigDecimal getPremonPayAmount() {
        return premonPayAmount;
    }

    public void setPremonPayAmount(BigDecimal premonPayAmount) {
        this.premonPayAmount = premonPayAmount;
    }

    @Basic
    @Column(name = "PREMON_PAY_NUM")
    public Long getPremonPayNum() {
        return premonPayNum;
    }

    public void setPremonPayNum(Long premonPayNum) {
        this.premonPayNum = premonPayNum;
    }

    @Basic
    @Column(name = "OPEN_DATE")
    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    @Basic
    @Column(name = "START_PAY_MONTH")
    public Date getStartPayMonth() {
        return startPayMonth;
    }

    public void setStartPayMonth(Date startPayMonth) {
        this.startPayMonth = startPayMonth;
    }

    @Basic
    @Column(name = "NEW_FLAG")
    public String getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(String newFlag) {
        this.newFlag = newFlag;
    }

    @Basic
    @Column(name = "CLOSE_CODE")
    public Long getCloseCode() {
        return closeCode;
    }

    public void setCloseCode(Long closeCode) {
        this.closeCode = closeCode;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }


    @ManyToOne(targetEntity = SystemUser.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = AdjustmentRecord.class)

    public List<AdjustmentRecord> getAdjustmentRecord() {
        return adjustmentRecord;
    }

    public void setAdjustmentRecord(List<AdjustmentRecord> adjustmentRecord) {
        this.adjustmentRecord = adjustmentRecord;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = PauseRecoveryRecord.class)
    public List<PauseRecoveryRecord> getPauseRecoveryRecord() {
        return pauseRecoveryRecord;
    }

    public void setPauseRecoveryRecord(List<PauseRecoveryRecord> pauseRecoveryRecord) {
        this.pauseRecoveryRecord = pauseRecoveryRecord;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = Payininfo.class)

    public List<Payininfo> getPayininfo() {
        return payininfo;
    }

    public void setPayininfo(List<Payininfo> payininfo) {
        this.payininfo = payininfo;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = PersonAccountFund.class)

    public List<PersonAccountFund> getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(List<PersonAccountFund> personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = USER_GGJ.class)

    public List<USER_GGJ> getUSER_GGJ() {
        return USER_GGJ;
    }

    public void setUSER_GGJ(List<USER_GGJ> USER_GGJ) {
        this.USER_GGJ = USER_GGJ;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = TransactionRecordDetail.class)
    public List<TransactionRecordDetail> getTransactionRecordDetail() {
        return transactionRecordDetail;
    }

    public void setTransactionRecordDetail(List<TransactionRecordDetail> transactionRecordDetail) {
        this.transactionRecordDetail = transactionRecordDetail;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = PayinSupplementInfo.class)

    public List<PayinSupplementInfo> getPayinSupplementInfo() {
        return payinSupplementInfo;
    }

    public void setPayinSupplementInfo(List<PayinSupplementInfo> payinSupplementInfo) {
        this.payinSupplementInfo = payinSupplementInfo;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = TransactionRecord.class)

    public List<TransactionRecord> getTransactionRecord() {
        return transactionRecord;
    }

    public void setTransactionRecord(List<TransactionRecord> transactionRecord) {
        this.transactionRecord = transactionRecord;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = CompanyAnnualBill.class)

    public List<CompanyAnnualBill> getCompanyAnnualBill() {
        return companyAnnualBill;
    }

    public void setCompanyAnnualBill(List<CompanyAnnualBill> companyAnnualBill) {
        this.companyAnnualBill = companyAnnualBill;
    }

    @ManyToOne(targetEntity = Company.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @OneToMany(mappedBy = "companyAccountFund", targetEntity = CompanyAccountHistory.class, cascade = CascadeType.ALL)
    public List<CompanyAccountHistory> getCompanyAccountHistory() {
        return companyAccountHistory;
    }

    public void setCompanyAccountHistory(List<CompanyAccountHistory> companyAccountHistory) {
        this.companyAccountHistory = companyAccountHistory;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof CompanyAccountFund))
            return false;

        CompanyAccountFund castOther = (CompanyAccountFund) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyId", getCompanyId()).toString();
    }

}
