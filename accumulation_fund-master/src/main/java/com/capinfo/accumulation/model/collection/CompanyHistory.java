package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.HadSiteEntity;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rexxxar on 2017/9/19.
 * 单位信息历史
 * @author fengjingsen
 */
@Entity
@Table(name = "COMPANY_HISTORY")
@SequenceGenerator(name = "seqCompanyHistory", sequenceName = "SEQ_COMPANY_HISTORY", allocationSize = 1)
public class CompanyHistory implements BaseEntity, HadSiteEntity {
    //ID
    private Long id;
    //单位ID
    private Long companyId;
    //单位名称(贯标)dwmc
    private String gbCompanyName;
    //组织机构代码(贯标) zzjgdm
    private String gbOrganizationCode;
    //统一社会信用代码
    private String creditCode;
    //主管机构代码
    private String competentCode;
    //单位账号(贯标) dwzh
    private String gbCompanyAccount;
    //账户性质
    private Long natureAccountId;
    private Dictionary natureAccount;
    //单位内部分类
    private Long companyInsideClassification;
    //机构类型
    private Long organizationTypeId;
    private Dictionary organizationType;
    //单位经济类型(贯标)  dwjjlx
    private String gbCompanyEconomicType;
    //单位经济类型
    private Long companyEconomicTypeId;
    private Dictionary companyEconomicType;
    //单位所属行业(贯标) dwsshy
    private String gbCompanyIndustries;
    //单位所属行业
    private Long companyIndustriesId;
    private Dictionary companyIndustries;
    //单位隶属关系(贯标) dwlsgx
    private String gbCompanyRelationships;
    //单位隶属关系
    private Long companyRelatoinships;
    //单位地址(贯标) dwdz
    private String gbCompanyAddress;
    //注册地址
    private String registeredAddress;
    //注册地邮编
    private Long registeredPostCode;
    //单位邮编(贯标) dwyb
    private String gbCompanyPostCode;
    //单位电子信箱(贯标) dwdzxx
    private String gbCompanyEmail;
    //单位设立日期(贯标) dwslrq
    private Date gbCompanyCreationDate;
    //单位联系地址
    private String companyAddress;
    //是否具备法人资格
    private Boolean legalPersonality;
    //单位法人代表证件类型(贯标) dwfrdbzjlx
    private String gbLegalPersonIdCardType;
    //单位法人代表证件类型
    private Long legalPersonIdCardTypeId;
    private Dictionary legalPersonIdCardType;
    //单位法人代表姓名(贯标) dwfrdbxm
    private String gbLegalPersonName;
    //单位法人代表证件号(贯标) dwfrdbzjhm
    private String gbLegalPersonIdCardNumber;
    //法定代表人移动电话
    private Long legalPersonMobilePhone;
    //单位开户日期(贯标) dwkhrq
    private Date companyOpenedDate = new Date();
    //单位发薪日(贯标) dwfxr
    private String gbCompanyPayDay;
    //经办人姓名(贯标) jbrxm
    private String gbAgentName;
    //经办人证件类型(贯标) jbrzjlx
    private String gbAgentIdCardType;
    //经办人证件类型
    private Long agentIdCardTypeId;
    private Dictionary agentIdCardType;
    //经办人证件号码(贯标) jbrzjhm
    private String gbAgentIdCardNumber;
    //经办人手机号码(贯标) jbrsjhm
    private String gbAgentMobilePhone;
    //经办人固定电话号码(贯标) jbrgddhhm
    private String gbAgentTelephone;
    //受托银行名称(贯标) styhmc
    private String gbTrusteeBank;
    //受托银行代码(贯标) styhdm
    private String gbTrusteeBankCode;
    //受托支行名称
    private String delegationBranch;
    //受托支行代码
    private String delegationBranchCode;
    //缴存网点
    private Long paymentSite;
    //区县
    private Long regionId;
    private Region region;
    //联系人移动电话
    private Long contactMobilePhone;
    //联系人姓名
    private String contactName;
    //联系人证件号码
    private String contactIdCardNumber;
    //联系人证件类型
    private Long contactIdCardTypeId;
    private Dictionary contactIdCardType;
    //联系地址
    private String contactAddress;
    //单位代码
    private String unitCode;
    //休息日
    private String restDay;
    //主管区县局代码
    private String responCode;
    //新开户代码
    private String newFlag;
    //客户编码
    private String custNo;
    //中心控制的账户管理状态
    private Long manageState;
    //每月交缴日
    private String payDay;
    //操作时间
    private Date dateTime = new Date();
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //银行编码
    private String bankCode;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime;
    //渠道
    private Long channels;
    //创建人名称
    private String createName;
    //上一条历史ID
    private Long previousId;


    private com.capinfo.accumulation.model.collection.Company company;

    //关联单位公积金历史
    List<CompanyAccountHistory> companyAccountHistories = new ArrayList<>();

    public CompanyHistory(Long id) {
        this.id = id;
    }

    public CompanyHistory() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCompanyHistory")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }


    @Column(name = "DWMC")
    public String getGbCompanyName() {
        return gbCompanyName;
    }

    public void setGbCompanyName(String gbCompanyName) {
        this.gbCompanyName = gbCompanyName;
    }


    @Column(name = "ZZJGDM")
    public String getGbOrganizationCode() {
        return gbOrganizationCode;
    }

    public void setGbOrganizationCode(String gbOrganizationCode) {
        this.gbOrganizationCode = gbOrganizationCode;
    }


    @Column(name = "DWZH")
    public String getGbCompanyAccount() {
        return gbCompanyAccount;
    }

    public void setGbCompanyAccount(String gbCompanyAccount) {
        this.gbCompanyAccount = gbCompanyAccount;
    }


    @Column(name = "DWJJLX")
    public String getGbCompanyEconomicType() {
        return gbCompanyEconomicType;
    }

    public void setGbCompanyEconomicType(String gbCompanyEconomicType) {
        this.gbCompanyEconomicType = gbCompanyEconomicType;
    }


    @Column(name = "DWSSHY")
    public String getGbCompanyIndustries() {
        return gbCompanyIndustries;
    }

    public void setGbCompanyIndustries(String gbCompany_Industries) {
        this.gbCompanyIndustries = gbCompany_Industries;
    }


    @Column(name = "DWLSGX")
    public String getGbCompanyRelationships() {
        return gbCompanyRelationships;
    }

    public void setGbCompanyRelationships(String gbCompanyRelationships) {
        this.gbCompanyRelationships = gbCompanyRelationships;
    }


    @Column(name = "DWDZ")
    public String getGbCompanyAddress() {
        return gbCompanyAddress;
    }

    public void setGbCompanyAddress(String gbCompanyAddress) {
        this.gbCompanyAddress = gbCompanyAddress;
    }


    @Column(name = "DWYB")
    public String getGbCompanyPostCode() {
        return gbCompanyPostCode;
    }

    public void setGbCompanyPostCode(String gbCompanyPostCode) {
        this.gbCompanyPostCode = gbCompanyPostCode;
    }


    @Column(name = "DWDZXX")
    public String getGbCompanyEmail() {
        return gbCompanyEmail;
    }

    public void setGbCompanyEmail(String gbCompanyEmail) {
        this.gbCompanyEmail = gbCompanyEmail;
    }


    @Column(name = "DWSLRQ")
    public Date getGbCompanyCreationDate() {
        return gbCompanyCreationDate;
    }

    public void setGbCompanyCreationDate(Date gbCompanyCreationDate) {
        this.gbCompanyCreationDate = gbCompanyCreationDate;
    }


    @Column(name = "DWFRDBZJLX")
    public String getGbLegalPersonIdCardType() {
        return gbLegalPersonIdCardType;
    }

    public void setGbLegalPersonIdCardType(String gbLegalPersonIdCardType) {
        this.gbLegalPersonIdCardType = gbLegalPersonIdCardType;
    }


    @Column(name = "DWFRDBXM")
    public String getGbLegalPersonName() {
        return gbLegalPersonName;
    }

    public void setGbLegalPersonName(String gbLegalPersonName) {
        this.gbLegalPersonName = gbLegalPersonName;
    }


    @Column(name = "DWFRDBZJHM")
    public String getGbLegalPersonIdCardNumber() {
        return gbLegalPersonIdCardNumber;
    }

    public void setGbLegalPersonIdCardNumber(String gbLegalPersonIdCardNumber) {
        this.gbLegalPersonIdCardNumber = gbLegalPersonIdCardNumber;
    }


    @Column(name = "DWKHRQ")
    public Date getCompanyOpenedDate() {
        return companyOpenedDate;
    }

    public void setCompanyOpenedDate(Date companyOpenedDate) {
        this.companyOpenedDate = companyOpenedDate;
    }


    @Column(name = "DWFXR")

    public String getGbCompanyPayDay() {
        return gbCompanyPayDay;
    }

    public void setGbCompanyPayDay(String gbCompanyPayDay) {
        this.gbCompanyPayDay = gbCompanyPayDay;
    }


    @Column(name = "JBRXM")
    public String getGbAgentName() {
        return gbAgentName;
    }

    public void setGbAgentName(String gbAgentName) {
        this.gbAgentName = gbAgentName;
    }


    @Column(name = "JBRZJLX")
    public String getGbAgentIdCardType() {
        return gbAgentIdCardType;
    }

    public void setGbAgentIdCardType(String gbAgentIdCardType) {
        this.gbAgentIdCardType = gbAgentIdCardType;
    }


    @Column(name = "JBRZJHM")
    public String getGbAgentIdCardNumber() {
        return gbAgentIdCardNumber;
    }

    public void setGbAgentIdCardNumber(String gbAgentIdCardNumber) {
        this.gbAgentIdCardNumber = gbAgentIdCardNumber;
    }


    @Column(name = "JBRSJHM")
    public String getGbAgentMobilePhone() {
        return gbAgentMobilePhone;
    }

    public void setGbAgentMobilePhone(String gbAgentMobilePhone) {
        this.gbAgentMobilePhone = gbAgentMobilePhone;
    }


    @Column(name = "JBRGDDHHM")
    public String getGbAgentTelephone() {
        return gbAgentTelephone;
    }

    public void setGbAgentTelephone(String gbAgentTelephone) {
        this.gbAgentTelephone = gbAgentTelephone;
    }


    @Column(name = "STYHMC")
    public String getGbTrusteeBank() {
        return gbTrusteeBank;
    }

    public void setGbTrusteeBank(String gbTrusteeBank) {
        this.gbTrusteeBank = gbTrusteeBank;
    }


    @Column(name = "STYHDM")
    public String getGbTrusteeBankCode() {
        return gbTrusteeBankCode;
    }

    public void setGbTrusteeBankCode(String gbTrusteeBankCode) {
        this.gbTrusteeBankCode = gbTrusteeBankCode;
    }





    @Column(name = "CREDIT_CODE")
    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }


    @Column(name = "COMPETENT_CODE")
    public String getCompetentCode() {
        return competentCode;
    }

    public void setCompetentCode(String competentCode) {
        this.competentCode = competentCode;
    }



    @Column(name = "COMPANY_INSIDE_CLASSIFICATION")
    public Long getCompanyInsideClassification() {
        return companyInsideClassification;
    }

    public void setCompanyInsideClassification(Long companyInsideClassification) {
        this.companyInsideClassification = companyInsideClassification;
    }


    @Column(name = "ORGANIZATION_TYPE")
    public Long getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(Long organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }


    @Column(name = "COMPANY_ECONOMIC_TYPE")
    public Long getCompanyEconomicTypeId() {
        return companyEconomicTypeId;
    }

    public void setCompanyEconomicTypeId(Long companyEconomicTypeId) {
        this.companyEconomicTypeId = companyEconomicTypeId;
    }


    @Column(name = "COMPANY_INDUSTRIES")
    public Long getCompanyIndustriesId() {
        return companyIndustriesId;
    }

    public void setCompanyIndustriesId(Long companyIndustriesId) {
        this.companyIndustriesId = companyIndustriesId;
    }


    @Column(name = "COMPANY_RELATOINSHIPS")
    public Long getCompanyRelatoinships() {
        return companyRelatoinships;
    }

    public void setCompanyRelatoinships(Long companyRelatoinships) {
        this.companyRelatoinships = companyRelatoinships;
    }


    @Column(name = "REGISTERED_ADDRESS")
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }


    @Column(name = "REGISTERED_POST_CODE")
    public Long getRegisteredPostCode() {
        return registeredPostCode;
    }

    public void setRegisteredPostCode(Long registeredPostCode) {
        this.registeredPostCode = registeredPostCode;
    }


    @Column(name = "COMPANY_ADDRESS")
    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }


    @Column(name = "IS_LEGAL_PERSONALITY")
    public Boolean getLegalPersonality() {
        return legalPersonality;
    }

    public void setLegalPersonality(Boolean legalPersonality) {
        this.legalPersonality = legalPersonality;
    }


    @Column(name = "LEGAL_PERSON_ID_CARD_TYPE")
    public Long getLegalPersonIdCardTypeId() {
        return legalPersonIdCardTypeId;
    }

    public void setLegalPersonIdCardTypeId(Long legalPersonIdCardTypeId) {
        this.legalPersonIdCardTypeId = legalPersonIdCardTypeId;
    }


    @Column(name = "LEGAL_PERSON_MOBILE_PHONE")
    public Long getLegalPersonMobilePhone() {
        return legalPersonMobilePhone;
    }

    public void setLegalPersonMobilePhone(Long legalPersonMobilePhone) {
        this.legalPersonMobilePhone = legalPersonMobilePhone;
    }


    @Column(name = "AGENT_ID_CARD_TYPE")
    public Long getAgentIdCardTypeId() {
        return agentIdCardTypeId;
    }

    public void setAgentIdCardTypeId(Long agentIdCardTypeId) {
        this.agentIdCardTypeId = agentIdCardTypeId;
    }


    @Column(name = "DELEGATION_BRANCH")
    public String getDelegationBranch() {
        return delegationBranch;
    }

    public void setDelegationBranch(String delegationBranch) {
        this.delegationBranch = delegationBranch;
    }


    @Column(name = "DELEGATION_BRANCH_CODE")
    public String getDelegationBranchCode() {
        return delegationBranchCode;
    }

    public void setDelegationBranchCode(String delegationBranchCode) {
        this.delegationBranchCode = delegationBranchCode;
    }


    @Column(name = "PAYMENT_SITE")
    public Long getPaymentSite() {
        return paymentSite;
    }

    public void setPaymentSite(Long paymentSite) {
        this.paymentSite = paymentSite;
    }


    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }


    @Column(name = "CONTACT_MOBILE_PHONE")
    public Long getContactMobilePhone() {
        return contactMobilePhone;
    }

    public void setContactMobilePhone(Long contactMobilePhone) {
        this.contactMobilePhone = contactMobilePhone;
    }


    @Column(name = "CONTACT_NAME")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }


    @Column(name = "CONTACT_ID_CARD_NUMBER")
    public String getContactIdCardNumber() {
        return contactIdCardNumber;
    }

    public void setContactIdCardNumber(String contactIdCardNumber) {
        this.contactIdCardNumber = contactIdCardNumber;
    }


    @Column(name = "CONTACT_ID_CARD_TYPE")
    public Long getContactIdCardTypeId() {
        return contactIdCardTypeId;
    }

    public void setContactIdCardTypeId(Long contactIdCardTypeId) {
        this.contactIdCardTypeId = contactIdCardTypeId;
    }


    @Column(name = "CONTACT_ADDRESS")
    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }


    @Column(name = "UNIT_CODE")
    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }


    @Column(name = "REST_DAY")
    public String getRestDay() {
        return restDay;
    }

    public void setRestDay(String restDay) {
        this.restDay = restDay;
    }


    @Column(name = "RESPON_CODE")
    public String getResponCode() {
        return responCode;
    }

    public void setResponCode(String responCode) {
        this.responCode = responCode;
    }


    @Column(name = "NEW_FLAG")
    public String getNewFlag() {
        return newFlag;
    }

    public void setNewFlag(String newFlag) {
        this.newFlag = newFlag;
    }


    @Column(name = "CUST_NO")
    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }


    @Column(name = "MANAGE_STATE")
    public Long getManageState() {
        return manageState;
    }

    public void setManageState(Long manageState) {
        this.manageState = manageState;
    }


    @Column(name = "PAY_DAY")
    public String getPayDay() {
        return payDay;
    }

    public void setPayDay(String payDay) {
        this.payDay = payDay;
    }


    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }


    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }


    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }


    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }


    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }


    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }


    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ORGANIZATION_TYPE", insertable = false, updatable = false)
    public Dictionary getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(Dictionary organizationType) {
        this.organizationType = organizationType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ECONOMIC_TYPE", insertable = false, updatable = false)
    public Dictionary getCompanyEconomicType() {
        return companyEconomicType;
    }

    public void setCompanyEconomicType(Dictionary companyEconomicType) {
        this.companyEconomicType = companyEconomicType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_INDUSTRIES", insertable = false, updatable = false)
    public Dictionary getCompanyIndustries() {
        return companyIndustries;
    }

    public void setCompanyIndustries(Dictionary companyIndustries) {
        this.companyIndustries = companyIndustries;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "LEGAL_PERSON_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getLegalPersonIdCardType() {
        return legalPersonIdCardType;
    }

    public void setLegalPersonIdCardType(Dictionary legalPersonIdCardType) {
        this.legalPersonIdCardType = legalPersonIdCardType;
    }

    @Basic
    @Column(name = "NATURE_ACCOUNT")
    public Long getNatureAccountId() {
        return natureAccountId;
    }

    public void setNatureAccountId(Long natureAccountId) {
        this.natureAccountId = natureAccountId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "NATURE_ACCOUNT", insertable = false, updatable = false)
    public Dictionary getNatureAccount() {
        return natureAccount;
    }

    public void setNatureAccount(Dictionary natureAccount) {
        this.natureAccount = natureAccount;
    }


    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "AGENT_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getAgentIdCardType() {
        return agentIdCardType;
    }

    public void setAgentIdCardType(Dictionary agentIdCardType) {
        this.agentIdCardType = agentIdCardType;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getContactIdCardType() {
        return contactIdCardType;
    }

    public void setContactIdCardType(Dictionary contactIdCardType) {
        this.contactIdCardType = contactIdCardType;
    }

    @Basic
    @Column(name = "PREVIOUS_ID")
    public Long getPreviousId() {
        return previousId;
    }

    public void setPreviousId(Long previousId) {
        this.previousId = previousId;
    }



    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("gbCompanyName", getGbCompanyName())
                .append("gbOrganizationCode", getGbOrganizationCode())
                .append("creditCode", getCreditCode()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof CompanyHistory))
            return false;
        CompanyHistory otherCompanyHistory = (CompanyHistory) other;
        return new EqualsBuilder().append(this.getId(),
                otherCompanyHistory.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @ManyToOne(targetEntity =Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public com.capinfo.accumulation.model.collection.Company getCompany() {
        return this.company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }


    @OneToMany(targetEntity=CompanyAccountHistory.class, cascade=CascadeType.ALL)
    @JoinColumn(name="COMPANY_HISTORY_ID")
    @OrderBy(value="id desc")
    public List<CompanyAccountHistory> getCompanyAccountHistories() {
        return this.companyAccountHistories;
    }

    public void setCompanyAccountHistories(List<CompanyAccountHistory> companyAccountHistories) {
        this.companyAccountHistories = companyAccountHistories;
    }

    @Transient
    public void addAccountHistory(CompanyAccountHistory accountHistory) {

        accountHistory.setCompanyHistory(this);
        getCompanyAccountHistories().add(accountHistory);
    }

    @Transient
    public void removeAccountHistory(CompanyAccountHistory accountHistory) {

        accountHistory.setCompanyHistory(null);
        getCompanyAccountHistories().remove(accountHistory);
    }
}
