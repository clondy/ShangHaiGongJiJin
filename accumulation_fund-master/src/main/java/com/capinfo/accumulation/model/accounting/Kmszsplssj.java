package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "KMSZSPLSSJ")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_ACCOUNT_BANK_SPECIALT", allocationSize = 1)
public class Kmszsplssj implements BaseEntity{
	
	private Long id;
	private Long kmszls;
	private int sjzt;
	private int spzt;
	private String spyj;
	private String czy;
	private Date czsj;
	private String spr;
	public void setSpsj(Date spsj) {
		this.spsj = spsj;
	}
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Basic
    @Column(name = "KMSZLS")
	public Long getKmszls() {
		return kmszls;
	}
	public void setKmszls(Long kmszls) {
		this.kmszls = kmszls;
	}
	@Basic
    @Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}
	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}
	@Basic
    @Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}
	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	@Basic
    @Column(name = "SPYJ")
	public String getSpyj() {
		return spyj;
	}
	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}
	@Basic
    @Column(name = "CZY")
	public String getCzy() {
		return czy;
	}
	public void setCzy(String czy) {
		this.czy = czy;
	}
	@Basic
    @Column(name = "CZSJ")
	public Date getCzsj() {
		return czsj;
	}
	public void setCzsj(Date czsj) {
		this.czsj = czsj;
	}
	@Basic
    @Column(name = "SPR")
	public String getSpr() {
		return spr;
	}
	public void setSpr(String spr) {
		this.spr = spr;
	}
	@Basic
    @Column(name = "SPSJ")
	private Date spsj;

	public Date getSpsj() {
		return spsj;
	}
	
	
	

}
