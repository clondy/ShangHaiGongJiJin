package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * 
 * 摘要定义
 *
 */
@Entity
@Table(name = "ZHAIYAO" )
@SequenceGenerator(name = "seqZhaiyao", sequenceName = "SEQ_ZHAIYAO", allocationSize = 1)
public class Zhaiyao implements BaseEntity{
	//id
	private Long id;
	//核算单位
	private Dictionary hsdw;
	//账套编号
	private Dictionary ztbh;
	//摘要类型
	private Dictionary zylx;
	//助记码
	private String zjm;
	//摘要内容
	private String zynr;
	//操作人
	private String czr;
	//数据状态
	private String sjzt;
	//插入时间
	private Date crsj;
	
	public Zhaiyao(Long id) {
        this.id = id;
    }

    public Zhaiyao() {
    }
    
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqZhaiyao")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="HSDW" ,referencedColumnName="ID" )
	public Dictionary getHsdw() {
		return hsdw;
	}

	public void setHsdw(Dictionary hsdw) {
		this.hsdw = hsdw;
	}
	

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ZTBH" ,referencedColumnName="ID" )
	public Dictionary getZtbh() {
		return ztbh;
	}
	
	
	public void setZtbh(Dictionary ztbh) {
		this.ztbh = ztbh;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="zylx" ,referencedColumnName="ID" )
	public Dictionary getZylx() {
		return zylx;
	}

	public void setZylx(Dictionary zylx) {
		this.zylx = zylx;
	}
	
	@Basic
    @Column(name = "ZJM")
	public String getZjm() {
		return zjm;
	}

	public void setZjm(String zjm) {
		this.zjm = zjm;
	}
	
	@Basic
    @Column(name = "ZYNR")
	public String getZynr() {
		return zynr;
	}

	public void setZynr(String zynr) {
		this.zynr = zynr;
	}
	
	@Basic
    @Column(name = "CZR")
	public String getCzr() {
		return czr;
	}

	public void setCzr(String czr) {
		this.czr = czr;
	}
	@Basic
    @Column(name = "SJZT")
	public String getSjzt() {
		return sjzt;
	}

	public void setSjzt(String sjzt) {
		this.sjzt = sjzt;
	}
	@Basic
    @Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}

	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}

	
}	

