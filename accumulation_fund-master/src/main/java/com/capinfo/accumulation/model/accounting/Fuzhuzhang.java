package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

@Entity
@Table(name = "FUZHUMXZ" )
@SequenceGenerator(name = "seqFuzhumxz", sequenceName = "SEQ_FUZHUMXZ", allocationSize = 1)
public class Fuzhuzhang implements BaseEntity{
	//id
	private Long id;
	//日期
	private Date riqi;

	private Fzhsxmjbsj hsxmmc;
	private int pzbh;
	private String zhaiyao;
	private double jffse;
	private double dffse;
	private Dictionary yefx;
	private double yue;
	private Kmszjbsj kmmc;
	private Fzlxsz fzlb;
	private String jzzt;
	
	@Basic
    @Column(name = "JZZT")
	public String getJzzt() {
		return jzzt;
	}
	public void setJzzt(String jzzt) {
		this.jzzt = jzzt;
	}
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqFuzhumxz")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Basic
    @Column(name = "RIQI")
	public Date getRiqi() {
		return riqi;
	}
	public void setRiqi(Date riqi) {
		this.riqi = riqi;
	}
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="hsxmmc" ,referencedColumnName="hsxmbm" )
	public Fzhsxmjbsj getHsxmmc() {
		return hsxmmc;
	}
	public void setHsxmmc(Fzhsxmjbsj hsxmmc) {
		this.hsxmmc = hsxmmc;
	}
	@Basic
    @Column(name = "PZBH")
	public int getPzbh() {
		return pzbh;
	}
	public void setPzbh(int pzbh) {
		this.pzbh = pzbh;
	}
	@Basic
    @Column(name = "ZHAIYAO")
	public String getZhaiyao() {
		return zhaiyao;
	}
	public void setZhaiyao(String zhaiyao) {
		this.zhaiyao = zhaiyao;
	}
	@Basic
    @Column(name = "JFFSE")
	public double getJffse() {
		return jffse;
	}
	public void setJffse(double jffse) {
		this.jffse = jffse;
	}
	@Basic
    @Column(name = "DFFSE")
	public double getDffse() {
		return dffse;
	}
	public void setDffse(double dffse) {
		this.dffse = dffse;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="YEFX" ,referencedColumnName="ID" )
	public Dictionary getYefx() {
		return yefx;
	}

	public void setYefx(Dictionary yefx) {
		this.yefx = yefx;
	}
	@Basic
    @Column(name = "YUE")
	public double getYue() {
		return yue;
	}
	public void setYue(double yue) {
		this.yue = yue;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="kmmc" ,referencedColumnName="kmbh" )
	public Kmszjbsj getKmmc() {
		return kmmc;
	}
	public void setKmmc(Kmszjbsj kmmc) {
		this.kmmc = kmmc;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="fzlb" ,referencedColumnName="hslxbm" )
	public Fzlxsz getFzlb() {
		return fzlb;
	}
	public void setFzlb(Fzlxsz fzlb) {
		this.fzlb = fzlb;
	}

}
