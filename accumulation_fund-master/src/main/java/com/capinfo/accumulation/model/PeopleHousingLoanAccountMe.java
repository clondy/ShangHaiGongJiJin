package com.capinfo.accumulation.model;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.sql.Time;

/**
 * <p>
 * 个人住房贷款账户信息
 * </p>
 */
@Entity
@Table(name = "PEOPLE_HOUSING_LOAN_ACCOUNT_ME")
@SequenceGenerator(name = "seqPeopleHousingLoanAccountMe", sequenceName = "SEQ_PEO_HOU_LOAN_ACCOUNT_ME", allocationSize = 1)
public class PeopleHousingLoanAccountMe {
    private Long id;
    private Long dqyhje;
    private Long dkqs;
    private Long llfdbl;
    private Long dkye;
    private Long dqjhghlx;
    private Long yqlxze;
    private Long dqyhfx;
    private Long hsbjze;
    private Long hslxze;
    private Long dqyhlx;
    private Long ljyqqs;
    private Long yqbjze;
    private Long dqjhghbj;
    private Long fxze;
    private String jkhtbh;
    private Time dkffrq;
    private Long dkffe;
    private Time dkjqrq;
    private Long dqjhhkje;
    private Long tqghbjze;
    private String dkfxdj;
    private String dkzh;
    private Long dqyhbj;
    private Long dkll;

    public PeopleHousingLoanAccountMe(Long id) {
        this.id = id;
    }

    public PeopleHousingLoanAccountMe() {
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DQYHJE")
    public Long getDqyhje() {
        return dqyhje;
    }

    public void setDqyhje(Long dqyhje) {
        this.dqyhje = dqyhje;
    }

    @Basic
    @Column(name = "DKQS")
    public Long getDkqs() {
        return dkqs;
    }

    public void setDkqs(Long dkqs) {
        this.dkqs = dkqs;
    }

    @Basic
    @Column(name = "LLFDBL")
    public Long getLlfdbl() {
        return llfdbl;
    }

    public void setLlfdbl(Long llfdbl) {
        this.llfdbl = llfdbl;
    }

    @Basic
    @Column(name = "DKYE")
    public Long getDkye() {
        return dkye;
    }

    public void setDkye(Long dkye) {
        this.dkye = dkye;
    }

    @Basic
    @Column(name = "DQJHGHLX")
    public Long getDqjhghlx() {
        return dqjhghlx;
    }

    public void setDqjhghlx(Long dqjhghlx) {
        this.dqjhghlx = dqjhghlx;
    }

    @Basic
    @Column(name = "YQLXZE")
    public Long getYqlxze() {
        return yqlxze;
    }

    public void setYqlxze(Long yqlxze) {
        this.yqlxze = yqlxze;
    }

    @Basic
    @Column(name = "DQYHFX")
    public Long getDqyhfx() {
        return dqyhfx;
    }

    public void setDqyhfx(Long dqyhfx) {
        this.dqyhfx = dqyhfx;
    }

    @Basic
    @Column(name = "HSBJZE")
    public Long getHsbjze() {
        return hsbjze;
    }

    public void setHsbjze(Long hsbjze) {
        this.hsbjze = hsbjze;
    }

    @Basic
    @Column(name = "HSLXZE")
    public Long getHslxze() {
        return hslxze;
    }

    public void setHslxze(Long hslxze) {
        this.hslxze = hslxze;
    }

    @Basic
    @Column(name = "DQYHLX")
    public Long getDqyhlx() {
        return dqyhlx;
    }

    public void setDqyhlx(Long dqyhlx) {
        this.dqyhlx = dqyhlx;
    }

    @Basic
    @Column(name = "LJYQQS")
    public Long getLjyqqs() {
        return ljyqqs;
    }

    public void setLjyqqs(Long ljyqqs) {
        this.ljyqqs = ljyqqs;
    }

    @Basic
    @Column(name = "YQBJZE")
    public Long getYqbjze() {
        return yqbjze;
    }

    public void setYqbjze(Long yqbjze) {
        this.yqbjze = yqbjze;
    }

    @Basic
    @Column(name = "DQJHGHBJ")
    public Long getDqjhghbj() {
        return dqjhghbj;
    }

    public void setDqjhghbj(Long dqjhghbj) {
        this.dqjhghbj = dqjhghbj;
    }

    @Basic
    @Column(name = "FXZE")
    public Long getFxze() {
        return fxze;
    }

    public void setFxze(Long fxze) {
        this.fxze = fxze;
    }

    @Basic
    @Column(name = "JKHTBH")
    public String getJkhtbh() {
        return jkhtbh;
    }

    public void setJkhtbh(String jkhtbh) {
        this.jkhtbh = jkhtbh;
    }

    @Basic
    @Column(name = "DKFFRQ")
    public Time getDkffrq() {
        return dkffrq;
    }

    public void setDkffrq(Time dkffrq) {
        this.dkffrq = dkffrq;
    }

    @Basic
    @Column(name = "DKFFE")
    public Long getDkffe() {
        return dkffe;
    }

    public void setDkffe(Long dkffe) {
        this.dkffe = dkffe;
    }

    @Basic
    @Column(name = "DKJQRQ")
    public Time getDkjqrq() {
        return dkjqrq;
    }

    public void setDkjqrq(Time dkjqrq) {
        this.dkjqrq = dkjqrq;
    }

    @Basic
    @Column(name = "DQJHHKJE")
    public Long getDqjhhkje() {
        return dqjhhkje;
    }

    public void setDqjhhkje(Long dqjhhkje) {
        this.dqjhhkje = dqjhhkje;
    }

    @Basic
    @Column(name = "TQGHBJZE")
    public Long getTqghbjze() {
        return tqghbjze;
    }

    public void setTqghbjze(Long tqghbjze) {
        this.tqghbjze = tqghbjze;
    }

    @Basic
    @Column(name = "DKFXDJ")
    public String getDkfxdj() {
        return dkfxdj;
    }

    public void setDkfxdj(String dkfxdj) {
        this.dkfxdj = dkfxdj;
    }

    @Basic
    @Column(name = "DKZH")
    public String getDkzh() {
        return dkzh;
    }

    public void setDkzh(String dkzh) {
        this.dkzh = dkzh;
    }

    @Basic
    @Column(name = "DQYHBJ")
    public Long getDqyhbj() {
        return dqyhbj;
    }

    public void setDqyhbj(Long dqyhbj) {
        this.dqyhbj = dqyhbj;
    }

    @Basic
    @Column(name = "DKLL")
    public Long getDkll() {
        return dkll;
    }

    public void setDkll(Long dkll) {
        this.dkll = dkll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PeopleHousingLoanAccountMe that = (PeopleHousingLoanAccountMe) o;

        if (id != that.id) return false;
        if (dqyhje != that.dqyhje) return false;
        if (dkqs != that.dkqs) return false;
        if (llfdbl != that.llfdbl) return false;
        if (dkye != that.dkye) return false;
        if (dqjhghlx != that.dqjhghlx) return false;
        if (yqlxze != that.yqlxze) return false;
        if (dqyhfx != that.dqyhfx) return false;
        if (hsbjze != that.hsbjze) return false;
        if (hslxze != that.hslxze) return false;
        if (dqyhlx != that.dqyhlx) return false;
        if (ljyqqs != that.ljyqqs) return false;
        if (yqbjze != that.yqbjze) return false;
        if (dqjhghbj != that.dqjhghbj) return false;
        if (fxze != that.fxze) return false;
        if (dkffe != that.dkffe) return false;
        if (dqjhhkje != that.dqjhhkje) return false;
        if (tqghbjze != that.tqghbjze) return false;
        if (dqyhbj != that.dqyhbj) return false;
        if (dkll != that.dkll) return false;
        if (jkhtbh != null ? !jkhtbh.equals(that.jkhtbh) : that.jkhtbh != null) return false;
        if (dkffrq != null ? !dkffrq.equals(that.dkffrq) : that.dkffrq != null) return false;
        if (dkjqrq != null ? !dkjqrq.equals(that.dkjqrq) : that.dkjqrq != null) return false;
        if (dkfxdj != null ? !dkfxdj.equals(that.dkfxdj) : that.dkfxdj != null) return false;
        if (dkzh != null ? !dkzh.equals(that.dkzh) : that.dkzh != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (dqyhje ^ (dqyhje >>> 32));
        result = 31 * result + (int) (dkqs ^ (dkqs >>> 32));
        result = 31 * result + (int) (llfdbl ^ (llfdbl >>> 32));
        result = 31 * result + (int) (dkye ^ (dkye >>> 32));
        result = 31 * result + (int) (dqjhghlx ^ (dqjhghlx >>> 32));
        result = 31 * result + (int) (yqlxze ^ (yqlxze >>> 32));
        result = 31 * result + (int) (dqyhfx ^ (dqyhfx >>> 32));
        result = 31 * result + (int) (hsbjze ^ (hsbjze >>> 32));
        result = 31 * result + (int) (hslxze ^ (hslxze >>> 32));
        result = 31 * result + (int) (dqyhlx ^ (dqyhlx >>> 32));
        result = 31 * result + (int) (ljyqqs ^ (ljyqqs >>> 32));
        result = 31 * result + (int) (yqbjze ^ (yqbjze >>> 32));
        result = 31 * result + (int) (dqjhghbj ^ (dqjhghbj >>> 32));
        result = 31 * result + (int) (fxze ^ (fxze >>> 32));
        result = 31 * result + (jkhtbh != null ? jkhtbh.hashCode() : 0);
        result = 31 * result + (dkffrq != null ? dkffrq.hashCode() : 0);
        result = 31 * result + (int) (dkffe ^ (dkffe >>> 32));
        result = 31 * result + (dkjqrq != null ? dkjqrq.hashCode() : 0);
        result = 31 * result + (int) (dqjhhkje ^ (dqjhhkje >>> 32));
        result = 31 * result + (int) (tqghbjze ^ (tqghbjze >>> 32));
        result = 31 * result + (dkfxdj != null ? dkfxdj.hashCode() : 0);
        result = 31 * result + (dkzh != null ? dkzh.hashCode() : 0);
        result = 31 * result + (int) (dqyhbj ^ (dqyhbj >>> 32));
        result = 31 * result + (int) (dkll ^ (dkll >>> 32));
        return result;
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .toString();
    }
}
