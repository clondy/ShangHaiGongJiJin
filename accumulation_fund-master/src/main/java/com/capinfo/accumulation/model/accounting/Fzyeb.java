package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.capinfo.framework.model.BaseEntity;

/**
 * 辅助余额实体
 * @author
 *
 */
@Entity
@Table(name = "FZYEB")
public class Fzyeb implements BaseEntity {

	private Long id;
	private String fmfx;
	private double qcye;
	private double bqjffse;
	private double bqdffse;
	private double ye;
	private Date insertdate;
	
	//科目名称
	private Kmszjbsj kmbm;
	//核算项目名陈
	private Fzhsxmjbsj hsxmbm;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="kmbm" ,referencedColumnName="kmbh")
	public Kmszjbsj getKmbm() {
		return kmbm;
	}

	public void setKmbm(Kmszjbsj kmbm) {
		this.kmbm = kmbm;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hsxmbm" ,referencedColumnName="hsxmbm")
	public Fzhsxmjbsj getHsxmbm() {
		return hsxmbm;
	}

	public void setHsxmbm(Fzhsxmjbsj hsxmbm) {
		this.hsxmbm = hsxmbm;
	}

	@Id
	@Column(name = "ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "FMFX")
	public String getFmfx() {
		return fmfx;
	}

	public void setFmfx(String fmfx) {
		this.fmfx = fmfx;
	}

	@Basic
	@Column(name = "QCYE")
	public double getQcye() {
		return qcye;
	}

	public void setQcye(double qcye) {
		this.qcye = qcye;
	}

	@Basic
	@Column(name = "BQJFFSE")
	public double getBqjffse() {
		return bqjffse;
	}

	public void setBqjffse(double bqjffse) {
		this.bqjffse = bqjffse;
	}

	@Basic
	@Column(name = "BQDFFSE")
	public double getBqdffse() {
		return bqdffse;
	}

	public void setBqdffse(double bqdffse) {
		this.bqdffse = bqdffse;
	}

	@Basic
	@Column(name = "YE")
	public double getYe() {
		return ye;
	}

	public void setYe(double ye) {
		this.ye = ye;
	}

	@Basic
    @Column(name = "INSERTDATE")
	public Date getInsertdate() {
		return insertdate;
	}

	public void setInsertdate(Date insertdate) {
		this.insertdate = insertdate;
	}

}
