package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;


/**
 * 委托收款合同
 * author：lishuai
 *   Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "ENTRUSTED_CONTRACT")
@SequenceGenerator(name = "seqEntrustedContract", sequenceName = "SEQ_ENTRUSTED_CONTRACT", allocationSize=1)
public class EntrustedContract implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //单位_id
    private Long companyId;
    private Company company;
    //开户行名称
    private String nameOfBank;
    //银行账号
    private String bankAccount;
    //账户名称
    private String titleAccount;
    //联系人
    private String contacts;
    //移动电话
    private String mobilePhone;
    //委托扣款日
    private String dateCommission;
    //签约日期
    private Date dateSigning=new Date();
    //解约日期
    private Date cancellingDate=new Date();
    //签约状态
    private Long signingStatusId;
    private Dictionary signingStatus;
    //统一社会信用代码
    private String creditCode;
    //组织机构代码
    private String organizationCode;
    //发生时间
    private Date dateTime=new Date();
    //银行编码
    private String bankCode;
    //操作员
    private String operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;

    //复核时间
    private Date confirmationTime=new Date();
    //渠道
    private Long channels;
    //创建日期
    private String createName;


    public EntrustedContract(Long id) {
        this.id = id;
    }

    public EntrustedContract() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqEntrustedContract")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "NAME_OF_BANK")
    public String getNameOfBank() {
        return nameOfBank;
    }

    public void setNameOfBank(String nameOfBank) {
        this.nameOfBank = nameOfBank;
    }

    @Basic
    @Column(name = "BANK_ACCOUNT")
    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Basic
    @Column(name = "TITLE_ACCOUNT")
    public String getTitleAccount() {
        return titleAccount;
    }

    public void setTitleAccount(String titleAccount) {
        this.titleAccount = titleAccount;
    }

    @Basic
    @Column(name = "CONTACTS")
    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @Basic
    @Column(name = "MOBILE_PHONE")
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Basic
    @Column(name = "DATE_COMMISSION")
    public String getDateCommission() {
        return dateCommission;
    }

    public void setDateCommission(String dateCommission) {
        this.dateCommission = dateCommission;
    }

    @Basic
    @Column(name = "DATE_SIGNING")
    public Date getDateSigning() {
        return dateSigning;
    }

    public void setDateSigning(Date dateSigning) {
        this.dateSigning = dateSigning;
    }

    @Basic
    @Column(name = "CANCELLING_DATE")
    public Date getCancellingDate() {
        return cancellingDate;
    }

    public void setCancellingDate(Date cancellingDate) {
        this.cancellingDate = cancellingDate;
    }

    @Basic
    @Column(name = "SIGNING_STATUS")

    public Long getSigningStatusId() {
        return signingStatusId;
    }

    public void setSigningStatusId(Long signingStatusId) {
        this.signingStatusId = signingStatusId;
    }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SIGNING_STATUS", insertable = false, updatable = false)

    public Dictionary getSigningStatus() {
        return signingStatus;
    }

    public void setSigningStatus(Dictionary signingStatus) {
        this.signingStatus = signingStatus;
    }

    @Basic
    @Column(name = "CREDIT_CODE")
    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    @Basic
    @Column(name = "ORGANIZATION_CODE")
    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }



    @Basic
    @Column(name = "BANK_CODE")

    public String getBankCode() {
        return bankCode;
    }
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SYSTEMUSER_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }
    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }


    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "SYSTEMUSER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }



    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof EntrustedContract))
            return false;

        EntrustedContract castOther = (EntrustedContract) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyId", getCompanyId())
                .append("nameOfBank", getNameOfBank())
                .append("bankAccount", getBankAccount())
                .append("titleAccount", getTitleAccount())
                .append("contacts", getContacts())
                .append("mobilePhone", getMobilePhone())
                .append("dateCommission", getDateCommission())
                .append("dateSigning", getDateSigning())
                .append("cancellingDate", getCancellingDate())
                .append("signingStatusId", getSigningStatusId())
                .append("creditCode", getCreditCode())
                .append("organizationCode", getOrganizationCode())
                .append("dateTime", getDateTime())
                .append("bankCode", getBankCode())
                .append("operatorId", getOperatorId())
                .append("regionId", getRegionId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("confirmationTime", getConfirmationTime())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();
    }
    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}

