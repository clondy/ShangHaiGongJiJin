package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <p>
 *     清册（员工转移信息）一个月 一次操作封存
 *</p>
 */
@Entity
@Table(name = "PERSON_TRANSFER_INFO")
@SequenceGenerator(name = "seqPersonTransferInfo", sequenceName = "SEQ_PERSON_TRANSFER_INFO", allocationSize = 1)
public class PersonTransferInfo  implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;

    private Company company;
    //业务类型
    private String businessType;
    //业务状态
    private String bussinessState;
    //职工账号
    private String personalAccount;

    private Person person;
    //原因
    private  Long reasonId;
    private  Dictionary reason;
    //证件类型
    private Long idCardTypeId;
    private Dictionary idCardType;
    //证件号码
    private String idCardNumber;
    //联系人
    private String contactName;
    //联系人移动电话
    private Long contactMobilePhone;
    //创建时间
    private Date createDate= new Date();
    //修改时间
    private Date updateDate= new Date();
    //转移时间
    private Date transfromDate= new Date();
    //发生时间
    private Date dateTime= new Date();
    //银行编码
    private Long bankCode;
    //操作员
    private Long operatorId;
    //区县
    private Long regionId;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    //复核时间
    private Date confirmationTime= new Date();
    //渠道
    private Long channels;
    //创建人名称
    private String createName;

    private PersonTransferInfo personTransferInfo;

    private List<PersonTransferItem> personTransferItem = new ArrayList<>();  //清册项

    public PersonTransferInfo() {

    }
    public PersonTransferInfo(Long id) {

        this.id = id;
    }
    @OneToMany(mappedBy="personTransferInfo", targetEntity=PersonTransferItem.class)
    public List<PersonTransferItem> getPersonTransferItem() {
        return personTransferItem;
    }

    public void setPersonTransferItem(List<PersonTransferItem> personTransferItemLst) {
        personTransferItem = personTransferItemLst;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPersonTransferInfo")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "CONTACT_MOBILE_PHONE")
    public Long getContactMobilePhone() {
        return contactMobilePhone;
    }

    public void setContactMobilePhone(Long contactMobilePhone) {
        this.contactMobilePhone = contactMobilePhone;
    }

    @Basic
    @Column(name = "CONTACT_NAME")
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }



    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }



    @Basic
    @Column(name = "BUSINESS_TYPE")
    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }


    @Basic
    @Column(name = "BUSSINESS_STATE")
    public String getBussinessState() {
        return bussinessState;
    }
    public void setBussinessState(String bussinessState) {
        this.bussinessState = bussinessState;
    }


    @Basic
    @Column(name = "PERSONAL_ACCOUNT")

    public String getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(String personalAccount) {
        this.personalAccount = personalAccount;
    }

    @Basic
    @Column(name = "REASON")

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REASON", insertable = false, updatable = false)
    public Dictionary getReason() {
        return reason;
    }

    public void setReason(Dictionary reason) {
        this.reason = reason;
    }


    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)

    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }



    @Basic
    @Column(name = "ID_CARD_TYPE")
    public Long getIdCardTypeId() { return idCardTypeId; }
    public void setIdCardTypeId(Long idCardTypeId) { this.idCardTypeId = idCardTypeId; }
    @ManyToOne(targetEntity = Dictionary.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_CARD_TYPE", insertable = false, updatable = false)
    public Dictionary getIdCardType() { return idCardType; }
    public void setIdCardType(Dictionary idCardType) { this.idCardType = idCardType; }



    @Basic
    @Column(name = "ID_CARD_NUMBER")

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    @Basic
    @Column(name = "CREATE_DATE")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Basic
    @Column(name = "TRANSFROM_DATE")
    public Date getTransfromDate() {
        return transfromDate;
    }

    public void setTransfromDate(Date transfromDate) {
        this.transfromDate = transfromDate;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }
    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonTransferInfo))
            return false;

        PersonTransferInfo castOther = (PersonTransferInfo) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id",getId())
                .append("createDate",getCreateDate())
                .append("updateDate",getUpdateDate())
                .append("transfromDate",getTransfromDate())
                .append("dateTime",getDateTime())
                .append("bankCode",getBankCode())
                .append("operatorId",getOperatorId())
                .append("regionId",getRegionId())
                .append("siteId",getSiteId())
                .append("confirmerId",getConfirmerId())
                .append("confirmationTime",getConfirmationTime())
                .append("channels",getChannels())
                .append("createName",getCreateName())
                .toString();
    }
}
