package com.capinfo.accumulation.model;

import javax.persistence.*;

/**
 * Created by Rexxxar on 2017/9/19.
 * 建设项目信息(贯标表)
 * @author fengjingsen
 */
@Entity
@Table(name = "BUILD_PROJECT_MESSAGE")
public class BuildProjectMessage {
    private long id;
    private String xmlx;
    private String xmbh;
    private String jsgcghxkzh;
    private long dked;
    private long tzgm;
    private long jsgm;
    private String xmpc;
    private String gytdsyzh;
    private String lxpfwh;
    private String jzgcsgxkzh;
    private String jsydghxkzh;
    private String xmmc;
    private long sscsdm;
    private String xmfljb;

    public BuildProjectMessage(long id) {
        this.id = id;
    }

    public BuildProjectMessage() {
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "XMLX")
    public String getXmlx() {
        return xmlx;
    }

    public void setXmlx(String xmlx) {
        this.xmlx = xmlx;
    }

    @Basic
    @Column(name = "XMBH")
    public String getXmbh() {
        return xmbh;
    }

    public void setXmbh(String xmbh) {
        this.xmbh = xmbh;
    }

    @Basic
    @Column(name = "JSGCGHXKZH")
    public String getJsgcghxkzh() {
        return jsgcghxkzh;
    }

    public void setJsgcghxkzh(String jsgcghxkzh) {
        this.jsgcghxkzh = jsgcghxkzh;
    }

    @Basic
    @Column(name = "DKED")
    public long getDked() {
        return dked;
    }

    public void setDked(long dked) {
        this.dked = dked;
    }

    @Basic
    @Column(name = "TZGM")
    public long getTzgm() {
        return tzgm;
    }

    public void setTzgm(long tzgm) {
        this.tzgm = tzgm;
    }

    @Basic
    @Column(name = "JSGM")
    public long getJsgm() {
        return jsgm;
    }

    public void setJsgm(long jsgm) {
        this.jsgm = jsgm;
    }

    @Basic
    @Column(name = "XMPC")
    public String getXmpc() {
        return xmpc;
    }

    public void setXmpc(String xmpc) {
        this.xmpc = xmpc;
    }

    @Basic
    @Column(name = "GYTDSYZH")
    public String getGytdsyzh() {
        return gytdsyzh;
    }

    public void setGytdsyzh(String gytdsyzh) {
        this.gytdsyzh = gytdsyzh;
    }

    @Basic
    @Column(name = "LXPFWH")
    public String getLxpfwh() {
        return lxpfwh;
    }

    public void setLxpfwh(String lxpfwh) {
        this.lxpfwh = lxpfwh;
    }

    @Basic
    @Column(name = "JZGCSGXKZH")
    public String getJzgcsgxkzh() {
        return jzgcsgxkzh;
    }

    public void setJzgcsgxkzh(String jzgcsgxkzh) {
        this.jzgcsgxkzh = jzgcsgxkzh;
    }

    @Basic
    @Column(name = "JSYDGHXKZH")
    public String getJsydghxkzh() {
        return jsydghxkzh;
    }

    public void setJsydghxkzh(String jsydghxkzh) {
        this.jsydghxkzh = jsydghxkzh;
    }

    @Basic
    @Column(name = "XMMC")
    public String getXmmc() {
        return xmmc;
    }

    public void setXmmc(String xmmc) {
        this.xmmc = xmmc;
    }

    @Basic
    @Column(name = "SSCSDM")
    public long getSscsdm() {
        return sscsdm;
    }

    public void setSscsdm(long sscsdm) {
        this.sscsdm = sscsdm;
    }

    @Basic
    @Column(name = "XMFLJB")
    public String getXmfljb() {
        return xmfljb;
    }

    public void setXmfljb(String xmfljb) {
        this.xmfljb = xmfljb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildProjectMessage that = (BuildProjectMessage) o;

        if (id != that.id) return false;
        if (dked != that.dked) return false;
        if (tzgm != that.tzgm) return false;
        if (jsgm != that.jsgm) return false;
        if (sscsdm != that.sscsdm) return false;
        if (xmlx != null ? !xmlx.equals(that.xmlx) : that.xmlx != null) return false;
        if (xmbh != null ? !xmbh.equals(that.xmbh) : that.xmbh != null) return false;
        if (jsgcghxkzh != null ? !jsgcghxkzh.equals(that.jsgcghxkzh) : that.jsgcghxkzh != null) return false;
        if (xmpc != null ? !xmpc.equals(that.xmpc) : that.xmpc != null) return false;
        if (gytdsyzh != null ? !gytdsyzh.equals(that.gytdsyzh) : that.gytdsyzh != null) return false;
        if (lxpfwh != null ? !lxpfwh.equals(that.lxpfwh) : that.lxpfwh != null) return false;
        if (jzgcsgxkzh != null ? !jzgcsgxkzh.equals(that.jzgcsgxkzh) : that.jzgcsgxkzh != null) return false;
        if (jsydghxkzh != null ? !jsydghxkzh.equals(that.jsydghxkzh) : that.jsydghxkzh != null) return false;
        if (xmmc != null ? !xmmc.equals(that.xmmc) : that.xmmc != null) return false;
        if (xmfljb != null ? !xmfljb.equals(that.xmfljb) : that.xmfljb != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (xmlx != null ? xmlx.hashCode() : 0);
        result = 31 * result + (xmbh != null ? xmbh.hashCode() : 0);
        result = 31 * result + (jsgcghxkzh != null ? jsgcghxkzh.hashCode() : 0);
        result = 31 * result + (int) (dked ^ (dked >>> 32));
        result = 31 * result + (int) (tzgm ^ (tzgm >>> 32));
        result = 31 * result + (int) (jsgm ^ (jsgm >>> 32));
        result = 31 * result + (xmpc != null ? xmpc.hashCode() : 0);
        result = 31 * result + (gytdsyzh != null ? gytdsyzh.hashCode() : 0);
        result = 31 * result + (lxpfwh != null ? lxpfwh.hashCode() : 0);
        result = 31 * result + (jzgcsgxkzh != null ? jzgcsgxkzh.hashCode() : 0);
        result = 31 * result + (jsydghxkzh != null ? jsydghxkzh.hashCode() : 0);
        result = 31 * result + (xmmc != null ? xmmc.hashCode() : 0);
        result = 31 * result + (int) (sscsdm ^ (sscsdm >>> 32));
        result = 31 * result + (xmfljb != null ? xmfljb.hashCode() : 0);
        return result;
    }
}
