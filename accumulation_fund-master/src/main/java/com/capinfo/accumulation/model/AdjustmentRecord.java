package com.capinfo.accumulation.model;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Rexxxar on 2017/9/19.
 * 基数调整记录
 * @author fengjingsen
 */
@Entity
@Table(name = "ADJUSTMENT_RECORD")
@SequenceGenerator(name = "seqAdjustmentRecord", sequenceName = "SEQ_ADJUSTMENT_RECORD", allocationSize = 1)
public class AdjustmentRecord implements BaseEntity {
    //主键
    private Long id;
    //单位公积金_id
    private Long companyAccountFundId;
    //个人公积_id
    private Long personAccountFundId;
    //单位比例
    private Long companyPaymentRatio;
    //个人比例
    private Long personPaymentRatio;
    //调整年月
    private String changeMonth;
    //发生时间
    private Date dateTime=new Date();
    //银行编码
    private String bankCode;
    //操作员
    private Long operatorId;
    private SystemUser operator;
    //区县
    private Long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime=new Date();
    //渠道
    private Long channels;
    //创建人名称
    private String createName;

    private CompanyAccountFund companyAccountFund;

    private PersonAccountFund personAccountFund;

    public AdjustmentRecord(Long id) {
        this.id = id;
    }

    public AdjustmentRecord() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAdjustmentRecord")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @Basic
    @Column(name = "PERSON_ACCOUNT_FUND_ID")
    public Long getPersonAccountFundId() {
        return personAccountFundId;
    }

    public void setPersonAccountFundId(Long personAccountFundId) {
        this.personAccountFundId = personAccountFundId;
    }

    @Basic
    @Column(name = "COMPANY_PAYMENT_RATIO")
    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Basic
    @Column(name = "PERSON_PAYMENT_RATIO")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    @Basic
    @Column(name = "CHANGE_MONTH")
    public String getChangeMonth() {
        return changeMonth;
    }

    public void setChangeMonth(String changeMonth) {
        this.changeMonth = changeMonth;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("companyAccountFundId", getCompanyAccountFundId())
                .append("personAccountFundId", getPersonAccountFundId())
                .append("companyPaymentRatio", getCompanyPaymentRatio())
                .append("personPaymentRatio", getPersonPaymentRatio())
                .append("changeMonth", getChangeMonth())
                .append("dateTime", getDateTime())
                .append("operatorId", getOperatorId())
                .append("regionId", getRegionId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("confirmationTime", getConfirmationTime())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof AdjustmentRecord))
            return false;
        AdjustmentRecord otherAdjustmentRecord = (AdjustmentRecord) other;
        return new EqualsBuilder().append(this.getId(),
                otherAdjustmentRecord.getId()).isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @ManyToOne(targetEntity = PersonAccountFund.class)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public PersonAccountFund getPersonAccountFund() {
        return this.personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }
    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return this.companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }
}
