package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.enums.Channel;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Comment 停缴/ 启封登记（复缴/ 停缴记录）
 * Created by Rexxxar on 2017/10/23.
 */
@Entity
@Table(name = "PAUSE_RECOVERY_INVENTORY")
@SequenceGenerator(name = "seqPAUSE_RECOVERY_INVENTORY", sequenceName = "SEQ_PAUSE_RECOVERY_INVENTORY", allocationSize = 1)

public class PauseRecoveryInventory implements BaseEntity {


    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    //单位
    private Long companyId;
    private Company company /*= new Company()*/;

    //单位公积金_id
    private Long companyAccountFundId;
    private CompanyAccountFund companyAccountFund /*= new CompanyAccountFund()*/;

    //业务类型 1:停缴 2:复缴
    private int status;

    //是否已完成
    private Boolean finish = false;

    //创建时间
    private Date createTime = new Date();

    //操作员
    private Long operatorId;

    private SystemUser operator/* = new SystemUser()*/;

    //区县
    private Long regionId;

    //网点
    private Long siteId;


    //渠道
    private int channels = Channel.中心网点.getToken();


    //关联停复缴记录表
    List<PauseRecoveryRecord> pauseRecoveryRecords = new ArrayList<>();


    public PauseRecoveryInventory(Long id) {
        this.id = id;
    }

    public PauseRecoveryInventory() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPAUSE_RECOVERY_INVENTORY")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }


    @ManyToOne(targetEntity = Company.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID")
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }


    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @Basic
    @Column(name = "STATUS")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "IS_FINISH")
    public Boolean getFinish() {
        return finish;
    }

    public void setFinish(Boolean finish) {
        this.finish = finish;
    }

    @Basic
    @Column(name = "CREATE_TIME")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Basic
    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }
    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }



    @Basic
    @Column(name = "CHANNELS")
    public int getChannels() {
        return channels;
    }

    public void setChannels(int channels) {
        this.channels = channels;
    }


    @OneToMany(targetEntity=PauseRecoveryRecord.class, cascade=CascadeType.ALL)
    @JoinColumn(name="PAUSE_RECOVERY_RECORD_ID")
    @OrderBy(value="id desc")
    public List<PauseRecoveryRecord> getPauseRecoveryRecords() {
        return this.pauseRecoveryRecords;
    }

    public void setPauseRecoveryRecords(List<PauseRecoveryRecord> pauseRecoveryRecords) {
        this.pauseRecoveryRecords = pauseRecoveryRecords;
    }

    @Transient
    public void addPauseRecoveryInventory(PauseRecoveryRecord pauseRecoveryRecord) {

        pauseRecoveryRecord.setPauseRecoveryInventory(this);
        getPauseRecoveryRecords().add(pauseRecoveryRecord);
    }

    @Transient
    public void removePauseRecoveryInventory(PauseRecoveryRecord pauseRecoveryRecord) {

        pauseRecoveryRecord.setPauseRecoveryInventory(null);
        getPauseRecoveryRecords().remove(pauseRecoveryRecord);
    }


    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof PauseRecoveryRecord))
            return false;
        PauseRecoveryRecord castOther = (PauseRecoveryRecord)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
