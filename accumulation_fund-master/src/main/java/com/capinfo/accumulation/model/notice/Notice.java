package com.capinfo.accumulation.model.notice;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by xiaopeng on 2017/10/20.
 *发送的消息
 *
 */
@Entity
@Table(name = "NOTICE")
@SequenceGenerator(name = "seqMessage", sequenceName = "SEQ_NOTICE", allocationSize = 1)
public class Notice implements BaseEntity {
    //主键
    private Long id;
    //标题
    private String title;
    //正文
    private String content;
    //接收者
    private SystemUser receiver;
    //接收角色
    private SystemRole receiverRole;
    //发送时间
    private Date publishTime;
    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Basic
    @Column(name = "CONTENT")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "RECEIVER_ID", insertable = false, updatable = false)
    public SystemUser getReceiver() {
        return receiver;
    }

    public void setReceiver(SystemUser receiver) {
        this.receiver = receiver;
    }


    @ManyToOne(targetEntity = SystemRole.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "RECEIVER_ROLE_ID", insertable = false, updatable = false)
    public SystemRole getReceiverRole() {
        return receiverRole;
    }

    public void setReceiverRole(SystemRole receiverRole) {
        this.receiverRole = receiverRole;
    }
    @Basic
    @Column(name = "PUBLISH_TIME")
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }


    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
