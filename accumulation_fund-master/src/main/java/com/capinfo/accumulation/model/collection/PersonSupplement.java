package com.capinfo.accumulation.model.collection;

import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * <p>
 *     个人补充信息表
 * </p>
 */
@Entity
@Table(name = "PERSON_SUPPLEMENT")
@SequenceGenerator(name = "seqPersonSupplement", sequenceName = "SEQ_PERSON_SUPPLEMENT", allocationSize = 1)
public class PersonSupplement implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //个人信息_id
    private Long personId;

    private Person person;

    //国籍
    private String nationality;
    //就业类别
    private String employmentCategory;
    //通信地址
    private String mailingAddress;
    //办公电话
    private String officePhone;
    //电子邮箱
    private String email;
    //配偶姓名
    private String spouseName;
    //配偶证件号码
    private String spouseCardid;
    //房屋数量
    private String houseQuantity;
    //房屋性质
    private String houseAttribute;
    //建筑面积
    private String buildArea;
    //缴存方式
    private String paymentMode;
    //缴存基数
    private BigDecimal paymentBase;
    //缴存比例
    private Long paymentRatio;
    //月缴存额
    private BigDecimal  monthPay;
    //开始年月
    private String startMonth;
    //开户银行
    private String openAccountBank;
    //公积金账号
    private String providentFundNum;
    //卡号
    private String cardNum;
    //签约日期（开始）
    private Date signDateStart= new Date();
    //签约日期（结束）
    private Date signDateEnd= new Date();

    public PersonSupplement() {
    }

    public PersonSupplement(Long id) {
        this.id = id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqPersonSupplement")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @ManyToOne(targetEntity = Person.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID", insertable = false, updatable = false)
    public Person getPerson() {
        return person;
    }


    public void setPerson(Person person) {
        this.person = person;
    }

    @Basic
    @Column(name = "NATIONALITY")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Basic
    @Column(name = "EMPLOYMENT_CATEGORY")
    public String getEmploymentCategory() {
        return employmentCategory;
    }

    public void setEmploymentCategory(String employmentCategory) {
        this.employmentCategory = employmentCategory;
    }

    @Basic
    @Column(name = "MAILING_ADDRESS")
    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    @Basic
    @Column(name = "OFFICE_PHONE")
    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "SPOUSE_NAME")
    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    @Basic
    @Column(name = "SPOUSE_CARDID")
    public String getSpouseCardid() {
        return spouseCardid;
    }

    public void setSpouseCardid(String spouseCardid) {
        this.spouseCardid = spouseCardid;
    }

    @Basic
    @Column(name = "HOUSE_QUANTITY")
    public String getHouseQuantity() {
        return houseQuantity;
    }

    public void setHouseQuantity(String houseQuantity) {
        this.houseQuantity = houseQuantity;
    }

    @Basic
    @Column(name = "HOUSE_ATTRIBUTE")
    public String getHouseAttribute() {
        return houseAttribute;
    }

    public void setHouseAttribute(String houseAttribute) {
        this.houseAttribute = houseAttribute;
    }

    @Basic
    @Column(name = "BUILD_AREA")
    public String getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(String buildArea) {
        this.buildArea = buildArea;
    }

    @Basic
    @Column(name = "PAYMENT_MODE")
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Basic
    @Column(name = "PAYMENT_BASE")
    public BigDecimal  getPaymentBase() {
        return paymentBase;
    }

    public void setPaymentBase(BigDecimal  paymentBase) {
        this.paymentBase = paymentBase;
    }

    @Basic
    @Column(name = "PAYMENT_RATIO")
    public Long getPaymentRatio() {
        return paymentRatio;
    }

    public void setPaymentRatio(Long paymentRatio) {
        this.paymentRatio = paymentRatio;
    }

    @Basic
    @Column(name = "MONTH_PAY")
    public BigDecimal  getMonthPay() {
        return monthPay;
    }

    public void setMonthPay(BigDecimal  monthPay) {
        this.monthPay = monthPay;
    }

    @Basic
    @Column(name = "START_MONTH")
    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    @Basic
    @Column(name = "OPEN_ACCOUNT_BANK")
    public String getOpenAccountBank() {
        return openAccountBank;
    }

    public void setOpenAccountBank(String openAccountBank) {
        this.openAccountBank = openAccountBank;
    }

    @Basic
    @Column(name = "PROVIDENT_FUND_NUM")
    public String getProvidentFundNum() {
        return providentFundNum;
    }

    public void setProvidentFundNum(String providentFundNum) {
        this.providentFundNum = providentFundNum;
    }

    @Basic
    @Column(name = "CARD_NUM")
    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    @Basic
    @Column(name = "SIGN_DATE_START")
    public Date getSignDateStart() {
        return signDateStart;
    }

    public void setSignDateStart(Date signDateStart) {
        this.signDateStart = signDateStart;
    }

    @Basic
    @Column(name = "SIGN_DATE_END")
    public Date getSignDateEnd() {
        return signDateEnd;
    }

    public void setSignDateEnd(Date signDateEnd) {
        this.signDateEnd = signDateEnd;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonSupplement))
            return false;

        PersonSupplement castOther = (PersonSupplement) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id",getId())
                .append("personId",getPersonId())
                .append("nationality",getNationality())
                .append("employmentCategory",getEmploymentCategory())
                .append("mailingAddress",getMailingAddress())
                .append("officePhone",getOfficePhone())
                .append("email",getEmail())
                .append("spouseName",getSpouseName())
                .append("spouseCardid",getSpouseCardid())
                .append("houseQuantity",getHouseQuantity())
                .append("houseAttribute",getHouseAttribute())
                .append("buildArea",getBuildArea())
                .append("paymentMode",getPaymentMode())
                .append("paymentBase",getPaymentBase())
                .append("paymentRatio",getPaymentRatio())
                .append("monthPay",getMonthPay())
                .append("startMonth",getStartMonth())
                .append("openAccountBank",getOpenAccountBank())
                .append("providentFundNum",getProvidentFundNum())
                .append("cardNum",getCardNum())
                .append("signDateStart",getSignDateStart())
                .append("signDateEnd",getSignDateEnd())
                .toString();
    }
}
