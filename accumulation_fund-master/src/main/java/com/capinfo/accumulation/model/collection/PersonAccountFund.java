package com.capinfo.accumulation.model.collection;

import com.capinfo.accumulation.model.AdjustmentRecord;
import com.capinfo.accumulation.model.PayinSupplementItem;
import com.capinfo.accumulation.model.TransactionRecord;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 员工公积金账户
 * </p>
 */
@Entity
@Table(name = "PERSON_ACCOUNT_FUND")
@SequenceGenerator(name = "seqPersonAccountFund", sequenceName = "SEQ_PERSON_ACCOUNT_FUND", allocationSize = 1)
public class PersonAccountFund implements BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long id;
    //单位公积金_id
    private Long companyAccountFundId;
    //个人_id
    private Long personId;
    //开户日期
    private Date openedDate;
    //账户类型
    private String accountTypeId;
    private Dictionary accountType;
    //账户状态
    private Long accountState;
    //单位代码
    private String companyCode;
    //月交存额
    private BigDecimal monthPay;
    //去年余额
    private BigDecimal lastYearBalance;
    //当前定期余额
    private BigDecimal regularBalance;
    //当前活期余额
    private BigDecimal currentBalance;
    //最后交存月份
    private String lastMonthPayment;
    //最后余额变动日期
    private String lastChangeBalanceDate;
    //累计汇交
    private BigDecimal totalRemit;
    //累计补交金额
    private BigDecimal totalSupplementaryPayment;
    //累计补交次数
    private Long totalSupplementaryPaymentCo;
    //累计支取
    private BigDecimal totalWithdraw;
    //累计支取次数
    private Long totalWithdrawCount;
    //累计结息额
    private BigDecimal totalInterest;
    //累计调帐金额
    private BigDecimal totalAdjustment;
    //累计调帐次数
    private Long totalAdjustmentCount;
    //当年交存
    private BigDecimal currentYearPayment;
    //当年补交金额
    private BigDecimal currentYearPayFees;
    //当年补交次数
    private Long currentYearPayFeesCount;
    //当年支取
    private BigDecimal currentYearDraw;
    //当年支取次数
    private Long currentYearDrawCount;
    //当年调帐金额
    private BigDecimal currentYearAdjustmentAmount;
    //当年调帐次数
    private Long currentYearAdjustmentCount;
    //中心计算的当年活期基数(积数)
    private BigDecimal currentYearDemandCardinal;
    //中心计算的当年定期基数(积数)
    private BigDecimal currentYearRegularCardinal;
    //月对帐时取得的银行当年利息
    private BigDecimal monthAdjustmeYearInterest;
    //状态
    private String state;
    //汇缴状态
    private String remitState;
    //销户日期
    private String closeDate;
    //销户原因
    private String closeReason;
    //新开户标志
    private String newOpenFlag;
    //单位交存比例
    private Long companyPaymentRatio;
    //个人交存比例
    private Long personPaymentRatio;
    //首次汇缴日期
    private String fisrtRemitMonth;
    //创建人id
    private Long operatorId;
    //渠道
    private Long Channels;
    //网点id
    private Long siteId;
    //审核人id
    private Long confirmerId;
    //创建时间
    private Date createTime;
    //对应个人历史公积金id
    private Long personHistoryAccountFundId;

    private PersonHistoryAccountFund personHistoryAccountFund;

    private Person person;   //个人信息
    private CompanyAccountFund companyAccountFund;   //单位公积金

    //个人账号
    private String accountNumber;

    //状态
    //1：停缴、2：复缴
    private Long payment;
    //1：冻结、0：解冻

    private Long freeze;
    //0：解封、1：封存

    private Long archive;
    //0：正常、1：销户

    private Long destroy;

    private List<PersonHistoryAccountFund> personHistoryAccountFundList = new ArrayList<PersonHistoryAccountFund>();  //外联汇缴信息

    private List<PauseRecoveryRecord> pauseRecoveryRecordList = new ArrayList<PauseRecoveryRecord>();
    //    private List<Payininfo> payininfoList = new ArrayList<Payininfo>();  //外联汇缴信息
    private List<AdjustmentRecord> adjustmentRecordList = new ArrayList<AdjustmentRecord>();//基数调整记录
    private List<TransactionRecord> transactionRecordList = new ArrayList<TransactionRecord>(); //交易流水（收/ 付款）
    private List<PayinSupplementItem> payinSupplementItemList = new ArrayList<PayinSupplementItem>(); //补交核定明细


    @ManyToOne(targetEntity = CompanyAccountFund.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_ACCOUNT_FUND_ID")
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund CompanyAccountFund) {
        this.companyAccountFund = CompanyAccountFund;
    }

    @ManyToOne(targetEntity = Person.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID")
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


    @OneToMany(targetEntity = PayinSupplementItem.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID")
    @OrderBy(value = "id desc")
    public List<PayinSupplementItem> getPayinSupplementItemList() {
        return payinSupplementItemList;
    }

    public void setPayinSupplementItemList(List<PayinSupplementItem> payinSupplementItemList) {
        this.payinSupplementItemList = payinSupplementItemList;
    }


    @OneToMany(targetEntity = TransactionRecord.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID")
    @OrderBy(value = "id desc")
    public List<TransactionRecord> getTransactionRecordList() {
        return transactionRecordList;
    }

    public void setTransactionRecordList(List<TransactionRecord> transactionRecordList) {
        this.transactionRecordList = transactionRecordList;
    }

    @OneToMany(targetEntity = PauseRecoveryRecord.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID")
    @OrderBy(value = "id desc")
    public List<PauseRecoveryRecord> getPauseRecoveryRecordList() {
        return pauseRecoveryRecordList;
    }

    public void setPauseRecoveryRecordList(List<PauseRecoveryRecord> pauseRecoveryRecordList) {
        this.pauseRecoveryRecordList = pauseRecoveryRecordList;
    }

    @OneToMany(targetEntity = AdjustmentRecord.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID")
    @OrderBy(value = "id desc")
    public List<AdjustmentRecord> getAdjustmentRecordList() {
        return adjustmentRecordList;
    }

    public void setAdjustmentRecordList(List<AdjustmentRecord> adjustmentRecordList) {
        this.adjustmentRecordList = adjustmentRecordList;
    }

//    @OneToMany(targetEntity=PersonAnnualbill.class, cascade=CascadeType.ALL,fetch = FetchType.LAZY)
//    @JoinColumn(name="PERSON_ACCOUNT_FUND_ID")
//    @OrderBy(value="id desc")
//    public List<PersonAnnualbill> getPersonAnnualbillList() {
//        return personAnnualbillList;
//    }
//
//    public void setPersonAnnualbillList(List<PersonAnnualbill> personAnnualbillList) {
//        this.personAnnualbillList = personAnnualbillList;
//    }

//    @OneToMany(targetEntity=Payininfo.class, cascade=CascadeType.ALL,fetch = FetchType.LAZY)
//    @JoinColumn(name="PERSON_ACCOUNT_FUND_ID")
//    @OrderBy(value="id desc")
//    public List<Payininfo> getPayininfoList() {
//        return payininfoList;
//    }
//
//    public void setPayininfoList(List<Payininfo> payininfoList) {
//        this.payininfoList = payininfoList;
//    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPersonAccountFund")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ACCOUNT_FUND_ID", insertable = false, updatable = false)
    public Long getCompanyAccountFundId() {
        return companyAccountFundId;
    }

    public void setCompanyAccountFundId(Long companyAccountFundId) {
        this.companyAccountFundId = companyAccountFundId;
    }

    @Basic
    @Column(name = "PERSON_ID", insertable = false, updatable = false)
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Basic
    @Column(name = "OPENED_DATE")
    public Date getOpenedDate() {
        return openedDate;
    }

    public void setOpenedDate(Date openedDate) {
        this.openedDate = openedDate;
    }

    @Basic
    @Column(name = "ACCOUNT_TYPE")
    public String getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(String accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    @ManyToOne(targetEntity = Dictionary.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_TYPE", insertable = false, updatable = false)
    public Dictionary getAccountType() {
        return accountType;
    }

    public void setAccountType(Dictionary accountType) {
        this.accountType = accountType;
    }

    @Basic
    @Column(name = "ACCOUNT_STATE")
    public Long getAccountState() {
        return accountState;
    }

    public void setAccountState(Long accountState) {
        this.accountState = accountState;
    }

    @Basic
    @Column(name = "COMPANY_CODE")
    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    @Basic
    @Column(name = "MONTH_PAY")
    public BigDecimal getMonthPay() {
        return monthPay;
    }

    public void setMonthPay(BigDecimal monthPay) {
        this.monthPay = monthPay;
    }

    @Basic
    @Column(name = "LAST_YEAR_BALANCE")
    public BigDecimal getLastYearBalance() {
        return lastYearBalance;
    }

    public void setLastYearBalance(BigDecimal lastYearBalance) {
        this.lastYearBalance = lastYearBalance;
    }

    @Basic
    @Column(name = "REGULAR_BALANCE")
    public BigDecimal getRegularBalance() {
        return regularBalance;
    }

    public void setRegularBalance(BigDecimal regularBalance) {
        this.regularBalance = regularBalance;
    }

    @Basic
    @Column(name = "CURRENT_BALANCE")
    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Basic
    @Column(name = "LAST_MONTH_PAYMENT")
    public String getLastMonthPayment() {
        return lastMonthPayment;
    }

    public void setLastMonthPayment(String lastMonthPayment) {
        this.lastMonthPayment = lastMonthPayment;
    }

    @Basic
    @Column(name = "LAST_CHANGE_BALANCE_DATE")
    public String getLastChangeBalanceDate() {
        return lastChangeBalanceDate;
    }

    public void setLastChangeBalanceDate(String lastChangeBalanceDate) {
        this.lastChangeBalanceDate = lastChangeBalanceDate;
    }

    @Basic
    @Column(name = "TOTAL_REMIT")
    public BigDecimal getTotalRemit() {
        return totalRemit;
    }

    public void setTotalRemit(BigDecimal totalRemit) {
        this.totalRemit = totalRemit;
    }

    @Basic
    @Column(name = "TOTAL_SUPPLEMENTARY_PAYMENT")
    public BigDecimal getTotalSupplementaryPayment() {
        return totalSupplementaryPayment;
    }

    public void setTotalSupplementaryPayment(BigDecimal totalSupplementaryPayment) {
        this.totalSupplementaryPayment = totalSupplementaryPayment;
    }

    @Basic
    @Column(name = "TOTAL_SUPPLEMENTARY_PAYMENT_CO")
    public Long getTotalSupplementaryPaymentCo() {
        return totalSupplementaryPaymentCo;
    }

    public void setTotalSupplementaryPaymentCo(Long totalSupplementaryPaymentCo) {
        this.totalSupplementaryPaymentCo = totalSupplementaryPaymentCo;
    }

    @Basic
    @Column(name = "TOTAL_WITHDRAW")
    public BigDecimal getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(BigDecimal totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    @Basic
    @Column(name = "TOTAL_WITHDRAW_COUNT")
    public Long getTotalWithdrawCount() {
        return totalWithdrawCount;
    }

    public void setTotalWithdrawCount(Long totalWithdrawCount) {
        this.totalWithdrawCount = totalWithdrawCount;
    }

    @Basic
    @Column(name = "TOTAL_INTEREST")
    public BigDecimal getTotalInterest() {
        return totalInterest;
    }

    public void setTotalInterest(BigDecimal totalInterest) {
        this.totalInterest = totalInterest;
    }

    @Basic
    @Column(name = "TOTAL_ADJUSTMENT")
    public BigDecimal getTotalAdjustment() {
        return totalAdjustment;
    }

    public void setTotalAdjustment(BigDecimal totalAdjustment) {
        this.totalAdjustment = totalAdjustment;
    }

    @Basic
    @Column(name = "TOTAL_ADJUSTMENT_COUNT")
    public Long getTotalAdjustmentCount() {
        return totalAdjustmentCount;
    }

    public void setTotalAdjustmentCount(Long totalAdjustmentCount) {
        this.totalAdjustmentCount = totalAdjustmentCount;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_PAYMENT")
    public BigDecimal getCurrentYearPayment() {
        return currentYearPayment;
    }

    public void setCurrentYearPayment(BigDecimal currentYearPayment) {
        this.currentYearPayment = currentYearPayment;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_PAY_FEES")
    public BigDecimal getCurrentYearPayFees() {
        return currentYearPayFees;
    }

    public void setCurrentYearPayFees(BigDecimal currentYearPayFees) {
        this.currentYearPayFees = currentYearPayFees;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_PAY_FEES_COUNT")
    public Long getCurrentYearPayFeesCount() {
        return currentYearPayFeesCount;
    }

    public void setCurrentYearPayFeesCount(Long currentYearPayFeesCount) {
        this.currentYearPayFeesCount = currentYearPayFeesCount;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_DRAW")
    public BigDecimal getCurrentYearDraw() {
        return currentYearDraw;
    }

    public void setCurrentYearDraw(BigDecimal currentYearDraw) {
        this.currentYearDraw = currentYearDraw;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_DRAW_COUNT")
    public Long getCurrentYearDrawCount() {
        return currentYearDrawCount;
    }

    public void setCurrentYearDrawCount(Long currentYearDrawCount) {
        this.currentYearDrawCount = currentYearDrawCount;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_ADJUSTMENT_AMOUNT")
    public BigDecimal getCurrentYearAdjustmentAmount() {
        return currentYearAdjustmentAmount;
    }

    public void setCurrentYearAdjustmentAmount(BigDecimal currentYearAdjustmentAmount) {
        this.currentYearAdjustmentAmount = currentYearAdjustmentAmount;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_ADJUSTMENT_COUNT")
    public Long getCurrentYearAdjustmentCount() {
        return currentYearAdjustmentCount;
    }

    public void setCurrentYearAdjustmentCount(Long currentYearAdjustmentCount) {
        this.currentYearAdjustmentCount = currentYearAdjustmentCount;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_DEMAND_CARDINAL")
    public BigDecimal getCurrentYearDemandCardinal() {
        return currentYearDemandCardinal;
    }

    public void setCurrentYearDemandCardinal(BigDecimal currentYearDemandCardinal) {
        this.currentYearDemandCardinal = currentYearDemandCardinal;
    }

    @Basic
    @Column(name = "CURRENT_YEAR_REGULAR_CARDINAL")
    public BigDecimal getCurrentYearRegularCardinal() {
        return currentYearRegularCardinal;
    }

    public void setCurrentYearRegularCardinal(BigDecimal currentYearRegularCardinal) {
        this.currentYearRegularCardinal = currentYearRegularCardinal;
    }

    @Basic
    @Column(name = "MONTH_ADJUSTME_YEAR_INTEREST")
    public BigDecimal getMonthAdjustmeYearInterest() {
        return monthAdjustmeYearInterest;
    }

    public void setMonthAdjustmeYearInterest(BigDecimal monthAdjustmeYearInterest) {
        this.monthAdjustmeYearInterest = monthAdjustmeYearInterest;
    }

    @Basic
    @Column(name = "STATE")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "REMIT_STATE")
    public String getRemitState() {
        return remitState;
    }

    public void setRemitState(String remitState) {
        this.remitState = remitState;
    }

    @Basic
    @Column(name = "CLOSE_DATE")
    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    @Basic
    @Column(name = "CLOSE_REASON")
    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    @Basic
    @Column(name = "NEW_OPEN_FLAG")
    public String getNewOpenFlag() {
        return newOpenFlag;
    }

    public void setNewOpenFlag(String newOpenFlag) {
        this.newOpenFlag = newOpenFlag;
    }

    @Basic
    @Column(name = "FISRT_REMIT_MONTH")
    public String getFisrtRemitMonth() {
        return fisrtRemitMonth;
    }

    public void setFisrtRemitMonth(String fisrtRemitMonth) {
        this.fisrtRemitMonth = fisrtRemitMonth;
    }

    @Column(name = "OPERATOR_ID")
    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    @Column(name = "CHANNELS")
    public Long getChannels() {
        return Channels;
    }

    public void setChannels(Long channels) {
        Channels = channels;
    }

    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @Column(name = "CREATE_TIME")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

//    @OneToMany(mappedBy = "companyAccountFund", targetEntity = Payininfo.class)
//    public List<Payininfo> getPayininfo() {
//        return payininfo;
//    }
//
//    public void setPayininfo(List<Payininfo> payininfo) {
//        this.payininfo = payininfo;
//    }

    @Column(name = "COMPANY_PAYMENT_RATIO")
    public Long getCompanyPaymentRatio() {
        return companyPaymentRatio;
    }

    public void setCompanyPaymentRatio(Long companyPaymentRatio) {
        this.companyPaymentRatio = companyPaymentRatio;
    }

    @Column(name = "PERSON_PAYMENT_RATIO")
    public Long getPersonPaymentRatio() {
        return personPaymentRatio;
    }

    public void setPersonPaymentRatio(Long personPaymentRatio) {
        this.personPaymentRatio = personPaymentRatio;
    }

    public boolean equals(Object other) {

        if ((this == other))
            return true;
        if (!(other instanceof PersonAccountFund))
            return false;

        PersonAccountFund castOther = (PersonAccountFund) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    public int hashCode() {

        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("companyAccountFundId", getCompanyAccountFundId())
                .append("personId", getPersonId())
                .append("openedDate", getOpenedDate())
                .append("accountType", getAccountType())
                .append("accountState", getAccountState())
                .append("companyCode", getCompanyCode())
                .append("monthPay", getMonthPay())
                .append("lastYearBalance", getLastYearBalance())
                .append("regularBalance", getRegularBalance())
                .append("currentBalance", getCurrentBalance())
                .append("lastMonthPayment", getLastMonthPayment())
                .append("lastChangeBalanceDate", getLastChangeBalanceDate())
                .append("totalRemit", getTotalRemit())
                .append("totalSupplementaryPayment", getTotalSupplementaryPayment())
                .append("totalSupplementaryPaymentCo", getTotalSupplementaryPaymentCo())
                .append("totalWithdraw", getTotalWithdraw())
                .append("totalWithdrawCount", getTotalWithdrawCount())
                .append("totalInterest", getTotalInterest())
                .append("totalAdjustment", getTotalAdjustment())
                .append("totalAdjustmentCount", getTotalAdjustmentCount())
                .append("currentYearPayment", getCurrentYearPayment())
                .append("currentYearPayFees", getCurrentYearPayFees())
                .append("currentYearPayFeesCount", getCurrentYearPayFeesCount())
                .append("currentYearDraw", getCurrentYearDraw())
                .append("currentYearDrawCount", getCurrentYearDrawCount())
                .append("currentYearAdjustmentAmount", getCurrentYearAdjustmentAmount())
                .append("currentYearAdjustmentCount", getCurrentYearAdjustmentCount())
                .append("currentYearDemandCardinal", getCurrentYearDemandCardinal())
                .append("currentYearRegularCardinal", getCurrentYearRegularCardinal())
                .append("monthAdjustmeYearInterest", getMonthAdjustmeYearInterest())
                .append("state", getState())
                .append("remitState", getRemitState())
                .append("closeDate", getCloseDate())
                .append("closeReason", getCloseReason())
                .append("newOpenFlag", getNewOpenFlag())
                .append("fisrtRemitMonth", getFisrtRemitMonth())
                .append("operatorId", getOperatorId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("channels", getChannels())
                .toString();
    }

    @Column(name = "PERSON_HISTORY_ACCOUNT_FUND_ID")
    public Long getPersonHistoryAccountFundId() {
        return personHistoryAccountFundId;
    }

    public void setPersonHistoryAccountFundId(Long personHistoryAccountFundId) {
        this.personHistoryAccountFundId = personHistoryAccountFundId;
    }

    @Column(name = "ACCOUNT_NUMBER")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @OneToMany(targetEntity = PersonHistoryAccountFund.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "PERSON_ACCOUNT_FUND_ID")
    @OrderBy(value = "id desc")
    public List<PersonHistoryAccountFund> getPersonHistoryAccountFundList() {
        return personHistoryAccountFundList;
    }

    public void setPersonHistoryAccountFundList(List<PersonHistoryAccountFund> personHistoryAccountFundList) {
        this.personHistoryAccountFundList = personHistoryAccountFundList;
    }

    @Column(name = "IS_PAYMENT")
    public Long getPayment() {
        return payment;
    }

    public void setPayment(Long payment) {
        this.payment = payment;
    }

    @Column(name = "IS_FREEZE")
    public Long getFreeze() {
        return freeze;
    }

    public void setFreeze(Long freeze) {
        this.freeze = freeze;
    }

    @Column(name = "IS_ARCHIVE")
    public Long getArchive() {
        return archive;
    }

    public void setArchive(Long archive) {
        this.archive = archive;
    }

    @Column(name = "IS_DESTROY")
    public Long getDestroy() {
        return destroy;
    }

    public void setDestroy(Long destroy) {
        this.destroy = destroy;
    }

}
