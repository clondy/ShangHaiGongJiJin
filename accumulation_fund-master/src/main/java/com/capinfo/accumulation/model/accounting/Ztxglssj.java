package com.capinfo.accumulation.model.accounting;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
@Entity
@Table(name = "ZTXGLSSj")
@SequenceGenerator(name = "SEQZTXGLSSJ", sequenceName = "SEQ_ZTXGLSSJ", allocationSize = 1)
public class Ztxglssj implements BaseEntity{
	private Long id;
	//账套名称
	private String ztmc;
	//账套代码
	private Long ztdm;
	//账套描述
	private String ztms;
	//核算单位
	private Dictionary hsdw;
	//数据状态
	private int sjzt;
	//插入时间
	private Date crsj;
    private Long ztid;
    //审批状态
    private int spzt;
    //资金类型
    private Dictionary zjlx;
    //组织机构
	private Dictionary zzjg;
	private String czy;
	
	private List<Ztsplssj> ztsplssjList;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ZJLX" ,referencedColumnName="ID" )
	public Dictionary getZjlx() {
		return zjlx;
	}
	public void setZjlx(Dictionary zjlx) {
		this.zjlx = zjlx;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ZZJG" ,referencedColumnName="ID" )
	public Dictionary getZzjg() {
		return zzjg;
	}
	public void setZzjg(Dictionary zzjg) {
		this.zzjg = zzjg;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="HSDW" ,referencedColumnName="ID" )
    public Dictionary getHsdw() {
		return hsdw;
	}
	public void setHsdw(Dictionary hsdw) {
		this.hsdw = hsdw;
	}
	@Basic
	@Column(name = "ZTID")
	public Long getZtid() {
		return ztid;
	}
	public void setZtid(Long ztid) {
		this.ztid = ztid;
	}
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQZTXGLSSJ")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Basic
	@Column(name = "ZTMC")
	public String getZtmc() {
		return ztmc;
	}
	public void setZtmc(String ztmc) {
		this.ztmc = ztmc;
	}
	@Basic
	@Column(name = "ZTDM")
	public Long getZtdm() {
		return ztdm;
	}
	public void setZtdm(Long ztdm) {
		this.ztdm = ztdm;
	}
	@Basic
	@Column(name = "ZTMS")
	public String getZtms() {
		return ztms;
	}
	public void setZtms(String ztms) {
		this.ztms = ztms;
	}

	@Basic
	@Column(name = "SJZT")
	public int getSjzt() {
		return sjzt;
	}
	public void setSjzt(int sjzt) {
		this.sjzt = sjzt;
	}
	@Basic
	@Column(name = "CRSJ")
	public Date getCrsj() {
		return crsj;
	}
	public void setCrsj(Date crsj) {
		this.crsj = crsj;
	}
	@Basic
	@Column(name = "SPZT")
	public int getSpzt() {
		return spzt;
	}
	public void setSpzt(int spzt) {
		this.spzt = spzt;
	}
	
	@Basic
	@Column(name = "CZY")
	public String getCzy() {
		return czy;
	}
	public void setCzy(String czy) {
		this.czy = czy;
	}
	@OneToMany(mappedBy="ztls" ,fetch =FetchType.LAZY )   
	//@OrderBy(clause="crsj DESC")
	public List<Ztsplssj> getZtsplssjList() {
		return ztsplssjList;
	}
	public void setZtsplssjList(List<Ztsplssj> ztsplssjList) {
		this.ztsplssjList = ztsplssjList;
	}
	
	

}
