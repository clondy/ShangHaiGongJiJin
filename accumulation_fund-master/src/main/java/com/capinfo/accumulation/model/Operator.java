package com.capinfo.accumulation.model;



import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;


/**
 * Comment 经办人
 * Created by Rexxxar on 2017/9/19.
 */
@Entity
@Table(name = "OPERATOR")
@SequenceGenerator(name = "seqOPERATOR", sequenceName = "SEQ_OPERATOR", allocationSize = 1)
public class Operator implements BaseEntity {


    private static final long serialVersionUID = 1L;
    //ID
    private Long id;

    //单位_id
    private Long companyId;

    //
    private Company company;

    //
    private CompanyAccountFund companyAccountFund;

    //密码/开户/销户/托管/等等
    private String mask;

    //证件类型
    private Long idCardTypeId;

    //
    private Dictionary idCardType;

    //证件号
    private String idCardNumber;

    //是否有效
    private Boolean enabled;




    public Operator(){

    }

    public Operator(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="seqOPERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMPANY_ID")
    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }


    @ManyToOne(targetEntity = CompanyAccountFund.class)
    @JoinColumn(name = "COMPANY_ACCOUNT_Fund_ID", insertable = false, updatable = false)
    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    @ManyToOne(targetEntity = Company.class)
    @JoinColumn(name = "COMPANY_ID", insertable = false, updatable = false)
    public  Company getCompany(){return company;}

    public void  setCompany(Company company){
        this.company = company;
    }


    @Basic
    @Column(name = "MASK")
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    @ManyToOne(targetEntity = Dictionary.class)
    @JoinColumn(name = "IS_ENABLE", insertable = false, updatable = false)
    public Dictionary getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(Dictionary idCardType) {
        this.idCardType = idCardType;
    }

    @Basic
    @Column(name = "ID_CARD_TYPE")
    public Long getIdCardTypeId() {
        return idCardTypeId;
    }

    public void setIdCardTypeId(Long idCardTypeId) {
        this.idCardTypeId = idCardTypeId;
    }


    @Basic
    @Column(name = "Id_Card_Number")
    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }


    @Basic
    @Column (name = "ENABLED")
    public Boolean getEnabled(){ return enabled;}

    public void setEnabled(Boolean enabled){this.enabled = enabled;}

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if(!(other instanceof Operator))
            return false;
        Operator castOther = (Operator)other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();

    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        //显示出需要看的属性
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
