package com.capinfo.accumulation.model;

import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.model.BaseEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Rexxxar on 2017/9/19.
 *银行专户信息
 * @author fengjingsen 
 */

@Entity
@Table(name = "ACCOUNT_BANK_SPECIAL")
@SequenceGenerator(name = "seqAccountBankSpecial", sequenceName = "SEQ_ACCOUNT_BANK_SPECIALT", allocationSize = 1)
public class AccountBankSpecial implements BaseEntity{
    private Long id;
    //(贯标)科目编号 kmbh
    private String gbSubjectNum;
    //科目编号
    private Long subjectNum;
    //(贯标) 银行名称 yhmc
    private String gbBankName;
    //(贯标) 银行代码yhdm
    private String gbBankCode;
    //银行代码
    private Long bankCode;
    //(贯标) 银行专户名称 yhzhmc
    private String gbBankAccountName;
    //(贯标) 银行专户号码 YHZHHM
    private String gbBankAccountNumber;
    //(贯标) 专户性质 zhxz
    private String gbAccountType;
    //专户性质
    private String accountType;
    //开户日期(贯标) khrq
    private Date gbOpenedDate;
    //销户日期(贯标)  xhrq
    private Date gbPersonCloseDate=new Date();
    //序号
    private String serialNumber;
    //开户行名称
    private String bankName;
    //账户名称
    private String accountName;
    //账号
    private String account;
    //账户性质
    private String natureAccount;
    //账户用途
    private String accountUse;
    //账户开户申请日期
    private Date applicationDate;
    //开户申请书编号
    private String applicationNumber;
    //业务类型
    private String businessTypes;
    //财政局批准文件号
    private String approvalNumber;
    //财政局批准文件日期
    private Date approvalDate=new Date();
    //账户管理人
    private String accountManager;
    //操作时间
    private Date dateTime=new Date();
    //操作员
    private String operatorId;
    private SystemUser operator;
    //区县
    private long regionId;
    private Region region;
    //网点
    private Long siteId;
    //复核人
    private Long confirmerId;
    private SystemUser confirmer;
    //复核时间
    private Date confirmationTime=new Date();
    //渠道
    private Long channels;
    //创建人名称
    private String createName;

    List<TransactionRecord> transactionRecords = new ArrayList<>();

    public AccountBankSpecial(Long id) {
        this.id = id;
    }

    public AccountBankSpecial() {
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAccountBankSpecial")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "KMBH")
    public String getGbSubjectNum() {
        return gbSubjectNum;
    }

    public void setGbSubjectNum(String gbSubjectNum) {
        this.gbSubjectNum = gbSubjectNum;
    }

    @Basic
    @Column(name = "YHMC")
    public String getGbBankName() {
        return gbBankName;
    }

    public void setGbBankName(String gbBankName) {
        this.gbBankName = gbBankName;
    }

    @Basic
    @Column(name = "YHDM")
    public String getGbBankCode() {
        return gbBankCode;
    }

    public void setGbBankCode(String gbBankCode) {
        this.gbBankCode = gbBankCode;
    }

    @Basic
    @Column(name = "YHZHMC")
    public String getGbBankAccountName() {
        return gbBankAccountName;
    }

    public void setGbBankAccountName(String gbBbankAccountName) {
        this.gbBankAccountName = gbBbankAccountName;
    }

    @Basic
    @Column(name = "YHZHHM")
    public String getGbBankAccountNumber() {
        return gbBankAccountNumber;
    }

    public void setGbBankAccountNumber(String gbBankAccountNumber) {
        this.gbBankAccountNumber = gbBankAccountNumber;
    }

    @Basic
    @Column(name = "ZHXZ")
    public String getGbAccountType() {
        return gbAccountType;
    }

    public void setGbAccountType(String gbAccountType) {
        this.gbAccountType = gbAccountType;
    }

    @Basic
    @Column(name = "KHRQ")
    public Date getGbOpenedDate() {
        return gbOpenedDate;
    }

    public void setGbOpenedDate(Date gbOpenedDate) {
        this.gbOpenedDate = gbOpenedDate;
    }

    @Basic
    @Column(name = "XHRQ")
    public Date getGbPersonCloseDate() {
        return gbPersonCloseDate;
    }

    public void setGbPersonCloseDate(Date gbPersonCloseDate) {
        this.gbPersonCloseDate = gbPersonCloseDate;
    }

    @Basic
    @Column(name = "SUBJECT_NUM")
    public Long getSubjectNum() {
        return subjectNum;
    }

    public void setSubjectNum(Long subjectNum) {
        this.subjectNum = subjectNum;
    }

    @Basic
    @Column(name = "BANK_CODE")
    public Long getBankCode() {
        return bankCode;
    }

    public void setBankCode(Long bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "ACCOUNT_TYPE")
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Basic
    @Column(name = "SERIAL_NUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Basic
    @Column(name = "BANK_NAME")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "ACCOUNT_NAME")
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "ACCOUNT")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Basic
    @Column(name = "NATURE_ACCOUNT")
    public String getNatureAccount() {
        return natureAccount;
    }

    public void setNatureAccount(String natureAccount) {
        this.natureAccount = natureAccount;
    }

    @Basic
    @Column(name = "ACCOUNT_USE")
    public String getAccountUse() {
        return accountUse;
    }

    public void setAccountUse(String accountUse) {
        this.accountUse = accountUse;
    }

    @Basic
    @Column(name = "APPLICATION_DATE")
    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Basic
    @Column(name = "APPLICATION_NUMBER")
    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    @Basic
    @Column(name = "BUSINESS_TYPES")
    public String getBusinessTypes() {
        return businessTypes;
    }

    public void setBusinessTypes(String businessTypes) {
        this.businessTypes = businessTypes;
    }

    @Basic
    @Column(name = "APPROVAL_NUMBER")
    public String getApprovalNumber() {
        return approvalNumber;
    }

    public void setApprovalNumber(String approvalNumber) {
        this.approvalNumber = approvalNumber;
    }

    @Basic
    @Column(name = "APPROVAL_DATE")
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Basic
    @Column(name = "ACCOUNT_MANAGER")
    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    @Basic
    @Column(name = "DATE_TIME")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "OPERATOR_ID", insertable = false, updatable = false)
    public SystemUser getOperator() {
        return operator;
    }

    public void setOperator(SystemUser operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @ManyToOne(targetEntity = Region.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "REGION_ID", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Basic
    @Column(name = "SITE_ID")
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "CONFIRMER_ID")
    public Long getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(Long confirmerId) {
        this.confirmerId = confirmerId;
    }

    @ManyToOne(targetEntity = SystemUser.class, cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
    @JoinColumn(name = "CONFIRMER_ID", insertable = false, updatable = false)
    public SystemUser getConfirmer() {
        return confirmer;
    }

    public void setConfirmer(SystemUser confirmer) {
        this.confirmer = confirmer;
    }

    @Basic
    @Column(name = "CONFIRMATION_TIME")
    public Date getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Date confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    @Basic
    @Column(name = "CHANNELS")
    public Long getChannels() {
        return channels;
    }

    public void setChannels(Long channels) {
        this.channels = channels;
    }

    @Basic
    @Column(name = "CREATE_NAME")
    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("gbSubjectNum", getGbSubjectNum())
                .append("subjectNum", getSubjectNum())
                .append("gbBankName", getGbBankName())
                .append("gbBankCode", getGbBankCode())
                .append("bankCode", getBankCode())
                .append("gbBankAccountName", getGbBankAccountName())
                .append("gbBankAccountNumber", getGbBankAccountNumber())
                .append("gbAccountType", getGbAccountType())
                .append("accountType", getAccountType())
                .append("gbOpenedDate", getGbOpenedDate())
                .append("gbPersonCloseDate", getGbPersonCloseDate())
                .append("serialNumber", getSerialNumber())
                .append("bankName", getBankName())
                .append("accountName", getAccountName())
                .append("account", getAccount())
                .append("natureAccount", getNatureAccount())
                .append("accountUse", getAccountUse())
                .append("applicationDate", getApprovalDate())
                .append("applicationNumber", getApplicationNumber())
                .append("businessTypes", getBusinessTypes())
                .append("approvalNumber", getApprovalNumber())
                .append("approvalDate", getApprovalDate())
                .append("accountManager", getAccountManager())
                .append("dateTime", getDateTime())
                .append("operatorId", getOperatorId())
                .append("regionId", getRegionId())
                .append("siteId", getSiteId())
                .append("confirmerId", getConfirmerId())
                .append("confirmationTime", getConfirmationTime())
                .append("channels", getChannels())
                .append("createName", getCreateName()).toString();
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if (!(other instanceof AccountBankSpecial))
            return false;
        AccountBankSpecial otherAccountBankSpecial = (AccountBankSpecial) other;
        return new EqualsBuilder().append(this.getId(),
                otherAccountBankSpecial.getId()).isEquals();
    }

    @OneToMany(mappedBy="accountBankSpecial", targetEntity=TransactionRecord.class)
    public List<TransactionRecord> getTransactionRecords() {
        return transactionRecords;
    }

    public void setTransactionRecords(List<TransactionRecord> transactionRecords) {
        this.transactionRecords = transactionRecords;
    }

}
