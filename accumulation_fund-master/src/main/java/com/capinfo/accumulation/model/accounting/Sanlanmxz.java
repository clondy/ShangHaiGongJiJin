package com.capinfo.accumulation.model.accounting;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.model.BaseEntity;

/**
 * 
 * 三栏明细账
 *
 */
@Entity
@Table(name = "SANLANMXZ" )
@SequenceGenerator(name = "seqSanlanmxz", sequenceName = "SEQ_SANLANMXZ", allocationSize = 1)
public class Sanlanmxz implements BaseEntity{
	//id
	private Long id;
	//日期
	private Date riqi;
	//凭证编码
	private int pzbm;
	//摘要
	private String zhaiyao;
	//科目名称
	private Kmszjbsj kmmc;
	//对应科目
	private Kmszjbsj dykm;
	//借方发生额
	private double jffse;
	//贷方发生额
	private double dffse;
	//余额方向
	private Dictionary yefx;
	//余额
	private double yue;
	//记账状态
	private String jzzt;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqSanlanmxz")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Basic
    @Column(name = "RIQI")
	public Date getRiqi() {
		return riqi;
	}
	public void setRiqi(Date riqi) {
		this.riqi = riqi;
	}
	@Basic
    @Column(name = "PZBM")
	public int getPzbm() {
		return pzbm;
	}
	public void setPzbm(int pzbm) {
		this.pzbm = pzbm;
	}
	@Basic
    @Column(name = "ZHAIYAO")
	public String getZhaiyao() {
		return zhaiyao;
	}
	public void setZhaiyao(String zhaiyao) {
		this.zhaiyao = zhaiyao;
	}
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="kmmc" ,referencedColumnName="kmbh" )
	public Kmszjbsj getKmmc() {
		return kmmc;
	}
	public void setKmmc(Kmszjbsj kmmc) {
		this.kmmc = kmmc;
	}
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="dykm" ,referencedColumnName="kmbh" )
	public Kmszjbsj getDykm() {
		return dykm;
	}
	public void setDykm(Kmszjbsj dykm) {
		this.dykm = dykm;
	}
	@Basic
    @Column(name = "JFFSE")
	public double getJffse() {
		return jffse;
	}
	public void setJffse(double jffse) {
		this.jffse = jffse;
	}
	@Basic
    @Column(name = "DFFSE")
	public double getDffse() {
		return dffse;
	}
	public void setDffse(double dffse) {
		this.dffse = dffse;
	}
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="YEFX" ,referencedColumnName="ID" )
	public Dictionary getYefx() {
		return yefx;
	}

	public void setYefx(Dictionary yefx) {
		this.yefx = yefx;
	}
	@Basic
    @Column(name = "YUE")
	public double getYue() {
		return yue;
	}
	public void setYue(double yue) {
		this.yue = yue;
	}
	public String getJzzt() {
		return jzzt;
	}
	@Basic
    @Column(name = "JZZT")
	public void setJzzt(String jzzt) {
		this.jzzt = jzzt;
	}
	
}
