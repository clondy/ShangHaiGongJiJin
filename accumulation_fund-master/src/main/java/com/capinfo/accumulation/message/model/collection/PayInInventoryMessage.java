/*
 *    	@(#)@PayInInventoryMessage.java  2017年10月27日
 *     
 *      @COPYRIGHT@
 */
package com.capinfo.accumulation.message.model.collection;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * <p>
 * 创建汇补交清册明细时发出的消息体。
 * </p>
 * 
 * @version 1.0
 * @author 张名扬
 */
public class PayInInventoryMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3390092302196665035L;

	// 汇补交单位id
	private Long companyId;

	private Long payinfoId;

	private String companyAccount;

	public PayInInventoryMessage(Long companyId) {
		this.companyId = companyId;
	}

	public PayInInventoryMessage(Long payinfoId, String companyAccount, Long companyAccountId) {

		this.payinfoId = payinfoId;
		this.companyAccount = companyAccount;
		this.companyAccountId = companyAccountId;
	}

	public String getCompanyAccount() {
		return companyAccount;
	}

	public void setCompanyAccount(String companyAccount) {
		this.companyAccount = companyAccount;
	}

	public Long getPayinfoId() {
		return payinfoId;
	}

	public void setPayinfoId(Long payinfoId) {
		this.payinfoId = payinfoId;
	}

	public PayInInventoryMessage() {
		
	}

	private Long companyAccountId;

	public Long getCompanyAccountId() {
		return companyAccountId;
	}

	public void setCompanyAccountId(Long companyAccountId) {
		this.companyAccountId = companyAccountId;
	}



	public Long getCompanyId() {
		
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		
		this.companyId = companyId;
	}
	
	public String toString() {
		
		return ToStringBuilder.reflectionToString(this);
	}

}
