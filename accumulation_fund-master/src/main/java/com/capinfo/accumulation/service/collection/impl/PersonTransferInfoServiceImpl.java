package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.PersonTransferInfo;
import com.capinfo.accumulation.parameter.collection.PersonTransferInfoParameter;
import com.capinfo.accumulation.service.collection.PersonTransferInfoService;
import com.capinfo.framework.dao.SearchCriteria;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

/**
 * Created by Lee on 2017/10/19.
 */
public class PersonTransferInfoServiceImpl extends CommonsDataOperationServiceImpl<PersonTransferInfo, PersonTransferInfoParameter> implements PersonTransferInfoService {
    public SearchCriteriaBuilder<PersonTransferInfo> getSearchCriteriaBuilder(PersonTransferInfoParameter parameter) {

        SearchCriteriaBuilder<PersonTransferInfo> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);

        searchCriteriaBuilder.addQueryCondition("personalAccount", RestrictionExpression.LIKE_OP, parameter.getEntity().getPersonalAccount());
        searchCriteriaBuilder.addOrderCondition("id", SearchCriteria.OrderRow.ORDER_DESC);

        return searchCriteriaBuilder;
    }


}
