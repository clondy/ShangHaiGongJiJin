package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;

/**
 * Created by cairunbin on 2017/10/24.
 */
public interface CompanyAccountFundService extends CommonsDataOperationService<CompanyAccountFund, CompanyAccountFundParameter> {

    /**
     * 补充公积金账户form.jsp，通过单位账号查询
     * 1.单位账号是否已开设补充公积金账户
     * 2.校验单位基本公积金账户为正常缴存
     * @param parameter 单位账号
     * @return 是否存在
     */
    public CompanyAccountFund isExist(CompanyAccountFundParameter parameter);

    /**
     * 审核提交
     * @param parameter  从company对象的id作为参数
     * @return 提交成功返回结果
     */
    public boolean audit(CompanyAccountFundParameter parameter);


    /**
     * 已知ID获取单位
     * @param strID
     * @return
     */
   // public Company getCompanyById(Long strID);

    //根据账户查单位公积金信息
    public List<CompanyAccountFund> getCompanyAccountByCretier(String companyAccount,Long accumulationFundAccountTypeId);



}
