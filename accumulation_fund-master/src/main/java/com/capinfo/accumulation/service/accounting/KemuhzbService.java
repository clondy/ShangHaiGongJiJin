package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Kemuhzb;
import com.capinfo.accumulation.parameter.accounting.KemuhzbParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface KemuhzbService extends CommonsDataOperationService<Kemuhzb, KemuhzbParameter>{

}
