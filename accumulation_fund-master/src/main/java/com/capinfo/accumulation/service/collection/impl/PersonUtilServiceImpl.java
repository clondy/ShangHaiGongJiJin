package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.service.collection.PersonUtilService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

/**
 * Created by xiaopeng on 2017/9/25.
 *  个人实现类
 * @author xiaopeng
 */
public class PersonUtilServiceImpl implements PersonUtilService {
    /*@Autowired
    private LocalSessionFactoryBean sessionFactory4Ecomm;*/
    
    @Autowired
    private JdbcTemplate jdbcTemplate4Ecomm;
    //个人生成规则 待定，目前用单位账号的生成规则
    //生成单位代码
    //isBase是true时，生成基本公积金代码
    //isBase是false时，生成补充公积金代码

    //*
    //旧规则
    // 基本：8位顺序码+校验码+205；补充：209+基本账号的前8位+校验码
    // 新规则
    // 在就规则前增加 字母N ，表明是新系统生成的代码
    // *
    //为保证未来数据迁移回滚方案，编码长度还为12位，8位顺序码设为以9开头，原系统以8开头 （2017-10-18）
    @Override
    public String generateBasePersonCode(){
    	
            List<Map<String, Object>> list=jdbcTemplate4Ecomm.queryForList("select SEQ_PERSON_CODE.nextVal from dual");//sessionFactory4Ecomm.getObject().getCurrentSession().createNativeQuery("select SEQ_COMPANY_CODE.nextVal from dual").getResultList();
        String seqNum=String.valueOf(list.get(0).get("NEXTVAL"));
        //@todo 细化从数据库sequnce读取
        String verifyCode="0";
        //@todo 等了解具体生成规则后，改为从sequnence中读取
        String sequnceNum="9"+StringUtils.leftPad(seqNum+"",7,"0");
        return sequnceNum+verifyCode+"205";
    }
    @Override
    public String generateSupplyPersonCode(String personAccount){
        String verifyCode="0";
        String baseCode=personAccount;
        String sequnceNum=baseCode.substring(1,9);

        return "209"+sequnceNum+verifyCode;
    }

}
