package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Sanlanmxz;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.parameter.accounting.SanlanmxzParameter;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundParameter;
import com.capinfo.accumulation.service.accounting.SanlanmxzService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class SanlanmxzServiceImpl extends CommonsDataOperationServiceImpl<Sanlanmxz, SanlanmxzParameter> implements SanlanmxzService{

	@Override
	public List<Kmszjbsj> findKmmc() {
		// TODO Auto-generated method stub
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	
	 @Override
	 public SearchCriteriaBuilder<Sanlanmxz> getSearchCriteriaBuilder(SanlanmxzParameter parameter){
		 
		 SearchCriteriaBuilder<Sanlanmxz> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
		 if(parameter.getEntity().getKmmc()!=null){
			 searchCriteriaBuilder.addQueryCondition("kmmc.kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmmc().getKmbh());
			 searchCriteriaBuilder.addQueryCondition("jzzt", RestrictionExpression.EQUALS_OP, parameter.getEntity().getJzzt());
			 searchCriteriaBuilder.addQueryCondition("riqi", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartDate());
	         searchCriteriaBuilder.addQueryCondition("riqi", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndDate());
	         searchCriteriaBuilder.addQueryCondition("zhaiyao", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZhaiyao());
	         searchCriteriaBuilder.addQueryCondition("yue", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartMoney());
	         searchCriteriaBuilder.addQueryCondition("yue", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndMoney());
		 }
		

         
         return searchCriteriaBuilder;
	 }

}
