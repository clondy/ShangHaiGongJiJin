package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Fzyeb;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.FzyebParameter;
import com.capinfo.accumulation.service.accounting.FzyebService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzyebServiceImpl extends CommonsDataOperationServiceImpl<Fzyeb, FzyebParameter> implements FzyebService {

	//科目查询
	@Override
	public List<Kmszjbsj> findAllFzlxhsfz() {
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	
	//核算项目查询
	@Override
	public List<Fzhsxmjbsj> findAllHsxmmc(String id) {
		SearchCriteriaBuilder<Fzhsxmjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzhsxmjbsj>(Fzhsxmjbsj.class);
		searchCriteriaBuilder.addQueryCondition("hslxbm", RestrictionExpression.EQUALS_OP, id);
		return  this.getGeneralService().getObjects(searchCriteriaBuilder.build());
	}
	
	//核算类型查询
	@Override
	public List<Fzlxsz> findAllhslx() {
		SearchCriteriaBuilder<Fzlxsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxsz>(Fzlxsz.class);
		List<Fzlxsz> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}

	//页面输入框条件查询
	@Override
	public SearchCriteriaBuilder<Fzyeb> getSearchCriteriaBuilder(FzyebParameter parameter) {
		SearchCriteriaBuilder<Fzyeb> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzyeb>(Fzyeb.class);
		if(parameter.getEntity().getKmbm()!=null || parameter.getEntity().getHsxmbm()!=null){
			searchCriteriaBuilder.addQueryCondition("kmbm.kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmbm().getKmbh());
			searchCriteriaBuilder.addQueryCondition("hsxmbm.hsxmbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsxmbm().getHsxmbm());
			searchCriteriaBuilder.addQueryCondition("insertdate", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStarttime());
			searchCriteriaBuilder.addQueryCondition("insertdate", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndtime());
		}
		return searchCriteriaBuilder;
	}

}