package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.parameter.collection.PayinitemParameter;
import com.capinfo.accumulation.service.collection.PayinitemService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wcj on 2017/10/30.
 */
public class PayinitemServiceImpl extends CommonsDataOperationServiceImpl<Payinitem, PayinitemParameter> implements PayinitemService{


    public Boolean saveOrUpdateBatch(List<PersonAccountFund> personAccountFunds, String companyAccount, Long payinfoId) {
        List<Payinitem> payinitems = new ArrayList<>();
        if(!personAccountFunds.isEmpty()) {
            for (PersonAccountFund personAccountFund : personAccountFunds) {

                Payinitem payinitem = new Payinitem();

                payinitem.setPayininfoId(payinfoId);
                payinitem.setPersonAccountFundId(personAccountFund.getId());
                payinitem.setCompanyAccount(companyAccount);

                getGeneralService().fetchLazyProperty(personAccountFund, "person");
                Person person = personAccountFund.getPerson();
                payinitem.setPeopleMonthSealed(new BigDecimal(person.getWageIncome().doubleValue() * personAccountFund.getPersonPaymentRatio() * 1.0 / 100));
                payinitem.setCompanyMonthSealed(
                        new BigDecimal(person.getWageIncome().doubleValue()
                                * personAccountFund.getCompanyPaymentRatio() * 1.0 / 100));
                payinitem.setAmountAccount(new BigDecimal(personAccountFund.getRegularBalance().doubleValue()
                        + personAccountFund.getCurrentBalance().doubleValue()));
                payinitem.setDateTime(new Date());
                payinitems.add(payinitem);


            }
            getGeneralService().saveOrUpdateAll(payinitems);
            payinitems.clear();
            return true;
        }
       return false;
    }

}
