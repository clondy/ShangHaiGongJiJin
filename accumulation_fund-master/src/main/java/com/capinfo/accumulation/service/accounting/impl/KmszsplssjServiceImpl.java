package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszsplssj;
import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.parameter.accounting.KmszsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.accumulation.service.accounting.KmszsplssjService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class KmszsplssjServiceImpl extends  CommonsDataOperationServiceImpl<Kmszsplssj, KmszsplssjParameter> implements KmszsplssjService{

	public List<Kmszsplssj> dantiaochaxun(KmszsplssjParameter parameter) {
		SearchCriteriaBuilder<Kmszsplssj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszsplssj>(Kmszsplssj.class);
		searchCriteriaBuilder.addQueryCondition("kmszls", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmszls());
		List<Kmszsplssj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	
	
	 @Override
	    public SearchCriteriaBuilder<Kmszsplssj> getSearchCriteriaBuilder(KmszsplssjParameter parameter) {
	        SearchCriteriaBuilder<Kmszsplssj> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
	        searchCriteriaBuilder.addQueryCondition("kmszls", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmszls());
	        return searchCriteriaBuilder;
	    }
}
