package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundHistoryService;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteria;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by cairunbin on 2017/10/24.
 */
public class CompanyAccountFundServiceImpl extends CommonsDataOperationServiceImpl<CompanyAccountFund, CompanyAccountFundParameter>  implements CompanyAccountFundService {

    private static final Log logger = LogFactory.getLog(RegionService.class);

    @Autowired
    private CompanyAccountFundHistoryService companyAccountFundHistoryService;


    public CompanyAccountFund isExist(CompanyAccountFundParameter parameter) {
        CompanyAccountFund companyAccountFund = parameter.getEntity();
        SearchCriteriaBuilder<CompanyAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<CompanyAccountFund>(CompanyAccountFund.class);
        if (parameter.getEntity() != null){
            if (parameter.getEntity().getCompany() != null){
                searchCriteriaBuilder.addQueryCondition("company.gbCompanyAccount", RestrictionExpression.EQUALS_OP, parameter.getEntity().getCompany().getGbCompanyAccount());
            }

        }
        searchCriteriaBuilder.addOrderCondition("id", SearchCriteria.OrderRow.ORDER_DESC);
        System.out.println(searchCriteriaBuilder);
        List<CompanyAccountFund> companyAccountFunds = this.getGeneralService().getObjects(searchCriteriaBuilder.build());
        if (companyAccountFunds == null || companyAccountFunds.size() == 0)
            return null;

        //存在两个基本、补充公积金账户  倒序取值
        return companyAccountFunds.get(0);//取到基本公积金账户 或者 取到补充公积金账户
    }

    @Override
    public boolean audit(CompanyAccountFundParameter parameter) {
        CompanyAccountFund curCompanyAccountFund = parameter.getEntity();
        CompanyAccountFund companyAccountFund;

        CompanyAccountHistory companyAccountHistory = null;
        System.out.println(curCompanyAccountFund.getId()+"******单位公积金历史ID******");
        if (curCompanyAccountFund.getId() != null) {
            companyAccountHistory = companyAccountFundHistoryService.getCompanyAccountHistoryById(curCompanyAccountFund.getId());
            if (companyAccountHistory == null)
                return false;

            companyAccountFund = new CompanyAccountFund();

            BeanUtils.copyProperties(companyAccountHistory, companyAccountFund,
                    new String[]{"id", "companyAccountFundId","companyHistoryId","delegationPaymentId"});
            companyAccountFund.setCompanyId(companyAccountHistory.getCompanyHistoryId());
            parameter.setEntity(companyAccountFund);
        } else {
            return false;
        }
        //companyAccountHistory.setCompanyAccountFundId();
        companyAccountFund = this.saveOrUpdate(parameter);
        if (companyAccountFund.getId() != null) {
            companyAccountFundHistoryService.auditInfo(curCompanyAccountFund.getId(), companyAccountFund.getId(), null);
        } else {
            return false;
        }
        return true;
    }

    //补充公积金账号查询
   @Override
   public SearchCriteriaBuilder<CompanyAccountFund> getSearchCriteriaBuilder(CompanyAccountFundParameter parameter){
       SearchCriteriaBuilder<CompanyAccountFund> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
        if(parameter.getEntity() != null){
            if(parameter.getEntity().getCompany()!=null){
                searchCriteriaBuilder.addQueryCondition("paymentSite", RestrictionExpression.EQUALS_OP, parameter.getEntity().getCompany().getPaymentSite());
                searchCriteriaBuilder.addQueryCondition("companyAccount", RestrictionExpression.EQUALS_OP, parameter.getEntity().getCompanyAccount());
                searchCriteriaBuilder.addQueryCondition("unitName", RestrictionExpression.LIKE_OP, parameter.getEntity().getUnitName());
            }
        }
        if(parameter.getStartOpenDate()!=null){
            searchCriteriaBuilder.addQueryCondition("openDate", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartOpenDate());
        }
        if(parameter.getEndOpenDate()!=null){
            searchCriteriaBuilder.addQueryCondition("openDate", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndOpenDate());
        }
      searchCriteriaBuilder.addQueryCondition("accumulationFundAccountTypeId", RestrictionExpression.EQUALS_OP, 88L);

       return searchCriteriaBuilder;
   }

     @Override
     public List<CompanyAccountFund> getCompanyAccountByCretier(String companyAccount,Long accumulationFundAccountTypeId) {
         SearchCriteriaBuilder<CompanyAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<CompanyAccountFund>(CompanyAccountFund.class);
         if(companyAccount != null){
             searchCriteriaBuilder.addQueryCondition("companyAccount",RestrictionExpression.EQUALS_OP,companyAccount);
         }
         if(accumulationFundAccountTypeId != null){
             searchCriteriaBuilder.addQueryCondition("accumulationFundAccountType",RestrictionExpression.EQUALS_OP,accumulationFundAccountTypeId);
         }

         List<CompanyAccountFund> companyAccounts = this.getGeneralService().getObjects(searchCriteriaBuilder.build());
         return companyAccounts;
     }
}
