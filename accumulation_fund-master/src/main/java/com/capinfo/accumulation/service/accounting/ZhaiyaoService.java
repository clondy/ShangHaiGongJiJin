package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Zhaiyao;
import com.capinfo.accumulation.parameter.accounting.ZhaiyaoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface ZhaiyaoService extends CommonsDataOperationService<Zhaiyao, ZhaiyaoParameter>{

}
