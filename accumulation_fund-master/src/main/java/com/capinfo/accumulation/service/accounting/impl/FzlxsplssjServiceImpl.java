package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzlxsplssj;
import com.capinfo.accumulation.parameter.accounting.FzlxsplssjParameter;
import com.capinfo.accumulation.service.accounting.FzlxsplssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzlxsplssjServiceImpl extends CommonsDataOperationServiceImpl<Fzlxsplssj, FzlxsplssjParameter> implements FzlxsplssjService{
	 private static final Log logger = LogFactory.getLog(RegionService.class);
}
