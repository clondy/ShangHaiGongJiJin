package com.capinfo.accumulation.service.collection;


import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundHistoryParameter;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * Created by cairunbin on 2017/10/24.
 */
public interface CompanyAccountFundHistoryService extends CommonsDataOperationService<CompanyAccountHistory,CompanyAccountFundHistoryParameter>{

    /**
     *将单位补充公积金信息存入单位公积金历史表
     *@Author: cairunbin
     *@Description
     *@Date: 2017/10/30 11:17
     */

    CompanyAccountHistory saveOrUpdate(CompanyAccountFundHistoryParameter companyAccountFundHistoryParameter);


    /*
     * 根据ID查询单位历史
     *@Author:cairunbin
     *@Description
     *@Date: 2017/10/30 14:21
     */

    CompanyAccountHistory getCompanyAccountHistoryById(Long Id);


    /**
     * 根据单位公积金ID
     * 将单位公积金表与单位公积金历史表 一对多关联
     * 分别将单位公积金历史ID插入单位公积金表
     * 和单位公积金ID将单位公积金历史表
     *@Author:cairunbin
     *@Description
     *@Date: 2017/10/31 10:17
     */

    public boolean auditInfo(Long id, Long companyAccountFundId, Long confirmerId);


    /**
     * 通过companyId得到基本公积金账户信息
     * @param companyId
     * @return
     */
    public CompanyAccountHistory getCompanyAccountFundByCompanyId(Long companyId, CompanyAccountFundHistoryParameter parameter);
}
