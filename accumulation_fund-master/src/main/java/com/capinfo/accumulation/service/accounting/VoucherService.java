package com.capinfo.accumulation.service.accounting;
import java.util.List;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface VoucherService  extends CommonsDataOperationService<Voucher, VoucherParameter>{
	
	/**
	 * 记账凭证复核
	 * @param parameter
	 */
	void jzpzfh(VoucherParameter parameter);
	
}
