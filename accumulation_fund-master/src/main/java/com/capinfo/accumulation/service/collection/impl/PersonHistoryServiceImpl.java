package com.capinfo.accumulation.service.collection.impl;


import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.parameter.collection.PersonHistoryParameter;
import com.capinfo.accumulation.service.collection.CompanyUtilService;
import com.capinfo.accumulation.service.collection.PersonHistoryService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by Rexxxar on 2017/9/28.
 */

public class PersonHistoryServiceImpl extends CommonsDataOperationServiceImpl<PersonHistory,PersonHistoryParameter> implements PersonHistoryService {

    private static final Log logger = LogFactory.getLog(CompanyHistory.class);

    @Autowired
    CompanyUtilService companyUtilService;

    @Override
    public SearchCriteriaBuilder<PersonHistory> getSearchCriteriaBuilder(PersonHistoryParameter parameter) {

        SearchCriteriaBuilder<PersonHistory> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);

        searchCriteriaBuilder.addQueryCondition("personId", RestrictionExpression.EQUALS_OP, parameter.getEntity().getPersonId());
        return searchCriteriaBuilder;
    }
}





