package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Ztsplssj;
import com.capinfo.accumulation.parameter.accounting.ZtsplssjParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface ZtsplssjService extends CommonsDataOperationService<Ztsplssj, ZtsplssjParameter>{

}
