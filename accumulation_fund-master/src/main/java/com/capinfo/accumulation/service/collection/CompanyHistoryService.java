package com.capinfo.accumulation.service.collection;


import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;


/**
 * Created by Rexxxar on 2017/9/28.
 */
public interface CompanyHistoryService extends CommonsDataOperationService<CompanyHistory,CompanyHistoryParameter>{


    /**
     *将单位信息保存在单位历史表和单位公积金历史表
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/9/28 15:45
     */

    @Override
    CompanyHistory saveOrUpdate(CompanyHistoryParameter companyHistoryParameter);


    /**
     * 根据ID查询单位历史
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/10 9:57
     */

    CompanyHistory getCompanyHistoryById(Long Id);


    /**
     * 根据ID
     * 将公司ID、复审人
     * 分别插入单位信息历史表和单位公积金历史表
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/11 10:17
     */

    public boolean auditInfo(Long id, Long companyId, Long confirmerId);
    //网点变更查询
    //fjs
    public List<CompanyHistory> getList(CompanyHistoryParameter parameter, long dataTotalCount, int currentPieceNum);

    CompanyHistory saveChangesite(CompanyHistoryParameter companyHistoryParameter);

    List<CompanyHistory> getCompanyInfoChangeList(CompanyHistoryParameter parameter, long dataTotalCount, int currentPieceNum);

    CompanyHistory saveBasicInfoChange(CompanyHistoryParameter companyHistoryParameter);
}
