package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Fzyeb;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.FzyebParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface FzyebService extends CommonsDataOperationService<Fzyeb, FzyebParameter> {

	/**
	 * 科目查询
	 * @return
	 */
	public List<Kmszjbsj> findAllFzlxhsfz();
	
	/**
	 * 核算项目查询
	 * @return
	 */
	public List<Fzhsxmjbsj> findAllHsxmmc(String id);
	
	/**
	 * 核算类型查询
	 * @return
	 */
	public List<Fzlxsz> findAllhslx();
}
