package com.capinfo.accumulation.service.accounting.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.service.accounting.VoucherService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import com.capinfo.framework.util.LogUtils;

public class VoucherServiceImpl extends  CommonsDataOperationServiceImpl<Voucher, VoucherParameter> implements VoucherService {

	
	public SearchCriteriaBuilder<Voucher> getSearchCriteriaBuilder(VoucherParameter parameter) {
		SearchCriteriaBuilder<Voucher> searchCriteriaBuilder = new SearchCriteriaBuilder<Voucher>(Voucher.class);
		if(parameter.getFhzt()!=null){
		if(parameter.getFhzt().equals("1")){
			searchCriteriaBuilder.addQueryCondition("fh", RestrictionExpression.IS_NOT_NULL);
		}else if (parameter.getFhzt().equals("0"))
		{
			searchCriteriaBuilder.addQueryCondition("fh", RestrictionExpression.IS_NULL);
		}
		}
        searchCriteriaBuilder.addQueryCondition("pzlb", RestrictionExpression.EQUALS_OP, parameter.getEntity().getPzlb());
        searchCriteriaBuilder.addQueryCondition("jzrq", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartTime());
        searchCriteriaBuilder.addQueryCondition("jzrq", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndTime());
        searchCriteriaBuilder.addQueryCondition("jzpzh", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartJzpzh());
        searchCriteriaBuilder.addQueryCondition("jzpzh", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndJzpzh());
        return searchCriteriaBuilder;
    }

	@Override
	public void jzpzfh(VoucherParameter parameter) {
		if(parameter.getJzpzids()!=null){
			String[] ids = parameter.getJzpzids().split(",");
			for(int i =0 ; i< ids.length ; i++){
				//getCommonsDataOperationService().saveOrUpdate(parameter);
				SearchCriteriaBuilder<Voucher> searchCriteriaBuilder = new SearchCriteriaBuilder<Voucher>(Voucher.class);
				searchCriteriaBuilder.addQueryCondition("xspzh", RestrictionExpression.EQUALS_OP, ids[i]);
				List<Voucher> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
				for(int j = 0 ; j<entities.size() ; j++){
					Voucher tmpVoucher = 	entities.get(j);
					tmpVoucher.setFhrq(new Date());
					tmpVoucher.setFh("管理员");
					this.saveOrUpdate(tmpVoucher);
				}
			}
		}
		
		
	}
	
}
