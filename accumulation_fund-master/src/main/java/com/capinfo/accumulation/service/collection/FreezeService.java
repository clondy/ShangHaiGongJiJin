package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Freeze;
import com.capinfo.accumulation.parameter.collection.FreezeParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by thinkpadwyl on 2017/10/19.
 */

public interface FreezeService extends CommonsDataOperationService<Freeze, FreezeParameter> {
    /**
     * 个人征信是否存在
     * @param parameter 冻结信息
     * @return 是否存在
     */
    public boolean isExist(FreezeParameter parameter);

    /**
     * 冻结审核
     * @param parameter  冻结信息
     * @return 成功与否
     */
    public boolean freezeAudit(FreezeParameter parameter);

    /**
     * 解冻冻审核
     * @param parameter  冻结信息
     * @return 成功与否
     */
    public boolean unfreezeAudit(FreezeParameter parameter);

    /**
     * 解冻定时任务
     */
    public void unfreezeJob();

    /**
     * 延期
     * @param parameter 延期信息
     * @return 成功与否
     */
    public boolean delay(FreezeParameter parameter);

    /**
     * 解冻
     * @param parameter 解冻信息
     * @return 成功与否
     */
    public boolean unfreeze(FreezeParameter parameter);
}
