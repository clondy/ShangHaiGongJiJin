package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.accumulation.model.collection.PauseRecoveryRecord;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryInventoryParameter;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryRecordParameter;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.service.CommonsDataOperationService;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;


/**
 * Created by Rexxxar on 2017/10/19.
 */
public interface PauseRecoveryInventoryService extends CommonsDataOperationService <PauseRecoveryInventory,PauseRecoveryInventoryParameter>{

    public PauseRecoveryInventory saveOrUpdateInventory(PauseRecoveryInventoryParameter parameter);


}
