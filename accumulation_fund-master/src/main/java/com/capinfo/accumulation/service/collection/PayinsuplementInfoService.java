package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.PayinSupplementInfo;
import com.capinfo.accumulation.model.collection.Payinitem;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.parameter.collection.PayinitemParameter;
import com.capinfo.accumulation.parameter.collection.PayinsuplementInfoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by wcj on 2017/10/31.
 */
public interface PayinsuplementInfoService extends CommonsDataOperationService<PayinSupplementInfo, PayinsuplementInfoParameter> {
        public PayinSupplementInfo buildPayinSupplementInfoQc(PayinsuplementInfoParameter parameter,String operator,Long operateId);
}
