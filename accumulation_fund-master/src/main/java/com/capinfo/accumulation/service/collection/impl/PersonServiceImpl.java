package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.parameter.collection.PersonParameter;
import com.capinfo.accumulation.service.collection.CompanyHistoryService;
import com.capinfo.accumulation.service.collection.PersonService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by wangyulin on 2017/9/27.
 */
public class PersonServiceImpl extends CommonsDataOperationServiceImpl<Person, PersonParameter>  implements PersonService {

    private static final Log logger = LogFactory.getLog(RegionService.class);

    @Autowired
    private CompanyHistoryService historyService;

    /**
     * 根据传递的参数判断业务
     * @param parameter
     * @return
     */
    @Override
    public SearchCriteriaBuilder<Person> getSearchCriteriaBuilder(PersonParameter parameter) {
        String deposit = parameter.getDeposit();    //封存
        String extra = parameter.getExtra();    //补充
        SearchCriteriaBuilder<Person> searchCriteriaBuilder;
        searchCriteriaBuilder = new SearchCriteriaBuilder<Person>(Person.class);
//        if(deposit==deposit){
//            searchCriteriaBuilder.addQueryCondition("creditCode", RestrictionExpression.EQUALS_OP, "封存");
//        }
//        if(extra==extra){
//            searchCriteriaBuilder.addQueryCondition("creditCode", RestrictionExpression.EQUALS_OP, "补充");
//        }


        return super.getSearchCriteriaBuilder(parameter);
    }

    @Override
    public Company getDepositPerson() {
        SearchCriteriaBuilder<Company> searchCriteriaBuilder = new SearchCriteriaBuilder<Company>(Company.class);
        searchCriteriaBuilder.addQueryCondition("gbCompanyName", RestrictionExpression.EQUALS_OP, " 市公积金管理中心住房公积金集中封存专户");
        List<Company> companys=this.getGeneralService().getObjects(searchCriteriaBuilder.build());
        if(companys ==  null|| companys.size()==0){
            return null;
        }
        return companys.get(0);
    }

    @Override
    public Person getPerson(PersonParameter parameter) {
        SearchCriteriaBuilder<Person> searchCriteriaBuilder = new SearchCriteriaBuilder<Person>(Person.class);
        //个人账号
        searchCriteriaBuilder.addQueryCondition("personalAccount", RestrictionExpression.EQUALS_OP,parameter.getEntity().getPersonalAccount());
        searchCriteriaBuilder.addQueryCondition("idCardNumber", RestrictionExpression.EQUALS_OP,parameter.getEntity().getIdCardNumber());
        long count = getGeneralService().getCount(searchCriteriaBuilder.build());
        if(count>0){
            List<Person> objects = getGeneralService().getObjects(searchCriteriaBuilder.build());
            return objects.get(0);
        }else{
            return null;
        }

    }
}
