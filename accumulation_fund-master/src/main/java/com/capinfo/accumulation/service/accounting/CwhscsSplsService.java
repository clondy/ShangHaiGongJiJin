package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.CwhscsSpls;
import com.capinfo.accumulation.parameter.accounting.CwhscsSplsParameter;
import com.capinfo.framework.service.CommonsDataOperationService;
/**
 * 财务核算参数设置审批流水Serviuce
 *
 */
public interface CwhscsSplsService  extends CommonsDataOperationService<CwhscsSpls, CwhscsSplsParameter>{

}
