package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.accumulation.model.collection.PauseRecoveryRecord;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryInventoryParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.accumulation.service.collection.PauseRecoveryInventoryService;
import com.capinfo.framework.dao.SearchCriteria;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.GeneralService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import com.capinfo.framework.util.LogUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;

/**
 * Created by capinfo on 2017/10/31.
 */
public class PauseRecoveryInventoryServiceImpl extends CommonsDataOperationServiceImpl<PauseRecoveryInventory,PauseRecoveryInventoryParameter> implements PauseRecoveryInventoryService{
    private static final Log logger = LogFactory.getLog(PauseRecoveryInventoryService.class);

    @Autowired
    private CompanyAccountFundService companyAccountFundService;
    /**
     * 停复缴清册查询
     * @Author: Rexxxar
     * @Description
     * @Date: 2017/10/31 18:56
     */
    @Override
    public SearchCriteriaBuilder<PauseRecoveryInventory> getSearchCriteriaBuilder(PauseRecoveryInventoryParameter parameter) {
        SearchCriteriaBuilder<PauseRecoveryInventory> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);

        searchCriteriaBuilder.addQueryCondition("status", RestrictionExpression.EQUALS_OP, parameter.getPauseRecoveryInventoryFlag());
        searchCriteriaBuilder.addQueryCondition("companyAccountFund.companyAccount", RestrictionExpression.EQUALS_OP, parameter.getCompanyAccountFund().getCompanyAccount());
        searchCriteriaBuilder.addQueryCondition("companyAccountFund.unitName", RestrictionExpression.EQUALS_OP, parameter.getCompanyAccountFund().getUnitName());

        return searchCriteriaBuilder;
    }

    /**
     * 通过单位账号查询单位公积金对象
     * @Author: Rexxxar
     * @Description
     * @Date: 2017/10/31 18:56
     */
    public CompanyAccountFund searchCompanyAccountFundByCompanyAccount(String companyAccount){

        SearchCriteriaBuilder<CompanyAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<CompanyAccountFund>(CompanyAccountFund.class);

        searchCriteriaBuilder.addQueryCondition("companyAccount",RestrictionExpression.EQUALS_OP,companyAccount);

        return this.getGeneralService().getObjectByCriteria(searchCriteriaBuilder.build());
    }

    /**
     * 停复缴清册添加
     * @Author: Rexxxar
     * @Description
     * @Date: 2017/10/31 22:08
     */
    @Override
    public PauseRecoveryInventory saveOrUpdateInventory(PauseRecoveryInventoryParameter parameter) {
        PauseRecoveryInventory entity = parameter.getEntity();
        Class<PauseRecoveryInventory> entityClazz = parameter.getEntityClazz();

        Long id = (null == parameter.getId()) ? entity.getId(): parameter.getId();

        if (null == id) {

            try {

                CompanyAccountFund companyAccountFund = searchCompanyAccountFundByCompanyAccount(parameter.getCompanyAccountFund().getCompanyAccount());

                //entity = getGeneralService().getObjectById(PauseRecoveryInventory.class, entity.getId());
                entity.setCompanyAccountFundId(companyAccountFund.getId());
                entity.setStatus(parameter.getPauseRecoveryInventoryFlag());
                entity.setRegionId(10010L);
                entity.setCompanyId(companyAccountFund.getCompanyId());

              /*  PauseRecoveryInventory newEntity = new PauseRecoveryInventory();

                BeanUtils.copyProperties(entity, newEntity, new String[]{"id","company","companyAccountFund","operator",});
                getGeneralService().persist(newEntity);*/
                getGeneralService().persist(entity);

                return entity;
            } catch (Exception e) {

                LogUtils.debugException(logger, e);
            }
        }else {

           /* try {
                PauseRecoveryInventory pauseRecoveryInventory = getGeneralService().getObjectById(entityClazz, id);

                //入库处理逻辑
                getGeneralService().update(entity);

                return entity;
            } catch (Exception e) {

                LogUtils.debugException(logger, e);
                throw e;
            }*/
            try {

                PauseRecoveryInventory pauseRecoveryInventory = getGeneralService().getObjectById(entityClazz, id);
                BeanUtils.copyProperties(entity, pauseRecoveryInventory, this.getIgnorePropertiesWithSaveOrUpdate());
                this.getGeneralService().update(pauseRecoveryInventory);

                return pauseRecoveryInventory;
            } catch (Exception e) {

                LogUtils.debugException(logger, e);
            }
        }



        return null;
    }
}
