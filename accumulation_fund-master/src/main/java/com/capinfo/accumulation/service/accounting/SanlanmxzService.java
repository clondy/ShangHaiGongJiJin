package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Sanlanmxz;
import com.capinfo.accumulation.parameter.accounting.SanlanmxzParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface SanlanmxzService extends CommonsDataOperationService<Sanlanmxz, SanlanmxzParameter>{
	public List<Kmszjbsj> findKmmc();
}
