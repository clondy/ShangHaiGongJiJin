package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fuzhuzhang;
import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Sanlanmxz;
import com.capinfo.accumulation.parameter.accounting.FuzhuzhangParameter;
import com.capinfo.accumulation.parameter.accounting.SanlanmxzParameter;
import com.capinfo.accumulation.service.accounting.FuzhuzhangService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FuzhuzhangServiceImpl extends CommonsDataOperationServiceImpl<Fuzhuzhang, FuzhuzhangParameter> implements FuzhuzhangService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	@Override
	public List<Kmszjbsj> findKmmc() {
		// TODO Auto-generated method stub
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	@Override
	public List<Fzlxsz> findFzlb() {
		// TODO Auto-generated method stub
		SearchCriteriaBuilder<Fzlxsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxsz>(Fzlxsz.class);
		List<Fzlxsz> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	@Override
	public List<Fzhsxmjbsj> findHsxmmc() {
		// TODO Auto-generated method stub
		SearchCriteriaBuilder<Fzhsxmjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzhsxmjbsj>(Fzhsxmjbsj.class);
		List<Fzhsxmjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	
	 @Override
	 public SearchCriteriaBuilder<Fuzhuzhang> getSearchCriteriaBuilder(FuzhuzhangParameter parameter){
		 
		 SearchCriteriaBuilder<Fuzhuzhang> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
		 if(parameter.getEntity().getKmmc()!=null){
			 searchCriteriaBuilder.addQueryCondition("kmmc.kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmmc().getKmbh());
			 searchCriteriaBuilder.addQueryCondition("fzlb.hslxbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getFzlb().getHslxbm());
			 searchCriteriaBuilder.addQueryCondition("hsxmmc.hsxmbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsxmmc().getHsxmbm());
			 searchCriteriaBuilder.addQueryCondition("jzzt", RestrictionExpression.EQUALS_OP, parameter.getEntity().getJzzt());
			 searchCriteriaBuilder.addQueryCondition("riqi", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartDate());
	         searchCriteriaBuilder.addQueryCondition("riqi", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndDate());
	         searchCriteriaBuilder.addQueryCondition("zhaiyao", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZhaiyao());
		 }
		

        
        return searchCriteriaBuilder;
	 }
}
