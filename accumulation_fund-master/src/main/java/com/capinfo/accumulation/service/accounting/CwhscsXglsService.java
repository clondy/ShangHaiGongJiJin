package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.CwhscsXgls;
import com.capinfo.accumulation.parameter.accounting.CwhscsXglsParameter;
import com.capinfo.framework.service.CommonsDataOperationService;
/**
 * 财务核算参数设置修改流水Serviuce
 *
 */
public interface CwhscsXglsService  extends CommonsDataOperationService<CwhscsXgls, CwhscsXglsParameter>{

}
