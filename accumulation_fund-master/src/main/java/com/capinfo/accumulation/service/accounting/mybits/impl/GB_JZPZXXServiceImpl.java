package com.capinfo.accumulation.service.accounting.mybits.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.capinfo.accumulation.dao. GB_JZPZXXDao;
import com.capinfo.accumulation.model.accounting. GB_JZPZXX;
import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.GB_JZPZXXServiceI;
@Service("gB_JZPZXXService")
public class  GB_JZPZXXServiceImpl implements  GB_JZPZXXServiceI {
	
	@Autowired
	 GB_JZPZXXDao  gB_JZPZXXDao;
	@Override
	public int deleteByPrimaryKey(String id) {
		return this.gB_JZPZXXDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(GB_JZPZXX record) {
		return this.gB_JZPZXXDao.insertSelective(record);
	}

	@Override
	public GB_JZPZXX selectByPrimaryKey(String id) {
		return this.gB_JZPZXXDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(GB_JZPZXX record) {
		return this.gB_JZPZXXDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public DataGrid findbyByGrid(Map map) {
		DataGrid dataGrid = new DataGrid();
		map.put("dataGrid", dataGrid);
		List<GB_JZPZXX> retList = this.gB_JZPZXXDao.findbyByGrid(map);
		dataGrid.setRows(retList);
		return dataGrid;
	}

	@Override
	public List<Map<String, Object>> hsdw() {
		return this.gB_JZPZXXDao.hsdw();
	}

	@Override
	public List<Map<String, Object>> cwzt() {
		return this.gB_JZPZXXDao.cwzt();
	}
	
}
