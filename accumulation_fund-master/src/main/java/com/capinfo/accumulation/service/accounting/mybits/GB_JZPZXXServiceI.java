package com.capinfo.accumulation.service.accounting.mybits;

import java.util.List;
import java.util.Map;

import com.capinfo.accumulation.model.accounting.GB_JZPZXX;
import com.capinfo.accumulation.service.accounting.BaseServiceI;
public interface GB_JZPZXXServiceI extends BaseServiceI<GB_JZPZXX> {


	
	public List<Map<String, Object>> hsdw();
	
	public List<Map<String, Object>> cwzt();

}
