package com.capinfo.accumulation.service.accounting.impl;
import java.util.List;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.service.accounting.VoucherjzService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class VoucherjzServiceImpl  extends  CommonsDataOperationServiceImpl<Voucher, VoucherParameter> implements VoucherjzService {
	
	public SearchCriteriaBuilder<Voucher> getSearchCriteriaBuilder(VoucherParameter parameter) {
		SearchCriteriaBuilder<Voucher> searchCriteriaBuilder = new SearchCriteriaBuilder<Voucher>(Voucher.class);
		if(parameter.getJzzt()!=null){
			if(parameter.getJzzt().equals("1")){
				searchCriteriaBuilder.addQueryCondition("jz", RestrictionExpression.IS_NOT_NULL);
			}else if (parameter.getJzzt().equals("0"))
			{
				searchCriteriaBuilder.addQueryCondition("jz", RestrictionExpression.IS_NULL);
			}
			}
          searchCriteriaBuilder.addQueryCondition("xspzh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getJzpzh());
          searchCriteriaBuilder.addQueryCondition("jzrq", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartTime());
          searchCriteriaBuilder.addQueryCondition("jzrq", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndTime());
          searchCriteriaBuilder.addQueryCondition("jzpzh", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartJzpzh());
          searchCriteriaBuilder.addQueryCondition("jzpzh", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndJzpzh());
        return searchCriteriaBuilder;
        
    }

	@Override
	public List<Voucher> jzpzcx(VoucherParameter parameter) {
		SearchCriteriaBuilder<Voucher> searchCriteriaBuilder = new SearchCriteriaBuilder<Voucher>(Voucher.class);
		searchCriteriaBuilder.addQueryCondition("xspzh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getXspzh());
		List<Voucher> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}

	

	

}
