package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.capinfo.accumulation.model.accounting.Fzlxxglssj;
import com.capinfo.accumulation.parameter.accounting.FzlxxglssjParameter;
import com.capinfo.accumulation.service.accounting.FzlxxglssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzlxxglssjServiceImpl  extends CommonsDataOperationServiceImpl<Fzlxxglssj, FzlxxglssjParameter> implements FzlxxglssjService{
	 private static final Log logger = LogFactory.getLog(RegionService.class);
	 //模糊查询的方法
//	@Override
//	public SearchCriteriaBuilder<Fzlxxglssj> getSearchCriteriaBuilder(FzlxxglssjParameter parameter) {
//		SearchCriteriaBuilder<Fzlxsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxsz>(Fzlxsz.class);
//		searchCriteriaBuilder.addQueryCondition("hslxbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHslxbm());
//		searchCriteriaBuilder.addQueryCondition("hslxmc", RestrictionExpression.LIKE_OP, parameter.getEntity().getHslxmc());
//		searchCriteriaBuilder.addQueryCondition("hsfl", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsfl());
//		searchCriteriaBuilder.addQueryCondition("sjzt", RestrictionExpression.EQUALS_OP, parameter.getEntity().getSjzt());
//		//如果等于Null 主界面会直接不现实
//		if(parameter.getEntity().getHsdw() != null){
//			searchCriteriaBuilder.addQueryCondition("hsdw", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsdw().getId());
//		}
//		return searchCriteriaBuilder;
//	}

}
