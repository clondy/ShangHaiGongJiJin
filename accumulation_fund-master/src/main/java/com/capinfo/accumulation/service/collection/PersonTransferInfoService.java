package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.CompanyRecord;
import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.PersonTransferInfo;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.PersonTransferInfoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by Lee on 2017/10/19.
 */
public interface PersonTransferInfoService extends CommonsDataOperationService<PersonTransferInfo, PersonTransferInfoParameter>{


}
