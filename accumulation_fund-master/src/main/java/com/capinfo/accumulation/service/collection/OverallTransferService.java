package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonTransferInfo;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.PersonTransferInfoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;

/**
 * Created by wangguigui on 2017/10/31.
 */
public interface OverallTransferService extends CommonsDataOperationService<CompanyAccountFund, CompanyAccountFundParameter> {
    //查询转出单位
    public List<CompanyAccountFund> QueryTurnOutCompanyAccount(CompanyAccountFundParameter parameter);
    //查询转入单位
    public boolean QueryTurnInCompanyAccount(CompanyAccountFundParameter parameter);
}
