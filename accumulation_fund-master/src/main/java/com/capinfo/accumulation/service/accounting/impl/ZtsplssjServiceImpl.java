package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Ztsplssj;
import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.parameter.accounting.ZtsplssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.ZtsplssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class ZtsplssjServiceImpl extends CommonsDataOperationServiceImpl<Ztsplssj, ZtsplssjParameter> implements ZtsplssjService {
	 private static final Log logger = LogFactory.getLog(RegionService.class);
	 {
  }
		@Override
		public SearchCriteriaBuilder<Ztsplssj> getSearchCriteriaBuilder(ZtsplssjParameter parameter) {
			SearchCriteriaBuilder<Ztsplssj> searchCriteriaBuilder = new SearchCriteriaBuilder<Ztsplssj>(Ztsplssj.class);
			searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
			searchCriteriaBuilder.addQueryCondition("ztls", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZtls());

			return searchCriteriaBuilder;
		}
}
