package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.PersonAccountFundParameter;
import com.capinfo.accumulation.service.collection.PersonAccountFundService;
import com.capinfo.accumulation.service.collection.PersonHistoryAccountFundService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.List;

import static org.springframework.beans.BeanUtils.copyProperties;

/**
 * Created by wangyulin on 2017/9/27.
 */
public class PersonAccountServiceImpl extends CommonsDataOperationServiceImpl<PersonAccountFund, PersonAccountFundParameter> implements PersonAccountFundService {

    private static final Log logger = LogFactory.getLog(RegionService.class);

    @Autowired
    private PersonHistoryAccountFundService historyService;

    /**
     * 根据传递的参数判断业务
     *
     * @param parameter
     * @return
     */
    @Override
    public SearchCriteriaBuilder<PersonAccountFund> getSearchCriteriaBuilder(PersonAccountFundParameter parameter) {
        SearchCriteriaBuilder<PersonAccountFund> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);

        String deposit = parameter.getDeposit();    //封存
        String extra = parameter.getExtra();    //补充

        // TODO: 2017/10/25 判断是否封存
//        if(deposit==deposit){
//            searchCriteriaBuilder.addQueryCondition("creditCode", RestrictionExpression.EQUALS_OP, "封存");
//        }
//        if(extra==extra){
//            searchCriteriaBuilder.addQueryCondition("creditCode", RestrictionExpression.EQUALS_OP, "补充");
//        }

        //拼接查询条件
        if (parameter.getStartOpenedDate() != null && parameter.getEntityPerson() != null) {
            String strStartDate = (new SimpleDateFormat("yyyy-MM-dd")).format(parameter.getStartOpenedDate());
            String strEndDate = (new SimpleDateFormat("yyyy-MM-dd")).format(parameter.getEndOpenedDate());
            searchCriteriaBuilder.addQueryCondition("openedDate", RestrictionExpression.GREATER_EQUALS_OP, strStartDate);
            searchCriteriaBuilder.addQueryCondition("openedDate", RestrictionExpression.LESS_EQUALS_OP, strEndDate);
        }

        if (parameter.getEntityPerson() != null) {
            searchCriteriaBuilder.addQueryCondition("person.personalAccount", RestrictionExpression.EQUALS_OP, parameter.getEntityPerson().getPersonalAccount());
            searchCriteriaBuilder.addQueryCondition("person.idCardNumber", RestrictionExpression.EQUALS_OP, parameter.getEntityPerson().getIdCardNumber());
            searchCriteriaBuilder.addQueryCondition("person.name", RestrictionExpression.LIKE_OP, parameter.getEntityPerson().getName());

            if (parameter.getEntityPerson().getCompany() != null) {
                searchCriteriaBuilder.addQueryCondition("person.company.gbCompanyAccount", RestrictionExpression.EQUALS_OP, parameter.getEntityPerson().getCompany().getGbCompanyAccount());
                searchCriteriaBuilder.addQueryCondition("person.company.gbCompanyName", RestrictionExpression.LIKE_OP, parameter.getEntityPerson().getCompany().getGbCompanyName());
            }
        }

        if (parameter.getEntity().getOperatorId() != null) {
            searchCriteriaBuilder.addQueryCondition("operatorId", RestrictionExpression.EQUALS_OP, parameter.getEntity().getOperatorId());
        }

        return searchCriteriaBuilder;
    }

    @Override
    public Company getDepositPerson() {
        // TODO: 2017/10/25  获取封管办的单位信息 提交
        return null;
    }

    @Override
    public List<PersonAccountFund> queryByCompanyAccountId(Long companyAccountFundId) {
        SearchCriteriaBuilder<PersonAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<PersonAccountFund>(PersonAccountFund.class);
        searchCriteriaBuilder.addQueryCondition("companyAccountFundId", RestrictionExpression.EQUALS_OP, companyAccountFundId);
        searchCriteriaBuilder.addQueryCondition("isPayment", RestrictionExpression.EQUALS_OP, 1L);
        List<PersonAccountFund> personAccountFunds = getGeneralService().getObjects(searchCriteriaBuilder.build());

        return personAccountFunds;
    }

    @Override
    public List<PersonAccountFund> getPersonAccountByCompanyId(Long companyAccountFundId) {
        SearchCriteriaBuilder<PersonAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<PersonAccountFund>(PersonAccountFund.class);
        searchCriteriaBuilder.addQueryCondition("companyAccountFundId", RestrictionExpression.EQUALS_OP, companyAccountFundId);
        searchCriteriaBuilder.addQueryCondition("state", RestrictionExpression.EQUALS_OP, 0L);
        List<PersonAccountFund> personAccountFunds = getGeneralService().getObjects(searchCriteriaBuilder.build());

        return personAccountFunds;
    }

    @Override
    public PersonAccountFund getPersonAccountFund(PersonAccountFundParameter parameter) {
        String extra = parameter.getExtra();
        SearchCriteriaBuilder<PersonAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<PersonAccountFund>(PersonAccountFund.class);
        //个人公积金
        searchCriteriaBuilder.addQueryCondition("personalAccount", RestrictionExpression.EQUALS_OP, parameter.getEntityPerson().getPersonalAccount());
        searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
        long count = getGeneralService().getCount(searchCriteriaBuilder.build());
        if (count > 0) {
            List<PersonAccountFund> objects = getGeneralService().getObjects(searchCriteriaBuilder.build());
            if (count > 1) {
                if (extra != null && "extra".equals(extra)) {
                    //获取补充公积金
                    return objects.get(0);
                } else {
                    return objects.get(1);
                }
            } else {
                return objects.get(0);
            }
        } else {
            return null;
        }
    }

    public List<PersonAccountFund> getPersonAccountBycri(SearchCriteriaBuilder criteriaBuilder) {
        List objects = getGeneralService().getObjects(criteriaBuilder.build());
        return objects;
    }


    @Override
    public PersonAccountFund audit(PersonAccountFundParameter parameter) {
        String extra = parameter.getExtra();    //开户类型

        PersonAccountFund personAccountFund = parameter.getEntity();
        PersonAccountFund personAccount = new PersonAccountFund();

        if (personAccountFund.getId() != null) {
            /**
             * 根据历史id获取历史信息并赋值到新的personAccountl里面
             */
            PersonHistoryAccountFund historyAccount = getGeneralService().getObjectById(PersonHistoryAccountFund.class, personAccountFund.getId());
            getGeneralService().fetchLazyProperty(historyAccount, "personHistory");
            copyProperties(historyAccount, personAccount,
                    new String[]{"id", "personAccountFundId"});

            if (!"extra".equals(extra)) {     //个人基本开户
                PersonHistory personHistory = historyAccount.getPersonHistory();
                Person person = new Person();
                BeanUtils.copyProperties(personHistory, person,
                        new String[]{"id", "nameAllPinYin", "gbmaritalStatus", "company", "maritalStatus", "postCode", "gbeducation", "educationId", "education", "homeAddress", "fixedTelephoneNumber", "mobilePhone", "gbprofession", "profession", "gbtitle", "title", "gbpositionalTitle", "positionalTitle", "homeMonthlyIncome", "isFreeze", "loanState", "timeControl", "reserve", "newFlag", "customerNumber", "societyCardNo", "manageState", "companyPaymentRatio", "personPaymentRatio", "bankCode", "regionId",});

                //个人信息的单位账号 为个人基本的公积金账号
                person.setPersonalAccount(personAccount.getAccountNumber());
                //将历史id放入正式表的historyid
                person.setHistoryId(personHistory.getId());
                personAccount.setPersonHistoryAccountFundId(historyAccount.getId());
                personAccount.getPersonHistoryAccountFundList().add(historyAccount);
                person.getPersonAccountFundList().add(personAccount);
                person.getPersonHistorieList().add(personHistory);
                getGeneralService().persist(person);
            } else {
                personAccount.setPersonHistoryAccountFundId(personAccountFund.getId());
                //保存到历史id
                historyAccount.setPersonId(historyAccount.getPerson().getId());
                getGeneralService().update(historyAccount);
                getGeneralService().persist(personAccount);

            }
        }
        return personAccount;
    }


    @Override
    public List<PersonAccountFund> getList(PersonAccountFundParameter parameter, long dataTotalCount, int currentPieceNum) {


        return super.getList(parameter, dataTotalCount, currentPieceNum);
    }
}