package com.capinfo.accumulation.service.collection;


import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.parameter.collection.PersonHistoryParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * Created by Rexxxar on 2017/9/28.
 */
public interface PersonHistoryService extends CommonsDataOperationService<PersonHistory,PersonHistoryParameter>{



}
