package com.capinfo.accumulation.service.accounting;


import java.util.List;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


public interface ZtszService extends CommonsDataOperationService<Ztsz, ZtszParameter> {
	
	/**
	 * 获得账套信息
	 * @return
	 */
	List<Ztsz> getZtsz();
	
}
