package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.model.collection.PersonHistoryAccountFund;
import com.capinfo.accumulation.parameter.collection.PersonHistoryAccountFundParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by zhangxu on 2017/10/24.
 */
public interface PersonHistoryAccountFundService extends CommonsDataOperationService<PersonHistoryAccountFund, PersonHistoryAccountFundParameter> {

    public PersonHistory changeSaveOrUpdate(PersonHistoryAccountFundParameter parameter);

    public Person changeAudit(PersonHistoryAccountFundParameter parameter);

    public PersonHistoryAccountFund  saveOrUpdate(PersonHistoryAccountFundParameter parameter);
}
