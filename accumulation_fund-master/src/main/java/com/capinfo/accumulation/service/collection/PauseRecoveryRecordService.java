package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.PauseRecoveryRecordParameter;
import com.capinfo.framework.service.CommonsDataOperationService;
import net.sf.json.JSONObject;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by Rexxxar on 2017/10/19.
 */
public interface PauseRecoveryRecordService extends CommonsDataOperationService <PauseRecoveryRecord,PauseRecoveryRecordParameter>{


    /**
     *停缴处理删除
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/24 10:44
     */
    public boolean delete(PauseRecoveryRecordParameter parameter);


    boolean recoveryCheckAccount(String companyAccountFundId);

    /**
     * 根据查询下拉框字典
     * 将数据库字典的sort_id出来，存进list中
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/26 21:51
     */
    List optionFund(Long parameter);

    /**
     *将数据录入到数据库
     *
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/27 17:38
     */


    PauseRecoveryRecord saveOrUpdateRecord(PauseRecoveryRecordParameter PauseRecoveryRecordParameter);

    /**
     * 创建清册表
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/10/30 21:38
     */
    public boolean createInventory(PauseRecoveryRecordParameter pauseRecoveryRecordParameter);


    /**启封校验规则之
     * 校验月缴存额
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/11/1 11:28
     */
    JSONObject recoveryCheckratio(BigDecimal wageIncome, String companyAccount, String idCardNumber);



    /**
     * 点击录入，显示单位账号和单位名称
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/11/2 12:57
     */
    CompanyAccountFund searchCompanyAccountAndName(String pauseRecoveryInventoryaccount);


    /**验证个人是否可以停复缴
     *@Author: Rexxxar
     *@Description
     *@Date: 2017/11/2 20:21
     */
    JSONObject recoveryCheckPeople(String account,String idCardNumber, Long payment);
}
