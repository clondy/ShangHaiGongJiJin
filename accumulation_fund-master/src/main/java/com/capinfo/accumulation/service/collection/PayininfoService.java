package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;

/**
 * Created by wcj on 2017/10/24.
 */
public interface PayininfoService extends CommonsDataOperationService<Payininfo, PayininfoParameter> {

      public CompanyAccountFund getCompanyAccount(PayininfoParameter parameter);

      //创建汇缴核定清单
      public boolean buildPayininfoQc(PayininfoParameter parameter,String operator);

      public List<Payininfo> searchPayinfouncheck(PayininfoParameter parameter);

      public boolean confirm(PayininfoParameter parameter);

      public boolean cancel(PayininfoParameter parameter);
}
