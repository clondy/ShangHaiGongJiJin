package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Kmyeb;
import com.capinfo.accumulation.parameter.accounting.KmyebParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface KmyebService extends CommonsDataOperationService<Kmyeb, KmyebParameter> {

}
