package com.capinfo.accumulation.service.collection;

/**
 * Created by xiaopeng on 2017/9/25.
 *  单位服务接口类
 * @author xiaopeng
 */
public interface PersonUtilService {
    //生成个人账号
    //isBase是true时，生成基本公积金代码
    //isBase是false时，生成补充公积金代码
    public String generateBasePersonCode();
    public String generateSupplyPersonCode(String person);
    //获取法人信息，没有时返回空Company对象
    //参数code长度必须9位以上（9位是组织机构代码、18位统一社会信用代码）
//    public Company getLegalPersonInfo(String code);

}
