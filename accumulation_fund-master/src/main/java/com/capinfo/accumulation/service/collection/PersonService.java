package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.parameter.collection.PersonParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by zhangxu on 2017/10/24.
 */
public interface PersonService extends CommonsDataOperationService<Person, PersonParameter> {

    Company getDepositPerson();

    Person getPerson(PersonParameter parameter);

}
