package com.capinfo.accumulation.service.accounting.impl;
import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.accumulation.parameter.accounting.KmszxgParameter;
import com.capinfo.accumulation.parameter.collection.CompanyHistoryParameter;
import com.capinfo.accumulation.service.accounting.KmszxgService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class KmszxgServiceImpl extends  CommonsDataOperationServiceImpl<Kmszxglssj,KmszxgParameter> implements KmszxgService {

	
	public List<Kmszxglssj> dantiaochaxun(KmszxgParameter parameter) {
		SearchCriteriaBuilder<Kmszxglssj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszxglssj>(Kmszxglssj.class);
		searchCriteriaBuilder.addQueryCondition("kmszid", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmszid());
		List<Kmszxglssj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	
	
	    @Override
	    public SearchCriteriaBuilder<Kmszxglssj> getSearchCriteriaBuilder(KmszxgParameter parameter) {
	        SearchCriteriaBuilder<Kmszxglssj> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
	        searchCriteriaBuilder.addQueryCondition("kmszid", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmszid());
	        return searchCriteriaBuilder;
	    }
}

