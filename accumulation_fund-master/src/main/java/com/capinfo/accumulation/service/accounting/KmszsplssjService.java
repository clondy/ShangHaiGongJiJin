package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszsplssj;
import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.accumulation.parameter.accounting.KmszsplssjParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface KmszsplssjService  extends CommonsDataOperationService<Kmszsplssj,KmszsplssjParameter> {

	
	List<Kmszsplssj> dantiaochaxun(KmszsplssjParameter parameter);

}
