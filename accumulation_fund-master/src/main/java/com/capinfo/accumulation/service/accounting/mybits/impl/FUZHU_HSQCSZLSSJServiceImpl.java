package com.capinfo.accumulation.service.accounting.mybits.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capinfo.accumulation.dao. FUZHU_HSQCSZLSSJDao;
import com.capinfo.accumulation.model.accounting. FUZHU_HSQCSZLSSJ;
import com.capinfo.accumulation.parameter.accounting.DataGrid;
import com.capinfo.accumulation.service.accounting.mybits.FUZHU_HSQCSZLSSJServiceI;
@Service("fUZHU_HSQCSZLSSJService")
public class  FUZHU_HSQCSZLSSJServiceImpl implements  FUZHU_HSQCSZLSSJServiceI {
	
	@Autowired
	 FUZHU_HSQCSZLSSJDao  fUZHU_HSQCSZLSSJDao;
	@Override
	public int deleteByPrimaryKey(String id) {
		return this.fUZHU_HSQCSZLSSJDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(FUZHU_HSQCSZLSSJ record) {
		return this.fUZHU_HSQCSZLSSJDao.insertSelective(record);
	}

	@Override
	public FUZHU_HSQCSZLSSJ selectByPrimaryKey(String id) {
		return this.fUZHU_HSQCSZLSSJDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(FUZHU_HSQCSZLSSJ record) {
		return this.fUZHU_HSQCSZLSSJDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public DataGrid findbyByGrid(Map map) {
		DataGrid dataGrid = new DataGrid();
		map.put("dataGrid", dataGrid);
		List<FUZHU_HSQCSZLSSJ> retList = this.fUZHU_HSQCSZLSSJDao.findbyByGrid(map);
		dataGrid.setRows(retList);
		return dataGrid;
	}


	@Override
	public DataGrid aaa(Map map) {
		DataGrid dataGrid = new DataGrid();
		map.put("dataGrid", dataGrid);
		List retList = this.fUZHU_HSQCSZLSSJDao.aaa(map);
		dataGrid.setRows(retList);
		return dataGrid;
	}

	@Override
	public DataGrid findBySelect(Map map) {
		DataGrid dataGrid = new DataGrid();
		map.put("dataGrid", dataGrid);
		List retList = this.fUZHU_HSQCSZLSSJDao.findBySelect(map);
		dataGrid.setRows(retList);
		return dataGrid;
	}

		
}
