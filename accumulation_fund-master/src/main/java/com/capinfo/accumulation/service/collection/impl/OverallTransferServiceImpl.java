package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.*;
import com.capinfo.accumulation.parameter.collection.CompanyAccountFundParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.parameter.collection.PersonTransferInfoParameter;
import com.capinfo.accumulation.service.collection.OverallTransferService;
import com.capinfo.framework.dao.SearchCriteria;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

import java.util.List;

/**
 * Created by wangguigui on 2017/10/31.
 */
public class OverallTransferServiceImpl extends CommonsDataOperationServiceImpl<CompanyAccountFund, CompanyAccountFundParameter> implements OverallTransferService {

    @Override
    public List<CompanyAccountFund> QueryTurnOutCompanyAccount(CompanyAccountFundParameter parameter){

        SearchCriteriaBuilder<CompanyAccountFund> searchCriteriaBuilder = new SearchCriteriaBuilder<CompanyAccountFund>(CompanyAccountFund.class);
        //根据单位账号查询 单位公积金表 返回全部信息
        searchCriteriaBuilder.addQueryCondition("turnOutCompanyAccount", RestrictionExpression.EQUALS_OP, parameter.getEntity().getCompanyAccount());
        List<CompanyAccountFund> companyAccountFunds = getGeneralService().getObjects(searchCriteriaBuilder.build());
        return companyAccountFunds;
    }


    @Override
    public boolean QueryTurnInCompanyAccount(CompanyAccountFundParameter parameter){
        SearchCriteriaBuilder<Company> searchCriteriaBuilder = new SearchCriteriaBuilder<Company>(Company.class);
        searchCriteriaBuilder.addQueryCondition("turnInCompanyAccount", RestrictionExpression.EQUALS_OP, parameter.getEntity().getCompanyAccount());
        return true;
    }

}
