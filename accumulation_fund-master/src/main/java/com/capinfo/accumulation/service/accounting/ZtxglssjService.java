package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface ZtxglssjService extends CommonsDataOperationService<Ztxglssj, ZtxglssjParameter>{
	public List<Ztxglssj> getList(ZtxglssjParameter parameter, long dataTotalCount, int currentPieceNum);


}
