package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface VoucherjzService   extends CommonsDataOperationService<Voucher, VoucherParameter> {
	List <Voucher> jzpzcx(VoucherParameter parameter);
}

