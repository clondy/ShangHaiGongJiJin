package com.capinfo.accumulation.service.accounting.impl;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.accumulation.parameter.accounting.ZtxglssjParameter;
import com.capinfo.accumulation.service.accounting.ZtxglssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class ZtxglssjServiceImpl extends CommonsDataOperationServiceImpl<Ztxglssj,ZtxglssjParameter> implements ZtxglssjService {
	 private static final Log logger = LogFactory.getLog(RegionService.class);{
	 }
	
		@Override
		public SearchCriteriaBuilder<Ztxglssj> getSearchCriteriaBuilder(ZtxglssjParameter parameter) {
			SearchCriteriaBuilder<Ztxglssj> searchCriteriaBuilder = new SearchCriteriaBuilder<Ztxglssj>(Ztxglssj.class);
			searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
			searchCriteriaBuilder.addQueryCondition("ztid", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZtid());

			return searchCriteriaBuilder;
		}
		
 }

