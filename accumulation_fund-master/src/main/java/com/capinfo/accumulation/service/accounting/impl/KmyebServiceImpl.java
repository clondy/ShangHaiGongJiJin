package com.capinfo.accumulation.service.accounting.impl;

import com.capinfo.accumulation.model.accounting.Kmyeb;
import com.capinfo.accumulation.parameter.accounting.KmyebParameter;
import com.capinfo.accumulation.service.accounting.KmyebService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class KmyebServiceImpl extends CommonsDataOperationServiceImpl<Kmyeb, KmyebParameter> implements KmyebService {

	//页面输入框条件查询
	@Override
	public SearchCriteriaBuilder<Kmyeb> getSearchCriteriaBuilder(KmyebParameter parameter) {
		SearchCriteriaBuilder<Kmyeb> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmyeb>(Kmyeb.class);
		if(parameter.getXsfs()!=null && !parameter.getXsfs().equals("")){
			searchCriteriaBuilder.addQueryCondition("kmbh.kmjb", RestrictionExpression.EQUALS_OP, Integer.parseInt(parameter.getXsfs()));
		}
		searchCriteriaBuilder.addQueryCondition("kmbh.kmbh", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStartKm());
		searchCriteriaBuilder.addQueryCondition("kmbh.kmbh", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndKm());
		searchCriteriaBuilder.addQueryCondition("inserttime", RestrictionExpression.GREATER_EQUALS_OP, parameter.getStarttime());
		searchCriteriaBuilder.addQueryCondition("inserttime", RestrictionExpression.LESS_EQUALS_OP, parameter.getEndtime());
		return searchCriteriaBuilder;
	}
}
