package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Zhaiyao;
import com.capinfo.accumulation.parameter.accounting.ZhaiyaoParameter;
import com.capinfo.accumulation.service.accounting.ZhaiyaoService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class ZhaiyaoServiceImpl extends CommonsDataOperationServiceImpl<Zhaiyao, ZhaiyaoParameter> implements ZhaiyaoService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	@Override
	public SearchCriteriaBuilder<Zhaiyao> getSearchCriteriaBuilder(ZhaiyaoParameter parameter) {
		SearchCriteriaBuilder<Zhaiyao> searchCriteriaBuilder = new SearchCriteriaBuilder<Zhaiyao>(Zhaiyao.class);
		searchCriteriaBuilder.addQueryCondition("ztbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZtbh());
		searchCriteriaBuilder.addQueryCondition("hsdw", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsdw());
		return searchCriteriaBuilder;
	}
	
}
