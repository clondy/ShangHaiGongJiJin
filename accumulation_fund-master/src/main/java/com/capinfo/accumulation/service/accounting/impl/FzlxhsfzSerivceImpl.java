package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzlxhsfz;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.parameter.accounting.FzlxhsfzParameter;
import com.capinfo.accumulation.parameter.accounting.FzlxszParameter;
import com.capinfo.accumulation.service.accounting.FzlxhsfzService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzlxhsfzSerivceImpl extends CommonsDataOperationServiceImpl<Fzlxhsfz, FzlxhsfzParameter> implements FzlxhsfzService{
	 private static final Log logger = LogFactory.getLog(RegionService.class);
	 
	  /**
	   * 辅助核算类型分组全部查询
	   * @param parameter
	   * @return
	   */
	  public List<Fzlxhsfz> findAllFzlxhsfz() {
			SearchCriteriaBuilder<Fzlxhsfz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxhsfz>(Fzlxhsfz.class);
			//重写了默认方法下的方法
			List<Fzlxhsfz> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
			return entities;
		}
		
}
