package com.capinfo.accumulation.service.accounting.impl;

import com.capinfo.accumulation.model.accounting.CwhscsSpls;
import com.capinfo.accumulation.parameter.accounting.CwhscsSplsParameter;
import com.capinfo.accumulation.service.accounting.CwhscsSplsService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
/**
 * 财务核算审批流水service实现
 *
 */
public class CwhscsSplsServiceImpl  extends CommonsDataOperationServiceImpl<CwhscsSpls, CwhscsSplsParameter> implements CwhscsSplsService {
	
}
