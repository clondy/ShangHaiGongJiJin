package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Fzlxhsfz;
import com.capinfo.accumulation.parameter.accounting.FzlxhsfzParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface FzlxhsfzService extends CommonsDataOperationService<Fzlxhsfz, FzlxhsfzParameter>{
	
	//查找的方法
	  public List<Fzlxhsfz> findAllFzlxhsfz();

}
