package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.parameter.accounting.FzlxszParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface FzlxszService extends CommonsDataOperationService<Fzlxsz, FzlxszParameter> {

	List<Fzlxsz> getFzlxsz();
	
	
}
