package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Fuzhuzhang;
import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.parameter.accounting.FuzhuzhangParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface FuzhuzhangService extends CommonsDataOperationService<Fuzhuzhang, FuzhuzhangParameter>{
	public List<Kmszjbsj> findKmmc();
	public List<Fzlxsz> findFzlb();
	public List<Fzhsxmjbsj> findHsxmmc();
}
