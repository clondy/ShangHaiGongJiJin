package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Kemuhzb;
import com.capinfo.accumulation.parameter.accounting.KemuhzbParameter;
import com.capinfo.accumulation.service.accounting.KemuhzbService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class KemuhzbServiceImpl extends CommonsDataOperationServiceImpl<Kemuhzb, KemuhzbParameter> implements KemuhzbService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	@Override
	public SearchCriteriaBuilder<Kemuhzb> getSearchCriteriaBuilder(KemuhzbParameter parameter) {
		SearchCriteriaBuilder<Kemuhzb> searchCriteriaBuilder = new SearchCriteriaBuilder<Kemuhzb>(Kemuhzb.class);
	
		searchCriteriaBuilder.addQueryCondition("riqi", RestrictionExpression.EQUALS_OP, parameter.getTjny());
			
		return searchCriteriaBuilder;
	}

}
