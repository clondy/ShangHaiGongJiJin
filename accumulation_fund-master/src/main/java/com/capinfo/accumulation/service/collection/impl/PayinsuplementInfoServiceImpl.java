package com.capinfo.accumulation.service.collection.impl;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PayinSupplementInfo;
import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.parameter.collection.PayinsuplementInfoParameter;
import com.capinfo.accumulation.service.collection.CompanyAccountFundService;
import com.capinfo.accumulation.service.collection.PayinsuplementInfoService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by wcj on 2017/10/31.
 */
public class PayinsuplementInfoServiceImpl extends CommonsDataOperationServiceImpl<PayinSupplementInfo, PayinsuplementInfoParameter> implements PayinsuplementInfoService{

    @Autowired
    private CompanyAccountFundService companyAccountFundService;


    //创建补缴带核定清册
    @Override
    public PayinSupplementInfo buildPayinSupplementInfoQc(PayinsuplementInfoParameter parameter, String operator,Long operateId) {
        List<CompanyAccountFund> companyAccountByCretier = companyAccountFundService.getCompanyAccountByCretier(parameter.getCompanyAccot(), parameter.getEntity().getPaymentOptions());
        if(!companyAccountByCretier.isEmpty()){
            PayinSupplementInfo payinSupplementInfo = new PayinSupplementInfo();
            payinSupplementInfo.setCompanyAccountFundId(companyAccountByCretier.get(0).getId());
            payinSupplementInfo.setPaymentOptions(companyAccountByCretier.get(0).getAccumulationFundAccountTypeId());
            payinSupplementInfo.setStartTime(parameter.getEntity().getStartTime());
            payinSupplementInfo.setEndTime(parameter.getEntity().getEndTime());
            payinSupplementInfo.setPaymentReason(parameter.getEntity().getPaymentReason());
            payinSupplementInfo.setSiteId(parameter.getEntity().getSiteId());
            payinSupplementInfo.setDateTime(new Date());
            payinSupplementInfo.setCreateName(operator);
            payinSupplementInfo.setOperatorId(operateId);
            return  this.saveOrUpdate(payinSupplementInfo);

        }
        return null;
    }

    @Override
    public SearchCriteriaBuilder<PayinSupplementInfo> getSearchCriteriaBuilder(PayinsuplementInfoParameter parameter) {
        SearchCriteriaBuilder<PayinSupplementInfo> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
        searchCriteriaBuilder.addQueryCondition("siteId", RestrictionExpression.EQUALS_OP,parameter.getEntity().getSiteId());
        if(parameter.getEntity().getCompanyAccountFund()!=null){
            searchCriteriaBuilder.addQueryCondition("companyAccountFund.companyAccount",RestrictionExpression.EQUALS_OP,parameter.getEntity().getCompanyAccountFund().getCompanyAccount());
            searchCriteriaBuilder.addQueryCondition("companyAccountFund.unitName", RestrictionExpression.LIKE_OP, parameter.getEntity().getCompanyAccountFund().getUnitName());
        }


        searchCriteriaBuilder.addQueryCondition("paymentOptions",RestrictionExpression.EQUALS_OP,parameter.getEntity().getPaymentOptions());

        if(parameter.getStartReceivedDate()!=null) {
            String sformat = new SimpleDateFormat("yyyy-MM-dd").format(parameter.getStartReceivedDate());
            searchCriteriaBuilder.addQueryCondition("receivedDate", RestrictionExpression.GREATER_EQUALS_OP, sformat);
        }
        if(parameter.getEndReceivedDate()!=null) {
            String eformat = new SimpleDateFormat("yyyy-MM-dd").format(parameter.getEndReceivedDate());
            searchCriteriaBuilder.addQueryCondition("receivedDate", RestrictionExpression.LESS_EQUALS_OP, eformat);
        }
        return searchCriteriaBuilder;
    }
}
