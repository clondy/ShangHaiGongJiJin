package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzlxsz;
import com.capinfo.accumulation.parameter.accounting.FzlxszParameter;
import com.capinfo.accumulation.service.accounting.FzlxszService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzlxszServiceImpl  extends CommonsDataOperationServiceImpl<Fzlxsz, FzlxszParameter> implements FzlxszService{
	 private static final Log logger = LogFactory.getLog(RegionService.class);
	 //模糊查询的方法
	@Override
	public SearchCriteriaBuilder<Fzlxsz> getSearchCriteriaBuilder(FzlxszParameter parameter) {
		SearchCriteriaBuilder<Fzlxsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxsz>(Fzlxsz.class);
		searchCriteriaBuilder.addQueryCondition("hslxbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHslxbm());
		searchCriteriaBuilder.addQueryCondition("hslxmc", RestrictionExpression.LIKE_OP, parameter.getEntity().getHslxmc());
		searchCriteriaBuilder.addQueryCondition("hsfl", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsfl());
		searchCriteriaBuilder.addQueryCondition("sjzt", RestrictionExpression.EQUALS_OP, parameter.getEntity().getSjzt());
		//如果等于Null 主界面会直接不现实
		if(parameter.getEntity().getHsdw() != null){
			searchCriteriaBuilder.addQueryCondition("hsdw", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsdw().getId());
		}
		return searchCriteriaBuilder;
	}
	@Override
	public List<Fzlxsz> getFzlxsz() {
		SearchCriteriaBuilder<Fzlxsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzlxsz>(Fzlxsz.class);
		List<Fzlxsz> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;		
		
	}

	
	
}
