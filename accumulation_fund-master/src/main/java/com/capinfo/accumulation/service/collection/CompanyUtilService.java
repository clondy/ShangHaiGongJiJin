package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;

/**
 * Created by xiaopeng on 2017/9/25.
 *  单位服务接口类
 * @author xiaopeng
 */
public interface CompanyUtilService {
    //生成单位代码
    //isBase是true时，生成基本公积金代码
    //isBase是false时，生成补充公积金代码
    public String generateBaseCompanyCode();
    public String generateSupplyCompanyCode(Company company);
    //获取法人信息，没有时返回空Company对象
    //参数code长度必须9位以上（9位是组织机构代码、18位统一社会信用代码）
    public Company getLegalPersonInfo(String code);
/*    //工商判断企业是否合法的接口
    //与需求人员沟通，此接口与法人信息是一个接口
    public boolean isLegalFromIndustryCommerceBureau(Company company);*/
}
