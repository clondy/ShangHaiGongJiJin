package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Fzlxsplssj;
import com.capinfo.accumulation.parameter.accounting.FzlxsplssjParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface FzlxsplssjService extends CommonsDataOperationService<Fzlxsplssj, FzlxsplssjParameter>{

}
