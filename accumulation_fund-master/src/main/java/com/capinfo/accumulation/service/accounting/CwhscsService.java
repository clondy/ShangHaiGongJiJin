package com.capinfo.accumulation.service.accounting;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.accumulation.parameter.accounting.CwhscsParameter;
import com.capinfo.framework.service.CommonsDataOperationService;
/**
 * 财务核算参数设置Serviuce
 *
 */
public interface CwhscsService  extends CommonsDataOperationService<Cwhscs, CwhscsParameter>{

}
