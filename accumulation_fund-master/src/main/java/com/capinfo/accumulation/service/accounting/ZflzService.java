package com.capinfo.accumulation.service.accounting;

import java.util.List;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.accumulation.parameter.accounting.ZflzParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface ZflzService extends CommonsDataOperationService<Zflz, ZflzParameter>{
	public List<Kmszjbsj> findKmmc();
}
