package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

/**
 * Created by wangyulin on 2017/9/27.
 */
public interface CompanyService extends CommonsDataOperationService<Company, CompanyParameter> {
    /**
     * 是否存在
     * @param parameter  统一社会信用代码/组织机构代码
     * @return 是否存在
     */
    public Company isExist(CompanyParameter parameter);

    /**
     * 审核提交
     * @param parameter  从company对象的id作为参数
     * @return 提交成功返回结果
     */
    public boolean audit(CompanyParameter parameter);


    /**
     * 已知ID获取单位
     * @param strID
     * @return
     */
    public Company getCompanyById(Long strID);

    /**
     * 根据parameter获取单位
     * @param parameter
     * @return
     */
    public Company getCompany(CompanyParameter parameter);

    Company saveChangeSiteCompany(CompanyParameter companyParameter);

    Company basicInfo(CompanyParameter companyParameter);

    Company saveCompanyBasicInfo(CompanyParameter companyParameter);
}
