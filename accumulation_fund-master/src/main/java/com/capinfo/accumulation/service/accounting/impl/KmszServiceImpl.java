package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.parameter.accounting.KmszParameter;
import com.capinfo.accumulation.parameter.accounting.VoucherParameter;
import com.capinfo.accumulation.parameter.collection.CompanyParameter;
import com.capinfo.accumulation.service.accounting.KmszService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteria.OrderRow;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

import java.util.List;

import org.apache.commons.logging.Log;

public class KmszServiceImpl extends  CommonsDataOperationServiceImpl<Kmszjbsj, KmszParameter> implements KmszService {
	private static final Log logger = LogFactory.getLog(KmszServiceImpl.class);
	
    public SearchCriteriaBuilder<Kmszjbsj> getSearchCriteriaBuilder(KmszParameter parameter) {
        SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
        searchCriteriaBuilder.addQueryCondition("kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmbh());
        searchCriteriaBuilder.addQueryCondition("kmsx", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmsx());
        searchCriteriaBuilder.addQueryCondition("rjzlx", RestrictionExpression.EQUALS_OP, parameter.getEntity().getRjzlx());
        searchCriteriaBuilder.addQueryCondition("tzbj", RestrictionExpression.EQUALS_OP, parameter.getEntity().getTzbj());
        searchCriteriaBuilder.addQueryCondition("kjnd", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKjnd());
        searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
        searchCriteriaBuilder.addOrderCondition("spzt", OrderRow.ORDER_ASC);
        return searchCriteriaBuilder;
       
        
    }
	@Override
	public List<Kmszjbsj> dantiaochaxun(KmszParameter parameter) {
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}
	@Override
	public String yzkmbh(KmszParameter parameter) {
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		searchCriteriaBuilder.addQueryCondition("kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmbh());
		searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.NOT_EQUALS_OP, parameter.getEntity().getId());
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		if(entities == null || entities.size() == 0){
			return "true";
		}else{
			return "false";
		}
	}
}

