package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.accumulation.parameter.accounting.CwhscsParameter;
import com.capinfo.accumulation.service.accounting.CwhscsService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;
/**
 * 财务核算参数设置实现类
 *
 */
public class CwhscsServiceImpl  extends CommonsDataOperationServiceImpl<Cwhscs, CwhscsParameter> implements CwhscsService {

	private static final Log logger = LogFactory.getLog(CwhscsServiceImpl.class);
	
	public SearchCriteriaBuilder<Cwhscs> getSearchCriteriaBuilder(CwhscsParameter parameter) {
        SearchCriteriaBuilder<Cwhscs> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
        searchCriteriaBuilder.addQueryCondition("cwnd", RestrictionExpression.EQUALS_OP,parameter.getEntity().getCwnd());
        return searchCriteriaBuilder;
       
        
    }

}
