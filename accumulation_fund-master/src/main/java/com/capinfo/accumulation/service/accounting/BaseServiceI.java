package com.capinfo.accumulation.service.accounting;

import java.util.Map;

import com.capinfo.accumulation.parameter.accounting.DataGrid;

public interface BaseServiceI<T> {
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(String id);
	/**
	 * 新增
	 * @param record
	 * @return
	 */
    int insert(T record);
    /**
     *  插入
     * @param id
     * @return
     */
    T selectByPrimaryKey(String id);
    /**
     * 更新
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(T record);
    
	/**
	 * 查询合同表格
	 * @param map
	 * @return
	 */
       DataGrid  findbyByGrid(Map map);
}
