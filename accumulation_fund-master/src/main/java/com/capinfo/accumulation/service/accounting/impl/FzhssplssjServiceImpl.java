package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzhssplssj;
import com.capinfo.accumulation.parameter.accounting.FzhssplssjParameter;
import com.capinfo.accumulation.service.accounting.FzhssplssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzhssplssjServiceImpl extends CommonsDataOperationServiceImpl<Fzhssplssj,FzhssplssjParameter> implements FzhssplssjService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
}
