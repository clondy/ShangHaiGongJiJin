package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.accumulation.model.collection.Payinitem;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.accumulation.parameter.collection.PayininfoParameter;
import com.capinfo.accumulation.parameter.collection.PayinitemParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;

/**
 * Created by wcj on 2017/10/30.
 */
public interface PayinitemService extends CommonsDataOperationService<Payinitem, PayinitemParameter> {
    public Boolean saveOrUpdateBatch(List<PersonAccountFund> personAccountFunds, String companyAccount, Long payinfoId);
}
