package com.capinfo.accumulation.service.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.accumulation.parameter.collection.PersonAccountFundParameter;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.service.CommonsDataOperationService;

import java.util.List;

/**
 * Created by zhangxu on 2017/10/24.
 */
public interface PersonAccountFundService extends CommonsDataOperationService<PersonAccountFund, PersonAccountFundParameter> {
    Company getDepositPerson();

    //查询单位公积金下需要缴存的个人公积金
    public List<PersonAccountFund> queryByCompanyAccountId(Long companyAccountFundId);

    //查询个人公积金账户信息
    public PersonAccountFund getPersonAccountFund(PersonAccountFundParameter parameter);

    PersonAccountFund audit(PersonAccountFundParameter personAccountFundParameter);

    List<PersonAccountFund> getPersonAccountBycri(SearchCriteriaBuilder criteriaBuilder);
    //整体转移_根据单位id 查询出可以转出的个人信息
    public List<PersonAccountFund> getPersonAccountByCompanyId(Long companyAccountFundId);
}
