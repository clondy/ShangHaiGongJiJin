package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzlxhsfz;
import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.accumulation.parameter.accounting.ZflzParameter;
import com.capinfo.accumulation.service.accounting.ZflzService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class ZflzServiceImpl extends CommonsDataOperationServiceImpl<Zflz, ZflzParameter> implements ZflzService{

	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	@Override
	public SearchCriteriaBuilder<Zflz> getSearchCriteriaBuilder(ZflzParameter parameter) {
		SearchCriteriaBuilder<Zflz> searchCriteriaBuilder = new SearchCriteriaBuilder<Zflz>(Zflz.class);
		if(parameter.getEntity().getKmmc()!=null){
			searchCriteriaBuilder.addQueryCondition("kjnd", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKjnd());
			searchCriteriaBuilder.addQueryCondition("kmmc.kmbh", RestrictionExpression.EQUALS_OP, parameter.getEntity().getKmmc().getKmbh());
		}
		
		return searchCriteriaBuilder;
	}

	@Override
	public List<Kmszjbsj> findKmmc() {
		// TODO Auto-generated method stub
		SearchCriteriaBuilder<Kmszjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Kmszjbsj>(Kmszjbsj.class);
		List<Kmszjbsj> entities = getGeneralService().getObjects(searchCriteriaBuilder.build());
		return entities;
	}

	

}
