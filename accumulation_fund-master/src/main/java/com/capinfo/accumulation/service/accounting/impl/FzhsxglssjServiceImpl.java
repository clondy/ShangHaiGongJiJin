package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzhsxglssj;
import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.parameter.accounting.FzhsxglssjParameter;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.service.accounting.FzhsxglssjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzhsxglssjServiceImpl extends CommonsDataOperationServiceImpl<Fzhsxglssj,FzhsxglssjParameter> implements FzhsxglssjService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	
	@Override
	public SearchCriteriaBuilder<Fzhsxglssj> getSearchCriteriaBuilder(FzhsxglssjParameter parameter) {
		SearchCriteriaBuilder<Fzhsxglssj> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzhsxglssj>(Fzhsxglssj.class);
		searchCriteriaBuilder.addQueryCondition("id", RestrictionExpression.EQUALS_OP, parameter.getEntity().getId());
		searchCriteriaBuilder.addQueryCondition("hsid", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsid());

		return searchCriteriaBuilder;
	}
	
}
