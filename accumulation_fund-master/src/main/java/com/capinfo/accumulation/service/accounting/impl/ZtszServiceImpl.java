package com.capinfo.accumulation.service.accounting.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.accumulation.model.enums.SjztEnums;
import com.capinfo.accumulation.parameter.accounting.ZtszParameter;
import com.capinfo.accumulation.service.accounting.ZtszService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;



public class ZtszServiceImpl extends CommonsDataOperationServiceImpl<Ztsz, ZtszParameter> implements ZtszService {
	 private static final Log logger = LogFactory.getLog(RegionService.class);
 
	 
	@Override
	public SearchCriteriaBuilder<Ztsz> getSearchCriteriaBuilder(ZtszParameter parameter) {
		SearchCriteriaBuilder<Ztsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Ztsz>(Ztsz.class);
		searchCriteriaBuilder.addQueryCondition("ztmc", RestrictionExpression.LIKE_OP, parameter.getEntity().getZtmc());
		searchCriteriaBuilder.addQueryCondition("ztms", RestrictionExpression.LIKE_OP, parameter.getEntity().getZtms());
		searchCriteriaBuilder.addQueryCondition("ztdm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getZtdm());
		if(parameter.getEntity().getHsdw()!=null){
			searchCriteriaBuilder.addQueryCondition("hsdw", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHsdw().getId());
		}

		return searchCriteriaBuilder;
	}


	@Override
	public List<Ztsz> getZtsz() {
		SearchCriteriaBuilder<Ztsz> searchCriteriaBuilder = new SearchCriteriaBuilder<Ztsz>(Ztsz.class);
		//searchCriteriaBuilder.addQueryCondition("sjzt", RestrictionExpression.EQUALS_OP, SjztEnums.正常使用.getToken());
		return  this.getGeneralService().getObjects(searchCriteriaBuilder.build());
	}
	
 

}
