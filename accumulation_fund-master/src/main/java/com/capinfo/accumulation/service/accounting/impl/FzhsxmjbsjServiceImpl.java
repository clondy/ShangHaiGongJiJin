package com.capinfo.accumulation.service.accounting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.accumulation.parameter.accounting.FzhsxmjbsjParameter;
import com.capinfo.accumulation.service.accounting.FzhsxmjbsjService;
import com.capinfo.commons.project.service.region.RegionService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;

public class FzhsxmjbsjServiceImpl extends CommonsDataOperationServiceImpl<Fzhsxmjbsj,FzhsxmjbsjParameter> implements FzhsxmjbsjService{
	private static final Log logger = LogFactory.getLog(RegionService.class);
	
	 
		@Override
		public SearchCriteriaBuilder<Fzhsxmjbsj> getSearchCriteriaBuilder(FzhsxmjbsjParameter parameter) {
			SearchCriteriaBuilder<Fzhsxmjbsj> searchCriteriaBuilder = new SearchCriteriaBuilder<Fzhsxmjbsj>(Fzhsxmjbsj.class);
			if(parameter.getEntity().getHslxbm()!=null){
				searchCriteriaBuilder.addQueryCondition("hslxbm.hslxbm", RestrictionExpression.EQUALS_OP, parameter.getEntity().getHslxbm().getHslxbm());
			}
			searchCriteriaBuilder.addQueryCondition("hsxmmc", RestrictionExpression.LIKE_OP, parameter.getEntity().getHsxmmc());
			return searchCriteriaBuilder;
		}


	
	
}
