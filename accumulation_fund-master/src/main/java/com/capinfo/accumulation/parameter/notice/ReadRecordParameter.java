package com.capinfo.accumulation.parameter.notice;

import com.capinfo.accumulation.model.notice.ReadRecord;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by xiaopeng on 2017/10/20.
 */
public class ReadRecordParameter extends DataListParameter<ReadRecord> {
    @Override
    public ReadRecord getEntity() {
        return entity;
    }

    @Override
    public void setEntity(ReadRecord entity) {
        this.entity = entity;
    }

    @Override
    public Class<ReadRecord> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<ReadRecord> entityClazz) {
        this.entityClazz = entityClazz;
    }

    private ReadRecord entity = new ReadRecord();
    private Class<ReadRecord> entityClazz = ReadRecord.class;
}
