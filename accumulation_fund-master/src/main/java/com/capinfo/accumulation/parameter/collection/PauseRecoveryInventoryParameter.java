package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by capinfo on 2017/10/31.
 */
public class PauseRecoveryInventoryParameter extends DataListParameter<PauseRecoveryInventory> {

    private PauseRecoveryInventory entity = new PauseRecoveryInventory();
    private Class<PauseRecoveryInventory> entityClazz = PauseRecoveryInventory.class;

    //目录上路径状态
    private int pauseRecoveryInventoryFlag;

    private Company company = new Company();

    private CompanyAccountFund companyAccountFund = new CompanyAccountFund();



    @Override
    public PauseRecoveryInventory getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PauseRecoveryInventory entity) {
        this.entity = entity;
    }

    @Override
    public Class<PauseRecoveryInventory> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PauseRecoveryInventory> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public int getPauseRecoveryInventoryFlag() {
        return pauseRecoveryInventoryFlag;
    }

    public void setPauseRecoveryInventoryFlag(int pauseRecoveryInventoryFlag) {
        this.pauseRecoveryInventoryFlag = pauseRecoveryInventoryFlag;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }
}
