package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.CwhscsSpls;
import com.capinfo.framework.parameter.DataListParameter;

public class CwhscsSplsParameter  extends DataListParameter<CwhscsSpls>{
	private CwhscsSpls entity = new CwhscsSpls();
	private Class<CwhscsSpls> entityClazz = CwhscsSpls.class;
	public CwhscsSpls getEntity() {
		return entity;
	}
	public void setEntity(CwhscsSpls entity) {
		this.entity = entity;
	}
	public Class<CwhscsSpls> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<CwhscsSpls> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
