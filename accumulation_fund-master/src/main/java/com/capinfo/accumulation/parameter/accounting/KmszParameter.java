package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Kmszjbsj;
import com.capinfo.framework.parameter.DataListParameter;

public class KmszParameter extends DataListParameter<Kmszjbsj>{
	private String spyj;
	private Long oldkmjb;
	private Long lsbid;
	private Kmszjbsj entity  = new Kmszjbsj();
	private Class<Kmszjbsj> entityClazz = Kmszjbsj.class;
	public Long getOldkmjb() {
		return oldkmjb;
	}
	public void setOldkmjb(Long oldkmjb) {
		this.oldkmjb = oldkmjb;
	}
	private Long oldkmbh;		

	public Long getOldkmbh() {
		return oldkmbh;
	}
	public void setOldkmbh(Long oldkmbh) {
		this.oldkmbh = oldkmbh;
	}
	private String  oldkmmc;
	public String getOldkmmc() {
		return oldkmmc;
	}
	public void setOldkmmc(String oldkmmc) {
		this.oldkmmc = oldkmmc;
	}
	private Long newkmbh;
	private String newkmmc;

	
	public Long getNewkmbh() {
		return newkmbh;
	}
	public void setNewkmbh(Long newkmbh) {
		this.newkmbh = newkmbh;
	}
	public String getNewkmmc() {
		return newkmmc;
	}
	public void setNewkmmc(String newkmmc) {
		this.newkmmc = newkmmc;
	}

	@Override
	public Kmszjbsj getEntity() {
		return entity;
	}
	@Override
	public void setEntity(Kmszjbsj entity) {
		this.entity = entity;
	}
	@Override
	public Class<Kmszjbsj> getEntityClazz() {
		return entityClazz;
	}
	@Override
	public void setEntityClazz(Class<Kmszjbsj> entityClazz) {
		this.entityClazz = entityClazz;
	}
	public Long getLsbid() {
		return lsbid;
	}
	public void setLsbid(Long lsbid) {
		this.lsbid = lsbid;
	}
	public String getSpyj() {
		return spyj;
	}
	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}
	
}
