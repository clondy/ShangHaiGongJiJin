package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Ztsz;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by cairunbin on 2017/10/24.
 */
public class ZtszParameter extends DataListParameter<Ztsz>{
	 
    private Ztsz entity = new Ztsz();
    private Class<Ztsz> entityClazz = Ztsz.class;
	public Ztsz getEntity() {
		return entity;
	}
	public void setEntity(Ztsz entity) {
		this.entity = entity;
	}
	public Class<Ztsz> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Ztsz> entityClazz) {
		this.entityClazz = entityClazz;
	}
	




}
