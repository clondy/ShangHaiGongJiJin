package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzhsxglssj;
import com.capinfo.framework.parameter.DataListParameter;

public class FzhsxglssjParameter extends DataListParameter<Fzhsxglssj> {
	   private Long hsid;
	
	
	
	
	private Fzhsxglssj entity = new Fzhsxglssj();
	    private Class<Fzhsxglssj> entityClazz = Fzhsxglssj.class;
		public Fzhsxglssj getEntity() {
			return entity;
		}
		public void setEntity(Fzhsxglssj entity) {
			this.entity = entity;
		}
		public Class<Fzhsxglssj> getEntityClazz() {
			return entityClazz;
		}
		public void setEntityClazz(Class<Fzhsxglssj> entityClazz) {
			this.entityClazz = entityClazz;
		}
		  public Long getHsid() {
				return hsid;
			}
			public void setHsid(Long hsid) {
				this.hsid = hsid;
			}
}