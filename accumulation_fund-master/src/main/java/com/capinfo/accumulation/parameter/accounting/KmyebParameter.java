package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Kmyeb;
import com.capinfo.framework.parameter.DataListParameter;

public class KmyebParameter extends DataListParameter<Kmyeb>{
	//开始时间
	private String starttime;
	//结束时间
	private String endtime;
	private String startKm;
	private String endKm;
	private String xsfs;
	private Kmyeb entity = new Kmyeb();
    private Class<Kmyeb> entityClazz = Kmyeb.class;
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public Kmyeb getEntity() {
		return entity;
	}
	public void setEntity(Kmyeb entity) {
		this.entity = entity;
	}
	public Class<Kmyeb> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Kmyeb> entityClazz) {
		this.entityClazz = entityClazz;
	}
	public String getStartKm() {
		return startKm;
	}
	public void setStartKm(String startKm) {
		this.startKm = startKm;
	}
	public String getEndKm() {
		return endKm;
	}
	public void setEndKm(String endKm) {
		this.endKm = endKm;
	}
	public String getXsfs() {
		return xsfs;
	}
	public void setXsfs(String xsfs) {
		this.xsfs = xsfs;
	}
	
}
