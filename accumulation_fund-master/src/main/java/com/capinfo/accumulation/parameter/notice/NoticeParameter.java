package com.capinfo.accumulation.parameter.notice;

import com.capinfo.accumulation.model.notice.Notice;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by xiaopeng on 2017/10/20.
 */
public class NoticeParameter extends DataListParameter<Notice> {
    @Override
    public Notice getEntity() {
        return entity;
    }

    @Override
    public void setEntity(Notice entity) {
        this.entity = entity;
    }

    @Override
    public Class<Notice> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<Notice> entityClazz) {
        this.entityClazz = entityClazz;
    }

    private Notice entity = new Notice();
    private Class<Notice> entityClazz = Notice.class;
}
