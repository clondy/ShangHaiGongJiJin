package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.accumulation.model.collection.Payinitem;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by wcj on 2017/10/30.
 */
public class PayinitemParameter  extends DataListParameter<Payinitem> {
    private Payinitem entity=new Payinitem();

    private Class<Payinitem> entityClazz =Payinitem.class;

    @Override
    public Payinitem getEntity() {
        return entity;
    }

    @Override
    public void setEntity(Payinitem entity) {
        this.entity = entity;
    }

    @Override
    public Class<Payinitem> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<Payinitem> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
