package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Kmszsplssj;
import com.capinfo.framework.parameter.DataListParameter;

public class KmszsplssjParameter extends DataListParameter<Kmszsplssj> {
	private Long kmszid;
	public Long getKmszid() {
		return kmszid;
	}
	public void setKmszid(Long kmszid) {
		this.kmszid = kmszid;
	}
	private Kmszsplssj entity=new Kmszsplssj();
	private Class<Kmszsplssj> entityClazz=Kmszsplssj.class;
	public Kmszsplssj getEntity() {
		return entity;
	}
	public void setEntity(Kmszsplssj entity) {
		this.entity = entity;
	}
	public Class<Kmszsplssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Kmszsplssj> entityClazz) {
		this.entityClazz = entityClazz;
	}
}
