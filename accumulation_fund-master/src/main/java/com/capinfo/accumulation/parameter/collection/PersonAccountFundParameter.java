package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

import java.util.Date;

/**
 * Created by thinkpadwyl on 2017/9/27.
 */
public class PersonAccountFundParameter extends DataListParameter<PersonAccountFund>{
    private PersonAccountFund entity = new PersonAccountFund();
    private Class<PersonAccountFund> entityClazz = PersonAccountFund.class;

    private Person entityPerson = new Person();
    private Class<Person> entityPersonClazz = Person.class;

    private Long previousId;

    private int isFirstSave;

    private String companyAccount;

    private String deposit; //是否封存专户
    private String extra; //是否补充开户
    private String companyName;

    private Long depositRateMarkId ;    //  缴存率标志
    private Date startOpenedDate;
    private Date endOpenedDate;

    private Long regionId;

    public Long getDepositRateMarkId() {
        return depositRateMarkId;
    }

    public void setDepositRateMarkId(Long depositRateMarkId) {
        this.depositRateMarkId = depositRateMarkId;
    }

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    public Person getEntityPerson() {
        return entityPerson;
    }

    public void setEntityPerson(Person entityPerson) {
        this.entityPerson = entityPerson;
    }

    public Class<Person> getEntityPersonClazz() {
        return entityPersonClazz;
    }

    public void setEntityPersonClazz(Class<Person> entityPersonClazz) {
        this.entityPersonClazz = entityPersonClazz;
    }



    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public PersonAccountFund getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PersonAccountFund entity) {
        this.entity = entity;
    }

    @Override
    public Class<PersonAccountFund> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PersonAccountFund> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Date getStartOpenedDate() {
        return startOpenedDate;
    }

    public void setStartOpenedDate(Date startOpenedDate) {
        this.startOpenedDate = startOpenedDate;
    }

    public Date getEndOpenedDate() {
        return endOpenedDate;
    }

    public void setEndOpenedDate(Date endOpenedDate) {
        this.endOpenedDate = endOpenedDate;
    }

    public Long getPreviousId() {
        return previousId;
    }

    public void setPreviousId(Long previousId) {
        this.previousId = previousId;
    }

    public int getIsFirstSave() {
        return isFirstSave;
    }

    public void setIsFirstSave(int isFirstSave) {
        this.isFirstSave = isFirstSave;
    }
}
