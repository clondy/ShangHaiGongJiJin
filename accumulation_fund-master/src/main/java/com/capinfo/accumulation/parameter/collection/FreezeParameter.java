package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Freeze;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by thinkpadwyl on 2017/10/19.
 */
public class FreezeParameter extends DataListParameter<Freeze> {
    private Freeze entity = new Freeze();
    private Class<Freeze> entityClazz = Freeze.class;

    private String freezeCompanyAccount;

    private String freezeFlag;  //标准：1-冻结；2-冻结延期；3-解冻；
    private String idNumber;  //身份证号
    private String idName;     //身份名称


    public String getFreezeFlag() {
        return freezeFlag;
    }

    public void setFreezeFlag(String freezeFlag) {
        this.freezeFlag = freezeFlag;
    }

    public String getFreezeCompanyAccount() {
        return freezeCompanyAccount;
    }

    public void setFreezeCompanyAccount(String freezeCompanyAccount) {
        this.freezeCompanyAccount = freezeCompanyAccount;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }


    @Override
    public Freeze getEntity() {
        return entity;
    }

    @Override
    public void setEntity(Freeze entity) {
        this.entity = entity;
    }

    @Override
    public Class<Freeze> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<Freeze> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
