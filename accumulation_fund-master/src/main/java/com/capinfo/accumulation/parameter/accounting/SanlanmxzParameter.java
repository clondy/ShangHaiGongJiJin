package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Sanlanmxz;
import com.capinfo.framework.parameter.DataListParameter;

public class SanlanmxzParameter extends DataListParameter<Sanlanmxz>{

	private Sanlanmxz entity = new Sanlanmxz();
	private Class<Sanlanmxz> entityClazz = Sanlanmxz.class;
	
	private String startDate;//开始日期
    private String endDate;//截止日期
    
    private double startMoney;//最小金额
    private double endMoney;//最大金额
    
    
	
	public double getStartMoney() {
		return startMoney;
	}
	public void setStartMoney(double startMoney) {
		this.startMoney = startMoney;
	}
	public double getEndMoney() {
		return endMoney;
	}
	public void setEndMoney(double endMoney) {
		this.endMoney = endMoney;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Sanlanmxz getEntity() {
		return entity;
	}
	public void setEntity(Sanlanmxz entity) {
		this.entity = entity;
	}
	public Class<Sanlanmxz> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Sanlanmxz> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
	
}
