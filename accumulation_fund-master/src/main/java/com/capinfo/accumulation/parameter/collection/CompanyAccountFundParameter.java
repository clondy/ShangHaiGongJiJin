package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

import java.util.Date;

/**
 * Created by cairunbin on 2017/10/24.
 */
public class CompanyAccountFundParameter extends DataListParameter<CompanyAccountFund>{
    private CompanyAccountFund entity = new CompanyAccountFund();
    private Class<CompanyAccountFund> entityClazz = CompanyAccountFund.class;

    private String startOpenDate;//开户日期（查询框左侧）
    private String endOpenDate;//开户日期（查询框右侧）
    private String confirmerId; //审核人Id

    private String startCompanyCloseDate;//销户申请日期
    private String endCompanyCloseDate;//销户申请日期

    public String getStartCompanyCloseDate() {
        return startCompanyCloseDate;
    }

    public void setStartCompanyCloseDate(String startCompanyCloseDate) {
        this.startCompanyCloseDate = startCompanyCloseDate;
    }

    public String getEndCompanyCloseDate() {
        return endCompanyCloseDate;
    }

    public void setEndCompanyCloseDate(String endCompanyCloseDate) {
        this.endCompanyCloseDate = endCompanyCloseDate;
    }

    public String getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(String confirmerId) {
        this.confirmerId = confirmerId;
    }

    public String getStartOpenDate() {
        return startOpenDate;
    }

    public void setStartOpenDate(String startOpenDate) {
        this.startOpenDate = startOpenDate;
    }

    public String getEndOpenDate() {
        return endOpenDate;
    }

    public void setEndOpenDate(String endOpenDate) {
        this.endOpenDate = endOpenDate;
    }

    @Override
    public CompanyAccountFund getEntity() {
        return entity;
    }

    @Override
    public void setEntity(CompanyAccountFund entity) {
        this.entity = entity;
    }

    @Override
    public Class<CompanyAccountFund> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<CompanyAccountFund> entityClazz) {
        this.entityClazz = entityClazz;
    }


}
