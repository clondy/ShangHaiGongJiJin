package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Cwhscs;
import com.capinfo.framework.parameter.DataListParameter;

public class CwhscsParameter extends DataListParameter<Cwhscs>{
   
	private Long lsid;
	public Long getLsid() {
		return lsid;
	}
	public void setLsid(Long lsid) {
		this.lsid = lsid;
	}
	private Cwhscs entity = new Cwhscs();
	private Class<Cwhscs> entityClazz = Cwhscs.class;
	
	public Cwhscs getEntity() {
		return entity;
	}
	public void setEntity(Cwhscs entity) {
		this.entity = entity;
	}
	public Class<Cwhscs> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Cwhscs> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
