package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.CwhscsXgls;
import com.capinfo.framework.parameter.DataListParameter;

public class CwhscsXglsParameter extends DataListParameter<CwhscsXgls>{
   
	private CwhscsXgls entity = new CwhscsXgls();
	private Class<CwhscsXgls> entityClazz = CwhscsXgls.class;
	public CwhscsXgls getEntity() {
		return entity;
	}
	public void setEntity(CwhscsXgls entity) {
		this.entity = entity;
	}
	public Class<CwhscsXgls> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<CwhscsXgls> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
