package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Ztxglssj;
import com.capinfo.framework.parameter.DataListParameter;

public class ZtxglssjParameter extends DataListParameter<Ztxglssj>{
	
	private Ztxglssj entity = new Ztxglssj();
    private Class<Ztxglssj> entityClazz = Ztxglssj.class;
    
	public Ztxglssj getEntity() {
		return entity;
	}
	public void setEntity(Ztxglssj entity) {
		this.entity = entity;
	}
	public Class<Ztxglssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Ztxglssj> entityClazz) {
		this.entityClazz = entityClazz;
	}
}
