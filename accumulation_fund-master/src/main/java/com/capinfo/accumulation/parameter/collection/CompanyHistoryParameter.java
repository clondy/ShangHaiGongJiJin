package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.framework.parameter.DataListParameter;


/**
 * Created by Rexxxar on 2017/9/28.
 */
public class CompanyHistoryParameter extends DataListParameter<CompanyHistory> {

    private CompanyHistory entity = new CompanyHistory();
    private Class<CompanyHistory> entityClazz = CompanyHistory.class;

    private CompanyAccountHistory companyAccount = new CompanyAccountHistory();
    private Class<CompanyAccountHistory> entityClazzAccount = CompanyAccountHistory.class;

    //标志位 默认是0,包含有组织机构代码或者社会信用代码；没有和没填是1。
    private Long flag = 0L;
    //网点变更 需要接受页面的历史ID的参数
    private Long companyHistoryId = 0L;


    @Override
    public CompanyHistory getEntity() {
        return entity;
    }

    @Override
    public void setEntity(CompanyHistory entity) {
        this.entity = entity;
    }

    @Override
    public Class<CompanyHistory> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<CompanyHistory> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public CompanyAccountHistory getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(CompanyAccountHistory companyAccount) {
        this.companyAccount = companyAccount;
    }

    public Class<CompanyAccountHistory> getEntityClazzAccount() {
        return entityClazzAccount;
    }

    public void setEntityClazzAccount(Class<CompanyAccountHistory> entityClazzAccount) {
        this.entityClazzAccount = entityClazzAccount;
    }


    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getCompanyHistoryId() {
        return companyHistoryId;
    }

    public void setCompanyHistoryId(Long companyHistoryId) {
        this.companyHistoryId = companyHistoryId;
    }
}
