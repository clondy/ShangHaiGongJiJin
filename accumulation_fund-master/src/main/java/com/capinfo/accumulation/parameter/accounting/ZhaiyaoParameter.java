package com.capinfo.accumulation.parameter.accounting;
import com.capinfo.accumulation.model.accounting.Zhaiyao;
import com.capinfo.framework.parameter.DataListParameter;

public class ZhaiyaoParameter extends DataListParameter<Zhaiyao>{

	private Zhaiyao entity = new Zhaiyao();
	private Class<Zhaiyao> entityClazz = Zhaiyao.class;
	
	public Zhaiyao getEntity() {
		return entity;
	}

	public void setEntity(Zhaiyao entity) {
		this.entity = entity;
	}

	public Class<Zhaiyao> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClazz(Class<Zhaiyao> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
	
}
