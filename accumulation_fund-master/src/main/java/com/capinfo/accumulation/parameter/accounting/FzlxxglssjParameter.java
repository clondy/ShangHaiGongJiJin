package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzlxxglssj;

import com.capinfo.framework.parameter.DataListParameter;

public class FzlxxglssjParameter extends DataListParameter<Fzlxxglssj>{
	private Fzlxxglssj entity = new Fzlxxglssj();
    private Class<Fzlxxglssj> entityClazz = Fzlxxglssj.class;
	public Fzlxxglssj getEntity() {
		return entity;
	}
	public void setEntity(Fzlxxglssj entity) {
		this.entity = entity;
	}
	public Class<Fzlxxglssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fzlxxglssj> entityClazz) {
		this.entityClazz = entityClazz;
	}
	


}
