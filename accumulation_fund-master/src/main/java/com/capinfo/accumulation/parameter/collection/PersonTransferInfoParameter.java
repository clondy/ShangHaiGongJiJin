package com.capinfo.accumulation.parameter.collection;


import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.PersonTransferInfo;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by Lee on 2017/10/19.
 */
public class PersonTransferInfoParameter extends DataListParameter<PersonTransferInfo> {

    private PersonTransferInfo entity = new PersonTransferInfo();
    private Class<PersonTransferInfo> entityClazz = PersonTransferInfo.class;
    private CompanyAccountFund companyAccountFund = new CompanyAccountFund();
    private String companyAccount; //单位账号
    private String personalAccount;//个人账号


    public CompanyAccountFund getCompanyAccountFund() {
        return companyAccountFund;
    }

    public void setCompanyAccountFund(CompanyAccountFund companyAccountFund) {
        this.companyAccountFund = companyAccountFund;
    }

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    public String getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(String personalAccount) {
        this.personalAccount = personalAccount;
    }

    @Override
    public PersonTransferInfo getEntity() {
        return entity;
    }

    public void setEntity(PersonTransferInfo entity) {
        this.entity = entity;
    }

    @Override
    public Class<PersonTransferInfo> getEntityClazz() {
        return entityClazz;
    }

    public void setEntityClazz(Class<PersonTransferInfo> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
