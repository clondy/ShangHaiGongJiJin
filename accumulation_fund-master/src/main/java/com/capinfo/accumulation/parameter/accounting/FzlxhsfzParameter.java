package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzlxhsfz;
import com.capinfo.framework.parameter.DataListParameter;

public class FzlxhsfzParameter extends DataListParameter<Fzlxhsfz>{
	private Fzlxhsfz entity = new Fzlxhsfz();
	private Class<Fzlxhsfz> entityClazz = Fzlxhsfz.class;
	public Fzlxhsfz getEntity() {
		return entity;
	}
	public void setEntity(Fzlxhsfz entity) {
		this.entity = entity;
	}
	public Class<Fzlxhsfz> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fzlxhsfz> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
