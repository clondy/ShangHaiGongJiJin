package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Kmszxglssj;
import com.capinfo.framework.parameter.DataListParameter;

public class KmszxgParameter extends DataListParameter<Kmszxglssj> {
	private Kmszxglssj entity=new Kmszxglssj();
	private Class<Kmszxglssj> entityClazz=Kmszxglssj.class;
	public Kmszxglssj getEntity() {
		return entity;
	}
	public void setEntity(Kmszxglssj entity) {
		this.entity = entity;
	}
	public Class<Kmszxglssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Kmszxglssj> entityClazz) {
		this.entityClazz = entityClazz;
	} 

}
