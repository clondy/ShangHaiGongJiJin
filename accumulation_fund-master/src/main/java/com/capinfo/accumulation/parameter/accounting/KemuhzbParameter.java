package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Kemuhzb;
import com.capinfo.framework.parameter.DataListParameter;

public class KemuhzbParameter extends DataListParameter<Kemuhzb>{
	
	private String tjny;
	
	
	public String getTjny() {
		return tjny;
	}
	public void setTjny(String tjny) {
		this.tjny = tjny;
	}
	private Kemuhzb entity = new Kemuhzb();
	private Class<Kemuhzb> entityClazz = Kemuhzb.class;
	
	public Kemuhzb getEntity() {
		return entity;
	}
	public void setEntity(Kemuhzb entity) {
		this.entity = entity;
	}
	public Class<Kemuhzb> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Kemuhzb> entityClazz) {
		this.entityClazz = entityClazz;
	}
}
