package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.PersonTransferItem;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by wgg on 2017/11/1.
 */
public class PersonTransferItemParameter extends DataListParameter<PersonTransferItem> {

    private PersonTransferItem entity = new PersonTransferItem();
    private Class<PersonTransferItem> entityClazz = PersonTransferItem.class;

    @Override
    public PersonTransferItem getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PersonTransferItem entity) {
        this.entity = entity;
    }

    @Override
    public Class<PersonTransferItem> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PersonTransferItem> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
