package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.accumulation.model.collection.PersonHistoryAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by thinkpadwyl on 2017/9/27.
 */
public class PersonHistoryAccountFundParameter extends DataListParameter<PersonHistoryAccountFund>{

    private PersonHistoryAccountFund entity = new PersonHistoryAccountFund();
    private Class<PersonHistoryAccountFund> entityClazz = PersonHistoryAccountFund.class;


    private PersonHistory entityPerson = new PersonHistory();
    private Class<PersonHistory> entityPersonClazz = PersonHistory.class;

    private Long previousId;
    //是否是第一次点击保存按钮(0是第一次保存，1不是)
    private int isFirstSave = 0;

    private String companyAccount;

    private String deposit; //是否封存专户
    private String extra; //是否补充开户
    private String companyName;
    private Long regionId;

    private Long depositRateMarkId ;    //  缴存率标志

    public Long getDepositRateMarkId() {
        return depositRateMarkId;
    }

    public void setDepositRateMarkId(Long depositRateMarkId) {
        this.depositRateMarkId = depositRateMarkId;
    }

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    public PersonHistory getEntityPerson() {
        return entityPerson;
    }

    public void setEntityPerson(PersonHistory entityPerson) {
        this.entityPerson = entityPerson;
    }

    public Class<PersonHistory> getEntityPersonClazz() {
        return entityPersonClazz;
    }

    public void setEntityPersonClazz(Class<PersonHistory> entityPersonClazz) {
        this.entityPersonClazz = entityPersonClazz;
    }



    @Override
    public PersonHistoryAccountFund getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PersonHistoryAccountFund entity) {
        this.entity = entity;
    }

    @Override
    public Class<PersonHistoryAccountFund> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PersonHistoryAccountFund> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getPreviousId() {
        return previousId;
    }

    public void setPreviousId(Long previousId) {
        this.previousId = previousId;
    }

    public int getIsFirstSave() {
        return isFirstSave;
    }

    public void setIsFirstSave(int isFirstSave) {
        this.isFirstSave = isFirstSave;
    }
}
