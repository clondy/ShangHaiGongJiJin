package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

import java.util.Date;

/**
 * Created by thinkpadwyl on 2017/9/27.
 */
public class PersonParameter extends DataListParameter<Person>{
    private Person entity = new Person();
    private PersonAccountFund personAccount = new PersonAccountFund();

    private Class<Person> entityClazz = Person.class;

    private String deposit; //是否封存专户
    private String extra; //是否补充开户

    private String turnOutPaymentDate;//转出缴存年月
    private String turnInPaymentDate;//转入缴存年月
    private String turnOutCompanyAccount;//转出单位账号
    private String turnInCompanyAccount;//转入单位账号
    private String turnOutCompanyName;//转出单位名称
    private String turnInCompanyName;//转入单位名称

    public String getTurnOutPaymentDate() {
        return turnOutPaymentDate;
    }

    public void setTurnOutPaymentDate(String turnOutPaymentDate) {
        this.turnOutPaymentDate = turnOutPaymentDate;
    }

    public String getTurnInPaymentDate() {
        return turnInPaymentDate;
    }

    public void setTurnInPaymentDate(String turnInPaymentDate) {
        this.turnInPaymentDate = turnInPaymentDate;
    }

    public String getTurnOutCompanyName() {
        return turnOutCompanyName;
    }

    public void setTurnOutCompanyName(String turnOutCompanyName) {
        this.turnOutCompanyName = turnOutCompanyName;
    }

    public String getTurnInCompanyName() {
        return turnInCompanyName;
    }

    public void setTurnInCompanyName(String turnInCompanyName) {
        this.turnInCompanyName = turnInCompanyName;
    }

    public String getTurnInCompanyAccount() {
        return turnInCompanyAccount;
    }

    public void setTurnInCompanyAccount(String turnInCompanyAccount) {
        this.turnInCompanyAccount = turnInCompanyAccount;
    }

    public String getTurnOutCompanyAccount() {
        return turnOutCompanyAccount;
    }

    public void setTurnOutCompanyAccount(String turnOutCompanyAccount) {
        this.turnOutCompanyAccount = turnOutCompanyAccount;
    }


    @Override
    public Person getEntity() {
        return entity;
    }

    @Override
    public void setEntity(Person entity) {
        this.entity = entity;
    }

    public PersonAccountFund getPersonAccount() {
        return personAccount;
    }

    public void setPersonAccount(PersonAccountFund personAccount) {
        this.personAccount = personAccount;
    }

    @Override
    public Class<Person> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<Person> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
