package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzyeb;
import com.capinfo.framework.parameter.DataListParameter;

public class FzyebParameter extends DataListParameter<Fzyeb>{

	//开始时间
	private String starttime;
	//结束时间
	private String endtime;
	private Fzyeb entity = new Fzyeb();
    private Class<Fzyeb> entityClazz = Fzyeb.class;
	public Fzyeb getEntity() {
		return entity;
	}
	public void setEntity(Fzyeb entity) {
		this.entity = entity;
	}
	public Class<Fzyeb> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fzyeb> entityClazz) {
		this.entityClazz = entityClazz;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
    
}
