package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzlxsplssj;
import com.capinfo.framework.parameter.DataListParameter;

public class FzlxsplssjParameter extends DataListParameter<Fzlxsplssj>{
	private Fzlxsplssj entity = new Fzlxsplssj();
    private Class<Fzlxsplssj> entityClazz = Fzlxsplssj.class;
	public Fzlxsplssj getEntity() {
		return entity;
	}
	public void setEntity(Fzlxsplssj entity) {
		this.entity = entity;
	}
	public Class<Fzlxsplssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fzlxsplssj> entityClazz) {
		this.entityClazz = entityClazz;
	}
	


}
