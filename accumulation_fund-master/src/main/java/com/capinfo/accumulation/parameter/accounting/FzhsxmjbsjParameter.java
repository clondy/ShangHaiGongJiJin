package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzhsxmjbsj;
import com.capinfo.framework.parameter.DataListParameter;

public class FzhsxmjbsjParameter extends DataListParameter<Fzhsxmjbsj>{
	  private Fzhsxmjbsj entity = new Fzhsxmjbsj();
	    private Class<Fzhsxmjbsj> entityClazz = Fzhsxmjbsj.class;
		public Fzhsxmjbsj getEntity() {
			return entity;
		}
		public void setEntity(Fzhsxmjbsj entity) {
			this.entity = entity;
		}
		public Class<Fzhsxmjbsj> getEntityClazz() {
			return entityClazz;
		}
		public void setEntityClazz(Class<Fzhsxmjbsj> entityClazz) {
			this.entityClazz = entityClazz;
		}
}
