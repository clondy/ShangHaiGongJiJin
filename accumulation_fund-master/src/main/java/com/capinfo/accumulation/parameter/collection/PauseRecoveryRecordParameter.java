package com.capinfo.accumulation.parameter.collection;


import com.capinfo.accumulation.model.collection.PauseRecoveryRecord;
import com.capinfo.accumulation.model.collection.PauseRecoveryInventory;
import com.capinfo.accumulation.model.collection.Person;
import com.capinfo.accumulation.model.collection.PersonAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by Rexxxar on 2017/10/18.
 */
public class PauseRecoveryRecordParameter extends DataListParameter<PauseRecoveryRecord> {

    private PauseRecoveryRecord entity = new PauseRecoveryRecord();
    private Class<PauseRecoveryRecord> entityClazz = PauseRecoveryRecord.class;


    private PersonAccountFund personAccountFund = new PersonAccountFund();
    private Class<PersonAccountFund> entityClazzAccount = PersonAccountFund.class;


    //单位名称
    private String gbCompanyName = null;

    private int pauseRecoveryFlag;

    private int pauseRecoveryMiddleFlag;

    //单位公积金账号
    private String companyAccount;
    //个人公积金账号
    private String idCardNumber;

    //停复缴标记
    private int pauseRecoveryInventoryFlag;

    //取session中清册id
    private long pauseRecoveryInventoryId;




    @Override
    public PauseRecoveryRecord getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PauseRecoveryRecord entity) {
        this.entity = entity;
    }

    @Override
    public Class<PauseRecoveryRecord> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PauseRecoveryRecord> entityClazz) {
        this.entityClazz = entityClazz;
    }

    public PersonAccountFund getPersonAccountFund() {
        return personAccountFund;
    }

    public void setPersonAccountFund(PersonAccountFund personAccountFund) {
        this.personAccountFund = personAccountFund;
    }

    public Class<PersonAccountFund> getEntityClazzAccount() {
        return entityClazzAccount;
    }

    public void setEntityClazzAccount(Class<PersonAccountFund> entityClazzAccount) {
        this.entityClazzAccount = entityClazzAccount;
    }

    public String getGbCompanyName() {
        return gbCompanyName;
    }

    public void setGbCompanyName(String gbCompanyName) {
        this.gbCompanyName = gbCompanyName;
    }

    public int getPauseRecoveryFlag() {
        return pauseRecoveryFlag;
    }

    public void setPauseRecoveryFlag(int pauseRecoveryFlag) {
        this.pauseRecoveryFlag = pauseRecoveryFlag;
    }

    public int getPauseRecoveryMiddleFlag() {
        return pauseRecoveryMiddleFlag;
    }

    public void setPauseRecoveryMiddleFlag(int pauseRecoveryMiddleFlag) {
        this.pauseRecoveryMiddleFlag = pauseRecoveryMiddleFlag;
    }

    public String getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public int getPauseRecoveryInventoryFlag() {
        return pauseRecoveryInventoryFlag;
    }

    public void setPauseRecoveryInventoryFlag(int pauseRecoveryInventoryFlag) {
        this.pauseRecoveryInventoryFlag = pauseRecoveryInventoryFlag;
    }

    public long getPauseRecoveryInventoryId() {
        return pauseRecoveryInventoryId;
    }

    public void setPauseRecoveryInventoryId(long pauseRecoveryInventoryId) {
        this.pauseRecoveryInventoryId = pauseRecoveryInventoryId;
    }
}



