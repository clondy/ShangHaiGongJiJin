package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzlxsz;

import com.capinfo.framework.parameter.DataListParameter;

public class FzlxszParameter extends DataListParameter<Fzlxsz>{
	private Fzlxsz entity = new Fzlxsz();
    private Class<Fzlxsz> entityClazz = Fzlxsz.class;
	public Fzlxsz getEntity() {
		return entity;
	}
	public void setEntity(Fzlxsz entity) {
		this.entity = entity;
	}
	public Class<Fzlxsz> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fzlxsz> entityClazz) {
		this.entityClazz = entityClazz;
	}
	


}
