package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Ztsplssj;
import com.capinfo.framework.parameter.DataListParameter;

public class ZtsplssjParameter extends DataListParameter<Ztsplssj>{
	private Long id;
	private Long ztls;
	
	
	  public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getZtls() {
		return ztls;
	}
	public void setZtls(Long ztls) {
		this.ztls = ztls;
	}
	private Ztsplssj entity = new Ztsplssj();
	    public Ztsplssj getEntity() {
		return entity;
	}
	public void setEntity(Ztsplssj entity) {
		this.entity = entity;
	}
	public Class<Ztsplssj> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Ztsplssj> entityClazz) {
		this.entityClazz = entityClazz;
	}
		private Class<Ztsplssj> entityClazz = Ztsplssj.class;
}
