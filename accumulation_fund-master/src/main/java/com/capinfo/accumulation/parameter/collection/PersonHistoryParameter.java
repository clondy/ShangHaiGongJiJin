package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.PersonHistory;
import com.capinfo.framework.parameter.DataListParameter;


/**
 * Created by Rexxxar on 2017/9/28.
 */
public class PersonHistoryParameter extends DataListParameter<PersonHistory> {

    private PersonHistory entity = new PersonHistory();
    private Class<PersonHistory> entityClazz = PersonHistory.class;

//    private PersonAccountHistory companyAccount = new CompanyAccountHistory();
//    private Class<CompanyAccountHistory> entityClazzAccount = CompanyAccountHistory.class;

    private String deposit; //是否封存专户
    private String extra; //是否补充开户

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    @Override
    public PersonHistory getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PersonHistory entity) {
        this.entity = entity;
    }

    @Override
    public Class<PersonHistory> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PersonHistory> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
