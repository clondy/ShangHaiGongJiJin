package com.capinfo.accumulation.parameter.accounting;


import com.capinfo.accumulation.model.accounting.Voucher;
import com.capinfo.framework.parameter.DataListParameter;

public class VoucherParameter  extends DataListParameter<Voucher> {
	
	private String startTime;
	private String endTime;
	private String fhzt;
	private String qxfw;
	private String startJzpzh;
	private String endJzpzh;
	private Long  id;
	private String jzzt;
	private String jzpzids;
	public String getJzzt() {
		return jzzt;
	}
	public void setJzzt(String jzzt) {
		this.jzzt = jzzt;
	}
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	private Voucher entity  = new Voucher();
	public Voucher getEntity() {
		return entity;
	}
	public void setEntity(Voucher entity) {
		this.entity = entity;
	}
	public Class<Voucher> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Voucher> entityClazz) {
		this.entityClazz = entityClazz;
	}
	private Class<Voucher> entityClazz = Voucher.class;
	
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFhzt() {
		return fhzt;
	}
	public void setFhzt(String fhzt) {
		this.fhzt = fhzt;
	}
	public String getQxfw() {
		return qxfw;
	}
	public void setQxfw(String qxfw) {
		this.qxfw = qxfw;
	}
	public String getStartJzpzh() {
		return startJzpzh;
	}
	public void setStartJzpzh(String startJzpzh) {
		this.startJzpzh = startJzpzh;
	}
	public String getEndJzpzh() {
		return endJzpzh;
	}
	public void setEndJzpzh(String endJzpzh) {
		this.endJzpzh = endJzpzh;
	}
	public String getJzpzids() {
		return jzpzids;
	}
	public void setJzpzids(String jzpzids) {
		this.jzpzids = jzpzids;
	}
	
	
}
