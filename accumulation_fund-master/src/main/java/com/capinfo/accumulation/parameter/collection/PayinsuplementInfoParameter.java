package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.PayinSupplementInfo;
import com.capinfo.accumulation.model.collection.Payinitem;
import com.capinfo.framework.parameter.DataListParameter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wcj on 2017/10/30.
 */
public class PayinsuplementInfoParameter  extends DataListParameter<PayinSupplementInfo> {
    private PayinSupplementInfo entity=new PayinSupplementInfo();

    private Class<PayinSupplementInfo> entityClazz =PayinSupplementInfo.class;

    private Date startReceivedDate;

    private Date endReceivedDate;

    private String companyAccot;

    public String getCompanyAccot() {
        return companyAccot;
    }

    public void setCompanyAccot(String companyAccot) {
        this.companyAccot = companyAccot;
    }

    public Date getEndReceivedDate() {
        return endReceivedDate;
    }

    private String companyAccount;

    public String getCompanyAccount() {
        return companyAccount;
    }

    private String personAccount;

    public String getPersonAccount() {
        return personAccount;
    }

    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setPersonAccount(String personAccount) {
        this.personAccount = personAccount;
    }

    private String personName;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public void setCompanyAccount(String companyAccount) {
        this.companyAccount = companyAccount;
    }
    private String  unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void setEndReceivedDate(Date endReceivedDate) {
        this.endReceivedDate = endReceivedDate;
    }

    public Date getStartReceivedDate() {
        return startReceivedDate;
    }

    public void setStartReceivedDate(Date startReceivedDate) {
        this.startReceivedDate = startReceivedDate;
    }

    @Override
    public PayinSupplementInfo getEntity() {
        return entity;
    }

    @Override
    public void setEntity(PayinSupplementInfo entity) {
        this.entity = entity;
    }

    @Override
    public Class<PayinSupplementInfo> getEntityClazz() {
        return entityClazz;
    }

    @Override
    public void setEntityClazz(Class<PayinSupplementInfo> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
