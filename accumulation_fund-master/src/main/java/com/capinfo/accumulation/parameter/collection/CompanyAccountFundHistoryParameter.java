package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.CompanyAccountHistory;
import com.capinfo.accumulation.model.collection.CompanyHistory;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * Created by cairunbin on 2017/10/24.
 */
public class CompanyAccountFundHistoryParameter extends DataListParameter<CompanyAccountHistory>{

    //单位历史信息
    private CompanyHistory entityCompany = new CompanyHistory();
    private Class<CompanyHistory> entityCompanyClazz = CompanyHistory.class;

    //单位公积金历史信息
    private CompanyAccountHistory entity = new CompanyAccountHistory();
    private Class<CompanyAccountHistory> entityClazz = CompanyAccountHistory.class;

    //单位信息
    private Company companyEntity = new Company();
    private Class<Company> companyEntityClazz = Company.class;

    //单位id
    private Long companyId;


    public Company getCompanyEntity() {
        return companyEntity;
    }

    public void setCompanyEntity(Company companyEntity) {
        this.companyEntity = companyEntity;
    }

    public Class<Company> getCompanyEntityClazz() {
        return companyEntityClazz;
    }

    public void setCompanyEntityClazz(Class<Company> companyEntityClazz) {
        this.companyEntityClazz = companyEntityClazz;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public CompanyHistory getEntityCompany() {
        return entityCompany;
    }

    public void setEntityCompany(CompanyHistory entityCompany) {
        this.entityCompany = entityCompany;
    }

    public Class<CompanyHistory> getEntityCompanyClazz() {
        return entityCompanyClazz;
    }

    public void setEntityCompanyClazz(Class<CompanyHistory> entityCompanyClazz) {
        this.entityCompanyClazz = entityCompanyClazz;
    }

    @Override
    public CompanyAccountHistory getEntity() {
        return entity;
    }

    public void setEntity(CompanyAccountHistory entity) {
        this.entity = entity;
    }

    @Override
    public Class<CompanyAccountHistory> getEntityClazz() {
        return entityClazz;
    }

    public void setEntityClazz(Class<CompanyAccountHistory> entityClazz) {
        this.entityClazz = entityClazz;
    }
}
