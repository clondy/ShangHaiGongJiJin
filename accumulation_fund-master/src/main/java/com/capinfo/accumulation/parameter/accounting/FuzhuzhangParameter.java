package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fuzhuzhang;
import com.capinfo.framework.parameter.DataListParameter;

public class FuzhuzhangParameter extends DataListParameter<Fuzhuzhang>{
	
	private String startDate;
	private String endDate;
	
	
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	private Fuzhuzhang entity = new Fuzhuzhang();
	private Class<Fuzhuzhang> entityClazz = Fuzhuzhang.class;
	public Fuzhuzhang getEntity() {
		return entity;
	}
	public void setEntity(Fuzhuzhang entity) {
		this.entity = entity;
	}
	public Class<Fuzhuzhang> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Fuzhuzhang> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
