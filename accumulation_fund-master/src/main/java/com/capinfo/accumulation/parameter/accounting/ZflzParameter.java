package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Zflz;
import com.capinfo.framework.parameter.DataListParameter;

public class ZflzParameter extends DataListParameter<Zflz>{
	
	private String time;
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	private Zflz entity = new Zflz();
	private Class<Zflz> entityClazz = Zflz.class;
	
	public Zflz getEntity() {
		return entity;
	}
	public void setEntity(Zflz entity) {
		this.entity = entity;
	}
	public Class<Zflz> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Zflz> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
	
}
