package com.capinfo.accumulation.parameter.accounting;

import com.capinfo.accumulation.model.accounting.Fzhssplssj;
import com.capinfo.framework.parameter.DataListParameter;

public class FzhssplssjParameter extends DataListParameter<Fzhssplssj> {
	    private Fzhssplssj entity = new Fzhssplssj();
	    private Class<Fzhssplssj> entityClazz = Fzhssplssj.class;
		public Fzhssplssj getEntity() {
			return entity;
		}
		public void setEntity(Fzhssplssj entity) {
			this.entity = entity;
		}
		public Class<Fzhssplssj> getEntityClazz() {
			return entityClazz;
		}
		public void setEntityClazz(Class<Fzhssplssj> entityClazz) {
			this.entityClazz = entityClazz;
		}
}
