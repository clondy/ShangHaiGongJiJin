package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.Company;
import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.framework.parameter.DataListParameter;

import java.util.Date;

/**
 * Created by thinkpadwyl on 2017/9/27.
 */
public class CompanyParameter extends DataListParameter<Company>{
    private Company entity = new Company();
    private CompanyAccountFund companyAccount = new CompanyAccountFund();
    private Class<Company> entityClazz = Company.class;
    private String companyCode; //社会信用统一编码
    private String confirmerId; //审核人Id

    private String turnOutCompanyAccount;//转出单位账号
    private String turnInCompanyAccount;//转入单位账号

    private Long accountType;   //账户类型

    private String companyPaymentDate;//转出缴存年月

    private String gbCompanyName;//转入单位名称


    public String getGbCompanyName() {
        return gbCompanyName;
    }

    public void setGbCompanyName(String gbCompanyName) {
        this.gbCompanyName = gbCompanyName;
    }

    public String getCompanyPaymentDate() {
        return companyPaymentDate;
    }

    public void setCompanyPaymentDate(String companyPaymentDate) {
        this.companyPaymentDate = companyPaymentDate;
    }

    private Long companyHistoryId;

    public String getTurnOutCompanyAccount() {
        return turnOutCompanyAccount;
    }

    public void setTurnOutCompanyAccount(String turnOutCompanyAccount) {
        this.turnOutCompanyAccount = turnOutCompanyAccount;
    }

    public String getTurnInCompanyAccount() {
        return turnInCompanyAccount;
    }

    public void setTurnInCompanyAccount(String turnInCompanyAccount) {
        this.turnInCompanyAccount = turnInCompanyAccount;
    }

    public Long getCompanyHistoryId() {
        return companyHistoryId;
    }

    public void setCompanyHistoryId(Long companyHistoryId) {
        this.companyHistoryId = companyHistoryId;
    }

    private String gbCompanyAccount;

    public String getGbCompanyAccount() {
        return gbCompanyAccount;
    }

    public void setGbCompanyAccount(String gbCompanyAccount) {
        this.gbCompanyAccount = gbCompanyAccount;
    }

    public Long getAccountType() {
        return accountType;
    }

    public void setAccountType(Long accountType) {
        this.accountType = accountType;
    }

    //标志位 默认是0,包含有组织机构代码或者社会信用代码；没有和没填是1。
    private Long flag = 0L;

    private Long siteId;

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public String getConfirmerId() {
        return confirmerId;
    }

    public void setConfirmerId(String confirmerId) {
        this.confirmerId = confirmerId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }


    public Company getEntity() {
        return entity;
    }


    public void setEntity(Company entity) {
        this.entity = entity;
    }


    public Class<Company> getEntityClazz() {
        return entityClazz;
    }


    public void setEntityClazz(Class<Company> entityClazz) {
        this.entityClazz = entityClazz;
    }


    public CompanyAccountFund getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(CompanyAccountFund companyAccount) {
        this.companyAccount = companyAccount;
    }
}
