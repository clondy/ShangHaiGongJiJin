package com.capinfo.accumulation.parameter.collection;

import com.capinfo.accumulation.model.collection.CompanyAccountFund;
import com.capinfo.accumulation.model.collection.Payininfo;
import com.capinfo.framework.parameter.DataListParameter;

import java.util.Date;

/**
 * Created by wcj on 2017/10/24.
 */
public class PayininfoParameter extends DataListParameter<Payininfo> {
    private Payininfo entity=new Payininfo();

    private Class<Payininfo> entityClazz = Payininfo.class;

    private CompanyAccountFund companyAccount;

    public String getCompanyAccot() {
        return companyAccot;
    }

    public void setCompanyAccot(String companyAccot) {
        this.companyAccot = companyAccot;
    }

    private String companyAccot;

    private Long verificationOptions;

    public Long getVerificationOptions() {
        return verificationOptions;
    }

    public void setVerificationOptions(Long verificationOptions) {
        this.verificationOptions = verificationOptions;
    }

    public CompanyAccountFund getCompanyAccount() {
        return companyAccount;
    }

    public void setCompanyAccount(CompanyAccountFund companyAccount) {
        this.companyAccount = companyAccount;
    }

    private String  startPaymentDate;

    private String  endPaymentDate;

    private Date startReceivedDate;

    private Date endReceivedDate;

    private Date remitDate;

    public Date getRemitDate() {
        return remitDate;
    }

    public void setRemitDate(Date remitDate) {
        this.remitDate = remitDate;
    }

    public String  getStartPaymentDate() {
        return startPaymentDate;
    }

    public void setStartPaymentDate(String startPaymentDate) {
        this.startPaymentDate = startPaymentDate;
    }

    public String  getEndPaymentDate() {
        return endPaymentDate;
    }

    public void setEndPaymentDate(String endPaymentDate) {
        this.endPaymentDate = endPaymentDate;
    }

    public Date getStartReceivedDate() {
        return startReceivedDate;
    }

    public void setStartReceivedDate(Date startReceivedDate) {
        this.startReceivedDate = startReceivedDate;
    }

    public Date getEndReceivedDate() {
        return endReceivedDate;
    }

    public void setEndReceivedDate(Date endReceivedDate) {
        this.endReceivedDate = endReceivedDate;
    }


    public Class<Payininfo> getEntityClazz() {
        return entityClazz;
    }


    public void setEntityClazz(Class<Payininfo> entityClazz) {
        this.entityClazz = entityClazz;
    }


    public Payininfo getEntity() {
        return entity;
    }


    public void setEntity(Payininfo entity) {
        this.entity = entity;
    }


}
