package com.capinfo.commons.project.parameter.dictionary;

import com.capinfo.commons.project.model.dictionary.DictionarySort;
import com.capinfo.framework.parameter.DataListParameter;


public class DictionarySortParameter extends DataListParameter<DictionarySort>{
	
	private DictionarySort entity = new DictionarySort();
	
	private Class<DictionarySort> entityClazz = DictionarySort.class;

	public DictionarySort getEntity() {
		return entity;
	}

	public void setEntity(DictionarySort entity) {
		this.entity = entity;
	}

	public Class<DictionarySort> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClass(Class<DictionarySort> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
	
}
