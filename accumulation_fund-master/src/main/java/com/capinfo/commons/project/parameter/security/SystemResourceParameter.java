package com.capinfo.commons.project.parameter.security;

import com.capinfo.commons.project.model.security.SystemResource;
import com.capinfo.framework.parameter.DataListParameter;

public class SystemResourceParameter extends DataListParameter<SystemResource> {
	
	private SystemResource entity = new SystemResource();

	private Class<SystemResource> entityClazz = SystemResource.class;

	private Long[] roleIds = new Long[0];
	
	private Long currentId;
	private Long targetId;
	private String position;
	
	private String contextPath;
	
	private boolean allSubResourceHasParentRole;
	
	public SystemResource getEntity() {
		return entity;
	}

	public void setEntity(SystemResource entity) {
		this.entity = entity;
	}

	public Class<SystemResource> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClazz(Class<SystemResource> entityClazz) {
		this.entityClazz = entityClazz;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public Long getCurrentId() {
		return currentId;
	}

	public void setCurrentId(Long currentId) {
		this.currentId = currentId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public boolean isAllSubResourceHasParentRole() {
		return allSubResourceHasParentRole;
	}

	public void setAllSubResourceHasParentRole(boolean allSubResourceHasParentRole) {
		this.allSubResourceHasParentRole = allSubResourceHasParentRole;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
}
