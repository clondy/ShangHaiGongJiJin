package com.capinfo.commons.project.model.dictionary;

import com.capinfo.framework.model.BaseEntity;
import com.capinfo.framework.model.StatusEnabled;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * 字典分类，对应多个字典项
 * </p>
 * 
 * @version 1.0
 * @author
 */
@Entity
@Table(name = "DICTIONARY_SORT")
@SequenceGenerator(name = "seqDictionarySort", sequenceName = "SEQ_DICTIONARY_SORT", allocationSize = 1)
public class DictionarySort implements com.capinfo.framework.model.dictionary.DictionarySort<Dictionary>, StatusEnabled, BaseEntity {

	// ID
	private Long id;

	// 字典分类的名称。
	private String name;

	// 字典分类的描述。
	private String description;
	
	// 创建时间
	private Date createTime = new Date();

	// 是否可用 1-可用 0-不可用。
	private Boolean enabled = true;

	// 关联字典表。
	private Set<Dictionary> dictionaries = new HashSet<Dictionary>();

	public DictionarySort() {
	}

	public DictionarySort(Long id) {
		this.id = id;
	}

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqDictionarySort")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "TITLE", length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "DESCRIPTION", length = 1000)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy="sort", targetEntity=Dictionary.class)
	@OrderBy(value="position asc")
	@Where(clause="enabled=1")
	public Set<Dictionary> getDictionaries() {
		return Collections.unmodifiableSet(this.dictionaries);
	}

	public void setDictionaries(Set<Dictionary> dictionaries) {
		this.dictionaries = dictionaries;
	}

	public void addDictionaryItem(Dictionary dictionary) {
		if (dictionary == null) {
			throw new IllegalArgumentException("数据字典项目分组不能为空");
		}
		if (dictionary.getSort() != null) {
			// 数据字典项目分组不能重新绑定到其他数据字典项目中
			StringBuffer error = new StringBuffer().append("数据字典项目")
					.append(dictionary.getSort().getName())
					.append("中已存在相应的分组名称：").append(dictionary.getName());
			throw new IllegalArgumentException(error.toString());
		}
		dictionary.setSort(this);
		this.dictionaries.add(dictionary);
	}

	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "ENABLED")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String toString() {
		return new ToStringBuilder(this).append("id", getId())
				.append("name", getName())
				.append("description", getDescription()).toString();
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if (!(other instanceof DictionarySort))
			return false;
		DictionarySort otherDataDictionary = (DictionarySort) other;
		return new EqualsBuilder().append(this.getName(),
				otherDataDictionary.getName()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getName()).toHashCode();
	}

}