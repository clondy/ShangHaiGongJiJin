package com.capinfo.commons.project.service.security.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.capinfo.commons.project.model.security.SystemViewStamp;
import com.capinfo.commons.project.parameter.security.SystemViewStampParameter;
import com.capinfo.commons.project.service.security.SystemViewStampService;
import com.capinfo.components.security.cache.manager.SecurityCacheManager;

public class SecurityViewStampServiceImpl extends SystemViewStampServiceImpl {
	private static final Log logger = LogFactory.getLog(SystemViewStampService.class);

	private SecurityCacheManager securityCacheManager;
	
	public SecurityCacheManager getSecurityCacheManager() {
		
		return securityCacheManager;
	}

	public void setSecurityCacheManager(SecurityCacheManager securityCacheManager) {
		
		this.securityCacheManager = securityCacheManager;
	}
	
	public SystemViewStamp saveOrUpdate(SystemViewStampParameter parameter) {

		CacheParameter cacheParameter = super.getCacheParameter(parameter);
		SystemViewStamp newEntity = cacheParameter.getNewEntity();
		if(null != newEntity){
			
			securityCacheManager.modifyViewStampInCache(newEntity, cacheParameter.getOriginalName(), cacheParameter.getOriginalRoles());
		}
		return newEntity;
	}
	
	@Transactional
	public boolean delete(SystemViewStampParameter parameter) {
		
		SystemViewStamp systemViewStamp = getObjectById(parameter);
		
		boolean token = super.delete(parameter);
		
		if( ( null != systemViewStamp )&&token ){
			
			securityCacheManager.removeViewStampFromCache(systemViewStamp);
		}
		
		return token;
	}
	
}
