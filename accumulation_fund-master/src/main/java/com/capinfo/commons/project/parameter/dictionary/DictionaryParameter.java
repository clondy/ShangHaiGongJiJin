package com.capinfo.commons.project.parameter.dictionary;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.framework.parameter.DataListParameter;


public class DictionaryParameter extends DataListParameter<Dictionary>{
	
	private Dictionary entity = new Dictionary();
	
	private Class<Dictionary> entityClazz = Dictionary.class;

	public Dictionary getEntity() {
		return entity;
	}

	public void setEntity(Dictionary entity) {
		this.entity = entity;
	}

	public Class<Dictionary> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClass(Class<Dictionary> entityClazz) {
		this.entityClazz = entityClazz;
	}
	
}
