package com.capinfo.commons.project.service.security;

import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.commons.project.parameter.security.SystemUserParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2013-8-29 
 * <p>功能描述:   系统用户Service</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface SystemUserService extends CommonsDataOperationService<SystemUser, SystemUserParameter> {

	/**
	 * <p>描述: 通过用户id获得用户</p>
	 * @param id
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public SystemUser getSystemUserById(Long id);
	
	/**
	 * <p>描述: 验证登录名是否已存在</p>
	 * @param parameter
	 * @return 
	 * @author: xuxianping
	 * @update:
	 */
	public boolean validatorLogonNameExists(SystemUserParameter parameter);

	
	/**
	 * <p>描述: 修改密码</p>
	 * @param parameter
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public boolean modifyPassword(SystemUserParameter parameter);
	
	
	/**
	 * <p>描述: 通过登录名查询用户</p>
	 * @param logonName
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public SystemUser getSystemUserByLogonName(String logonName);
	

	/**
	 * <p> 验证用户名密码是否正确</p>
	 * @param logonName
	 * @param password
	 * @return
	 * @author:	xuxianping 
	 * @update:
	 */
	public boolean validateLogonNameAndPassword(String logonName, String password);
	
	/**
	 * <p> 验证用户名密码是否正确</p>
	 * @param logonName
	 * @param password
	 * @return
	 * @author:	xuxianping 
	 * @update:
	 */
	public SystemUser getSystemUserByLogonNameAndPassword(String logonName, String password);
	
	/**
	 * <p> 修改密码 </p>
	 * @param logonName
	 * @param originalPassword
	 * @param newPassword
	 * @return
	 * @author:	xuxianping 
	 * @update:
	 */
	public boolean modifyPassword(String logonName, String originalPassword, String newPassword);
}