package com.capinfo.commons.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.capinfo.framework.model.BaseEntity;

/**
 * <p>
 * 应用可用资源的声明表，用来暴露应用中的功能
 * </p>
 * 
 * @author
 * 
 * 
 */
@Entity
@Table(name="SYSTEM_CONFIGS")
@SequenceGenerator(name="seqSystemConfig",sequenceName="SEQ_SYSTEM_CONFIGS", allocationSize=1)
public class SystemConfig implements BaseEntity {
	
	private Long rootId = Long.valueOf(1);

	private Long id;
	
	private String key ;

	private String value ;
	
	@Column(name="KEY", length=100)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Transient
	public Long getRootId() {
		return rootId;
	}

	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}
	
	@Id
	@Column(name="ID", unique=true, nullable=false, precision=12, scale=0)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqSystemConfig")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="VALUE", length=3000)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	

	public boolean equals(Object other) {

		if ((this == other))
			return true;
		if (!(other instanceof SystemConfig))
			return false;

		SystemConfig castOther = (SystemConfig) other;
		return new EqualsBuilder().append(this.getId(), castOther.getId())
		// .append(this.getName(), castOther.getName())
				.isEquals();
	}

	public int hashCode() {

		return new HashCodeBuilder().append(getId())
		// .append(getName())
				.toHashCode();
	}
	
	@Override
	public String toString() {
		
		return "id : " + id + ",key :" + key + ",value :" + value; 
	}

}