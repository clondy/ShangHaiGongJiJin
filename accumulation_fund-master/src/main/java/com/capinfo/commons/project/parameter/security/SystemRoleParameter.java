package com.capinfo.commons.project.parameter.security;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.framework.parameter.DataListParameter;

public class SystemRoleParameter extends DataListParameter<SystemRole> {
	
	private SystemRole entity = new SystemRole();

	private Class<SystemRole> entityClazz = SystemRole.class;
	
	private String resourceIdString;

	public SystemRole getEntity() {
		return entity;
	}

	public void setEntity(SystemRole entity) {
		this.entity = entity;
	}

	public Class<SystemRole> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClazz(Class<SystemRole> entityClazz) {
		this.entityClazz = entityClazz;
	}

	public String getResourceIdString() {
		return resourceIdString;
	}

	public void setResourceIdString(String resourceIdString) {
		this.resourceIdString = resourceIdString;
	}

}
