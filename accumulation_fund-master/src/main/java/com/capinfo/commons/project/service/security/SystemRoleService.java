package com.capinfo.commons.project.service.security;

import java.util.List;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.parameter.security.SystemRoleParameter;
import com.capinfo.commons.project.parameter.security.SystemUserParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2013-8-29 
 * <p>功能描述:  系统角色Service接口</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface SystemRoleService extends CommonsDataOperationService<SystemRole,SystemRoleParameter> {
	
	/**
	 * <p>描述: 获得所有的角色</p>
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public List<SystemRole> getAllRoles();
	
	/**
	 * <p>描述: 验证角色名称是否已存在</p>
	 * @param parameter
	 * @return 
	 * @author: xuxianping
	 * @update:
	 */
	public boolean validatorNameExists(SystemRoleParameter parameter);

}
