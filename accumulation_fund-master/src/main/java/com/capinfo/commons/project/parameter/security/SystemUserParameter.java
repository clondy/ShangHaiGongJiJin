package com.capinfo.commons.project.parameter.security;

import com.capinfo.commons.project.model.security.SystemUser;
import com.capinfo.framework.parameter.DataListParameter;

public class SystemUserParameter extends DataListParameter<SystemUser> {

	private SystemUser entity = new SystemUser();
	
	private Class<SystemUser> entityClazz = SystemUser.class;
	
	private Long[] roleIds;

	private Long district;//= ManageOrganization.ROOT_ID;

	private Long street = null;
	
	private Long city = null;
	
	private String startDate;
	
	private String endDate;
	
	private String specifiedScopeIds;
	
	private Boolean isProvider = false;
	
	/**
	 * 原始密码
	 */
	private String originalPassword;
	
	/**
	 * 新密码
	 */
	private String newPassword;
	
	/**
	 * 确认密码
	 */
	private String confirmPassword;

	public String getSpecifiedScopeIds() {
		return specifiedScopeIds;
	}

	public void setSpecifiedScopeIds(String specifiedScopeIds) {
		this.specifiedScopeIds = specifiedScopeIds;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public SystemUser getEntity() {
		return entity;
	}

	public void setEntity(SystemUser entity) {
		this.entity = entity;
	}

	public Class<SystemUser> getEntityClazz() {
		return entityClazz;
	}

	public void setEntityClazz(Class<SystemUser> entityClazz) {
		this.entityClazz = entityClazz;
	}

	public Long getDistrict() {
		return district;
	}

	public void setDistrict(Long district) {
		this.district = district;
	}

	public Long getStreet() {
		return street;
	}
	
	public Long getCity() {
		if( null == street ) {
			if(null == district){
				return city;
			}else{
				return district;
			}
		}else{
			return street;
		}
	}
	
	public void setCity(Long city) {
		this.city = city;
	}

	public void setStreet(Long street) {
		this.street = street;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsProvider() {
		return isProvider;
	}

	public void setIsProvider(Boolean isProvider) {
		this.isProvider = isProvider;
	}

	public String getOriginalPassword() {
		return originalPassword;
	}

	public void setOriginalPassword(String originalPassword) {
		this.originalPassword = originalPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
