package com.capinfo.commons.project.service.dictionary.impl;

import com.capinfo.commons.project.model.dictionary.DictionarySort;
import com.capinfo.commons.project.parameter.dictionary.DictionarySortParameter;
import com.capinfo.commons.project.service.dictionary.DictionarySortService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:  字典分类管理service实现</p>
 * @author  xuxianping
 * @version 0.1
 */
public class DictionarySortServiceImpl extends CommonsDataOperationServiceImpl<DictionarySort, DictionarySortParameter> implements DictionarySortService{

	@Override
	public SearchCriteriaBuilder<DictionarySort> getSearchCriteriaBuilder(DictionarySortParameter parameter) {
		
		SearchCriteriaBuilder<DictionarySort> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
		searchCriteriaBuilder.addQueryCondition("name", RestrictionExpression.LIKE_OP, parameter.getEntity().getName());
		
		return searchCriteriaBuilder;
	}
	
}
