package com.capinfo.commons.project.model.dictionary;

import com.capinfo.framework.model.BaseEntity;
import com.capinfo.framework.model.StatusEnabled;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;

/**
 * <p>
 * 字典项，真正与数据表进行关联的字典表
 * </p>
 * 
 * @version 1.0
 * @author
 */
@Entity
@Table(name = "DICTIONARIES")
@SequenceGenerator(name = "seqDictionary", sequenceName = "SEQ_DICTIONARIES", allocationSize = 1)
public class Dictionary implements com.capinfo.framework.model.dictionary.Dictionary<DictionarySort>, StatusEnabled, BaseEntity, Comparable<Dictionary> {

	// ID
	private Long id;

	// 字典项显示的名称
	private String name;

	// 字典项中的标准（国标）代码
	private String code;

	// 字典项排列显示的位置
	private Integer position;

	// 字典项的描述
	private String description;

	// 创建时间
	private Date createTime = new Date();

	// 是否可用 1-可用 0-不可用
	private Boolean enabled = true;

	// 字典分类
	private Long sortId;
	private DictionarySort sort;

	public Dictionary() {
	}

	public Dictionary(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqDictionary")
	@Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "TITLE", length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STANDARD_CODE", length = 300)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "POSITION", precision = 3, scale = 0)
	public Integer getPosition() {
		return this.position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	@Column(name = "DESCRIPTION", length = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "SORT_ID")
	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	@ManyToOne(targetEntity = DictionarySort.class)
	@JoinColumn(name = "SORT_ID", insertable = false, updatable = false)
	public DictionarySort getSort() {
		return this.sort;
	}

	public void setSort(DictionarySort sort) {
		this.sort = sort;
	}
	
	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "ENABLED")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String toString() {
		return new ToStringBuilder(this).append("id", getId())
				.append("code", getCode()).append("name", this.getName())
				.toString();
	}

	public boolean equals(Object other) {
		if ((this == other)) {

			return true;
		}
		if (!(other instanceof Dictionary)) {

			return false;
		}

		Dictionary otherDictionaryItem = (Dictionary) other;
		return new EqualsBuilder().append(this.getName(),
				otherDictionaryItem.getName()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getName()).toHashCode();
	}

	public int compareTo(Dictionary object) {
		// if ( !(other instanceof DictionaryImpl) ) return false;
		return new CompareToBuilder().append(this.getPosition(),
				object.getPosition()).toComparison();
	}

}