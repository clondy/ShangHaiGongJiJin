package com.capinfo.commons.project.service.dictionary.impl;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.parameter.dictionary.DictionaryParameter;
import com.capinfo.commons.project.service.dictionary.DictionaryService;
import com.capinfo.framework.dao.SearchCriteriaBuilder;
import com.capinfo.framework.dao.impl.restriction.RestrictionExpression;
import com.capinfo.framework.service.impl.CommonsDataOperationServiceImpl;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:  字典管理service实现</p>
 * @author  xuxianping
 * @version 0.1
 */
public class DictionaryServiceImpl extends CommonsDataOperationServiceImpl<Dictionary, DictionaryParameter> implements DictionaryService{

	@Override
	public SearchCriteriaBuilder<Dictionary> getSearchCriteriaBuilder(DictionaryParameter parameter) {
		
		SearchCriteriaBuilder<Dictionary> searchCriteriaBuilder = super.getSearchCriteriaBuilder(parameter);
		
		searchCriteriaBuilder.addQueryCondition("name", RestrictionExpression.LIKE_OP, parameter.getEntity().getName());
		searchCriteriaBuilder.addQueryCondition("sortId", RestrictionExpression.EQUALS_OP, parameter.getEntity().getSortId());
		
		return searchCriteriaBuilder;
	}
	
}
