package com.capinfo.commons.project.model.security;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;

import com.capinfo.components.security.model.SecureViewStamp;
import com.capinfo.framework.model.StatusEnabled;

@Entity
@Table(name="SYSTEM_VIEW_STAMPS")
@SequenceGenerator(name="seqSystemViewStamps",sequenceName="SEQ_SYSTEM_VIEW_STAMPS", allocationSize=1)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SystemViewStamp implements  StatusEnabled, SecureViewStamp<SystemRole, SystemResource>, Serializable {
	 
	//ID
    private Long id;
    
    //名称
    private String name;
    
    //角色的描述
    private String description;
    
    //是否可用 1-可用 0-不可用
    private Boolean enabled = Boolean.TRUE;
    
    //资源
    private Set<SystemResource> resources = new HashSet<SystemResource>();
  
    //角色
  	private Set<SystemRole> roles = new HashSet<SystemRole>();


  	
  	
    public SystemViewStamp() {
	}

	@Id	
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="seqSystemViewStamps") 
    @Column(name="ID", unique=true, nullable=false, precision=12, scale=0)
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 名称
	 */
	@Column(name="NAME", length=30)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 角色的描述
	 */
	@Column(name="DESCRIPTION", length=300)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 是否有效
	 */
	@Column(name="ENABLED", precision=1, scale=0)
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * 资源
	 */
	@OneToMany(targetEntity = SystemResource.class, mappedBy = "viewStamp", fetch = FetchType.LAZY)
	public Set<SystemResource> getResources() {
		return resources;
	}

	public void setResources(Set<SystemResource> resources) {
		this.resources = resources;
	}
	
	/**
	 * 角色
	 */
	@ManyToMany(targetEntity=SystemRole.class, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="SYSTEM_ROLES_VIEWSTAMPS", joinColumns={@JoinColumn(name="VIEW_STAMP_ID")},inverseJoinColumns={@JoinColumn(name="ROLE_ID")})
	@Where( clause = "ENABLED = 1")
	public Set<SystemRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<SystemRole> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		
		return new ToStringBuilder(this).append("id" , getId())
										.append("name" ,this.getName())
										.append("description",this.getDescription()).toString(); 
	}
}
