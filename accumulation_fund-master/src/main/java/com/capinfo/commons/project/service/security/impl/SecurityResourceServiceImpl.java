package com.capinfo.commons.project.service.security.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.capinfo.commons.project.model.security.SystemResource;
import com.capinfo.commons.project.parameter.security.SystemResourceParameter;
import com.capinfo.components.security.cache.manager.SecurityCacheManager;


/**
 * 
 * @author dinglei
 *
 */
@Transactional(transactionManager = "transactionManager4Ecomm", propagation = Propagation.REQUIRED)
public class SecurityResourceServiceImpl extends SystemResourceServiceImpl {
	
	private SecurityCacheManager securityCacheManager;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public SystemResource saveOrUpdate(SystemResourceParameter parameter) {

		CacheParameter cacheParameter = super.getCacheParameter(parameter);
		SystemResource newEntity = cacheParameter.getNewEntity();
		if(null != newEntity){
			getSecurityCacheManager().modifyResourceInCache(newEntity, cacheParameter.getResourceValue(), cacheParameter.getOriginalViewStamp());
		}
		return newEntity;
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean delete(SystemResourceParameter parameter) {
		SystemResource resource = this.deleteResource(parameter);
		if(null != resource){
			
			String originalValue = resource.getValue();

			securityCacheManager.removeResourceFromCache(originalValue);
			return true;
		}
		return false;
	}
	public SecurityCacheManager getSecurityCacheManager() {
		return securityCacheManager;
	}

	public void setSecurityCacheManager(SecurityCacheManager securityCacheManager) {
		this.securityCacheManager = securityCacheManager;
	}
}
