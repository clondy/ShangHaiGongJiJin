package com.capinfo.commons.project.parameter.region;

import com.capinfo.commons.project.model.region.Region;
import com.capinfo.framework.parameter.DataListParameter;

/**
 * @author zhangxianwei
 *
 */
public class RegionParameter extends DataListParameter<Region> {
	
	private Region entity = new Region();
	
	private Class<Region> entityClazz = Region.class;
	
	private String writeReturnIdString;//选择区域页面（choose_region_tree.jsp），选择操作时，需回写id的标签ID
	
	private String writeReturnNameString;//选择区域页面（choose_region_tree.jsp），选择操作时，需回写name的标签ID
	
	private String selectedIdsString;	//已选择的区域ids
	
	public Region getEntity() {
		return entity;
	}
	public void setEntity(Region entity) {
		this.entity = entity;
	}
	public Class<Region> getEntityClazz() {
		return entityClazz;
	}
	public void setEntityClazz(Class<Region> entityClazz) {
		this.entityClazz = entityClazz;
	}
	public String getWriteReturnIdString() {
		return writeReturnIdString;
	}
	public void setWriteReturnIdString(String writeReturnIdString) {
		this.writeReturnIdString = writeReturnIdString;
	}
	public String getWriteReturnNameString() {
		return writeReturnNameString;
	}
	public void setWriteReturnNameString(String writeReturnNameString) {
		this.writeReturnNameString = writeReturnNameString;
	}
	public String getSelectedIdsString() {
		return selectedIdsString;
	}
	public void setSelectedIdsString(String selectedIdsString) {
		this.selectedIdsString = selectedIdsString;
	}
	
}
