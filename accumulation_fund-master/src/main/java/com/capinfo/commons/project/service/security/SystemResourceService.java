package com.capinfo.commons.project.service.security;

import java.util.List;

import com.capinfo.commons.project.model.security.SystemResource;
import com.capinfo.commons.project.parameter.security.SystemResourceParameter;
import com.capinfo.framework.service.CommonsDataOperationService;

public interface SystemResourceService extends CommonsDataOperationService<SystemResource, SystemResourceParameter> {
	
	public SystemResource getResourceById(Long id);
	
	public boolean validateResourceExists(SystemResourceParameter parameter);
	
	public byte[] retrieveResource(SystemResourceParameter parameter) ;
	
	public boolean delete(SystemResourceParameter parameter);
	
	public boolean move(SystemResourceParameter parameter);
	
	/**
	 * <p>
	 * 将resource对象，从root开始，按照层级结构生成json字符串。
	 * </p>
	 * 
	 * @param parameter
	 * @return	resource对象的json字符串。
	 */
	public String buildResourceJson();

	public boolean rebuildResourceToDataBase(SystemResourceParameter parameter);
	
	/**
	 * <p>描述: 返回所有作为分类菜单的资源</p>
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public List<SystemResource> getCategoryMenus();
	
	
	/**
	 * <p>描述: 返回可显示的子资源</p>
	 * @param parentId
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public List<SystemResource> getDisplayChildren(Long parentId);
}
