package com.capinfo.commons.project.service.dictionary;

import com.capinfo.commons.project.model.dictionary.DictionarySort;
import com.capinfo.commons.project.parameter.dictionary.DictionarySortParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:   字典分类管理service接口</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface DictionarySortService extends CommonsDataOperationService<DictionarySort, DictionarySortParameter>{
	
		
}
