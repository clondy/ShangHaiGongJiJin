package com.capinfo.commons.project.parameter.security;

import com.capinfo.commons.project.model.security.SystemViewStamp;
import com.capinfo.framework.parameter.DataListParameter;

public class SystemViewStampParameter extends DataListParameter<SystemViewStamp> {
	
	private SystemViewStamp entity = new SystemViewStamp();

	private Class<SystemViewStamp> entityClazz = SystemViewStamp.class;
	
	private Long[] roleIds;
	
	public SystemViewStamp getEntity() {
		return entity;
	}

	public void setEntity(SystemViewStamp entity) {
		this.entity = entity;
	}

	public Class<SystemViewStamp> getEntityClazz() {
		return entityClazz;
	}

	public Long[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}

	public void setEntityClazz(Class<SystemViewStamp> entityClazz) {
		this.entityClazz = entityClazz;
	}
}
