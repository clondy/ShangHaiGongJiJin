package com.capinfo.commons.project.service.security.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.capinfo.commons.project.model.security.SystemRole;
import com.capinfo.commons.project.parameter.security.SystemRoleParameter;
import com.capinfo.commons.project.service.security.SystemRoleService;
import com.capinfo.components.security.cache.manager.SecurityCacheManager;

public class SecurityRoleServiceImpl extends SystemRoleServiceImpl {
	
	private static final Log logger = LogFactory.getLog(SystemRoleService.class);

	private SecurityCacheManager securityCacheManager;
	
	@Override
	public SystemRole saveOrUpdate(SystemRoleParameter parameter) {
		
		CacheParameter cacheParameter = super.getCacheParameter(parameter);
		SystemRole newEntity = cacheParameter.getNewEntity();
		
		// 4. 修改成功后的载入缓存
		if( null != newEntity ) {
			
			this.getSecurityCacheManager().modifyRoleInCache(newEntity, cacheParameter.getOriginalResources(), cacheParameter.getOriginalRoleName());
		}
		
		return newEntity;
	}
	

	@Override
	public boolean delete(SystemRoleParameter parameter) {
		
		SystemRole systemRole = getObjectById(parameter);
		
		boolean token = super.delete(parameter);
		
		// 角色从缓存中删除
		if( ( null != systemRole )&&token ){
			
			this.getSecurityCacheManager().removeRoleFromCache(systemRole);
		}
		
		return token;
	}



	public SecurityCacheManager getSecurityCacheManager() {
		return securityCacheManager;
	}

	public void setSecurityCacheManager(SecurityCacheManager securityCacheManager) {
		this.securityCacheManager = securityCacheManager;
	}
	
}
