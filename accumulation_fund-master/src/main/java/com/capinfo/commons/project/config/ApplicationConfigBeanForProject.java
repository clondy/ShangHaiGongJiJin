package com.capinfo.commons.project.config;

import com.capinfo.framework.config.ApplicationConfigBean;


/**
 * 作成日期： 2013-12-31 
 * <p>功能描述:  为本应用提供的可配参数的类 </p>
 * @author  xuxianping
 * @version 0.1
 */
public class ApplicationConfigBeanForProject extends ApplicationConfigBean {
	
	// 凭证条存活时间（秒）
	private int tokenSignatureLifetimeSeconds = 60;
	
	// 凭证条密钥
	private String tokenSignatureKey = "capinfo-ecommunity-app-remember-me";

	private String regionClassName = "";
	
	public int getTokenSignatureLifetimeSeconds() {
		return tokenSignatureLifetimeSeconds;
	}

	public void setTokenSignatureLifetimeSeconds(int tokenSignatureLifetimeSeconds) {
		this.tokenSignatureLifetimeSeconds = tokenSignatureLifetimeSeconds;
	}

	public String getTokenSignatureKey() {
		return tokenSignatureKey;
	}

	public void setTokenSignatureKey(String tokenSignatureKey) {
		this.tokenSignatureKey = tokenSignatureKey;
	}

	public String getRegionClassName() {
		return regionClassName;
	}

	public void setRegionClassName(String regionClassName) {
		this.regionClassName = regionClassName;
	}
}
