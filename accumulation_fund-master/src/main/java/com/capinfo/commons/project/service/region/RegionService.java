package com.capinfo.commons.project.service.region;


import com.capinfo.commons.project.model.region.Region;
import com.capinfo.commons.project.parameter.region.RegionParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2014-1-20 
 * <p>功能描述:   区域service</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface RegionService extends	CommonsDataOperationService<Region, RegionParameter> {
	
	
	/**
	 * <p>描述: 根据区域父ID获得下级区域的json字符串</p>
	 * @param parentId 父Id
	 * @return 返回下级区域的json字符串，若没有下级则返回null
	 * @author: xuxianping
	 * @update:
	 */
	public String getChildrenJson(Long parentId);
	
	
	/**
	 * <p>描述: 通过id获得区域</p>
	 * @param id 区域
	 * @return 返回指定id的区域，若没有区域则返回null
	 * @author: xuxianping
	 * @update:
	 */
	public Region getRegionById(Long id);
	
	
	/**
	 * <p>描述: 获取指定区域级联上级数据id</p>
	 * @param id 指定区域id
	 * @return 
	 * @author: xuxianping
	 * @update:
	 */
	public String getRegionIds(Long id);
	
	/**
	 * <p>描述: 获取区域数据JSON</p>
	 * @return 区域数据json
	 * @author: xuxianping
	 * @update:
	 */
	public String getRegionJson();
	
	
	/**
	 * <p>描述: 生成区域数据JSON</p>
	 * @param parameter
	 * @return 
	 * @author: xuxianping
	 * @update:
	 */
	public boolean buildRegionJson(RegionParameter parameter);
}
