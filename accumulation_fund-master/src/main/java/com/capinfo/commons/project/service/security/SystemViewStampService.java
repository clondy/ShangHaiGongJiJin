package com.capinfo.commons.project.service.security;

import java.util.List;

import com.capinfo.commons.project.model.security.SystemViewStamp;
import com.capinfo.commons.project.parameter.security.SystemViewStampParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2014-5-12 
 * <p>功能描述:  视图标记Service接口</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface SystemViewStampService extends CommonsDataOperationService<SystemViewStamp, SystemViewStampParameter> {
	
	
	/**
	 * <p>描述: 获取所有视图标记</p>
	 * @return
	 * @author: xuxianping
	 * @update:
	 */
	public List<SystemViewStamp> getAllViewStamps();
	
	/**
	 * <p>描述: 验证角色名称是否已存在</p>
	 * @param parameter
	 * @return 
	 * @author: 张名扬
	 * @update:
	 */
	public boolean validatorNameExists(SystemViewStampParameter parameter);
}
