package com.capinfo.commons.project.service.dictionary;

import com.capinfo.commons.project.model.dictionary.Dictionary;
import com.capinfo.commons.project.parameter.dictionary.DictionaryParameter;
import com.capinfo.framework.service.CommonsDataOperationService;


/**
 * 作成日期： 2013-12-23 
 * <p>功能描述:  字典管理service接口</p>
 * @author  xuxianping
 * @version 0.1
 */
public interface DictionaryService extends CommonsDataOperationService<Dictionary, DictionaryParameter>{
	
		
}
