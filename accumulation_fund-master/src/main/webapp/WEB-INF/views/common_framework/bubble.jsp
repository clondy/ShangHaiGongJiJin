<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type='text/javascript' src="<c:url value="/js/jquery/jquery.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/css/bslide.css" />"></link>
<div
	style="position: fixed; left: 0pt; right: 0pt; width: 100%; height: 0pt; z-index: 8675309; overflow: visible; bottom: 0pt;">
	<div id="simplereach-slide-element"
		style="position: absolute; display: none; right: -30px; width: 430px; height: 120px; bottom: 0pt;">
		<div id="simplereach-slide-header">
			<div id="simplereach-slide-title" style="display: block;">通知提示</div>
			<!-- <div id="simplereach-slide-help-header" style="display: none;">OPTIONS</div> -->
		</div>
		<div id="simplereach-slide-content" style="display: block;height:100px;">
			<div id="icon-link" style="display: block;"><img id="simplereach-slide-image" src="<c:url value="/images/bubble.png"/>"></div>
			<div id="simplereach-slide-link">尊敬的用户，您有<span id="unimport_amount" style="color:#c30"></span>条未读通知！是否查看？</div>
			<div id="simplereach-slide-btn" style="text-align: center;">
				<button style="text-align: center;" id="reading_btn">
					<span>查看</span>
				</button>
				<button style="text-align: center;" id="exit_btn">
					<span>退出</span>
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">     
dojo.require("dojox.widget.DialogSimple");

//上一次弹出时得到的值
var previousValue = null;
//创建定时时间
var timing = 120000;

function showBubble(){
	var url = '<c:url value="/business/notification_reading/notification_bubble.shtml"/>';
	 $.get(url, function(result){
		 if(result!=""){
		 	var data = result.split(",");
		 	var time = data[0];
		 	var count = data[1];
		 	var canShow = data[2];
		    if(time!=previousValue&&canShow=="yes"){
		    	previousValue=time;
		    	$("#unimport_amount").html(count);
		    	$("#simplereach-slide-element").slideToggle();
		    }
		 }
		 else{
			 setInterval('showBubble()',timing);
		 }
		  });
	
}

var mDialog=null;
function notifDialog(){
	var url = '<c:url value="/business/notification_reading/notification_dialog.shtml"/>';
	if(mDialog==null){
		mDialog = new dijit.Dialog();
	}
	mDialog.set("title", "通知内容");
	mDialog.set("style", "width:820px");
	mDialog.setHref(url);
	mDialog.show();	

}

$(function() {
	$("#reading_btn").click(function(event) {
		$("#simplereach-slide-element").css("display","none");
		notifDialog();
		setInterval('showBubble()',timing);
	});
	$("#exit_btn").click(function(event) {
		$("#simplereach-slide-element").css("display","none");
		setInterval('showBubble()',timing);
	});
});

 $(document).ready(function() {
   //initBSlide();
   showBubble();
   
 });       
 
 </script>