<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> 

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Base Project</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/js/easyui/themes/default/easyui.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/js/easyui/themes/icon.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/scrollbar.css" />">
	
	<script type="text/javascript" src="<c:url value="/js/easyui/jquery-1.8.0.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/easyui/jquery.easyui.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/easyui/locale/easyui-lang-zh_CN.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/easyui/src/jquery.parser.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/framework/easyui/service/crud/base.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/validate_value.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/validator-idcard.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/calendar/WdatePicker.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/framework/extJquery.js" />"></script>
	<style type="text/css">
		/**改变th的字体*/
		th {
			font-weight: normal;
			color: #0E2D5F;
			height: 20px;
			line-height: 20px;
			width: 85px;
			text-align: center
		}
		
		td {
			padding: 2px;
		}
		
		.form_view_border {
			border-collapse: collapse; /* 让边框合并 */
			border: 1px solid #95B8E7; /*边框1个像素  */
		}
		
		.form_view_input {
			width:184px;
			height: 27px;
		}
		
		.form_view_textarea {
			width: 98%
		}
		
		.div_center {
			width:100%;
			height:100%;
			justify-content: center;
			align-items: center;
			
			display: box;			/* OLD - Android 4.4- */
			display: -webkit-box;	/* OLD - iOS 6-, Safari 3.1-6 */
			display: -moz-box;		/* OLD - Firefox 19- (buggy but mostly works) */
			display: -ms-flexbox;	/* TWEENER - IE 10 */
			display: -webkit-flex;	/* NEW - Chrome */
			display: flex;			/* NEW, Spec - Opera 12.1, Firefox 20+ */
		}
		
		#dark{
			background-color:#333;
			border:1px solid #000;
			padding:10px;
			margin-top:20px;
		}
	
		#light{
			background-color:#FFF;
			border:1px solid #dedede;
			padding:10px;
			margin-top:20px;
			}
	
		dt{ 
			list-style:none;
			padding-top:10px;
			/* padding-bottom:10px; */
			padding-bottom:2px;
		}

		.button, .button:visited {
			background: #222 url(<c:url value="/images/overlay.png" />) repeat-x;
			display: inline-block; 
			padding: 5px 10px 6px; 
			color: #fff; 
			text-decoration: none;
			-moz-border-radius: 6px; 
			-webkit-border-radius: 6px;
			-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
			-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
			text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
			border-bottom: 1px solid rgba(0,0,0,0.25);
			position: relative;
			cursor: pointer
		}
 
		.button:hover							{ background-color: #111; color: #fff; }
		.button:active							{ top: 1px; }
		.small.button, .small.button:visited	{ font-size: 12px;}
		.button, .button:visited,
		.medium.button, .medium.button:visited	{
													font-size: 13px; 
													font-weight: bold; 
													line-height: 1; 
													text-shadow: 0 -1px 1px rgba(0,0,0,0.25); 
													/* color: #000000; */
												}
		.large.button, .large.button:visited 	{ font-size: 14px; 
													padding: 6px 28px 7px; }
		.super.button, .super.button:visited 	{ font-size: 34px; 
													padding: 8px 14px 9px; }
		
		.pink.button, .magenta.button:visited	{ background-color: #e22092; }
		.pink.button:hover						{ background-color: #c81e82; }
		.green.button, .green.button:visited	{ background-color: #91bd09; }
		.green.button:hover						{ background-color: #749a02; }
		.red.button, .red.button:visited		{ background-color: #e62727; }
		.red.button:hover						{ background-color: #cf2525; }
		.orange.button, .orange.button:visited	{ background-color: #ff5c00; }
		.orange.button:hover					{ background-color: #d45500; }
		.blue.button, .blue.button:visited		{ background-color: #95B8E7; }/*#E0ECFF*/
		.blue.button:hover						{ background-color: #2575cf; }
		.yellow.button, .yellow.button:visited	{ background-color: #ffb515; }
		.yellow.button:hover					{ background-color: #fc9200; }
		
	</style>
	<script type="text/javascript">
	
		$(function() {
			
			$.ajaxSetup({
			  contentType: "application/x-www-form-urlencoded; charset=utf-8"
			});
			
			$.extend($.fn.validatebox.defaults.rules, {
				equals: {
					validator: function(value, param) {
						
						return value == $(param[0]).val();
					}
					, message: '两次输入的值不一致，请重新输入！'
				}
				, complexValid : {
				
					/*
					混合验证，四个参数：
						1.第一个是正则表达式，
						2.第二个是错误的提示信息，前两个参数用户合法性验证；
						3.第三个参数是一个ajax验证回调对象的对象名；
							回调对象的结构如下（包括三个方法）：
							{
								getValidateUrl: function() {return '';}
								, getValidateParameter: function() {return {}}
								, validator: function() {return false;}
							}
						4.第四个是ajax验证错误的提示信息。
						5.如果第五个参数存在 正则的匹配结果不取反。
						这四个参数之间既可各自分别验证（只写前两个参数/ 前两个参数为空，第三/ 四个参数有值），也可在四个参数全部有值的情况下复合验证。
					*/
					validator: function(value, param) {

                        var regExp = new RegExp(jQuery.trim(param[0]));
                        var regResult = regExp.test(jQuery.trim(value));
					    if( !param[4] ) {

                            regResult = !regResult;
						}
						if ( regResult ) {
							
							//动态设置message提示信息，complexValid与方法名对应
							$.fn.validatebox.defaults.rules.complexValid.message = param[1];
							return false;
						} else if( 4 == param.length ) {
							
							var validateCallback = eval(param[2]);
							
							var result = $.ajax({
								
								url: validateCallback.getValidateUrl()
								, data: validateCallback.getValidateParameter(value)
								, async: false
								, type: "post"
							}).responseText;
							
							var token = validateCallback.validator(result);
							if( !token ) {

								$.fn.validatebox.defaults.rules.complexValid.message = param[3];
							}
							
							return token;
						}
						
						return true;
					}
					, message : "" 
				}
			});
			
			$('#main_modify_password_form_dialog').dialog({
				
				title: '密码修改'
				, width: 600
				, height: 450
				, closed: true
				, cache: false
				, resizable: true
				, collapsible: true
				, maximizable: true
				, buttons: [{
					text:'保存'
					, iconCls: 'icon-save'
					, handler: function() {
						
						var innerThat = this;
						$.messager.progress();

						$('#system_user_modify_password_form').form('submit', {
							url: '<c:url value="/manage/systemUser/modify_password.shtml"/>'
							, onSubmit: function() {

								var isValid = $(this).form('validate');
								if ( !isValid ){
									
									$.messager.progress('close');
								}
								
								return isValid;
							}
							, success: function(data) {

								data = jQuery.parseJSON('' + data);
								$.messager.progress('close');
								
								if( data ) {

 									alert('密码修改成功');
									
									$($(innerThat).context.parentElement.parentElement).dialog('close');
								} else {

									alert('密码修改失败，请重试。');
								}
							}
							, error: function (error) {alert('系统忙，请稍候再试！');}
						});
					}
				}
				, {
					text:'取消',
					iconCls:'icon-cancel',
					handler:function(){$($(this).context.parentElement.parentElement).dialog('close');}
				}]
				, modal: true
			});
		});
	</script>
</head>

<body class="easyui-layout">

	<div data-options="region:'north', split:true" style="height:107px;padding:10px;" align="right">
		<div class="header">
			<h3><img src="<c:url value="/images/logo-w.png" />" alt=""></h3>
			<h4>上海公积金</h4>
			<dl>
				<dt>
					<img src="<c:url value="/images/hicon01.png" />" alt="">
					欢迎您，<span><c:out value="${eccomm_admin.name}" /></span>
				</dt>
				<dd>
					<a href="javascript:void(0)" onClick="$('#main_modify_password_form_dialog').dialog('refresh', '<c:url value="/manage/systemUser/modify_password_form.shtml?entity.id="/>' + ${eccomm_admin.id}); $('#main_modify_password_form_dialog').dialog('open');"><img src="<c:url value="/images/hicon02.png" />" alt="" />修改密码</a>
					<a href="<c:url value="/j_spring_security_logout"/>"><img src="<c:url value="/images/hicon03.png" />" alt="">退出系统</a>
				</dd>
			</dl>
		</div>
	</div>
	<!-- div data-options="region:'north',split:true" title="North Title" style="height:100px;padding:10px;">
		<p>The north content.</p>
	</div -->
	<!-- div data-options="region:'south',split:true" title="South Title" style="height:100px;padding:10px;background:#efefef;">
		<div class="easyui-layout" data-options="fit:true" style="background:#ccc;">
			<div data-options="region:'center'">sub center</div>
			<div data-options="region:'east',split:true" style="width:200px;">sub center</div>
		</div>
	</div -->
	
	<div data-options="region:'center'" style="overflow:hidden;padding:2px;">
	
		<div id="category_menu_tabs" class="easyui-tabs" data-options="fit:true,border:false">
			<c:forEach items="${categoryMenus}" var="categoryMenu">
				<sec:authorize access="hasAnyRole('${categoryMenu.viewStamp.name}')">
					<div id="${categoryMenu.id}" title="${categoryMenu.name}" style="padding:1px;overflow:hidden;">
						<div class="easyui-layout" data-options="fit: true">
							<div data-options="region: 'west', split: true, href: '<c:url value="/manage/left.shtml?resourceId=${categoryMenu.id}"/>'" title="菜单" style="width:200px;padding1:10px;overflow:hidden;">
							</div>
							<div data-options="region:'center'" style="overflow:hidden;">
								<div id="framework_work_area_tabs_${categoryMenu.id}" class="easyui-tabs" data-options="fit:true,border:false">

								</div>
							</div>
						</div>
					</div>
				</sec:authorize>
			</c:forEach>
		</div>
	</div>
	
	<div id="main_modify_password_form_dialog">Dialog Content.</div>
</body>
</html>