<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> 

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">

<script type="text/javascript">
	
	function loadMenu(url, title){

		if( -1 == url.indexOf('#') ) {

			var tabArea = $('#framework_work_area_tabs_' + $('#category_menu_tabs').tabs('getSelected')[0].id);
			if ( tabArea.tabs('exists', title) ) {
				
				tabArea.tabs('select', title);
			} else {
				
				tabArea.tabs('add', {
					title: title
					, href: url
					, cache: true
					, closable: true
				});
			}
		}
	}
</script>

<div class="easyui-accordion" style="text-align: left;" data-options="fit:true, border:false, nimate:true, lines:true">
	
	<c:forEach var="resource" items="${showMenus}">
		<sec:authorize access="hasAnyRole('${resource.authorities}')">
			<c:if test="${resource.hasChildren}">
			<div title="${resource.name}" style="text-align: left; padding: 10px; overflow: auto;">
				
				<c:forEach var="child" items="${resource.children}">
					
					<sec:authorize access="hasAnyRole('${child.authorities}')">
					
					<c:if test="${child.hasChildren}">
					
					<ul class="easyui-tree" data-options="animate: true">
						<li>
							<span id="span_${child.id}">
								<c:out value="${child.name }" escapeXml="false"/>
							</span>
							<ul>
							<c:forEach var="grandson" items="${child.children}">
							
								<sec:authorize access="hasAnyRole('${grandson.authorities}')">
								<li><div onclick="javascript:loadMenu('<c:url value="${grandson.value}"/>','<c:out value="${grandson.name }" escapeXml="false"/>');">
									<c:out value="${grandson.name }" escapeXml="false"/>
								</div></li>
								</sec:authorize>
							</c:forEach>
							</ul>
						</li>
					</ul>
					</c:if>
					<c:if test="${!child.hasChildren}">
					<ul class="easyui-tree">
						<li><span id="span_${child.id}"><div onclick="javascript:loadMenu('<c:url value="${child.value}"/>','<c:out value="${child.name }" escapeXml="false"/>');">
							<c:out value="${child.name }" escapeXml="false"/>
						</div></span></li>
					</ul>
					</c:if>
					</sec:authorize>
				</c:forEach>
				</ul>
			</div>
			</c:if>
			<c:if test="${!resource.hasChildren}">
			<ul class="easyui-tree">
				<li><span id="span_${resource.id}"><div onclick="javascript:loadMenu('<c:url value="${resource.value}"/>','<c:out value="${resource.name }" escapeXml="false"/>');">
					<c:out value="${resource.name}" escapeXml="false"/>
				</div></span></li>
			</ul>
			</c:if>
		</sec:authorize>
	</c:forEach>
</div>