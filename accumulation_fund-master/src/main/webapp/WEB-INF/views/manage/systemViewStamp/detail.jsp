<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<table width="600px" border="0" align="center" cellpadding="0" cellspacing="0" class="page_tablebg"><tr><td> 
    	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="page_table">
     	 	<tr height="35">
        		<td colspan="4" class="page_tablelist2"><div align="center">系统视图标记信息</div></td>
      		</tr>
        	<tr height="30">
		           <td class="table_gray" width="30%">名称： </td>
		           <td class="page_input">
		           		${SystemViewStamp.name }
		            </td>
		       	</tr>
		       	<tr height="30">
		           <td class="table_gray">
						描述：
					</td>
				 	<td class="page_input">
				 		${SystemViewStamp.description }
		            </td>
	          	</tr>
          <tr>
            <td class="table_gray" width="20%">角色： </td>
            <td class="page_input" colspan="3">
            	<select disabled="disabled" id="userroleIdid" multiple="true" size="5">
            	<c:forEach items="${SystemViewStamp.roles }" var="role">
					<option value="${role.name}"><c:out value="${role.description}" /></option>
            	</c:forEach>
            	</select>
            </td>
            
          </tr>
          
          <tr height="35">
    		<td colspan="4" align="center"> 
    			<div align="center">
					<button  id="close_detail_button" dojoType="dijit.form.Button" onclick="javascript:goBack();;"><span>关闭</span></button>
				</div>
			</td>
          </tr>
          
          
        </table>
      </td>
    </tr>

</table>

<script type="text/javascript">
	function goBack(){
		hideDialog('system_view_stamp_detail_dialog');
	}
</script>
