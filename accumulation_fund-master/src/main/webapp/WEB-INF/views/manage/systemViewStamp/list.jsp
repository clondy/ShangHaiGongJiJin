<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
{
	"total": <c:out value="${DataTotalCount}" />
	, "rows": [
		<c:forEach var="systemViewStamp" items="${SystemViewStampList}" varStatus="status">
		{"id": "${systemViewStamp.id}", "name": "${systemViewStamp.name}", "description": "${systemViewStamp.description}"}<c:if test="${!status.last}">, </c:if>
		</c:forEach>
	]
}