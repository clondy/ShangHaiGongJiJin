<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<script>
	
		var systemViewStamp;
		$(function() {
			
			systemViewStamp = new $.BaseService('#system_view_stamp_query_form', '#system_view_stamp_list_datagrid'
													, '#system_view_stamp_form_dialog', '#system_view_stamp_edit_form'
													, {actionRootUrl: '<c:url value="/manage/systemViewStamp/" />'
														, entityTitle: "系统视图标记"
														, datagridAddButtonId: 'system_view_stamp_add_button'
														, datagridBatchDeleteButtonId: 'system_view_stamp_batch_delete_button'
														, datagridColumns: [
															{field: 'name', title: '视图标记名称', width: 220, rowspan: 2, align: 'center', sortable: true
																, sorter: function(a, b) {
																	return (a>b?1:-1);
																}
															}
															, {field: 'description', title: '描述', width: 150, rowspan: 2, align: 'center'}
														]
													});
			
			$('#system_view_stamp_query_form_search_button').click(function() {$('#system_view_stamp_query_form').submit();});
		});
		
	</script>
	
	<div style="margin:10px 0;"></div>
	
	<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="system_view_stamp_query_form" id="system_view_stamp_query_form"
				method="post" action="" onsubmit="return false;">
			
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="1px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header">角色名称</th>
						<td><form:input path="entity.name"
								class="form_view_input combo easyui-validatebox" style="height: 27px;" /></td>
					</tr>
				</table>
			</form:form>
		</span>
		<span style="float:right;">
			<a class="l-btn" id="system_view_stamp_query_form_search_button"><span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
		<table>
		<tr><td height="30px"><td></tr>
		</table>
	</div>

<table id="system_view_stamp_list_datagrid"></table>
<div id="system_view_stamp_form_dialog">Dialog Content.</div>