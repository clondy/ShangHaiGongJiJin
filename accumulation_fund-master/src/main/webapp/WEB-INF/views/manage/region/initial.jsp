<%@page import="com.capinfo.commons.project.model.region.Region"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	require([ "dojo/data/ItemFileWriteStore", "dijit/tree/TreeStoreModel",
			"dijit/Tree", "dijit/tree/dndSource" ]);

	var currentRegionItem;

	// 添加区域
	function addRegion(item) {
		currentRegionItem = item;
		var formPane = dijit.byId("id_Security_Region_content_pane");
		formPane.set("href",
				'<c:url value="/manage/region/form.shtml?entity.parentId=" />'
						+ item.id);
	}

	// 修改区域
	function modifyRegion(item) {
		currentRegionItem = item;
		var formPane = dijit.byId("id_Security_Region_content_pane");
		formPane.set("href",
				'<c:url value="/manage/region/form.shtml?entity.id=" />'
						+ item.id);

	}

	// 删除区域
	function deleteRegion(item) {
		if (confirm("您确认要删除本区域吗?")) {
			var param = {
				url : '<c:url value="/manage/region/delete.shtml?entity.id=" />'
						+ item.id,
				sync : true,
				load : function(data) {
					flag = data;
					if (data == 'true') {
						alert("删除成功！");
						regionsStore.deleteItem(item);
					} else {
						alert("删除失败，有子区域不允许删除！");
					}
				},
				error : function(error) {
					alert('系统忙，请稍候再试！');
				}
			};
			dojo.xhrGet(param);
		}
	}
</script>

<table width="100%" border="0" align="center" cellpadding="2"
	cellspacing="1" class="page_table">
	<tr height="35">
		<td colspan="4" class="page_tablelist2"><div align="center">区域管理</div></td>
	</tr>
	<tr>
		<td>
			<div align="center">
				<table style="width: 100%;" border="0" cellspacing="0"
					cellpadding="0">
					<tr height="350px">
						<td valign="top" width="40%">
							<div data-dojo-type="dojo/data/ItemFileWriteStore"
								data-dojo-id="regionsStore"
								data-dojo-props="url: '<c:url value="/manage/region/regionJson.shtml" />'"></div>

							<div data-dojo-type="dijit/tree/TreeStoreModel"
								data-dojo-id="regionsModel"
								data-dojo-props="store: regionsStore, query: {id: '<%=Region.ROOT_ID%>'}, rootId: 'regionsRoot', rootLabel:'北京市'"></div>
							<!-- 设置树的缓存模型 -->

							<div data-dojo-type="dijit/Tree" id="regionsTree"
								data-dojo-props="model: regionsModel, showRoot: true, openOnClick:true, persist:true">
								<!--  onClick: modifyRegion -->
							</div> <!--  暂不提供新增、修改、删除功能， 需要时打开注释        2014.1.23  xuxianping -->

							<ul id="regions_base_tree_menu" data-dojo-type="dijit/Menu"
								data-dojo-props='style:"display: none;", targetNodeIds: ["regionsTree"], selector: ".dijitTreeNode"'>
								<li data-dojo-type="dijit/MenuItem">
									<script type="dojo/connect" data-dojo-event="onClick">
										regionsCurrentItem = dijit.byNode(this.getParent().currentTarget).item;
										addRegion(regionsCurrentItem, false);
									</script> 
									添加区域
								</li>
								<li data-dojo-type="dijit/MenuItem">
									<script type="dojo/connect" data-dojo-event="onClick">
										regionsCurrentItem = dijit.byNode(this.getParent().currentTarget).item;
										modifyRegion(regionsCurrentItem, false);
									</script> 
									修改区域
								</li>
								<li data-dojo-type="dijit/MenuItem">
									<script type="dojo/connect" data-dojo-event="onClick">
									regionsCurrentItem = dijit.byNode(this.getParent().currentTarget).item;
									deleteRegion(regionsCurrentItem);
									</script> 
									删除区域
								</li>
							</ul>
						</td>

						<td valign="top" width="60%">
							<div data-dojo-type="dojox/layout/ContentPane" title="区域信息"
								id="id_Security_Region_content_pane"
								data-dojo-props="closable:false, urlPreventCache:true, selected:true">
							</div>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr height="35">
		<td align="center">
			<button data-dojo-type="dijit/form/Button"
				id="rebuild_region_tree_button">
				<span>重新生成区域树</span>
				<script type="dojo/on" data-dojo-event="click" data-dojo-args="evt">
					this.set("disabled", true);
					var param = {
								  url:'<c:url value="/manage/region/build.shtml" />'
								, load:function(data){
									if(data == 'true'){
										
										alert("重建成功！");
									}else{
										
										alert("重建失败！");
									}
									dijit.byId("rebuild_region_tree_button").set("disabled", false);
								}
								, error: function (error) {
									alert('系统忙，请稍候再试！');	
									dijit.byId("rebuild_region_tree_button").set("disabled", false);
								}
							};
					dojo.xhrPost(param);
				</script>
			</button>
		</td>
	</tr>
</table>