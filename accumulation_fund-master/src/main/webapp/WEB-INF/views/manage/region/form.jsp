<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form name="region_form" id="region_form" method="post" action="" data-dojo-type="dijit/form/Form">
	<form:hidden path="entity.id"/>
	<form:hidden path="entity.parentId" id="entity_parentId"/>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="page_table">
		<tr height="30">
			<td class="table_gray" align="right" width="35%">区域名称：</td>
			<td class="page_input" align="left">
				<form:input path="entity.name" size="20" required="true" 
							data-dojo-type="dijit/form/ValidationTextBox" 
							data-dojo-props="regExp:'\\\\S{1,20}', promptMessage:'请输入区域名称', invalidMessage:'请确认区域名称长度在1~20个字之间', maxlength:20, trim:true"/>&nbsp;
				<font color="red">*</font>
		</tr>
		<tr height="30">
			<td class="table_gray" align="right">区域编码：</td>
			<td class="page_input" align="left">
				<form:input path="entity.standardcode" size="20" required="true" 
							data-dojo-type="dijit/form/ValidationTextBox" 
							data-dojo-props="regExp:'\\\\S{1,12}', promptMessage:'请输入区域编码', invalidMessage:'请确认区域编码长度在1~12个字之间', maxlength:12, trim:true"/>&nbsp;
				<font color="red">*</font>
			</td>
		</tr>
		<tr height="30">
			<td class="table_gray" align="right">描述：</td>
			<td class="page_input" align="left">
				<form:input path="entity.description" size="20" 
							data-dojo-type="dijit/form/ValidationTextBox" 
							data-dojo-props="regExp:'\\\\S{1,100}', promptMessage:'请输入描述', invalidMessage:'请确认描述长度在1~100个字之间', maxlength:100, trim:true"/>&nbsp;
			</td>
		</tr>
		<tr height="35">
			<td class="page_input" colspan="2">&nbsp;
				<button data-dojo-type="dijit/form/Button">
					<span><c:if test="${empty command.entity.id}">增加</c:if><c:if test="${not empty command.entity.id}">修改</c:if></span>
					<script type="dojo/on" data-dojo-event="click" data-dojo-args="evt">
						
						if(dijit.byId('region_form').validate()){
								var param = {
											url:'<c:url value="/manage/region/saveOrUpdate.shtml" />'
											,form: 'region_form'
											,load:function(data){
													var retValue = dojo.fromJson(data);
                    
													<c:if test='${empty command.entity.id}'>
														regionsStore.newItem(retValue, currentResourceItem);
                    									regionsModel.newItem(retValue, currentResourceItem);
														alert('增加成功!');
													</c:if>
													<c:if test='${not empty command.entity.id}'>
														regionsStore.setValue(currentResourceItem, "name", retValue.name);
														alert('修改成功!');
													</c:if>
											}
											,error: function (error) {alert('系统忙，请稍候再试！');}
									};
								dojo.xhrPost(param);
						}
	    			</script>
				</button>
			</td>
		</tr>
	</table>
</form:form>