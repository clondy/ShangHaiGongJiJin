<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<script>
	
		var companyService;
		$(function() {
			
			companyService = new $.BaseService('#company_query_form', '#company_list_datagrid'
													, '#company_form_dialog', '#system_role_edit_form'
													, {actionRootUrl: '<c:url value="/manage/systemRole/" />'
														, entityTitle: "公司"
														, datagridAddButtonId: 'company_add_button'
														, datagridBatchDeleteButtonId: 'company_batch_delete_button'
														, datagridColumns: [
															{field: 'name', title: '角色名称', width: 220, rowspan: 2, align: 'center', sortable: true
																, sorter: function(a, b) {
																	return (a>b?1:-1);
																}
															}
															, {field: 'description', title: '描述', width: 150, rowspan: 2, align: 'center'}
														]
														, dialogWidth: 800
														, dialogHeight: 600
														, dialogButtons: [
															{
																id: 'queren'
																, text:'确认'
																//, iconCls: 'icon-save'
																, handler: function() {
																	
																	if( confirm("要执行“确认”操作吗？？？") ) {
																		
																		$.messager.alert('通知', this.id + ': 确认');
	
																		$("#fuhe").linkbutton('enable');
																	}
																}
															}
															, {
																id: 'fuhe'
																, text:'复核'
																, disabled: true
																//, iconCls: 'icon-save'
																, handler: function() {
																	
																	if( confirm("要执行“复核”操作吗？？？") ) {
																		
																		$('#company_confirm_form_dialog').dialog({
																			
																			title: '业务复核'
																			//, href: this.actionRootUrl + this.formUrl
																			, width: 600
																			, height: 250
																			, closed: true
																			, cache: false
																			, resizable: true
																			, collapsible: true
																			, maximizable: true
																			, buttons: [{
																					text:'复核'
																					, iconCls: 'icon-save'
																					, handler: function() {
																						
																						$($(this).context.parentElement.parentElement).dialog('close');
																						companyService.dialog.dialog('close');
																					}
																				}
																				, {
																					text:'取消',
																					iconCls:'icon-cancel',
																					handler: function() {
																						
																						$($(this).context.parentElement.parentElement).dialog('close');

																						$("#fuhe").linkbutton('enable');
																					}
																				}]
																			, modal: true
																		}).dialog('open');
																		
																		$("#fuhe").linkbutton('disable');
																	}
																}
															}
															, {
																id: 'chongzhi'
																, text:'重置'
																//, iconCls: 'icon-save'
																, handler: function() {
																	
																	if( confirm("要执行“重置”操作吗？？？") ) {

																		$("#fuhe").linkbutton('disable');
																		companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
																	}
																}
															}
															, {
																text:'取消',
																iconCls:'icon-cancel',
																handler: function() {$($(this).context.parentElement.parentElement).dialog('close');}
															}
														]
													});
			
			$('#company_query_form_search_button').click(function() {$('#company_query_form').submit();});
		});
		
	</script>
	
	<div style="margin:10px 0;"></div>
	
	<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="company_query_form" id="company_query_form"
				method="post" action="" onsubmit="return false;">
			
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="1px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header">角色名称</th>
						<td><form:input path="entity.name"
								class="form_view_input combo easyui-validatebox" style="height: 27px;" /></td>
					</tr>
				</table>
			</form:form>
		</span>
		<span style="float:right;">
			<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
		<table>
		<tr><td height="30px"><td></tr>
		</table>
	</div>

<table id="company_list_datagrid"></table>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog">确认表单Dialog Content.</div>