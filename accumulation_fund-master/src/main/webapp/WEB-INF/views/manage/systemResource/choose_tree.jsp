<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div dojoType="dijit.layout.ContentPane" style="height: 450px;width: 350px" class="nihilo"> 
	<div id="CheckboxTreeForResource"></div>
</div>

<div align="center">
	<!-- dialog 不能执行js 所以需要设置 script type="dojo/connect" 执行-->
	<button  dojoType="dijit.form.Button">确定
	   		<script type="dojo/connect">
				//console.log(checkboxTreeForResource);
				checkboxTreeForResource.createTree();
			</script>
			
			<script type="dojo/method" event="onClick">
    			checkboxTreeForResource.lookSelected();
				checkboxTreeForResource.hideDialog();
			</script>
	</button>
	
	<button  dojoType="dijit.form.Button">取消
			<script type="dojo/method" event="onClick">
				checkboxTreeForResource.hideDialog();
			</script>
	</button>
</div>