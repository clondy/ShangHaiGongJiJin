<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
  <head>
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <title>后台操作权限管理列表</title>
	
	<script type='text/javascript' src="<c:url value="/js/string_prototype.js" />"></script>
	<script type='text/javascript' src="<c:url value="/js/dhtml_x_tree/dhtmlXCommon.js" />"></script>
	<script type='text/javascript' src="<c:url value="/js/dhtml_x_tree/dhtmlXTree.js" />"></script>
	
	<script type='text/javascript' src="<c:url value="/dwr/engine.js" />"></script>
	<script type='text/javascript' src="<c:url value="/dwr/util.js" />"></script>
	
  </head>
  
  <body>
    <table width=95% border=0 cellspacing=0 cellpadding=0>
		<tr>
			<td valign="top" align="center">
				<div id="treeboxbox_tree" style="width:550; height:418;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div>
				<br><br>
			</td>
		</tr>
	</table>
	<script>
	
		var tree=new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);
		tree.setImagePath("<%=request.getContextPath()%>/images/dhtml_x_tree/");
		//tree.enableCheckBoxes(1);
		tree.enableDragAndDrop(1);
		//tree.enableThreeStateCheckboxes(true);
		tree.setDragHandler(movePosition);
		tree.loadXML("<%=request.getContextPath()%>/manage/resource/xml_data.shtml");
		
		function showActionForm(formId, itemId) {

			if( itemId ) {
				getResource(itemId);
			}
			
			var actionTable = $('actionTable');
			var rows = actionTable.rows;
			for(var i = 0, num = rows.length; i < num; i++) {
				var row = rows[i];
				
				if( row.id == formId ) {
					row.style.display = 'block';
				} else {
					row.style.display = 'none';
				}
			}
			
			populateRoles('roles');
		}
		
		dwr.util.useLoadingMessage("数据进行处理中，请稍后…………");
	</script>
	
    <table id="actionTable" width="95%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
		<tr id="addChildCell" style="display: none">
			<td valign="top" align="center">
				<input type="hidden" value="URL" name="type" id="type" readonly="readonly"/>
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr align="center">
						<td>资源访问名称：</td><td><input type="text" value="" name="name" /></td>
						<td>资源访问值：</td><td><input type="text" value="" name="value" id="value" /></td>
					</tr>
					<tr align="center">
						<td>是否显示：</td><td><input type="checkbox" name="display" id="display" checked /></td>
						<td>允许访问的角色：</td><td><select name="roles" id="roles" multiple="multiple" size="5"></td>
					</tr>
					<tr align="center">
						<td align="center" colspan="4"><a href="javascript:void(0);" onclick="javascript:addChildResource();">添加子分类</a></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="modifyCell" style="display: none">
			<td valign="top" align="center">
				<input type="hidden" name="id" id="modify_id" />
				<input type="hidden" value="URL" name="type" id="modify_type" readonly="readonly"/>
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr align="center">
						<td>资源访问名称：</td><td><input type="text" value="" name="name" id="modify_name" /></td>
						<td>资源访问值：</td><td colspan="3"><input type="text" value="" name="value" id="modify_value" /></td>
					</tr>
					<tr align="center">
						<td>是否显示：</td><td><input type="checkbox" name="display" id="modify_display" /></td>
						<td>允许访问的角色：</td><td><select name="roles" id="modify_roles" multiple="multiple" size="5"></select></td>
						<td>子资源完全复制访问角色：</td><td><input type="checkbox" name="full_clone_role" id="full_clone_role" /></td>
					</tr>
					<tr align="center">
						<td align="center" colspan="6"><a href="javascript:void(0);" onclick="javascript:modifyResource();">修改指定分类</a></td>
					</tr>
				</table>
			</td>
		</tr>
  </body>
</html>
