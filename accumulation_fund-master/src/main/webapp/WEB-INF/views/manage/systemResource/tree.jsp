<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">

	(function($) {
	
		$.SystemResourceTree = function(treeId, dialogId, editFormId, args) {
			
			var that = this;
			this.args = args || {};

			this.positionMapping = {append: 'over', top: 'before', bottom: 'after'};
			
			this.treeId = treeId;
			this.tree = $(treeId);
			
			this.dialogId = dialogId;
			this.dialog = $(dialogId);
			
			this.editFormId = editFormId;
			
			//指定模块的根目录
			this.actionRootUrl = this.args.actionRootUrl;
			
			//请求URL参数
			this.formUrl = this.args.formUrl || 'form.shtml';
			this.deleteUrl = this.args.deleteUrl || 'delete.shtml';
			this.listUrl = this.args.listUrl || 'list.shtml';
			this.detailUrl = this.args.detailUrl || 'detail.shtml';
			this.saveOrUpdateUrl = this.args.saveOrUpdateUrl || 'save_or_update.shtml';
			
			this.entityTitle = this.args.entityTitle || '权限资源';

			this.dialog.dialog({
				
				title: this.entityTitle + '信息维护'
				, href: that.actionRootUrl + that.formUrl
				, width: 600
				, height: 450
				, closed: true
				, cache: false
				, resizable: true
				, collapsible: true
				, maximizable: true
				, buttons: [{
					text:'保存'
					, iconCls: 'icon-save'
					, handler: function() {
						
						var innerThat = this;
						$.messager.progress();

						$(that.editFormId).form('submit', {
							url: that.actionRootUrl + that.saveOrUpdateUrl
							, onSubmit: function() {

								var isValid = $(this).form('validate');
								if ( !isValid ){
									
									$.messager.progress('close');
								}
								
								return isValid;
							}
							, success: function(data) {

								data = jQuery.parseJSON('' + data);
								$.messager.progress('close');
								
								if( data ) {

									var node = that.tree.tree('getSelected');
									if( node ) {
										
										if( $('#system_resource_edit_form_entity_id')[0].value ) {

											that.tree.tree('update', {
												target: node.target
												, text: data.name
											});
										} else {

											that.tree.tree('append', {
												parent : (node? node.target: null),
												data : [{
													id: data.id
													, text : data.name
												}]
											});
										}
									}
									
									$($(innerThat).context.parentElement.parentElement).dialog('close');
								} else {

									$.messager.alert('通知', "权限资源提交失败。");
								}
							}
						});
					}
				}
				, {
					text:'取消',
					iconCls:'icon-cancel',
					handler:function(){$($(this).context.parentElement.parentElement).dialog('close');}
				}]
				, modal: true
			});
			
			this.openFormDialog = function(isAdd) {
				
				var node = this.tree.tree('getSelected');
				
				var parameterName = ( isAdd )? 'entity.parentId': 'entity.id';
				if( node ) {

					this.dialog.dialog('refresh', this.actionRootUrl + this.formUrl + '?' + parameterName + '=' + node.id);
					this.dialog.dialog('open');
				}
			}
			
			this.remove = function() {
				
				var node = this.tree.tree('getSelected');
				if( node ) {

					if( confirm("您确认要删除本资源吗?") ) {

						$.ajax({
							url: this.actionRootUrl + this.deleteUrl
							, type : "post"
							, dataType : 'json'
							, data : {'entity.id': node.id}
							, success : function(data, response, status) {
				
								if( jQuery.parseJSON('' + data) ) {
									
									$.messager.alert('通知', "权限资源删除成功。");
									that.tree.tree('remove', node.target);
								} else {
									
									$.messager.alert('通知', "权限资源删除失败。");
								}
							}
						});
					}
				}
			}

			this.collapse = function() {
				
				var node = this.tree.tree('getSelected');
				this.tree.tree('collapse', node.target);
			}
			
			this.expand = function (){

				var node = this.tree.tree('getSelected');
				this.tree.tree('expand', node.target);
			}
			
			this.rebuildResourceJson = function() {

				$.ajax({
					url: this.actionRootUrl + 'rebuildResource.shtml'
					, async: false
					, type : "get"
					, dataType : 'json'
					, success : function(data, response, status) {
		
						if( jQuery.parseJSON('' + data) ) {

							$.messager.alert('通知', "资源树重建成功！");
							that.tree.tree('reload');
						} else {

							$.messager.alert('通知', "资源树重建失败！");
						}
					}
					, error: function (error) {$.messager.alert('通知', '系统忙，请稍候再试！');}
				});
			}
		}
	})(jQuery);
	
	var systemResourceService;
	$(function() {
		
		systemResourceService = new $.SystemResourceTree('#system_resource_tree'
														, '#system_resource_edit_form_dialog'
														, '#system_resource_edit_form'
														, {
															actionRootUrl: '<c:url value="/manage/systemResource/" />'
															, saveOrUpdateUrl: 'saveOrUpdate.shtml'
														});

		$('#system_resource_rebuild_button').click(function() {systemResourceService.rebuildResourceJson();});
	});
	
</script>

<table>
	<tr>
		<td height="10px">
		</td>
	</tr>
	<tr>
		<td width="10px"></td>
		<td width="85%">
			<ul id="system_resource_tree" class="easyui-tree"
				data-options="url: '<c:url value="/manage/systemResource/resource.shtml" />'
							, animate: true
							, method: 'get'
							, dnd: true
							, onContextMenu: function(e, node) {
							
								e.preventDefault();
								$(this).tree('select', node.target);
								
								var menu = $('#system_resource_tree_menu');
								if( 1 == node.id ) {
								
									menu = $('#system_resource_tree_root_menu');
								}
								menu.menu('show'
											, {
												left: e.pageX
												, top: e.pageY
											}
										);
							}
							, onDrop: function(target, source, point) {
					
								/*console.log($(this).tree('getNode', target));
								console.log(source.id);
								console.log(point);
								console.log(positionMapping[point]);*/
								
								$.ajax({
									url: '<c:url value="/manage/systemResource/move.shtml"/>'
									, async: false
									, type : 'post'
									, dataType : 'json'
									, data : {currentId: source.id, targetId: $(this).tree('getNode', target).id, position: systemResourceService.positionMapping[point]}
									, success : function(data, response, status) {
						
										//flag = jQuery.parseJSON('' + data);
										//console.log(flag);
									}
									, error: function (error) { $.messager.alert('通知', '系统忙，请稍候再试！'); }
								});
							}">
			</ul>
		</td>
		<td>
			<a class="l-btn" id="system_resource_rebuild_button"><span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">重建资源树</span></span></a>
		</td>
	</tr>
</table>


<div id="system_resource_tree_root_menu" class="easyui-menu" style="width: 120px;">
	<div onclick="systemResourceService.openFormDialog(true)" data-options="iconCls:'icon-add'">添加</div>
	<div onclick="systemResourceService.openFormDialog(false)" data-options="iconCls:'icon-add'">修改</div>
	<div class="menu-sep"></div>
	<div onclick="systemResourceService.expand()">展开</div>
	<div onclick="systemResourceService.collapse()">收起</div>
</div>
<div id="system_resource_tree_menu" class="easyui-menu" style="width: 120px;">
	<div onclick="systemResourceService.openFormDialog(true)" data-options="iconCls:'icon-add'">添加</div>
	<div onclick="systemResourceService.openFormDialog(false)" data-options="iconCls:'icon-add'">修改</div>
	<div onclick="systemResourceService.remove()" data-options="iconCls:'icon-remove'">删除</div>
	<div class="menu-sep"></div>
	<div onclick="systemResourceService.expand()">展开</div>
	<div onclick="systemResourceService.collapse()">收起</div>
</div>

<div id="system_resource_edit_form_dialog">Dialog Content.</div>