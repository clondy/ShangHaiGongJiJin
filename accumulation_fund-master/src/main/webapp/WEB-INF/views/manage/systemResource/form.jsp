<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript">

	var validateResourceValue = {
		
		getValidateUrl: function() {
			
			return '<c:url value="/manage/systemResource/validate.shtml" />';
		}
		, getValidateParameter: function(value) {

			var value = jQuery.trim(value);
			if( value ) {
				
				return {'entity.value': value, 'entity.id': $('#system_resource_edit_form_entity_id').val()};
			}
			
			return {}
		}
		, validator: function(result) {
	
			//因为当相同的资源值已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
			return !jQuery.parseJSON('' + result);
		}
	}
</script>

<div class="div_center">
	<form:form name="system_resource_edit_form" id="system_resource_edit_form" method="post" action="" onsubmit="return false;" >
		<form:hidden path="entity.id" id="system_resource_edit_form_entity_id" />
		<form:hidden path="entity.parentId" id="system_resource_edit_form_entity_parentid" />
		
		<table class="form_view_border" bordercolordark="#FFFFFF"
			bordercolorlight="#45b97c" border="1px" cellpadding="0"
			cellspacing="0">
			<tr>
				<th class="panel-header">资源描述名称</th>
				<td >
					<form:input path="entity.name" id="system_resource_edit_form_entity_name"
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px;" />
				</td>
				<th class="panel-header">资源类型</th>
				<td >
					<form:input path="entity.type"
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px;" />
				</td>
			</tr>
			<tr>
				<th class="panel-header">资源访问值</th>
				<td colspan="3">
					<form:input path="entity.value" 
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px; width: 463px;"
						validType="complexValid['', '', 'validateResourceValue', '此值（地址）的资源已存在。']" />
				</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<th class="panel-header">是否显示</th>
				<td >
					<form:checkbox path="entity.display" />
				</td>
				<th class="panel-header">需要授权访问</th>
				<td >
					<form:checkbox path="entity.needAuthorized" onClick="var roleIds = $('#system_resource_edit_form_role_ids'); roleIds.combobox('options').required = this.checked; roleIds.combobox({});" />
				</td>
			</tr>
			<tr>
				<th class="panel-header">作为分类菜单</th>
				<td >
					<form:checkbox path="entity.categoryMenu" />
				</td>
				<th class="panel-header">子资源继承角色</th>
				<td >
					<form:checkbox path="allSubResourceHasParentRole" />
				</td>
			</tr>
			<tr>
				<th class="panel-header">视图标记</th>
				<td colspan="3">
					<form:select class="easyui-combobox"
						path="entity.viewStampId" id="system_resource_edit_form_view_stamp_id"
						data-options="required: true, panelHeight: 'auto'">
						<form:option value="">--请选择--</form:option>
						<c:forEach var="item" items="${viewStamps}">
							<form:option label="${item.description}" value="${item.id}" />
						</c:forEach>
					</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">允许访问的角色</th>
				<td colspan="3">
					<form:select
						path="roleIds" id="system_resource_edit_form_role_ids" class="easyui-combobox"
						data-options="required: true, multiple: true, panelHeight: 'auto'"
						style="width: 465px;">
						<c:forEach var="item" items="${roles}">
							<form:option label="${item.description}" value="${item.id}" />
						</c:forEach>
					</form:select>
				</td>
			</tr>
		</table>
	</form:form>
</div>
