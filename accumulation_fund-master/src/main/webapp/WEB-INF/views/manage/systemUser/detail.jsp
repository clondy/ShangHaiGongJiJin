<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table width="100%" border="0" align="center" cellpadding="2"
	cellspacing="1" class="page_table">
	<tr height="35">
		<td colspan="4" class="page_tablelist2"><div align="center">系统用户信息</div></td>
	</tr>
	<tr height="30">
		<td class="table_gray" width="20%">所属区域：</td>
		<td class="page_input" colspan="3"><c:out
				value="${SystemUser.region.hierarchicalName }" /></td>
	</tr>
	<tr height="30">
		<td class="table_gray" width="20%">姓名：</td>
		<td class="page_input" width="30%"><c:out
				value="${SystemUser.name }" /></td>
		<td class="table_gray" width="20%">登录名：</td>
		<td class="page_input" width="30%"><c:out
				value="${SystemUser.logonName }" /></td>
	</tr>
	<tr>
		<td class="table_gray" width="20%">角色：</td>
		<td class="page_input" colspan="3"><select disabled="disabled"
			id="userroleIdid" multiple="true" size="5">
				<c:forEach items="${SystemUser.roles }" var="role">
					<option value="${role.id}">
						<c:out value="${role.description}" />
					</option>
				</c:forEach>
		</select></td>

	</tr>

	<tr height="35">
		<td colspan="4" align="center">
			<div align="center">
				<button data-dojo-type="dijit/form/Button" onclick="javascript:systemUserService.hideForm();">
					<span>关闭</span>
				</button>
			</div>
		</td>
	</tr>
</table>
