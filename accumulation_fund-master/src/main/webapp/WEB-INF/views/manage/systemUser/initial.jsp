<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<script>
	
		var systemUserService;
		$(function() {
			
			systemUserService = new $.BaseService('#system_user_query_form', '#system_user_list_datagrid'
													, '#system_user_form_dialog', '#system_user_edit_form'
													, {actionRootUrl: '<c:url value="/manage/systemUser/" />'
														, entityTitle: "系统用户"
														/* , optionFormatter: function(value, rec) {
															
															return '<a href="#" title="修-改" onClick="systemUserService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + systemUserService.actionRootUrl + systemUserService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
																	+ '&nbsp;&nbsp;'
																	+ '<a href="#" title="删-除" onClick="systemUserService.datagrid.datagrid(\'checkRow\', systemUserService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + systemUserService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>';
														} */
														, datagridAddButtonId: 'system_user_add_button'
														, datagridBatchDeleteButtonId: 'system_user_batch_delete_button'
														, datagridColumns: [
															{field: 'name', title: '姓名', width: 220, rowspan: 2, align: 'center', sortable: true
																, sorter: function(a, b) {
																	return (a>b?1:-1);
																}
															}
															, {field: 'logonName', title: '登录名', width: 120, align: 'center'}
															, {field: 'regionName', title: '所属区域', width: 150, rowspan: 2, align: 'center'}
														]
													});
			
			$('#system_user_query_form_search_button').click(function() {$('#system_user_query_form').submit();});
		});
		
	</script>
	
	<div style="margin:10px 0;"></div>
	
	<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="system_user_query_form" id="system_user_query_form"
				method="post" action="" onsubmit="return false;">
			
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="1px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header">姓名</th>
						<td><form:input path="entity.name"
								class="form_view_input combo easyui-validatebox" style="height: 27px;" /></td>
						<th class="panel-header">登录名</th>
						<td><form:input path="entity.logonName"
								class="form_view_input combo easyui-validatebox" style="height: 27px;" /><div id="repeat_tips" style="position: absolute;"></div></td>
					</tr>
				</table>
			</form:form>
		</span>
		<span style="float:right;">
			<a class="l-btn" id="system_user_query_form_search_button"><span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
		<table>
		<tr><td height="30px"><td></tr>
		</table>
	</div>

<table id="system_user_list_datagrid"></table>
<div id="system_user_form_dialog">Dialog Content.</div>
	