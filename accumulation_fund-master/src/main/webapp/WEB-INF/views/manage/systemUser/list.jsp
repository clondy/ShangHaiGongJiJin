<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
{
	"total": <c:out value="${DataTotalCount}" />
	, "rows": [
		<c:forEach var="systemUser" items="${SystemUserList}" varStatus="status">
		{"id": "${systemUser.id}", "logonName": "${systemUser.logonName}", "name": "${systemUser.name}", "regionName": "${systemUser.region.hierarchicalName}"}<c:if test="${!status.last}">, </c:if>
		</c:forEach>
	]
}