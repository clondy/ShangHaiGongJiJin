<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
	
	var validateSystemUserLogonName = {
		
		getValidateUrl: function() {
			
			return '<c:url value="/manage/systemUser/validator.shtml" />';
		}
		, getValidateParameter: function(value) {

			var value = jQuery.trim(value);
			if( value ) {
				
				return {'entity.logonName': value, 'entity.id': $('#system_user_entity_id').val()};
			}
			
			return {}
		}
		, validator: function(result) {

			//因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
			return !jQuery.parseJSON('' + result);
		}
	}
</script>

<div class="div_center">
	<form:form name="system_user_edit_form" id="system_user_edit_form" method="post"
		action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="system_user_entity_id" />
		<form:hidden path="entity.regionId" id="system_user_region_form_regionId" value="1000" />
		<table class="form_view_border" bordercolordark="#FFFFFF"
			bordercolorlight="#45b97c" border="1px" cellpadding="0"
			cellspacing="0">
			<tr>
				<th class="panel-header">姓名</th>
				<td><form:input path="entity.name"
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px;" /></td>
				<th class="panel-header">登录名</th>
				<td><form:input path="entity.logonName" id="system_user_logonname"
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px;"
						validType="complexValid['', '', 'validateSystemUserLogonName', '此登录名已被使用。']" /></td>
			</tr>

			<c:if test='${empty command.entity.id}'>
				<tr height="30">
					<th class="panel-header">密码</th>
					<td><form:password path="entity.password" id="system_user_password"
							class="form_view_input combo easyui-validatebox"
							data-options="required: true" style="height: 27px;"
							validType="complexValid['^\\\\w{6,18}$','请输入密码,长度在6~18之间，并且只能包含字符、数字和下划线。']" /></td>
					<th class="panel-header">密码确认</th>
					<td><form:password path="confirmPassword" id="system_user_confirm_password"
							class="form_view_input combo easyui-validatebox"
							data-options="required: true" style="height: 27px;"
							validType="equals['#system_user_password']" /></td>
				</tr>
			</c:if>
			<tr>
				<th class="panel-header">角色</th>
				<td colspan="3">
					<!-- textarea class="combo form_view_textarea"></textarea -->
					<form:select
						path="roleIds" class="easyui-combobox"
						data-options="required: true, multiple: true, panelHeight: 'auto'"
						style="width: 465px;">
						<c:forEach var="item" items="${roles}">
							<form:option label="${item.description}" value="${item.id}" />
						</c:forEach>
					</form:select>
				</td>
			</tr>
		</table>
	</form:form>
</div>
