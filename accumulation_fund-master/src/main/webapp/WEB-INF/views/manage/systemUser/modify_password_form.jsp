<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="div_center">
<form:form name="system_user_modify_password_form" id="system_user_modify_password_form"
	method="post" action="" data-dojo-type="dijit/form/Form"
	onsubmit="return false;">
	<form:hidden path="entity.id" id="entity_id" />

	<table class="form_view_border" bordercolordark="#FFFFFF"
			bordercolorlight="#45b97c" border="1px" cellpadding="0"
			cellspacing="0">
		<tr height="30">
			<th class="panel-header">现密码</th>
			<td class="page_input" width="60%" colspan="3">
				<form:password path="originalPassword" id="system_user_modify_password_original_password"
							class="form_view_input combo easyui-validatebox"
							data-options="required: true" style="height: 27px;"
							validType="complexValid['^\\\\w{6,18}$','请输入现密码,长度在6~18之间，并且只能包含字母、数字和下划线。']" />
			</td>
		</tr>
		<tr height="30">
			<th class="panel-header">新密码</th>
			<td class="page_input" width="60%">
				<form:password path="newPassword" id="system_user_modify_password_new_password"
							class="form_view_input combo easyui-validatebox"
							data-options="required: true" style="height: 27px;"
							validType="complexValid['^\\\\w{6,18}$','请输入新密码,长度在6~18之间，并且只能包含字母、数字和下划线。']" />
			</td>
		</tr>
		<tr height="30">
			<th class="panel-header">新密码确认</th>
			<td class="page_input" width="60%">
				<form:password path="confirmPassword" id="system_user_modify_password_confirm_password"
							class="form_view_input combo easyui-validatebox"
							data-options="required: true" style="height: 27px;"
							validType="equals['#system_user_modify_password_new_password']" />
			</td>
		</tr>
	</table>
</form:form>
</div>
