<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:if test="${not empty DictionarySortList}">
	<table width="100%" border="1" cellspacing="0" cellpadding="6" bordercolor="#999999" bordercolordark="#ffffff">
		<tr> 
			<td height="30">字典分类列表</td>
		</tr>
	</table>
	<div id="dictionarySortGridDiv"></div>
	<table width="100%" style="border: 0; border-spacing: 0; padding: 0">
		<tr>
			<td style="text-align: left">
				<button data-dojo-type="dijit/form/Button" id="dictionary_sort_delete_button" onclick='dictionarySortService.delBatch()'>删 除</button>
			</td>
			<td id="dictionary_sort_page_bar_display_foot" style="text-align: right">
			</td>
		</tr>
	</table>
	
	<script type="text/javascript">
		require(["framework/dijit/list/PageBar"
		         ,"dojo/data/ItemFileWriteStore"
		         ,"dojox/grid/DataGrid"
				 ,"dijit/form/ComboButton"
		         ,"dojox/grid/_CheckBoxSelector"], function(PageBar, ItemFileWriteStore, DataGrid, ComboButton, _CheckBoxSelector){
			
			if('${DataTotalCount}' != 0){
				var pageBar = new PageBar(dojo.hitch(dictionarySortService, dictionarySortService.pageBarOperation)
															, ['dictionary_sort_page_bar_display_foot']
															, '<c:out value="${DataTotalCount}" />'
															, '<c:out value="${PerPieceSize}" />');
				pageBar.setCurrentPage(<c:out value="${CurrentPieceNum}"/>);
				pageBar.displayHiddenJumpForm();

				var dictionarySortListJson = {identifier: "id", items: []};
				<c:forEach var="dictionarySort" items="${DictionarySortList}">
				
				dictionarySortListJson.items.push({'id': '${dictionarySort.id}'
										, 'name': '${dictionarySort.name}'
										, 'description': '${dictionarySort.description}'
										});
				</c:forEach>
				var dictionarySortListStore = new ItemFileWriteStore({data: dictionarySortListJson});
				
				var layout = [{type: "dojox.grid._CheckBoxSelector"}
							, [{'styles': 'text-align: center;', 'name': 'Id', 'field': 'id', 'hidden': "true"}
								, {'styles': 'text-align: center;', 'field': 'name', 'name': '名称', 'width': '30%'}
								, {'styles': 'text-align: center;', 'field': 'description', 'name': '描述', 'width': '30%'}
								, {
									styles: 'text-align: center;height:30px', 
								    field: "id",
									width: '30%',
 									name: "操作",
									headStyles:"display:hidden",
		  							type: dojox.grid.cells._Widget,
		 							editable: false,
		 						 	formatter: function(value, index, node){
										var menu = new dijit.Menu({ });
          								var menuItem1 = new dijit.MenuItem({
               										 			label: "编辑",
              								  					onClick: function() { 
																	var grid = node.grid;
	    															var item = grid.getItem(index);
	    															var store = grid.store;
																	var id=store.getValue(item, "id");
																	dictionarySortService.modifyForm(id);
																 }
           													 });
           							 	var menuItem2 = new dijit.MenuItem({
                												label: "删除",
               													onClick: function() {
																	var grid = node.grid;
	    															var item = grid.getItem(index);
	    															var store = grid.store;
																	var id=store.getValue(item, "id");
																	dictionarySortService.del(id); 
																}
            												});

										var menuItem3 = new dijit.MenuItem({
                												label: "管理字典项",
               													onClick: function() {
																	var grid = node.grid;
	    															var item = grid.getItem(index);
	    															var store = grid.store;
																	var id=store.getValue(item, "id");
																	
																	dictionarySortService.dialogBuilder.showDialog({
																													url: '<c:url value="/manage/dictionary/search.shtml?entity.sortId="/>'+id
																												   , id: 'dictionary_manage_dialog'
																												   , title: '字典项管理'
																												   , style: 'heiht:400px; width:800px'																											
																												}); 
																}
            												});
           						 		menu.addChild(menuItem1);
            							menu.addChild(menuItem2);
										menu.addChild(menuItem3);
            							return new ComboButton({
               													 optionsTitle: "Save Options",
              													  label: "操作",
              													  dropDown: menu,
               													 onClick:function(){ this.openDropDown(); }
           													 }, "combo");
									}}
							]];

				var arguments = {id: 'grid_capinfo_manage_dictionary_sort_list', store: dictionarySortListStore, structure: layout
								, autoHeight: true, escapeHTMLInData: false, rowsPerPage: '${PerPieceSize}'
								, onRowClick: function(e){
									
									this.edit.rowClick(e);
								 }};
				if( dojo.isIE ) {
					
					arguments['width'] = '100%';
				} else {

					arguments['width'] = 'auto';	//'90%'
					//, autoWidth: true
				}
				grid = new DataGrid(arguments, 'dictionarySortListGrid');
				grid.placeAt("dictionarySortGridDiv");
				grid.startup();
			}
		});
	</script>
</c:if>
<c:if test="${empty DictionarySortList}">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="page_table">
		<tr height="35">
			<td><div align="center">未查询到相关数据</div></td>
		</tr>
	</table>
</c:if>
