<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<script type="text/javascript">
		require(['framework/dojo/service/crud/base'], function(BaseService){
		
			dictionaryService = new BaseService({actionRootUrl: '<c:url value="/manage/dictionary/" />'
																	, entityName: '字典项'
																	, dialogId: 'capinfo_manage_dojo_dictionary_dialog'
																	, queryFormId: 'query_dictionary_form'
																	, saveOrUpdateFormId: 'dictionary_form'
																	, addOrModifyFlag: 'dictionaryEntityId'
																	, refreshPaneId: 'id_manage_dictionary_search_listdata_pane'
																	, dataListGridId: 'grid_capinfo_manage_dictionary_list'
																	, addOrModifyValidationFunction: function() {
																										return dijit.byId(this.saveOrUpdateFormId).validate();
																									}
																	});
		});
		
		function addDictionaryForm(){
			dictionaryService.representForm('<c:url value="/manage/dictionary/form.shtml?entity.sortId=${command.entity.sortId}"/>');
		}
		
		
		function queryDictionary(){
			dojo.byId("query_dictionary_form_current_piece_num").value = 1;
			dictionaryService.query();
		}
	</script>

    <form:form name="query_dictionary_form" id="query_dictionary_form" method="post" action="" data-dojo-type="dijit/form/Form" onsubmit="return false;">
	    <form:hidden path="currentPieceNum" id="query_dictionary_form_current_piece_num" />
	    <form:hidden path="entity.sortId"/>
		<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0" class="page_tablebg">
			<tr>
				<td>
					<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="page_table">
						<tr height="35">
							<td colspan="6" class="page_tablelist2"><div align="center">字典项管理</div></td>
						</tr>
						<tr height="35">
							<td class="table_gray" width="20%" align="left">
								名称：
							</td>
							<td class="page_input" width="80%" align="left">
								<form:input path="entity.name" size="16" /> &nbsp;&nbsp;&nbsp;&nbsp;
		
								<button data-dojo-type="dijit/form/Button"
									onclick="javascript:queryDictionary();">
									<span>查询</span>
								</button> &nbsp;&nbsp;
								<button data-dojo-type="dijit/form/Button"
									onclick="javascript:addDictionaryForm();">
									<span>新增</span>
								</button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form:form>
	
	<div id="id_manage_dictionary_search_listdata_pane" data-dojo-type="dojox/layout/ContentPane" preventCache="true" style="padding: 0;">
		列表
	</div>
	<script type="text/javascript">
		queryDictionary();
	</script>
