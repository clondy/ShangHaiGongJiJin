<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form id="dictionary_sort_form" method="post" action="" data-dojo-type="dijit/form/Form" onsubmit="return false;">
	<form:hidden path="entity.id" id="dictionarySortEntityId" />
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="page_table">
		<tr height="30">
			<td class="table_gray" align="right">
				名称：
			</td>
			<td class="page_input" align="left">
				<form:input path="entity.name" size="20" required="true" id="dictionary_sort_name"
							data-dojo-type="dijit/form/ValidationTextBox" 
							data-dojo-props="regExp:'\\\\S{1,20}', promptMessage:'请输入字典名称', invalidMessage:'请确认字典名称长度在1~20个字之间', maxlength:20, trim:true"/>&nbsp;&nbsp;
				<font color="red">*</font>
			</td>
		</tr>
		<tr height="30">
			<td class="table_gray" align="right">
				描述：
			</td>
			<td class="page_input" align="left">
				<form:textarea path="entity.description" size="20" required="true" id="dictionary_sort_description"
							 	data-dojo-type="dijit/form/ValidationTextBox"
							 	data-dojo-props="regExp:'\\\\S{1,100}', promptMessage:'请输入角色描述', invalidMessage:'请确认角色描述长度在1~100个字之间!', maxlength:200, trim:true"/>&nbsp;&nbsp;			
				<font color="red">*</font>
			</td>
		</tr>
		<tr height="30">
			<td class="table_gray" align="right">
				是否可用：
			</td>
			<td class="page_input" align="left">
				<form:checkbox path="entity.enabled" data-dojo-type="dijit/form/CheckBox"/>
			</td>
		</tr>
		<tr height="35">
			<td class="page_input" colspan="2" align="center">
				<button data-dojo-type="dijit/form/Button"
					onclick="dictionarySortService.addOrModify();">
					<span><c:if test="${empty command.entity.id}">增加</c:if><c:if test="${not empty command.entity.id}">修改</c:if></span>
				</button>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button data-dojo-type="dijit/form/Button"
					onclick="dictionarySortService.hideForm()">
					<span>关闭</span>
				</button>
			</td>
		</tr>
	</table>
</form:form>