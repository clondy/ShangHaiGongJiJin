<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
{
	"total": <c:out value="${DataTotalCount}" />
	, "rows": [
		<c:forEach var="systemRole" items="${SystemRoleList}" varStatus="status">
		{"id": "${systemRole.id}", "name": "${systemRole.name}", "description": "${systemRole.description}"}<c:if test="${!status.last}">, </c:if>
		</c:forEach>
	]
}