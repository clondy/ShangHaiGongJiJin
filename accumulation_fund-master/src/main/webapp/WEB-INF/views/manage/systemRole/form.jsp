<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
	
	var validateSystemRoleName = {
		
		getValidateUrl: function() {
			
			return '<c:url value="/manage/systemRole/validator.shtml" />';
		}
		, getValidateParameter: function(value) {

			var value = jQuery.trim(value);
			if( value ) {
				
				return {'entity.name': value, 'entity.id': $('#system_role_entity_id').val()};
			}
			
			return {}
		}
		, validator: function(result) {

			//因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
			return !jQuery.parseJSON('' + result);
		}
	}
	
	$(function() {
		
	});
</script>

<div class="div_center">
	<form:form name="system_role_edit_form" id="system_role_edit_form" method="post"
		action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="system_role_entity_id" />
		<form:hidden path="resourceIdString" id="system_role_resource_id_string"/>
		
		<table class="form_view_border" bordercolordark="#FFFFFF"
			bordercolorlight="#45b97c" border="1px" cellpadding="0"
			cellspacing="0">
			<tr>
				<th class="panel-header">角色名称</th>
				<td>
					<form:input path="entity.name"
						class="form_view_input combo easyui-validatebox"
						data-options="required: true" style="height: 27px;"
						validType="complexValid['^\\\\w{2,30}$', '请确认角色名称长度在2~30个字符之间！并且只能包括字母、数字和下划线。', 'validateSystemRoleName', '此角色名称已经存在。']" />
				</td>
			</tr>
			<tr>
				<th class="panel-header">角色描述</th>
				<td>
					<form:textarea path="entity.description" cols="60" rows="10"
						class="combo form_view_textarea textarea easyui-validatebox"
						data-options="required: true"
						validType="complexValid['^.{4,500}$','请确认角色描述的长度在4~500个字之间！']" />
				</td>
			</tr>
			<tr>
				<th class="panel-header">权限资源分配</th>
				<td>
					<div style="overflow: scroll; height: 180px">
						<ul id="system_role_checkbox_resource_tree" class="easyui-tree"
							data-options="url: '<c:url value="/manage/systemResource/resource.shtml" />'
										, animate: true
										, method: 'get'
										, checkbox: true
										, onLoadSuccess: function(node, data) {
										
											var resourceIdString = $('#system_role_resource_id_string')[0].value;
											if( resourceIdString ) {
											
												var ids = resourceIdString.split(',');
												for(var i = 0, j = ids.length; i < j; i++) {
													
													var node1 = $(this).tree('find', ids[i]);
													if( node1 ) {
													
														$(this).tree('check', node1.target);
													}
												}
											}
										}">
						</ul>
					</div>
				</td>
			</tr>
		</table>
	</form:form>
</div>
