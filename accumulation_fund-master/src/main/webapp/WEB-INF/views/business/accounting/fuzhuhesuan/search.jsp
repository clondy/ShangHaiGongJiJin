<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<script>
	var fuzhuService;
	$(function() {
		fuzhuService = new $.BaseService('#fuzhuhesuan_query_form','#fuzhuhesuan_list_datagrid','#fuzhuhesuan_form_dialog', '#fuzhuhesuan_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/fuzhuhesuan/" />',
					entityTitle : "辅助核算",
					optionFormatter : function(value, rec) {
					
					     		return '<a href="#" onClick="Sp('+ rec.ida+ ')">'+'新增经办'+'</a>'
								+ '&nbsp;&nbsp;'
								+'<a href="#" onClick="fuzhuService.dialog.dialog(\'open\').dialog(\'refresh\', \''+ '<c:url value="/business/accounting/fuzhuxglssj/" />'+ 'formxg.shtml'+ '?entity.id='+ rec.ida+ '\');" >'+'编辑'+'</a>'
								+ '&nbsp;&nbsp;'
								+'<a href="#" onClick="fuzhuService.datagrid.datagrid(\'checkRow\', fuzhuService.datagrid.datagrid(\'getRowIndex\', '+ rec.id+ '));$(\'#'+ fuzhuService.datagridBatchDeleteButtonId+ '\').click();">'+'删除'+'</a>'
					     
					     	
					     	
					     	
						
					}
					,
					datagridBatchDeleteButtonId : 'fz_batch_delete_button'
				   , datagridToolbar: [{
						id: 'fz_batch_delete_button'
						, text: '批量删除'
						, disabled: false
						, iconCls: 'icon-remove'
						, handler: function() {
			
							var checkedData = {};
							var items = fuzhuService.datagrid.datagrid('getChecked');
							for(var i = 0; i < items.length; i++) {
								
								checkedData["ids[" + i + "]"] = items[i].id;
							}
							var dataCount = Object.keys(checkedData).length;
							var prefix = ( 1 < dataCount )? "批量": "";
							if( dataCount ) {
								if( confirm("确定要删除吗？？？") ) {
			
									$.ajax({
										url: '${pageContext.request.contextPath}/business/accounting/fuzhuhesuan/deletexg.shtml'
										, type : "post"
										, dataType : 'json'
										, data : checkedData
										, success : function(data, response, status) {
							
											if( jQuery.parseJSON('' + data) ) {
												
												$.messager.alert('通知',  "删除成功。");
												fuzhuService.datagrid.datagrid('clearChecked');
												fuzhuService.datagrid.datagrid('reload');
											} else {
												
												$.messager.alert('通知',  "删除失败。");
											}
										}
									});
								} else {
			
									fuzhuService.datagrid.datagrid('clearChecked');
								}
							} else {
								
								$.messager.alert('通知', "请选中数据后再执行删除。");
							}
							
						}
					}]
					,datagridColumns : [ 
									    {field: 'hsxmbm', title: '核算项目编码', width: 140,align: 'center'},
									    {field: 'hsxmmc', title: '核算项目名称', width: 200,align: 'center'},
										{field: 'sfdjmx', title: '是否底级明细', width: 140,align: 'center',
									    	 formatter : function(value, rec) {
                                        		 if(value == 2){
                                        		  return '否';
	                                        	 }else if(value == 1){
	                                        		 return '是';
	                                        	 }}},
										{field: 'kjnd', title: '会计年度', width: 170,align: 'center'}]
					,
					dialogWidth : 800,
					dialogHeight : 600,
					dialogButtons : [
							{
								id : 'company_confirm',
								text : '保存'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#fuzhuhesuan_edit_form').form('validate')) {

										$.messager
												.confirm(
														'确认',
														'要执行“确认”操作吗？？？',
														function(r) {
															if (!r) {
																$.messager.progress('close');
																return false;
															} else {
																companyHistorySaveOrUpdate();

															}
														});
									} else {
										$.messager.progress('close');
									}
									function companyHistorySaveOrUpdate() {
										$("#fuzhuhesuan_edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/fuzhuxglssj/save_or_update.shtml',
															onSubmit : function() {
																var isValid = $('#fuzhuhesuan_edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$.messager.progress('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																fuzhuService.dialog.dialog('close');
																$('#fuzhuhesuan_query_form').submit();
																$.messager.alert('通知',"成功！");
															}
														});
									}
								}
							}

							,
							{
								id : 'chongzhi',
								text : '重置'
								//, iconCls: 'icon-save'
								,
								handler : function() {

									if (confirm("要执行“重置”操作吗？？？")) {

										$("#fuhe").linkbutton('disable');
										fuzhuService.dialog.dialog('refresh',
												fuzhuService.actionRootUrl
														+ fuzhuService.formUrl);
									}
								}
							},
							{
								text : '取消',
								iconCls : 'icon-cancel',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}
							} ]
				});

		$('#chaxun_search_button').click(function() {

			$('#fuzhuhesuan_query_form').submit();
		});
		
		$('#reset_button').click(function() {

			$('#fuzhuhesuan_query_form')[0].reset();
		});
	});
	$('#add_hs_button').click(
			function() {
				fuzhuService.dialog.dialog('open').dialog('refresh','<c:url value="/business/accounting/fuzhuxglssj/"/>'+'formxg.shtml');
			});
	function SpHistory() {

		var items = fuzhuService.datagrid.datagrid('getChecked');
		if(items != 0){
			
		if(items.length <= 1){
		$('#ztspHistory_dialog')
				.dialog(
						{
							title : '审批历史记录',
							href : "${pageContext.request.contextPath}/business/accounting/fuzhusplssj/search.shtml?entity.id="+(items.length > 0 ? items[0].id : 0),
							width : 900,
							height : 600,
							closed : false,
							cache : false,
							resizable : true,
							collapsible : true,
							maximizable : true,
							modal : true,
							buttons : [ {
								text : '关闭',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}

							} ]
						});
        }else{
        	$.messager.alert('通知',"只能选择一条数据！");
        }
}

else{
	$.messager.alert('通知',"请先选择一条数据！");
       }
	}
	
	function XgHistory() {

		var items = fuzhuService.datagrid.datagrid('getChecked');
if(items != 0){
	
if(items.length <= 1){
		$('#fzxgHistory_dialog')
				.dialog(
						{
							title : '修改历史记录',
							href : "${pageContext.request.contextPath}/business/accounting/fuzhuxglssj/search.shtml?entity.hsid="+(items.length > 0 ? items[0].id : 0),
							width : 900,
							height : 600,
							closed : false,
							cache : false,
							resizable : true,
							collapsible : true,
							maximizable : true,
							modal : true,
							buttons : [ {
								text : '关闭',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}

							} ]
						});
        }else{
        	$.messager.alert('通知',"只能选择一条数据！");
        }
}

else{
	$.messager.alert('通知',"请先选择一条数据！");
       }
	}
	$('#hssp_dialog').dialog(
			{
				title : '请输入审批意见',
				width : 400,
				height : 350,
				closed : true,
				cache : false,
				resizable : true,
				collapsible : true,
				maximizable : true,
				modal : true,
				buttons : [
							{
								id : 'company_confirm',
								text : '通过'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#fuzhuhesuan_edit_form').form('validate')) {

										SpHistorySaveOrUpdate();

									} 
									function SpHistorySaveOrUpdate() {
										$("#fuzhuhesuan_edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/fuzhusplssj/save_or_update.shtml',
															onSubmit : function() {
																var isValid = $('#fuzhuhesuan_edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$('#hssp_dialog').dialog('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																$.messager.alert('通知',"已通过！");
																$.messager.progress('close');
																$('#fuzhuhesuan_list_datagrid').datagrid("reload");
															}
														});
									}
								}
							}

							,
							{
								id : 'chongzhi',
								text : '不通过'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#fuzhuhesuan_edit_form').form('validate')) {

										SpHistorySaveOrUpdate();

									} 
									function SpHistorySaveOrUpdate() {
										$("#fuzhuhesuan_edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/Fzhssplssj/saveOrupdate2.shtml',
															onSubmit : function() {
																var isValid = $('#fuzhuhesuan_edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$('#hssp_dialog').dialog('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																$.messager.alert('通知',"不通过！");
																$.messager.progress('close');
																$('#fuzhuhesuan_list_datagrid').datagrid("reload");
															}
														});
									}
								}
							},{
					text : '取消',
					handler : function() {
						$(
								$(this).context.parentElement.parentElement).dialog('close');
					}

				} ]
			});
	
	function Sp(id) {
		$('#hssp_dialog').dialog('open').dialog('refresh',"${pageContext.request.contextPath}/business/accounting/fuzhusplssj/form2.shtml?entity.hsspid="+id);
	}
	
	
</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span>
			<form:form name="fuzhuhesuan_query_form" id="fuzhuhesuan_query_form"
                       method="post" action="" onsubmit="return false;">
		  <a class="l-btn" id="chaxun_search_button"><span class="l-btn-left">
          <span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text ">重置</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="add_hs_button"><span class="l-btn-left">
          <span class="l-btn-text ">新增</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text ">删除</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text " onclick="SpHistory()">审批历史</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text " onclick="XgHistory()">历史数据</span></span></a>
          <hr>
		<table>
	 <tr height="15px" >
		               <th class="panel-header">辅助核算类型</th>
				       <td>
        				   <form:select path="entity.hslxbm.hslxbm"
									 style="width: 156px;height: 30px;"
									 class="easyui-combobox">
							  <form:option value="" >--请选择--</form:option>
							  <form:options items="${fzlxszList}" itemValue="hslxbm" itemLabel="hslxmc" />
						   </form:select>
                       </td>
                       <td>&nbsp;&nbsp;&nbsp;</td>
                       <th class="panel-header">辅助核算项目</th>				
					   <td><form:input path="entity.hsxmmc"
							class="form_view_input combo easyui-validatebox" style="width: 153px;height: 27px;"/>
				       </td> 
		</tr>
		<tr height="5px"><td></td></tr>
   </table>
       </form:form>
		</span>
</div>
<div>&nbsp;</div>
<table id="fuzhuhesuan_list_datagrid"></table>
<div id="fuzhuhesuan_form_dialog">Dialog Content.</div>
<div id="fuzhuhesuan_confirm_form_dialog"></div>
<div id="fzhsxgHistory_dialog"></div>
<div id="fzxgHistory_dialog"></div>
<div id="hssp_dialog"></div>
<div id="ztspHistory_dialog"></div>

