<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %> {
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="voucher" items="${VoucherList}" varStatus="status">
    {"id": "${voucher.id}",
    "ywlx": "${voucher.ywlx}",
    "zhaiyao": "${voucher.zhaiyao}",
    "jffse": "${voucher.jffse}",
    "dffse": "${voucher.dffse}",
    "fjdjs": "${voucher.fjdjs}",
    "pzrq": "${voucher.pzrq}",
    "zd": "${voucher.zd}",
    "jz": "${voucher.jz}",
    "fh": "${voucher.fh}",
    "pzzt": "${voucher.pzzt}",
    "hzpzh": "${voucher.hzpzh}",
    "xspzh": "${voucher.xspzh}",
    "pzlb": "${voucher.pzlb}",
    "wtsm": "${voucher.wtsm}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}
