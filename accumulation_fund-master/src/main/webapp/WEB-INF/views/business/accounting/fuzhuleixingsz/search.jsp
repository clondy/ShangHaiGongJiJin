<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<script>
	var fzlxService;
	$(function() {

		fzlxService = new $.BaseService(
				'#fzlx_query_form',
				'#fzlx_list_datagrid',
				'#fzlx_form_dialog',
				'#fzlx_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/fuzhuleixingsz/" />',
					entityTitle : "辅助核算类型设置",
					optionFormatter : function(value,rec) {
						if(rec.sjzt == 2){
							return '<a href="#" onClick="Xzsp('+ rec.id+ ')">'+'新增经办'+'</a>'
									+ '&nbsp;&nbsp;'
							        +'<a href="#" title="编辑" onClick="fzlxService.dialog.dialog(\'open\').dialog(\'refresh\', \'' 
									+ fzlxService.actionRootUrl 
									+ fzlxService.formUrl + '?entity.id=' + rec.id 
									+ '\');" ><font  color="blue">编辑</font></a>'
									+ '&nbsp;&nbsp;'
									+'<a href="#" onClick="fzlxService.datagrid.datagrid(\'checkRow\', fzlxService.datagrid.datagrid(\'getRowIndex\', '+ rec.id+ '));$(\'#'+ fzlxService.datagridBatchDeleteButtonId+ '\').click();"><font color="red">'+'删除'+'</font></a>'
						}else if(rec.sjzt == 1 || rec.sjzt == 3){
							return '<a href="#" title="编辑" onClick="fzlxService.dialog.dialog(\'open\').dialog(\'refresh\', \'' 
									+ fzlxService.actionRootUrl 
									+ fzlxService.formUrl + '?entity.id=' + rec.id 
									+ '\');" ><font  color="blue">编辑</font></a>'
									+ '&nbsp;&nbsp;'
									+'<a href="#" onClick="fzlxService.datagrid.datagrid(\'checkRow\', fzlxService.datagrid.datagrid(\'getRowIndex\', '+ rec.id+ '));$(\'#'+ fzlxService.datagridBatchDeleteButtonId+ '\').click();"><font color="red">'+'删除'+'</font></a>'
						}else if(rec.sjzt == 4){
							return '<a href="#" onClick="Jyjp('+ rec.id+ ')">'+'禁用经办'+'</a>'
									+ '&nbsp;&nbsp;'
							        +'<a href="#" title="编辑" onClick="fzlxService.dialog.dialog(\'open\').dialog(\'refresh\', \'' 
									+ fzlxService.actionRootUrl 
									+ fzlxService.formUrl + '?entity.id=' + rec.id 
									+ '\');" ><font  color="blue">编辑</font></a>'
									+ '&nbsp;&nbsp;'
									+'<a href="#" onClick="fzlxService.datagrid.datagrid(\'checkRow\', fzlxService.datagrid.datagrid(\'getRowIndex\', '+ rec.id+ '));$(\'#'+ fzlxService.datagridBatchDeleteButtonId+ '\').click();"><font color="red">'+'删除'+'</font></a>'
						}	
					},
					//datagridAddButtonId : 'kmsz_add_button',
					//批量删除
					datagridBatchDeleteButtonId : 'fzlxsz_batch_delete_button', 
					datagridToolbar: [{
						id: 'fzlxsz_batch_delete_button'
						, text: '批量删除'
						, disabled: false
						, iconCls: 'icon-remove'
						, handler: function() {
							var checkedData = {};
							var items = fzlxService.datagrid.datagrid('getChecked');
							for(var i = 0; i < items.length; i++) {
								
								checkedData["ids[" + i + "]"] = items[i].id;
							}
							var dataCount = Object.keys(checkedData).length;
							var prefix = ( 1 < dataCount )? "批量": "";
							if( dataCount ) {
								if( confirm("确定要删除吗？？？") ) {
									$.messager.progress();
									$.ajax({
										url: fzlxService.actionRootUrl + fzlxService.deleteUrl,
										type : "post", 
										dataType : 'json', 
										data : checkedData, 
										success : function(data, response, status) {
							
											if( jQuery.parseJSON('' + data) ) {		
												$.messager.alert('通知',  "删除成功。");
												fzlxService.datagrid.datagrid('clearChecked');
												fzlxService.datagrid.datagrid('reload');
												$.messager.progress('close');
											} else {		
												$.messager.alert('通知',  "删除失败。");
												$.messager.progress('close');
											}
										}
									});
								} else {
			
									fzlxService.datagrid.datagrid('clearChecked');
								}
							} else {
								
								$.messager.alert('通知', "请选中数据后再执行删除。");
							}
							
						}
					}]
					,
					datagridColumns : [{
						field : 'hslxbm',
						title : '核算类别编码',
						width : 220,
						align : 'center'
					}, {
						field : 'hslxmc',
						title : '核算类别名称',
						width : 220,
						align : 'center'
					}, {
						field : 'hsfzsm',
						title : '核算类别分组说明',
						width : 150,
						align : 'center'
					}, {
						field : 'ztbh',
						title : '帐套编号',
						width : 150,
						align : 'center'
					}, {
						field : 'hsdw',
						title : '核算单位',
						width : 150,
						align : 'center'
					}, {
						field : 'hsfl',
						title : '核算分类',
						width : 150,
						align : 'center'
					}, {
						field : 'sjzt',
						title : '数据状态',
						width : 150,
						align : 'center',
							formatter : function(value, rec){
								if(value == 1){
									return '正常使用';
								}else if(value == 2){
									return '新增审批中';
								}else if(value == 3){
									return '停用审批中';
								}else if(value == 4){
									return '停用中';
								}
							}
					} , {
						field : 'spzt',
						title : '审批状态',
						width : 150,
						align : 'center',
						formatter : function(value, rec){
							if(value == 1){
								return '待审批';
							}else if(value == 2){
                       			return '初审通过';
                       	 	}else if(value == 3){
                       		 	return '初审不通过';
                       	 }
						}
					}
					],
					
					dialogWidth : 800,
					dialogHeight : 600,
					dialogButtons : [
										{
											id : 'company_confirm',
											text : '保存',
											handler : function() {
												$.messager.progress();
												if ($('#fzlxsz_edit_form').form('validate')) {

													$.messager.confirm(
																	'确认',
																	'要执行“确认”操作吗？？？',
																	function(r) {
																		if (!r) {
																			$.messager.progress('close');
																			return false;
																		} else {
																			companyHistorySaveOrUpdate();
																		}
																	});
												} else {
													$.messager.progress('close');
												}
												function companyHistorySaveOrUpdate() {
													$("#fzlxsz_edit_form").form('submit',
													{
														url : '${pageContext.request.contextPath}/business/accounting/fuzhuleixingsz/save_or_update.shtml',
														onSubmit : function() {
															var isValid = $('#fzlxsz_edit_form').form('validate');
															if (!isValid) {
																$.messager.progress('close');
															}
															return isValid
														},
														success : function(data) {
															$.messager.progress('close');
															console.log(data)
															data = jQuery.parseJSON(data);
															fzlxService.dialog.dialog('close');
															$('#fzlx_list_datagrid').datagrid('reload');		
															$.messager.alert('通知',"添加成功！");
														}
													});
												}
											}
										},	
								{
											id : 'chongzhi',
											text : '重置',
											//, iconCls: 'icon-save'
											handler : function() {
												if (confirm("要执行“重置”操作吗？？？")) {
													$("#fuhe").linkbutton('disable');
													fzlxService.dialog.dialog('refresh',fzlxService.actionRootUrl+ fzlxService.formUrl);
												}
											}
										},
										{
											text : '取消',
											iconCls : 'icon-cancel',
											handler : function() {
												$(
														$(this).context.parentElement.parentElement).dialog('close');
											}
										} ]
							});
		
	
		//审批历史跳转按钮  #fzlxszspHistory_dialog
		$('#SpHistory_button').click(function() {
		
		var items = fzlxService.datagrid.datagrid('getChecked');
		if(items != 0){
			
		if(items.length <= 1){
		$('#fzlxszspHistory_dialog').dialog(
						{
							title : '审批历史记录',
							href : "${pageContext.request.contextPath}/business/accounting/fuzhuleixingxgls/search.shtml?entity.id="+(items.length > 0 ? items[0].id : 0),
							width : 900,
							height : 600,
							closed : false,
							cache : false,
							resizable : true,
							collapsible : true,
							maximizable : true,
							modal : true,
							buttons : [ {
								text : '关闭',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement).dialog('close');
														}
													}]
												});
						        }else{
						        	$.messager.alert('通知',"只能选择一条数据！");
						        }
						}
						
						else{
							$.messager.alert('通知',"请先选择一条数据！");
						       }
		});
								
		//修改历史
		$('#XgHistory_button').click(function(){
			
			var items = fzlxService.datagrid.datagrid('getChecked');
			if(items != 0){
				
			if(items.length <= 1){
			$('#fzlxszxgHistory_dialog').dialog(
							{
								title : '修改历史记录',
								href : "${pageContext.request.contextPath}/business/accounting/fuzhuleixingspls/search.shtml?entity.id="+(items.length > 0 ? items[0].id : 0),

								width : 900,
								height : 600,
								closed : false,
								cache : false,
								resizable : true,
								collapsible : true,
								maximizable : true,
								modal : true,
								buttons : [ {
									text : '关闭',
									handler : function() {
										$(
												$(this).context.parentElement.parentElement).dialog('close');
															}
														}]
													});
							        }else{
							        	$.messager.alert('通知',"只能选择一条数据！");
							        }
							}
							
							else{
								$.messager.alert('通知',"请先选择一条数据！");
							       }
		});
		
		
		
		$('#fzlx_query_form_search_button').click(function() {
			$('#fzlx_query_form').submit();
		});

		$('#fzlx_chongzhi_form_chongzhi_button').click(function() {
			$('#fzlx_query_form')[0].reset();
		});
	});
	//添加按钮
	$('#fzlxsz_query_form_add_button').click(
			function() {
				fzlxService.dialog.dialog('open').dialog('refresh',fzlxService.actionRootUrl + fzlxService.formUrl);
			});
	
/* 	//新增经办按钮 审批意见
	function Xzsp(id) {
		alert(id)
		$('#sp_dialog').dialog('open').dialog('refresh',"${pageContext.request.contextPath}/business/accounting/fuzhuleixingspls/form2.shtml?entity.hsspid="+id);
	}
	 
	 */
</script>

	<div style="margin: 10px 0;"></div>
	<div style="margin-top: 30px; text-align: center;aligin=center">
		<span> 
			<a class="l-btn" id="fzlx_query_form_search_button">
				<span class="l-btn-left">
					<span class="l-btn-text icon-search l-btn-icon-left">查询</span>
				</span>
			</a>
			&nbsp;&nbsp;
		 </span>
		
		<span> 
			<a class="l-btn" id="fzlxsz_query_form_add_button">
				<span class="l-btn-left">
					<span class="l-btn-text icon-search l-btn-icon-left">新增</span>
				</span>
			</a>
			&nbsp;&nbsp;
		</span> 
		<span> 
			<a class="l-btn" id="SpHistory_button">
				<span class="l-btn-left">
					<span class="l-btn-text icon-search l-btn-icon-left">审批历史</span>
				</span>
			</a>
			&nbsp;&nbsp;
		</span>
		<span> 
			<a class="l-btn" id="XgHistory_button">
				<span class="l-btn-left">
					<span class="l-btn-text icon-search l-btn-icon-left">历史数据</span>
				</span>
			</a>
			&nbsp;&nbsp;
		</span>
		 <span>
		 	<a class="l-btn" id="fzlx_chongzhi_form_chongzhi_button">
		 		<span class="l-btn-left">
		 			<span class="l-btn-text icon-search l-btn-icon-left">重置</span>
		 		</span>
		 	</a>
			&nbsp;&nbsp;
		</span> 
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>

	<div class="text clearfix" style="text-align: center;aligin=center">
		<span> <form:form name="fzlx_query_form" id="fzlx_query_form" method="post" action="" onsubmit="return false;">
				<div>
					<table class="form_view_border" bordercolordark="#FFFFFF" bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0" style="">
						<tr>
						<th class="panel-header"><nobr>核算类别编码</nobr></th>
							<td><form:input path="entity.hslxbm"
									class="form_view_input combo easyui-validatebox" id="input"
									style="height: 27px;" /></td>
						<th class="panel-header"><nobr>核算类别名称</nobr></th>
							<td><form:input path="entity.hslxmc"
									class="form_view_input combo easyui-validatebox" id="input"
									style="height: 27px;" /></td>					
						<th class="panel-header"><nobr>核算分类</nobr></th>
							<td><form:select path="entity.hsfl"
									style="width: 156px;height: 27px;" class=" easyui-combobox"
									data-options="required: true, editable: false">
									<form:option value="">全部核算类型</form:option>
									<form:option value="一元核算">一元核算</form:option>
									<form:option value="二元核算">二元核算</form:option>
									<form:option value="一元核算/二元核算">一元核算/二元核算</form:option>
								</form:select></td>
								<th class="panel-header"><nobr>核算单位名称</nobr></th>
							<td><form:select path="entity.hsdw.id"
									style="width: 156px;height: 27px;" class=" easyui-combobox"
									data-options="required: true, editable: false">
									<form:option value="">--请选择--</form:option>
									<form:options items="${hsdwList}" itemValue="id"
									itemLabel="name"/>
								
								</form:select></td>

						<th class="panel-header"><nobr>审批状态</nobr></th>
							<td><form:select path="entity.sjzt"
									style="width: 156px;height: 27px;" class=" easyui-combobox"
									data-options="required: true, editable: false">
									<form:option value="">全部审批状态</form:option>
									<form:option value="1">正常使用</form:option>
									<form:option value="2">新增审批中</form:option>
									<form:option value="3">停用审批中</form:option>
									<form:option value="4">停用中</form:option>	
								</form:select></td>
						</tr>
					</table>
				</div>
			</form:form>
		</span>
	</div>
	<div id="pzjz_ck_window"></div>
	<table id="fzlx_list_datagrid"></table>
	<!-- 辅助类型设置审批_dialog -->
	<div id="fzlxszspHistory_dialog"></div>
	<div id="fzlxszxgHistory_dialog"></div>
	<div id="fzlx_form_dialog">Dialog Content.</div>
	<div id="changeSite_dialog"></div>
	<div id="sp_dialog"></div>
