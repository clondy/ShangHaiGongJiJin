<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="div_center" id="id" style="height:400px">
	<form:form name="kmsz_edit_form" id="kmsz_edit_form" method="post" action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="kmsz_entity_id" />
		<table class="form_view_border" border="1px" cellpadding="0" cellspacing="0" id="table_id">
		<tr>
		<th class="panel-header">账套名称</th>
				<td><form:input path="entity.ztdm.ztmc"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].ztdm}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
				<th class="panel-header">会计年度</th>
				<td><form:input path="entity.kjnd.id"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].kjnd}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
				</tr>
			<tr>
				<th class="panel-header">余额</th>
				<td>
					<form:input path="entity.ye" 
								class="form_view_input combo easyui-validatebox" 
								data-options="required: true"/>
				</td>
				<th class="panel-header">余额方向</th>
				<td><form:input path="entity.kmyefx"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].kmyefx}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
			</tr>
			<tr>
				<th class="panel-header">本年累计借方</th>
				<td><form:input path="entity.bnljjf"  
								class="form_view_input combo easyui-validatebox"
								data-options="required: true" />
				</td>
				<th class="panel-header">末级标记</th>
				<td><form:input path="entity.sfmjkm"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].sfmjkm}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
			</tr>
			<tr>
				<th class="panel-header">台账标记</th>
				<td><form:input path="entity.tzbj"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].tzbj}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
				<th class="panel-header">科目属性</th>
				<td><form:input path="entity.kmsx"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].kmsx}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
			</tr>
			<tr>
				<th class="panel-header">本年累计贷方</th>
				<td>
					<form:input path="entity.bnljdf" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox"/>
				</td>
					<th class="panel-header">日记账类型</th>
				<td>
					<form:input path="entity.rjzlx"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].rjzlx}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
			</tr>
			<tr>
				<th class="panel-header">科目级别</th>
				<td>
					<form:input path="entity.kmjb"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].kmjb+1}" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
				<th class="panel-header">科目编号</th>
				<td>	
					<form:input path="entity.PKMBH.kmbh"  
								class="form_view_input combo easyui-validatebox" 
								value="${KmszjbsjList[0].kmbh}" 
								readonly="true"  style="background-color:#A1A1A1;width :70px;"
								 />
					<form:input path="entity.kmbh" 
								data-options="required: true,
								validType:[\"isNumber['${pageContext.request.contextPath}/business/accounting/kemushezhi/yzkmbh.shtml','entity.kmbh','账套代码已经被使用']\",\"complexValid['^.{2,3}$','请确认单位名称的长度在2~3个字之间！']\"]" 
								class="form_view_input combo easyui-validatebox"
								style ="width :110px"
								></form:input>
				</td>
			</tr>
			<tr>
				<th class="panel-header">科目名称</th>
				<td>
					<form:input path="entity.kmmc" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox"
								readonly="true" />
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </td>
			    <th class="panel-header">年初余额</th>
				<td>
					<form:input path="entity.ncye" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" />
				</td>
			</tr>
			
		</table>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		 <tr>
        <td style="text-align:right;"><font size="2"><b>辅助核算</b></font></td>
        <td style="text-align:left">
            <span class="radioSpan">
                <input type="radio" name="adminFlag" value="0"><font size="1">核算类别1</font>
                <input type="radio" name="adminFlag" value="1"><font size="1">核算类别2</font>
                <input type="radio" name="adminFlag" value="2"><font size="1">核算类别3</font>
            </span>
        </td>
    </tr>
	</form:form>
</div>

