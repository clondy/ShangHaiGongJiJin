<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
// ...
$rs = mysql_query("select count(*) from item");
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];
 
$rs = mysql_query("select * from item limit $offset,$rows");
 
$items = array();
while($row = mysql_fetch_object($rs)){
	array_push($items, $row);
}
$result["rows"] = $items;
 
echo json_encode($result);
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>	
		<table class="easyui-datagrid" id="tt" title="历史数据列表" style="width:1200px;height:408px"
			data-options="singleSelect:true,collapsible:true,
			url:'<%=request.getContextPath()%>/business/accounting/kemushezhilssj/site_list.shtml?entity.kmszid=${command.entity.kmszid}',
			method:'get'"
			rownumbers="true" pagination="true">
	<thead>
		<tr>
			<th field="zt" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '正在使用';
			}
			else if(value=='2'){
			return '新增审批中';
			}
			else if(value=='3'){
			return '禁用审批中';
			}
			else{
			return '禁用中';
			}
			}">状态<th>
			<th field="jbjg" width=80, rowspan= 1, align='center'>经办机构</th>
			<th field="kmbh" width=80, rowspan= 1, align='center'>科目编号</th>
			<th field="kmmc" width=80, rowspan= 1, align='center'>科目名称</th>
			<th field="kmjb" width=80, rowspan= 1, align='center'>科目级别<th>
			<th field="kmsx" width=80, rowspan= 1, align='center'>科目属性</th>
			<th field="kmyefx" width=80, rowspan= 1, align='center'>余额方向</th>
			<th field="ncye"  width=80, rowspan= 1, align='center'>年初余额</th>
			<th field="bnljjf"  width=80, rowspan= 1, align='center'>本年累计借方<th>
			<th field="bnljdf"  width=80, rowspan= 1, align='center'>本年累计贷方</th>
			<th field="tzbj"  width=80, rowspan= 1, align='center'>台账标记</th>
			<th field="sfmjkm"  width=80, rowspan= 1, align='center'>末级标记</th>
			<th field="rjzlx" width=80, rowspan= 1, align='center'>日记账类型<th>
			<th field="czy"  width=80, rowspan= 1, align='center'>操作员</th>
		</tr>
	</thead>
</table>
</body>
</html>