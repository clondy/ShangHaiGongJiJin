<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="cwhscs" items="${CwhscsXglsList}" varStatus="status">
    {
     "id": "${cwhscs.id}",
     "HSDW": "${cwhscs.hsdw.name}",
     "ZTDM": "${cwhscs.ztdm.ztmc}",
     "SFYXDXTZ": "${cwhscs.sfyxdxtz}",
     "SFYXPZWK": "${cwhscs.sfyxpzwk}",
     "PZBHSCFS": "${cwhscs.pzbhscfs}",
     "SFYXKPZBH": "${cwhscs.sfyxkpzbh}",
     "PZBHCD": "${cwhscs.pzbhcd}",
     "PZBHSCGZ": "${cwhscs.pzbhscgz}",
     "ZDPZZDR": "${cwhscs.zdpzzdr}",
     "DFYXPZFJZ": "${cwhscs.dfyxpzfjz}",
     "SFYXFYJ": "${cwhscs.sfyxfyj}",
     "SFYXQZYJ": "${cwhscs.sfyxqzyj}",
     "SJZT": "${cwhscs.sjzt}",
     "CRSJ": "${cwhscs.crsj}",
     "CWND": "${cwhscs.cwnd}",
     "CZY": "${cwhscs.czy}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]
}

${cwhscs.cwhscsxgls[0].ztdm}