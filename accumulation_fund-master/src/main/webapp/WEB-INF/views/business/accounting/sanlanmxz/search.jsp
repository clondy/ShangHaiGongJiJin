<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var accountService;
    $(function () {
    	accountService = new $.BaseService('#mingxizhang_query_form', '#mingxizhang_list_datagrid'
            , '#mingxizhang_form_dialog', '#mingxizhang_edit_form'
            , {
        	actionRootUrl: '<c:url value="/business/accounting/sanlanmxz/" />'
                 , entityTitle: "三栏明细账"
               , optionFormatter: function(value, rec) {
                    return  '<a href="#" title="修改" onClick="accountService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + accountService.actionRootUrl + accountService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                    + '&nbsp;&nbsp;'
                    + '<a href="#" title="删除" onClick="accountService.datagrid.datagrid(\'checkRow\', accountService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + accountService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
                }               
                ,datagridAddButtonId: 'company_add_button'
                , datagridBatchDeleteButtonId: 'company_batch_delete_button'
                , datagridToolbar: []
                , datagridHasFrozenColumns: false
                , mydatagridColumns:[[                     
				    {field: 'riqi', title: '日期', width: 220, rowspan: 2, align: 'center'}, 
 					{field: 'pzbm', title: '凭证编码', width: 220, rowspan: 2, align: 'center'},
 					{field: 'zhaiyao', title: '摘要', width: 400, rowspan: 2, align: 'center'},
 					{field: 'dykm', title: '对应科目', width: 160, rowspan: 2, align: 'center'},
 					{field: 'jffse', title: '借方发生额', width: 220, rowspan: 2, align: 'center'},
 					{field: 'dffse', title: '贷方发生额', width: 200, rowspan: 2, align: 'center'} ,
					{field: 'yefx', title: '方向', width: 110, rowspan: 2, align: 'center'},
					{field: 'yue', title: '余额', width: 150, rowspan: 2, align: 'center'} 
                ] ]
                , dialogWidth: 700
                , dialogHeight: 500
                , dialogButtons:[
                    {
                        id: 'mingxizhang_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();

                        if($('#mingxizhang_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                    companyHistorySaveOrUpdate();

                            }
                        }else{
                            $.messager.progress('close');
                        } 
                        function companyHistorySaveOrUpdate(){
                            $("#mingxizhang_edit_form").form('submit',{
                                url: '${pageContext.request.contextPath}/business/accounting/account/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#mingxizhang_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    accountService.dialog.dialog('close');
                                    $('#mingxizhang_query_form').submit();
                                    if (data) {
                                        $('#mingxizhang_entity_id').val(data.id);

                                        $.messager.alert('通知', "添加成功！");
                                        $.messager.progress('close');
                                       // $('#mingxizhang_audit').linkbutton('enable');
                                    } else {

                                        if(data.flag == "one"){
                                            $.messager.alert('通知',data.errMsg );
										}
                                        if(data.flag == "two"){
                                            $.messager.alert('通知',data.errMsg );
                                        }
                                        $.messager.alert('通知', "添加失败！");
                                    }
                                }
                            });
                        }
                    }
                    }
                ]
            });

        $('#chaxun_search_button').click(function () {
            $('#mingxizhang_query_form').submit();
        });
    });

	//导出Excel
	$('#export4_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#mingxizhang_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/sanlanmxz/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}
	
</script>



<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span >
			<form:form name="mingxizhang_query_form" id="mingxizhang_query_form"
                       method="post" action="" onsubmit="return false;">   
          <span style="algin: center"> 
          <a class="l-btn" id="chaxun_search_button"><span class="l-btn-left">
          <span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		  &nbsp;&nbsp; 
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
		  <span class="l-btn-text">重置</span></span></a>
	      </span> 
	      &nbsp;&nbsp;
		<a class="l-btn" id="export4_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
  <hr/>
  <table>
	 <tr height="15px" >
		               <th class="panel-header">科目名称</th>
				       <td>
        				   <form:select path="entity.kmmc.kmbh"
									 style="width: 156px;height: 30px;"
									 class="easyui-combobox">
							  <form:option value="" >--请选择--</form:option>
							  <form:options items="${kmmcList}" itemValue="kmbh" itemLabel="kmmc"/>
						   </form:select>
                       </td>
                       <td>&nbsp;&nbsp;&nbsp;</td>	
                       <th class="panel-header">记账状态</th>
				       <td>
        				   <form:select path="entity.jzzt"
									 style="width: 156px;height: 30px;"
									 class="easyui-combobox">
							  <form:option value="" >--请选择--</form:option>
							  <form:option value="1" >已记账</form:option>
							  <form:option value="2" >未记账</form:option>
						   </form:select>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <th class="panel-header">开始日期</th>
                        <td>
                        <form:input path="startDate"
										style="width: 156px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
                    	<th class="panel-header">截止日期</th>
						<td>
						<form:input path="endDate"
										style="width: 156px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
		</tr>
		<tr height="5px"><td></td></tr>
   </table>
   <table>
		<tr height="15px" >	
		            <th class="panel-header">摘要</th>				
					<td><form:input path="entity.zhaiyao"
							class="form_view_input combo easyui-validatebox" style="width: 153px;height: 27px;"/>
				    </td>
				    <td>&nbsp;&nbsp;&nbsp;</td>
					<th class="panel-header">金额范围</th>
                    <td><form:input path="startMoney"
							class="form_view_input combo easyui-validatebox" style="width: 156px;height: 29px;"/>
					</td>
                    <td><span>至</span></td>
					<td><form:input path="endMoney"
					 		class="form_view_input combo easyui-validatebox" style="width: 156px;height: 29px;"/>
					</td>	
        </tr>     
	</table>
	<hr>
	<div style="text-align:center;font-size:21px;letter-spacing: 20px;">明细账</div>
	<table style="text-align:left">
	        <tr>
	            <th><b>核算单位 :</b></th> 
	            <td>上海市公积金管理中心</td>
	            <td style="width: 120px;"></td>
	            <th><b>财务账套 :</b></th> 
	            <td>住房公积金财务核算账套</td>
	            <td style="width: 120px;"></td>
	        </tr>
	        <tr>
	           <th><b>科目编号  :</b></th>
	           <td>201</td> 
	            <td style="width: 120px;"></td>
	            <th><b>科目名称 :</b></th> 
	            <td>住房公积金</td>
	            <td style="width: 120px;"></td>
	        </tr>
	</table>
            </form:form>
		</span>
    
	<!-- <table>
		<tr><td height="7px"><td></tr>
		<tr><td height="7px"><td></tr>
	</table> -->
</div>


<table id="mingxizhang_list_datagrid"></table>
<div id="mingxizhang_form_dialog">Dialog Content.</div>
<div id="mingxizhang_confirm_form_dialog">
</div>
<table >
    <tr>
    <td style="width: 820px;"></td>
    <th style="font-size:14px;"><b>合计 :</b></th>
    <td style="text-align:left">贷 9,500.00</td>
    </tr>
</table>