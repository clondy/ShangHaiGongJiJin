<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script>
	var FzyebService;
	$(function() {
		FzyebService = new $.BaseService(
				'#fzyeb_query_form',
				'#fzyeb_list_datagrid',
				'#fzyeb_form_dialog',
				'#fzyeb_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/fzyeb/" />',
					entityTitle : "辅助余额",
					optionFormatter : function(value, rec) {}
					,mydatagridColumns : 
					                     [[
					                       
					                        {field : 'kmbm',title:"科目编码",align : 'center',width:100}, 
					                        {field : 'kmmc',title:"科目名称",align : 'center',width:100},  
					                    	{field : 'kmfx',title:"科目方向",align : 'center',width:100},  
											{field : 'hsxmbm',title:"核算项目编码",align : 'center',width:100},  
											{field : 'hsxmmc',title:"核算项目名称",align : 'center',width:100},  
											{field : 'qcye',title:"期初余额",align : 'center',width:100},
											{field : 'bqjffse',title:"本期借方发生额",align : 'center',width:100},  
											{field : 'bqdffse',title:"本期贷方发生额",align : 'center',width:100},
										    {field : 'ye',title:"余额",align : 'center',width:100}
										    
										 ]],
		                      
					dialogWidth : 800,
					dialogHeight : 600,
					datagridToolbar: []
				});

		$('#fzyeb_query_form_search_button').click(function() {
			$('#fzyeb_query_form').submit();
		});
		
		$('#reset_button').click(function() {
			$('#fzyeb_query_form')[0].reset();
		});
	});

 	$(function () {
            var _mkid = $('#mkid').combobox({
            	url:'${pageContext.request.contextPath}/business/accounting/fzyeb/hslx.shtml',
                editable: false,
                valueField: 'hslxbm',
                textField: 'hslxmc',
                onSelect: function (record) {
                    _zhbid.combobox({
                        disabled: false,
                        url:'${pageContext.request.contextPath}/business/accounting/fzyeb/hsxm.shtml?id=' + record.hslxbm,
                        valueField: 'hsxmbm',
                        textField: 'hsxmmc'
                    }).combobox('clear');
                }
            });
            var _zhbid = $('#zhbid').combobox({
                disabled: true,
                valueField: 'hsxmbm',
                textField: 'hsxmmc'
            });
        });
        
    //导出Excel
	$('#export_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#fzyeb_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/fzyeb/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}
	
</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">

	<span style="algin: center"> 
		<a class="l-btn" id="fzyeb_query_form_search_button"> <span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		<a class="l-btn" id="reset_button"><span class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
			&nbsp;&nbsp;
		<a class="l-btn" id="export_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
	</span>
	<hr/>
		<span> <form:form name="fzyeb_query_form" id="fzyeb_query_form" method="post" action="" onsubmit="return false;">
			<center>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c" border="0px" cellpadding="0" cellspacing="0">
					<tr>
                       <th class="panel-header">财务科目</th>
							<td>
								<form:select path="entity.kmbm.kmbh" style="width: 180px;height: 27px;" class="form_view_input combo easyui-combobox" data-options="required: true, editable: false">
									<form:option value="">--请选择--</form:option>
									<form:options items="${fzyebList}" itemValue="kmbh" itemLabel="kmmc" />
								</form:select>
							</td>	
							<td>&nbsp;&nbsp;</td>
							<th class="panel-header">核算类型</th>
								 <td style="width: 160px">
	                                 <input class="easyui-combobox" id="mkid" style="width: 180px;height: 27px;"  data-options="valueField:'hslxbm', textField:'hslxmc', panelHeight:'auto'" >  
	                            </td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th class="panel-header">核算项目</th>
							<td style="width: 160px">               
                                <input class="easyui-combobox" id="zhbid" name = "entity.hsxmbm.hsxmbm" style="width: 180px;height: 27px;" data-options="valueField:'hsxmbm', textField:'hsxmmc', panelHeight:'auto'" >  
                            </td>
						<td>&nbsp;&nbsp;</td>
						
					 <th class="panel-header">开始日期</th>
                        <td>
                        	<form:input path="starttime"
										style="width: 180px;height: 27px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
                    	<td style="text-align:center"><span>至</span></td>
						<td>
							<form:input path="endtime"
										style="width: 180px;height: 27px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
					</tr>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>
</div>

<table id="fzyeb_list_datagrid"></table>
<div id="fzyeb_form_dialog">Dialog Content</div>
