<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="cwhscs" items="${CwhscsList}" varStatus="status">
    {
     "id": "${cwhscs.id}",
     "lsid": "${cwhscs.cwhscsxgls[0].id}",
     "HSDW": "${cwhscs.cwhscsxgls[0].hsdw.name}",
     "ZTDM": "${cwhscs.cwhscsxgls[0].ztdm.ztmc}",
     "SFYXDXTZ": "${cwhscs.cwhscsxgls[0].sfyxdxtz}",
     "SFYXPZWK": "${cwhscs.cwhscsxgls[0].sfyxpzwk}",
     "PZBHSCFS": "${cwhscs.cwhscsxgls[0].pzbhscfs}",
     "SFYXKPZBH": "${cwhscs.cwhscsxgls[0].sfyxkpzbh}",
     "PZBHCD": "${cwhscs.cwhscsxgls[0].pzbhcd}",
     "PZBHSCGZ": "${cwhscs.cwhscsxgls[0].pzbhscgz}",
     "ZDPZZDR": "${cwhscs.cwhscsxgls[0].zdpzzdr}",
     "DFYXPZFJZ": "${cwhscs.cwhscsxgls[0].dfyxpzfjz}",
     "SFYXFYJ": "${cwhscs.cwhscsxgls[0].sfyxfyj}",
     "SFYXQZYJ": "${cwhscs.cwhscsxgls[0].sfyxqzyj}",
     "SJZT": "${cwhscs.cwhscsxgls[0].sjzt}",
     "CRSJ": "${cwhscs.cwhscsxgls[0].crsj}",
     "CWND": "${cwhscs.cwhscsxgls[0].cwnd}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]
}

${cwhscs.cwhscsxgls[0].ztdm}