<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="cwhscs" items="${CwhscsSplsList}" varStatus="status">
    {
     "id": "${cwhscs.id}",
     "sjzt": "${cwhscs.sjzt}",
     "spzt": "${cwhscs.spzt}",
     "spyj": "${cwhscs.spyj}",
     "czy": "${cwhscs.czy}",
     "czsj": "${cwhscs.czsj}",
     "spr": "${cwhscs.spr}",
     "spsj": "${cwhscs.spsj}",
     "cwhscsxgls": "${cwhscs.cwhscsxgls}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]
}

