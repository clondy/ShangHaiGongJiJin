<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="div_center" style="margin-top: 0px">
	<form:form name="fuzhuhesuan_edit_form" id="fuzhuhesuan_edit_form" method="post"
		style="width:800px" action="" onsubmit="return false;">
		<form:hidden path="entity.id" />
		<form:hidden path="entity.hsid" />
		<form:hidden path="entity.crsj" />
		
		<center>
			<table bordercolordark="#45b97c" bordercolorlight="#45b97c"
				border="0px" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">核算项目编码</th>
					<td><form:input path="entity.hsxmbm" data-options="required: true "
							style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"></form:input></td>
						<th class="panel-header">辅助核算类型</th>
							<td><form:select path="entity.hslxbm.hslxbm"
							style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:options items="${fzlxszList}" itemValue="hslxbm" itemLabel="hslxmc" />
						</form:select></td>
				</tr>

				<tr>
					<th class="panel-header">核算项目名称</th>
					<td><form:input path="entity.hsxmmc" data-options="required: true "
							style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"></form:input></td>
					<th class="panel-header">会计年度</th>
							<td><form:select path="entity.kjnd.id"
							style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:options items="${kjndList}" itemValue="id" itemLabel="name" />
							
						</form:select></td>
				</tr>
				<tr>
				<th class="panel-header">是否低级明细</th>
					<td><form:select path="entity.sfdjmx" style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:option value="1">是</form:option>
			     			<form:option value="2">否</form:option>
						</form:select></td>
				
				
				</tr>
			</table>
		</center>
	</form:form>


</div>
