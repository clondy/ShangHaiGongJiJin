<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>


    var fuzhuzhangService;
    $(function () {
    	 fuzhuzhangService = new $.BaseService('#fuzhuzhang_query_form', '#fuzhuzhang_list_datagrid'
            , '#fuzhuzhang_form_dialog', '#fuzhuzhang_form_search_button'
            , {
        	actionRootUrl: '<c:url value="/business/accounting/fuzhumxz/" />'
                 , entityTitle: "辅助明细账" ,
                datagridHasFrozenColumns:false
                , mydatagridColumns: [[
                	{field: 'riqi', title: '日期', width: 220, rowspan: 2, align: 'center'},
				    {field: 'hsxmbm', title: '核算项目编码', width: 220, rowspan: 2, align: 'center'},
 					{field: 'hsxmmc', title: '核算项目名称', width: 220, rowspan: 2, align: 'center'},
 					{field: 'pzbh', title: '凭证编号', width: 220, rowspan: 2, align: 'center'},
 					{field: 'zhaiyao', title: '摘要', width: 190, rowspan: 2, align: 'center'},
 					{field: 'jffse', title: '借方发生额', width: 220, rowspan: 2, align: 'center'},
 					{field: 'dffse', title: '贷方发生额', width: 200, rowspan: 2, align: 'center'} ,
					{field: 'yefx', title: '方向', width: 200, rowspan: 2, align: 'center'},
					{field: 'yue', title: '余额', width: 150, rowspan: 2, align: 'center'}
                ] ]
                , dialogWidth: 800
                , dialogHeight: 500
                ,datagridToolbar:[]
        
     });
 });	
    $('#zhizuo_search_button').click(function () {
        $('#fuzhuzhang_query_form').submit();
    });
    
    //导出Excel
	$('#export2_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#fuzhuzhang_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/fuzhumxz/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}
	
</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span>
			<form:form name="fuzhuzhang_query_form" id="fuzhuzhang_query_form"
                       method="post" action="" onsubmit="return false;">
          <span style="algin: center"> 
		  <a class="l-btn" id="zhizuo_search_button"><span class="l-btn-left">
          <span class="l-btn-text ">制作</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text ">重置</span></span></a>
          </span>
           &nbsp;&nbsp;
		<a class="l-btn" id="export2_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
          <hr>
		<table>
			<tr height="15px" >
                   <th class="panel-header">科目名称</th>
                   <td>    				
					<form:select path="entity.kmmc.kmbh"
                                 style="width: 156px;height: 30px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false">
                    <form:option value="" >--请选择--</form:option>
					<form:options items="${kmmcList}" itemValue="kmbh" itemLabel="kmmc"/>
                    </form:select>
                   </td>
                   <td>&nbsp;&nbsp;&nbsp;</td>
                   <th class="panel-header">辅助类别</th>
                   <td>    				
     			    <form:select path="entity.fzlb.hslxbm"
                                 style="width: 156px;height: 30px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false">
                    <form:option value="" >--请选择--</form:option>
					<form:options items="${fzlbList}" itemValue="hslxbm" itemLabel="hslxmc"/>
                    </form:select>
	  			   </td>
	  			   <td>&nbsp;&nbsp;&nbsp;</td>
	  			   <th class="panel-header">核算项目</th>
	  			   <td>    				
     			    <form:select path="entity.hsxmmc.hsxmbm"
                                 style="width: 156px;height: 30px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false">
                    <form:option value="" >--请选择--</form:option>
					<form:options items="${hsxmmcList}" itemValue="hsxmbm" itemLabel="hsxmmc"/>
                    </form:select>	  			   
	  			   </td>
	  			   <td>&nbsp;&nbsp;&nbsp;</td>
	  			   <th class="panel-header">摘要</th>				
					<td><form:input path="entity.zhaiyao"
							class="form_view_input combo easyui-validatebox" style="width: 156px;height: 30px;"/>
				    </td>	  			  
	  	</tr>
	 <tr height="5px"><td></td></tr>
	 </table>
	 <table>
	  	<tr>
	  			       <th class="panel-header">开始日期</th>
                        <td>
                        <form:input path="startDate"
										style="width: 153px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
                    	<th class="panel-header">截止日期</th>
						<td>
						<form:input path="endDate"
										style="width: 153px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
					  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					  <th class="panel-header">记账状态</th>
					  <td> 
					   <form:select path="entity.jzzt"
                                 style="width: 153px;height: 30px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false">
                              <form:option value="" >--请选择--</form:option>
							  <form:option value="1" >已记账</form:option>
							  <form:option value="2" >未记账</form:option>
                       </form:select>
                     </td>
		</tr>	
	</table>
	<hr>
	<div style="text-align:center;font-size:21px;letter-spacing: 20px;">辅助明细账</div>
	<table style="text-align:left">
	        <tr>
	            <th><b>核算单位 :</b></th> 
	            <td>上海市公积金管理中心</td>
	            <td style="width: 120px;"></td>
	            
	            <th><b>财务账套 :</b></th>
	            <td >住房公积金财务核算账套</td> 
	            <td style="width: 120px;"></td>
	        </tr>
	        <tr>
	           <th><b>科目编号  :</b></th>
	           <td>201</td>
	            <td style="width: 120px;"></td>
	            
	            <th><b>科目名称 :</b></th>
	            <td>住房公积金</td> 
	            <td style="width: 120px;"></td>
	            
	             <th><b>辅助类别 :</b></th> 
	             <td>全部</td>
	            <td style="width: 120px;"></td>
	        </tr>
	</table>
       </form:form>
		</span>
</div>

<table id="fuzhuzhang_list_datagrid"></table>
<div id="fuzhuzhang_form_dialog">Dialog Content.</div>
<div id="fuzhuzhang_confirm_form_dialog">

</div>
