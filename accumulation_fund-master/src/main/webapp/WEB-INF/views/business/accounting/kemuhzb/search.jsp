<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var accountService;
    $(function () {
    	accountService = new $.BaseService(
    			'#huizongbiao_query_form',
    			'#huizongbiao_list_datagrid',
                '#huizongbiao_form_dialog', 
                '#huizongbiao_edit_form'
            , {
        	actionRootUrl: '<c:url value="/business/accounting/kemuhzb/" />'
                 , entityTitle: "科目汇总"
               , optionFormatter: function(value, rec) {
                    return  '<a href="#" title="修改" onClick="accountService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + accountService.actionRootUrl + accountService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                    + '&nbsp;&nbsp;'
                    + '<a href="#" title="删除" onClick="accountService.datagrid.datagrid(\'checkRow\', accountService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + accountService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
                }               
                ,datagridAddButtonId: 'company_add_button'
                , datagridBatchDeleteButtonId: 'company_batch_delete_button'
                , datagridToolbar: []
                , datagridHasFrozenColumns: false
                , mydatagridColumns:[[                     
				    {field: 'kmbh', title: '科目编号', width: 220, rowspan: 2, align: 'center'}, 
 					{field: 'kmmc', title: '科目名称', width: 400, rowspan: 2, align: 'center'},
 					{field: 'qcye', title: '期初余额', width: 400, rowspan: 2, align: 'center'},
 					{field: 'jffse', title: '借方发生额', width: 220, rowspan: 2, align: 'center'},
 					{field: 'dffse', title: '贷方发生额', width: 220, rowspan: 2, align: 'center'},
 					{field: 'yue', title: '余额', width: 400, rowspan: 2, align: 'center'}
                ] ]
                , dialogWidth: 700
                , dialogHeight: 500
                , dialogButtons:[
                    {
                        id: 'company_account_fund_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();

                        if($('huizongbiao_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                    companyHistorySaveOrUpdate();

                            }
                        }else{
                            $.messager.progress('close');
                        } 
                        function companyHistorySaveOrUpdate(){
                            $("#huizongbiao_edit_form").form('submit',{
                                url: '${pageContext.request.contextPath}/business/accounting/account/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#huizongbiao_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    accountService.dialog.dialog('close');
                                    $('#huizongbiao_query_form').submit();
                                    if (data) {
                                        $('#huizongbiao_entity_id').val(data.id);

                                        $.messager.alert('通知', "添加成功！");
                                        $.messager.progress('close');
                                       // $('#company_account_fund_audit').linkbutton('enable');
                                    } else {

                                        if(data.flag == "one"){
                                            $.messager.alert('通知',data.errMsg );
										}
                                        if(data.flag == "two"){
                                            $.messager.alert('通知',data.errMsg );
                                        }
                                        $.messager.alert('通知', "添加失败！");
                                    }
                                }
                            });
                        }
                    }
                    }
                ]
            });

        $('#chaxun_search_button').click(function () {
            $('#huizongbiao_query_form').submit();
        });
    });

	//导出Excel
	$('#export3_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#huizongbiao_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/kemuhzb/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}
	
</script>



<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span >
			<form:form name="huizongbiao_query_form" id="huizongbiao_query_form"
                       method="post" action="" onsubmit="return false;">  
          <span style="algin: center"> 
          <a class="l-btn" id="chaxun_search_button"><span class="l-btn-left">
          <span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		  &nbsp;&nbsp; 
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
		  <span class="l-btn-text">重置</span></span></a>
	      </span>
	      &nbsp;&nbsp;
		<a class="l-btn" id="export3_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
	      <hr>
    <table >
	 <tr height="15px" >                   
                        <th class="panel-header">统计年月</th>
                         <td>
                        	<form:input path="tjny"
										style="width: 180px;height: 27px;"
										data-options="required: true"
										class="Wdate"
										value=""
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
		</tr>
   </table>
   <hr/>
	<div style="text-align:center;font-size:21px;letter-spacing: 20px;">科目汇总表</div>
	<table>
	        <tr>
	            <th><b>核算单位 :</b></th>
	            <td>上海市公积金管理中心</td> 
	            <td style="width: 120px;"></td>
	            <th><b>财务账表 :</b></th> 
	            <td>科目汇总表</td>
	            <td style="width: 120px;"></td>
	        </tr>
	</table>
            </form:form>
		</span>
    
<!-- 	<table>
		<tr><td height="7px"><td></tr>
		<tr><td height="7px"><td></tr>
	</table> -->
</div>


<table id="huizongbiao_list_datagrid"></table>
<div id="huizongbiao_form_dialog">Dialog Content.</div>
<div id="huizongbiao_confirm_form_dialog">
</div>
<table>
    <tr>
    <th style="font-size:14px;"><b>财务主管 :</b></th>
    <td>王建</td>
    <td style="width: 320px;"></td>
    <th style="font-size:14px;"><b>制表人 :</b></th>
    <td>李平</td>
    </tr>
</table>