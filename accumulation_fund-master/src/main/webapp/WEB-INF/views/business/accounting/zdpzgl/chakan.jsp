<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	    <div style="margin:20px 0;"></div>
	<div align="center"><h2>记账凭证</h2></div>
    	<div style="margin:20px 0;"></div>
	<table class="form_view_border" border="1px" cellpadding="0" cellspacing="0" >
		<tr>
				<th class="panel-header" align="center">核算单位</th>
				<td><input path=""  
								class="form_view_input combo easyui-validatebox" />
				</td>
				<th class="panel-header" align="center">附单据数</th>
				<td><input path=""  
								class="form_view_input combo easyui-validatebox" />
				</td>
				<th class="panel-header" align="center">显示凭证号</th>
				<td><input path=""  
								class="form_view_input combo easyui-validatebox" />
				</td>
				<th class="panel-header" align="center">业务类型</th>
				<td><input path=""  
								class="form_view_input combo easyui-validatebox" />
				</td>
			</tr>
			</table>
		<div style="margin:20px 0;"></div>
	<table class="easyui-datagrid"  style="width:1200px;height:326px"
			data-options="singleSelect:true,collapsible:true,
			url:'<%=request.getContextPath()%>/business/accounting/zdpzgl/jizhangpingzheng?ID=${ID}',
			method:'get'">
		<thead>
			<tr>
				<th data-options="field:'FLXH',width:200,align:'center'">分录序号</th>
				<th data-options="field:'ZHAIYAO',width:200,align:'center'">摘要</th>
				<th data-options="field:'KMMC',width:200,align:'center'">科目名称</th>
				<th data-options="field:'KMBH',width:200,align:'center'">科目编号</th>
				<th data-options="field:'JFFSE',width:200,align:'center'">借方发生额</th>
				<th data-options="field:'DFFSE',width:200,align:'center'">贷方发生额</th>
			</tr>
		</thead>
	</table>
</body>
</html>