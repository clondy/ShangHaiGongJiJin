<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="company" items="${KmszxglssjList}" varStatus="status">
    {"id": "${company.id}",
    "spzt": "${company.hsdw}",
    "ztdm": "${company.ztdm}",
    "kmbh": "${company.kmbh}",
    "kmmc": "${company.kmmc}",
    "kmjb": "${company.kmjb}",
    "kmsx": "${company.kmsx}",
    "kjkmlb": "${company.kjkmlb}",
    "kmyefx": "${company.kmyefx}",
    "sfmjkm": "${company.sfmjkm}",
    "ncye": "${company.ncye}",
    "ye": "${company.ye}",
    "bnljjf": "${company.bnljjf}",
    "bnljdf": "${company.bnljdf}",
    "tzbj": "${company.tzbj}",
    "rjzlx": "${company.rjzlx}",
    "fzhslx": "${company.fzhslx}",
    "zt": "${company.zt}",
    "crsj": "${company.crsj}",
     "czy": "${company.czy}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}