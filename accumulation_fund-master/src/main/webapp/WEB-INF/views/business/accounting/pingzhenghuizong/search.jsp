<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script>
function openwindows(){
	 var win = $('#pzjz_ck_window').window({
		    title:'记账凭证查看',
		    width:1000,
		    height:600,
		 	href:'${pageContext.request.contextPath}/business/accounting/pingzhengjizhang/site_record.shtml?entity.xspzh=00001',
		    modal:true
		});
	 
}
function shuju(){
	 var win = $('#pzjz_ck_window').window({
	    title:'辅助核算查看',
	    width:1000,
	    height:600,
	 	href:'${pageContext.request.contextPath}/business/accounting/pingzhengfuhe/shuju.shtml?entity.xspzh=00001',
	    modal:true
	});
}	
</script>
<div style="margin: 10px 0;"></div>
<div style="margin-top: 30px; margin-left: 330px">
	<span> 
		<a class="l-btn" id="pzcx_query_form_search_button">
			<span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span>
		</a>
	</span>	
	<span> 
		<a class="l-btn" id="pzcx_query_form_search_button">
			<span class="l-btn-left"><span class="l-btn-text icon-reload l-btn-icon-left">重置</span></span>
		</a>
	</span>	
	<span> 
		<a class="l-btn" id="pzcx_query_form_search_button">
			<span class="l-btn-left"><span class="l-btn-text icon-ok  l-btn-icon-left">汇总</span></span>
		</a>
	</span>	
	<span> 
		<a class="l-btn" id="pzcx_query_form_search_button">
			<span class="l-btn-left"><span class="l-btn-text icon-cancel l-btn-icon-left">取消汇总</span></span>
		</a>
	</span>	
	<table><tr><td height="30px"></td></tr></table>
</div>
<div class="text clearfix" style="text-align: center;">
	<span> <form:form name="pzcx_query_form" id="pzcx_query_form" method="post" action="" onsubmit="return false;">
			<div style="float: left">
				<table class="form_view_border" bordercolordark="#FFFFFF" bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0" style="">
					<tr>
						<th class="panel-header">核算单位</th>
						<td><form:select path="" class="form_view_input combo easyui-combobox" data-options="required: false, editable: false, disabled: false">
								<form:option value="1">上海公积金管理中心</form:option>  
							</form:select>
						</td>
						<th class="panel-header">账套名称</th>
						<td>
							<form:select path="" class="form_view_input combo easyui-combobox" data-options="required: false, editable: false, disabled: false">
								<form:option value="0">公积金账套</form:option>
							</form:select>
						</td>
						<th class="panel-header">汇总日期</th>
						<td>
							<form:input path="" class="form_view_input combo easyui-datebox" />
						</td>
						<th class="panel-header">凭证类别</th>
						<td>
							<form:select path="" class="form_view_input combo easyui-combobox" data-options="required: false, editable: false, disabled: false">
								<form:option value="0">记账凭证</form:option>
							</form:select>
						</td>	
					</tr>
				</table>
			</div>
		</form:form>
	</span>
</div>
<div class="easyui-tabs"  data-options="fit:true">
        <div title="待汇总记账凭证" style="padding:10px">
      	<table id="dhzpzgrid" class="easyui-datagrid"  
      				data-options="rownumbers:true,singleSelect:true,fit:true"   pagination="true" url="/fund.accumulation/testDate/dhzpz.json">
		        <thead>
		            <tr>
		                <th data-options="field:'ck',checkbox:true"></th>
		                <th data-options="field:'zhaiyao',width:80">摘要</th>
		                <th data-options="field:'jffse',width:100,align:'right'" >借方发生额</th>
		                <th data-options="field:'dffse',width:100,align:'right'">贷方发申额</th>
		                <th data-options="field:'fjdjs',width:80,align:'left'">附件单据数</th>
		                <th data-options="field:'jzrq',width:90">记账日期</th>
		                <th data-options="field:'zd',width:60,align:'center'">制单</th>
		                <th data-options="field:'jz',width:60,align:'center'">记账</th>
		                <th data-options="field:'fh',width:60,align:'center'">复核</th>
		                <th data-options="field:'pzztbj',width:60,align:'center'">凭证状态标记</th>
		                <th data-options="field:'hzpzh',width:60,align:'center'">汇总凭证号</th>
		                <th data-options="field:'pzlb',width:60,align:'center'">凭证类别</th>
		                <th data-options="field:'cz',width:60,align:'center'">操作</th>
		            </tr>
		        </thead>
	    </table>
        </div>
        <div title="已汇总记账凭证" style="padding:10px">
          	<table id="yhzjzpz" class="easyui-datagrid"  url="/fund.accumulation/testDate/yhzpz.json"
          		  pagination="true"
         		  title="汇总凭证"
          		 data-options="rownumbers:true,singleSelect:true,onLoadSuccess:function(index, data){
          		 	$('#yhzjzpz').datagrid('selectRow', 0);
          		 }">
		        <thead>
		            <tr>
		                <th data-options="field:'ck',checkbox:true"></th>
		                <th data-options="field:'zhaiyao',width:80">摘要</th>
		                <th data-options="field:'jffse',width:100,align:'right'" >借方发生额</th>
		                <th data-options="field:'dffse',width:100,align:'right'">贷方发申额</th>
		                <th data-options="field:'fjdjs',width:80,align:'right'">附件单据数</th>
		                <th data-options="field:'jzrq',width:90">记账日期</th>
		                <th data-options="field:'zd',width:60,align:'center'">制单</th>
		                <th data-options="field:'jz',width:60,align:'center'">记账</th>
		                <th data-options="field:'fh',width:60,align:'center'">复核</th>
		                <th data-options="field:'pzztbj',width:60,align:'center'">凭证状态标记</th>
		                <th data-options="field:'hzpzh',width:60,align:'center'">汇总凭证号</th>
		                <th data-options="field:'pzlb',width:60,align:'center'">凭证类别</th>
		            </tr>
		        </thead>
	     	</table>
	     	<br>
	     	<table  id="jzpz" class="easyui-datagrid"  pagination="true" url="/fund.accumulation/testDate/pzmx.json"
	     	 	title="记账凭证"
	     	 	data-options="rownumbers:true,singleSelect:true">
		        <thead>
		            <tr>
		                <th data-options="field:'ck2',checkbox:true"></th>
		                <th data-options="field:'zhaiyao',width:80">摘要</th>
		                <th data-options="field:'jffse',width:100,align:'right'" >借方发生额</th>
		                <th data-options="field:'dffse',width:100,align:'right'">贷方发申额</th>
		                <th data-options="field:'fjdjs',width:80,align:'right'">附件单据数</th>
		                <th data-options="field:'pzrq',width:90">记账日期</th>
		                <th data-options="field:'zd',width:60,align:'center'">制单</th>
		                <th data-options="field:'jz',width:60,align:'center'">记账</th>
		                <th data-options="field:'fh',width:60,align:'center'">复核</th>
		                <th data-options="field:'pzztbj',width:60,align:'center'">凭证状态标记</th>
		                <th data-options="field:'hzpzh',width:60,align:'center'">汇总凭证号</th>
		                <th data-options="field:'pzlb',width:60,align:'center'">凭证类别</th>
		            </tr>
		        </thead>
	     	</table>
        </div>
    </div>
<div id="pzjz_ck_window"></div>