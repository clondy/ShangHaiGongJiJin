<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [

<c:forEach var="Ztsz" items="${ZtxglssjList}" varStatus="status">
    {"id": "${Ztsz.id}",
    "ztid": "${Ztsz.ztid}",
    "ztmc": "${Ztsz.ztmc}" ,
    "ztdm": "${Ztsz.ztdm}" ,
    "ztms": "${Ztsz.ztms}" ,
    "hsdw": "${Ztsz.hsdw.name}",
    "sjzt": "${Ztsz.sjzt}",
    "crsj": "${Ztsz.crsj}"
    
     }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
] }
 