<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Ztsz" items="${ZtxglssjList}" varStatus="status">
   <c:if test="${ Ztsz.ztsplssjList[0] != null }">
	    {"id": "${Ztsz.id}",
	    "ztmc": "${Ztsz.ztmc}" ,
	    "spyj": "${Ztsz.ztsplssjList[0].spyj}" ,
	    "spzt": "${Ztsz.ztsplssjList[0].spzt}" ,
	    "hsdw": "${Ztsz.hsdw.name}",
	    "czy": "${Ztsz.ztsplssjList[0].czy}",
	    "sjzt": "${Ztsz.ztsplssjList[0].sjzt}",
	    "czsj": "${Ztsz.ztsplssjList[0].czsj}"
       }
      </c:if>   
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
] }

