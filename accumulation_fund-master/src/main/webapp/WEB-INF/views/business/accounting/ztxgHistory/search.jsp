<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>




<script>

	//修改历史
    var XgHistoryService;
    $(function () {
    	XgHistoryService = new $.BaseService('#XG_query_form', '#xg_list'
    	            , '#XG_form_dialog', '#XG_edit_form'
    	            , {
    		              listUrl:'list.shtml?entity.ztid=${command.entity.ztid}'
    	                 ,actionRootUrl: '<c:url value="/business/accounting/ztxgHistory/"/>'
    	                 , entityTitle: "账套"
    	                 , datagridToolbar: []
    				     , datagridHasFrozenColumns: false
             	   	    , mydatagridColumns: [[
                   
                   {field: 'ztmc', title: '账套名称', width: 100, align: 'center'},
                   {field: 'ztdm', title: '账套代码', width: 100, align: 'center'},
                   {field: 'ztms', title: '账套描述', width: 100, align: 'center'},
                   {field: 'hsdw', title: '核算单位', width: 100, align: 'center'},
                   {field: 'sjzt', title: '数据状态', width: 100, align: 'center'},
                   {field: 'crsj', title: '插入时间', width: 100, align: 'center'}
               ]] 
                , dialogWidth: 800
                , dialogHeight: 600
            });
    });


</script>

<table id="xg_list"></table>
