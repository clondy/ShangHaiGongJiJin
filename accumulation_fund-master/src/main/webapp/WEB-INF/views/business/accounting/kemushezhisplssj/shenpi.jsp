<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">
function EditsubmitForm(){
    $("#kmszsp_edit_form").form('submit', {
    	url:"<%=request.getContextPath()%>/business/accounting/kemushezhisplssj/shenpi.shtml",
            onSubmit: function () {  
            	var isValid = $(this).form('validate');  
            	if (!isValid) {
					$.messager.progress('close');
				}
            	return isValid
            },
            success : function(data) {
            	if(data){
				$('#kmsz_list_datagrid').datagrid('reload');
				$.messager.alert('通知',"审核成功！");
				$('#lssj_ck_window').window('close');
            	}else {  
            		$.messager.alert('通知',"审核失败！");
            		$('#lssj_ck_window').window('close');
                }  
    }
    });
};
function EditsubmitForm123(){
    $("#kmszsp_edit_form").form('submit', {
    	url:"<%=request.getContextPath()%>/business/accounting/kemushezhisplssj/shenpino.shtml",
            onSubmit: function () {  
            	var isValid = $(this).form('validate');  
            	if (!isValid) {
					$.messager.progress('close');
				}
            	return isValid
            },
            success : function(data) {
            	if(data){
				$('#kmsz_list_datagrid').datagrid('reload');
				$.messager.alert('通知',"审核成功！");
				$('#lssj_ck_window').window('close');
            	}else {  
            		$.messager.alert('通知',"审核失败！");
            		$('#lssj_ck_window').window('close');
                }  
    }
    });
};
$('#quxiao_button').click(function () {
	$('#lssj_ck_window').window('close');  
                                          });
</script>
<p>请输入审批意见<p>
<form:form name="kmszsp_edit_form" id="kmszsp_edit_form" method="post" action="" onsubmit="return false;">
		<form:hidden path="entity.kmszls"  />
	  <form:textarea path="entity.spyj"  ></form:textarea> 
	</form:form>
	  <div style="center"> 
       <span> <a class="l-btn"
			id="kmsz_chongzhi_form_chongzhi_button" onclick="EditsubmitForm()"><span class="l-btn-left"><span
					>通过</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="kmsz_chongzhi_form_chongzhi_button" onclick="EditsubmitForm123()"><span class="l-btn-left"><span
					>不通过</span></span></a>
			&nbsp;&nbsp;
		</span> 
		<span> <a class="l-btn" id="quxiao_button" onclick="add()"><span
				class="l-btn-left"><span
					>取消</span></span></a>
				&nbsp;&nbsp;
		</span>
              </div> 
	
