<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div  align="center" >
	<font size="6">记账凭证</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<font>核算单位:</font>上海公积金管理中心&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<font>2017年3月10日</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<font>附件单据数:</font>${VoucherList[0].fjdjs}
	 <table  rules=all  style="width:800px;text-align:center;cellspacing=0;cellpadding=0;border: 1 solid;"  >
	 <tr height="10px">
	 	<td>摘要</td>
	 	<td>科目编号</td>
	 	<td>科目名称</td>
	 	<td>借方发生额</td>
	 	<td>贷方发生额</td>
	 </tr>
		 <tr height="20px">
		 <c:forEach var="voucher" items="${VoucherList}" varStatus="status">
		 	<td>${voucher.zhaiyao}</td>
	    	<td>${voucher.kmbh}</td>
	    	<td>${voucher.kmmc}</td>
	    	<td style="text-align:right;" >${voucher.jffse}</td>
	    	<td style="text-align:right;" >${voucher.dffse}</td>
		 </c:forEach>	
		 </tr>
	 <tr height="20px"> 
	 	<td>合计	</td>
	 	<td colspan=2>(大写):贰仟元整</td>
	 	<td style="text-align:right;">0</td>
	 	<td style="text-align:right;">2000</td>
	 </tr>
	 </table>
</div>

 <div  align="center" >财务主管:&nbsp;&nbsp;记账:&nbsp;&nbsp;复核:&nbsp;&nbsp;制单:&nbsp;&nbsp;出纳:&nbsp;&nbsp;</div>