<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>




<script>

	//辅助修改流水
    var fzxgHistoryService;
    $(function () {
    	fzxgHistoryService = new $.BaseService('#XG_query_form', '#xg_list'
    	            , '#XG_form_dialog', '#XG_edit_form'
    	            , {
    		              listUrl:'list.shtml?entity.hsid=${command.entity.id}'
    	                 ,actionRootUrl: '<c:url value="/business/accounting/fuzhuxglssj/"/>'
    	                 , entityTitle: "账套"
    	                 , datagridToolbar: []
    				     , datagridHasFrozenColumns: false
             	   	    , mydatagridColumns: [[
									{field: 'hsxmbm', title: '核算项目编码', width: 140, rowspan: 2, align: 'center'},
									{field: 'hsxmmc', title: '核算项目名称', width: 200, rowspan: 2, align: 'center'},
									{field: 'sfdjmx', title: '是否底级明细', width: 140, rowspan: 2, align: 'center'},
									{field: 'kjnd', title: '会计年度', width: 170, rowspan: 2, align: 'center'} ,
									{field: 'spzt', title: '审批状态', width: 170, rowspan: 2, align: 'center'}
               ]] 
                , dialogWidth: 800
                , dialogHeight: 600
            });
    });


</script>

<table id="xg_list"></table>
