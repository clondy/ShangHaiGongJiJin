<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:if test="${VoucherList[0].xspzh=='00002'}">
<div  align="center" >
	<font size="6">记账凭证</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
		 	<td width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
		 	<td width ="30%" style="text-align:content;">2017年12月06日</td>
		 	<td width ="30%" style="text-align:right;">附件单据数:${VoucherList[0].fjdjs}&nbsp;&nbsp;&nbsp;&nbsp;记字:${VoucherList[0].xspzh}</td>
		 </tr>
	</table>
	<table rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
	 	<td width="25%"><strong>摘要</strong></td>
	 	<td width="15%"><strong>科目编号</strong></td>
	 	<td width="30%"><strong>科目名称</strong></td>
	 	<td width="15%"><strong>借方发生额</strong></td>
	 	<td width="15%"><strong>贷方发生额</strong></td>
	 </tr>
		<tr height="20px">
		 	<td style="text-align:left;" >提取</td>
	    	<td style="text-align:left;">201(0103)</td>
	    	<td style="text-align:left;">住房公积金 （公积金-支取本金）</td>
	    	<td style="text-align:right;" >1,000.00</td>
	    	<td style="text-align:right;" ></td>
		  </tr>
		   <tr height="20px">
		 	<td style="text-align:left;">提取</td>
	    	<td style="text-align:left;">101(05)</td>
	    	<td style="text-align:left;">住房公积金存款 (提取专户)</td>
	    	<td style="text-align:right;" ></td>
	    	<td style="text-align:right;" >1,000.00</td>
		  </tr>
	 <tr height="20px"> 
	 	<td>合计	</td>
	 	<td colspan=2 style="text-align:left;">(大写):壹仟元整</td>
	 	<td style="text-align:right;">1,000.00</td>
	 	<td style="text-align:right;">1,000.00</td>
	 </tr>
	 </table>
</div>
<div  align="center" >
<table style="width:800px;">
  <tr>
    <th style="text-align:left;">财务主管:</th>
    <th style="text-align:left;">记账:${VoucherList[0].jz}</th>
    <th style="text-align:left;">复核:${VoucherList[0].fh}</th>
    <th style="text-align:left;">制单:${VoucherList[0].zd}</th>
  </tr>
</table>
</div>
</c:if>


<c:if test="${VoucherList[0].xspzh=='00003'}">
<div  align="center" >
	<font size="6">记账凭证</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
		 	<td width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
		 	<td width ="30%" style="text-align:content;">2017年12月06日</td>
		 	<td width ="30%" style="text-align:right;">附件单据数:${VoucherList[0].fjdjs}&nbsp;&nbsp;&nbsp;&nbsp;记字:${VoucherList[0].xspzh}</td>
		 </tr>
	</table>
	<table rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
	 	<td width="25%"><strong>摘要</strong></td>
	 	<td width="15%"><strong>科目编号</strong></td>
	 	<td width="30%"><strong>科目名称</strong></td>
	 	<td width="15%"><strong>借方发生额</strong></td>
	 	<td width="15%"><strong>贷方发生额</strong></td>
	 </tr>
		  <tr height="20px">
		 	<td style="text-align:left;" >提取</td>
	    	<td style="text-align:left;">201(0203)</td>
	    	<td style="text-align:left;">住房公积金 （补充公积金-支取本金）</td>
	    	<td style="text-align:right;" >500.00</td>
	    	<td style="text-align:right;" ></td>
		  </tr>
		   <tr height="20px">
		 	<td style="text-align:left;">提取</td>
	    	<td style="text-align:left;">101(05)</td>
	    	<td style="text-align:left;">住房公积金存款 (提取专户)</td>
	    	<td style="text-align:right;" ></td>
	    	<td style="text-align:right;" >500.00</td>
		  </tr>
	 <tr height="20px"> 
	 	<td>合计	</td>
	 	<td colspan=2 style="text-align:left;">(大写):伍佰元整</td>
	 	<td style="text-align:right;">500.00</td>
	 	<td style="text-align:right;">500.00</td>
	 </tr>
	 </table>
</div>
<div  align="center" >
<table style="width:800px;">
  <tr>
    <th style="text-align:left;">财务主管:</th>
    <th style="text-align:left;">记账:${VoucherList[0].jz}</th>
    <th style="text-align:left;">复核:${VoucherList[0].fh}</th>
    <th style="text-align:left;">制单:${VoucherList[0].zd}</th>
  </tr>
</table>
</div>
</c:if>

<c:if test="${VoucherList[0].xspzh=='00004'}">
<div  align="center" >
	<font size="6">记账凭证</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
		 	<td width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
		 	<td width ="30%" style="text-align:content;">2017年12月06日</td>
		 	<td width ="30%" style="text-align:right;">附件单据数:${VoucherList[0].fjdjs}&nbsp;&nbsp;&nbsp;&nbsp;记字:${VoucherList[0].xspzh}</td>
		 </tr>
	</table>
	<table rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
	 	<td width="25%"><strong>摘要</strong></td>
	 	<td width="15%"><strong>科目编号</strong></td>
	 	<td width="30%"><strong>科目名称</strong></td>
	 	<td width="15%"><strong>借方发生额</strong></td>
	 	<td width="15%"><strong>贷方发生额</strong></td>
	 </tr>
		<tr height="20px">
		 	<td style="text-align:left;" >提取</td>
	    	<td style="text-align:left;">201(0103)</td>
	    	<td style="text-align:left;">住房公积金 （公积金-支取本金）</td>
	    	<td style="text-align:right;" >1,000.00</td>
	    	<td style="text-align:right;" ></td>
		  </tr>
		   <tr height="20px">
		 	<td style="text-align:left;">提取</td>
	    	<td style="text-align:left;">101(05)</td>
	    	<td style="text-align:left;">住房公积金存款 (提取专户)</td>
	    	<td style="text-align:right;" ></td>
	    	<td style="text-align:right;" >1,000.00</td>
		  </tr>
	 <tr height="20px"> 
	 	<td>合计	</td>
	 	<td colspan=2 style="text-align:left;">(大写):壹仟元整</td>
	 	<td style="text-align:right;">1,000.00</td>
	 	<td style="text-align:right;">1,000.00</td>
	 </tr>
	 </table>
</div>
<div  align="center" >
<table style="width:800px;">
  <tr>
    <th style="text-align:left;">财务主管:</th>
    <th style="text-align:left;">记账:${VoucherList[0].jz}</th>
    <th style="text-align:left;">复核:${VoucherList[0].fh}</th>
    <th style="text-align:left;">制单:${VoucherList[0].zd}</th>
  </tr>
</table>
</div>
</c:if>

<c:if test="${VoucherList[0].zhaiyao=='缴存'}">
<div  align="center" >
	<font size="6">记账凭证</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
		 	<td width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
		 	<td width ="30%" style="text-align:content;">2017年12月06日</td>
		 	<td width ="30%" style="text-align:right;">附件单据数:${VoucherList[0].fjdjs}&nbsp;&nbsp;&nbsp;&nbsp;记字:${VoucherList[0].xspzh}</td>
		 </tr>
	</table>
	 <table rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
	 	<td width="25%"><strong>摘要</strong></td>
	 	<td width="15%"><strong>科目编号</strong></td>
	 	<td width="30%"><strong>科目名称</strong></td>
	 	<td width="15%"><strong>借方发生额</strong></td>
	 	<td width="15%"><strong>贷方发生额</strong></td>
	 </tr>
		 <tr height="20px">
		 	<td style="text-align:left;" >缴存</td>
	    	<td style="text-align:left;" >101(01)</td>
	    	<td style="text-align:left;" >住房公积金存款 (公积金户)</td>
	    	<td style="text-align:right;" >2,000.00</td>
	    	<td style="text-align:right;" ></td>
		  </tr>
		  <tr height="20px">
		 	<td style="text-align:left;" >缴存</td>
	    	<td style="text-align:left;" >201(0101)</td>
	    	<td style="text-align:left;" >住房公积金 (公积金-净上划)</td>
	    	<td style="text-align:right;" ></td>
	    	<td style="text-align:right;" >2,000.00</td>
		  </tr>
	 <tr height="20px"> 
	 	<td>合计	</td>
	 	<td colspan=2 style="text-align:left;">(大写):贰仟元整</td>
	 	<td style="text-align:right;">2,000.00</td>
	 	<td style="text-align:right;">2,000.00</td>
	 </tr>
	 </table>
</div>
<div  align="center" >
<table style="width:800px;">
  <tr>
    <th style="text-align:left;">财务主管:</th>
    <th style="text-align:left;">复核:${VoucherList[0].fh}</th>
    <th style="text-align:left;">记账:</th>
    <th style="text-align:left;">制单:${VoucherList[0].zd}</th>
  </tr>
</table>
</div>
</c:if>