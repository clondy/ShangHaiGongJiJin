<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<script>
	var KmyebService;
	$(function() {
		KmyebService = new $.BaseService(
				'#kmyeb_query_form',
				'#kmyeb_list_datagrid',
				'#kmyeb_form_dialog',
				'#kmyeb_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/kmyeb/" />',
					entityTitle : "科目余额",
					optionFormatter : function(value, rec) {}
		            , datagridHasFrozenColumns: false
			        ,datagridToolbar: []
					,mydatagridColumns : [
						                     [ 
						                        {field : 'kmbh',title:"科目编号",rowspan:2,align : 'center',width:100}, 
						                        {field : 'kmmc',title:"科目名称",rowspan:2,align : 'center',width:100},  
						                    	{field : 'jhd',title:"借或贷",rowspan:2,align : 'center',width:40},  
												{field : 'ncs',title:"年初数",rowspan:2,align : 'center',width:100},  
												{field : 'jhd1',title:"借或贷",rowspan:2,align : 'center',width:40},  
												{field : 'qcye',title:"期初余额",rowspan:2,align : 'center',width:100},
												{title:"本期累计",colspan:2,align : 'center',width:100},  
												{title:"本年累计",colspan:2,align : 'center',width:100},
												{field : 'jhd2',title:"借或贷",rowspan:2,align : 'center',width:40},
											    {field : 'ye',title:"余额",rowspan:2,align : 'center',width:100}
											    
											 ],
		                      
								   	[
								
								        {field : 'j',title:"借",align : 'center',width:50},  
								        {field : 'd',title:"贷",align : 'center',width:50},
								        {field : 'j1',title:"借",align : 'center',width:50},  
								        {field : 'd1',title:"贷",align : 'center',width:50}
						         	]],  
					dialogWidth : 800,
					dialogHeight : 600,
				});

		$('#ztsz_query_form_search_button').click(function() {
			$('#kmyeb_query_form').submit();
		});
		
		$('#reset_button').click(function() {
			$('#kmyeb_query_form')[0].reset();
		});
	});
	
	//导出Excel
	$('#export1_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#kmyeb_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/kmyeb/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}
	
</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">
	<span style="algin: center"> <a class="l-btn" id="ztsz_query_form_search_button"><span class="l-btn-left"><span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp; 
	<a class="l-btn" id="reset_button"><span class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;
	<a class="l-btn" id="export1_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
	</span>
	<hr/>
		<span> <form:form name="kmyeb_query_form" id="kmyeb_query_form" method="post" action="" onsubmit="return false;">
			<center>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c" border="0px" cellpadding="0" cellspacing="0">
					<tr>
						<th class="panel-header">显示方式</th>
								<td><form:select path="xsfs" style="width: 184px;height: 27px;" class="form_view_input combo easyui-combobox" data-options="required: true, editable: false">
								<form:option value="">---请选择---</form:option>
								<form:option value="1">一级科目</form:option>
								<form:option value="2">二级科目</form:option>
								<form:option value="3">三级科目</form:option>
							</form:select></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					
                        <th class="panel-header">科目范围</th>
                        	<td>
                        		<form:input path="startKm" style="width: 184px;height: 27px;" data-options="required: true" class="form_view_input combo"></form:input>
							</td>
                    	<td><span>至</span></td>
						<td>
							<form:input path="endKm" style="width: 184px;height: 27px;" data-options="required: true" class="form_view_input combo"></form:input>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<th class="panel-header">统计期间</th>
	                        <td><form:input path="starttime"
											style="width: 184px;height: 27px;"
											data-options="required: true"
											class="Wdate"
											onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
											validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td>
                    		<td><span>至</span></td>
							<td><form:input path="endtime"
											style="width: 184px;height: 27px;"
											data-options="required: true"
											class="Wdate"
											onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
											validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td>
					</tr>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>
</div>

<table id="kmyeb_list_datagrid"></table>
<div id="kmyeb_form_dialog">Dialog Content</div>
