<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
	var QcService;
	$(function() {
		QcService = new $.BaseService('#QC_query_form','#QC_list_datagrid','#QC_form_dialog', '#QC_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/fzhsqcsz/QC" />',
					entityTitle : "辅助核算期初设置",
					optionFormatter : [] 
				     , datagridToolbar: []
					 , mydatagridColumns:[[ 
					                     {field : 'KMBH',title : '科目编码',width : 100,align : 'center'}, 
                                         {field : 'KMMC',title : '科目名称',width : 100,align : 'center'}, 
                                         {field : 'KMYEFX',title : '科目方向',width : 100,align : 'center'},
                                         {field : 'HSXMBM',title : '核算项目编码',width : 100,align : 'center'},
                                         {field : 'HSXMMC',title : '核算项目名称',width : 100,align : 'center'},
                                         {field : 'NCYE',title : '年初余额',width : 100,align : 'center'},
                                         {field : 'BNLJJF',title : '本年累计借方',width : 100,align : 'center'},
                                         {field : 'BNLJDF',title : '本年累计贷方',width : 100,align : 'center'},
                                         {field : 'YUE',title : '余额',width : 100,align : 'center'}
                                         ]]
					,
					dialogWidth : 800,
					dialogHeight : 600,
					dialogButtons : []
				});

		$('#QC_query_form_search_button').click(function() {

			$('#QC_query_form').submit();
		});
		
		$('#reset_button').click(function() {

			$('#QC_query_form')[0].reset();
		});
	});
	
	
	$(function () {
        var _mkid = $('#mkid').combobox({
        	url:'${pageContext.request.contextPath}/business/accounting/fzyeb/hslx.shtml',
            editable: false,
            valueField: 'hslxbm',
            textField: 'hslxmc'
        });
    });
	
	
</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">

	<div>&nbsp;&nbsp;</div>
	<span style="algin: center"> <a class="l-btn"
		id="QC_query_form_search_button"><span class="l-btn-left"><span
				class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp; <a class="l-btn" id="reset_button"><span
			class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;<a class="l-btn" id="add_button"><span
			class="l-btn-left"><span class="l-btn-text">存盘</span></span></a>
		&nbsp;&nbsp;
	</span>
	<div>&nbsp;&nbsp;</div>

</div>
<span> <form:form name="QC_query_form" id="QC_query_form"
			method="post" action="" onsubmit="return false;">
			<center>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c"
					border="0px" cellpadding="0" cellspacing="0">
					<tr>
							<th class="panel-header">会计年度</th>
								 <td style="width: 160px">
	                                 <input name="kjnd" class="easyui-combobox" id="mkid" style="width: 180px;height: 27px;"  data-options="valueField:'hslxbm', textField:'hslxmc', panelHeight:'auto'" >  
	                            </td>
							<th class="panel-header">核算类别</th>
						<td><select name="" style="width: 160px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<option value="">--请选择--</option>
							</select></td>	
							<th class="panel-header">财务科目</th>
						<td><select name="" style="width: 160px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<option value="">--请选择--</option>
							</select></td>
						
						<th class="panel-header">科目余额</th>
						<td><select name="" style="width: 160px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<option value="">--请选择--</option>
							</select></td>
						<th class="panel-header">项目合计余额</th>
						<td><input name="" class="form_view_input combo easyui-validatebox"
								style="width: 160px;height: 27px;" /></td>
					</tr>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>

<table id="QC_list_datagrid"></table>
<div id="QC_form_dialog">Dialog Content</div>
