<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Fzlxxg" items="${FzlxxglssjList}" varStatus="status">
     {
     "id": "${Fzlxxg.id}",
     "hslxbm": "${Fzlxxg.hslxbm}",
     "hslbmc": "${Fzlxxg.hslbmc}",
	 "sjzt": "${Fzlxxg.sjzt}",
     "hsdw": "${Fzlxxg.hsdw.name}",
     "spsj": "${Fzlxxg.spsj}"
   		 }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
] }
 