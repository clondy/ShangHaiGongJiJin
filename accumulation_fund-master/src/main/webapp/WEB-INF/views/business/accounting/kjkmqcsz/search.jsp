<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script>
var kjkmqcszService;
$(function () {
	
	kjkmqcszService = new $.BaseService('#kjkmqcsz_query_form', '#kjkmqcsz_list_datagrid'
            , '#company_form_dialog', '#kjkmqcsz_edit_form'
        , {
        actionRootUrl: '<c:url value="/business/accounting/kjkmqcsz/" />'
        , entityTitle: "会计科目期初设置"
        , optionFormatter: function(value, rec) {
			return $.formatString('<a href="#" title="余额设置" onClick="kjkmqcszService.dialog.dialog(\'{0}\').dialog(\'{1}\',\'{2}\');">余额设置</a>','open','refresh',kjkmqcszService.actionRootUrl+ kjkmqcszService.formUrl+"?id="+rec.id )
					+ '&nbsp;&nbsp;'
 					
        }
        , datagridAddButtonId: 'kjkmqcsz_add_button'
        , datagridBatchDeleteButtonId: 'kjkmqcsz_batch_delete_button'
        , datagridColumns: [
                          
                             {field: 'zt', title: '审批状态', width: 180, rowspan: 2, align: 'center'
                              ,formatter: function(value,row,index){
                            	                             	 if(value == '2'){
                            	                  					return '新增审批中';
                            	                  				 }
                            	                             	 else if(value == '1'){
                            	                  					return '正在使用中';
                            	                  				 }
                            	                             	else if(value == '4'){
                            	                  					return '禁用中';
                            	                  				 }
                            	                             	else{
                            	                             		return '禁用审批中';
                            	                             	}
                            	                 			}},
                            {field: 'jbjg', title: '经办机构', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmbh', title: '科目编号', width: 100, rowspan: 2, align: 'center'},
                            {field: 'kmmc', title: '科目名称', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmjb', title: '科目级别', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmsx', title: '科目属性', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmyefx', title: '余额方向', width: 100, rowspan: 1, align: 'center'},
                            {field: 'ncye', title: '年初余额', width: 100, rowspan: 1, align: 'center'},
                            {field: 'bnljjf', title: '本年累计借方', width: 100, rowspan: 1, align: 'center'},
                            {field: 'bnljdf', title: '本年累计贷方', width: 100, rowspan: 1, align: 'center'},
                            {field: 'tzbj', title: '台账标记', width: 100, rowspan: 1, align: 'center'},
                            {field: 'sfmjkm', title: '末级标记', width: 100, rowspan: 1, align: 'center'},
                            {field: 'rjzlx', title: '日记账类型', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kjnd', title: '会计年度', width: 100, rowspan: 1, align: 'center'}
        ]
            ,datagridToolbar: []
            , dialogWidth: 800
            , dialogHeight: 600
            , dialogButtons: [
                {
                	id: 'kjkmqcsz_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#kjkmqcsz_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                kjkmqcszHistorySaveOrUpdate();
                            }
    					}else{
                            $.messager.progress('close');
    					}
                        function kjkmqcszHistorySaveOrUpdate(){
                            $("#kjkmqcsz_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/accounting/kemushezhi/save_or_update.shtml'
                                , onSubmit : function() {
									var isValid = $('#kjkmqcsz_edit_form').form('validate');
									if (!isValid) {
										$.messager.progress('close');
									}
									return isValid
								},
								success : function(data) {
									$.messager.progress('close');
									console.log(data)
									data = jQuery.parseJSON(data);
									kjkmqcszService.dialog.dialog('close');
									$('#ztsz_query_form').submit();
									 kjkmqcszService.datagrid.datagrid('reload');
									$.messager.alert('通知',"添加成功！");
                                }
                            });
                        }
                	}
                }
                , {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                    }
                }
            ]
        });

    $('#kjkmqcsz_query_form_search_button').click(function () {
        $('#kjkmqcsz_query_form').submit();
    });
    
    $('#kjkmqcsz_chongzhi_form_chongzhi_button').click(function () {
        $('#kjkmqcsz_query_form')[0].reset();
    }); 
   
});

</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">
	<div style="margin-top: 20px">
		<span> <a class="l-btn" id="kjkmqcsz_query_form_search_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="kjkmqcsz_chongzhi_form_chongzhi_button"><span class="l-btn-left"><span
					>重置</span></span></a>
			&nbsp;&nbsp;
		</span> 
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>
	<div style="margin-top: 0px">
		<span> <form:form name="kjkmqcsz_query_form" id="kjkmqcsz_query_form"
				method="post" action="" onsubmit="return false;">
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="px" cellpadding="0"
					cellspacing="0" style="">
					<%-- <tr>
						<th class="panel-header"><nobr>会计年度</nobr></th>
						<td><form:select path="entity.kjnd.id"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList}" itemValue="id" itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>科目属性</nobr></th>
						<td><form:select path="entity.kmsx"
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList1}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>会计科目</nobr></th>
						<td><form:input path="entity.kmbh"
								class="form_view_input combo easyui-validatebox" id="input"
								style="height: 27px;" /></td>
						<th class="panel-header"><nobr>台账标记</nobr></th>
						<td><form:select path="entity.tzbj"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList2}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>日记账类型</nobr></th>
						<td><form:select path="entity.rjzlx"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList3}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
					</tr>
				</table> --%>
			</form:form>

		</span>
	</div>
</div>
<div id="pzjz_ck_window"></div>
<div id="lssj_ck_window"></div>
<div id="spls_ck_window"></div>
<table id="kjkmqcsz_list_datagrid"></table>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog"></div>
<div id="changeSite_dialog"></div>
<div id="change_company_info_dialog"></div>
<div id="company_site_form_dialog"></div>