<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="company" items="${KmszjbsjList}" varStatus="status">
    {"id": "${company.id}",
    "lsbid": "${company.kmszxglssjList[0].id}",
    "ztdm": "${company.kmszxglssjList[0].ztdm}",
    "kmbh": "${company.kmszxglssjList[0].kmbh}",
    "kmmc": "${company.kmszxglssjList[0].kmmc}",
    "kmjb": "${company.kmszxglssjList[0].kmjb}",
    "kmsx": "${company.kmszxglssjList[0].kmsx}",
    "kjkmlb": "${company.kmszxglssjList[0].kjkmlb}",
    "kmyefx": "${company.kmszxglssjList[0].kmyefx}",
    "sfmjkm": "${company.kmszxglssjList[0].sfmjkm}",
    "ncye": "${company.kmszxglssjList[0].ncye}",
    "ye": "${company.kmszxglssjList[0].ye}",
    "bnljjf": "${company.kmszxglssjList[0].bnljjf}",
    "bnljdf": "${company.kmszxglssjList[0].bnljdf}",
    "tzbj": "${company.kmszxglssjList[0].tzbj}",
    "rjzlx": "${company.kmszxglssjList[0].rjzlx}",
    "fzhslx": "${company.kmszxglssjList[0].fzhslx}",
    "zt": "${company.kmszxglssjList[0].zt}",
    "crsj": "${company.kmszxglssjList[0].crsj}",
    "kjnd": "${company.kmszxglssjList[0].kjnd}",
    "jbjg": "${company.kmszxglssjList[0].jbjg}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}