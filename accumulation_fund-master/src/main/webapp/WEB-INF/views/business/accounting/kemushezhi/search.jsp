<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script>
var kmszService;
$(function () {
	
	kmszService = new $.BaseService('#kmsz_query_form', '#kmsz_list_datagrid'
            , '#company_form_dialog', '#kmsz_edit_form'
        , {
        actionRootUrl: '<c:url value="/business/accounting/kemushezhi/" />'
        , entityTitle: "科目设置"
        , optionFormatter: function(value, rec) {
			return $.formatString('<a href="#" title="编辑" onClick="kmszService.dialog.dialog(\'{0}\').dialog(\'{1}\',\'{2}\');">编辑</a>','open','refresh',kmszService.actionRootUrl+ kmszService.formUrl+"?entity.id="+rec.id +'&lsbid='+rec.lsbid )
					+ '&nbsp;&nbsp;'
 					+ '<a href="#"  onClick="jinyong('+rec.lsbid+') ">禁用</a>'
			        + '&nbsp;&nbsp;'
			        + '<a href="#" onClick="shenpi('+rec.lsbid+') ">新增经办</a>'
			        + '&nbsp;&nbsp;'
			        + '<a href="#" onClick="jinyongjingban('+rec.lsbid+') ">禁用经办</a>'
        }
        , datagridAddButtonId: 'kmsz_add_button'
        , datagridBatchDeleteButtonId: 'kmsz_batch_delete_button'
        , datagridColumns: [
                          
                             {field: 'zt', title: '审批状态', width: 180, rowspan: 2, align: 'center'
                              ,formatter: function(value,row,index){
                            	                             	 if(value == '2'){
                            	                  					return '新增审批中';
                            	                  				 }
                            	                             	 else if(value == '1'){
                            	                  					return '正在使用中';
                            	                  				 }
                            	                             	else if(value == '4'){
                            	                  					return '禁用中';
                            	                  				 }
                            	                             	else{
                            	                             		return '禁用审批中';
                            	                             	}
                            	                 			}},
                            {field: 'jbjg', title: '经办机构', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmbh', title: '科目编号', width: 100, rowspan: 2, align: 'center'},
                            {field: 'kmmc', title: '科目名称', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmjb', title: '科目级别', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmsx', title: '科目属性', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kmyefx', title: '余额方向', width: 100, rowspan: 1, align: 'center'},
                            {field: 'ncye', title: '年初余额', width: 100, rowspan: 1, align: 'center'},
                            {field: 'bnljjf', title: '本年累计借方', width: 100, rowspan: 1, align: 'center'},
                            {field: 'bnljdf', title: '本年累计贷方', width: 100, rowspan: 1, align: 'center'},
                            {field: 'tzbj', title: '台账标记', width: 100, rowspan: 1, align: 'center'},
                            {field: 'sfmjkm', title: '末级标记', width: 100, rowspan: 1, align: 'center'},
                            {field: 'rjzlx', title: '日记账类型', width: 100, rowspan: 1, align: 'center'},
                            {field: 'kjnd', title: '会计年度', width: 100, rowspan: 1, align: 'center'}
        ]
            ,datagridToolbar: []
            , dialogWidth: 800
            , dialogHeight: 600
            , dialogButtons: [
                {
                	id: 'kmsz_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#kmsz_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                kmszHistorySaveOrUpdate();
                            }
    					}else{
                            $.messager.progress('close');
    					}
                        function kmszHistorySaveOrUpdate(){
                            $("#kmsz_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/accounting/kemushezhi/save_or_update.shtml'
                                , onSubmit : function() {
									var isValid = $('#kmsz_edit_form').form('validate');
									if (!isValid) {
										$.messager.progress('close');
									}
									return isValid
								},
								success : function(data) {
									$.messager.progress('close');
									console.log(data)
									data = jQuery.parseJSON(data);
									kmszService.dialog.dialog('close');
									$('#ztsz_query_form').submit();
									 kmszService.datagrid.datagrid('reload');
									$.messager.alert('通知',"添加成功！");
                                }
                            });
                        }
                	}
                }
                , {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                    }
                }
            ]
        });

    $('#kmsz_query_form_search_button').click(function () {
        $('#kmsz_query_form').submit();
    });
    
    $('#kmsz_chongzhi_form_chongzhi_button').click(function () {
        $('#kmsz_query_form')[0].reset();
    }); 
    $('#kmsz_lssj_button').click(function () {
      lishishuju();
    }); 
    $('#kmspls_search_button').click(function () {
    	shenpilishi();
      }); 
    
});

//删除科目
function shanchu(){
	var ids=[];
	var row = $('#kmsz_list_datagrid').datagrid('getSelections');
	if(row.length==0){  
		alert("请至少选择一行数据!");  
		return false;  
	}
	else{
		for(var i = 0; i < row.length; i++) {
			ids[i]=row[i].id;
	          
	    }
		delet(ids); 
           }
}
	function delet(ids) {

		var checkedData = {};
			for(var i = 0; i < ids.length; i++) {
				checkedData["ids[" + i + "]"] = ids[i];
			}
			var dataCount = Object.keys(checkedData).length;
			var prefix = ( 1 < dataCount )? "批量": "";
			if( dataCount ) {

				if( confirm("确定要删除吗？？？") ) {

					$.ajax({
						url: '<c:url value="/business/accounting/kemushezhi/delete.shtml" />'
						, type : "post"
						, dataType : 'json'
						, data : checkedData
						, success : function(data, response, status) {
			
							if( jQuery.parseJSON('' + data) ) {
								
								$.messager.alert('通知', prefix + "删除成功。");
								kmszService.datagrid.datagrid('clearChecked');
								kmszService.datagrid.datagrid('reload');
							} else {
								
								$.messager.alert('通知', prefix + "删除失败。");
							}
						}
					});
				} else {

 					kmszService.datagrid.datagrid('clearChecked');
				}
			} 
		}
//增加科目
function add() {

	var row = $('#kmsz_list_datagrid').datagrid('getSelections');
	if(row == null || row.length == 0)
	{
		 $('#pzjz_ck_window').window({
			    title:'会计科目维护',
			    width:800,
			    height:500,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhi/site_record1.shtml',
			    modal:true
			});
	}else{
	var id=row[0].id;
	 if (row.length == 1) {  
		 $('#pzjz_ck_window').window({
			    title:'会计科目维护',
			    width:800,
			    height:500,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhi/site_record.shtml?entity.id='+id,
			    modal:true
			});
     } else {  
         $.messager.alert('提示', '请选择一条记录！', 'warning');  
     }
	}
}
//历史数据
function lishishuju(){
	var row = $('#kmsz_list_datagrid').datagrid('getSelections');
	if(row == null || row.length == 0)
	{
		 $('#lssj_ck_window').window({
			    title:'历史数据查询',
			    width:1214,
			    height:444,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhilssj/search1.shtml',
			    modal:true
			});
	}else{
	var tempid=row[0].id;
	 if (row.length == 1) {  

		 $('#lssj_ck_window').window({
			    title:'历史数据查询',
			    width:1214,
			    height:444,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhilssj/search1.shtml?tempid='+tempid,
			    modal:true,
			});
     } else {  
         $.messager.alert('提示', '请选择一条记录！', 'warning');  
     }
}
}

//审批
function shenpi(lsbid){
		 $('#lssj_ck_window').window({
			    title:'审批',
			    width:400,
			    height:400,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhisplssj/shenpiyijian.shtml?entity.kmszls='+lsbid,
			    modal:true,
			});
		
}
//禁用
function jinyong(lsbid){
$.ajax({
	url:'${pageContext.request.contextPath}/business/accounting/kemushezhilssj/jinyong.shtml?entity.id='+lsbid
	, type : "post"
	, dataType : 'json'
	, success : function(data, response, status) {
		if(data){
			$('#kmsz_list_datagrid').datagrid('reload');
			$.messager.alert('通知', "禁用审批中");
		} else {
			$.messager.alert('通知',"禁用审批失败。");
		}
	}
});
}

//禁用经办
function jinyongjingban(lsbid){
	$.ajax({
		url:'${pageContext.request.contextPath}/business/accounting/kemushezhilssj/jinyongjingban.shtml?entity.id='+lsbid
		, type : "post"
		, dataType : 'json'
		, success : function(data, response, status) {
			if(data){
				$('#kmsz_list_datagrid').datagrid('reload');
				$.messager.alert('通知', "禁用成功");
			} else {
				$.messager.alert('通知',"禁用失败。");
			}
		}
	});
}

//审批历史
function shenpilishi()
{
	var row = $('#kmsz_list_datagrid').datagrid('getSelections');
	if(row == null || row.length == 0)
	{
		 $('#spls_ck_window').window({
			    title:'审批历史数据查询',
			    width:803,
			    height:444,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhisplssj/search1.shtml',
			    modal:true
			});
	}else{
	var lsbid=row[0].lsbid;
	  alert(lsbid);
	 if (row.length == 1) {  
		 $('#lssj_ck_window').window({
			    title:'审批历史数据查询',
			    width:803,
			    height:444,
			 	href:'${pageContext.request.contextPath}/business/accounting/kemushezhisplssj/search1.shtml?kmszls='+lsbid,
			    modal:true,
			});
     } else {  
         $.messager.alert('提示', '请选择一条记录！', 'warning');  
     }
}
	
	
	}
</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">
	<div style="margin-top: 20px">
		<span> <a class="l-btn" id="kmsz_query_form_search_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="kmsz_chongzhi_form_chongzhi_button"><span class="l-btn-left"><span
					>重置</span></span></a>
			&nbsp;&nbsp;
		</span> 
		<span> <a class="l-btn" id="company_query_form_zengjia_button" onclick="add()"><span
				class="l-btn-left"><span
					>增加</span></span></a>
				&nbsp;&nbsp;
		</span>
			<span>
				<a class="l-btn" id="kmsz_delete_button"  onclick="shanchu()"><span class="l-btn-left"><span
					>删除</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
					>合并</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
					>拆分</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="kmspls_search_button"><span class="l-btn-left"><span
					>审批历史</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="kmsz_lssj_button"><span class="l-btn-left"><span
					>历史数据</span></span></a>
				&nbsp;&nbsp;
			</span> 
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>
	<div style="margin-top: 0px">
		<span> <form:form name="kmsz_query_form" id="kmsz_query_form"
				method="post" action="" onsubmit="return false;">
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header"><nobr>会计年度</nobr></th>
						<td><form:select path="entity.kjnd"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>科目属性</nobr></th>
						<td><form:select path="entity.kmsx"
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList1}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>会计科目</nobr></th>
						<td><form:input path="entity.kmbh"
								class="form_view_input combo easyui-validatebox" id="input"
								style="height: 27px;" /></td>
						<th class="panel-header"><nobr>台账标记</nobr></th>
						<td><form:select path="entity.tzbj"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList2}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>日记账类型</nobr></th>
						<td><form:select path="entity.rjzlx"
								style="width: 156px;height: 27px;" class="easyui-combobox"
								data-options="editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList3}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
					</tr>
				</table>
			</form:form>

		</span>
	</div>
</div>
<div id="pzjz_ck_window"></div>
<div id="lssj_ck_window"></div>
<div id="spls_ck_window"></div>
<table id="kmsz_list_datagrid"></table>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog"></div>
<div id="changeSite_dialog"></div>
<div id="change_company_info_dialog"></div>
<div id="company_site_form_dialog"></div>