<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Ztsz" items="${KmyebList}" varStatus="status">
	{
	    "kmbh": "${Ztsz.kmbh.kmbh}",
	    "kmmc": "${Ztsz.kmbh.kmmc}",
	    "jhd": "${Ztsz.ncjd}",
	    "ncs": "${Ztsz.ncs}",
	    "jhd1": "${Ztsz.qcjd}",
	    "qcye": "${Ztsz.qcye}",
	    "jhd2": "${Ztsz.yejd}",
	    "ye": "${Ztsz.ye}",
	    "j": "${Ztsz.bqj}",
	    "d": "${Ztsz.bqd}",
	    "j1": "${Ztsz.bnj}",
	    "d1": "${Ztsz.bnd}"
	}
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]}