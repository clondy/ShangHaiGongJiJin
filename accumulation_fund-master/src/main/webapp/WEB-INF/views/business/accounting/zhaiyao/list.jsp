<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="zhaiyao" items="${ZhaiyaoList}" varStatus="status">
    {"id": "${zhaiyao.id}",
    "hsdw": "${zhaiyao.hsdw.name}",
    "ztbh": "${zhaiyao.ztbh.name}",
    "zylx": "${zhaiyao.zylx.name}",
    "zjm": "${zhaiyao.zjm}",
    "zynr": "${zhaiyao.zynr}",
    "czr": "${zhaiyao.czr}",
    "sjzt": "${zhaiyao.sjzt}",
    "crsj": "${zhaiyao.crsj}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}