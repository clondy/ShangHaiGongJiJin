<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>


    var fuzhuzhangService;
    $(function () {
    	 fuzhuzhangService = new $.BaseService('#fuzhuzhang_query_form', '#fuzhuzhang_list_datagrid'
            , '#fuzhuzhang_form_dialog', '#fuzhuzhang_form_search_button'
            , {
        	actionRootUrl: '<c:url value="/business/accounting/fzhsxmqcjesz/" />'
                 , entityTitle: "辅助明细账" ,
                datagridHasFrozenColumns:false
                , mydatagridColumns: [[
                	{field: 'RQ', title: '日期', width: 220, rowspan: 2, align: 'center'},
				    {field: 'HSXMBM', title: '核算项目编码', width: 220, rowspan: 2, align: 'center'},
 					{field: 'HSXMMC', title: '核算项目名称', width: 220, rowspan: 2, align: 'center'},
 					{field: 'PZBH', title: '凭证编号', width: 220, rowspan: 2, align: 'center'},
 					{field: 'ZY', title: '摘要', width: 190, rowspan: 2, align: 'center'},
 					{field: 'JFFSE', title: '借方发生额', width: 220, rowspan: 2, align: 'center'},
 					{field: 'DFFSE', title: '贷方发生额', width: 200, rowspan: 2, align: 'center'} ,
					{field: 'FX', title: '方向', width: 200, rowspan: 2, align: 'center'},
					{field: 'YE', title: '余额', width: 150, rowspan: 2, align: 'center'}
                ] ]
                , dialogWidth: 800
                , dialogHeight: 500
                ,datagridToolbar:[]
        
     });
 });	
    $('#zhizuo_search_button').click(function () {

        $('#fuzhuzhang_query_form').submit();
               });
</script>

<h5>开发中...</h5>


