<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="div_center" style="margin-top: 0px">
	<form:form name="ztsz_edit_form" id="ztsz_edit_form"
		method="post" style="width:800px" action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="ztsz_entity_id" />
		<form:hidden path="entity.spzt" />
		<form:hidden path="entity.ztid" />
		<form:hidden path="entity.sjzt" />
		<form:hidden path="entity.czy" />
		
		
		<center>
			<table bordercolordark="#45b97c" bordercolorlight="#45b97c"
				border="0px" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">核算机构</th>
					<td><form:select path="entity.hsdw.id"
							style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:options items="${hsdwList}" itemValue="id" itemLabel="name" />
						</form:select></td>
											<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					    <th class="panel-header">组织机构</th>
					<td><form:select path="entity.zzjg.id"
							style="width: 184px;height: 27px;"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false">
							<form:option value="">--请选择--</form:option>
							<form:options items="${zzjgList}" itemValue="id"
								itemLabel="name" />
						</form:select></td>

				</tr>
							<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">账套代码</th>
					<td><form:input path="entity.ztdm" id="ZTDM"
							data-options="required: true "
							style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"
							></form:input></td>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<th class="panel-header">资金类型</th>
					<td><form:select path="entity.zjlx.id"
							style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:options items="${zjlxList}" itemValue="id"
								itemLabel="name" />
						</form:select></td>

				</tr>
						<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">账套名称</th>
					<td><form:input path="entity.ztmc" id="ZTMC"
							data-options="required: true" style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"
							validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
																		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<th class="panel-header">账套描述</th>
					<td><form:input path="entity.ztms" id="ZTDM"
							data-options="required: true" style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"
							validType="complexValid['^.{4,255}$','请确认单位名称的长度在4~255个字之间！']"></form:input></td>
				</tr>
						<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">建账日期</th>
					<td><form:input path="entity.crsj"
							data-options="required: true" style="width:184px;height: 27px;"
							id="crsj" class="Wdate"
							onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input></td>
				</tr>
			</table>
		</center>
	</form:form>
	
	
</div>

