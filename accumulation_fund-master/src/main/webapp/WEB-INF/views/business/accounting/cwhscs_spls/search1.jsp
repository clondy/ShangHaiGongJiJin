<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
// ...
$rs = mysql_query("select count(*) from item");
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];
 
$rs = mysql_query("select * from item limit $offset,$rows");
 
$items = array();
while($row = mysql_fetch_object($rs)){
	array_push($items, $row);
}
$result["rows"] = $items;
 
echo json_encode($result);
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>	
		<table class="easyui-datagrid" id="tt" title="审批历史列表" style="width:500;height:408px"
			data-options="singleSelect:true,collapsible:true,
			url:'<%=request.getContextPath()%>/business/accounting/cwhscs_spls/site_list.shtml?entity.cwhscsxgls=${command.entity.cwhscsxgls}',
			method:'get'"
			rownumbers="true" pagination="true">
	<thead>
	<tr>
			<th field="sjzt"  width=80, rowspan= 1, align='center'>数据状态</th>
			<th field="spzt"  width=80, rowspan= 1, align='center'>审批状态</th>
			<th field="spyj"  width=80, rowspan= 1, align='center'>审批意见</th>
			<th field="czy"  width=80, rowspan= 1, align='center'>操作员</th>
			<th field="czsj"  width=80, rowspan= 1, align='center'>操作时间</th>
			<th field="spr"  width=80, rowspan= 1, align='center'>审批人</th>
			<th field="spsj"  width=80, rowspan= 1, align='center'>审批时间</th>
		</tr>
	</thead>
</table>
</body>
</html>