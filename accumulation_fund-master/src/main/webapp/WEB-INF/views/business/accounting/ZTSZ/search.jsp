<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
	var ZtszService;
	$(function() {
		ZtszService = new $.BaseService('#ztsz_query_form','#ztsz_list_datagrid','#ztsz_form_dialog', '#ztsz_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/ZTSZ/" />',
					entityTitle : "账套",
					optionFormatter : function(value, rec) {
					     	//待审批 = 1
						if(rec.spzt == 1 && rec.sjzt ==2){
							return '<a href="#" onClick="Sp('+ rec.idx+ ')">'+'新增经办'+'</a>'
							+ '&nbsp;&nbsp;'
							+'<a href="#" onClick="ZtszService.dialog.dialog(\'open\').dialog(\'refresh\', \''+ '<c:url value="/business/accounting/ztxgHistory/" />'+ 'formxg.shtml'+ '?entity.id='+ rec.idx+ '\');" >'+'编辑'+'</a>'
							+ '&nbsp;&nbsp;'
							+'<a href="#" onClick="ZtszService.datagrid.datagrid(\'checkRow\', ZtszService.datagrid.datagrid(\'getRowIndex\', '+ rec.id+ '));$(\'#'+ ZtszService.datagridBatchDeleteButtonId+ '\').click();">'+'删除'+'</a>'
					        //数据状态中   停用审批中 =3
						}else  if(rec.spzt == 1 && rec.sjzt ==3){
							return '<a href="#" onClick="">'+'禁用经办'+'</a>'
						}else if(rec.spzt == 2 && rec.sjzt ==1){
							//初审通过 =2
							return '<a href="#" onClick="ZtszService.dialog.dialog(\'open\').dialog(\'refresh\', \''+ '<c:url value="/business/accounting/ztxgHistory/" />'+ 'formxg.shtml'+ '?entity.id='+ rec.idx+ '\');" >'+'编辑'+'</a>'
							+ '&nbsp;&nbsp;'
							+'<a href="#" onClick="">'+'禁用'+'</a>'
					     //初审不通过 = 3 （审批状态）
						}else if(rec.spzt == 3){
							return '<a href="#" onClick="ZtszService.dialog.dialog(\'open\').dialog(\'refresh\', \''+ '<c:url value="/business/accounting/ztxgHistory/" />'+ 'formxg.shtml'+ '?entity.id='+ rec.idx+ '\');" >'+'重审禁用'+'</a>'
						}
						
					}
					,
					datagridBatchDeleteButtonId : 'ztsz_batch_delete_button'
				   , datagridToolbar: [{
						id: 'ztsz_batch_delete_button'
						, text: '批量删除'
						, disabled: false
						, iconCls: 'icon-remove'
						, handler: function() {
			
							var checkedData = {};
							var items = ZtszService.datagrid.datagrid('getChecked');
							for(var i = 0; i < items.length; i++) {
								
								checkedData["ids[" + i + "]"] = items[i].id;
							}
							var dataCount = Object.keys(checkedData).length;
							var prefix = ( 1 < dataCount )? "批量": "";
							if( dataCount ) {
								if( confirm("确定要删除吗？？？") ) {
			
									$.ajax({
										url: '${pageContext.request.contextPath}/business/accounting/ZTSZ/deletexg.shtml'
										, type : "post"
										, dataType : 'json'
										, data : checkedData
										, success : function(data, response, status) {
							
											if( jQuery.parseJSON('' + data) ) {
												
												$.messager.alert('通知',  "删除成功。");
												ZtszService.datagrid.datagrid('clearChecked');
												ZtszService.datagrid.datagrid('reload');
											} else {
												
												$.messager.alert('通知',  "删除失败。");
											}
										}
									});
								} else {
			
									ZtszService.datagrid.datagrid('clearChecked');
								}
							} else {
								
								$.messager.alert('通知', "请选中数据后再执行删除。");
							}
							
						}
					}]
					,datagridColumns : [ 
					                     {field : 'ztmc',title : '账套名称',width : 100,rowspan : 2,align : 'center'}, 
                                         {field : 'ztdm',title : '账套代码',width : 100,rowspan : 2,align : 'center'}, 
                                         {field : 'ztms',title : '账套描述',width : 100,rowspan : 2,align : 'center'},
                                         {field : 'hsdw',title : '核算单位',width : 100,rowspan : 2,align : 'center'},
                                         {field : 'sjzt',title : '数据状态',width : 100,rowspan : 2,align : 'center',
    										 formatter : function(value, rec) {
                                        		 if(value == 2){
                                        		  return '新增审批中';
	                                        	 }else if(value == 1){
	                                        		 return '正在使用';
	                                        	 }else if(value == 3){
	                                        		 return '停用审批中';
	                                        	 }else if(value == 4){
	                                        		 return '禁用中';
	                                        	 }}},
	                                        	 
                                         {field : 'spzt',title : '审批状态',width : 100,rowspan : 2,align : 'center',
                                        	 formatter : function(value, rec) {
                                        		 if(value == 1){
                                        			 return '待审批';
	                                        	 }else if(value == 2){
	                                        		 return '初审通过';
	                                        	 }else if(value == 3){
	                                        		 return '初审不通过';
	                                        	 }
                                         }}]
					,
					dialogWidth : 800,
					dialogHeight : 600,
					dialogButtons : [
							{
								id : 'company_confirm',
								text : '保存'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#ztsz_edit_form').form('validate')) {

										$.messager
												.confirm(
														'确认',
														'要执行“确认”操作吗？？？',
														function(r) {
															if (!r) {
																$.messager.progress('close');
																return false;
															} else {
																companyHistorySaveOrUpdate();

															}
														});
									} else {
										$.messager.progress('close');
									}
									function companyHistorySaveOrUpdate() {
										$("#ztsz_edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/ztxgHistory/save_or_update.shtml',
															onSubmit : function() {
																var isValid = $('#ztsz_edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$.messager.progress('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																ZtszService.dialog.dialog('close');
																$('#ztsz_query_form').submit();
																$.messager.alert('通知',"成功！");
															}
														});
									}
								}
							}

							,
							{
								id : 'chongzhi',
								text : '重置'
								//, iconCls: 'icon-save'
								,
								handler : function() {

									if (confirm("要执行“重置”操作吗？？？")) {

										$("#fuhe").linkbutton('disable');
										ZtszService.dialog.dialog('refresh',
												ZtszService.actionRootUrl
														+ ZtszService.formUrl);
									}
								}
							},
							{
								text : '取消',
								iconCls : 'icon-cancel',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}
							} ]
				});

		$('#ztsz_query_form_search_button').click(function() {

			$('#ztsz_query_form').submit();
		});
		
		$('#reset_button').click(function() {

			$('#ztsz_query_form')[0].reset();
		});
	});
	$('#add_button').click(
			function() {
				ZtszService.dialog.dialog('open').dialog('refresh','<c:url value="/business/accounting/ztxgHistory/"/>'+'formxg.shtml');
			});
	function SpHistory() {

		var items = ZtszService.datagrid.datagrid('getChecked');
		if(items != 0){
			
		if(items.length <= 1){
		$('#ztspHistory_dialog')
				.dialog(
						{
							title : '审批历史记录',
							href : "${pageContext.request.contextPath}/business/accounting/ztspHistory/search.shtml?entity.id="+(items.length > 0 ? items[0].id : 0),
							width : 900,
							height : 600,
							closed : false,
							cache : false,
							resizable : true,
							collapsible : true,
							maximizable : true,
							modal : true,
							buttons : [ {
								text : '关闭',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}

							} ]
						});
        }else{
        	$.messager.alert('通知',"只能选择一条数据！");
        }
}

else{
	$.messager.alert('通知',"请先选择一条数据！");
       }
	}
	
	function XgHistory() {

		var items = ZtszService.datagrid.datagrid('getChecked');
if(items != 0){
	
if(items.length <= 1){
		$('#ztxgHistory_dialog')
				.dialog(
						{
							title : '修改历史记录',
							href : "${pageContext.request.contextPath}/business/accounting/ztxgHistory/search.shtml?entity.ztid="+(items.length > 0 ? items[0].id : 0),
							width : 900,
							height : 600,
							closed : false,
							cache : false,
							resizable : true,
							collapsible : true,
							maximizable : true,
							modal : true,
							buttons : [ {
								text : '关闭',
								handler : function() {
									$(
											$(this).context.parentElement.parentElement)
											.dialog('close');
								}

							} ]
						});
        }else{
        	$.messager.alert('通知',"只能选择一条数据！");
        }
}

else{
	$.messager.alert('通知',"请先选择一条数据！");
       }
	}
	$('#sp_dialog').dialog(
			{
				title : '请输入审批意见',
				//href : "${pageContext.request.contextPath}/business/accounting/ztspHistory/form2.shtml?entity.ztid="+id,
				width : 400,
				height : 350,
				closed : true,
				cache : false,
				resizable : true,
				collapsible : true,
				maximizable : true,
				modal : true,
				buttons : [
							{
								id : 'company_confirm',
								text : '通过'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#edit_form').form('validate')) {

										SpHistorySaveOrUpdate();

									} 
									function SpHistorySaveOrUpdate() {
										$("#edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/ztspHistory/save_or_update.shtml',
															onSubmit : function() {
																var isValid = $('#edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$('#sp_dialog').dialog('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																$.messager.alert('通知',"已通过！");
																$.messager.progress('close');
																$('#ztsz_list_datagrid').datagrid("reload");
															}
														});
									}
								}
							}

							,
							{
								id : 'chongzhi',
								text : '不通过'
								//, iconCls: 'icon-save'
								,
								handler : function() {
									$.messager.progress();
									if ($('#edit_form').form('validate')) {

										SpHistorySaveOrUpdate();

									} 
									function SpHistorySaveOrUpdate() {
										$("#edit_form").form('submit',
														{
															url : '${pageContext.request.contextPath}/business/accounting/ztspHistory/saveOrupdate2.shtml',
															onSubmit : function() {
																var isValid = $('#edit_form').form('validate');
																if (!isValid) {
																	$.messager.progress('close');
																}
																return isValid
															},
															success : function(data) {
																$('#sp_dialog').dialog('close');
																console.log(data)
																data = jQuery.parseJSON(data);
																$.messager.alert('通知',"不通过！");
																$.messager.progress('close');
																$('#ztsz_list_datagrid').datagrid("reload");
															}
														});
									}
								}
							},{
					text : '取消',
					handler : function() {
						$(
								$(this).context.parentElement.parentElement).dialog('close');
					}

				} ]
			});
	
	function Sp(id) {
		$('#sp_dialog').dialog('open').dialog('refresh',"${pageContext.request.contextPath}/business/accounting/ztspHistory/form2.shtml?entity.ztls="+id);
	}
	
	

	
	
</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">

	<div>&nbsp;&nbsp;</div>
	<span style="algin: center"> <a class="l-btn"
		id="ztsz_query_form_search_button"><span class="l-btn-left"><span
				class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp; <a class="l-btn" id="reset_button"><span
			class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;<a class="l-btn" id="add_button"><span
			class="l-btn-left"><span class="l-btn-text">新增</span></span></a>
		&nbsp;&nbsp;
	</span> <a class="l-btn" id="SpHistory_button"><span class="l-btn-left"
		onclick="SpHistory()"><span class="l-btn-text">审批历史</span></span></a>
	&nbsp;&nbsp; <a class="l-btn" id="XgHistory_button"><span
		class="l-btn-left" onclick="XgHistory()"><span
			class="l-btn-text">历史数据</span></span></a>
	<div>&nbsp;&nbsp;</div>
	<span> <form:form name="ztsz_query_form" id="ztsz_query_form"
			method="post" action="" onsubmit="return false;">
			<center>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c"
					border="0px" cellpadding="0" cellspacing="0">
					<tr>
						<th class="panel-header">账套名称</th>
						<td><form:input path="entity.ztmc"
								class="form_view_input combo easyui-validatebox"
								style="height: 27px;" /></td>
						<th class="panel-header">账套代码</th>
						<td><form:input path="entity.ztdm"
								class="form_view_input combo easyui-validatebox"
								style="height: 27px;" /></td>
						<th class="panel-header">核算单位名称</th>
						<td><form:select path="entity.hsdw.id"
								style="width: 184px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${hsdwList}" itemValue="id"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header">账套描述</th>
						<td><form:input path="entity.ztms"
								class="form_view_input combo easyui-validatebox"
								style="height: 27px;" /></td>
					</tr>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>
</div>


<table id="ztsz_list_datagrid"></table>
<div id="ztsz_form_dialog">Dialog Content</div>
<div id="ztspHistory_dialog"></div>
<div id="ztxgHistory_dialog"></div>
<div id="sp_dialog"></div>