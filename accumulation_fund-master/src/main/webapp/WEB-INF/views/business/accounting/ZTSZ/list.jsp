<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Ztsz" items="${ZtszList}" varStatus="status">
    {"id": "${Ztsz.id}",
     "idx": "${Ztsz.ztxglssjList[0].id}",
    "ztmc": "${Ztsz.ztxglssjList[0].ztmc}",
    "ztdm": "${Ztsz.ztxglssjList[0].ztdm}",
    "ztms": "${Ztsz.ztxglssjList[0].ztms}",
    "hsdw": "${Ztsz.ztxglssjList[0].hsdw.name}",
    "sjzt": "${Ztsz.ztxglssjList[0].sjzt}",
    "spzt": "${Ztsz.ztxglssjList[0].spzt}"}
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
] }
