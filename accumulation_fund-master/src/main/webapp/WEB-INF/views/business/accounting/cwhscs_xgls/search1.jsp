<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
// ...
$rs = mysql_query("select count(*) from item");
$row = mysql_fetch_row($rs);
$result["total"] = $row[0];
 
$rs = mysql_query("select * from item limit $offset,$rows");
 
$items = array();
while($row = mysql_fetch_object($rs)){
	array_push($items, $row);
}
$result["rows"] = $items;
 
echo json_encode($result);
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
${command.entity.cwhscsid}
<body>	
		<table class="easyui-datagrid" id="tt" title="历史数据列表" style="width:1200px;height:408px"
			data-options="singleSelect:true,collapsible:true,
			url:'<%=request.getContextPath()%>/business/accounting/cwhscs_xgls/site_list.shtml?entity.cwhscsid=${command.entity.cwhscsid}',
			method:'get'"
			rownumbers="true" pagination="true">
	<thead>
	<tr>
			<th field="zt" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">状态<th>
			<th field="HSDW" width=80, rowspan= 1, align='center'>核算单位</th>
			<th field="ZTDM" width=80, rowspan= 1, align='center'>账套名称</th>
			<th field="SFYXDXTZ" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">是否允许单向调账凭证</th>
			<th field="SFYXPZWK" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">是否允许凭证摘要为空<th>
			<th field="PZBHSCFS" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">凭证编号生成方式</th>
			<th field="SFYXKPZBH" width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">是否允许插入凭证编号</th>
			<th field="PZBHCD"  width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">凭证编号长度</th>
			<th field="PZBHSCGZ"  width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">凭证编号生成规则<th>
			<th field="ZDPZZDR"  width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">自动凭证制单人</th>
			<th field="DFYXPZFJZ"  width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">是否允许凭证反记账</th>
			<th field="SFYXFYJ"  width=80, rowspan= 1, align='center' data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '否';
			}
			}">是否允许反月结</th>
			<th field="SJZT" width=80, rowspan= 1, align='center'
			data-options="
			formatter: function(value,row,index){
			if(value=='1'){
			return '是';
			}
			else if(value=='0'){
			return '待审批';
			}
			}">数据状态<th>
			<th field="CRSJ"  width=80, rowspan= 1, align='center'>插入时间</th>
			<th field="CWND"  width=80, rowspan= 1, align='center'>财务年度</th>
			<th field="CZY"  width=80, rowspan= 1, align='center'>操作员</th>
		</tr>
	</thead>
</table>
</body>
</html>