<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>




<script>

	//修改历史
    var XgHistoryService;
    $(function () {
    	XgHistoryService = new $.BaseService(
    			'#SP_query_form', 
    			'#sp_list', 
    			'#SP_form_dialog', 
    			'#SP_edit_form', 
    			{
    		              listUrl:'splx.shtml?entity.hsspid=${command.entity.id}',
    	                  actionRootUrl: '<c:url value="/business/accounting/fuzhuleixingxgls/" />',
    	                  entityTitle: "审批流水",
    	                  datagridToolbar: [],
    				      datagridHasFrozenColumns: false,
             	   	      mydatagridColumns: [[
                   
             	  	{field: 'spzt', title: '审批状态', width: 100, align: 'center'},
                    {field: 'spyj', title: '审批意见', width: 100, align: 'center'},
                    {field: 'czsj', title: '操作时间', width: 100, align: 'center'},
                    {field: 'spr', title: '审批人', width: 100, align: 'center'},
                    {field: 'czy', title: '操作员', width: 100, align: 'center'}
     
               ]] 
                , dialogWidth: 800
                , dialogHeight: 600
            });
    });


</script>

<table id="sp_list"></table>
