<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>


    var fuzhuduolanzhangService;
    $(function () {
    	fuzhuduolanzhangService = new $.BaseService('#fuzhuduolanzhang_query_form', '#fuzhuduolanzhang_list_datagrid'
            , '#fuzhuduolanzhang_form_dialog', '##fuzhuduolanzhang_form_search_button'
            , {
        	actionRootUrl: '<c:url value="/business/accounting/fuzhuduolanzhang/" />'
                 , entityTitle: "辅助多栏明细账" ,
                datagridHasFrozenColumns:false
                , mydatagridColumns: [[
                	{field: 'RQ', title: '日期', width: 200, rowspan: 2, align: 'center'},
				    {field: 'PZBM', title: '凭证编码', width: 200, rowspan: 2, align: 'center'},
 					{field: 'ZY', title: '摘要', width: 220, rowspan: 2, align: 'center'},
 					{field: 'JFFSE', title: '借方发生额', width: 200, rowspan: 2, align: 'center'},
 					{field: 'DFFSE', title: '贷方发生额', width: 200, rowspan: 2, align: 'center'},
 					{field: 'FX', title: '方向', width: 200, rowspan: 2, align: 'center'},
 					{field: 'YE', title: '余额', width: 200, rowspan: 2, align: 'center'} ,
					{field: 'FZLB1', title: '辅助类别1', width: 200, rowspan: 2, align: 'center'},
					{field: 'FZLB2', title: '辅助类别2', width: 200, rowspan: 2, align: 'center'},
					{field: 'FZLB3', title: '辅助类别3', width: 200, rowspan: 2, align: 'center'},
 					{field: 'FZLB4', title: '辅助类别4', width: 200, rowspan: 2, align: 'center'} ,
					{field: 'FZLBS', title: '辅助类别...', width: 200, rowspan: 2, align: 'center'},
					{field: 'FZLBN', title: '辅助类别n', width: 200, rowspan: 2, align: 'center'}
                ] ]
                , dialogWidth: 700
                , dialogHeight: 500
                ,datagridToolbar:[]
        
     });
 });	
    $('#chaxun_search_button').click(function () {

        $('#fuzhuduolanzhang_query_form').submit();
               });
</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span>
			<form:form name="fuzhuduolanzhang_query_form" id="fuzhuduolanzhang_query_form"
                       method="post" action="" onsubmit="return false;">
		  <a class="l-btn" id="chaxun_search_button"><span class="l-btn-left">
          <span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
          &nbsp;&nbsp;
		  <a class="l-btn" id="reset_button"><span class="l-btn-left">
          <span class="l-btn-text ">重置</span></span></a>
          <hr>
		<table>
	 <tr height="15px" >
		               <th class="panel-header">科目名称</th>
				       <td>
        				   <form:select path="entity.pzbhscgz"
									 style="width: 156px;height: 30px;"
									 class="easyui-combobox">
							  <form:option value="1" >住房公积金</form:option>
				              <form:option value="2" >住房公积金存款</form:option>
							  <form:options items="${pzbhscgzList}" itemValue="name" itemLabel="name"/>
						   </form:select>
                       </td>
                       <td>&nbsp;&nbsp;&nbsp;</td>	
                       <th class="panel-header">记账状态</th>
				       <td>
        				   <form:select path="entity.pzbhcd"
									 style="width: 156px;height: 30px;"
									 class="easyui-combobox">
							  <form:option value="1" >已记账</form:option>
							  <form:option value="2" >未记账</form:option>
							  <form:options items="${pzbhcdList}" itemValue="name" itemLabel="name"/>
						   </form:select>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <th class="panel-header">开始日期</th>
                        <td>
                        <form:input path="entity.pzbhcd"
										style="width: 153px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value="${startDate}"
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
						<td>&nbsp;&nbsp;&nbsp;</td>
                    	<th class="panel-header">截止日期</th>
						<td>
						<form:input path="entity.pzbhcd"
										style="width: 156px;height: 29px;"
										data-options="required: true"
										class="Wdate"
										value="${endDate}"
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
						</td>
		</tr>
		<tr height="5px"><td></td></tr>
   </table>
   <table>
		<tr height="15px" >	
		            <th class="panel-header">摘要</th>				
					<td><form:input path="entity.pzbhcd"
							class="form_view_input combo easyui-validatebox" style="width: 153px;height: 27px;"/>
				    </td>
				    <td>&nbsp;&nbsp;&nbsp;</td>
					<th class="panel-header">金额范围</th>
                    <td><form:input path="entity.pzbhcd"
							class="form_view_input combo easyui-validatebox" style="width: 156px;height: 29px;"/>
					</td>
                    <td><span>至</span></td>
					<td><form:input path="entity.pzbhcd"
					 		class="form_view_input combo easyui-validatebox" style="width: 156px;height: 29px;"/>
					</td>	
        </tr>     
	</table>
	<hr>
	<div style="text-align:center;font-size:21px;letter-spacing: 20px;">辅助多栏账</div>
	<table style="text-align:left">
	        <tr>
	            <th><b>核算单位 :</b></th>
	            <td>上海市公积金管理中心</td> 
	            <td style="width: 120px;"></td>
	            
	            <th><b>财务账套 :</b></th>
	            <td>住房公积金财务核算账套</td>
	            <td style="width: 120px;"></td>
	        </tr>
	        <tr>
	           <th><b>科目编号  :</b></th>
	           <td>201</td>
	            <td style="width: 120px;"></td>
	            
	            <th><b>科目名称 :</b></th>
	            <td>住房公积金</td>
	            <td style="width: 120px;"></td>
	            
	             <th><b>辅助类别 :</b></th>
	             <td>管理部</td>
	            <td style="width: 120px;"></td>
	        </tr>
	</table>
       </form:form>
		</span>
</div>

<table id="fuzhuduolanzhang_list_datagrid"></table>
<div id="fuzhuduolanzhang_form_dialog">Dialog Content.</div>
<div id="fuzhuduolanzhang_confirm_form_dialog">

</div>

