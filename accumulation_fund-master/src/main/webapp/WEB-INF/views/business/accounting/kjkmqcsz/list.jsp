<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="kjkmqcsz" items="${KmszjbsjList}" varStatus="status">
    {"id": "${kjkmqcsz.id}",
    "lsbid": "${kjkmqcsz.kmszxglssjList[0].id}",
    "ztdm": "${kjkmqcsz.kmszxglssjList[0].ztdm.ztmc}",
    "kmbh": "${kjkmqcsz.kmszxglssjList[0].kmbh}",
    "kmmc": "${kjkmqcsz.kmszxglssjList[0].kmmc}",
    "kmjb": "${kjkmqcsz.kmszxglssjList[0].kmjb}",
    "kmsx": "${kjkmqcsz.kmszxglssjList[0].kmsx}",
    "kjkmlb": "${kjkmqcsz.kmszxglssjList[0].kjkmlb}",
    "kmyefx": "${kjkmqcsz.kmszxglssjList[0].kmyefx}",
    "sfmjkm": "${kjkmqcsz.kmszxglssjList[0].sfmjkm}",
    "ncye": "${kjkmqcsz.kmszxglssjList[0].ncye}",
    "ye": "${kjkmqcsz.kmszxglssjList[0].ye}",
    "bnljjf": "${kjkmqcsz.kmszxglssjList[0].bnljjf}",
    "bnljdf": "${kjkmqcsz.kmszxglssjList[0].bnljdf}",
    "tzbj": "${kjkmqcsz.kmszxglssjList[0].tzbj}",
    "rjzlx": "${kjkmqcsz.kmszxglssjList[0].rjzlx}",
    "fzhslx": "${kjkmqcsz.kmszxglssjList[0].fzhslx}",
    "zt": "${kjkmqcsz.kmszxglssjList[0].zt}",
    "crsj": "${kjkmqcsz.kmszxglssjList[0].crsj}",
    "kjnd": "${kjkmqcsz.kjnd.name}",
    "jbjg": "${kjkmqcsz.kmszxglssjList[0].jbjg}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}