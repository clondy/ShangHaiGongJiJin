<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="div_center">
	<form:form name="fzz_edit_form" id="fzz_edit_form" method="post"
		action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="fzz_entity_id" />
		<table class="form_view_border">
			<tr>
				<th class="panel-header">核算单位</th>
				<td><form:select path="entity.hsdw.id"
						class="form_view_input combo easyui-combobox"
						data-options="required: true, editable: false">
						<form:option value="">--请选择--</form:option>
						<form:options items="${hsdwList}" itemValue="id" itemLabel="name" />
					</form:select></td>
			</tr>

			<tr>
				<th class="panel-header">账套编号</th>
				<td><form:select path="entity.ztbh.id"
						class="form_view_input combo easyui-combobox"
						data-options="required: true, editable: false">
						<form:option value="">--请选择--</form:option>
						<form:options items="${ztbhList}" itemValue="id" itemLabel="name" />
					</form:select></td>
			</tr>

			<tr>
				<th class="panel-header">摘要类型</th>
				<td><form:select path="entity.zylx.id"
						class="form_view_input combo easyui-combobox"
						data-options="required: true, editable: false">
						<form:option value="">--请选择--</form:option>
						<form:options items="${zylxList}" itemValue="id" itemLabel="name" />
					</form:select></td>
			</tr>
			<tr>
				<th class="panel-header">助记码</th>
				<td><form:input path="entity.zjm" id="zjm"
						data-options="required: true"
						class="form_view_input combo easyui-validatebox"></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">摘要内容</th>
				<td><form:input path="entity.zynr" id="zynr"
						data-options="required: true"
						class="form_view_input combo easyui-validatebox"></form:input></td>
			</tr>
		</table>
	</form:form>
</div>
