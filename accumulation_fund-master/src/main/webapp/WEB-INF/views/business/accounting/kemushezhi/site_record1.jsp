<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script>
   //一级科目增加页面
	function companyHistorySaveOrUpdate() {
		$("#kmsz_edit_form") .form('submit',
						{
							url : '${pageContext.request.contextPath}/business/accounting/kemushezhi/save1.shtml',
							onSubmit : function() {
								var isValid = $(this).form( 'validate');
								if (!isValid) {
									$.messager.progress('close');
								}
								return isValid
							},
							success : function(data) {
								if (data) {
									$('#kmsz_list_datagrid') .datagrid('reload');
									$.messager.alert('通知', "添加成功！");
									$('#pzjz_ck_window').window('close');
								} else {
									$.messager.alert('通知', "添加失败！");
									$('#pzjz_ck_window').window( 'close');
								}
							}
						});
	};
	$('#kmsz_quxiao_button').click(function() {
		$('#pzjz_ck_window').window('close');
	});

</script>
<div class="div_center" id="id" style="height:400px">
	<form:form name="kmsz_edit_form" id="kmsz_edit_form" method="post" action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="kmsz_entity_id" />
		<table class="form_view_border" border="1px" cellpadding="0" cellspacing="0" id="table_id">
		<tr>
		<th class="panel-header">账套名称</th>
				<td>
				    <form:select path="entity.ztdm.ztdm" 
				    			 class="form_view_input combo easyui-combobox" 
				    			 data-options="required: true, editable: false" 
				    			 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList4}" itemValue="ztdm" itemLabel="ztmc" />
					</form:select>
				</td>
				<th class="panel-header">会计年度</th>
				<td>
				    <form:select path="entity.kjnd" 
				    			 class="form_view_input combo easyui-combobox" 
				    			 data-options="required: true, editable: false" 
				    			 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList6}" itemValue="name" itemLabel="name" />
					</form:select>
				</td>
				</tr>
			<tr>
				<th class="panel-header">余额</th>
				<td>
					<form:input path="entity.ye" 
								class="form_view_input combo easyui-validatebox" 
								data-options="required: true"/>
				</td>
				<th class="panel-header">余额方向</th>
				<td><form:select path="entity.kmyefx" 
							     class="form_view_input combo easyui-combobox" 
							     data-options="required: true, editable: false" 
							     validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList}" itemValue="code" itemLabel="name" />
									
					</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">本年累计借方</th>
				<td><form:input path="entity.bnljjf"  
								class="form_view_input combo easyui-validatebox"
								data-options="required: true" />
				</td>
				<th class="panel-header">末级标记</th>
				<td>
					<form:select path="entity.sfmjkm" 
							     class="form_view_input combo easyui-combobox" 
							     data-options="required: true, editable: false" 
							     validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList5}" itemValue="code" itemLabel="name" />
									
					</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">台账标记</th>
				<td>
					<form:select path="entity.tzbj" 
								 class="form_view_input combo easyui-combobox" 
								 data-options="required: true, editable: false" 
								 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList2}" itemValue="code" itemLabel="name" />
									
					</form:select>
				</td>
				<th class="panel-header">科目属性</th>
				<td>
					<form:select path="entity.kmsx" 
								class="form_view_input combo easyui-combobox" 
								data-options="required: true, editable: false" 
								validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList1}" itemValue="code" itemLabel="name" />
					</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">本年累计贷方</th>
				<td>
					<form:input path="entity.bnljdf" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox"/>
				</td>
					<th class="panel-header">日记账类型</th>
				<td>
				    <form:select path="entity.rjzlx" 
				    			 class="form_view_input combo easyui-combobox" 
				    			 data-options="required: true, editable: false" 
				    			 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
						<form:option value="">--请选择--</form:option>
						<form:options items="${paymentSiteList3}" itemValue="code" itemLabel="name" />
					</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">科目级别</th>
				<td><form:input path="entity.kmjb"  
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" 
								value="1" readonly="true"  style="background-color:#A1A1A1"
								 />
				</td>
				<th class="panel-header">科目编号</th>
				<td>
					<form:input path="entity.kmbh" 
								data-options="required: true,
								validType:[
								\"isNumber['${pageContext.request.contextPath}/business/accounting/kemushezhi/yzkmbh.shtml','entity.kmbh','账套代码已经被使用']\",
								\"complexValid['^.{2,3}$','请确认单位名称的长度在2~3个字之间！']\"
								]" 
								class="form_view_input combo easyui-validatebox"
								></form:input>
				</td>
			</tr>
			<tr>
				<th class="panel-header">科目名称</th>
				<td>
					<form:input path="entity.kmmc" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" />
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </td>
			    <th class="panel-header">年初余额</th>
				<td>
					<form:input path="entity.ncye" 
								data-options="required: true" 
								class="form_view_input combo easyui-validatebox" />
				</td>
			</tr>
			
		</table>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
		 <tr>
        <td style="text-align:right;"><font size="2"><b>辅助核算</b></font></td>
        <td style="text-align:left">
            <span class="radioSpan">
                <input type="radio" name="adminFlag" value="0"><font size="1">核算类别1</font>
                <input type="radio" name="adminFlag" value="1"><font size="1">核算类别2</font>
                <input type="radio" name="adminFlag" value="2"><font size="1">核算类别3</font>
            </span>
        </td>
    </tr>
	</form:form>
</div>
<div style="text-align:center;">
<span> <a class="l-btn" onclick="companyHistorySaveOrUpdate()" ><span class="l-btn-left"><span>保存</span></span></a>
			&nbsp;&nbsp;
		</span>
		<span> <a class="l-btn" id="kmsz_quxiao_button"><span class="l-btn-left"><span >取消</span></span></a>
			&nbsp;&nbsp;
		</span>
</div>
