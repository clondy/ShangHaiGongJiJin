<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

$('#cwhscs_save_button').click(function () {
	companyHistorySaveOrUpdate();
                                         });
function companyHistorySaveOrUpdate(){
    $("#cwhscs_edit_form").form('submit', {
        	url: '${pageContext.request.contextPath}/business/accounting/cwhscs/save.shtml',  
            onSubmit: function () {  
            	var isValid = $(this).form('validate');  
            	if (!isValid) {
					$.messager.progress('close');
				}
            	return isValid
            },
            success : function(data) {
            	if(data){
				$('#cwhscs_list_datagrid').datagrid('reload');
				$.messager.alert('通知',"添加成功！");
				$('#cwhscs_ck_window').window('close');
            	}else {  
            		$.messager.alert('通知',"添加失败！");
            		$('#cwhscs_ck_window').window('close');
                }  
    }
    });
};
$('#cwhscs_quxiao_button').click(function () {
	$('#cwhscs_ck_window').window('close');  
    });


</script>
<div class="div_center" id="id" style="height:400px">
	<form:form name="cwhscs_edit_form" id="cwhscs_edit_form" method="post" action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="kmsz_entity_id" />
		<table class="form_view_border" border="1px" cellpadding="0" cellspacing="0" id="table_id">
			<tr>
			<th class="panel-header"><nobr>财务年度</nobr></th>
						<td><form:select path="entity.cwnd" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList6}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
					<th class="panel-header"><nobr>核算单位</nobr></th>
						<td><form:select path="entity.hsdw.id" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList5}" itemValue="id" itemLabel="name" />
							</form:select></td>
			
			</tr>
			<tr>
			<th class="panel-header"><nobr>账套名称</nobr></th>
					<td><form:select path="entity.ztdm.ztdm" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList7}" itemValue="ztdm" itemLabel="ztmc" />
							</form:select></td>
							<th class="panel-header"><nobr>是否允许单向调账凭证</nobr></th>
						<td><form:select path="entity.sfyxdxtz" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			
			</tr>
			<tr>
			<th class="panel-header"><nobr>凭证编号生成方式</nobr></th>
						<td><form:select path="entity.pzbhscfs" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
							<th class="panel-header"><nobr>是否允许插入凭证编号</nobr></th>
					<td><form:select path="entity.sfyxkpzbh" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			
			</tr>
			<tr>
			
					<th class="panel-header"><nobr>凭证编号长度</nobr></th>
						<td><form:select path="entity.pzbhcd" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
					<th class="panel-header"><nobr>凭证编号生成规则</nobr></th>
						<td><form:select path="entity.pzbhscgz" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			
			</tr>
			<tr>
			<th class="panel-header"><nobr>自动凭证制单人</nobr></th>
					<td><form:select path="entity.zdpzzdr" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
							
					<th class="panel-header"><nobr>是否允许凭证反记账</nobr></th>
						<td><form:select path="entity.dfyxpzfjz" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			
			</tr>
			<tr>
			<th class="panel-header"><nobr>是否允许反月结</nobr></th>
						<td><form:select path="entity.sfyxfyj" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
							<th class="panel-header"><nobr>是否允许凭证为空</nobr></th>
					<td><form:select path="entity.sfyxpzwk" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			
			</tr>
			<tr>
			<th class="panel-header"><nobr>是否允许强制月结</nobr></th>
						<td><form:select path="entity.sfyxqzyj" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<option value="1">是</option> 
                                <option value="0">否</option>
							</form:select></td>
			</tr>
		</table>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	</form:form>
</div>
<div style="text-align:center;">
<span> <a class="l-btn" id="cwhscs_save_button"><span
				class="l-btn-left"><span
					>保存</span></span></a>
			&nbsp;&nbsp;
		</span>
		<span> <a class="l-btn" id="cwhscs_quxiao_button"><span
				class="l-btn-left"><span
					>取消</span></span></a>
			&nbsp;&nbsp;
		</span>
</div>
