<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script>
var zdpzService;
$(function () {
	
	zdpzService = new $.BaseService('#zdpz_query_form', '#kmsz_list_datagrid'
            , '#company_form_dialog', '#zdpz_edit_form'
        , {
		actionRootUrl: '<c:url value="/business/accounting/zdpzgl/load" />'
        ,entityTitle: "自动凭证"
        , optionFormatter: function(value, rec) {
        	return  '<a href="#" onClick="jizhangpingzheng('+rec.ID+') ">查看</a>'
         }
        , datagridAddButtonId: 'zdpz_add_button'
        , datagridBatchDeleteButtonId: 'zdpz_batch_delete_button'
        , datagridColumns: [
                          
				{field: 'YWLX', title: '业务类型', width: 100, rowspan: 1, align: 'center'},
				{field: 'ZHAIYAO', title: '摘要', width: 100, rowspan: 1, align: 'center'},
				{field: 'JFFSE', title: '借方发生额', width: 100, rowspan: 1, align: 'center'},
				{field: 'DFFSE', title: '贷方发生额', width: 100, rowspan: 1, align: 'center'},
				{field: 'FJDJS', title: '附件单据数', width: 100, rowspan: 1, align: 'center'},
				{field: 'JZRQ', title: '记账日期', width: 100, rowspan: 1, align: 'center'},
				{field: 'ZD', title: '制单', width: 100, rowspan: 1, align: 'center'},
				{field: 'FH', title: '复核', width: 100, rowspan: 1, align: 'center'},
				{field: 'JZ', title: '记账', width: 100, rowspan: 1, align: 'center'},
				{field: 'PZZTBS', title: '凭证状态标识', width: 100, rowspan: 1, align: 'center'},
				{field: 'HZPZH', title: '汇总凭证号', width: 100, rowspan: 1, align: 'center'},
				{field: 'PZLB', title: '凭证类别', width: 100, rowspan: 1, align: 'center'},
				{field: 'ZZBS', title: '制作标识', width: 100, rowspan: 1, align: 'center'},
				{field: 'XSPZH', title: '显示凭证号', width: 100, rowspan:1, align: 'center'}

        ]
            ,datagridToolbar: []
            , dialogWidth: 800
            , dialogHeight: 600
            , dialogButtons: [
                {
                	id: 'zdpz_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#zdpz_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                kmszHistorySaveOrUpdate();
                            }
    					}else{
                            $.messager.progress('close');
    					}
                        function kmszHistorySaveOrUpdate(){
                            $("#zdpz_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/accounting/zdpzgl'
                                , onSubmit : function() {
									var isValid = $('#zdpz_edit_form').form('validate');
									if (!isValid) {
										$.messager.progress('close');
									}
									return isValid
								},
								success : function(data) {
									$.messager.progress('close');
									console.log(data)
									data = jQuery.parseJSON(data);
									kmszService.dialog.dialog('close');
									$('#ztsz_query_form').submit();
									 kmszService.datagrid.datagrid('reload');
									$.messager.alert('通知',"添加成功！");
                                }
                            });
                        }
                	}
                }
                , {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                    }
                }
            ]
        });
});


$('#hsdw').combobox({
	url:'${pageContext.request.contextPath}/business/accounting/zdpzgl/hsdw.shtml',
    valueField:'ID',
    textField:'TITLE'
});

$('#cwzt').combobox({
	url:'${pageContext.request.contextPath}/business/accounting/zdpzgl/cwzt.shtml',
    valueField:'ZTDM',
    textField:'ZTMC'
});
$('#zdpz_search_button').click(function () {
    $('#zdpz_query_form').submit();
});
//禁用经办
function jizhangpingzheng(id){
	 $('#ck_ck_window').window({
		    title:'记账凭证',
		    width:1214,
		    height:481,
		 	href:'${pageContext.request.contextPath}/business/accounting/zdpzgl/chakan.shtml?ID='+id,
		    modal:true,
		});
}


</script>

<div style="margin: 30px 0;"></div>

<div class="text clearfix" style="text-align: center;">
	<div style="margin-top: 20px">
		<span> <a class="l-btn" id="zdpz_search_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="kmsz_chongzhi_form_chongzhi_button"><span class="l-btn-left"><span
					>重置</span></span></a>
			&nbsp;&nbsp;
		</span> 
		<span> <a class="l-btn" id="company_query_form_zengjia_button" onclick="add()"><span
				class="l-btn-left"><span
					>修改</span></span></a>
				&nbsp;&nbsp;
		</span>
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>
	<div style="margin-top: 0px">
		<span> <form:form name="zdpz_query_form" id="zdpz_query_form"
				method="post" action="" onsubmit="return false;">
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header">记账日期</th>
						<td><input  name="JZRQ"
										class="form_view_input combo easyui-datebox"/></td>
						<th class="panel-header"><nobr>账户分类</nobr></th>
						<td><select name="ASDF" style="width: 190px;height: 27px;" class="easyui-combobox" 
						data-options="required: true, editable: false"> 
							<option value="">--请选择--</option>
							<option value="0">DIVCSS5</option> 
							<option value="1">DIVCSS5</option> 
							</select></td>
					</tr>
					<tr>
							<th class="panel-header">核算单位</th>
							<td style="width: 160px">               
                                <input class="easyui-combobox" id="hsdw" name = "" style="width: 190px;height: 27px;" data-options="valueField:'ID', textField:'TITLE', panelHeight:'auto'" >  
                            </td>
							<th class="panel-header">财务账套</th>
							<td style="width: 160px">               
                                <input class="easyui-combobox" id="cwzt" name = "" style="width: 190px;height: 27px;" data-options="valueField:'ZTDM', textField:'ZTMC', panelHeight:'auto'" >  
                            </td>
					</tr>
				</table>
			</form:form>
			</span>
	</div>
</div>
<table id="kmsz_list_datagrid"></table>
<div id="ck_ck_window"></div>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog"></div>
<div id="changeSite_dialog"></div>
<div id="change_company_info_dialog"></div>
<div id="company_site_form_dialog"></div>