<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
	var ZflzService;
	$(function() {
		ZflzService = new $.BaseService(
				'#zflz_query_form',
				'#zflz_list_datagrid',
				'#zflz_form_dialog',
				'#zflz_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/zflz/" />',
					entityTitle : "总分类账",
					optionFormatter : function(value, rec) {
						
					}
					
					,datagridHasFrozenColumns: false
			        ,datagridToolbar: []
					,mydatagridColumns : [[
					                     {field : 'kmmc',title : '科目',width : 60,rowspan: 1,align : 'center'}, 
                                         {field : 'riqi',title : '日期',width : 60,rowspan: 1,align : 'center'}, 
                                         {field : 'zhaiyao',title : '摘要',width : 60,rowspan: 1,align : 'center'},
                                         {field : 'jffse',title : '借方发生额',width : 60,rowspan: 1,align : 'center'},
                                         {field : 'dffse',title : '贷方发生额',width : 60,rowspan: 1,align : 'center'},
                                         {field : 'yefx',title : '余额方向',width : 60,rowspan: 1,align : 'center'},
                                         {field : 'yue',title : '余额',width : 60,rowspan: 1,align : 'center'}
                                       ]]
					
				});
		
		$('#zflz_query_form_search_button').click(function() {
			$('#zflz_query_form').submit();
		});
		
		$('#reset_button').click(function() {
			$('#zflz_query_form')[0].reset();
		});
	});
	
	//导出Excel
	$('#export5_button').click(function () {
    	exportExcel();
      });
      
    //导出Excel
	function exportExcel(){
		$("#zflz_query_form") .form('submit',{
			url : '${pageContext.request.contextPath}/business/accounting/zflz/excels.shtml',
			success : function(data) {
				var strFullPath = window.document.location.href;
                var strPath = window.document.location.pathname;
                var pos = strFullPath.indexOf(strPath);
                var prePath = strFullPath.substring(0, pos);
                window.location.href = prePath + "/report/" + data;
			}
		});
	}

</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">

	<span style="algin: center"> <a class="l-btn"
		id="zflz_query_form_search_button"><span class="l-btn-left"><span
				class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp; <a class="l-btn" id="reset_button"><span
			class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;
		<a class="l-btn" id="export5_button"><span class="l-btn-left"><span class="l-btn-text">导出Excel</span></span></a>
	</span>
	
	<span> <form:form name="zflz_query_form" id="zflz_query_form"
			method="post" action="" onsubmit="return false;">
			<center>
			<hr>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c"
					border="0px" cellpadding="0" cellspacing="0">
					<tr>
						
						<th class="panel-header">年度</th>
						<td><form:select path="entity.kjnd.id"
										style="width: 184px;height: 30px;"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 >
							<form:option value="">--请选择--</form:option>  
                            <form:options items="${kjndList}" itemValue="id" itemLabel="name"/>  
						</form:select></td>
						
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th class="panel-header">科目</th>
                        <td><form:select path="entity.kmmc.kmbh"
										style="width: 184px;height: 30px;"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 >
							<form:option value="">--请选择--</form:option>  
							<form:options items="${kmmcList}" itemValue="kmbh" itemLabel="kmmc"/>  
						</form:select></td>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>
</div>

<table id="zflz_list_datagrid"></table>
<div id="zflz_form_dialog">Dialog Content</div>
