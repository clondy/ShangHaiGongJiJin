<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="company" items="${KmszsplssjList}" varStatus="status">
    {"id": "${company.id}",
    "kmszls": "${company.kmszls}",
    "sjzt": "${company.sjzt}",
    "spzt": "${company.spzt}",
    "spyj": "${company.spyj}",
    "czy": "${company.czy}",
    "czsj": "${company.czsj}",
    "spr": "${company.spr}",
    "spsj": "${company.spsj}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}