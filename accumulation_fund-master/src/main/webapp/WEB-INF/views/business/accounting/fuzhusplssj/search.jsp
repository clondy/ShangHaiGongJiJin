<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>




<script>
//审批历史
    var SpHistoryService;
    $(function () {
    	SpHistoryService = new $.BaseService('#SP_query_form', '#sp_list'
    	            , '#SP_form_dialog', '#SP_edit_form'
    	            , {
                       
    	                  listUrl:'splx.shtml?entity.hsid=${command.entity.id}'
    	                 ,actionRootUrl: '<c:url value="/business/accounting/fuzhuxglssj/" />'
    	                , entityTitle: "辅助核算审批信息"
    	                ,optionFormatter: function(value, rec) {
    	                }
    	                 , datagridToolbar: []
    				     , datagridHasFrozenColumns: false
    				     , mydatagridColumns: [[
										{field: 'hsxmbm', title: '核算项目编码', width: 120,align: 'center'},
									    {field: 'hsxmmc', title: '核算项目名称', width: 120,align: 'center'},
										{field: 'sfdjmx', title: '是否底级明细', width: 120,align: 'center'},
										{field: 'kjnd', title: '会计年度', width: 120,align: 'center'},
										{field: 'spr', title: '审批人', width: 120,align: 'center'},
										{field: 'spzt', title: '审批状态', width: 120,align: 'center'},
										{field: 'kjnd', title: '会计年度', width: 120,align: 'center'}
    				                        ]] 
                , dialogWidth: 800
                , dialogHeight: 600
            });
    });


</script>

<table id="sp_list"></table>
