
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
$('#dg').datagrid({   
    url:'/fund.accumulation/business/accounting/zdpzmb/list.shtml',
    width:1200
    ,height:200
    ,
    singleSelect:true,
    columns:[[   
        {field:'id', checkbox: true},   
        {field:'xuhao',title:'序号',width:165,align:'center'},   
        {field:'caozuo',title:'操作',width:165,align:'center'},  
        {field:'hsdw',title:'核算单位',width:165,align:'center'},
        {field:'ztmc',title:'账套名称',width:165,align:'center'},   
        {field:'mbmc',title:'模板名称',width:165,align:'center'},   
        {field:'mblx',title:'模板类型',width:165,align:'center'},   
        {field:'mbbh',title:'模板编号',width:170,align:'center'} 
    ]],
   		onCheck:function(){
    	var items = $('#dg').datagrid('getChecked');
	        if((items.length > 0 ? items[0].id : 0) ==1){
	        	$('#grid1').datagrid('resize');  
				$('#grid2').datagrid('resize'); 
				$('#grid3').datagrid('resize'); 
				$('#grid4').datagrid('resize');
				$('#grid7').datagrid('resize');
				$('#grid6').datagrid('resize');
	     	     $('#aaaaa').hide();
	             $('#bbbbb').hide();
	             $('#asdasd').show();
			}else if((items.length > 0 ? items[0].id : 0) ==2){
				$('#grid1').datagrid('resize');  
				$('#grid2').datagrid('resize'); 
				$('#grid3').datagrid('resize'); 
				$('#grid4').datagrid('resize');
				$('#grid7').datagrid('resize');
				$('#grid6').datagrid('resize');
				 $('#asdasd').hide();
				 $('#bbbbb').hide();
				 $('#aaaaa').show();
			}else if((items.length > 0 ? items[0].id : 0) ==3){
				 $('#grid1').datagrid('resize');  
				$('#grid2').datagrid('resize'); 
				$('#grid3').datagrid('resize'); 
				$('#grid4').datagrid('resize');
				$('#grid7').datagrid('resize');
				$('#grid6').datagrid('resize');
				$('#asdasd').hide();
				 $('#aaaaa').hide();
				 $('#bbbbb').show();	
			}
   }
    
});  


</script>

<div style="margin: 10px 0;"></div>


<div class="text clearfix" style="text-align: center;">
<div>&nbsp;&nbsp;</div>
	<span style="algin: center"> <a class="l-btn"
		id="ztsz_query_form_search_button"><span class="l-btn-left"><span
				class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp; <a class="l-btn" id="reset_button"><span
			class="l-btn-left"><span class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;<a class="l-btn" id="add_button"><span
			class="l-btn-left"><span class="l-btn-text">新增</span></span></a>
		&nbsp;&nbsp;<a class="l-btn" id="add_button"><span
			class="l-btn-left"><span class="l-btn-text">删除</span></span></a>
		&nbsp;&nbsp;
	</span> <a class="l-btn" id="SpHistory_button"><span class="l-btn-left"
		><span class="l-btn-text">审批历史</span></span></a>
	&nbsp;&nbsp; <a class="l-btn" id="XgHistory_button"><span
		class="l-btn-left" ><span
			class="l-btn-text">历史数据</span></span></a>
	<div>&nbsp;</div>
	<span> <form:form name="rzdz_query_form" id="rzdz_query_form"
			method="post" action="" onsubmit="return false;">
		</form:form>
	</span>
	<span> <form:form name="ztsz_query_form" id="ztsz_query_form"
			method="post" action="" onsubmit="return false;">
			<center>
				<table bordercolordark="#FFFFFF" bordercolorlight="#45b97c"
					border="0px" cellpadding="0" cellspacing="0">
					<tr>
						<th class="panel-header">核算单位</th>
						<td><form:select path=""
								style="width: 184px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:option value="2">上海市公积金管理中心</form:option>
							</form:select></td>
								<th class="panel-header">账套名称</th>
						<td><form:select path=""
								style="width: 184px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:option value="2">公积金账套</form:option>
							</form:select></td>
								<th class="panel-header">模板类型</th>
						<td><form:select path=""
								style="width: 184px;height: 27px;"
								class="form_view_input combo easyui-combobox"
								data-options="required: true, editable: false">
								    <form:option value="">--请选择--</form:option>
									<form:option value="2">汇缴托收</form:option>
									<form:option value="3">公积金提取</form:option>
									<form:option value="4">补充公积金提取</form:option>
							</form:select></td>
								<th class="panel-header">模板编码</th>
						<td><td><form:input path="entity.ztms"
								class="form_view_input combo easyui-validatebox"
								style="height: 27px;" /></td>
					</tr>
				</table>
				<div>&nbsp;&nbsp;&nbsp;</div>
			</center>
		</form:form>
	</span>
</div>
<center>

<table id="dg"></table>
<div>&nbsp;</div>
<hr>
<div >&nbsp;</div>



<div id="asdasd">
<h1>财务凭证模板</h1>

<div>&nbsp;</div>
<table class="easyui-datagrid" id="grid1" style="width:1200px;height:200px">
    <thead>
		<tr>
		     	<th data-options="field:'flxh',width:150,align:'center'">分录序号</th>
				<th data-options="field:'kmbm',width:150,align:'center'">科目编码</th>
				<th data-options="field:'kmmc',width:150,align:'center'">科目名称</th>
				<th data-options="field:'yyfz',width:150,align:'center'">一元辅助</th>
				<th data-options="field:'eyfz',width:150,align:'center'">二元辅助</th>
				<th data-options="field:'qzfs',width:148,align:'center'">取值方式</th>
			    <th data-options="field:'jdfx',width:145,align:'center'">借贷方向</th>
			    <th data-options="field:'zhaiyao',width:145,align:'center'">摘要</th>
			
		</tr>
    </thead>
    <tbody>
		<tr>
			<td>1</td><td>101</td><td>住房公积金存款</td><td>001</td><td></td><td>={托收金额}</td><td>借</td><td>汇缴托收</td>
		</tr>
		<tr>
			<td>2</td><td>201</td><td>住房公积金</td><td>002</td><td>003,004</td><td>={托收金额}</td><td>贷</td><td>汇缴托收</td>
		</tr>
	
		
	</tbody>
</table>
<div>&nbsp;</div>

<hr>
<div>&nbsp;</div>
<h1>辅助账凭证模板</h1>
<div>&nbsp;</div>


<table class="easyui-datagrid" id="grid2" style="width:1200px;height:200px">
    <thead>
		<tr>
			<th data-options="field:'flxh',width:130,align:'center'">分录序号</th>
				<th data-options="field:'glkmbm',width:130,align:'center'">关联科目编码</th>
				<th data-options="field:'yyfz',width:130,align:'center'">一元辅助</th>
				<th data-options="field:'yymcgs',width:130,align:'center'">一元名称公式</th>
				<th data-options="field:'eyfz',width:130,align:'center'">二元辅助</th>
				<th data-options="field:'eymcgs',width:130,align:'center'">二元名称公式</th>
			    <th data-options="field:'qzfs',width:130,align:'center'">取值方式</th>
   			    <th data-options="field:'jdfx',width:130,align:'center'">借贷方向</th>
   			    <th data-options="field:'zhaiyao',width:150,align:'center'">摘要</th>
		</tr>
    </thead>
    <tbody>
		<tr>
			<td>1</td><td>101</td><td>001</td><td>=02</td><td></td><td></td><td>={托收金额}</td><td>借</td><td>汇缴托收</td>

		</tr>
		<tr>
			<td>2</td><td>201</td><td>002</td><td>=0101</td><td></td><td></td><td>={托收金额}</td><td>贷</td><td>汇缴托收</td>
		</tr>
		<tr>
			<td>2</td><td>201</td><td>002</td><td>=0101</td><td>02</td><td>={管理部}</td><td>={托收金额}</td><td>贷</td><td>汇缴托收</td>
		</tr>
		<tr>
			<td>2</td><td>201</td><td>002</td><td>=0101</td><td>07</td><td>={组织机构}</td><td>={托收金额}</td><td>贷</td><td>汇缴托收</td>
		</tr>
	</tbody>
</table>
</div>
<div id="aaaaa"  style="display:none">
<h1>财务凭证模板</h1>

<div>&nbsp;</div>
<table class="easyui-datagrid" id="grid3" style="width:1200px;height:200px">
    <thead>
		<tr>
		     	<th data-options="field:'flxh',width:150,align:'center'">分录序号</th>
				<th data-options="field:'kmbm',width:150,align:'center'">科目编码</th>
				<th data-options="field:'kmmc',width:150,align:'center'">科目名称</th>
				<th data-options="field:'yyfz',width:150,align:'center'">一元辅助</th>
				<th data-options="field:'eyfz',width:150,align:'center'">二元辅助</th>
				<th data-options="field:'qzfs',width:148,align:'center'">取值方式</th>
			    <th data-options="field:'jdfx',width:145,align:'center'">借贷方向</th>
			    <th data-options="field:'zhaiyao',width:145,align:'center'">摘要</th>
			
		</tr>
    </thead>
    <tbody>
    <tr>
			<td>1</td><td>201</td><td>住房公积金</td><td>002</td><td>003,004</td><td>={托收金额}</td><td>贷</td><td>公积金提取</td>
		</tr>
	
		<tr>
			<td>2</td><td>101</td><td>住房公积金存款</td><td>005</td><td></td><td>={托收金额}</td><td>借</td><td>公积金提取</td>
		</tr>
		
		
	</tbody>
</table>
<div>&nbsp;</div>

<hr>
<div>&nbsp;</div>
<h1>辅助账凭证模板</h1>
<div>&nbsp;</div>


<table class="easyui-datagrid" id="grid4" style="width:1200px;height:200px">
    <thead>
		<tr>
			<th data-options="field:'flxh',width:130,align:'center'">分录序号</th>
				<th data-options="field:'glkmbm',width:130,align:'center'">关联科目编码</th>
				<th data-options="field:'yyfz',width:130,align:'center'">一元辅助</th>
				<th data-options="field:'yymcgs',width:130,align:'center'">一元名称公式</th>
				<th data-options="field:'eyfz',width:130,align:'center'">二元辅助</th>
				<th data-options="field:'eymcgs',width:130,align:'center'">二元名称公式</th>
			    <th data-options="field:'qzfs',width:130,align:'center'">取值方式</th>
   			    <th data-options="field:'jdfx',width:130,align:'center'">借贷方向</th>
   			    <th data-options="field:'zhaiyao',width:150,align:'center'">摘要</th>
		</tr>
    </thead>
    <tbody>
		<tr>
			<td>1</td><td>201</td><td>002</td><td>=0103</td><td></td><td></td><td>={提取金额}</td><td>借</td><td>公积金提取</td>

		</tr>
		<tr>
			<td>2</td><td>201</td><td>002</td><td>=0103</td><td>03</td><td>={管理部}</td><td>={提取金额}</td><td>借</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>3</td><td>201</td><td>002</td><td>=0103</td><td>04</td><td>={组织机构}</td><td>={提取金额}</td><td>借</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>4</td><td>101</td><td>001</td><td>=05</td><td></td><td></td><td>={提取金额}</td><td>贷</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>5</td><td>101</td><td>001</td><td>=05</td><td>04</td><td>={银行}</td><td>={提取金额}</td><td>贷</td><td>公积金提取</td>
		</tr>
	</tbody>
</table>
</div>


<div id="bbbbb" style="display:none">
<h1>财务凭证模板</h1>

<div>&nbsp;</div>
<table class="easyui-datagrid" id="grid6" style="width:1200px;height:200px">
    <thead>
		<tr>
		     	<th data-options="field:'flxh',width:150,align:'center'">分录序号</th>
				<th data-options="field:'kmbm',width:150,align:'center'">科目编码</th>
				<th data-options="field:'kmmc',width:150,align:'center'">科目名称</th>
				<th data-options="field:'yyfz',width:150,align:'center'">一元辅助</th>
				<th data-options="field:'eyfz',width:150,align:'center'">二元辅助</th>
				<th data-options="field:'qzfs',width:148,align:'center'">取值方式</th>
			    <th data-options="field:'jdfx',width:145,align:'center'">借贷方向</th>
			    <th data-options="field:'zhaiyao',width:145,align:'center'">摘要</th>
			
		</tr>
    </thead>
    <tbody>
		<tr>
			<td>1</td><td>101</td><td>住房公积金存款</td><td>001</td><td></td><td>={托收金额}</td><td>借</td><td>汇缴托收</td>
		</tr>
		<tr>
			<td>2</td><td>201</td><td>住房公积金</td><td>002</td><td>003,004</td><td>={托收金额}</td><td>贷</td><td>汇缴托收</td>
		</tr>
	
		
	</tbody>
</table>
<div>&nbsp;</div>

<hr>
<div>&nbsp;</div>
<h1>辅助账凭证模板</h1>
<div>&nbsp;</div>


<table class="easyui-datagrid" id="grid7" style="width:1200px;height:200px">
    <thead>
		<tr>
			<th data-options="field:'flxh',width:130,align:'center'">分录序号</th>
				<th data-options="field:'glkmbm',width:130,align:'center'">关联科目编码</th>
				<th data-options="field:'yyfz',width:130,align:'center'">一元辅助</th>
				<th data-options="field:'yymcgs',width:130,align:'center'">一元名称公式</th>
				<th data-options="field:'eyfz',width:130,align:'center'">二元辅助</th>
				<th data-options="field:'eymcgs',width:130,align:'center'">二元名称公式</th>
			    <th data-options="field:'qzfs',width:130,align:'center'">取值方式</th>
   			    <th data-options="field:'jdfx',width:130,align:'center'">借贷方向</th>
   			    <th data-options="field:'zhaiyao',width:150,align:'center'">摘要</th>
		</tr>
    </thead>
    <tbody>
		<tr>
			<td>1</td><td>201</td><td>002</td><td>=0203</td><td></td><td></td><td>={提取金额}</td><td>借</td><td>公积金提取</td>

		</tr>
		<tr>
			<td>2</td><td>201</td><td>002</td><td>=0203</td><td>03</td><td>={管理部}</td><td>={提取金额}</td><td>借</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>3</td><td>201</td><td>002</td><td>=0203</td><td>04</td><td>={组织机构}</td><td>={提取金额}</td><td>借</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>4</td><td>101</td><td>001</td><td>=05</td><td></td><td></td><td>={提取金额}</td><td>贷</td><td>公积金提取</td>
		</tr>
		<tr>
			<td>5</td><td>101</td><td>001</td><td>=05</td><td>04</td><td>={银行}</td><td>={提取金额}</td><td>贷</td><td>公积金提取</td>
		</tr>
	</tbody>
</table>
</div>
</center>