<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<div class="div_center" >
    <form:form name="company_account_fund_edit_form" id="company_account_fund_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.id" />
       <table align="center"  border="1px" bordercolor="blue" width="600px">
         <tr height="40px" align="left">
             <td style="border-style:none;padding-left:40px;width:140px" size="2px" >是否允许单向调用凭证</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
                 <c:if test="${entity.sfyxdxtz==1}">
				   <input type="radio" name="sfyxdxtz" id="Radios1" value="1" checked>允许
				 </c:if>
				 <c:if test="false">
				   <input type="radio" name="sfyxdxtz" id="Radios1" value="1" >允许
				 </c:if>
			    </label>
			</td>
             <td style="border-style:none;width:120px">
                 <c:if test="false">
				   <input type="radio" name="sfyxdxtz" id="Radios2" value="2" >不允许
				 </c:if>
				 <c:if test="false">
				   <input type="radio" name="sfyxdxtz" id="Radios2" value="2" checked>不允许
				 </c:if>
			 </td>
             <td style="border-style:none;width:140px;" size="2px">是否允许凭证摘要为空</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline2" id="Radios3" value="option1" checked>允许
			    </label>
			 </td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline2" id="Radios4" value="option1" checked>不允许
			    </label>
			 </td>
        </tr>
        
        <tr height="40px" align="left">
             <td style="border-style:none;padding-left:40px" size="2px" >凭证编号生成方式</td>
             <td style="border-style:none;width:60px"> 
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline3" id="Radios5" value="option1" >允许
			    </label>
			 </td>
             <td style="border-style:none;width:60px"> 
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline3" id="Radios6" value="option1" >不允许
			    </label>
			 </td>
             <td style="border-style:none;width:140px" size="2px">是否允许插入凭证编号</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline4" id="Radios7" value="option1" >允许
			    </label></td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline4" id="Radios8" value="option1" checked>不允许
			    </label>
			 </td>
        </tr>
        
        <tr height="40px" align="left">
             <td style="border-style:none;padding-left:40px" size="2px">凭证编号长度</td>
             <td style="border-style:none" colspan="2">
               <div class="spinner" >
                   <input type="text" readonly="readonly" value="1" id="t1" style="width:40px"/>
                   <div class="arrow" style="height:15px"><a onclick="addminus('t1', true)">▲</a><a onclick="addminus('t1')">▼</a></div>
                   </div>
               </div>
             </td>
             <td style="border-style:none" size="2px">凭证编号生成规则</td>
             <td style="border-style:none" colspan="2">
                  <select class="form-control" id="selectLevel" width="100px" hight="30px">
					<option value="1">一级菜单caidan</option>
					<option value="2" selected="selected">二级菜单</option>
	  			  <lect>
	  	     </td>
        </tr>
       
        <tr height="40px" align="left">
             <td style="border-style:none;padding-left:40px;width:140px"  size="2px">自动凭证制单人</td>
             <td style="border-style:none;border-style:none" colspan="2">
                <span width="100px">
                  <select class="form-control" id="selectLevel">
					<option value="1">一级菜单caidan</option>
					<option value="2" selected="selected">二级菜单</option>
	  			  <lect>
	  			</span>
	  	     </td>
             <td style="border-style:none;width:140px" size="2px">是否允许凭证反记账</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline8" id="Radios15" value="option1" checked>允许
			    </label></td>
             <td style="border-style:none;width:100px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline8" id="Radios16" value="option1" checked>不允许
			    </label>
			 </td>
        </tr>
      <tr height="40px" align="left">
             <td style="border-style:none;padding-left:40px;width:140px" size="2px" >是否允许反月结</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline9" id="Radios17" value="option1" checked>允许
			    </label>
			 </td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline9" id="Radios18" value="option1" checked>不允许
			    </label>
			 </td>
             <td style="border-style:none;width:120px;" size="2px">是否允许强制月结</td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline10" id="Radios19" value="option1" checked>允许
			    </label>
			 </td>
             <td style="border-style:none;width:60px">
                <label class="radio-inline">
				   <input type="radio" name="optionsRadiosinline10" id="Radios20" value="option1" checked>不允许
			    </label>
			 </td>
        </tr>
    </table>
        
    </form:form>
</div>
