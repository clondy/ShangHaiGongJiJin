<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<script type="text/javascript">
	/* 	财务核算参数设置 */
	var cwhsService;
	$(function () { 
		cwhsService = new $.BaseService(
        		'#cwhscs_query_form',
        		'#cwhscs_list_datagrid', 
        		'#company_form_dialog', 
        		'#cwhscs_edit_form',
        	{
                actionRootUrl: '<c:url value="/business/accounting/cwhscs/" />',
                 entityTitle: "财务核算参数",
                 optionFormatter: function(value, rec) {
                	 return $.formatString('<a href="#" title="编辑" onClick="cwhsService.dialog.dialog(\'{0}\').dialog(\'{1}\',\'{2}\');">编辑</a>','open','refresh',cwhsService.actionRootUrl+ cwhsService.formUrl+"?entity.id="+rec.id +'&lsid='+rec.lsid )
                 },
                datagridAddButtonId: 'company_add_button' ,
                datagridBatchDeleteButtonId: 'company_batch_delete_button', 
                datagridHasFrozenColumns:true,
                datagridColumns: [
                    {field: 'ZTDM', title: '账套名称', width: 100, rowspan: 2, align: 'center'},
                    {field: 'HSDW', title: '核算单位', width: 100, rowspan: 2, align: 'center'},
                    {field: 'SFYXDXTZ', title: '是否允许单向调账凭证', width: 230, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'SFYXPZWK', title: '是否允许凭证摘要为空', width: 230, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'PZBHSCFS', title: '凭证编号生成方式', width: 180, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'SFYXKPZBH', title: '是否允许插入凭证编号', width: 230, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'PZBHCD', title: '凭证编号长度', width: 145, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'PZBHSCGZ', title: '凭证编号生成规则', width: 180, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'ZDPZZDR', title: '自动凭证制单人', width: 160, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'DFYXPZFJZ', title: '是否允许凭证反记账', width: 210, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'SFYXFYJ', title: '是否允许反月结', width: 160, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}},
                    {field: 'SFYXQZYJ', title: '是否允许强制月结', width: 180, rowspan: 2, align: 'center',formatter: function(value,row,index){
        				if (value == '1'){
        					return '是';
        				} else {
        					return '否';
        				}
        			}}
               ], 
                  datagridToolbar: []
                , dialogWidth: 800
                , dialogHeight: 600
            });
	});
	//增加
	function add() {
			 $('#cwhscs_ck_window').window({
				    title:'财务核算参数维护',
				    width:800,
				    height:500,
				 	href:'${pageContext.request.contextPath}/business/accounting/cwhscs/site_record1.shtml',
				    modal:true
				});
		}
	//历史数据
	function lsshuju(){
		var row = $('#cwhscs_list_datagrid').datagrid('getSelections');
		if(row == null || row.length == 0)
		{
			 $('#lssj_ck_window').window({
				    title:'财务核算历史数据',
				    width:1214,
				    height:444,
				    href:'${pageContext.request.contextPath}/business/accounting/cwhscs_xgls/search1.shtml',
				    modal:true
				});
		}else{
		var tempid=row[0].lsid;
		 if (row.length == 1) {  

			 $('#lssj_ck_window').window({
				    title:'财务核算历史数据',
				    width:1214,
				    height:444,
				    href:'${pageContext.request.contextPath}/business/accounting/cwhscs_xgls/search1.shtml?cwhscsXgls='+tempid,
				    modal:true,
				});
	     } else {  
	         $.messager.alert('提示', '请选择一条记录！', 'warning');  
	     }
	}
	}
	//删除
	function shanchu(){
		var ids=[];
		var row = $('#cwhscs_list_datagrid').datagrid('getSelections');
		if(row.length==0){  
			alert("请至少选择一行数据!");  
			return false;  
		}
		else{
			for(var i = 0; i < row.length; i++) {
				ids[i]=row[i].id;
		          
		    }
			delet(ids); 
	           }
	}
	//删除
		function delet(ids) {

			var checkedData = {};
				for(var i = 0; i < ids.length; i++) {
					checkedData["ids[" + i + "]"] = ids[i];
				}
				var dataCount = Object.keys(checkedData).length;
				var prefix = ( 1 < dataCount )? "批量": "";
				if( dataCount ) {

					if( confirm("确定要删除吗？？？") ) {

						$.ajax({
							url: '<c:url value="/business/accounting/cwhscs/delete.shtml" />'
							, type : "post"
							, dataType : 'json'
							, data : checkedData
							, success : function(data, response, status) {
				
								if( jQuery.parseJSON('' + data) ) {
									
									$.messager.alert('通知', prefix + "删除成功。");
									kmszService.datagrid.datagrid('clearChecked');
									kmszService.datagrid.datagrid('reload');
								} else {
									
									$.messager.alert('通知', prefix + "删除失败。");
								}
							}
						});
					} else {

	 					kmszService.datagrid.datagrid('clearChecked');
					}
				} 
			}
	//审批历史
		function shenpilishi()
		{
			var row = $('#cwhscs_list_datagrid').datagrid('getSelections');
			if(row == null || row.length == 0)
			{
				 $('#spls_ck_window').window({
					    title:'审批历史数据查询',
					    width:803,
					    height:444,
					 	href:'${pageContext.request.contextPath}/business/accounting/cwhscs_spls/search1.shtml',
					    modal:true
					});
			}else{
			var tempid=row[0].lsid;
			 if (row.length == 1) {  
				 $('#lssj_ck_window').window({
					    title:'审批历史数据查询',
					    width:803,
					    height:444,
					 	href:'${pageContext.request.contextPath}/business/accounting/cwhscs_spls/search1.shtml?entity.cwhscsXgls='+tempid,
					    modal:true,
					});
		     } else {  
		         $.messager.alert('提示', '请选择一条记录！', 'warning');  
		     }
		}
			
			
			}
	
	$('#cwhscs_chongzhi_form_chongzhi_button').click(function () {
        $('#cwhscs_query_form')[0].reset();
    }); 
	  $('#cwhscs_query_form_search_button').click(function () {
	        $('#cwhscs_query_form').submit();
	    });
	  
	  $('#cwhscs_search_button').click(function () {
	    	shenpilishi();
	      }); 
	    
</script>
<div style="margin: 10px 0;"></div>
<div class="text clearfix" style="text-align: center;">
 <div style="margin-top: 20px">
		<span> <a class="l-btn" id="cwhscs_query_form_search_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="cwhscs_chongzhi_form_chongzhi_button"><span class="l-btn-left"><span
					>重置</span></span></a>
			&nbsp;&nbsp;
		</span> 
		<span> <a class="l-btn" id="cwhscs_query_form_zengjia_button" onclick="add()"><span
				class="l-btn-left"><span
					>增加</span></span></a>
				&nbsp;&nbsp;
		</span>
			<span>
				<a class="l-btn" id="kmsz_delete_button"  onclick="shanchu()"><span class="l-btn-left"><span
					>删除</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="cwhscs_search_button"><span class="l-btn-left"><span
					>审批历史</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="kmsz_lssj_button" onclick="lsshuju()"><span class="l-btn-left"><span
					>历史数据</span></span></a>
				&nbsp;&nbsp;
			</span> 
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>
	<div style="margin-top: 0px">
		<span> <form:form name="cwhscs_query_form" id="cwhscs_query_form"
				method="post" action="" onsubmit="return false;">
				<table class="form_view_border" bordercolordark="#FFFFFF"
					bordercolorlight="#45b97c" border="px" cellpadding="0"
					cellspacing="0" style="">
					<tr>
						<th class="panel-header"><nobr>账套名称</nobr></th>
						<td><form:select path="entity.ztdm.ztdm"
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList7}" itemValue="ztdm"
									itemLabel="ztmc" />
							</form:select></td>
						<th class="panel-header"><nobr>财务年度</nobr></th>
						<td><form:select path="entity.cwnd" 
								style="width: 156px;height: 27px;" class=" easyui-combobox"
								data-options="required: true, editable: false">
								<form:option value="">--请选择--</form:option>
								<form:options items="${paymentSiteList}" itemValue="name"
									itemLabel="name" />
							</form:select></td>
						<th class="panel-header"><nobr>状态</nobr></th>
						<td><form:input path="entity.pzbhcd"
								class="form_view_input combo easyui-validatebox" id="input"
								style="height: 27px;" /></td>
					</tr>
				</table>
			</form:form>

		</span>
	</div>
	</div>
<table id="cwhscs_list_datagrid"></table>
<div id="cwhscs_ck_window"></div>
<div id="lssj_ck_window"></div>
<div id="spls_ck_window"></div>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog"></div>
<div id="changeSite_dialog"></div>
<div id="change_company_info_dialog"></div>
<div id="company_site_form_dialog"></div>