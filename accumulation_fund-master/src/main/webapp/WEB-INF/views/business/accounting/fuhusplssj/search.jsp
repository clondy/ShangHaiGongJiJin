<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>




<script>
//审批历史
    var SpHistoryService;
    $(function () {
    	SpHistoryService = new $.BaseService('#SP_query_form', '#sp_list'
    	            , '#SP_form_dialog', '#SP_edit_form'
    	            , {
                       
    	                  listUrl:'splx.shtml?entity.ztid=${command.entity.id}'
    	                 ,actionRootUrl: '<c:url value="/business/accounting/ztxgHistory/" />'
    	                , entityTitle: "财务账套审批信息"
    	                ,optionFormatter: function(value, rec) {
    	                }
    	                 , datagridToolbar: []
    				     , datagridHasFrozenColumns: false
    				     , mydatagridColumns: [[
												{field: 'id', title: '编号', width: 100, align: 'center'},
												{field: 'hsdw', title: '核算单位名称', width: 100, align: 'center'},
												{field: 'ztmc', title: '账套名称', width: 100, align: 'center'},
												{field: 'spyj', title: '审批意见', width: 100, align: 'center'},
    				                            {field: 'sjzt', title: '数据状态', width: 100, align: 'center'},
    				                            {field: 'spzt', title: '审批状态', width: 100, align: 'center'},
    				                            {field: 'czy', title: '操作员', width: 100, align: 'center'},
    				                            {field: 'czsj', title: '操作时间', width: 100, align: 'center'}


    				                        ]] 
                , dialogWidth: 800
                , dialogHeight: 600
            });
    });


</script>

<table id="sp_list"></table>
