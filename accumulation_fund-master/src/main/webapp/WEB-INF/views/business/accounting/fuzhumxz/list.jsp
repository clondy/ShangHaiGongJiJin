<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="fuzhuzhang" items="${FuzhuzhangList}" varStatus="status">
    {
     "id": "${fuzhuzhang.id}",
     "riqi": "${fuzhuzhang.riqi}",
     "hsxmbm": "${fuzhuzhang.hsxmmc.hsxmbm}",
     "hsxmmc": "${fuzhuzhang.hsxmmc.hsxmmc}",
     "pzbh": "${fuzhuzhang.pzbh}",
     "zhaiyao": "${fuzhuzhang.zhaiyao}",
     "jffse": "${fuzhuzhang.jffse}",
     "dffse": "${fuzhuzhang.dffse}",
     "yefx": "${fuzhuzhang.yefx.name}",
     "yue": "${fuzhuzhang.yue}"

    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]

}


