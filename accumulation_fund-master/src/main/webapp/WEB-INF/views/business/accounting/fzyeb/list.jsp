<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Ztsz" items="${FzyebList}" varStatus="status">
	{
	    "kmbm": "${Ztsz.kmbm.kmbh}",
	    "kmmc": "${Ztsz.kmbm.kmmc}",
	    "kmfx": "${Ztsz.fmfx}",
	    "hsxmbm": "${Ztsz.hsxmbm.hsxmbm}",
	    "hsxmmc": "${Ztsz.hsxmbm.hsxmmc}",
	    "qcye": "${Ztsz.qcye}",
	    "bqjffse": "${Ztsz.bqjffse}",
	    "bqdffse": "${Ztsz.bqdffse}",
	    "ye": "${Ztsz.ye}"
	}
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]}