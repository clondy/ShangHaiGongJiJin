<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="div_center" style="margin-top: 0px">
	<form:form name="fzlxsz_edit_form" id="fzlxsz_edit_form"
		method="post" style="width:800px" action="" onsubmit="return false;">
		<form:hidden path="entity.id" id="fzlxsz_entity_id" />
		<center>
			<table bordercolordark="#45b97c" bordercolorlight="#45b97c"
				border="0px" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<th class="panel-header">核算单位</th>
					<td><form:select path="entity.hsdw.id" 
									style="width: 184px;height: 27px;"
							     	class="form_view_input combo easyui-combobox" 
							     	data-options="required: true, editable: false" 
							     	validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<!-- itemValue="id" 实际的值  itemLabel="name" 显示的名称-->
							<form:options items="${hsdwList}" itemValue="id" itemLabel="name" />
						</form:select>
					</td>
					<th class="panel-header">帐套编号</th>
					<td><form:input path="entity.ztbh" 
							data-options="required: true" style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"
							></form:input>
					</td>
				</tr>
				<tr>
					<th class="panel-header">核算类别编码</th>
					<td><form:input path="entity.hslxbm" 
							data-options="required: true"  style="width:184px;height: 27px;"
							validType="isNumber['${pageContext.request.contextPath}/business/accounting/fuzhuleixingsz/yzhslxbm.shtml','entity.hslxbm','核算类别编码已经被使用']" 
							class="form_view_input combo easyui-validatebox"
							></form:input>
					</td>
					<th class="panel-header">核算类别名称</th>
					<td><form:input path="entity.hslxmc" 
							data-options="required: true" style="width:184px;height: 27px;"
							class="form_view_input combo easyui-validatebox"
							></form:input>
					</td>
				</tr>
				<tr>
					<th class="panel-header">类别分组说明</th>
					<td>
						<form:select path="entity.hsfzbm.hsfzbm" 
									style="width: 184px;height: 27px;"
							     	class="form_view_input combo easyui-combobox" 
							     	data-options="required: true, editable: false" 
							     	validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							   
							<form:option value="">--请选择--</form:option>
							<form:options items="${fzlxhsfzList}" itemValue="hsfzbm" itemLabel="hsfzsm" />
						</form:select>
					</td>
					<th class="panel-header">核算分类</th>
					<td>
						<form:select path="entity.hsfl"
							style="width: 184px;height: 27px;"
							class="form_view_input combo easyui-combobox"
							data-options="required: true, editable: false"
							validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
							<form:option value="">--请选择--</form:option>
							<form:option value="一元核算">一元核算</form:option>
							<form:option value="二元核算">二元核算</form:option>
							<form:option value="一元核算/二元核算">一元核算/二元核算</form:option>
						</form:select>
					</td>
				</tr>
			</table>
		</center>
	</form:form>
	
	
</div>
