<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script>
	var RzdzService;
	$(function() {
		RzdzService = new $.BaseService(
				'#rzdz_query_form',
				'#rzdz_list_datagrid',
				'#rzdz_form_dialog',
				'#rzdz_edit_form',
				{
					actionRootUrl : '<c:url value="/business/accounting/rzdz/" />',
					entityTitle : "日终对账"
			        ,datagridToolbar: []
					,mydatagridColumns : [[{field : 'km',title : '科目',width : 220,align : 'center'} ,
					                       {field : 'cz',title : '操作',width : 120,align : 'center'} 
					                       
                                        ]],
					dialogWidth : 800,
					dialogHeight : 600,
					dialogButtons : []
				});

		$('#duizhang_button').click(function() {
			
				ZflzService = new $.BaseService(
						'#rzdz_query_form',
						'#rzdz_list_datagrid',
						'#rzdz_form_dialog',
						'#rzdz_edit_form',
						{
							actionRootUrl : '<c:url value="/business/accounting/rzdz/" />',
							entityTitle : "日终对账"
					        ,datagridToolbar: []
							,mydatagridColumns : [[{field : 'km',title : '科目',width : 220,align : 'center'} ,
							                       {field : 'cz1',title : '操作',width : 120,align : 'center'} 
							                       
		                                        ]],
							dialogWidth : 800,
							dialogHeight : 600,
							dialogButtons : []
						});
			
		});
		
	
	});
	

</script>

<div style="margin: 10px 0;"></div>

<div class="text clearfix" style="text-align: center;">

	<span style="algin: center;"> <a class="l-btn"
		id="duizhang_button"><span class="l-btn-left"><span
				class="l-btn-text" style="width:50px">对账</span></span></a>
	</span>
	<div>&nbsp;</div>
	<span> <form:form name="rzdz_query_form" id="rzdz_query_form"
			method="post" action="" onsubmit="return false;">
		</form:form>
	</span>
</div>


<table id="rzdz_list_datagrid"></table>
<div id="rzdz_form_dialog">Dialog Content</div>
