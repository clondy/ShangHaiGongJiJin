<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

var zydyService;
$(function () {
    zydyService = new $.BaseService('#zydy_query_form', '#zydy_list_datagrid'
        , '#zydy_form_dialog', '#zydy_edit_form'
        , {
        actionRootUrl: '<c:url value="/business/accounting/zhaiyao/" />'
        , entityTitle: "摘要定义"
        , optionFormatter: function(value, rec) {
        	console.info(rec);
			return '<a href="#" title="修改" onClick="zydyService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + zydyService.actionRootUrl + zydyService.formUrl + '?entity.id=' + rec.id + '\');">'+'修改'+'</a>'
					+ '&nbsp;&nbsp;'
					+ '<a href="#" title="删除" onClick="zydyService.datagrid.datagrid(\'checkRow\', zydyService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + zydyService.datagridBatchDeleteButtonId + '\').click();" >'+'删除'+'</a>'
        }
        , datagridAddButtonId: 'zydy_add_button'
        , datagridBatchDeleteButtonId: 'zydy_batch_delete_button'
        	, datagridToolbar: [{
            	id: 'zydy_batch_delete_button'
     			,text: '批量删除'
       			, disabled: false
       			, iconCls: 'icon-remove'
       			, handler: function() {

       			var checkedData = {};
       			var items = zydyService.datagrid.datagrid('getChecked');
     			for(var i = 0; i < items.length; i++) {
     			
     				checkedData["ids[" + i + "]"] = items[i].id;
     			}
        			
       			var dataCount = Object.keys(checkedData).length;
     			var prefix = ( 1 < dataCount )? "批量": "";
     			if( dataCount ) {
     	
     				if( confirm("确定要删除吗？？？") ) {
     	
     					$.ajax({
     						url: zydyService.actionRootUrl + zydyService.deleteUrl
     					, type : "post"
     						, dataType : 'json'
     						, data : checkedData
     						, success : function(data, response, status) {
     		
     							if( jQuery.parseJSON('' + data) ) {
     							
     								$.messager.alert('通知', prefix + "删除成功。");
     								zydyService.datagrid.datagrid('clearChecked');
     								zydyService.datagrid.datagrid('reload');
     							} else {
     							
     								$.messager.alert('通知', prefix + "删除失败。");
     							}
     					}
     					});
     				} else {
     					zydyService.datagrid.datagrid('clearChecked');
     				}
        			} else {
        					$.messager.alert('通知', "请选中数据后再执行删除。");
        				}
        			}
        		}]
             
       	, datagridColumns: [
            {field: 'ztbh', title: '账套编号', width: 100, rowspan: 1, align: 'center'},
            {field: 'hsdw', title: '核算单位', width: 100, rowspan: 1, align: 'center'},
            {field: 'zylx', title: '摘要类型', width: 100, rowspan: 2, align: 'center'},
            {field: 'zjm', title: '助记码', width: 100, rowspan: 1, align: 'center'},
            {field: 'zynr', title: '摘要', width: 100, rowspan: 1, align: 'center'}
        ]
        
            , dialogWidth: 800
            , dialogHeight: 600
            , dialogButtons: [
                {
                	id: 'zydy_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#zydy_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                zydyHistorySaveOrUpdate();
                            }
    					}else{
                            $.messager.progress('close');
    					}
                        function zydyHistorySaveOrUpdate(){
                            $("#zydy_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/accounting/zhaiyao/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#zydy_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    
                                    zydyService.dialog.dialog('close');
                                    //$('#zydy_query_form').submit();
                                    $.messager.alert('通知', "操作成功！");
                                    zydyService.datagrid.datagrid('reload');
                                   
                                }
                            });
                        }
                	}
                }
               
                , {
                    id: 'chongzhi'
                    , text: '重置'
                    //, iconCls: 'icon-save'
                    , handler: function () {

                        if (confirm("要执行“重置”操作吗？？？")) {

                            $("#fuhe").linkbutton('disable');
                            zydyService.dialog.dialog('refresh', zydyService.actionRootUrl + zydyService.formUrl);
                        }
                    }
                }
                , {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                    }
                }
            ]
        });

    $('#zydy_query_form_search_button').click(function () {
        $('#zydy_query_form').submit();
    });
    
    $('#zydy_query_form_reset_button').click(function () {
        $('#zydy_query_form')[0].reset();
        zydyService.datagrid.datagrid('reload');
    }); 
    $('#zydy_query_form_add_button').click(function() {
    	zydyService.dialog.dialog('open').dialog('refresh',zydyService.actionRootUrl + zydyService.formUrl);
	});
});

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">

	<span style="float:center;">
		<a class="l-btn" id="zydy_query_form_search_button"><span class="l-btn-left"><span
			class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp;
		<a class="l-btn" id="zydy_query_form_reset_button"><span class="l-btn-left"><span
			class="l-btn-text">重置</span></span></a>
		&nbsp;&nbsp;
		<a class="l-btn" id="zydy_query_form_add_button"><span class="l-btn-left"><span
			class="l-btn-text">新增</span></span></a>
		&nbsp;&nbsp;

	</span>
	<table>
		<tr><td height="10px"><td></tr>
	</table>

	<span style="float:left;">
		<form:form name="zydy_query_form" id="zydy_query_form"
			method="post" action="" onsubmit="return false;">

			<table class="form_view_border" >
				<tr>
					<th class="panel-header">核算单位</th>
                <td colspan="3">
                    <form:select path="entity.hsdw.id"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 style="height: 27px;">
                        <form:option value="" >--请选择--</form:option>
                        <form:options items="${hsdwLists}" itemValue="id" itemLabel="name"/>
                    </form:select>
                </td>
           
            
               <th class="panel-header">账套编号</th>
                <td colspan="3">
                    <form:select path="entity.ztbh.id"
                                 class="form_view_input combo easyui-combobox form"
                                 data-options="required: true, editable: false"
                                 style="height: 27px;">
                        <form:option value="" >--请选择--</form:option>
                        <form:options items="${ztbhLists}" itemValue="id" itemLabel="name"/>
                    </form:select>
                </td>
				</tr>
			</table>
		</form:form>
	</span>
	<table>
		<tr><td height="30px"><td></tr>
	</table>
</div>

<table id="zydy_list_datagrid"></table>
<div id="zydy_form_dialog">Dialog Content.</div>