<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
        var pzfhService;
        	$(function () {
        		pzfhService = new $.BaseService('#pzfh_query_form', '#pzfh_list_datagrid'
        	                , '#pzfh_form_dialog', '#pzfh_edit_form'
        	        , {
        	        actionRootUrl: '<c:url value="/business/accounting/pingzhengfuhe/" />'
        	        , entityTitle: "复核凭证"
        	         , optionFormatter: function(value, row) { 
        	        	 var str = $.formatString('<a onclick="openwindows3(\'{0}\');" >查看</a>', row.xspzh)
           				+"&nbsp;&nbsp;"
           				+$.formatString('<a onclick="shuju3(\'{0}\');" >辅助核算</a>', row.xspzh);
           				return 	  str;
        	        } 
        	        ,datagridToolbar: []
        	        , datagridAddButtonId: 'pzgh_add_button'
        	        , datagridBatchDeleteButtonId: 'pzfh_batch_delete_button'
        	        , datagridColumns: [
        	                            {field: 'xspzh', title: '记账凭证号', width: 100, rowspan: 1, align: 'center',
       	                            	 styler: function(value,row,index){
           	                				if (row.wtsm  == 1){
           	                					return 'background-color:#ffee00;color:red;';
           	                					
           	                				} 
           	                			} },
        	                            /* {field: 'ywlx', title: '业务类型', width: 180, rowspan: 2, align: 'center'}, */
        	                            {field: 'zhaiyao', title: '摘要', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'jffse', title: '借方发生额', width: 100, rowspan: 2, align: 'center' , formatter: function (value, row, index) {
        	                                if (row != null) {
        	                                    return (parseFloat(value).toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
        	                              }
        	                              } },
        	                            {field: 'dffse', title: '贷方发生额', width: 100, rowspan: 1, align: 'center', formatter: function (value, row, index) {
        	                                if (row != null) {
        	                                    return (parseFloat(value).toFixed(2) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
        	                              }
        	                              }},
        	                            {field: 'fjdjs', title: '附件单据数', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'pzrq', title: '凭证日期', width: 100, rowspan: 1, align: 'center', formatter: function (value, row, index) {
        	                               	return '2017-12-06';
      	                                }},
        	                            {field: 'zd', title: '制单', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'jz222', title: '记账', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'fh', title: '复核', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'pzzt', title: '凭证状态', width: 100, rowspan: 1, align: 'center', formatter: function (value, row, index) {
        	                                	return '已复核';
        	                                }},
        	                            {field: 'hzpzh', title: '汇总凭证号', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'pzlb', title: '凭证类别', width: 100, rowspan: 1, align: 'center'},
        	                            {field: 'wtsm', title: '问题说明', width: 100, rowspan: 1, align: 'center'
										,styler: function(value,row,index){
        	                				if (value ==2 ){
        	                					return 'background-color:#ffee00;color:red;';
        	                					
        	                				}
        	                			}}
        	        ]
        	            , dialogWidth: 800
        	            , dialogHeight: 600
        	            , dialogButtons: [
        	                {
        	                	id: 'pzfh_confirm'
        	                        , text: '确认'
        	                        //, iconCls: 'icon-save'
        	                        , handler: function () {
        	                        $.messager.progress();
        	                        if($('#pzfh_edit_form').form('validate')){
        	                            if (!confirm("要执行“确认”操作吗？？？")) {
        	                                $.messager.progress('close');
        	                                return false;
        	                            }else{
        	                                kmszHistorySaveOrUpdate();
        	                            }
        	    					}else{
        	                            $.messager.progress('close');
        	    					}
        	                        function kmszHistorySaveOrUpdate(){
        	                            $("#pzfh_edit_form").form('submit', {
        	                                url: '${pageContext.request.contextPath}/business/accounting/kemushezhi/save_or_update.shtml'
        	                                , onSubmit : function() {
        										var isValid = $('#pzfh_edit_form').form('validate');
        										if (!isValid) {
        											$.messager.progress('close');
        										}
        										return isValid
        									},
        									success : function(data) {
        										$.messager.progress('close');
        										console.log(data)
        										data = jQuery.parseJSON(data);
        										kmszService.dialog.dialog('close');
        										$('#pzfh_query_form').submit();
        										 kmszService.datagrid.datagrid('reload');
        										$.messager.alert('通知',"添加成功！");
        	                                }
        	                            });
        	                        }
        	                	}
        	                }
        	               
        	                , {
        	                    id: 'chongzhi'
        	                    , text: '重置'
        	                    //, iconCls: 'icon-save'
        	                    , handler: function () {

        	                        if (confirm("要执行“重置”操作吗？？？")) {

        	                            $("#fuhe").linkbutton('disable');
        	                            pzfhService.dialog.dialog('refresh', pzfhService.actionRootUrl + pzfhService.formUrl);
        	                        }
        	                    }
        	                }
        	                , {
        	                    text: '取消',
        	                    iconCls: 'icon-cancel',
        	                    handler: function () {
        	                        $($(this).context.parentElement.parentElement).dialog('close');
        	                    }
        	                }
        	            ]
        	        });
        		
        	 });  
        	$('#pzfh_query_form_search_button').click(function () {
        		
        	 $('#pzfh_query_form').submit(); 
	    });

        	 $('#voucherfh_question_search_button').click(function () {


        		 pzfhService.dialog.dialog('open').dialog('refresh', pzfhService.actionRootUrl + pzfhService.formUrl);
	    });
        	 $('#fkpz_chongzhi_form_chongzhi_button').click(function () {
        	        $('#pzfh_query_form')[0].reset();
        	    }); 
        	 $('#company_query_form_zengjia_button').click(function () {
         		var arr = $('#pzfh_list_datagrid').datagrid('getSelections');
         		var arrpush = []; 
         		for(var i = 0 ; i <arr.length ; i++ ){
         			arrpush.push(arr[i].xspzh);
         		}
         		
         		$.ajax({ 
         		     type : "POST", //提交方式 
         		     url : "${pageContext.request.contextPath}/business/accounting/pingzhengfuhe/jzpzfh.shtml",//路径 
         		     data : { 
         		    	 jzpzids:arrpush.join(",")
         		     }, 
         		     success : function(result) {//返回数据根据结果进行相应的处理 
         		    	 $('#pzfh_list_datagrid').datagrid('reload');//刷新
         		    	 $.messager.alert('通知',"复核成功");
         		     } 
         		    }); 
         		
         	});
        	 
        	 function shangchuan(xspzh){
        	 	   alert(xspzh)
        	 }	
        	 function openwindows3 (xspzh){
        		 var win = $('#pzjz_ck_window').window({
        			    title:'记账凭证查看',
        			    width:1000,
        			    height:600,
        			 	href:'${pageContext.request.contextPath}/business/accounting/pingzhengjizhang/site_record3.shtml?entity.xspzh='+xspzh,
        			    modal:true
        			});
        	 }	
        	 function shuju3(xspzh){
        		 var win = $('#pzjz_ck_window').window({
     			    title:'"辅助核算查看',
     			    width:1000,
     			    height:600,
     			 	href:'${pageContext.request.contextPath}/business/accounting/pingzhengfuhe/shuju.shtml?entity.xspzh='+xspzh,
     			    modal:true
     			});
        	 }	
        	</script>

<div style="margin:10px 0;"></div>
<div style="margin-top: 30px;margin-left:330px">
		<span> <a class="l-btn" id="pzfh_query_form_search_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn"
			id="fkpz_chongzhi_form_chongzhi_button"><span class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">重置</span></span></a>
			&nbsp;&nbsp;
		</span> <span> <a class="l-btn" id="company_query_form_zengjia_button"><span
				class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">复核</span></span></a>
			&nbsp;&nbsp;
		</span>
			<span>
				<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">消复核</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="voucherfh_question_search_button"><span class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">问题标注</span></span></a>
				&nbsp;&nbsp;
			</span>
			<span>
				<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">取消标注</span></span></a>
				&nbsp;&nbsp;
			</span>
		<table>
			<tr>
				<td height="30px">
				<td>
			</tr>
		</table>
	</div>

<div class="text clearfix" style="text-align:center;">
		<span>
			<form:form name="pzfh_query_form" id="pzfh_query_form"
					   method="post" action="" onsubmit="return false;">
                     <div style="float:left">
				<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">凭证类别</th>
						<td><form:select path="entity.pzlb"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 >
							<form:option value="" >--请选择--</form:option>
							<form:option value="1">记账凭证</form:option>  
						</form:select></td>
						
						<th class="panel-header">复核状态</th>
						<td><form:select path="fhzt"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 id="selectId"
										 >
							<form:option value="" >--请选择--</form:option>
							<form:option value="1">已复核</form:option>  
                            <form:option value="0">未复核</form:option>  
						</form:select></td>
						</tr>
						<tr>
						<th class="panel-header">全选范围</th>
						<td><form:select path="qxfw"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 >
							<form:option value="" >--请选择--</form:option>
							<form:option value="1">当前页</form:option>  
                            <form:option value="0">全部页</form:option>  
						</form:select></td>
					</tr>
					</table>
					</div>
					 <div style="float:rigth">
					<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">凭证日期</th>
						<td><form:input path="startTime"
										class="form_view_input combo easyui-datebox"/></td>
						<th class="panel-header">至</th>
						<td><form:input path="endTime"
										class="form_view_input combo easyui-datebox"/></td>
					</tr>
					<tr>
										<th class="panel-header">凭证编号</th>
						<td><form:input path="startJzpzh"
										class="form_view_input combo easyui-validatebox"/></td>
										<th class="panel-header">至</th>
						<td><form:input path="endJzpzh"
										class="form_view_input combo easyui-validatebox"/></td>
					</tr>
				</table>
				</div>
			</form:form>
		</span>
</div>
<div id="pzjz_ck_window"></div>
<div id="pzfh_ck_wind"></div>
<table id="pzfh_list_datagrid"></table>
<div id="pzfh_form_dialog"></div>
<div id="pzfh_cheange_dialog"></div>