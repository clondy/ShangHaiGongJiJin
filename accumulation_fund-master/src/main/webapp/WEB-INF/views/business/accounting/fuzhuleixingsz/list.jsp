<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

{ "total":
<c:out value="${DataTotalCount}" />
, "rows": [
<c:forEach var="Fzlxsz" items="${FzlxszList}" varStatus="status">
    {"id": "${Fzlxsz.id}",
    "hslxbm": "${Fzlxsz.fzlxxglssjList[0].hslxbm}",
    "hslxmc": "${Fzlxsz.fzlxxglssjList[0].hslxmc}",
    "hsfzsm": "${Fzlxsz.fzlxxglssjList[0].hsfzbm.hsfzsm}",
   	"ztbh": "${Fzlxsz.fzlxxglssjList[0].ztbh}",
    "hsdw": "${Fzlxsz.fzlxxglssjList[0].hsdw.name}",
    "hsfl": "${Fzlxsz.fzlxxglssjList[0].hsfl}",
    "sjzt": "${Fzlxsz.fzlxxglssjList[0].sjzt}",
    "spzt": "${Fzlxsz.fzlxxglssjList[0].spzt}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
 ] }