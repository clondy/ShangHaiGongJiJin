<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:if test="${VoucherList[0].xspzh=='00002'}">
<div  align="center" >
	<font size="6">辅助核算</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
			<td  width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
	 		<td  width ="40%"style="text-align:content;">2017年12月06日</td>
		 	<td  width ="30%"style="text-align:right;">记字:${VoucherList[0].xspzh}</td>
	 	</tr>
	</table>
 <table  rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
		<td>科目编码</td>
	 	<td>科目名称</td>
	 	<td>辅助核算类型</td>
	 	<td>辅助核算类型名称</td>
	 	<td>核算项目编码</td>
	 	<td>核算项目名称</td>
	 	<td>借方发生额</td>
	 	<td>贷方发生额</td>
	 	<td>备注</td>
	 </tr>
	  	<tr height="20px">
		 	<td style="text-align:content;">201</td>
	    	<td style="text-align:content;">住房公积金</td>
	    	<td style="text-align:content;" >028</td>
	    	<td style="text-align:content;">201核算</td>
	    	<td style="text-align:content;">0103</td>
	    	<td style="text-align:content;">公积金支取本金</td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	   <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >021</td>
	    	<td style="text-align:content;">101核算</td>
	    	<td style="text-align:content;">05</td>
	    	<td style="text-align:content;">公积金提取专户</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	  <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >001</td>
	    	<td style="text-align:content;">银行类型</td>
	    	<td style="text-align:content;">01</td>
	    	<td style="text-align:content;">建设银行</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:content;">二元核算</td>
	  </tr>
 </table>
</div>
</c:if>

<c:if test="${VoucherList[0].xspzh=='00003'}">
<div  align="center" >
	<font size="6">辅助核算</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
			<td  width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
	 		<td  width ="40%"style="text-align:content;">2017年12月06日</td>
		 	<td  width ="30%"style="text-align:right;">记字:${VoucherList[0].xspzh}</td>
	 	</tr>
	</table>
 <table  rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
		<td>科目编码</td>
	 	<td>科目名称</td>
	 	<td>辅助核算类型</td>
	 	<td>辅助核算类型名称</td>
	 	<td>核算项目编码</td>
	 	<td>核算项目名称</td>
	 	<td>借方发生额</td>
	 	<td>贷方发生额</td>
	 	<td>备注</td>
	 </tr>
	  <tr height="20px">
		 	<td style="text-align:content;">201</td>
	    	<td style="text-align:content;">住房公积金</td>
	    	<td style="text-align:content;" >028</td>
	    	<td style="text-align:content;">201核算</td>
	    	<td style="text-align:content;">0203</td>
	    	<td style="text-align:content;">补充公积金支取本金</td>
	    	<td style="text-align:right;">500.00</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:content;">二元核算</td>
	  </tr>
	   <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >021</td>
	    	<td style="text-align:content;">101核算</td>
	    	<td style="text-align:content;">05</td>
	    	<td style="text-align:content;">公积金提取专户</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">500.00</td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	  <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >001</td>
	    	<td style="text-align:content;">银行类型</td>
	    	<td style="text-align:content;">01</td>
	    	<td style="text-align:content;">建设银行</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">500.00</td>
	    	<td style="text-align:content;">二元核算</td>
	  </tr>
 </table>
</div>
</c:if>

<c:if test="${VoucherList[0].xspzh=='00004'}">
<div  align="center" >
	<font size="6">辅助核算</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
			<td  width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
	 		<td  width ="40%"style="text-align:content;">2017年12月06日</td>
		 	<td  width ="30%"style="text-align:right;">记字:${VoucherList[0].xspzh}</td>
	 	</tr>
	</table>
 <table  rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
		<td>科目编码</td>
	 	<td>科目名称</td>
	 	<td>辅助核算类型</td>
	 	<td>辅助核算类型名称</td>
	 	<td>核算项目编码</td>
	 	<td>核算项目名称</td>
	 	<td>借方发生额</td>
	 	<td>贷方发生额</td>
	 	<td>备注</td>
	 </tr>
	  	<tr height="20px">
		 	<td style="text-align:content;">201</td>
	    	<td style="text-align:content;">住房公积金</td>
	    	<td style="text-align:content;" >028</td>
	    	<td style="text-align:content;">201核算</td>
	    	<td style="text-align:content;">0103</td>
	    	<td style="text-align:content;">公积金支取本金</td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	   <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >021</td>
	    	<td style="text-align:content;">101核算</td>
	    	<td style="text-align:content;">05</td>
	    	<td style="text-align:content;">公积金提取专户</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	  <tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >001</td>
	    	<td style="text-align:content;">银行类型</td>
	    	<td style="text-align:content;">01</td>
	    	<td style="text-align:content;">建设银行</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">1,000.00</td>
	    	<td style="text-align:content;">二元核算</td>
	  </tr>
 </table>
</div>
</c:if>

<c:if test="${VoucherList[0].zhaiyao=='缴存'}">
<div  align="center" >
	<font size="6">辅助核算</font>
	<hr style="width:300px"/>
	<hr style="width:300px"/>
	<table   style="border:0px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
		<tr height="10px">
			<td  width ="30%" style="text-align:left;">核算单位:上海市公积金管理中心</td>
	 		<td  width ="40%"style="text-align:content;">2017年12月06日</td>
		 	<td  width ="30%"style="text-align:right;">记字:${VoucherList[0].xspzh}</td>
	 	</tr>
	</table>
 <table  rules=all   style="border:1px solid #000;width:800px;text-align:center;cellspacing=0;cellpadding=0" >
	 <tr height="10px">
		<td>科目编码</td>
	 	<td>科目名称</td>
	 	<td>辅助核算类型</td>
	 	<td>辅助核算名称</td>
	 	<td>核算项目编码</td>
	 	<td>核算项目名称</td>
	 	<td>借方发生额</td>
	 	<td>贷方发生额</td>
	 	<td>备注</td>
	 </tr>
	  	<tr height="20px">
		 	<td style="text-align:content;">101</td>
	    	<td style="text-align:content;">住房公积金存款</td>
	    	<td style="text-align:content;" >021</td>
	    	<td style="text-align:content;">101核算</td>
	    	<td style="text-align:content;">01</td>
	    	<td style="text-align:content;">公积金户</td>
	    	<td style="text-align:right;">2,000.00</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	  <tr height="20px">
		 	<td style="text-align:content;">201</td>
	    	<td style="text-align:content;">住房公积金</td>
	    	<td style="text-align:content;" >028</td>
	    	<td style="text-align:content;">201核算</td>
	    	<td style="text-align:content;">0101</td>
	    	<td style="text-align:content;">公积金-净上划</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">2,000.00</td>
	    	<td style="text-align:content;">一元核算</td>
	  </tr>
	   <tr height="20px">
		 	<td style="text-align:content;">201</td>
	    	<td style="text-align:content;">住房公积金</td>
	    	<td style="text-align:content;" >002</td>
	    	<td style="text-align:content;">管理部</td>
	    	<td style="text-align:content;">226</td>
	    	<td style="text-align:content;">奉贤管理部</td>
	    	<td style="text-align:right;"></td>
	    	<td style="text-align:right;">2,000.00</td>
	    	<td style="text-align:content;">二元核算</td>
	  </tr>
 </table>
</div>
</c:if>
