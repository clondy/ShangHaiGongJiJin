<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
 
 {
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="zflz" items="${ZflzList}" varStatus="status">
    {"id": "${zflz.id}",
    "kmmc": "${zflz.kmmc.kmmc}",
    "riqi": "${zflz.riqi}",
    "zhaiyao": "${zflz.zhaiyao}",
    "jffse": "${zflz.jffse}",
    "dffse": "${zflz.dffse}",
    "yefx": "${zflz.yefx.name}",
    "yue": "${zflz.yue}",
    "kjnd": "${zflz.kjnd.name}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}