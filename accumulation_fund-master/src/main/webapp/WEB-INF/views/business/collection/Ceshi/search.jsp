<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var aService;
    $(function () {
        aService = new $.BaseService('#company_account_fund_query_form', '#company_account_fund_list_datagrid'
            , '#company_account_fund_form_dialog', '#company_account_fund_edit_form'
            , {
                actionRootUrl: '<c:url value="/business/collection/Ceshi/" />'
                , entityTitle: "单位补充公积金账户"
                , optionFormatter: function(value, rec) {

                    return '<a href="#" title="修改" onClick="aService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + aService.actionRootUrl + aService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="删除" onClick="aService.datagrid.datagrid(\'checkRow\', aService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + aService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="单位绑定（办理后续业务）" onClick="companyBinding(' + rec.id + ')" iconCls="icon-tip" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="单位解绑（停止办理后续业务）" onClick="companyUnbinding()" iconCls="icon-tip" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="补充公积金账户设立" onClick="companyMiniAdding(' + rec.id + ')" iconCls="icon-mini-add" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="单位账户注销" onClick="companyUndoing(' + rec.id + ')" iconCls="icon-undo" class="easyui-linkbutton" data-options="plain: true"></a>'
                }
                , datagridAddButtonId: 'company_add_button'
//                 , listUrl:'abc.shtml'
                , datagridBatchDeleteButtonId: 'company_batch_delete_button'
                , datagridColumns: [
                    {field: 'name', title: '补充住房公积金账号', width: 150, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 700
                , dialogHeight: 500
                , dialogButtons: [
                    {
                        id: 'company_account_fund_confirm'
                        , text: '确认'
                        , handler: function () {
                        $.messager.progress();

                        if($('#company_account_fund_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                    companyHistorySaveOrUpdate();
                            }
                        }else{
                            $.messager.progress('close');
                        }
                        function companyHistorySaveOrUpdate(){
                            $("#company_account_fund_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/collection/Ceshi/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#company_account_fund_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    data = jQuery.parseJSON(data);
                                    if (data) {
                                         aService.dialog.dialog('close');
                                         $('#company_account_fund_query_form').submit();
                                         $.messager.alert('通知', "添加成功！");
                                    }
                                }
                            });
                        }}
                    }
                    , {
                        id: 'company_account_fund_audit'
                        , text: '审核'
                        , disabled: true
                        //, iconCls: 'icon-save'
                        , handler: function () {
                            if($('#company_account_fund_edit_form').form('validate')) {
                                if (!confirm("要执行“审核”操作吗？？？")) {
                                    $.messager.progress('close');
                                    return false;
                                } else {
                                    company_account_fund_audit();
                                }
                            }else{
                                $.messager.progress('close');
                            }

							function company_account_fund_audit() {

                                $("#company_account_fund_edit_form").form('submit', {
                                    url: '${pageContext.request.contextPath}/business/collection/company_account_fund/company_account_fund_audit.shtml'
                                    , onSubmit: function () {
                                    }
                                    , success: function (data) {
                                        $("#company_account_fund_audit").linkbutton('disable');
                                        //判断审核是否成功
                                        data = jQuery.parseJSON('' + data);
                                        alert(data.result+"///");
                                        if (data.result) {
                                            $.messager.alert('通知', "审核成功！");
                                            $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                if (r){
                                                    window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                }
                                            });
                                            $.messager.progress('close');
                                            //  aService.dialog.dialog('close');
                                        } else {
                                            $.messager.alert('通知', "审核失败！");
                                        }
                                        $.messager.progress('close');

                                    }
                                });
                            }
                        }
                    }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                aService.dialog.dialog('refresh', aService.actionRootUrl + aService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });

        $('#company_query_form_search_button').click(function () {
            $('#company_account_fund_query_form').submit();
        });
    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="company_account_fund_query_form" id="company_account_fund_query_form"
                       method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"
                       bordercolorlight="#45b97c" border="1px" cellpadding="0"
                       cellspacing="0" style="">

<!-- 					<tr> -->
<!-- 						 <th class="panel-header">缴存网点</th> -->
<%--                         <td><form:select path="entity.company.paymentSite" --%>
<%-- 										 style="width: 184px;height: 27px;" --%>
<%-- 										 class="form_view_input combo easyui-combobox" --%>
<%-- 										 data-options="required: true, editable: false" --%>
<%-- 										 validType="complexValid['^--请选择--$', '', '', '', 1]"> --%>
<%-- 							<form:option value="" >--请选择--</form:option> --%>
<%-- 							<form:options items="${paymentSiteList}" itemValue="id" itemLabel="name"/> --%>
<%-- 						</form:select> --%>
<!--                         </td> -->
<!-- 					    <th class="panel-header" style="width: auto">补充住房公积金账号</th> -->
<%-- 						<td><form:input path="entity.companyAccount" --%>
<%-- 										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td> --%>
<!-- 						<th class="panel-header">单位名称</th> -->
<%-- 						<td><form:input path="entity.unitName" --%>
<%-- 										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td> --%>
<!--                     </tr> -->
<!--                     <tr> -->
<!--                         <th class="panel-header">开户日期</th> -->
<%--                         <td><form:input path="startOpenDate" --%>
<%-- 										style="width: 184px;height: 27px;" --%>
<%-- 										data-options="required: true" --%>
<%-- 										class="Wdate" --%>
<%-- 										value="${startDate}" --%>
<%-- 										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})" --%>
<%-- 										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td> --%>
<!--                     	<td><span>至</span></td> -->
<%-- 						<td><form:input path="endOpenDate" --%>
<%-- 										style="width: 184px;height: 27px;" --%>
<%-- 										data-options="required: true" --%>
<%-- 										class="Wdate" --%>
<%-- 										value="${endDate}" --%>
<%-- 										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})" --%>
<%-- 										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td> --%>

<!-- 					</tr> -->
				</table>
            </form:form>
		</span>
    <span style="float:right;">
			<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
                    class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
	<table>
		<tr><td height="30px"><td></tr>
		<tr><td height="30px"><td></tr>
	</table>
</div>

<table id="company_account_fund_list_datagrid"></table>
<div id="company_account_fund_form_dialog">Dialog Content.</div>
<div id="company_account_fund_confirm_form_dialog">

</div>
