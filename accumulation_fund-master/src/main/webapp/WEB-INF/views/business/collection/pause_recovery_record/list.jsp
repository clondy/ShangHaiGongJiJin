<%@ page import="com.capinfo.framework.model.system.User" %>

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="pauseRecoveryRecord" items="${PauseRecoveryRecordList}" varStatus="status">
    {"id": "${pauseRecoveryRecord.id}",
    "accountType": "${pauseRecoveryRecord.personAccountFund.accountType}",
    "reasonRecordName": "${pauseRecoveryRecord.reason.name}",
    "companyAccountFund": "${pauseRecoveryRecord.companyAccountFund.companyAccount}",
    "companyAccountFundUnitName": "${pauseRecoveryRecord.companyAccountFund.unitName}",
    "personalAccount": "${pauseRecoveryRecord.personAccountFund.person.personalAccount}",
    "personName": "${pauseRecoveryRecord.personAccountFund.person.name}",
    "personGbidCardType": "${pauseRecoveryRecord.personAccountFund.person.idCardType.name}",
    "personIdCardNumber": "${pauseRecoveryRecord.personAccountFund.person.idCardNumber}",
    "wage": "${pauseRecoveryRecord.wage}",
    "monthPayment": "${pauseRecoveryRecord.monthPayment}",
    "dateTime": "${pauseRecoveryRecord.createTime}",
    "operatorName": "${pauseRecoveryRecord.systemUser.Name}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}