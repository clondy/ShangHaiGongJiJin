<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="personAccountFund" items="${PersonAccountFundList}" varStatus="status">
    {"id":"${personAccountFund.id}",
    "personalAccount": "${personAccountFund.person.personalAccount}",
    "name": "${personAccountFund.person.name}",
    "gbidCardType": "${personAccountFund.person.gbidCardType}",
    "idCardNumber": "${personAccountFund.person.idCardNumber}",
    "wageIncome": "${personAccountFund.person.wageIncome}",
    "openedDate":"${personAccountFund.openedDate}",
    "monthPay": "${personAccountFund.monthPay}",
    "companyPaymentRatio": "${personAccountFund.person.companyPaymentRatio}",
    "personPaymentRatio": "${personAccountFund.person.personPaymentRatio}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}