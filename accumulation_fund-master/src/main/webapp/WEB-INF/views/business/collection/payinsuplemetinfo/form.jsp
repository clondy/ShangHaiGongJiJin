<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">


    //拼接字符拼接字符串
    function showJson() {
        var idCardNum = $("#gbLegalPersonIdCardNumber").val()
        var legalPersonName = $("#gbLegalPersonName").val()
        var legalPersonIdCardTypeId = $("#legalPersonIdCardTypeId").val()
        var legalPersonMobilePhone = $("#legalPersonMobilePhone").val()

        var str = {
            gbLegalPersonIdCardNumber: idCardNum,
            gbLegalPersonName: legalPersonName,
            legalPersonIdCardTypeId: legalPersonIdCardTypeId,
            legalPersonMobilePhone: legalPersonMobilePhone
        };
        return str;
    }
    //定义查询返回的json数据
    var payininfoCallBackJson;
    //根据信用代码查询信息

    $("#companyAccount").blur(function () {
          var account=$('#companyAccount').val();
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/showuname.shtml",
            type: "post",
            data: {companyAccot: account},
            datatype: "json",
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                console.log(obj)
                $('#unitName').val(obj.uname);

            }
        })
    });


    $('#payininfo_edit_form').change(function() {
        $('#payininfo_audit').linkbutton('enable');
    })

</script>

<div class="div_center" style="margin-top: 90px">
    <div class="text clearfix row" style="text-align:center;height: 80px;float: left">
		<span style="float:left;">
			<form:form name="payinsuplemetinfo_add_form" id="payinsuplemetinfo_add_form"
                       method="post" action="" onsubmit="return false;">

                <table class="form_view_border" bordercolordark="#FFFFFF"
                       bordercolorlight="#45b97c" border="1px" cellpadding="0"
                       cellspacing="0" style="">
                    <tr>



                        <th class="panel-header">单位账号</th>
                        <td><form:input path="companyAccount" id="companyAccount"
                                        class="form_view_input combo easyui-validatebox" required="true"  style="width:184px;height: 27px;"/></td>
                        <%--<form:input path="entity.id" id="payinfo_id" value="${payinfo_id}" type="hidden"--%>
                                    <%--class="form_view_input combo easyui-validatebox"   style="width:184px;height: 27px;"/>--%>
                        <th class="panel-header">单位名称</th>
                        <td><form:input path="unitName" id="unitName"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>

                    </tr>
                    <tr>
                        <th class="panel-header">缴存网点</th>

                        <td><form:select path="entity.siteId"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${siteList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>
                        <th class="panel-header">补缴选项</th>
                        <td><form:select path="entity.paymentOptions" id="options"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                                >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${bjItemList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>

                    </tr>
                    <tr>
                    <th class="panel-header">补缴起始年月</th>
                    <td>
                        <form:input path="entity.startTime" id="startTime"
                                    style="width: 184px;height: 27px;"
                                    data-options="required: true"
                                    class="Wdate"
                                    onfocus="WdatePicker({dateFmt:'yyyyMM',readOnly:true,minDate:'%y-%M-%D'})"
                                    validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
                    </td>

                    <th class="panel-header">补缴中止年月</th>
                    <td>
                        <form:input path="entity.endTime" id="endTime"
                                    style="width: 184px;height: 27px;"
                                    data-options="required: true"
                                    class="Wdate"
                                    onfocus="WdatePicker({dateFmt:'yyyyMM',readOnly:true,minDate:'%y-%M-%D'})"
                                    validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>

                    </td>

                </tr>
                    <tr>
                        <th class="panel-header">补缴原因</th>
                        <td><form:select path="entity.paymentReason" id="paymentReason"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                                >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${bjReasonList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>



                    </tr>
                </table>
            </form:form>
		</span>

        <table>
            <tr>
                <td height="30px">
                <td>
            </tr>
        </table>


    </div>

    <br/>
    <%--<table id="payininfo_unchecklist_datagrid"  STYLE="width:500px;height: 500px" class="easyui-datagrid" >--%>
        <%--<thead>--%>
        <%--<tr>--%>
            <%--<th data-options="field:'id'">流水号</th>--%>
            <%--<th data-options="field:'payMonthly'">缴存年月</th>--%>
            <%--<th data-options="field:'verificationOptions'">核定选项</th>--%>
            <%--<th data-options="field:'id'">流水号</th>--%>
            <%--<th data-options="field:'lastRemiAmount'">上月汇缴金额</th>--%>
            <%--<th data-options="field:'lastRemitNumber'">上月汇缴人数</th>--%>
            <%--<th data-options="field:'increaseRemitAmount'">本月增加金额</th>--%>
            <%--<th data-options="field:'increaseRemitNumber'">本月增加人数</th>--%>
            <%--<th data-options="field:'reduceRemitAmount'">本月减少金额</th>--%>
            <%--<th data-options="field:'reduceRemitNumber'">本月减少人数</th>--%>
            <%--<th data-options="field:'remitAmount'">本月汇缴金额</th>--%>
            <%--<th data-options="field:'remitNumber'">本月汇缴人数</th>--%>

        <%--</tr>--%>
        <%--</thead>--%>
    <%--</table>--%>
</div>
