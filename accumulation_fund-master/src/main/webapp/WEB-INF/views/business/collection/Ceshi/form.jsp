<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<div class="div_center" style="margin-top: 90px">
    <form:form name="company_account_fund_edit_form" id="company_account_fund_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.id" id="company_entity_id"/>

        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
            
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.name" id="name"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
            </tr>
        </table>
    </form:form>
</div>
