<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<div class="div_center">
    <form:form name="company_review_form" id="company_review_form"
               method="post" action="" data-dojo-type="dijit/form/Form"
               onsubmit="return false;">

        <table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
               bordercolorlight="#45b97c" border="1px" cellpadding="0"
               cellspacing="0">
            <tr height="30">
                <th class="panel-header">单位账户</th>
                <td class="page_input" width="60%" colspan="3">
                    <input type="text" id="companyAccountFund.companyAccount" name="companyAccountFund.companyAccount"
                           class="form_view_input combo easyui-validatebox"
                           data-options="required: true" style="height: 27px;">
                </td>
            </tr>
        </table>
    </form:form>
</div>