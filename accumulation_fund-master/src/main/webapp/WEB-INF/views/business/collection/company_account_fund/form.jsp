<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    var validateCompany_redit_code = {
    getValidateUrl: function() {
    return '<c:url value="/business/collection/company_account_fund/is_exist.shtml" />';
    }
    , getValidateParameter: function(value) {
    var value = jQuery.trim(value);
    if( value ) {
    return {'companyCode': value};
    }
    return {}
    }
    , validator: function(data) {
    //因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
    data = jQuery.parseJSON(data);
    return !data.result;
    }
    }

    //拼接字符拼接字符串
    function showJson() {
        var idCardNum = $("#gbLegalPersonIdCardNumber").val()
        var legalPersonName = $("#gbLegalPersonName").val()
        var legalPersonIdCardTypeId = $("#legalPersonIdCardTypeId").val()
        var legalPersonMobilePhone = $("#legalPersonMobilePhone").val()

        var str = {
            gbLegalPersonIdCardNumber: idCardNum,
            gbLegalPersonName: legalPersonName,
            legalPersonIdCardTypeId: legalPersonIdCardTypeId,
            legalPersonMobilePhone: legalPersonMobilePhone
        };
        return str;
    }
    //定义查询返回的json数据
    var companyCallBackJson;
    //根据单位账号查询信息

    $("#company_account_fund_search_Info").click(function () {
        var code = $("#gb_company_account").val();

        if (!(code.length == 12)) {
            $.messager.alert("提示", "单位账号不正确！")
            return false;
        }
        //查询本地
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/company_account_fund/is_exist.shtml",
            type: "post",
            data: {'entity.company.gbCompanyAccount': code},
            datatype: "json",
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                console.log(obj)

                if (!obj.result) {
                    if(obj.flag == "one"){
                        $.messager.alert("通知", obj.errMsg);
                    }
                    if(obj.flag == "two"){
                        $.messager.alert("通知", obj.errMsg);
                    }
                    if(obj.flag == "three"){
                        $.messager.alert("通知", obj.errMsg);
                    }
                    /*$('#flag').val(data.result);
                     $.messager.alert("通知", "该企业已开户！")*/
                    return false;
                } else {

                    $("#company_gbCompanyName").val(obj.gbCompanyName);
                    $("#company_account_fund_companyAccount").val(obj.gbCompanyAccount);
                    $("#gbCompanyAddress").val(obj.companyAddress);
                    $("#gbCompanyPostCode").val(obj.gbCompanyPostCode);
                    $("#company_natureAccountId").val(obj.natureAccountId);
                    $("#startPaymentDate").val(obj.startPaymentDate);
                    $("#contactName").val(obj.contactName);
                    $("#contactIdCardTypeId").val(obj.contactIdCardTypeId);
                    $("#contactIdCardNumber").val(obj.contactIdCardNumber);
                    $("#contactMobilePhone").val(obj.contactMobilePhone);
                    $("#companyId").val(obj.companyId);

                }
            }
        })
    });

    $('#company_account_fund_edit_form').change(function() {
        $('#company_audit').linkbutton('enable');
    })

</script>

<div class="div_center" >
    <form:form name="company_account_fund_edit_form" id="company_account_fund_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.id" id="company_account_fund_entity_id"/>
        <form:hidden path="entity.companyAccount" id="company_account_fund_companyAccount"/>
        <input type="hidden" name="companyId" id="companyId"/>



        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
                <th class="panel-header">单位账号</th>
                <td><form:input path="entity.company.gbCompanyAccount" id="gb_company_account"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
                <td style="text-align:left;" colspan="2">
                    <a href="#" class="easyui-linkbutton" id="company_account_fund_search_Info">查询</a></td>
            </tr>
            <tr>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.company.gbCompanyName" id="company_gbCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
                <th class="panel-header">单位联系地址</th>
                <td><form:input path="entity.company.gbCompanyAddress" id="gbCompanyAddress"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">邮政编码</th>
                <td><form:input path="entity.company.gbCompanyPostCode" id="gbCompanyPostCode"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
                <th class="panel-header">账户性质</th>
                <td><form:select path="entity.company.natureAccountId" id="company_natureAccountId"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$', '', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${natureAccountIdList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
            </tr>

            <tr>
                <th style="text-align:center;" class="panel-header" colspan="4">缴存信息</th>
            </tr>
            <tr>
                <th class="panel-header">缴存率标志</th>
                <td><form:select path="entity.depositRateMarkId" id="depositRateMarkId"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${depositRateMarkIdList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
                <th class="panel-header">启缴年月</th>
                <td><form:input path="entity.startPaymentDate" id="startPaymentDate"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                value=""
                                class="form_view_input combo easyui-validatebox"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">单位缴存比例(%)</th>
                <td><form:select path="entity.companyPaymentRatio" id="companyPaymentRatio"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:option value="5" >5</form:option>
                    <form:options items="${companyPaymentRatioList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>

                <th class="panel-header">个人缴存比例(%)</th>
                <td><form:select path="entity.personPaymentRatio" id="personPaymentRatio"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:option value="5" >5</form:option>
                    <form:options items="${personPaymentRatioList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
            </tr>

            <tr>
                <th style="text-align:center;" class="panel-header" colspan="4">联系人信息</th>
            </tr>
            <tr>
                <th class="panel-header">联系人姓名</th>
                <td><form:input path="entity.company.contactName" id="contactName"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{1,25}$','姓名长度不能超过25字符！']"></form:input></td>
                <th class="panel-header">联系人证件类型</th>
                <td><form:select path="entity.company.contactIdCardTypeId" id="contactIdCardTypeId"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$', '', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${contactIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
            </tr>
            <tr>
                <th class="panel-header">联系人证件号码</th>
                <td><form:input path="entity.company.contactIdCardNumber" id="contactIdCardNumber"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"></form:input></td>
                <th class="panel-header">联系人移动电话</th>
                <td><form:input path="entity.company.contactMobilePhone" id="contactMobilePhone"
                                style="width:182px;height: 27px;"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^1[3,4,5,7,8]\\\d{9}$','请输入正确手机号！']"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">备注</th>
                <td rowspan="2" colspan="3"><form:input path="entity.notes" id="notes"
                                                        style="width:99.7%;height: 40px"
                                                        class="form_view_input combo easyui-validatebox"
                                                        data-options="multiline:true"></form:input>
            </tr>
        </table>
    </form:form>
</div>
