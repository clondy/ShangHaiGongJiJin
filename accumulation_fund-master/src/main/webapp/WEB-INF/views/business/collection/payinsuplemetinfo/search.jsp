<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var payinsuplemetinfoService;
    $(function () {

        payinsuplemetinfoService = new $.BaseService('#payinsuplemetinfo_query_form', '#payinsuplemetinfo_list_datagrid'
            , '#payinsuplemetinfo_form_dialog', '#payinsuplemetinfo_edit_form'
            , {
                formUrl:'form.shtml'
                ,actionRootUrl: '<c:url value="/business/collection/payinsuplemetinfo/" />'
                , entityTitle: "单位补缴核定"

                ,datagridToolbar: [{
                        id: this.datagridSearchButtonId
                        , text: '查询'
                        , iconCls: 'icon-search'
                        , handler: function() {
                            $('#payinsuplemetinfo_query_form').submit();
                        }
                    },'-',{
                        id: this.datagridAddButtonId
                        , text: '新增清册'
                        , iconCls: 'icon-add'
                        , handler: function() {

                            payinsuplemetinfoService.dialog.dialog('open').dialog('refresh', payinsuplemetinfoService.actionRootUrl + payinsuplemetinfoService.formUrl);
                        }
                    },'-',{
                        id: this.datagridBatchDeleteButtonId
                        , text: '批量删除'
                        , iconCls: 'icon-remove'
                        , handler: function() {
                            var checkedData = {};
                            var items = payinsuplemetinfoService.datagrid.datagrid('getChecked');
                            for(var i = 0; i < items.length; i++) {

                                checkedData["ids[" + i + "]"] = items[i].id;
                            }

                            var dataCount = Object.keys(checkedData).length;
                            var prefix = ( 1 < dataCount )? "批量": "";
                            if( dataCount ) {

                                if( confirm("确定要删除吗？？？") ) {

                                    $.ajax({
                                        url: payinsuplemetinfoService.actionRootUrl + payinsuplemetinfoService.deleteUrl
                                        , type : "post"
                                        , dataType : 'json'
                                        , data : checkedData
                                        , success : function(data, response, status) {

                                            if( jQuery.parseJSON('' + data) ) {

                                                $.messager.alert('通知', prefix + "删除成功。");
                                                payinsuplemetinfoService.datagrid.datagrid('clearChecked');
                                                payinsuplemetinfoService.datagrid.datagrid('reload');
                                            } else {

                                                $.messager.alert('通知', prefix + "删除失败。");
                                            }
                                        }
                                    });
                                } else {

                                    payinsuplemetinfoService.datagrid.datagrid('clearChecked');
                                }
                            } else {

                                $.messager.alert('通知', "请选中数据后再执行删除。");
                            }
                        }

                    }

                    ]

                    ,    datagridColumns: [
                    {field: 'serialNumber', title: '流水号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFund.companyAccount', title: '单位帐号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFund.unitName', title: '单位名称', width: 75, rowspan: 2, align: 'center'},
                    {field: 'paymentOptions', title: '补缴选项', width: 75, rowspan: 2, align: 'center'},
                    {field: 'paymentReason', title: '补缴原因', width: 75, rowspan: 2, align: 'center'},
                    {field: 'receivedDate', title: '核定日期', width: 75, rowspan: 2, align: 'center'},
                        {field: 'approvalDate', title: '到账日期', width: 75, rowspan: 2, align: 'center'},
                        {field: 'paymentAmount', title: '补缴总额 ', width: 75, rowspan: 2, align: 'center'}

                ]
                , dialogWidth: 600
                , dialogHeight: 400
                , dialogButtons: [
                    {
                            id: 'payinsuplemetinfo_'
                            , text: '创建清册'
                            //, iconCls: 'icon-save'


                        ,handler: function () {
                        if($('#payinsuplemetinfo_add_form').form('validate')){
                                if (confirm("要执行确认操作吗？？？")) {
                                      console.log($('#verificationOptions').val())
                                   // $("#").linkbutton('disable');
                                    $.ajax({
                                            url:  "${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/payinsuplemetinfo_build.shtml",
                                            data:  {
                                        'entity.paymentOptions':$("#options").combobox("getValue"),
                                                'companyAccot':$('#companyAccount').val(),
                                                'entity.startTime':$("#startTime").val(),
                                                'entity.endTime':$("#endTime").val(),
                                                'entity.paymentReason':$("#paymentReason").combobox("getValue")

                                    },
                                    datatype: "json",
                                            success: function (data) {
                                                var obj = jQuery.parseJSON(data);
                                                console.log(obj.paysupleId);
                                                if (obj.result==1 ) {

                                                    $.messager.alert('通知', "确认成功！");
                                                    $('#paySup').val(obj.paysupleId);
                                                    $('#payinsuplemetinfo_form_dialog').dialog('close');
                                                    $('#payinsuplemetinfo_confirm_form_dialog').dialog({
                                                        title: '添加补缴信息'
                                                        , href: "${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/confirm.shtml"
                                                        , width: 980
                                                        , height: 420
                                                        , closed: true
                                                        , cache: false
                                                        , resizable: true
                                                        , collapsible: true
                                                        , maximizable: true
                                                        , buttons: [{
                                                            text: '取消',
                                                            iconCls: 'icon-cancel',
                                                            handler: function () {
                                                                $($(this).context.parentElement.parentElement).dialog('close');
                                                                $.messager.progress('close');
                                                                return false;
                                                            }
                                                        }]
                                                        , modal: true
                                                    }).dialog('open');

                                                } else {
                                                    $.messager.alert('通知', "单位帐号无效！");
                                                    $.messager.progress('close');
                                                }
                                            }
                                    })

                                }
                            }
                        }
                    }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                //$("#").linkbutton('disable');
                                payinsuplemetinfoService.dialog.dialog('refresh', payinsuplemetinfoService.actionRootUrl + payininfoService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');

                        }
                    }
                ]
            });


    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;height: 80px">
		<span style="float:left;">
			<form:form name="payinsuplemetinfo_query_form" id="payinsuplemetinfo_query_form"
                       method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"
                       bordercolorlight="#45b97c" border="1px" cellpadding="0"
                       cellspacing="0" style="">
					<tr>
                        <th class="panel-header">缴存网点</th>
                        <td><form:select path="entity.siteId"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${siteList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>

                        <th class="panel-header">单位账号</th>
                        <td><form:input path="entity.companyAccountFund.companyAccount"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>
                        <th class="panel-header">单位名称</th>
                        <td><form:input path="entity.companyAccountFund.unitName"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>


                    </tr>
                    <tr>
                        <th class="panel-header">补缴选项</th>

                        <td><form:select path="entity.paymentOptions"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                                         >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${bjItemList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>
                        <%--<th class="panel-header">缴存年月</th>--%>
                        <%--<td><form:input path="startPaymentDate" id="startPaymentDate"--%>
                                        <%--style="width: 120px;height: 27px;"--%>
                                        <%--data-options="required: true"--%>
                                        <%--class="Wdate"--%>
                                        <%--onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"--%>
                                        <%--validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>至--%>
                            <%--<form:input path="endPaymentDate" id="endPaymentDate"--%>
                                        <%--style="width: 120px;height: 27px;"--%>
                                        <%--data-options="required: true"--%>
                                        <%--class="Wdate"--%>
                                        <%--onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"--%>
                                        <%--validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>--%>
                        <%--</td>--%>
                        <th class="panel-header">核定年月</th>
                        <td><form:input path="startReceivedDate"
                                        data-options="required: true"
                                        style="width:120px;height: 27px;" id="startReceivedDate"
                                        class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input>至
                            <form:input path="endReceivedDate" id="endReceivedDate"
                                        style="width: 120px;height: 27px;"
                                        data-options="required: true"
                                        class="Wdate"
                                        onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"
                                        validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
                        </td>

                    </tr>
				</table>
            </form:form>
		</span>

    <table>
        <tr>
            <td height="30px">
            <td>
        </tr>
    </table>
</div>

<table id="payinsuplemetinfo_list_datagrid"></table>
<input id="paySup" type="hidden"/>
<div id="payinsuplemetinfo_form_dialog">Dialog Content.</div>
<div id="payinsuplemetinfo_confirm_form_dialog">

</div>
