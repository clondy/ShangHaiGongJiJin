<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var companyHistoryService;
    $(function () {

        companyHisetoryService = new $.BaseService( '#company_info_query_form','#company_info_list_datagrid'
            , '#company_info_form_dialog', '#company_info_edit_form'
            ,{
                // url: '${pageContext.request.contextPath}/business/collection/company_history/history/site_list.shtml'
                formUrl:'form.shtml'
                ,listUrl:'company_info_list.shtml?companyHistoryId=${command.entity.companyHistoryId}'
                ,actionRootUrl: '<c:url value="/business/collection/company_history/" />'
                , entityTitle: "单位信息修改记录"
                , datagridColumns: [
                    {field: 'gbCompanyAccount', title: '单位账号', width: 180, rowspan: 1, align: 'center'},
                    {field: 'gbCompanyName', title: '单位名称', width: 100, rowspan: 1, align: 'center'},
                    {field: 'confirmerId', title: '变更操作员', width: 100, rowspan: 1, align: 'center'},
                    {field: 'confirmationTime', title: '变更时间', width: 200, rowspan: 1, align: 'center'}

                ]
                ,datagridToolbar:[
                    {
                    id: this.datagridAddButtonId
                    , text: '基本信息更改'
                    , iconCls: 'icon-edit'
                    , handler:function(){
                        $('#basic_info_form_dialog').dialog({
                            title: '基本信息更改'
                            ,modal:true
                            , href: "${pageContext.request.contextPath}/business/collection/company/basic_company_info.shtml?companyHistoryId=${command.entity.companyHistoryId}"
                            , width: 700
                            , height: 700
                            , closed: false
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [
                                {id: 'basic_info_confirm'
                                , text: '确认'
                                //, iconCls: 'icon-save'
                                , handler: function () {
                                    //$.messager.progress();
                                        $("#company_basic_info_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company_history/save_basic_info_change.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#company_basic_info_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }
                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {
                                                    //历史ID 赋值
                                                    $('#company_history_id').val(data["entity.companyHistoryId"]);

                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });
                                                    //审核按钮可用
                                                    $('#basic_info_audit').linkbutton('enable');

                                               } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        });

                                  }
                                }
                                , {
                                    id: 'basic_info_audit'
                                    , text: '审核'
                                    , disabled: true
                                    //, iconCls: 'icon-save'
                                    , handler: function () {
                                        $("#company_basic_info_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company/save_company_basic_info.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#company_basic_info_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }

                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {
                                                    //ID 赋值
                                                    $('#company_id').val(data["entity.id"]);

                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });

                                                } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        })
                                    }
                                }
                                , {
                                    id: 'basic_info_reset'
                                    , text: '重置'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {

                                        if (confirm("要执行“重置”操作吗？？？")) {

                                            $("#basic_info_audit").linkbutton('disable');
                                            $('#company_basic_info_form').form("clear");
                                            }
                                    }
                                }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                    }
                                }




                            ]
                        });

                    }
                },
                    {
                        id: this.datagridAddButtonId
                        , text: '关键信息更改'
                        , iconCls: 'icon-edit'
                        , handler:function(){


                        $('#major_info_form_dialog').dialog({
                            title: '关键信息更改'
                            , href: "${pageContext.request.contextPath}/business/collection/company/major_company_info.shtml?companyHistoryId=${command.entity.companyHistoryId}"
                            , width: 700
                            , height: 400
                            , closed: false
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [
                                {id: 'major_info_confirm'
                                    , text: '确认'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {
                                    //$.messager.progress();
                                    $("#company_major_info_form").form('submit', {
                                        url: '${pageContext.request.contextPath}/business/collection/company_history/history/save_change_site.shtml'
                                        , onSubmit: function () {
                                            var isValid = $('#company_major_info_form').form('validate');
                                            if (!isValid) {
                                                $.messager.progress('close');
                                            }
                                            return isValid
                                        }
                                        , success: function (data) {
                                            $.messager.progress('close');
                                            console.log(data)
                                            data = jQuery.parseJSON(data);
                                            if (data.result ) {
                                                //历史ID 赋值
                                                $('#company_history_id').val(data["entity.companyHistoryId"]);

                                                //    $.messager.alert('通知', "添加成功！");
                                                $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                    if (r){
                                                        window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                    }
                                                });
                                                //审核按钮可用
                                                $('#major_info_audit').linkbutton('enable');

                                            } else {
                                                $.messager.alert('通知', "添加失败！");
                                            }
                                        }
                                    });

                                }
                                }
                                , {
                                    id: 'major_info_audit'
                                    , text: '审核'
                                    , disabled: true
                                    //, iconCls: 'icon-save'
                                    , handler: function () {
                                        $("#company_major_info_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company/save_change_site_company.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#company_major_info_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }

                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {
                                                    //ID 赋值
                                                    $('#company_id').val(data["entity.id"]);

                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });

                                                } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        })
                                    }
                                }
                                , {
                                    id: 'major_info_reset'
                                    , text: '重置'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {

                                        if (confirm("要执行“重置”操作吗？？？")) {

                                            $("#major_info_audit").linkbutton('disable');
                                            $('#company_major_info_form').form("clear");
                                        }
                                    }
                                }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                    }
                                }




                            ]
                        });
                    }
                    }

                ]
                , dialogWidth: 1000
                , dialogHeight: 800

            });

        $('#company_query_form_search_button').click(function () {
            $('#company_site_query_form').submit();
            href: "${pageContext.request.contextPath}/business/collection/company/site_record.shtml?entity.gbCompanyAccount="+companyAccount
        });

    });

</script>


<table id="company_info_list_datagrid"></table>
<div id="basic_info_form_dialog"></div>
<div id="major_info_form_dialog"></div>
