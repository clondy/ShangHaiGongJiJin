<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript">


    //拼接字符拼接字符串
    function showJson() {
        var idCardNum = $("#gbLegalPersonIdCardNumber").val()
        var legalPersonName = $("#gbLegalPersonName").val()
        var legalPersonIdCardTypeId = $("#legalPersonIdCardTypeId").val()
        var legalPersonMobilePhone = $("#legalPersonMobilePhone").val()

        var str = {
            gbLegalPersonIdCardNumber: idCardNum,
            gbLegalPersonName: legalPersonName,
            legalPersonIdCardTypeId: legalPersonIdCardTypeId,
            legalPersonMobilePhone: legalPersonMobilePhone
        };
        return str;
    }
    //定义查询返回的json数据
    var payininfoCallBackJson;
    //根据信用代码查询信息

    $("#personAccount").blur(function () {
        var account=$('#personAccount').val();
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/showpersonname.shtml",
            type: "post",
            data: {personAccount: account},
            datatype: "json",
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                console.log(obj)
                $('#personName').val(obj.uname);

            }
        })
    });


    $('#payininfo_edit_form').change(function() {
        $('#payininfo_audit').linkbutton('enable');
    })

</script>
<div class="div_center">
    <form:form name="company_review_form" id="company_review_form"
               method="post" action="" data-dojo-type="dijit/form/Form"
               onsubmit="return false;">

        <table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
               bordercolorlight="#45b97c" border="1px" cellpadding="0"
               cellspacing="0">
            <tr height="30">
                <th class="panel-header">个人帐号</th>
                <td><form:input path="personAccount" id="personAccount"
                                class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>
                <th class="panel-header">姓名</th>
                <td><form:input path="personName" id="personName"
                                class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>

            </tr>
            <tr height="30">
                <th class="panel-header">补缴金额</th>
                <td>
                    <form:input path="amount" id="amount"
                                class="form_view_input combo easyui-validatebox" required="true"    style="float:left;width:80px;height: 27px;"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>