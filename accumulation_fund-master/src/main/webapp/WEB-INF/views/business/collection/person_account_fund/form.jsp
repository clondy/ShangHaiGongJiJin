<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

   /* $(function () {

        if(){
            //如果是集中封存专户
            $("#gbCompanyAccount").val("");
            $("#gbCompanyAccount").blur();

            $("#gbCompanyAccount").attr("disabled","disabled");
            $("#gbCompanyName").attr("disabled","disabled");
        }

    });*/

   //校验单位账号是否存在
   var validate_company_account = {
       getValidateUrl: function () {
           url: "${pageContext.request.contextPath}/business/collection/company/get_company.shtml"
       }
       , getValidateParameter: function (value) {
           var value = jQuery.trim(value);
           if (value) {
               return {'entity.gbCompanyAccount': value};
           }
           return {}
       }
       , validator: function (data) {
           //因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
           data = jQuery.parseJSON(data);
           return !data.result;
       }
   }

   //查询单位账号信息
   $("#query_company_account").click(function () {

       var companyAccount = $("#gbCompanyAccount").val();
       if(companyAccount==""){
            $.messager.alert("警告","请输入单位账号后在进行查询！");
            return false;
       }

       $.ajax({
           url: "${pageContext.request.contextPath}/business/collection/company/get_company.shtml",
           type: "post",
           data: {'entity.gbCompanyAccount': companyAccount},
           datatype: "json",
           success: function (data) {
               if(!jQuery.isEmptyObject(data)){
                   data = jQuery.parseJSON(data);
                   if(data.result){
//                       var loadData = {};
//                       for (var key in data) {
//                           loadData['entity.' + key] = data[key];
//                       }
                       $('#person_edit_form').form('load', data);
                   }else {
                       $.messager.alert("通知","此单位账号不存在，请重新输入！");
                       return false;
                   }

               }else {
                   $.messager.alert("通知","此单位账号不存在，请重新输入！");
                   return false;
               }
           }
       });

   });


   //本地数据库校验身份证
   $("#check_idcard_only").click(function () {

       var idCardNumber = $('#idCardNumber').val();
       var idCardType = $('#idCardType').combobox('getText');

       if(idCardType =="--请选择--"){
           $.messager.alert("警告","请选择证件类型！");
           return false ;
       }else if(idCardType =="身份证"){
           if(idCardNumber.length != 18){
               $.messager.alert("警告","请输入18位身份证号码！");
               return false ;
           }
       }

       $.ajax({
           url: "${pageContext.request.contextPath}/business/collection/person/get_person.shtml",
           type: "post",
           data: {"entity.idCardNumber": idCardNumber},
           datatype: "json",
           success: function (data) {
               if(!jQuery.isEmptyObject(data)){
                   data = jQuery.parseJSON(data);
                    alert(data.result);
                   if(data.result){
                       //弹出dialog
                       $('#system_account_infomation').dialog({
                           title: '账户信息'
                           , href: "${pageContext.request.contextPath}/business/collection/person_account_fund/account_infomation.shtml"
                           , width: 460
                           , height: 300
                           , closed: true
                           , cache: false
                           , resizable: true
                           , collapsible: true
                           , maximizable: true
                           <%--, href:'${pageContext.request.contextPath}/views/business/collection/person_account_fund/account_infomation.jsp'--%>
                           ,onLoad:function(){
                               $("#nameTemp").val();
                               $("#idCardNumberTemp").val();
                               $("#personalAccountTemp").val();
                               $("#CompanyNameTemp").val();
                               $("#stateTemp").val();
                           }
                           , buttons: [
                               {
                                   text: '确定',
                                   iconCls: 'icon-ok',
                                   handler: function () {
                                       $($(this).context.parentElement.parentElement).dialog('close');
                                       $.messager.progress('close');
                                       return false;
                                   }
                               }
                               ,{
                                   text: '取消',
                                   iconCls: 'icon-cancel',
                                   handler: function () {
                                       $($(this).context.parentElement.parentElement).dialog('close');
                                       $.messager.progress('close');
                                       return false;
                                   }
                               }]
                           , modal: true
                       }).dialog('close');

                       $('#system_account_infomation').dialog('open');
                   }else{
                       $.messager.alert("通知","此号码无开户信息，可以开户！") ;
                   }
               }
           }
       });
   });

   //上海市征信库校验身份证
   $("#personal_credit_information_check").click(function () {

       var idCardNumber = $('#idCardNumber').val();
       var name = $("#name").val();

       if(idCardNumber=="" || name ==""){
           $.messager.alert("警告","身份证与姓名必须同时填写才可进行校验！");
           return false ;
       }

       $.ajax({
           url: "${pageContext.request.contextPath}/business/collection/person/personal_credit_information_check.shtml",
           type: "post",
           data: {"entity.idCardNumber": idCardNumber,"entity.name": name},
           datatype: "json",
           success: function (data) {
               if(!jQuery.isEmptyObject(data)){
                   //data = jQuery.parseJSON(data);
                   $.messager.alert("通知","上海市征信库校验通过！");
               }else{
                   $.messager.alert("警告","身份证号与姓名不符！");

               }
           }
       });
   });


   //月缴存额随工资收入变化
   $("#wageIncome").on('input',function(e){

       var wageIncome = $("#wageIncome").val();

       var companyPaymentRatio = parseInt($("#companyPaymentRatio").val());
       var personPaymentRatio = parseInt($("#personPaymentRatio").val());
       var monthPay = wageIncome*(companyPaymentRatio+personPaymentRatio)/100;
       $("#monthPay").val(monthPay);
   });

   //根据身份证号回显出生日期和性别
   function isEchoInf(){
       var idCardType = $('#idCardType').combobox('getText');
       if($('#idCardType').combobox('getText')=="身份证"){
           var idCardNumber = $("#idCardNumber").val();
           var sex = getSex(idCardNumber);
           var birthDay = getBirthday(idCardNumber);
           $("#birthYearMonth").val(birthDay);
           if(sex=="male"){
               $("#sex").combobox('setValue',89);
           }else{
               $("#sex").combobox('setValue',90);
           }
       }
   }


</script>

<div class="div_center" style="margin-top: 0px">
    <form:form name="person_edit_form" id="person_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.companyAccountFundId" id="company_account_fund_id"/>
        <form:hidden path="entityPerson.companyId" id="entityPerson_company_id"/>
        <form:hidden path="entity.id" id="entityId"/>
        <form:hidden path="isFirstSave" id="isFirstSave"/>
        <table class="form_view_border" bordercolordark="#45b97c" bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 20%" class="panel-header">单位账号</th>
                <td><form:input path="companyAccount" id="gbCompanyAccount"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
                                <%--</form:input></td>--%>
                </td>
                <td style="text-align:left;" colspan="2"><a href="#" class="easyui-linkbutton" id="query_company_account">查询</a></td>
            </tr>
            <tr>
                <th class="panel-header">单位名称</th>
                <td><form:input path="companyName" id="gbCompanyName" disabled="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
                <th class="panel-header">证件类型</th>
                <td><form:select path="entityPerson.idCardTypeId" id="idCardType"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${legalPersonIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">证件号码</th>
                <td><form:input path="entityPerson.idCardNumber"
                                style="width:184px;height: 27px;"
                                id="idCardNumber"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"
                                onblur="isEchoInf()"></form:input></td>
                <td style="text-align:left;" colspan="2"><a href="#" class="easyui-linkbutton" id="check_idcard_only">查询</a>
                    <a href="#" class="easyui-linkbutton" id="personal_credit_information_check">身份证校验</a></td>
            </tr>
            <tr>
                <th class="panel-header">姓名</th>
                <td><form:input path="entityPerson.name" id="name"
                                data-options="required: true"
                                style="width:184px;height: 27px;"></form:input></td>
                <th class="panel-header">出生日期</th>
                <td><form:input path="entityPerson.birthYearMonth"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"
                                id="birthYearMonth"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">性别</th>
                <td><form:select path="entityPerson.sexId" id="sex"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${sexList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
                <th class="panel-header">户籍</th>
                <td><form:input path="entityPerson.koseki"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox" id="koseki"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">工资收入</th>
                <td colspan="1"><form:input path="entityPerson.wageIncome" id="wageIncome"
                                            data-options="required: true"
                                            class="form_view_input combo easyui-validatebox"
                                            style="width:99.7%;height: 27px;"></form:input>
                </td>
                <th class="panel-header">缴存率标志</th>
                <td><form:select path="depositRateMarkId"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]" disabled="true">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${depositRateMarkIdList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
            </tr>
            <tr>
                <th class="panel-header">单位缴存比例(%)</th>
                <td><form:input path="entity.companyPaymentRatio" id="companyPaymentRatio"
                                style="width: 186px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner" disabled="true"></form:input></td>
                <th class="panel-header">个人缴存比例(%)</th>
                <td><form:input path="entity.personPaymentRatio" id="personPaymentRatio"
                                style="width: 184px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner" disabled="true"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">月缴存额</th>
                <td colspan="3"><form:input path="entity.monthPay"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="monthPay"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
            </tr>
        </table>
    </form:form>
</div>
<div>
    <input id="nameTemp" type="hidden"/>
    <input id="idCardNumberTemp" type="hidden"/>
    <input id="personalAccountTemp" type="hidden"/>
    <input id="CompanyNameTemp" type="hidden"/>
    <input id="stateTemp" type="hidden"/>

</div>

<div id="system_account_infomation">

</div>