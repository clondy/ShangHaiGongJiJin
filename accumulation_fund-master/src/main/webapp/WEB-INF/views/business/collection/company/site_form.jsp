<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script>

    //输入框触发事件

    //定义查询返回的json数据
    var siteJson;
    $("#company_search_Info").click(function () {
        var code = $("#gbCompanyAccount").val();
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/company/get_company_site_form.shtml",
            type: "post",

            data: {gbCompanyAccount: code},
            datatype: "json",
            success: function (data) {
                if (!jQuery.isEmptyObject(data)) {
                    data = jQuery.parseJSON(data);
                    siteJson = data;
                    console.log(data);
                    var loadData = {};
                    for (var key in data) {
                        loadData[key] = data[key];
                        $('#company_site_form').form("clear");
                        $('#company_site_form').form('load', data);                    }
                    }

            }
        });
    })


</script>
<div class="div_center">
    <form:form name="company_site_form" id="company_site_form" method="post"
               style="width:600px"
               action="" onsubmit="return false;">
        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
                <th class="panel-header">单位账号</th>
                <td><form:input path="entity.gbCompanyAccount"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="gbCompanyAccount"
                                class="form_view_input combo easyui-validatebox"></form:input>
                    <a href="#" class="easyui-linkbutton" id="company_search_Info">查询</a>
                </td>
            </tr>
            <tr>
                <th class="panel-header">统一社会信用代码（组织机构代码）</th>
                <td><form:input path="entity.creditCode"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="creditCode"
                                class="form_view_input combo easyui-validatebox"></form:input>
                </td>
             </tr>
            <tr>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.gbCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="gbCompanyName"
                                class="form_view_input combo easyui-validatebox"></form:input>
                </td>
            </tr>
                 <tr>
                     <th class="panel-header">单位联系地址</th>
                     <td><form:input path="entity.companyAddress"
                                     data-options="required: true"
                                     style="width:184px;height: 27px;" id="companyAddress"
                                     class="form_view_input combo easyui-validatebox"></form:input>
                     </td>
                 </tr>
                 <tr>
                     <th class="panel-header">联系人姓名</th>
                     <td><form:input path="entity.contactName"
                                     data-options="required: true"
                                     style="width:184px;height: 27px;" id="contactName"
                                     class="form_view_input combo easyui-validatebox"></form:input>
                     </td>
                 </tr>
                 <tr>
                     <th class="panel-header">联系人移动电话</th>
                     <td><form:input path="entity.contactMobilePhone"
                                     data-options="required: true"
                                     style="width:184px;height: 27px;" id="contactMobilePhone"
                                     class="form_view_input combo easyui-validatebox"></form:input>
                     </td>
                 </tr>
                     <th class="panel-header">缴存网点</th>
                     <td><form:input path="entity.paymentSite"
                                     data-options="required: true"
                                     style="width:184px;height: 27px;" id="paymentSite"
                                     class="form_view_input combo easyui-validatebox"></form:input>
                     </td>
                 </tr>
            <form:hidden path="entity.id" id="company_id"></form:hidden>
            <form:hidden path="companyHistoryId" id="company_history_id"></form:hidden>
        </table>
    </form:form>
</div>
