<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<div class="div_center">
    <form:form name="company_review_form" id="company_review_form"
               method="post" action="" data-dojo-type="dijit/form/Form"
               onsubmit="return false;">

        <table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
               bordercolorlight="#45b97c" border="1px" cellpadding="0"
               cellspacing="0">
            <tr height="30">
                <th class="panel-header">单位账户</th>
                <td class="page_input" width="60%" colspan="3">
                    <input type="text" id="reviewUsername" name="reviewUsername"
                           class="form_view_input combo easyui-validatebox"
                           data-options="required: true" style="height: 27px;">
                </td>
            </tr>
            <tr height="30">
                <th class="panel-header">启封选项</th>
                <td><select path="openReason"
                                id="openReason"
                                 style="width:184px;height: 27px;"
                                 data-options="required: true,editable: false,valueField:'id',textField:'text',url:'<c:url value="/business/collection/pause_recovery_record/searchListBySortId.shtml?sortId=18"/>'"
                                 name="openReason"
                                 class="form_view_input combo easyui-combobox">

                </select></td>
            </tr>
        </table>
    </form:form>
</div>