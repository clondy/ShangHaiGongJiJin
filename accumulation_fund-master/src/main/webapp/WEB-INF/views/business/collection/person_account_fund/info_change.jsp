<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>




    var personChangeService;
    $(function () {




        personChangeService = new $.BaseService('', '#person_change_list_datagrid'
            , '', ''
            , {
                listUrl:'change_list.shtml?entity.personAccountFundId='+${personAccountFundId}
                ,actionRootUrl: '<c:url value="/business/collection/person_account_fund/" />'
                , entityTitle: "个人账户变更"
                , optionFormatter: function(value, rec) {

                    return '<a href="#" title="详细信息" onClick="showDiffence(' + rec.id + ')" iconCls="icon-company-edit" class="easyui-linkbutton" data-options="plain: true"></a>';
                }
                , datagridAddButtonId: 'person_change_add_button'
                , datagridBatchDeleteButtonId: 'person_change_batch_delete_button'
                , datagridColumns: [
                    {field: 'personalAccount', title: '个人账号', width: 220, rowspan: 2, align: 'center'},
                    {field: 'dateTime', title: '变更日期', width: 220, rowspan: 2, align: 'center'},
                    {field:'id',title:'id', width: 220, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 700
                , dialogHeight: 450
                ,datagridToolbar: [
                    {
                        id:'change_button'
                        ,text:'变更'
                        ,iconCls: 'icon-add'
                        , handler: function() {
//                            personChangeService.dialog.dialog('open').dialog('refresh', personChangeService.actionRootUrl +'change_form.shtml');
                        $('#change_audit_form').dialog({
                            title: '账户变更'
                            , href: "${pageContext.request.contextPath}/business/collection/person_account_fund/change_form.shtml?entity.id="+${personAccountFundId}
                            , width: 690
                            , height: 460
                            , closed: true
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [
                                {
                                    id: 'person_change_confirm'
                                    , text: '保存'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {
                                    $.messager.progress();
                                    if($('#person_change_edit_form').form('validate')){
                                        if (!confirm("要执行“确认”操作吗？？？")) {
                                            $.messager.progress('close');
                                            return false;
                                        }else{
                                            personChangeHistorySaveOrUpdate();
                                        }
                                    }else{
                                        $.messager.progress('close');
                                    }
                                    function personChangeHistorySaveOrUpdate() {
                                        $("#person_change_edit_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/person_history_account_fund/history/change_save_or_update.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#person_change_edit_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }
                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                alert(data.result);
                                                $("#person_history_id").val(data.personHistoryId);
                                                $("#isFirstSave").val(data.isFirstSave);
                                                if (data.result) {
                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });
                                                    $('#person_change_audit').linkbutton('enable');
                                                } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        });
                                    }
                                }
                                }
                                ,{
                                    id:'person_change_audit'
                                    ,text:'审核'
                                    ,disabled: true
                                    ,handler:function(){

                                        var idCardNumber = $('#idCardNumber').val();
                                        alert(idCardNumber);
                                        alert(flag);

                                        if(flag){
                                            $.ajax({
                                                url: "${pageContext.request.contextPath}/business/collection/person/get_person.shtml",
                                                type: "post",
                                                data: {"entity.idCardNumber": idCardNumber},
                                                datatype: "json",
                                                success: function (data) {
                                                    if(!jQuery.isEmptyObject(data)){
                                                        $.messager.alert("警告","身份证号重复，请进行复核","info",function(){
                                                            $('#change_confirm_form_dialog').dialog('open');
                                                        });
                                                    }else {
                                                        person_change_audit();
                                                    }
                                                }
                                            });
                                        } else {
                                            person_change_audit();
                                        }

                                        $.messager.progress();
                                        //复核业务dialog
                                        $('#change_confirm_form_dialog').dialog({
                                            title: '业务复核'
                                            , href: "${pageContext.request.contextPath}/business/collection/company/review.shtml"
                                            , width: 600
                                            , height: 250
                                            , closed: true
                                            , cache: false
                                            , resizable: true
                                            , collapsible: true
                                            , maximizable: true
                                            , buttons: [{
                                                text: '复核'
                                                , iconCls: 'icon-save'
                                                , handler: function () {
                                                    // $($(this).context.parentElement.parentElement).dialog('close');
                                                    //查询用户名密码是否正确
                                                    $('#company_review_form').form('submit', {
                                                        url: '${pageContext.request.contextPath}/business/collection/company/company_review_user.shtml',
                                                        onSubmit: function () {
                                                            // do some check
                                                            var reviewIsVal = $('#company_review_form').form('validate');
                                                            if (!reviewIsVal) {
                                                                $.messager.progress('close');
                                                            } else {
                                                                if (!confirm("要执行“复核”操作吗？？？")) {
                                                                    $('#company_review_form').dialog('close')

                                                                }
                                                            }
                                                            return reviewIsVal
                                                        },
                                                        success: function (data) {
                                                            data = jQuery.parseJSON(data);
                                                            console.log(data)
                                                            if (data.result) {
                                                                $('#company_confirmerId').val(data.userId);
                                                                if ("${eccomm_admin.id}" == data.userId) {
                                                                    $.messager.alert('通知', "不能使用当前用户复核！");
                                                                    $.messager.progress('close');
                                                                } else {
                                                                    person_change_audit();
                                                                }

                                                            } else {
                                                                $.messager.alert('通知', "用户名密码错误！");
                                                                $.messager.progress('close');
                                                            }
                                                        }
                                                    });

                                                }
                                            }
                                                , {
                                                    text: '取消',
                                                    iconCls: 'icon-cancel',
                                                    handler: function () {
                                                        $($(this).context.parentElement.parentElement).dialog('close');
                                                        $.messager.progress('close');
                                                        return false;
                                                    }
                                                }]
                                            , modal: true
                                        }).dialog('close');

                                        function person_change_audit(){
                                            $("#person_change_edit_form").form('submit', {

                                                url: '${pageContext.request.contextPath}/business/collection/person_history_account_fund/person_change_audit.shtml'
                                                , onSubmit: function () {
                                                }
                                                , success: function (data) {
                                                    $("#person_change_audit").linkbutton('disable');
                                                    $.messager.progress('close');
                                                    console.log(data)
                                                    data = jQuery.parseJSON(data);
                                                    if (data.result) {
                                                        $.messager.alert('通知', "审核成功！");
                                                        $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                            if (r){
                                                                window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                            }
                                                            $('#change_audit_form').dialog('close');
                                                        });
                                                        $.messager.progress('close');

                                                    } else {
                                                        $.messager.alert('通知', "审核失败！");

                                                    }
                                                    $.messager.progress('close');
                                                    $("#change_audit_form").dialog('close');
                                                }
                                            });
                                        }
                                    }
                                }
                                , {
                                    id: 'person_change_reset'
                                    , text: '重置'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {

                                        if (confirm("要执行“重置”操作吗？？？")) {

                                            $("#person_audit").linkbutton('disable');
                                            personChangeService.dialog.dialog('refresh', personChangeService.actionRootUrl + personChangeService.formUrl);
                                        }
                                    }
                                }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                    }
                                }
                                ]
                            , modal: true
                        }).dialog('close');

                        $('#change_audit_form').dialog('open');
                        }
                    }
                ]
                , dialogButtons: [

                ]
                ,
            });
    });

    function showDiffence(id) {
        alert(id);
        $('#show_diffence_info').dialog({
            title: '账户变更信息对比'
            , href: "${pageContext.request.contextPath}/business/collection/person_account_fund/show_diffence.shtml?entity.id="+id
            , width: 460
            , height: 300
            , closed: true
            , cache: false
            , resizable: true
            , collapsible: true
            , maximizable: true
            , buttons: [
                {
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                        $.messager.progress('close');
                        return false;
                    }
                }
                ,{
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                        $.messager.progress('close');
                        return false;
                    }
                }]
            , modal: true
        }).dialog('close');

        $('#show_diffence_info').dialog('open');
    }
    
</script>

<div style="margin:10px 0;"></div>

<table id="person_change_list_datagrid"></table>

<div id="change_audit_form">

</div>

<div id="show_diffence_info"></div>

<div id="change_confirm_form_dialog"></div>
