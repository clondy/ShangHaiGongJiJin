<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="freeze" items="${FreezeList}" varStatus="status">
    {"id": "${freeze.id}",
    "personalAccount": "${freeze.person.personalAccount}",
    "name": "${freeze.person.name}",
    "idCardNumber": "${freeze.person.idCardNumber}",
    "gbCompanyAccount": "${freeze.person.company.gbCompanyAccount}",
    "gbCompanyName": "${freeze.person.company.gbCompanyName}",
    "type": "${freeze.type.name}",
    "freezeReason": "${freeze.freezeReason.name}",
    "caseNumber": "${freeze.caseNumber}",
    "judgeName": "${freeze.judgeName}",
    "contactInfo": "${freeze.contactInfo}",
    "startTime": "${freeze.startTime}",
    "freezePeriod": "${freeze.freezePeriod}",
    "endTime": "${freeze.endTime}",
    "remark": "${freeze.remark}",
    "operator": "${freeze.operator.name}",
    "courtName": "${freeze.courtName.name}",
    "amount": "${freeze.amount}",
    "unfreezeReason": "${freeze.unfreezeReason.name}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}