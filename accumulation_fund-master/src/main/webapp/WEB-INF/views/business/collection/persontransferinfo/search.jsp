<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var personTransferInfo;
    $(function() {

        personTransferInfo = new $.BaseService('#person_transfer_info_query_form', '#person_transfer_info_list_datagrid'
            , '#person_transfer_info_form_dialog', '#person_transfer_info_edit_form'
            , {actionRootUrl: '<c:url value="/business/collection/persontransferinfo/" />'
                , entityTitle: "封存转移"
                , datagridAddButtonId: 'person_transfer_info_add_button'
                , datagridBatchDeleteButtonId: 'person_transfer_info_batch_delete_button'
                , datagridColumns:
                    [
                        {field: 'turnInCompanyAccount', title: '转入单位账号', width: 150, rowspan: 1, align: 'center'},
                        {field: 'turnOutCompanyAccount', title: '转出单位账号', width: 150, rowspan: 1, align: 'center'},
                        {field: 'transfromDate', title: '转移日期', width: 150, rowspan: 1, align: 'center'},
                        {field: 'personalAccount', title: '职工账号', width: 150, rowspan: 1, align: 'center'},
                        {field: 'bussinessState', title: '业务状态', width: 150, rowspan: 1, align: 'center'},
                        {field: 'businessType', title: '业务类型', width: 150, rowspan: 1, align: 'center'}
                    ]
                , dialogWidth: 800
                , dialogHeight: 600
                ,dialogButtons: [

        {
            id: 'freeze_confirm'
            , text: '保存'
            //, iconCls: 'icon-save'
            , handler: function () {
            $.messager.progress();
            if($('#person_transfer_info_edit_form').form('validate')){
                if (!confirm("要执行“保存”操作吗？？？")) {
                    $.messager.progress('close');
                    return false;
                }else{

                    companyHistorySaveOrUpdate();


                }
            }else{
                $.messager.progress('close');
            }
            function companyHistorySaveOrUpdate(){
                $("#person_transfer_info_edit_form").form('submit', {
                    url: '${pageContext.request.contextPath}/business/collection/persontransferinfo/save_or_update.shtml'
                    , onSubmit: function () {
                        var isValid = $('#person_transfer_info_edit_form').form('validate');
                        if (!isValid) {
                            $.messager.progress('close');
                        }
                        return isValid
                    }

                });
            }



        }
        }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
                ,
            });

        $('#person_transfer_info_query_form_search_button').click(function () {
            $('#person_transfer_info_query_form').submit();
        });
    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="person_transfer_info_query_form" id="person_transfer_info_query_form"
					   method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">职工账号</th>
						<td><form:input path="entity.PersonalAccount"
										class="form_view_input combo easyui-validatebox" style="height: 27px;" /></td>
					</tr>
				</table>
			</form:form>
		</span>
	<span style="float:right;">
			<a class="l-btn" id="person_transfer_info_query_form_search_button"><span class="l-btn-left">
				<span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
	<table>
		<tr><td height="30px"><td></tr>
	</table>
</div>

<table id="person_transfer_info_list_datagrid"></table>
<div id="person_transfer_info_form_dialog">Dialog Content.</div>