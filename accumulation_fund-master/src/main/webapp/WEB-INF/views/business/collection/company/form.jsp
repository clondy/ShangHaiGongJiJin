<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    var validateCompany_redit_code = {
        getValidateUrl: function() {
            return '<c:url value="/business/collection/company/is_exist.shtml" />';
        }
        , getValidateParameter: function(value) {
            var value = jQuery.trim(value);
            if( value ) {
                return {'companyCode': value};
            }
            return {}
        }
        , validator: function(data) {
            //因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
            data = jQuery.parseJSON(data);
            return !data.result;
        }
    }

    //拼接字符拼接字符串
    function showJson() {
        var idCardNum = $("#gbLegalPersonIdCardNumber").val()
        var legalPersonName = $("#gbLegalPersonName").val()
        var legalPersonIdCardTypeId = $("#legalPersonIdCardTypeId").val()
        var legalPersonMobilePhone = $("#legalPersonMobilePhone").val()

        var str = {
            gbLegalPersonIdCardNumber: idCardNum,
            gbLegalPersonName: legalPersonName,
            legalPersonIdCardTypeId: legalPersonIdCardTypeId,
            legalPersonMobilePhone: legalPersonMobilePhone
        };
        return str;
    }
    //定义查询返回的json数据
    var companyCallBackJson;
    //根据信用代码查询信息

    $("#company_search_Info").click(function () {
        var code = $("#company_redit_code").val();
        if (!(code.length == 9 || code.length == 18)) {
            $.messager.alert("提示", "信用代码或组织机构代码不正确！")
            return false;
        }
        //查询本地
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/company/is_exist.shtml",
            type: "post",
            data: {companyCode: code},
            datatype: "json",
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                console.log(obj)
                //已存在
                if (obj.result) {
                    $('#flag').val(data.result);
                    $.messager.alert("通知", "该企业已开户！")
                    return false;
                } else {
                    //查询法人库
                    $.ajax({
                        url: "${pageContext.request.contextPath}/business/collection/company/get_legal_person.shtml",
                        type: "post",
                        data: {companyCode: code},
                        datatype: "json",
                        success: function (data) {
                            if(!jQuery.isEmptyObject(data)){
                                data = jQuery.parseJSON(data);
                                companyCallBackJson = data;
                                console.log(data);
                                var loadData = {};
                                for (var key in data) {
                                    loadData['entity.' + key] = data[key];
                                }
                                $('#company_edit_form').form('load', loadData);
                            }else{
                                $('#company_flag').val(1);
                            }

                        }
                    });

                }
            }
        })
    });

    $('#company_edit_form').change(function() {
        $('#company_audit').linkbutton('enable');
    })

</script>

<div class="div_center" style="margin-top: 90px">
    <form:form name="company_edit_form" id="company_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.id" id="company_entity_id"/>
        <form:hidden path="confirmerId" id="company_confirmerId"/>
        <form:hidden path="flag" id="company_flag" value="0" />
        <form:hidden path="accountType" id="accountType" value="0" />


        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
                <th style="width: 20%" class="panel-header">统一社会信用代码（组织机构代码）</th>
                <td><form:input path="entity.creditCode" id="company_redit_code"
                                style="width:184px;height: 27px;"

                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^([0-9A-HJ-NPQRTUWXY]{2})([0-9]{6})([0-9A-HJ-NPQRTUWXY]{9})([0-9A-HJ-NPQRTUWXY])$)|(^[A-Z0-9]{13}$)|(^[A-Z0-9]{9}$)','社会信用代码有误！','validateCompany_redit_code','公司已开户']"></form:input></td>
                <td style="text-align:left;" colspan="2"><a href="#" class="easyui-linkbutton">读卡</a>
                    <a href="#" class="easyui-linkbutton" id="company_search_Info">查询</a></td>
            </tr>
            <tr>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.gbCompanyName" id="gbCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
                <th class="panel-header">区县</th>
                <td><form:select path="entity.regionId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${regionIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">主管机构代码</th>
                <td><form:input path="entity.competentCode"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox" id="competentCode"
                                validType="complexValid['','']"></form:input></td>
                <th class="panel-header">账户性质</th>
                <td><form:select path="entity.natureAccountId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${natureAccountIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>


            </tr>
            <tr>
                <th class="panel-header">注册地址</th>
                <td colspan="3"><form:input path="entity.registeredAddress" id="registeredAddress"
                                            data-options="required: true"
                                            class="form_view_input combo easyui-validatebox"
                                            style="width:99.7%;height: 27px;"
                                            validType="complexValid['^.{1,255}$','地址长度不能超过255字符！']"></form:input>
                </td>
            </tr>
            <tr>
                <th class="panel-header">注册地邮编</th>
                <td><form:input path="entity.registeredPostCode"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="registeredPostCode"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^[1-9][0-9]{5}$','请输入正确的邮编！']"></form:input></td>
                <th class="panel-header">发薪日</th>
                <td><form:input path="entity.gbCompanyPayDay" id="gbCompanyPayDay"
                                data-options="required: true,increment:1,min:1,max:31"
                                style="width: 184px;height: 27px;"
                                class="easyui-numberspinner"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">单位联系地址</th>
                <td colspan="3"><form:input path="entity.companyAddress" id="companyAddress"
                                            data-options="required: true"
                                            class="form_view_input combo easyui-validatebox"
                                            style="width:99.7%;height: 27px;"
                                            validType="complexValid['^.{1,255}$','地址长度不能超过255字符！']"></form:input>
                </td>
            </tr>
            <tr>
                <th class="panel-header">邮政编码</th>
                <td><form:input path="entity.gbCompanyPostCode"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="gbCompanyPostCode"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^[1-9][0-9]{5}$','请输入正确的邮编！']"></form:input></td>
                <th class="panel-header">经济类型</th>
                <td><form:select path="entity.companyEconomicTypeId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${companyEconomicTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">隶属关系</th>
                <td><form:select path="entity.companyRelatoinshipsId"
                                 style="width:186px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${companyRelatoinshipsIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
                <th class="panel-header">机构类型</th>
                <td><form:select path="entity.organizationTypeId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${organizationTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">开户日期</th>
                <td><form:input path="entity.companyOpenedDate"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="CompanyOpenedDate"
                                class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input></td>
                <th class="panel-header">缴存网点</th>
                <td>
                    <form:input path="entity.paymentSite" id="siteId"
                                data-options="required: true"
                                style="width:182px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"></form:input>
                </td>
            </tr>
            <tr>
                <th class="panel-header">是否具备法人资格</th>
                <td colspan="3">
                    <form:select path="entity.legalPersonality"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                        <form:option value="" >--请选择--</form:option>
                        <form:option value="1" >是</form:option>
                        <form:option value="0" >否</form:option>
                    </form:select>
                </td>
            </tr>
            <tr>
                <th class="panel-header">法人代表姓名</th>
                <td><form:input path="entity.gbLegalPersonName"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="gbLegalPersonName"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{1,25}$','姓名长度不能超过25字符！']"></form:input></td>
                <th class="panel-header" style="width: 20%">法人代表证件类型</th>
                <td><form:select path="entity.legalPersonIdCardTypeId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${legalPersonIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">法人代表证件号码</th>
                <td><form:input path="entity.gbLegalPersonIdCardNumber" id="gbLegalPersonIdCardNumber"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"></form:input></td>
                <th class="panel-header">法人代表移动电话</th>
                <td><form:input path="entity.legalPersonMobilePhone"
                                style="width:182px;height: 27px;"
                                id="legalPersonMobilePhone"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^1[3,4,5,7,8]\\\d{9}$','请输入正确手机号！']"></form:input></td>
            </tr>

            <tr>
                <th style="text-align:center;" class="panel-header" colspan="4">联系人信息</th>
            </tr>

            <tr>
                <th class="panel-header">联系人姓名</th>
                <td><form:input path="entity.contactName" id="contactName"
                                style="width:184px;height: 27px;"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{1,25}$','姓名长度不能超过25字符！']"></form:input></td>
                <th class="panel-header">联系人证件类型</th>
                <td><form:select path="entity.contactIdCardTypeId"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${contactIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">联系人证件号码</th>
                <td><form:input path="entity.contactIdCardNumber"
                                style="width:184px;height: 27px;"
                                id="contactIdCardNumber"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"></form:input></td>
                <th class="panel-header">联系人移动电话</th>
                <td><form:input path="entity.contactMobilePhone"
                                style="width:182px;height: 27px;" id="contactMobilePhone"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^1[3,4,5,7,8]\\\d{9}$','请输入正确手机号！']"></form:input></td>
            </tr>
            <tr>
                <th style="text-align:center;" class="panel-header" colspan="4">缴存信息</th>
            </tr>

            <tr>
                <th class="panel-header">启缴年月</th>
                <td><form:input path="companyAccount.startPaymentDate" id="startPaymentDate"
                                style="width: 184px;height: 27px;"
                                data-options="required: true"
                                class="Wdate"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"
                                validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td>
                <%--<th class="panel-header">缴存网点</th>--%>
                <%--<td>--%>
                    <%--<form:input path="companyAccount.siteId" id="siteId"--%>
                                <%--data-options="required: true"--%>
                                <%--style="width:182px;height: 27px;"--%>
                                <%--class="form_view_input combo easyui-validatebox"></form:input>--%>
                <%--</td>--%>

            </tr>
            <tr>
                <th class="panel-header">缴交日</th>
                <td><form:input path="companyAccount.paymentDate" id="paymentDate"
                                data-options="required: true,increment:1,min:1,max:31"
                                style="width: 184px;height: 27px;"
                                class="easyui-numberspinner"></form:input></td>

                <th class="panel-header">缴存率标志</th>
                <td><form:select path="companyAccount.depositRateMarkId"
                                      style="width: 184px;height: 27px;"
                                      class="form_view_input combo easyui-combobox"
                                      data-options="required: true, editable: false"
                                      validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                <form:option value="" >--请选择--</form:option>
                <form:options items="${depositRateMarkIdList}" itemValue="id" itemLabel="name"/>
            </form:select>

            </td>

            </tr>
            <tr>
                <th class="panel-header">单位缴存比例(%)</th>
                <td><form:input path="companyAccount.companyPaymentRatio" id="companyPaymentRatio"
                                style="width: 186px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner"></form:input></td>
                <th class="panel-header">个人缴存比例(%)</th>
                <td><form:input path="companyAccount.personPaymentRatio" id="personPaymentRatio"
                                style="width: 184px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">备注</th>
                <td rowspan="2" colspan="3"><form:input path="companyAccount.notes" id=""
                                                        style="width:99.7%;height: 40px"
                                                        class="form_view_input combo easyui-validatebox"
                                                        data-options="multiline:true"
                                                        validType="complexValid['','']"></form:input>
            </tr>
        </table>
    </form:form>
</div>
