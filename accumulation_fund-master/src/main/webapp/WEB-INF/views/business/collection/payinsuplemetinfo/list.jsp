<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="payinsupplementinfo" items="${PayinSupplementInfoList}" varStatus="status">
    { "id": "${payinsupplementinfo.id}",
    "serialNumber": "${payinsupplementinfo.id}",
    "companyAccountFund.companyAccount": "${payinsupplementinfo.companyAccountFund.companyAccount}",
    "companyAccountFund.unitName": "${payinsupplementinfo.companyAccountFund.unitName}",
    "paymentOptions": "${payinsupplementinfo.paymentOptions}",
    "paymentReason": "${payinsupplementinfo.paymentReason}",
    "paymentAmount": "${payinsupplementinfo.paymentAmount}",
    "receivedDate": "${payinsupplementinfo.receivedDate}",
    "approvalDate": "${payinsupplementinfo.approvalDate}",
    "startTime": "${payinsupplementinfo.startTime}",
    "endTime": "${payinsupplementinfo.endTime}",
    "numberOfPeople": "${payinsupplementinfo.numberOfPeople}",
    "dateTime": "${payinsupplementinfo.dateTime}",
    "bankCode": "${payinsupplementinfo.bankCode}",
    "operatorId": "${payinsupplementinfo.operatorId}",
    "regionId": "${payinsupplementinfo.regionId}",
    "siteId": "${payinsupplementinfo.siteId}",
    "confirmerId": "${payinsupplementinfo.confirmerId}",
    "confirmationTime": "${payinsupplementinfo.confirmationTime}",
    "channels": "${payinsupplementinfo.channels}",
    "confirmerId": "${payinsupplementinfo.confirmerId}",
    "confirmationTime": "${payinsupplementinfo.confirmationTime}",
    "channels": "${payinsupplementinfo.channels}",
    "createName": "${payinsupplementinfo.createName}"

    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}