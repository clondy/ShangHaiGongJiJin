<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>


    var payininfoService;
    $(function () {

        payininfoService = new $.BaseService('#payininfo_query_form', '#payininfo_list_datagrid'
            , '#payininfo_form_dialog', '#payininfo_edit_form'
            , {
                formUrl:'form.shtml'
                ,actionRootUrl: '<c:url value="/business/collection/payininfo/" />'
                , entityTitle: "单位汇缴核定"
                ,optionFormatter: function(value, rec) {

                        return '<a href="#" title="继续办理" onClick="payininfoService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + payininfoService.actionRootUrl + payininfoService.formUrl + '?entity.id=' + rec.id +'&entity.verificationOptions='+rec.verificationOptions+ '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                                + '&nbsp;&nbsp;'
                                + '<a href="#" title="删除"  onClick="payinfoDelete(' + rec.id + ')" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
                    }

                ,datagridToolbar: [{
                        id: this.datagridSearchButtonId
                        , text: '查询'
                        , iconCls: 'icon-search'
                        , handler: function() {
                            $('#payininfo_query_form').submit();
                        }
                    },'-',{
                        id: this.datagridAddButtonId
                        , text: '新增核定'
                        , iconCls: 'icon-add'
                        , handler: function() {

                            payininfoService.dialog.dialog('open').dialog('refresh', payininfoService.actionRootUrl + payininfoService.formUrl);
                        }
                    },'-',{
                        id: this.datagridBatchDeleteButtonId
                        , text: '批量删除'
                        , iconCls: 'icon-remove'
                        , handler: function() {
                            var checkedData = {};
                            var items = payininfoService.datagrid.datagrid('getChecked');
                            for(var i = 0; i < items.length; i++) {

                                checkedData["ids[" + i + "]"] = items[i].id;
                            }

                            var dataCount = Object.keys(checkedData).length;
                            var prefix = ( 1 < dataCount )? "批量": "";

                            if( dataCount ) {

                                if( confirm("确定要删除吗？？？") ) {
                                    $.ajax({
                                        url: payininfoService.actionRootUrl + payininfoService.deleteUrl
                                        , type : "post"
                                        , dataType : 'json'
                                        , data : checkedData
                                        , success : function(data, response, status) {

                                            if( jQuery.parseJSON('' + data) ) {

                                                $.messager.alert('通知', prefix + "删除成功。");
                                                payininfoService.datagrid.datagrid('clearChecked');
                                                payininfoService.datagrid.datagrid('reload');
                                            } else {

                                                $.messager.alert('通知', prefix + "删除失败。");
                                            }
                                        }
                                    });

                                } else {

                                    payininfoService.datagrid.datagrid('clearChecked');
                                }
                            } else {

                                $.messager.alert('通知', "请选中数据后再执行删除。");
                            }
                        }


                    }

                    ]


                    ,    datagridColumns: [
                    {field: 'serialNumber', title: '流水号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'payMonthly', title: '缴存年月', width: 100, rowspan: 2, align: 'center'},
                    {field: 'verificationOptions', title: '核定选项', width: 75, rowspan: 2, align: 'center'},
                    {field: 'lastRemiAmount', title: '上月汇缴金额', width: 75, rowspan: 2, align: 'center'},
                    {field: 'lastRemitNumber', title: '上月汇缴人数', width: 75, rowspan: 2, align: 'center'},
                    {field: 'increaseRemitAmount', title: '本月增加金额', width: 75, rowspan: 2, align: 'center'},
                        {field: 'increaseRemitNumber', title: '本月增加人数', width: 75, rowspan: 2, align: 'center'},
                        {field: 'reduceRemitAmount', title: '本月减少金额', width: 75, rowspan: 2, align: 'center'},
                        {field: 'reduceRemitNumber', title: '本月减少人数', width: 75, rowspan: 2, align: 'center'},
                        {field: 'remitAmount', title: '本月汇缴金额', width: 75, rowspan: 2, align: 'center'},
                        {field: 'remitNumber', title: '本月汇缴人数', width: 75, rowspan: 2, align: 'center'},
                        {field: 'isfinished', title: '完成状态', width: 75, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 400
                , dialogButtons: [
                    {
                        id: 'payininfo_confirm'
                        , text: '核定'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        //$.messager.progress();
                        if($('#payininfo_hd_form').form('validate')){
                            if (!confirm("要执行“核定”操作吗？？？")) {

                                //$.messager.progress('close');
                                return false;
                            }else{
                                payininfoSaveOrUpdate();

						}
                        function payininfoSaveOrUpdate(){
                            $("#payininfo_hd_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/collection/payininfo/build_payininfo_inventory.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#payininfo_hd_form').form('validate');
//                                    if (!isValid) {
//                                        $.messager.progress('close');
//                                    }
                                    return isValid;
                                }
                                , success: function (data) {
                                   // $.messager.progress('close');
                                    console.log(data);
                                    data = jQuery.parseJSON(data);
                                    if (data.result ) {
                                           payininfouncheckshow();
                                           $.messager.alert('通知', "核定成功！");

                                    } else {
                                        $.messager.alert('通知', "核定失败！");
                                    }
                                }
                            });
                        }
                           function payininfouncheckshow(){
                               console.log("核定")
                               var account1=$('#companyAccount').val();
                               var startPaymentDate=$('#startPaymentDate').val();
                               var verificationOptions=$('#verificationOptions').val();

                               $.ajax({
                                   url: "${pageContext.request.contextPath}/business/collection/payininfo/search_payinfounchcked.shtml",
                                   type: "post",
                                   data: {companyAccot: account1,

                                       verificationOptions:verificationOptions},
                                   datatype: "json",
                                   success: function (data) {
                                       var obj = jQuery.parseJSON(data);
                                       //$('#payininfo_unchecklist_datagrid').datagrid('loadData',data);
                                       console.log(obj);
                                       $('#count').text(obj.count);
                                       console.log(obj.count);
                                       $('#amount').text(obj.amount);
                                       $('#num').text(obj.num);
                                       $('#account1').text(obj.account);

                                       $('#payinfo_confirm').linkbutton('enable');
                                       $('#payinfo_cancel').linkbutton('enable');

                                   }
                               })
/*                               $.ajax({
                                   url: "${pageContext.request.contextPath}/business/collection/payininfo/search_payinfounchcked.shtml",
                                   type: "post",
                                   data: {companyAccot: account,
                                       startPaymentDate:startPaymentDate,
                                       verificationOptions:verificationOptions
                                   },
                                   datatype: "json",
                                   success: function (data) {
                                       var obj = jQuery.parseJSON(data);
                                       alert('12345')

                                   }
                               })*/
                           }


                    }
                    }}
                        , {
                            id: 'payinfo_cancel'
                            , text: '取消核定'
                            //, iconCls: 'icon-save'
                            ,   disabled: true

                            ,handler: function () {

                                if (confirm("要执行确认操作吗？？？")) {

                                    $.post("${pageContext.request.contextPath}/business/collection/payininfo/payinfo_cancel.shtml",
                                            {
                                                companyAccot: $('#companyAccount').val(),

                                                verificationOptions:$("#options").combobox("getValue"),
                                                remitDate:$('#dateTime').val()
                                            },
                                            function (data, textStatus) {
                                                if (data.result ) {

                                                    $.messager.alert('通知', "取消成功！");

                                                } else {
                                                    $.messager.alert('通知', "取消失败！");
                                                }
                                            }, "json");
                                }
                            }
                        }
                        , {
                            id: 'payinfo_confirm'
                            , text: '确认'
                            //, iconCls: 'icon-save'
                            ,   disabled: true

                        ,handler: function () {

                                if (confirm("要执行确认操作吗？？？")) {
                                      console.log($('#verificationOptions').val())
                                   // $("#").linkbutton('disable');
                                    $.post("${pageContext.request.contextPath}/business/collection/payininfo/payinfo_confirm.shtml",
                                            {
                                                companyAccot: $('#companyAccount').val(),

                                                verificationOptions:$("#options").combobox("getValue"),
                                                remitDate:$('#dateTime').val()
                                            },
                                            function (data, textStatus) {
                                                if (data.result ) {

                                                    $.messager.alert('通知', "确认成功！");

                                                } else {
                                                    $.messager.alert('通知', "确认失败！");
                                                }
                                            }, "json");
                                }
                            }
                        }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                //$("#").linkbutton('disable');
                                payininfoService.dialog.dialog('refresh', payininfoService.actionRootUrl + payininfoService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');

                        }
                    }
                ]
            });


    });
    function payinfoDelete(id){
        var checkedData = {};
        checkedData["ids[" + 0 + "]"] = id;
        $.ajax({
            url: payininfoService.actionRootUrl + payininfoService.deleteUrl
            , type : "post"
            , dataType : 'json'
            , data : checkedData
            , success : function(data, response, status) {

                if( jQuery.parseJSON('' + data) ) {

                    $.messager.alert('通知',  "删除成功。");
                    payininfoService.datagrid.datagrid('clearChecked');
                    payininfoService.datagrid.datagrid('reload');
                } else {

                    $.messager.alert('通知', prefix + "删除失败。");
                }
            }
        });

    }

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;height: 80px">
		<span style="float:left;">
			<form:form name="payininfo_query_form" id="payininfo_query_form"
                       method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"
                       bordercolorlight="#45b97c" border="1px" cellpadding="0"
                       cellspacing="0" style="">
					<tr>
                        <th class="panel-header">缴存网点</th>
                        <td><form:select path="entity.siteId"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${siteList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>

                        <th class="panel-header">单位账号</th>
                        <td><form:input path="entity.companyAccount"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>
                        <th class="panel-header">单位名称</th>
                        <td><form:input path="companyAccount.unitName"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>


                    </tr>
                    <tr>
                        <th class="panel-header">核定选项</th>

                        <td><form:select path="entity.verificationOptions"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                                         >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${hdItemList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>
                        <%--<th class="panel-header">缴存年月</th>--%>
                        <%--<td><form:input path="startPaymentDate" id="startPaymentDate"--%>
                                        <%--style="width: 120px;height: 27px;"--%>
                                        <%--data-options="required: true"--%>
                                        <%--class="Wdate"--%>
                                        <%--onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"--%>
                                        <%--validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>至--%>
                            <%--<form:input path="endPaymentDate" id="endPaymentDate"--%>
                                        <%--style="width: 120px;height: 27px;"--%>
                                        <%--data-options="required: true"--%>
                                        <%--class="Wdate"--%>
                                        <%--onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"--%>
                                        <%--validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>--%>
                        <%--</td>--%>
                        <th class="panel-header">核定年月</th>
                        <td><form:input path="startReceivedDate"
                                        data-options="required: true"
                                        style="width:120px;height: 27px;" id="startReceivedDate"
                                        class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input>至
                            <form:input path="endReceivedDate" id="endReceivedDate"
                                        style="width: 120px;height: 27px;"
                                        data-options="required: true"
                                        class="Wdate"
                                        onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"
                                        validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
                        </td>

                    </tr>
				</table>
            </form:form>
		</span>

    <table>
        <tr>
            <td height="30px">
            <td>
        </tr>
    </table>
</div>

<table id="payininfo_list_datagrid"></table>
<div id="payininfo_form_dialog">Dialog Content.</div>
<div id="payininfo_confirm_form_dialog">

</div>
