<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    $(function (){

        var court = $("#court${command.freezeFlag}").val();
        if(court==""){
            alert(2);
            $("#court${command.freezeFlag}").attr("disabled", true);
        }

        $('#FreezePeriod${command.freezeFlag}').bind('input propertychange', function() {
            var FreezePeriod = $("#FreezePeriod${command.freezeFlag}").val();
            var startTime = $("#startTime${command.freezeFlag}").val();
            var endTime = $("#endTime${command.freezeFlag}").val();

            var year= parseInt(FreezePeriod)
            var d1=new Date(startTime);
            var d2=new Date(d1);
            d2.setFullYear(d2.getFullYear()+year);
            $("#endTime${command.freezeFlag}").val(d2.getFullYear()+"-"+(d2.getMonth()+1)+"-"+d2.getDate());
        });


    });




</script>

<div class="div_center">
    <form:form name="freeze_examine_form${command.freezeFlag}" id="freeze_examine_form${command.freezeFlag}" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.id" id="freeze_entity_id"/>
        <form:hidden path="entity.personId" id="freeze_personId"/>
        <form:hidden path="entity.person.id" id="freeze_person_id"/>
        <form:hidden path="entity.person.company.id" id="freeze_person_company_id" value="1267"/>
        <%--   <form:hidden path="freezeFlag" id="freeze_flag" value="${freezeFlag}"/>--%>


        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
                <th style="width: 20%" class="panel-header">证件号码啊</th>
                <td><form:input path="entity.person.idCardNumber" id="freeze_idCardNumber${command.freezeFlag}"
                                readonly="true"
                                style="width:184px;height: 27px;"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">个人账号</th>
                <td><form:input path="entity.person.personalAccount" id="personalAccount${command.freezeFlag}"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>

                <th class="panel-header">姓名</th>
                <td><form:input path="entity.person.name" id="freezeName${command.freezeFlag}"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">单位账号</th>
                <td><form:input path="entity.person.company.gbCompanyAccount"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox" id="gbCompanyAccount"
                                validType=""></form:input></td>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.person.company.gbCompanyName"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox" id="gbCompanyName"
                                validType=""></form:input></td>
            </tr>
            <tr>
                <c:if test="${freezeFlag=='1' || freezeFlag=='2'}">
                    <th class="panel-header">冻结原因</th>
                    <td><form:select path="entity.freezeReasonId"
                                     style="width:184px;height: 27px;"
                                     class="form_view_input combo easyui-combobox"
                                     data-options="required: true, editable: false"
                                     validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                        <form:option value="" >--请选择--</form:option>
                        <form:options items="${freezeReasonList}" itemValue="id" itemLabel="name"/>
                    </form:select></td>
                </c:if>
                <c:if test="${freezeFlag=='3'}">
                    <th class="panel-header">解冻原因</th>
                    <td><form:select path="entity.unfreezeReasonId"
                                     style="width:184px;height: 27px;"
                                     class="form_view_input combo easyui-combobox"
                                     data-options="required: true, editable: false"
                                     validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                        <form:option value="" >--请选择--</form:option>
                        <form:options items="${unfreezeReasonList}" itemValue="id" itemLabel="name"/>
                    </form:select></td>
                </c:if>
                    <%--<th><form:checkbox path="entity.courtName" value="entity.courtNameId"  id="courtNamecheckbox_id"></form:checkbox></th>--%>

                <th class="panel-header">法院名称</th>
                <td><form:select path="entity.courtNameId"
                                 readonly="true"
                                 id="courtName${command.freezeFlag}"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${courtNameList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
            </tr>
            <tr>
                <th class="panel-header" style="text-align:right;"  colspan="3">非本市法院名称</th>
                <td style="text-align:right;" colspan="3">
                    <form:input path="entity.court" id="court${command.freezeFlag}"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                style="width:184px;height: 27px;"
                                validType=""></form:input>
                </td>
            </tr>
            <tr>
                <th class="panel-header">执行案号</th>
                <td><form:input path="entity.caseNumber" id="caseNumber${command.freezeFlag}"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                style="width:184px;height: 27px;"
                                validType=""></form:input>
                </td>
                <th class="panel-header">法官姓名</th>
                <td><form:input path="entity.judgeName" id="judgeName${command.freezeFlag}"
                                readonly="true"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                style="width:184px;height: 27px;"
                                validType=""></form:input>
                </td>
            </tr>
            <tr>
                <th class="panel-header">联系方式</th>
                <td><form:input path="entity.contactInfo"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="contactInfo${command.freezeFlag}"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
                <th class="panel-header">冻结金额</th>
                <td><form:input path="entity.amount"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="amount${command.freezeFlag}"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">受理日期</th>
                <td><form:input path="entity.startTime"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="startTime${command.freezeFlag}"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
                <th class="panel-header">冻结期限</th>
                <td><form:input path="entity.freezePeriod"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="FreezePeriod${command.freezeFlag}"
                                class="form_view_input combo easyui-validatebox"
                                validType=""></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">冻结日期</th>
                <td><form:input path="entity.endTime"
                                readonly="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="endTime${command.freezeFlag}"
                                class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input></td>
            </tr>


            <tr>
                <th class="panel-header">备注</th>
                <td  rowspan="2" colspan="3"><form:input path="entity.remark" id="remark${command.freezeFlag}"
                                                         style="width:99.7%;height: 40px"
                                                         class="form_view_input combo easyui-validatebox"
                                                         data-options="multiline:true"
                                                         validType="complexValid['','']"></form:input>
            </tr>
        </table>
    </form:form>
</div>
