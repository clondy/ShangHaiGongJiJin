<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="personTransferInfo" items="${PersonList}" varStatus="status">
    {
    "id":"${personTransferInfo.id}",
    "personalAccount":"${personTransferInfo.personalAccount}",
    "name":"${personTransferInfo.name}",
    "idCardTypeId":"${personTransferInfo.idCardTypeId}",
    "idCardNumber":"${personTransferInfo.idCardNumber}",
    "postCode":"${personTransferInfo.postCode}",
    "gbeducation":"${personTransferInfo.gbeducation}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}