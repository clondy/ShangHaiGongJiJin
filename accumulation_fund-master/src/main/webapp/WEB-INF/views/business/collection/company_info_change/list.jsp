<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="companyHistory" items="${CompanyHistoryList}" varStatus="status">
    {"id": "${companyHistory.id}",
    "gbCompanyAccount": "${companyHistory.gbCompanyAccount}",
    "gbCompanyName": "${companyHistory.gbCompanyName}",
    "confirmerId": "${companyHistory.confirmerId}",
    "confirmationTime": "${companyHistory.confirmationTime}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]

}