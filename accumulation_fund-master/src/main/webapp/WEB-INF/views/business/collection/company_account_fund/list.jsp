<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="companyAccountFund" items="${CompanyAccountFundList}" varStatus="status">
    {"id": "${companyAccountFund.id}",
    "companyId": "${companyAccountFund.companyId}",
    "gbCompanyAccount": "${companyAccountFund.company.gbCompanyAccount}",
    "companyAccount": "${companyAccountFund.companyAccount}",
    "unitName": "${companyAccountFund.unitName}",
    "gbCompanyCloseReason": "${companyAccountFund.gbCompanyCloseReason}",
    "depositRateMark": "${companyAccountFund.depositRateMark.name}",
    "natureAccount": "${companyAccountFund.company.natureAccount.name}",
    "gbCompanyPostCode": "${companyAccountFund.company.gbCompanyPostCode}",
    "gbCompanyAddress": "${companyAccountFund.company.gbCompanyAddress}",
    "paymentSite": "${companyAccountFund.company.paymentSite}",
    "startPaymentDate": "${companyAccountFund.startPaymentDate}",
    "gbCompanyName": "${companyAccountFund.company.gbCompanyName}",
    "depositRateMarkId": "${companyAccountFund.depositRateMarkId}",
    "companyPaymentRatio": "${companyAccountFund.companyPaymentRatio}",
    "personPaymentRatio": "${companyAccountFund.personPaymentRatio}",
    "contactName": "${companyAccountFund.company.contactName}",
    "contactIdCardTypeId": "${companyAccountFund.company.contactIdCardTypeId}",
    "contactIdCardNumber": "${companyAccountFund.company.contactIdCardNumber}",
    "contactMobilePhone": "${companyAccountFund.company.contactMobilePhone}",
    "notes": "${companyAccountFund.notes}",
    "gbCompanyCloseReason": "${companyAccountFund.gbCompanyCloseReason}",
    "companyCloseDate": "${companyAccountFund.companyCloseDate}",
    "companyAddress": "${companyAccountFund.company.companyAddress}",
    "creditCode": "${companyAccountFund.company.creditCode}",
    "companyCloseReasonId": "${companyAccountFund.companyCloseReasonId}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}