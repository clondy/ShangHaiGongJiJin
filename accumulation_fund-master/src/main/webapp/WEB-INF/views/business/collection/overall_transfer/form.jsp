<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script>

    //定义查询返回的json数据
    var overallTransfer;
    $("#company_search_Info").click(function () {
        var code = $("#turnOutCompanyAccount").val().trim();
        if (code.length== 0) {
            $.messager.alert("提示", "转出单位账号不能为空！")
            return false;
        }
        //查询本地转出单位信息
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/overall_transfer/get_localhost_turnOutCompanyAccount.shtml",
            type: "post",
            data: {turnOutCompanyAccount: code},
            datatype: "json",
            success: function (data) {
                if(!jQuery.isEmptyObject(data)){
                    data = jQuery.parseJSON(data);
                    $('#count').text(data.count);
//                    console.log(data);
//                    var loadData = {};
//                    for (var key in data) {
//                        loadData['entity.company.' + key] = data[key];
//                    }
                    $('#overall_transfer_info_edit_form').form('load', data);
                }

            }
        });
    })


    $("#turnInCompany_search_Info").click(function () {
        var turnInCompanyAccount = $("#turnInCompanyAccount").val().trim();
        if (turnInCompanyAccount.length== 0) {
            $.messager.alert("提示", "转入单位账号不能为空！")
            return false;
        }
        //查询本地职工信息
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/overall_transfer/get_localhost_turnInCompanyAccount.shtml",
            type: "post",
            data: {turnInCompanyAccount: turnInCompanyAccount},
            datatype: "json",
            success: function (data) {
                if(!jQuery.isEmptyObject(data)){
                    data = jQuery.parseJSON(data);

                  /*  console.log(data);
                    var loadData = {};
                    for (var key in data) {
                        loadData['entity.' + key] = data[key];
                    }*/
                    $('#overall_transfer_info_edit_form').form('load', data);
                }

            }
        });
    })

</script>
<div class="div_center" style="margin-top: 2px;">
    <div class="text clearfix row" style="text-align:center;height: 80px;float: left">
		<span style="float:left;">
    <form:form name="person_transfer_info_edit_form" id="overall_transfer_info_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">

        <form:hidden path="entity.id" id="" />
        <form:hidden path="" id="" />

        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
            <tr>
                <th class="panel-header">转出单位账号</th>
                <td><form:input path="turnOutCompanyAccount"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="turnOutCompanyAccount"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
               <%-- <th class="panel-header">账户类型</th>
                        <td><form:select path="" id="options"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                        >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${hdItemList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>--%>
                <td style="text-align:left;" colspan="2">
                    <a href="#" class="easyui-linkbutton" id="company_search_Info">查询</a></td>
            </tr>
            <tr>
                <th class="panel-header">转出单位名称</th>
                <td><form:input path="turnOutCompanyName" id="gbCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                readonly="true"
                                class="form_view_input combo easyui-validatebox"
                                ></form:input></td>
                <th class="panel-header">转出缴存年月</th>
                <td><form:input path="turnOutPaymentDate" id="companyPaymentDate"
                                style="width: 184px;height: 27px;"
                                data-options="required: true"
                                readonly="true"
                                class="Wdate"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"
                                validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">转入单位账号</th>
                <td><form:input path="turnInCompanyAccount" id="turnInCompanyAccount"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                style="width:99.7%;height: 27px;"
                ></form:input>
                </td>
                <%--<th class="panel-header">账户类型</th>
                        <td><form:select path="" id="options"
                                         style="width:184px;height: 27px;"
                                         class="form_view_input combo easyui-combobox"
                                         data-options="required: true, editable: false"
                                         validType="complexValid['^--请选择--$','必填项！', '', '', 1]"
                        >
                            <form:option value="" >--请选择--</form:option>
                            <form:options items="${hdItemList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>--%>
                 <td style="text-align:left;" colspan="2">
                    <a href="#" class="easyui-linkbutton" id="turnInCompany_search_Info">查询</a></td>
            </tr>
            <tr>
                <th class="panel-header">转入单位名称</th>
                <td><form:input path="turnInCompanyName" id="turnInCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
                <th class="panel-header">转入缴存年月</th>
                <td><form:input path="turnInPaymentDate" id="turnInCompanyPaymentDate"
                                style="width: 184px;height: 27px;"
                                data-options="required: true"
                                class="Wdate"
                                readonly="readonly"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM',readOnly:true,minDate:'%y-%M-%D'})"
                                validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input></td>
            </tr>
        </table>
    </form:form>
    </span>

        <table>
            <tr>
                <td height="30px">
                <td>
            </tr>
        </table>
    <table class="form_view_border" bordercolordark="#FFFFFF"
           bordercolorlight="#45b97c" border="1px" cellpadding="0"
           cellspacing="0" style="">
        <tr>

            <th class="panel-header">本次转移总人数</th>
            <td><span  id="count"
                       style="width:100px;height: 27px;"/></td>

        </tr>

    </table>
    </div>
</div>
