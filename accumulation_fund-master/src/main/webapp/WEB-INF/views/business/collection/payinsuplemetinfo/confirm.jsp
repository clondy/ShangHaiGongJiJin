<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var PayinSuplementItemService;
    $(function () {

        PayinSuplementItemService = new $.BaseService('#payinSuplementItem_query_form', '#payinSuplementItem_list_datagrid'
            , '#payinSuplementItem_form_dialog', '#payinSuplementItem_edit_form'
            , {
                listUrl:'payinsupitem_list.shtml',
                actionRootUrl: '<c:url value="/business/collection/payinsuplemetinfo/" />'
                , entityTitle: "补缴人员"
                , datagridSearchButtonId: 'search_recovery_record'
                , datagridResetButtonId: 'reset_search_recovery_page'
                , datagridToolbar: [{
                    text: '录入补缴人员',
                    iconCls: 'icon-add',
                    handler: function () {
                        //第四个form
                        $('#insert_message').dialog({
                            title: '录入人员'
                            , href: "${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/add.shtml"
                            , width: 600
                            , height: 330
                            , closed: true
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [{
                                text: '保存'
                                , iconCls: 'icon-save'
                                , handler: function () {
                                    $("#payininfo_hd_form").form('submit', {
                                        url: '${pageContext.request.contextPath}/business/collection/payinsuplemetinfo/add_payinsuplemetinfo.shtml'
                                        , onSubmit: function () {
                                            var isValid = $('#payininfo_hd_form').form('validate');
//                                    if (!isValid) {
//                                        $.messager.progress('close');
//                                    }
                                            return isValid;
                                        }
                                        , success: function (data) {
                                            // $.messager.progress('close');
                                            console.log(data);
                                            data = jQuery.parseJSON(data);
                                            if (data.result ) {
                                                payininfouncheckshow();
                                                $.messager.alert('通知', "核定成功！");

                                            } else {
                                                $.messager.alert('通知', "核定失败！");
                                            }
                                        }
                                    });

                                }
                            }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                        $.messager.progress('close');
                                        return false;
                                    }
                                }]
                            , modal: true
                        }).dialog('open');
                    }
                }, {
                    text: '核定确认',
                    iconCls: 'icon-save',
                    handler: function () {

                        alert("核定启封信息")
                    }
                }


                ]
                , datagridPrintId: 'print_recovery_record_list'
                , datagridColumns: [
                    {field: 'fundOption', title: '流水号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'reasonRecordName', title: '启封原因', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFund', title: '单位账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFundUnitName', title: '单位名称', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personalAccount', title: '个人账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personName', title: '姓名', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personGbidCardType', title: '证件类型', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personIdCardNumber', title: '证件号码', width: 100, rowspan: 2, align: 'center'},
                    {field: 'wage', title: '工资收入', width: 100, rowspan: 2, align: 'center'},
                    {field: 'monthPayment', title: '月缴存额', width: 100, rowspan: 2, align: 'center'},
                    {field: 'dateTime', title: '启封日期', width: 100, rowspan: 2, align: 'center'},
                    {field: 'operatorName', title: '操作员', width: 100, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 500
//                , dialogButtons: [
//                    {
//                        text: '取消',
//                        iconCls: 'icon-cancel',
//                        handler: function () {
//                            $($(this).context.parentElement.parentElement).dialog('close');
//                        }
//                    }
//                ]
            });

    });

</script>

<div style="margin:10px 0;"></div>


<table id="payinSuplementItem_list_datagrid"></table>
<div id="insert_message"></div>
