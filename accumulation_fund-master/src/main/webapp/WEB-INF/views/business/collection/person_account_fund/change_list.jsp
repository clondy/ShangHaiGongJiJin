<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="personHistory" items="${PersonHistoryList}" varStatus="status">
    {"id":"${personHistory.id}",
    "personalAccount": "${personHistory.personalAccount}",
    "name": "${personHistory.name}",
    "birthYearMonth": "${personHistory.birthYearMonth}",
    "idCardNumber": "${personHistory.idCardNumber}",
    "operatorId": "${personHistory.operatorId}",
    "dateTime": "${personHistory.dateTime}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}