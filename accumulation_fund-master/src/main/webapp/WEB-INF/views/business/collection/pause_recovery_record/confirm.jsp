<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var PauseRecoveryRecordService;
    $(function () {

        PauseRecoveryRecordService = new $.BaseService('#recovery_query_form${command.pauseRecoveryMiddleFlag }', '#recovery_list_datagrid${command.pauseRecoveryMiddleFlag}'
            , '#recovery_form_dialog', '#recovery_edit_form'
            , {
                listUrl:'searchMiddleRecoryByAccountAndFundOptionId.shtml?pauseRecoveryFlag=${command.pauseRecoveryMiddleFlag}',
                actionRootUrl: '<c:url value="/business/collection/pause_recovery_record/" />'
                , entityTitle: "启封人员"
                , optionFormatter: function (value, rec) {
                    return '<a href="#" title="查看详情" onClick="PauseRecoveryRecordService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + PauseRecoveryRecordService.actionRootUrl + PauseRecoveryRecordService.formUrl + '&entity.id=' + rec.id + '\');" iconCls="icon-search" class="easyui-linkbutton" data-options="plain: true"></a>';
                }
                , datagridSearchButtonId: 'search_recovery_record'
                , datagridResetButtonId: 'reset_search_recovery_page'
                , datagridToolbar: [{
                    text: '录入启封人员',
                    iconCls: 'icon-add',
                    handler: function () {
                        //第四个form
                        $('#insert_message').dialog({
                            title: '录入人员'
                            , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_record/insert_message.shtml"
                            , width: 600
                            , height: 330
                            , closed: true
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [{
                                text: '确认'
                                , iconCls: 'icon-save'
                                , handler: function () {
                                    /*//添加录入信息
                                    $('#company_review_form').form('submit', {
                                        url: '${pageContext.request.contextPath}/business/collection/pause_recovery_record/insert_message.shtml',
                                        onSubmit: function () {
                                            var reviewIsVal = $('#company_review_form').form('validate');
                                            if (!reviewIsVal) {
                                                $.messager.progress('close');
                                            } else {
                                                if (!confirm("要执行“确认”操作吗？？？")) {
                                                    $('#insert_message').dialog('close')

                                                }
                                            }
                                            return reviewIsVal
                                        },
                                        success: function (data) {
                                            data = jQuery.parseJSON(data);
                                            console.log(data)
                                            if (data.result) {
                                                $('#insert_message').dialog('close');
                                                //刷新或打开第三个页面
                                                $('#recovery_confirm_list_dialog').dialog('close');
                                                $('#recovery_confirm_list_dialog').dialog({
                                                    title: '确认启封信息'
                                                    , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_record/confirm.shtml"
                                                    , width: 980
                                                    , height: 420
                                                    , closed: true
                                                    , cache: false
                                                    , resizable: true
                                                    , collapsible: true
                                                    , maximizable: true
                                                    , buttons: [{
                                                        text: '取消',
                                                        iconCls: 'icon-cancel',
                                                        handler: function () {
                                                            $($(this).context.parentElement.parentElement).dialog('close');
                                                            $.messager.progress('close');
                                                            return false;
                                                        }
                                                    }]
                                                    , modal: true
                                                }).dialog('open');

                                            } else {
                                                $.messager.alert('通知', "录入值有误！");
                                                $.messager.progress('close');
                                            }
                                        }
                                    });*/

                                }
                            }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                        $.messager.progress('close');
                                        return false;
                                    }
                                }]
                            , modal: true
                        }).dialog('open');
                    }
                }, {
                    text: '确认启封信息',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        alert("核定启封信息")
                    }
                }


                ]
                , datagridPrintId: 'print_recovery_record_list'
                , datagridColumns: [
                    {field: 'fundOption', title: '启封选项', width: 100, rowspan: 2, align: 'center'},
                    {field: 'reasonRecordName', title: '启封原因', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFund', title: '单位账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFundUnitName', title: '单位名称', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personalAccount', title: '个人账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personName', title: '姓名', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personGbidCardType', title: '证件类型', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personIdCardNumber', title: '证件号码', width: 100, rowspan: 2, align: 'center'},
                    {field: 'wage', title: '工资收入', width: 100, rowspan: 2, align: 'center'},
                    {field: 'monthPayment', title: '月缴存额', width: 100, rowspan: 2, align: 'center'},
                    {field: 'dateTime', title: '启封日期', width: 100, rowspan: 2, align: 'center'},
                    {field: 'operatorName', title: '操作员', width: 100, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 500
                , dialogButtons: [
                    {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });

    });

</script>

<div style="margin:10px 0;"></div>


<table id="recovery_list_datagrid${command.pauseRecoveryMiddleFlag }"></table>
<div id="insert_message"></div>
