<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>



<div class="div_center" style="margin-top: 90px">
    <div class="text clearfix" style="text-align:center;height: 80px">
		<span style="float:left;">
			<form:form name="payininfo_edit_form" id="payininfo_edit_form"
                       method="post" action="" onsubmit="return false;">

                <table class="form_view_border" bordercolordark="#FFFFFF"
                       bordercolorlight="#45b97c" border="1px" cellpadding="0"
                       cellspacing="0" style="">
                    <tr>
                        <th class="panel-header">单位账号</th>
                        <td><form:input path="companyAccountFund.companyAccount" id="companyAccount"
                                        class="form_view_input combo easyui-validatebox" required="true"  style="width:184px;height: 27px;"/></td>

                        <th class="panel-header">单位名称</th>
                        <td><form:input path="companyAccountFund.unitName" id="unitName"
                                        class="form_view_input combo easyui-validatebox" required="true"  style="width:184px;height: 27px;"/></td>



                    </tr>
                    <tr>
                        <th class="panel-header">个人账号</th>
                        <td><form:input path="person.personalAccount" id="personalAccount"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>
                        <th class="panel-header">姓名</th>
                        <td><form:input path="person.name" id="name"
                                        class="form_view_input combo easyui-validatebox"  style="width:184px;height: 27px;"/></td>


                    </tr>

                </table>
            </form:form>
		</span>

        <table>
            <tr>
                <td height="30px">
                <td>
            </tr>
        </table>
    </div>

</div>
