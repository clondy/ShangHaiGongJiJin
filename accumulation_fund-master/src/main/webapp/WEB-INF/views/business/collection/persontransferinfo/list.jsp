<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="personTransferInfo" items="${PersonTransferInfoList}" varStatus="status">
    {
    "id":"${personTransferInfo.id}",
    "turnInCompanyAccount":"${personTransferInfo.turnInCompanyAccount}",
    "turnOutCompanyAccount":"${personTransferInfo.turnOutCompanyAccount}",
    "transfromDate":"${personTransferInfo.transfromDate}",
    "personalAccount":"${personTransferInfo.personalAccount}",
    "bussinessState":"${personTransferInfo.bussinessState}",
    "businessType":"${personTransferInfo.businessType}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}