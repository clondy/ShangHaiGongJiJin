<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
		   uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    <%--var data = ${personAccountFund};--%>

    <%--if(data.result){--%>
        <%--$('#person_edit_form').form('load', data);--%>
    <%--}--%>

    //校验单位账号是否存在
    var validate_person_account = {
        getValidateUrl: function () {
            url: "${pageContext.request.contextPath}/business/collection/company/get_company.shtml"
        }
        , getValidateParameter: function (value) {
            var value = jQuery.trim(value);
            if (value) {
                return {'entity.gbCompanyAccount': value};
            }
            return {}
        }
        , validator: function (data) {
            //因为当相同的登录名已存在时，服务器端会返回true，反之返回false，故此这里会这样执行一次取反操作。
            data = jQuery.parseJSON(data);
            return !data.result;
        }
    }

</script>

<div class="div_center" style="margin-top: 0px">
	<form:form name="person_edit_form" id="person_edit_form" method="post" style="width:600px"
			   action="" onsubmit="return false;">
		<form:hidden path="entity.companyAccountFundId" id="company_account_fund_id"/>
		<form:hidden path="extra" id="extra"/>
		<form:hidden path="isFirstSave" id="isFirstSave"/>
		<form:hidden path="entity.id" id="supplementEntityId"/>
		<table class="form_view_border" bordercolordark="#45b97c" bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0">
			<tr>
				<th style="width: 20%" class="panel-header">个人账号</th>
				<td><form:input path="entityPerson.personalAccount" id="personalAccount"
								data-options="required: true"
								style="width:184px;height: 27px;"
								class="form_view_input combo easyui-validatebox"
								validType="'','','validate_person_account','个人账号不存在！','']"></form:input></td>
				</td>
				<td style="text-align:left;" colspan="2"><a href="#" class="easyui-linkbutton" id="query_person_supplement_account">查询</a></td>
			</tr>
			<tr>
				<th style="width: 20%" class="panel-header">单位账号</th>
				<td><form:input path="entityPerson.company.gbCompanyAccount" id="gbCompanyAccount" disabled="true"
								data-options="required: true"
								style="width:184px;height: 27px;"
								class="form_view_input combo easyui-validatebox"
								validType="complexValid['^[0-9]{13}$','单位账号输入有误！','validate_company_account','单位账号不存在！','']"></form:input></td>
					<%--</form:input></td>--%>
				</td>
				<th class="panel-header">单位名称</th>
				<td><form:input path="entityPerson.company.gbCompanyName" id="gbCompanyName" disabled="true"
								data-options="required: true"
								style="width:184px;height: 27px;"
								class="form_view_input combo easyui-validatebox"
								validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">证件类型</th>
				<td><form:select path="entityPerson.idCardTypeId" id="idCardType" disabled="true"
								 style="width:184px;height: 27px;"
								 class="form_view_input combo easyui-combobox"
								 data-options="required: true, editable: false"
								 validType="">
					<form:option value="" >--请选择--</form:option>
					<form:options items="${legalPersonIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
				</form:select></td>
				<th class="panel-header">证件号码</th>
				<td><form:input path="entityPerson.idCardNumber" disabled="true"
								style="width:184px;height: 27px;"
								id="idCardNumber"
								data-options=""
								class="form_view_input combo easyui-validatebox"
								validType=""></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">姓名</th>
				<td><form:input path="entityPerson.name" id="name" disabled="true"
								data-options=""
								style="width:184px;height: 27px;"></form:input></td>
				<th class="panel-header">出生日期</th>
				<td><form:input path="entityPerson.birthYearMonth" disabled="true"
								data-options=""
								style="width:184px;height: 27px;"
								class="form_view_input combo easyui-validatebox"
								id="birthYearMonth"></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">性别</th>
				<td><form:select path="entityPerson.sexId" id="sex" disabled="true"
								 style="width:184px;height: 27px;"
								 class="form_view_input combo easyui-combobox"
								 data-options="required: true, editable: false"
								 validType="">
					<form:option value="" >--请选择--</form:option>
					<form:options items="${sexList}" itemValue="id" itemLabel="name"/>
				</form:select></td>
				<th class="panel-header">户籍</th>
				<td><form:input path="entityPerson.koseki" disabled="true"
								data-options="required: true"
								style="width:184px;height: 27px;"
								class="form_view_input combo easyui-validatebox" id="koseki"></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">工资收入</th>
				<td colspan="1"><form:input path="entityPerson.wageIncome" id="wageIncome" disabled="true"
											data-options="required: true"
											class="easyui-numberspinner"
											style="width:99.7%;height: 27px;"></form:input>
				</td>
				<th class="panel-header">缴存率标志</th>
				<td><form:select path="entity.companyAccountFund.depositRateMarkId"
								 style="width: 184px;height: 27px;"
								 class="form_view_input combo easyui-combobox"
								 data-options="required: true, editable: false"
								 validType="" disabled="true">
					<form:option value="" >--请选择--</form:option>
					<form:options items="${depositRateMarkIdList}" itemValue="id" itemLabel="name"/>
				</form:select>
				</td>
			</tr>
			<tr>
				<th class="panel-header">单位缴存比例(%)</th>
				<td><form:input path="entity.companyPaymentRatio" id="companyPaymentRatio"
								style="width: 186px;height: 27px;"
								data-options="required: true,increment:1,min:1,max:5"
								class="easyui-numberspinner"></form:input></td>
				<th class="panel-header">个人缴存比例(%)</th>
				<td><form:input path="entity.personPaymentRatio" id="personPaymentRatio"
								style="width: 184px;height: 27px;"
								data-options="required: true,increment:1,min:1,max:5"
								class="easyui-numberspinner" ></form:input></td>
			</tr>
			<tr>
				<th class="panel-header">月缴存额</th>
				<td colspan="3"><form:input path="entity.monthPay"
											data-options="required: true"
											style="width:184px;height: 27px;" id="monthPay"
											class="easyui-numberspinner"></form:input></td>
			</tr>
		</table>
	</form:form>
</div>
<div>
	<input id="nameTemp" type="hidden"/>
	<input id="idCardNumberTemp" type="hidden"/>
	<input id="personalAccountTemp" type="hidden"/>
	<input id="CompanyNameTemp" type="hidden"/>
	<input id="stateTemp" type="hidden"/>
</div>

<div id="system_account_infomation">

</div>