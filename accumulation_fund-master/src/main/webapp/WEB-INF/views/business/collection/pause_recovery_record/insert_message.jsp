<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
		   uri="http://www.springframework.org/security/tags" %>
<div class="div_center">
	<form:form id="insert_message"
			   method="post" action="" data-dojo-type="dijit/form/Form"
			   onsubmit="return false;">

		<table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
			   bordercolorlight="#45b97c" border="1px" cellpadding="0"
			   cellspacing="0">
			<tr height="30">
				<th class="panel-header">单位账号</th>
				<td class="page_input">
					<input type="text" id="companyAccountFundId" name="companyAccountFundId"
						   class="form_view_input combo easyui-validatebox"
						   readonly="readonly"
						   data-options="required: true" style="height: 27px;">
				</td>
				<th class="panel-header">单位名称</th>
				<td class="page_input">
					<input type="text" id="gbCompanyName" name="gbCompanyName"
						   class="form_view_input combo easyui-validatebox"
						   readonly="readonly"
						   data-options="required: true" style="height: 27px;">
				</td>
			</tr>
			<tr height="30">
				<th class="panel-header">单位缴存比例</th>
				<td class="page_input">
					<input type="text" id="companyPaymentRatio" name="companyPaymentRatio"
						   class="form_view_input combo easyui-validatebox"
						   readonly="readonly"
						   data-options="required: true" style="height: 27px;">
				</td>
				<th class="panel-header">个人缴存比例</th>
				<td class="page_input">
					<input type="text" id="personPaymentRatio" name="personPaymentRatio"
						   class="form_view_input combo easyui-validatebox"
						   readonly="readonly"
						   data-options="required: true" style="height: 27px;">
				</td>
			</tr>
			<tr height="30">
				<th class="panel-header">证件类型</th>
				<td class="page_input"><select path="gbidCardType"
											   style="width:184px;height: 27px;"
											   data-options="required: true,editable: false,valueField:'id',textField:'text',url:'<c:url value="/business/collection/pause_recovery_record/searchListBySortId.shtml?sortId=7"/>'"
											   name="gbidCardType"
											   class="form_view_input combo easyui-combobox">

				</select></td>
				<th class="panel-header">证件号码</th>
				<td class="page_input">
					<input type="text" id="idCardNumber" name="idCardNumber"
						   class="form_view_input combo easyui-validatebox"
						   data-options="required: true" style="height: 27px;">
				</td>
			</tr>
			<tr height="30">
				<th class="panel-header">个人账号</th>
				<td class="page_input">
					<input type="text" id="personalAccount" name="personalAccount"
						   class="form_view_input combo easyui-validatebox"
						   data-options="required: true" style="height: 27px;">
				</td>
				<th class="panel-header">姓名</th>
				<td class="page_input">
					<input type="text" id="name" name="name"
						   class="form_view_input combo easyui-validatebox"
						   data-options="required: true" style="height: 27px;">
				</td>
			</tr>
			<tr height="30">
				<th class="panel-header">工资收入</th>
				<td class="page_input">
					<input type="text" id="wage" name="wage"
						   class="form_view_input combo easyui-validatebox"
						   data-options="required: true" style="height: 27px;">
				</td>
				<th class="panel-header">月缴存额</th>
				<td class="page_input">
					<input type="text" id="monthPayment" name="monthPayment"
						   class="form_view_input combo easyui-validatebox"
						   data-options="required: true" style="height: 27px;">
				</td>
			</tr>
			<tr height="30">
				<th class="panel-header">启封原因</th>
				<td><select path="reasonRecordId"
							style="width:184px;height: 27px;"
							data-options="required: true,editable: false,valueField:'id',textField:'text',url:'<c:url value="/business/collection/pause_recovery_record/searchListBySortId.shtml?sortId=16"/>'"
							name="reasonRecordId"
							class="form_view_input combo easyui-combobox">

				</select></td>
			</tr>
		</table>
	</form:form>
</div>