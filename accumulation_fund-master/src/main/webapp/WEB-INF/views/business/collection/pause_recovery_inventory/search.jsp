<%@ page import="com.capinfo.accumulation.model.collection.PauseRecoveryInventory" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
    var PauseRecoveryRecordService;
    $(function () {

        PauseRecoveryRecordService = new $.BaseService('#recovery_query_form${command.pauseRecoveryInventoryFlag }', '#recovery_list_datagrid${command.pauseRecoveryInventoryFlag }'
            , '#recovery_form_dialog', '#recovery_edit_form'
            , {
                listUrl:'inventory_list.shtml?pauseRecoveryInventoryFlag=${command.pauseRecoveryInventoryFlag}',
                actionRootUrl: '<c:url value="/business/collection/pause_recovery_inventory/" />'
                , entityTitle: <c:if test="${command.pauseRecoveryInventoryFlag==1 }">"停缴清册"</c:if>
                				<c:if test="${command.pauseRecoveryInventoryFlag==2 }">"启封清册"</c:if>
                , optionFormatter: function (value, rec) {
                    return '<a href="#" title="查看详情" onClick="PauseRecoveryRecordService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + PauseRecoveryRecordService.actionRootUrl + PauseRecoveryRecordService.formUrl + '&entity.id=' + rec.id + '\');" iconCls="icon-search" class="easyui-linkbutton" data-options="plain: true"></a>';
                }
                , datagridSearchButtonId: 'search_recovery_record'
                , datagridResetButtonId: 'reset_search_recovery_page'
                , datagridToolbar: [{
                    id: this.datagridSearchButton
                    ,text: '查询'
                    ,iconCls: 'icon-search'
                    , handler: function() {

						/*if($("#entityCompanyAccountFundId").val()!=""){*/
                            $('#recovery_query_form${command.pauseRecoveryInventoryFlag }').submit();
						/*}else{
                            $.messager.alert("通知","单位账号不能为空！");
						}*/
                    }
                }
                , '-'
				, {
                        id: this.datagridAddRecoveryButton
                        ,text: <c:if test="${command.pauseRecoveryInventoryFlag==1 }">'创建停缴清册'</c:if>
                        		<c:if test="${command.pauseRecoveryInventoryFlag==2 }">'创建启封清册'</c:if>
                        ,iconCls: 'icon-add'
                        , handler: function() {

                           /*var companyAccount = $.messager.prompt("请输入单位账户","");*/
                            $('#recovery_search_form_dialog').dialog({
                                title: '创建清册'
                                , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_inventory/inventory_form.shtml"
                                , width: 600
                                , height: 250
                                , closed: true
                                , cache: false
                                , resizable: true
                                , collapsible: true
                                , maximizable: true
                                , buttons: [{
                                    text: '确认'
                                    , iconCls: 'icon-save'
                                    , handler: function () {
                                        //查询单位账号是否正确
                                        $('#company_review_form').form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/pause_recovery_inventory/save_or_update_inventory.shtml?pauseRecoveryInventoryFlag=${command.pauseRecoveryInventoryFlag }',
                                            onSubmit: function () {
                                                // do some check
                                                var reviewIsVal = $('#company_review_form').form('validate');

                                                if (!reviewIsVal) {
                                                    $.messager.progress('close');
                                                } else {
                                                    if (!confirm("确定要创建清册单吗？？")) {
														return false;
                                                    }
                                                }
                                                return reviewIsVal
                                            },
                                            success: function (data) {
                                                data = jQuery.parseJSON(data);
                                                console.log(data);
                                                if (data.result) {
                                                    $('#recovery_search_form_dialog').dialog('close');
                                                    $('#recovery_confirm_list_dialog').dialog({
                                                        title: <c:if test="${command.pauseRecoveryInventoryFlag==1 }">'确认停缴信息'</c:if>
                                                        		<c:if test="${command.pauseRecoveryInventoryFlag==2 }">'确认启封信息'</c:if>
                                                        , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_inventory/confirm.shtml?pauseRecoveryInventoryFlag=${command.pauseRecoveryInventoryFlag }"
                                                        , width: 980
                                                        , height: 420
                                                        , closed: true
                                                        , cache: false
                                                        , resizable: true
                                                        , collapsible: true
                                                        , maximizable: true
                                                        , buttons: [{
                                                                text: '取消',
                                                                iconCls: 'icon-cancel',
                                                                handler: function () {
                                                                    $($(this).context.parentElement.parentElement).dialog('close');
                                                                    $.messager.progress('close');
                                                                    return false;
                                                                }
                                                            }]
                                                        , modal: true
                                                    }).dialog('open');

                                                } else {
                                                    $.messager.alert('通知', "单位账号无效！");
                                                    $.messager.progress('close');
                                                }
                                            }
                                        });

                                    }
                                }
                                    , {
                                        text: '取消',
                                        iconCls: 'icon-cancel',
                                        handler: function () {
                                            $($(this).context.parentElement.parentElement).dialog('close');
                                            $.messager.progress('close');
                                            return false;
                                        }
                                    }]
                                , modal: true
                            }).dialog('open');
						}
					}
				]
                , datagridPrintId: 'print_recovery_record_list'
                , datagridColumns: [
                    {field: 'accumulationFundAccountType', title: '启封选项', width: 150, rowspan: 2, align: 'center'},
                    {field: 'companyAccount', title: '单位账号', width: 220, rowspan: 2, align: 'center'},
                    {field: 'gbCompanyName', title: '单位名称', width: 220, rowspan: 2, align: 'center'},
                    {field: 'createTime', title: '发生日期', width: 220, rowspan: 2, align: 'center'},
                    {field: 'name', title: '操作员', width: 150, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 500
                , dialogButtons: [
                    {
						text: '打印'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if (!confirm("要执行“打印”操作吗？？？")) {
                            $.messager.progress('close');
                            return false;
                        } else {
                            window.location.href = "${pageContext.request.contextPath}/print/index.shtml?" + data.id;
                        }
                    }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });
    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="recovery_query_form" id="recovery_query_form${command.pauseRecoveryInventoryFlag }"
                       method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0" style="">
					<tr>
						<th class="panel-header">单位账号</th>
						<td><form:input id="companyAccountFund.companyAccount" path="companyAccountFund.companyAccount" class="form_view_input combo easyui-validatebox" style="width:184px; height: 27px;"/></td>
                        <th class="panel-header">单位名称</th>
						<td><form:input path="companyAccountFund.unitName" class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
					</tr>
				</table>
            </form:form>
		</span>

	<table>
		<tr><td height="30px"><td></tr>
	</table>

</div>

<table id="recovery_list_datagrid${command.pauseRecoveryInventoryFlag }"></table>
<div id="recovery_form_dialog">Dialog Content.</div>
<div id="recovery_search_form_dialog"></div>
<div id="recovery_confirm_list_dialog"></div>
