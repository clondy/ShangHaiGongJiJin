<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
    var flagRecord=true;
    var PauseRecoveryRecordService;
    $(function () {

        PauseRecoveryRecordService = new $.BaseService('#recovery_query_form${command.pauseRecoveryFlag }', '#recovery_list_datagrid${command.pauseRecoveryFlag }'
            , '#recovery_form_dialog', '#recovery_edit_form'
            , {
                listUrl:'list.shtml?pauseRecoveryFlag=${command.pauseRecoveryFlag}',
                actionRootUrl: '<c:url value="/business/collection/pause_recovery_record/" />'
                , entityTitle: "启封记录"
                , optionFormatter: function (value, rec) {
                    return '<a href="#" title="查看详情" onClick="PauseRecoveryRecordService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + PauseRecoveryRecordService.actionRootUrl + PauseRecoveryRecordService.formUrl + '&entity.id=' + rec.id + '\');" iconCls="icon-search" class="easyui-linkbutton" data-options="plain: true"></a>';
                }
                , datagridSearchButtonId: 'search_recovery_record'
                , datagridResetButtonId: 'reset_search_recovery_page'
                , datagridToolbar: [{
                    id: this.datagridSearchButton
                    ,text: '查询'
                    ,iconCls: 'icon-search'
                    , handler: function() {

						/*if($("#entityCompanyAccountFundId").val()!=""){*/
                            $('#recovery_query_form${command.pauseRecoveryFlag }').submit();
						/*}else{
                            $.messager.alert("通知","单位账号不能为空！");
						}*/
                    }
                }
                <c:if test="${command.pauseRecoveryFlag==3 }">
                , '-'
				, {
                        id: this.datagridAddRecoveryButton
                        ,text: '创建复缴清册'
                        ,iconCls: 'icon-add'
                        , handler: function() {

                           /*var companyAccount = $.messager.prompt("请输入单位账户","");*/
                            $('#recovery_search_form_dialog').dialog({
                                title: '复缴处理'
                                , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_record/add.shtml"
                                , width: 600
                                , height: 250
                                , closed: true
                                , cache: false
                                , resizable: true
                                , collapsible: true
                                , maximizable: true
                                , buttons: [{
                                    text: '复缴'
                                    , iconCls: 'icon-save'
                                    , handler: function () {
                                        //查询单位账号是否正确
                                        $('#company_review_form').form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/pause_recovery_record/check.shtml',
                                            onSubmit: function () {
                                                // do some check
                                                var reviewIsVal = $('#company_review_form').form('validate');

                                                if (!reviewIsVal) {
                                                    $.messager.progress('close');
                                                } else {

                                                    if (!confirm("要执行“复缴”操作吗？？？")) {
                                                     //   $('#recovery_search_form_dialog').dialog('close')
                                                        flagRecord=false;
                                                    }
                                                }
                                                return reviewIsVal
                                            },
                                            success: function (data) {
                                                data = jQuery.parseJSON(data);
                                                console.log(data)
                                                if (data.result) {
                                                    $('#recovery_search_form_dialog').dialog('close');

                                                    $('#recovery_confirm_list_dialog').dialog({
                                                        title: '确认启封信息'
                                                        , href: "${pageContext.request.contextPath}/business/collection/pause_recovery_record/confirm.shtml"
                                                        , width: 980
                                                        , height: 420
                                                        , closed: true
                                                        , cache: false
                                                        , resizable: true
                                                        , collapsible: true
                                                        , maximizable: true
                                                        , buttons: [{
                                                                text: '取消',
                                                                iconCls: 'icon-cancel',
                                                                handler: function () {
                                                                    $($(this).context.parentElement.parentElement).dialog('close');
                                                                    $.messager.progress('close');
                                                                    return false;
                                                                }
                                                            }]
                                                        , modal: true
                                                    }).dialog('open');

                                                } else {
                                                    $.messager.alert('通知', "单位账号无效！");
                                                    $.messager.progress('close');
                                                }
											}
                                        });

                                    }
                                }
                                    , {
                                        text: '取消',
                                        iconCls: 'icon-cancel',
                                        handler: function () {
                                            $($(this).context.parentElement.parentElement).dialog('close');
                                            $.messager.progress('close');
                                            return false;
                                        }
                                    }]
                                , modal: true
                            }).dialog('open');


						}
					}
				</c:if>
                <c:if test="${command.pauseRecoveryFlag==1 }">
                , '-'
                , {
                    id: this.datagridAddPauseButton
                    ,text: '创建停缴单'
                    ,iconCls: 'icon-add'
                    , handler: function() {
                            var companyAccount = $.messager.prompt("请输入单位账户","");

                    }
                }
                </c:if>


				]
                , datagridPrintId: 'print_recovery_record_list'
                , datagridColumns: [
                    {field: 'fundOption', title: '启封选项', width: 100, rowspan: 2, align: 'center'},
                    {field: 'reasonRecordName', title: '启封原因', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFund', title: '单位账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyAccountFundUnitName', title: '单位名称', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personalAccount', title: '个人账号', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personName', title: '姓名', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personGbidCardType', title: '证件类型', width: 100, rowspan: 2, align: 'center'},
                    {field: 'personIdCardNumber', title: '证件号码', width: 100, rowspan: 2, align: 'center'},
                    {field: 'wage', title: '工资收入', width: 100, rowspan: 2, align: 'center'},
                    {field: 'monthPayment', title: '月缴存额', width: 100, rowspan: 2, align: 'center'},
                    {field: 'dateTime', title: '启封日期', width: 100, rowspan: 2, align: 'center'},
                    {field: 'operatorName', title: '操作员', width: 100, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 500
                , dialogButtons: [
                    {
						text: '打印'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if (!confirm("要执行“打印”操作吗？？？")) {
                            $.messager.progress('close');
                            return false;
                        } else {
                            window.location.href = "${pageContext.request.contextPath}/print/index.shtml?" + data.id;
                        }
                    }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });

       /* $('#recovery_query_form_search_button').click(function () {
            $('#recovery_query_form').submit();
        });*/
    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="recovery_query_form" id="recovery_query_form${command.pauseRecoveryFlag }"
                       method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0" style="">
					<tr>
						<th class="panel-header">单位账号</th>
						<td><form:input id="entityCompanyAccountFundId" path="entity.companyAccountFundId" class="form_view_input combo easyui-validatebox" data-options="required: true" style="width:184px; height: 27px;"/></td>
                        <th class="panel-header">单位名称</th>
						<td><form:input path="gbCompanyName" class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
						<th class="panel-header">启封选项</th>
						<td><form:select path="entity.fundOptionId"
										 style="width:184px;height: 27px;"
										 data-options="editable: false"
										 class="form_view_input combo easyui-combobox">
							<form:option value="">--请选择--</form:option>
							<form:options items="${fundOptionList}" itemValue="id" itemLabel="name"/>
						</form:select></td>
						<th class="panel-header">启封原因</th>
						<td><form:select path="entity.reasonRecordId"
										 style="width:184px;height: 27px;"
										 data-options="editable: false"
										 class="form_view_input combo easyui-combobox">
							<form:option value="">--请选择--</form:option>
							<form:options items="${recoveryReasonList}" itemValue="id" itemLabel="name"/>
						</form:select></td>
					</tr>
					<tr>
						<th class="panel-header">个人账号</th>
						<td><form:input path="entity.personId"
										class="form_view_input combo easyui-validatebox"
										style="width:184px; height: 27px;"/></td>
						<th class="panel-header">姓名</th>
						<td><form:input path="person.name" class="form_view_input combo easyui-validatebox"
										style="height: 27px;"/></td>
						<th class="panel-header">证件类型</th>
						<td><form:select path="person.gbidCardType" style="width:184px;height: 27px;"
										 data-options="editable: false"
                                         class="form_view_input combo easyui-combobox">
                            <form:option value="">--请选择--</form:option>
                            <form:options items="${contactIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                        </form:select></td>
						<th class="panel-header">证件号码</th>
						<td><form:input path="person.idCardNumber"
                                        style="width:184px;height:27px;"
                                        data-options="required: true"
                                        class="form_view_input combo easyui-validatebox"
                                        validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"></form:input></td>
					</tr>
					<c:if test="${command.pauseRecoveryFlag==1 }">
					<tr>
						<th class="panel-header">停缴至</th>
						<td><form:input path="entity.stopTo"
										class="form_view_input combo easyui-validatebox"
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
										style="width:184px; height: 27px;"/></td>
					</tr>
					</c:if>
				</table>
            </form:form>
		</span>
	<%--此查询换到form表单中--%>
   <%-- <span style="float:right;">
			<a class="l-btn" id="recovery_query_form_search_button"><span class="l-btn-left"><span
                    class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>--%>

	<table>
		<tr><td height="30px"><td></tr>
		<tr><td height="30px"><td></tr>
		<c:if test="${command.pauseRecoveryFlag==1}">
		<tr><td height="30px"><td></tr>
		</c:if>
	</table>

</div>

<table id="recovery_list_datagrid${command.pauseRecoveryFlag }"></table>
<div id="recovery_form_dialog">Dialog Content.</div>
<div id="recovery_search_form_dialog"></div>
<div id="recovery_confirm_list_dialog"></div>
