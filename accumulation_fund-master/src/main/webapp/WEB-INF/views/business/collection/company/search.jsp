<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
	 
	function companyBinding(id) {
		
		$.ajax({
			url: '<c:url value="/business/collection/company/binding.shtml" />'
			, type : "post"
			, dataType : 'json'
			, data : {'entity.id': id}
			, success : function(data, response, status) {
	
				if( jQuery.parseJSON('' + data) ) {
					
					$.messager.alert('通知', "办理后续业务的单位帐号绑定成功。");
				} else {
					
					$.messager.alert('通知', "办理后续业务的单位帐号绑定失败。");
				}
			}
		});
	}
	
	function companyUnbinding() {
	
		$.ajax({
			url: '<c:url value="/business/collection/company/unbinding.shtml" />'
			, type : "get"
			, dataType : 'json'
			//, data : {'entity.id': id}
			, success : function(data, response, status) {
	
				if( jQuery.parseJSON('' + data) ) {
					
					$.messager.alert('通知', "办理后续业务的单位帐号解绑成功。");
				} else {
					
					$.messager.alert('通知', "办理后续业务的单位帐号解绑失败。");
				}
			}
		});
	}

    var companyService;
    $(function () {
		

        companyService = new $.BaseService('#company_query_form', '#company_list_datagrid'
            , '#company_form_dialog', '#company_edit_form'
            , {
                <%--formUrl:'form_option.shtml?accountType=${command.accountType}'--%>
                 actionRootUrl: '<c:url value="/business/collection/company/" />'
                , entityTitle: "企业登记"
                , optionFormatter: function(value, rec) {
                	console.info(rec);
					return '<a href="#" title="修改" onClick="companyService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + companyService.actionRootUrl + companyService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="删除" onClick="companyService.datagrid.datagrid(\'checkRow\', companyService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + companyService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="单位绑定（办理后续业务）" onClick="companyBinding(' + rec.id + ')" iconCls="icon-tip" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="单位解绑（停止办理后续业务）" onClick="companyUnbinding()" iconCls="icon-tip" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="补充公积金账户设立" onClick="companyMiniAdding(' + rec.id + ')" iconCls="icon-mini-add" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="单位账户注销" onClick="companyUndoing(' + rec.id + ')" iconCls="icon-undo" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="网点变更" onClick="changeSite(' + rec.gbCompanyAccount + ')" iconCls="icon-gcoding" class="easyui-linkbutton" data-options="plain: true"></a>'
							+ '&nbsp;&nbsp;'
							+ '<a href="#" title="单位信息更改" onClick="companyInfoChange(' + rec.companyHistoryId + ')" iconCls="icon-company-edit" class="easyui-linkbutton" data-options="plain: true"></a>';
                }
                , datagridAddButtonId: 'company_add_button'
                , datagridBatchDeleteButtonId: 'company_batch_delete_button'
                , datagridColumns: [
                    {field: 'gbCompanyName', title: '企业名称', width: 180, rowspan: 2, align: 'center'},
                    {field: 'natureAccount', title: '账户性质', width: 100, rowspan: 1, align: 'center'},
                    {field: 'organizationType', title: '机构类型', width: 100, rowspan: 2, align: 'center'},
                    {field: 'companyIndustries', title: '单位所属行业', width: 100, rowspan: 1, align: 'center'},
                    {field: 'paymentSite', title: '缴存网点', width: 100, rowspan: 1, align: 'center'},
                    {field: 'gbCompanyAccount', title: '单位账号', width: 100, rowspan: 1, align: 'center'}
                ]
                , dialogWidth: 800
                , dialogHeight: 600
                , dialogButtons: [
                    {
                        id: 'company_confirm'
                        , text: '确认'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#company_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                var company_redit_code = $('#company_redit_code').val();
                                console.log(company_redit_code)
                                if (company_redit_code == null || company_redit_code == undefined || company_redit_code == "") {
                                    $.messager.confirm('通知', '统一社会信用代码（组织机构代码）未填写，是否特殊开户？', function (r) {
                                        if (r) {
                                            $('#company_flag').val(1);
                                            companyHistorySaveOrUpdate();
                                        } else {
                                            $.messager.progress('close');
                                        }
                                    });
                                }else{
                                    companyHistorySaveOrUpdate();
                                }

                            }
						}else{
                            $.messager.progress('close');
						}
                        function companyHistorySaveOrUpdate(){
                            $("#company_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/collection/company_history/history/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#company_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    if (data.result ) {
                                        $('#company_entity_id').val(data.id);
                                        if($('#flag').val()==1){
                                            $('#company_edit_form').form('load',{
                                                'entity.creditCode':data.creditCode
                                            });

                                        }
                                        //    $.messager.alert('通知', "添加成功！");
                                        $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                            if (r){
                                                window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                            }
                                        });
                                        $('#company_audit').linkbutton('enable');
                                    } else {
                                        $.messager.alert('通知', "添加失败！");
                                    }
                                }
                            });
                        }


                    }
                    }
                    , {
                        id: 'company_audit'
                        , text: '审核'
                        , disabled: true
                        //, iconCls: 'icon-save'
                        , handler: function () {
                            if (!$.isEmptyObject(companyCallBackJson)) {
                                console.log("需要双岗")
                                //比较json 如果flag为true则需要双岗审核
                                var formJson = showJson();
                                var flag = false;
                                for (var key in companyCallBackJson) {
                                    if (formJson[key] != companyCallBackJson[key]) {
                                        flag = true;
                                        break;
                                    }
                                }
                            } else {
                                console.log("不需要双岗")
                            }
                            $.messager.progress();
                            //复核业务dialog
                            $('#company_confirm_form_dialog').dialog({
                                title: '业务复核'
                                , href: "${pageContext.request.contextPath}/business/collection/company/review.shtml"
                                , width: 600
                                , height: 250
                                , closed: true
                                , cache: false
                                , resizable: true
                                , collapsible: true
                                , maximizable: true
                                , buttons: [{
                                    text: '复核'
                                    , iconCls: 'icon-save'
                                    , handler: function () {
                                        // $($(this).context.parentElement.parentElement).dialog('close');
                                        //查询用户名密码是否正确
                                        $('#company_review_form').form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company_review_user.shtml',
                                            onSubmit: function () {
                                                // do some check
                                                var reviewIsVal = $('#company_review_form').form('validate');
                                                if (!reviewIsVal) {
                                                    $.messager.progress('close');
                                                } else {
                                                    if (!confirm("要执行“复核”操作吗？？？")) {
                                                        $('#company_review_form').dialog('close')

                                                    }
                                                }
                                                return reviewIsVal
                                            },
                                            success: function (data) {
                                                data = jQuery.parseJSON(data);
                                                console.log(data)
                                                if (data.result) {
                                                    $('#company_confirmerId').val(data.userId);
                                                    if ("${eccomm_admin.id}" == data.userId) {
                                                        $.messager.alert('通知', "不能使用当前用户复核！");
                                                        $.messager.progress('close');
                                                    } else {
                                                        company_audit();
                                                    }

                                                } else {
                                                    $.messager.alert('通知', "用户名密码错误！");
                                                    $.messager.progress('close');
                                                }
                                            }
                                        });

                                    }
                                }
                                    , {
                                        text: '取消',
                                        iconCls: 'icon-cancel',
                                        handler: function () {
                                            $($(this).context.parentElement.parentElement).dialog('close');
                                            $.messager.progress('close');
                                            return false;
                                        }
                                    }]
                                , modal: true
                            }).dialog('close');

                            //判断是否需要双岗审核
                            if (flag) {
                                $('#company_confirm_form_dialog').dialog('open');
                            } else {
                                company_audit();
                            }
                            function company_audit() {
                                $("#company_edit_form").form('submit', {
                                    url: '${pageContext.request.contextPath}/business/collection/company/company_audit.shtml'
                                    , onSubmit: function () {
                                    }
                                    , success: function (data) {
                                        $("#company_audit").linkbutton('disable');
                                        //判断复核是否成功
                                        data = jQuery.parseJSON('' + data);
                                        if (data) {
                                            $.messager.alert('通知', "审核成功！");
                                            $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                if (r){
                                                    window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                }
                                            });
                                            $.messager.progress('close');
                                          //  companyService.dialog.dialog('close');
                                        } else {
                                            $.messager.alert('通知', "审核失败！");
                                        }
                                        $.messager.progress('close');

                                    }
                                });
                            }
                        }
                    }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });

        $('#company_query_form_search_button').click(function () {
            $('#company_query_form').submit();
        });
    });

    //网点变更记录dialog
	//fjs
    function changeSite(companyAccount) {


        $('#changeSite_dialog').dialog({
            title: '网点变更记录'
			,href: "${pageContext.request.contextPath}/business/collection/company/site_record.shtml?entity.gbCompanyAccount="+companyAccount
            , width: 900
            , height: 600
            , closed: false
            , cache: false
            , resizable: true
            , collapsible: true
            , maximizable: true
            , modal: true
            ,buttons:[
            	{text:'关闭',
                handler:function(){
                    $($(this).context.parentElement.parentElement).dialog('close');
				}

				}
			]
        });

        }

    //单位信息修改记录dialog
    //fjs
    function companyInfoChange(companyHistoryId) {


        $('#change_company_info_dialog').dialog({
            title: '单位信息变更记录'
            ,href: "${pageContext.request.contextPath}/business/collection/company/company_info_search.shtml?entity.companyHistoryId="+companyHistoryId
            , width: 900
            , height: 600
            , closed: false
            , cache: false
            , resizable: true
            , collapsible: true
            , maximizable: true
            , modal: true
            ,buttons:[
                {text:'关闭',
                    handler:function(){
                        $($(this).context.parentElement.parentElement).dialog('close');
                    }

                }
            ]
        });

    }

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
	<span style="float:left;">
		<form:form name="company_query_form" id="company_query_form"
			method="post" action="" onsubmit="return false;">

			<table class="form_view_border" bordercolordark="#FFFFFF"
				bordercolorlight="#45b97c" border="1px" cellpadding="0"
				cellspacing="0" style="">
				<tr>
					<th class="panel-header">企业名称</th>
					<td><form:input path="entity.gbCompanyName"
							class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
				</tr>
			</table>
		</form:form>
	</span>
	<span style="float:right;">
		<a class="l-btn" id="company_query_form_search_button"><span class="l-btn-left"><span
			class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
		&nbsp;&nbsp;
	</span>
	<table>
		<tr><td height="30px"><td></tr>
	</table>
</div>

<table id="company_list_datagrid"></table>
<div id="company_form_dialog">Dialog Content.</div>
<div id="company_confirm_form_dialog"></div>
<div id="changeSite_dialog"></div>
<div id="change_company_info_dialog"></div>
<div id="company_site_form_dialog"></div>