<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var overallTransfer;
    $(function() {
        overallTransfer = new $.BaseService('#overall_transfer_query_form', '#overall_transfer_list_datagrid'
            , '#overall_transfer_form_dialog', '#person_transfer_info_edit_form'
            , {actionRootUrl: '<c:url value="/business/collection/overall_transfer/" />'
                , entityTitle: "整体转移"
                , datagridAddButtonId: 'person_transfer_info_add_button'
                , datagridBatchDeleteButtonId: 'person_transfer_info_batch_delete_button'
                , datagridColumns:
                    [
                        {field: 'personalAccount', title: '职工账号', width: 150, rowspan: 1, align: 'center'},
                        {field: 'name', title: '职工姓名', width: 150, rowspan: 1, align: 'center'},
                        {field: 'idCardTypeId', title: '证件类型', width: 150, rowspan: 1, align: 'center'},
                        {field: 'idCardNumber', title: '证件号码', width: 150, rowspan: 1, align: 'center'},
                        {field: 'postCode', title: '缴存余额', width: 150, rowspan: 1, align: 'center'},
                        {field: 'gbeducation', title: '缴存状态', width: 150, rowspan: 1, align: 'center'}
                    ]
                , dialogWidth: 800
                , dialogHeight: 600
                , dialogButtons: [
                    {
                        id: 'company_confirm'
                        , text: '保存'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#company_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                companyHistorySaveOrUpdate();
                            }
                        }
                        function companyHistorySaveOrUpdate(){
                            var OutstartPaymentDate= $("#companyPaymentDate").val();
                            var InstartPaymentDate= $("#turnInCompanyPaymentDate").val();
                            if(OutstartPaymentDate==null && InstartPaymentDate==null){
                                $.messager.alert("提示","缴存年月不能为空请填写！");
                                $.messager.progress('close');
                                return false;
                            }
                            if(OutstartPaymentDate!=InstartPaymentDate){
                                $.messager.alert("提示","缴存年月不相符无法办理此业务！");
                                $.messager.progress('close');
                                return false;
                            }
                            $("#overall_transfer_info_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/collection/overall_transfer/save_insert.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#overall_transfer_info_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    if (data.result ) {
                                        $('#company_entity_id').val(data.id);
                                        if($('#flag').val()==1){
                                            $('#company_edit_form').form('load',{
                                                'entity.creditCode':data.creditCode
                                            });

                                        }
                                        /*$.messager.confirm('通知', '添加成功！是否打印', function(r){
                                            if (r){
                                                window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                            }
                                        });*/
                                        $('#company_audit').linkbutton('enable');
                                    } else {
                                        $.messager.alert('通知', "添加失败！");
                                    }
                                }
                            });
                        }



                    }
                    }
                    , {
                        id: 'company_audit'
                        , text: '审核'
                        , disabled: true
                        //, iconCls: 'icon-save'
                        , handler: function () {
                            if (!$.isEmptyObject(companyCallBackJson)) {
                                console.log("需要双岗")
                                //比较json 如果flag为true则需要双岗审核
                                var formJson = showJson();
                                var flag = false;
                                for (var key in companyCallBackJson) {
                                    if (formJson[key] != companyCallBackJson[key]) {
                                        flag = true;
                                        break;
                                    }
                                }
                            } else {
                                console.log("不需要双岗")
                            }
                            $.messager.progress();
                            //复核业务dialog
                            $('#company_confirm_form_dialog').dialog({
                                title: '业务复核'
                                , href: "${pageContext.request.contextPath}/business/collection/review.shtml"
                                , width: 600
                                , height: 250
                                , closed: true
                                , cache: false
                                , resizable: true
                                , collapsible: true
                                , maximizable: true
                                , buttons: [{
                                    text: '复核'
                                    , iconCls: 'icon-save'
                                    , handler: function () {
                                        // $($(this).context.parentElement.parentElement).dialog('close');
                                        //查询用户名密码是否正确
                                        $('#company_review_form').form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company_review_user.shtml',
                                            onSubmit: function () {
                                                // do some check
                                                var reviewIsVal = $('#company_review_form').form('validate');
                                                if (!reviewIsVal) {
                                                    $.messager.progress('close');
                                                } else {
                                                    if (!confirm("要执行“复核”操作吗？？？")) {
                                                        $('#company_review_form').dialog('close')

                                                    }
                                                }
                                                return reviewIsVal
                                            },
                                            success: function (data) {
                                                data = jQuery.parseJSON(data);
                                                console.log(data)
                                                if (data.result) {
                                                    $('#company_confirmerId').val(data.userId);
                                                    if ("${eccomm_admin.id}" == data.userId) {
                                                        $.messager.alert('通知', "不能使用当前用户复核！");
                                                        $.messager.progress('close');
                                                    } else {
                                                        company_audit();
                                                    }

                                                } else {
                                                    $.messager.alert('通知', "用户名密码错误！");
                                                    $.messager.progress('close');
                                                }
                                            }
                                        });

                                    }
                                }
                                    , {
                                        text: '取消',
                                        iconCls: 'icon-cancel',
                                        handler: function () {
                                            $($(this).context.parentElement.parentElement).dialog('close');
                                            $.messager.progress('close');//关闭滚动条
                                            return false;
                                        }
                                    }]
                                , modal: true
                            }).dialog('close');

                            //判断是否需要双岗审核
                            if (flag) {
                                $('#company_confirm_form_dialog').dialog('open');
                            } else {
                                company_audit();
                            }
                            function company_audit() {
                                $("#company_edit_form").form('submit', {
                                    url: '${pageContext.request.contextPath}/business/collection/company_audit.shtml'
                                    , onSubmit: function () {
                                    }
                                    , success: function (data) {
                                        $("#company_audit").linkbutton('disable');
                                        //判断复核是否成功
                                        data = jQuery.parseJSON('' + data);
                                        if (data) {
                                            $.messager.alert('通知', "审核成功！");
                                            $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                if (r){
                                                    window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                }
                                            });
                                            $.messager.progress('close');
                                            //  companyService.dialog.dialog('close');
                                        } else {
                                            $.messager.alert('通知', "审核失败！");
                                        }
                                        $.messager.progress('close');

                                    }
                                });
                            }
                        }
                    }
                    , {
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
            });
        $('#person_transfer_info_quesry_form_search_button').click(function() {$('#overall_transfer_query_form').submit();});
    });

</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="overall_transfer_query_form" id="overall_transfer_query_form"
					   method="post" action="" onsubmit="return false;">

				<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">转出单位账号</th>
						<td><form:input path="entity.company.gbCompanyAccount"
										data-options="required: true"
										class="form_view_input combo easyui-validatebox" style="height: 27px;" /></td>
					</tr>
				</table>
			</form:form>
		</span>
	<span style="float:right;">
			<a class="l-btn" id="person_transfer_info_quesry_form_search_button"><span class="l-btn-left">
				<span class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>
	<table>
		<tr><td height="30px"><td></tr>
	</table>
</div>





<table id="overall_transfer_list_datagrid"></table>
<div id="overall_transfer_form_dialog">Dialog Content.</div>