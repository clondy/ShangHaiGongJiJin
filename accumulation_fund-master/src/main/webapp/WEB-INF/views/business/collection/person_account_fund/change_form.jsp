<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    var data = ${personAccountFund};

    if(data.result){
        $('#person_change_edit_form').form('load', data);
    }

    //月缴存额随工资收入变化
    $("#wageIncome").on('input',function(e){

        var wageIncome = $("#wageIncome").val();

        var companyPaymentRatio = parseInt($("#companyPaymentRatio").val());
        var personPaymentRatio = parseInt($("#personPaymentRatio").val());
        var monthPay = wageIncome*(companyPaymentRatio+personPaymentRatio)/100;
        $("#monthPay").val(monthPay);
    });

    //判断身份证是否被修改
    var flag = false ;
    var idCardNumber = $("#idCardNumber").val();
    $("#idCardNumber").blur(function () {
        var idCardNumberModify =  $("#idCardNumber").val();
        alert("idCardNumber="+idCardNumber+"/idCardNumberModify="+idCardNumberModify);
        if(idCardNumber != idCardNumberModify){
            flag = true;
        }
    });
    alert(flag);

</script>

<div class="div_center" style="margin-top: 0px">
    <form:form name="person_change_edit_form" id="person_change_edit_form" method="post" style="width:600px"
               action="" onsubmit="return false;">
        <form:hidden path="entity.companyAccountFundId" id="company_account_fund_id"/>
        <form:hidden path="entityPerson.id" id="entityPersonId"/>
        <form:hidden path="entityPerson.companyId" id="entityPerson_company_id"/>
        <form:hidden path="previousId" id="person_history_id"/>
        <form:hidden path="isFirstSave" id="isFirstSave"/>

        <table class="form_view_border" bordercolordark="#45b97c" bordercolorlight="#45b97c" border="1px" cellpadding="0" cellspacing="0">
            <%--<tr>--%>
                <%--<th style="width: 20%" class="panel-header">个人账号</th>--%>
                <%--<td><form:input path="entityPerson.personalAccount" id="personalAccount"--%>
                                <%--data-options="required: true"--%>
                                <%--style="width:184px;height: 27px;"--%>
                                <%--class="form_view_input combo easyui-validatebox"--%>
                                <%--validType="'','','validate_person_account','个人账号不存在！','']"></form:input></td>--%>
                <%--</td>--%>
                <%--<td style="text-align:left;" colspan="2"><a href="#" class="easyui-linkbutton" id="query_person_account">查询</a></td>--%>
            <%--</tr>--%>
            <tr>
                <th style="width: 20%" class="panel-header">单位账号</th>
                <td><form:input path="entityPerson.company.gbCompanyAccount" id="gbCompanyAccount" disabled="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^[0-9]{13}$','单位账号输入有误！','validate_company_account','单位账号不存在！','']"></form:input></td>
                    <%--</form:input></td>--%>
                </td>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entityPerson.company.gbCompanyName" id="gbCompanyName" disabled="true"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['^.{4,20}$','请确认单位名称的长度在4~20个字之间！']"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">证件类型</th>
                <td><form:select path="entityPerson.idCardTypeId" id="idCardType"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${legalPersonIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
                <th class="panel-header">证件号码</th>
                <td><form:input path="entityPerson.idCardNumber"
                                style="width:184px;height: 27px;"
                                id="idCardNumber"
                                data-options="required: true"
                                class="form_view_input combo easyui-validatebox"
                                validType="complexValid['(^[1-9]\\\d{5}(18|19|([23]\\\d))\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{3}[0-9Xx]$)|(^[1-9]\\\d{5}\\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\\d{2}[0-9Xx]$)','证件号不合法！']"
                                ></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">姓名</th>
                <td><form:input path="entityPerson.name" id="name"
                                data-options=""
                                style="width:184px;height: 27px;"></form:input></td>
                <th class="panel-header">出生日期</th>
                <td><form:input path="entityPerson.birthYearMonth"
                                data-options=""
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"
                                id="birthYearMonth"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">性别</th>
                <td><form:select path="entityPerson.sexId" id="sex"
                                 style="width:184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="complexValid['^--请选择--$','必填项！', '', '', 1]">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${sexList}" itemValue="id" itemLabel="name"/>
                </form:select></td>
                <th class="panel-header">户籍</th>
                <td><form:input path="entityPerson.koseki"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                class="form_view_input combo easyui-validatebox" id="koseki"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">工资收入</th>
                <td colspan="1"><form:input path="entityPerson.wageIncome" id="wageIncome"
                                            data-options="required: true"
                                            class="form_view_input combo easyui-validatebox"
                                            style="width:99.7%;height: 27px;"></form:input>
                </td>
                <th class="panel-header">缴存率标志</th>
                <td><form:select path="entity.companyAccountFund.depositRateMarkId"
                                 style="width: 184px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                                 validType="">
                    <form:option value="" >--请选择--</form:option>
                    <form:options items="${depositRateMarkIdList}" itemValue="id" itemLabel="name"/>
                </form:select>
                </td>
            </tr>
            <tr>
                <th class="panel-header">单位缴存比例(%)</th>
                <td><form:input path="entity.companyAccountFund.companyPaymentRatio" id="companyPaymentRatio"
                                style="width: 186px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner" disabled="true"></form:input></td>
                <th class="panel-header">个人缴存比例(%)</th>
                <td><form:input path="entity.companyAccountFund.personPaymentRatio" id="personPaymentRatio"
                                style="width: 184px;height: 27px;"
                                data-options="required: true,increment:1,min:1,max:7"
                                class="easyui-numberspinner" disabled="true"></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">月缴存额</th>
                <td colspan="3"><form:input path="entity.monthPay" disabled="true"
                                            data-options="required: true"
                                            style="width:184px;height: 27px;" id="monthPay"
                                            class="form_view_input combo easyui-validatebox"></form:input></td>
            </tr>
        </table>
    </form:form>
</div>