<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">


</script>

<div class="div_center">
        <table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
               bordercolorlight="#45b97c" border="1px" cellpadding="0"
               cellspacing="0" id="dataTable">
            <tr><td class="panel-header" colspan="3" align="center">变更信息对比</td></tr>
            <tr height="30" <c:if test="${personHistory.name != personHistoryAfter.name}"> style="color: #FF2F2F;" </c:if>>
                <th class="panel-header">姓名</th>
                    <td class="panel-header" id="name" width="100px">${personHistory.name}</td>
                    <td class="panel-header" id="nameP" width="100px">${personHistoryAfter.name}</td>

            </tr><tr height="30" <c:if test="${personHistory.idCardNumber != personHistoryAfter.idCardNumber}"> style="color: #FF2F2F;" </c:if>>
            <th class="panel-header">证件号码</th>
            <td class="panel-header" id="idCardNumber" width="100px">${personHistory.idCardNumber}</td>
            <td class="panel-header" id="idCardNumberP" width="100px">${personHistoryAfter.idCardNumber}</td>
        </tr><tr height="30" <c:if test="${personHistory.sex.name != personHistoryAfter.sex.name}"> style="color: #FF2F2F;" </c:if>>
            <th class="panel-header">性别</th>
            <td class="panel-header" id="sex" width="100px">${personHistory.sex.name}</td>
            <td class="panel-header" id="sexP" width="100px">${personHistoryAfter.sex.name}</td>
        </tr><tr height="30" <c:if test="${personHistory.birthYearMonth != personHistoryAfter.birthYearMonth}"> style="color: #FF2F2F;" </c:if>>
            <th class="panel-header">出生日期</th>
            <td class="panel-header" id="birthDay" width="100px">${personHistory.birthYearMonth}</td>
            <td class="panel-header" id="birthDayP" width="100px">${personHistoryAfter.birthYearMonth}</td>
        </tr><tr height="30" <c:if test="${personHistory.company.gbCompanyName != personHistoryAfter.company.gbCompanyName}"> style="color: #FF2F2F;" </c:if>>
            <th class="panel-header">公司名称</th>
            <td class="panel-header" id="companyName" width="100px">${personHistory.company.gbCompanyName}</td>
            <td class="panel-header" id="companyNameP" width="100px">${personHistory.company.gbCompanyName}</td>
        </tr>

        </table>
</div>