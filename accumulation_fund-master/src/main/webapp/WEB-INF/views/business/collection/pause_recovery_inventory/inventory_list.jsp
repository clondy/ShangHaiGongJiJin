<%@ page import="com.capinfo.framework.model.system.User" %>

<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="pauseRecoveryInventory" items="${PauseRecoveryInventoryList}" varStatus="status">
    {"id": "${pauseRecoveryInventory.id}",
    "accumulationFundAccountType": "${pauseRecoveryInventory.companyAccountFund.accumulationFundAccountType.name}",
    "companyAccount": "${pauseRecoveryInventory.companyAccountFund.companyAccount}",
    "gbCompanyName": "${pauseRecoveryInventory.company.gbCompanyName}",
    "createTime": "<fmt:formatDate value="${pauseRecoveryInventory.createTime}"/>",
    "name": "${pauseRecoveryInventory.operator.name}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}