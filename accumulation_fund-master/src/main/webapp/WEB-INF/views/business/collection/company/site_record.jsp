<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var companyHistoryService;
    $(function () {

        companyHisetoryService = new $.BaseService( '#company_site_query_form','#company_site_list_datagrid'
            , '#company_site_form_dialog', '#company_site_edit_form'
            ,{
                 formUrl:'site_form.shtml'
                ,listUrl:'site_list.shtml?gbCompanyAccount=${command.entity.gbCompanyAccount}'
                ,actionRootUrl: '<c:url value="/business/collection/company_history/" />'
                , entityTitle: "网点记录"
                , datagridColumns: [
                    {field: 'gbCompanyAccount', title: '单位账号', width: 180, rowspan: 1, align: 'center'},
                   // {field: 'gbOrganizationCode', title: '补充住房公积金账号', width: 100, rowspan: 1, align: 'center'},
                    {field: 'gbCompanyName', title: '单位名称', width: 100, rowspan: 1, align: 'center'},
                    {field: 'contactName', title: '联系人姓名', width: 100, rowspan: 1, align: 'center'},
                    {field: 'contactMobilePhone', title: '联系人移动电话', width: 100, rowspan: 1, align: 'center'},
					{field: 'paymentSite', title: '缴存网点', width: 100, rowspan: 1, align: 'center'},
                    {field: 'confirmationTime', title: '操作时间', width: 100, rowspan: 1, align: 'center'}
                ]
                ,datagridToolbar:[{
                    id: this.datagridAddButtonId
                    , text: '更改'
                    , iconCls: 'icon-edit'
                    , handler:function(){


                        $('#company_site_form_dialog').dialog({
                            title: '网点变更'
                            , href: "${pageContext.request.contextPath}/business/collection/company/site_form.shtml?entity.gbCompanyAccount=${command.entity.gbCompanyAccount}"
                            , width: 700
                            , height: 400
                            , closed: false
                            , cache: false
                            , resizable: true
                            , collapsible: true
                            , maximizable: true
                            , buttons: [
                                {id: 'company_confirm'
                                , text: '确认'
                                //, iconCls: 'icon-save'
                                , handler: function () {
                                    //$.messager.progress();
                                        $("#company_site_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company_history/history/save_change_site.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#company_site_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }
                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {
                                                    //历史ID 赋值
                                                    $('#company_history_id').val(data["companyHistoryId"]);

                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });
                                                    //审核按钮可用
                                                    $('#company_site_audit').linkbutton('enable');

                                               } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        });

                                  }
                                }
                                , {
                                    id: 'company_site_audit'
                                    , text: '审核'
                                    , disabled: true
                                    //, iconCls: 'icon-save'
                                    , handler: function () {
                                        $("#company_site_form").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/company/save_change_site_company.shtml'
                                            , onSubmit: function () {
                                                var isValid = $('#company_site_form').form('validate');
                                                if (!isValid) {
                                                    $.messager.progress('close');
                                                }
                                                return isValid
                                            }

                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {
                                                    //ID 赋值
                                                    $('#company_id').val(data["entity.id"]);

                                                    //    $.messager.alert('通知', "添加成功！");
                                                    $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                        if (r){
                                                            window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                        }
                                                    });

                                                } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        })
                                    }
                                }
                                , {
                                    id: 'site_reset'
                                    , text: '重置'
                                    //, iconCls: 'icon-save'
                                    , handler: function () {

                                        if (confirm("要执行“重置”操作吗？？？")) {

                                            $("#company_site_audit").linkbutton('disable');
                                            $('#company_site_form').form("clear");
                                            //document.getElementById("company_site_form").reset();
                                            //companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                                        }
                                    }
                                }
                                , {
                                    text: '取消',
                                    iconCls: 'icon-cancel',
                                    handler: function () {
                                        $($(this).context.parentElement.parentElement).dialog('close');
                                    }
                                }




                            ]
                        });
                    }
                }

                ]
                , dialogWidth: 1000
                , dialogHeight: 800

            });

        $('#company_query_form_search_button').click(function () {
            $('#company_site_query_form').submit();
            href: "${pageContext.request.contextPath}/business/collection/company/site_record.shtml?entity.gbCompanyAccount="+companyAccount
        });

    });

</script>


<table id="company_site_list_datagrid"></table>
<div id="company_site_form_dialog"></div>