<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var companyService;
    $(function () {


        companyService = new $.BaseService('#thaw_query_form', '#thaw_list_datagrid'
            , '#thawexamine_form_dialog', '#freeze_examine_form${command.freezeFlag}'
            , {
                actionRootUrl: '<c:url value="/business/collection/freeze/" />'
                , entityTitle: "冻结"
                , optionFormatter: function(value, rec) {

                                return  '<a href="#" title="解冻" onClick="thaw('+ rec.id + ')" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                                    + '&nbsp;&nbsp;'+
								'<a href="#" title="解冻审核" onClick="examined('+ rec.id + ')" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>';
				   }
                , datagridAddButtonId: 'thaw_add_button'
                , datagridBatchDeleteButtonId: 'thaw_batch_delete_button'
                , datagridHasFrozenColumns: false

                , datagridToolbar: [

                    {
                        id:thaw_query_form
                        , text: '查询'
                        , iconCls: 'icon-search'
                        , handler: function() {

                        $('#thaw_query_form').submit();
                    }
                    }
                ]
                , datagridColumns: [
                    {field: 'personalAccount', title: '个人账号', width: 220, rowspan: 2, align: 'center'},
                    {field: 'name', title: '姓名', width: 220, rowspan: 2, align: 'center'},
                    {field: 'idCardNumber', title: '证件号码', width: 150, rowspan: 2, align: 'center'},
                    {field: 'gbCompanyAccount', title: '单位账号', width: 150, rowspan: 2, align: 'center'},
                    {field: 'gbCompanyName', title: '单位名称', width: 150, rowspan: 2, align: 'center'},
                    {field: 'unfreezeReason', title: '解冻原因', width: 150, rowspan: 2, align: 'center'},
                    {field: 'courtName', title: '法院名称', width: 150, rowspan: 2, align: 'center'},
                    {field: 'caseNumber', title: '执行案号', width: 150, rowspan: 2, align: 'center'},
                    {field: 'judgeName', title: '法官姓名', width: 150, rowspan: 2, align: 'center'},
                    {field: 'contactInfo', title: '联系方式', width: 150, rowspan: 2, align: 'center'},
                    {field: 'amount', title: '冻结金额', width: 150, rowspan: 2, align: 'center'},
                    {field: 'startTime', title: '受理日期', width: 150, rowspan: 2, align: 'center'},
                    {field: 'freezePeriod', title: '冻结期限', width: 150, rowspan: 2, align: 'center'},
                    {field: 'endTime', title: '冻结结束日期', width: 150, rowspan: 2, align: 'center'},
                    {field: 'remark', title: '备注', width: 150, rowspan: 2, align: 'center'},
                    {field: 'operator', title: '操作员', width: 150, rowspan: 2, align: 'center'}
                ]
                , dialogWidth: 600
                , dialogHeight: 320
                , dialogButtons: [
					{
                        id: 'thaw_examine'
                        , text: '审核'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                            $.messager.progress();
                            if($('#thaw_edit_form').form('validate')){
                                if (!confirm("要执行“审核”操作吗？？？")) {
                                    $.messager.progress('close');
                                    return false;
                                }else{

                                    $("#thaw_edit_form").form('submit', {
                                        url: '${pageContext.request.contextPath}/business/collection/freeze/thawAudit.shtml'
                                        , onSubmit: function () {

                                        }
                                        , success: function (data) {
                                            $.messager.progress('close');
                                            console.log(data)
                                            data = jQuery.parseJSON(data);
                                            if (data.result ) {
                                                $.messager.alert('通知', "审核成功！");

                                            } else {
                                                $.messager.alert('通知', "审核失败！");
                                            }
                                        }
                                    });


                                }
                            }else{
                                $.messager.progress('close');
                            }

                        }
                    },

					{
                        id: 'chongzhi'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
				,
            });


    });

 function thaw(a){

        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/freeze/CanUnfreeze.shtml?entity.id="+ a + "&thawFlag=3",
            type: "post",
            datatype: "json",
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                //已存在
                if (obj.result) {


                    $('#thaw_form_dialog').dialog({
                        title: '冻结'
                        , width: 600
                        , height: 360
                        , closed: true
                        , cache: false
                        , resizable: true
                        , collapsible: true
                        , maximizable: true
                        , buttons: [

                            {
                                id: 'freeze_confirm'
                                , text: '保存'
                                //, iconCls: 'icon-save'
                                , handler: function () {
                                $.messager.progress();
                                if($('#freeze_examine_form${command.freezeFlag}').form('validate')){
                                    if (!confirm("要执行“保存”操作吗？？？")) {
                                        $.messager.progress('close');
                                        return false;
                                    }else{

                                        $("#freeze_examine_form${command.freezeFlag}").form('submit', {
                                            url: '${pageContext.request.contextPath}/business/collection/freeze/unfreeze.shtml?freezeFlag=3'
                                            , onSubmit: function () {
                                            }
                                            , success: function (data) {
                                                $.messager.progress('close');
                                                console.log(data)
                                                data = jQuery.parseJSON(data);
                                                if (data.result ) {

                                                    $.messager.alert('通知', "添加成功！");

                                                } else {
                                                    $.messager.alert('通知', "添加失败！");
                                                }
                                            }
                                        });

                                    }
                                }else{
                                    $.messager.progress('close');
                                }

                            }
                            },
                            {
                                id: 'chongzhi'
                                , text: '重置'
                                //, iconCls: 'icon-save'
                                , handler: function () {

                                if (confirm("要执行“重置”操作吗？？？")) {

                                    $("#fuhe").linkbutton('disable');
                                    companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                                }
                            }
                            }
                            , {
                                text: '取消',
                                iconCls: 'icon-cancel',
                                handler: function () {
                                    $($(this).context.parentElement.parentElement).dialog('close');
                                }
                            }
                        ]
                        , modal: true
                    })
                    $('#thaw_form_dialog').dialog('open').dialog('refresh', '${pageContext.request.contextPath}/business/collection/freeze/examine.shtml?entity.id=' + a + '&freezeFlag=3')





                } else {

                    $.messager.alert("通知", "此人不能冻结！")
                    return false;

                }
            }
        })


        }

function examined(a){


    $.ajax({
        url: "${pageContext.request.contextPath}/business/collection/freeze/CanUnfreezeAudit.shtml?entity.id="+ a + "&thawFlag=3",
        type: "post",
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            //已存在
            if (obj.result) {


                $('#thaw_form_dialog').dialog({
                    title: '冻结'
                    , width: 600
                    , height: 360
                    , closed: true
                    , cache: false
                    , resizable: true
                    , collapsible: true
                    , maximizable: true
                    , buttons: [

                        {
                            id: 'freeze_confirm'
                            , text: '审核'
                            //, iconCls: 'icon-save'
                            , handler: function () {
                            $.messager.progress();
                            if($('#freeze_examine_form${command.freezeFlag}').form('validate')){
                                if (!confirm("要执行“审核”操作吗？？？")) {
                                    $.messager.progress('close');
                                    return false;
                                }else{

                                    $("#freeze_examine_form${command.freezeFlag}").form('submit', {
                                        url: '${pageContext.request.contextPath}/business/collection/freeze/unfreezeAudit.shtml?freezeFlag=3'
                                        , onSubmit: function () {
                                        }
                                        , success: function (data) {
                                            $.messager.progress('close');
                                            console.log(data)
                                            data = jQuery.parseJSON(data);
                                            if (data.result ) {

                                                $.messager.alert('通知', "添加成功！");

                                            } else {
                                                $.messager.alert('通知', "添加失败！");
                                            }
                                        }
                                    });

                                }
                            }else{
                                $.messager.progress('close');
                            }

                        }
                        },
                        {
                            id: 'chongzhi'
                            , text: '重置'
                            //, iconCls: 'icon-save'
                            , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#fuhe").linkbutton('disable');
                                companyService.dialog.dialog('refresh', companyService.actionRootUrl + companyService.formUrl);
                            }
                        }
                        }
                        , {
                            text: '取消',
                            iconCls: 'icon-cancel',
                            handler: function () {
                                $($(this).context.parentElement.parentElement).dialog('close');
                            }
                        }
                    ]
                    , modal: true
                })
                $('#thaw_form_dialog').dialog('open').dialog('refresh', '${pageContext.request.contextPath}/business/collection/freeze/examine.shtml?entity.id=' + a + '&freezeFlag=3')

            } else {

                $.messager.alert("通知", "此人不能冻结审核！")
                return false;

            }
        }
    })





}


</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="thaw_query_form" id="thaw_query_form"
					   method="post" action="" onsubmit="return false;">
				<form:hidden path="freezeFlag" id="thaw_flag"/>

				<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">区县</th>
						<td><form:select path="entity.regionId"
										 id="courtName"
										 style="height: 27px;"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false"
										 >
							<form:option value="" >--请选择--</form:option>
							<form:options items="${regionList}" itemValue="id" itemLabel="name"/>
						</form:select></td>
						<th class="panel-header">单位账号</th>
						<td><form:input path="entity.person.company.gbCompanyAccount"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

						<th class="panel-header">单位名称</th>
						<td><form:input path="entity.person.company.gbCompanyName"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

					</tr>
					<tr>
						<th class="panel-header">个人账号</th>
						<td><form:input path="entity.person.personalAccount"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

						<th class="panel-header">证件号码</th>
						<td><form:input path="entity.person.idCardNumber"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

						<th class="panel-header">姓名</th>
						<td><form:input path="entity.person.name"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

						<th class="panel-header">受理日期</th>
						<td><form:input path="entity.startTime"
										class="form_view_input combo easyui-datebox" style="height: 27px;"/>至
							<form:input path="entity.endTime"
										class="form_view_input combo easyui-datebox" style="height: 27px;"/>
						</td>
					</tr>
				</table>
			</form:form>
		</span>
	<%--<span style="float:right;">
			<a class="l-btn" id="thaw_query_form_search_button"><span class="l-btn-left"><span
					class="l-btn-text icon-search l-btn-icon-left">查询</span></span></a>
			&nbsp;&nbsp;
		</span>--%>
	<table>
		<tr>
		<td height="30px">
		<td>
	</tr>
		<tr>
			<td height="30px">
			<td>
		</tr>
	</table>
</div>

<table id="thaw_list_datagrid"></table>
<div id="thaw_form_dialog">Dialog Content.</div>
<div id="thawexamine_form_dialog">Dialog Content.</div>
<div id="thaw_confirm_form_dialog">

</div>
