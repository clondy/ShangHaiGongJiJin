<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">

    $(function() {

        var nameTemp = $("#nameTemp").val();
        var idCardNumberTemp = $("#idCardNumberTemp").val();
        var personalAccountTemp = $("#personalAccountTemp").val();
        var CompanyNameTemp = $("#CompanyNameTemp").val();
        var stateTemp = $("#stateTemp").val();
        $("#name").text(nameTemp);
        $("#idCardNumber").text(idCardNumberTemp);
        $("#personalAccount").text(personalAccountTemp);
        $("#CompanyName").text(CompanyNameTemp);
        $("#state").text(stateTemp);


    });
</script>

<div class="div_center">

    <table class="form_view_border" bordercolordark="#FFFFFF"  autocomplete="off"
           bordercolorlight="#45b97c" border="1px" cellpadding="0"
           cellspacing="0">
        <tr><td class="panel-header" colspan="2" align="center">系统中已存在此账户信息</td></tr>
        <tr height="30">
            <th class="panel-header">姓名</th>
            <td class="panel-header" id="name" width="100px"></td>
        </tr><tr height="30">
            <th class="panel-header">证件号码</th>
        <td class="panel-header" id="idCardNumber" width="100px"></td>
        </tr><tr height="30">
            <th class="panel-header">账号</th>
            <td class="panel-header" id="personalAccount"></td>
        </tr><tr height="30">
            <th class="panel-header">单位名称</th>
            <td class="panel-header" id="CompanyName"></td>
        </tr><tr height="30">
            <th class="panel-header">缴存状态</th>
            <td class="panel-header" id="state"></td>
    </table>
</div>