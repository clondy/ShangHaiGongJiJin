<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="company" items="${CompanyList}" varStatus="status">
    {"id": "${company.id}",
    "gbCompanyName": "${company.gbCompanyName}",
    "gbOrganizationCode": "${company.gbOrganizationCode}",
    "creditCode": "${company.creditCode}",
    "competentCode": "${company.competentCode}",
    "gbCompanyAccount": "${company.gbCompanyAccount}",
    "natureAccount": "${company.natureAccount.name}",
    "companyInsideClassification": "${company.companyInsideClassification.name}",
    "organizationType": "${company.organizationType.name}",
    "companyEconomicType": "${company.companyEconomicType.name}",
    "companyIndustries": "${company.companyIndustries.name}",
    "companyRelatoinships": "${company.companyRelatoinships.name}",
    "gbCompanyAddress": "${company.gbCompanyAddress}",
    "registeredAddress": "${company.registeredAddress}",
    "registeredPostCode": "${company.registeredPostCode}",
    "gbCompanyPostCode": "${company.gbCompanyPostCode}",
    "gbCompanyEmail": "${company.gbCompanyEmail}",
    "gbCompanyCreationDate": "${company.gbCompanyCreationDate}",
    "companyAddress": "${company.companyAddress}",
    "legalPersonality": "${company.legalPersonality}",
    "legalPersonIdCardType": "${company.legalPersonIdCardType.name}",
    "gbLegalPersonName": "${company.gbLegalPersonName}",
    "gbLegalPersonIdCardNumber": "${company.gbLegalPersonIdCardNumber}",
    "legalPersonMobilePhone": "${company.legalPersonMobilePhone}",
    "companyOpenedDate": "${company.companyOpenedDate}",
    "gbCompanyPayDay": "${company.gbCompanyPayDay}",
    "gbAgentName": "${company.gbAgentName}",
    "agentIdCardType": "${company.agentIdCardType.name}",
    "gbAgentIdCardNumber": "${company.gbAgentIdCardNumber}",
    "gbAgentMobilePhone": "${company.gbAgentMobilePhone}",
    "gbAgentTelephone": "${company.gbAgentTelephone}",
    "gbTrusteeBank": "${company.gbTrusteeBank}",
    "gbTrusteeBankCode": "${company.gbTrusteeBankCode}",
    "delegationBranch": "${company.delegationBranch}",
    "delegationBranchCode": "${company.delegationBranchCode}",
    "paymentSite": "${company.paymentSite}",
    <%--"region": "${company.region.name}",--%>
    "contactMobilePhone": "${company.contactMobilePhone}",
    "contactName": "${company.contactName}",
    "contactIdCardNumber": "${company.contactIdCardNumber}",
    "contactIdCardType": "${company.contactIdCardType.name}",
    "contactAddress": "${company.contactAddress}",
    "unitCode": "${company.unitCode}",
    "restDay": "${company.restDay}",
    "responCode": "${company.responCode}",
    "newFlag": "${company.newFlag}",
    "custNo": "${company.custNo}",
    "manageState": "${company.manageState}",
    "payDay": "${company.payDay}",
    "companyHistoryId": "${company.companyHistoryId}"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}