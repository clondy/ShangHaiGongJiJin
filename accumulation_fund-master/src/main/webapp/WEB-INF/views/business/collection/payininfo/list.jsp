<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
{
"total": <c:out value="${DataTotalCount}"/>
, "rows": [
<c:forEach var="payininfo" items="${PayininfoList}" varStatus="status">
    { "id": "${payininfo.id}",
    "serialNumber": "${payininfo.id}",
    "companyAccount": "${payininfo.companyAccount}",
    "verificationOptions": "${payininfo.verificationOptions}",
    "payMonthly": "${payininfo.payMonthly}",
    "receivedDate": "${payininfo.receivedDate}",
    "remitAmount": "${payininfo.remitAmount}",
    "remitNumber": "${payininfo.remitNumber}",
    "lastRemiAmount": "${payininfo.lastRemitAmount}",
    "lastRemitNumber": "${payininfo.lastRemitNumber}",
    "increaseRemitAmount": "${payininfo.increaseRemitAmount}",
    "increaseRemitNumber": "${payininfo.increaseRemitNumber}",
    "reduceRemitAmount": "${payininfo.reduceRemitAmount}",
    "reduceRemitNumber": "${payininfo.reduceRemitNumber}",
    "dateTime": "${payininfo.dateTime}",
    "bankCode": "${payininfo.bankCode}",
    "operatorId": "${payininfo.operatorId}",
    "regionId": "${payininfo.regionId}",
    "siteId": "${payininfo.siteId}",
    "confirmerId": "${payininfo.confirmerId}",
    "confirmationTime": "${payininfo.confirmationTime}",
    "channels": "${payininfo.channels}",
    "createName": "${payininfo.createName}",
    "isfinished": "<c:if test="${payininfo.isfinished}">完成 </c:if><c:if test="${!payininfo.isfinished}">未完成 </c:if>"
    }
    <c:if test="${!status.last}">, </c:if>
</c:forEach>
]


}