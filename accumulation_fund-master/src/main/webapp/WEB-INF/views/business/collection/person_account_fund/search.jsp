<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>

    var personService;
    $(function () {

        personService = new $.BaseService('#person_query_form', '#person_list_datagrid'
            , '#person_form_dialog', '#person_edit_form'
            , {
                formUrl:'form_option.shtml'
                ,actionRootUrl: '<c:url value="/business/collection/person_account_fund/" />'
                , entityTitle: "个人账户"
                , optionFormatter: function(value, rec) {

                    return '<a href="#" title="修改" onClick="personService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + personService.actionRootUrl + personService.formUrl + '?entity.id=' + rec.id + '\');" iconCls="icon-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="删除" onClick="personService.datagrid.datagrid(\'checkRow\', personService.datagrid.datagrid(\'getRowIndex\', ' + rec.id + '));$(\'#' + personService.datagridBatchDeleteButtonId + '\').click();" iconCls="icon-remove" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
						+ '<a href="#" title="设立补充账户" onClick="personService.dialog.dialog(\'open\').dialog(\'refresh\', \'' + personService.actionRootUrl + personService.formUrl +'?extra=extra' + '&entity.id=' + rec.id + '\');" iconCls="icon-add" class="easyui-linkbutton" data-options="plain: true"></a>'
                        + '&nbsp;&nbsp;'
                        + '<a href="#" title="个人补充账户信息变更" onClick="infoChage(' + rec.id + ')" iconCls="icon-company-edit" class="easyui-linkbutton" data-options="plain: true"></a>'
                        ;
                }
                , datagridAddButtonId: 'person_add_button'
                , datagridBatchDeleteButtonId: 'person_batch_delete_button'
                , datagridColumns: [
                    {field: 'personalAccount', title: '个人账号', width: 220, rowspan: 2, align: 'center'},
                    {field: 'name', title: '姓名', width: 150, rowspan: 2, align: 'center'},
                    {field: 'gbidCardType', title: '证件类型', width: 150, rowspan: 2, align: 'center'},
                    {field: 'idCardNumber', title: '证件号码', width: 150, rowspan: 2, align: 'center'},
                    {field: 'wageIncome', title: '工资收入', width: 150, rowspan: 2, align: 'center'},
                    {field: 'monthPay', title: '月缴存额', width: 120, rowspan: 2, align: 'center'},
                    {field: 'dwjce', title: '单位缴存额', width: 120, rowspan: 2, align: 'center'},
                    {field: 'grjce', title: '个人缴存额', width: 120, rowspan: 2, align: 'center'},
                    {field: 'companyPaymentRatio', title: '单位缴存比例', width: 130, rowspan: 2, align: 'center'},
                    {field: 'personPaymentRatio', title: '个人缴存比例', width: 130, rowspan: 2, align: 'center'},
                    {field: 'openedDate', title: '开户日期', width: 150, rowspan: 2, align: 'center'},
                ]
                , dialogWidth: 700
                , dialogHeight: 450
				,datagridToolbar: [{
                    id: this.datagridSearchButtonId
                    , text: '查询'
                    , iconCls: 'icon-search'
                    , handler: function() {
                        $('#person_query_form').submit();
                    }
                }
				,{
					id: this.datagridAddButtonId
					, text: '设立基本账户'
					, iconCls: 'icon-add'
					, handler: function() {

						personService.dialog.dialog('open').dialog('refresh', personService.actionRootUrl + personService.formUrl);
					}
                }
                    ,{
                        id: this.datagridBatchDeleteButtonId
                        , text: '批量删除'
                        , iconCls: 'icon-remove'
                        , handler: function() {
                            var checkedData = {};
                            var items = personService.datagrid.datagrid('getChecked');
                            for(var i = 0; i < items.length; i++) {

                                checkedData["ids[" + i + "]"] = items[i].id;
                            }

                            var dataCount = Object.keys(checkedData).length;
                            var prefix = ( 1 < dataCount )? "批量": "";

                            if( dataCount ) {

                                if( confirm("确定要删除吗？？？") ) {
                                    $.ajax({
                                        url: personService.actionRootUrl + personService.deleteUrl
                                        , type : "post"
                                        , dataType : 'json'
                                        , data : checkedData
                                        , success : function(data, response, status) {

                                            if( jQuery.parseJSON('' + data) ) {

                                                $.messager.alert('通知', prefix + "删除成功。");
                                                personService.datagrid.datagrid('clearChecked');
                                                personService.datagrid.datagrid('reload');
                                            } else {
                                                $.messager.alert('通知', prefix + "删除失败。");
                                            }
                                        }
                                    });

                                } else {
                                    personService.datagrid.datagrid('clearChecked');
                                }
                            } else {
                                $.messager.alert('通知', "请选中数据后再执行删除。");
                            }
                        }


                    }
                ]
                , dialogButtons: [
                    {
                        id: 'person_confirm'
                        , text: '保存'
                        //, iconCls: 'icon-save'
                        , handler: function () {
                        $.messager.progress();
                        if($('#person_edit_form').form('validate')){
                            if (!confirm("要执行“确认”操作吗？？？")) {
                                $.messager.progress('close');
                                return false;
                            }else{
                                personHistorySaveOrUpdate();
                            }
                        }else{
                            $.messager.progress('close');
                        }
                        function personHistorySaveOrUpdate() {
                            $("#person_edit_form").form('submit', {
                                url: '${pageContext.request.contextPath}/business/collection/person_history_account_fund/history/save_or_update.shtml'
                                , onSubmit: function () {
                                    var isValid = $('#person_edit_form').form('validate');
                                    if (!isValid) {
                                        $.messager.progress('close');
                                    }
                                    return isValid
                                }
                                , success: function (data) {
                                    $.messager.progress('close');
                                    console.log(data)
                                    data = jQuery.parseJSON(data);
                                    alert(data.result);
                                    if (data.result) {
										//$("#personAccountFundId").val(data.id);
                                        $("#entityId").val(data.id);
                                        $("#supplementEntityId").val(data.id);
                                        console.log( $("#entityId").val())

                                        //    $.messager.alert('通知', "添加成功！");
                                        $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                            if (r){
                                                window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                            }
                                        });
                                        $('#person_audit').linkbutton('enable');
                                    } else {
                                        $.messager.alert('通知', "添加失败！");
                                    }
                                }
                            });
                        }
                    }
                    }
                    ,{
                        id:'person_audit'
                        ,text:'审核'
                        ,disabled: true
                        ,handler:function(){
                            if (confirm("要执行“审核”操作吗？？？")) {
                                person_audit();
                            }
                            function person_audit(){
                                $("#person_edit_form").form('submit', {
                                    url: '${pageContext.request.contextPath}/business/collection/person_account_fund/person_audit.shtml'
									///?"entity.id='+$("#personAccountFundId").val()
                                    , onSubmit: function () {
                                    }
                                    , success: function (data) {
                                        $("#person_audit").linkbutton('disable');
                                        $.messager.progress('close');
                                        console.log(data)
                                        data = jQuery.parseJSON(data);
                                        if (data.result) {
                                            $.messager.alert('通知', "审核成功！");
                                            $.messager.confirm('通知', '添加成功！是否打印', function(r){
                                                if (r){
                                                    window.location.href="${pageContext.request.contextPath}/print/index.shtml?"+data.id
                                                }
                                            });
                                            $.messager.progress('close');

                                        } else {
                                            $.messager.alert('通知', "审核失败！");
                                        }
                                        $.messager.progress('close');

                                    }
                                });
                            }
                        }
                    }
                    , {
                        id: 'person_reset'
                        , text: '重置'
                        //, iconCls: 'icon-save'
                        , handler: function () {

                            if (confirm("要执行“重置”操作吗？？？")) {

                                $("#person_audit").linkbutton('disable');
                                personService.dialog.dialog('refresh', personService.actionRootUrl + personService.formUrl);
                            }
                        }
                    }
                    , {
                        text: '取消',
                        iconCls: 'icon-cancel',
                        handler: function () {
                            $($(this).context.parentElement.parentElement).dialog('close');
                        }
                    }
                ]
                ,
            });

        $('#person_query_form_search_button').click(function () {
            $('#person_query_form').submit();
        });
    });

    function infoChage(id){
        $('#person_account_infomation_chage').dialog({
            title: '账户信息'
            , href: "${pageContext.request.contextPath}/business/collection/person_account_fund/info_change.shtml?entity.personAccountFundId="+id
            , width: 800
            , height: 600
            , closed: true
            , cache: false
            , resizable: true
            , collapsible: true
            , maximizable: true
            , buttons: [
                {
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                        $.messager.progress('close');
                        return false;
                    }
                }
                ,{
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        $($(this).context.parentElement.parentElement).dialog('close');
                        $.messager.progress('close');
                        return false;
                    }
                }]
            , modal: true
        }).dialog('close');

        $('#person_account_infomation_chage').dialog('open');
    }
</script>

<div style="margin:10px 0;"></div>

<div class="text clearfix" style="text-align:center;">
		<span style="float:left;">
			<form:form name="person_query_form" id="person_query_form"
					   method="post" action="" onsubmit="return false;">
				<form:hidden path="deposit" class="form_view_input combo easyui-validatebox" style="height: 27px;"/>
				<form:hidden path="extra" class="form_view_input combo easyui-validatebox" style="height: 27px;"/>
				<input type="hidden" id="personAccountFundId" />

				<table class="form_view_border" bordercolordark="#FFFFFF"
					   bordercolorlight="#45b97c" border="1px" cellpadding="0"
					   cellspacing="0" style="">
					<tr>
						<th class="panel-header">区县</th>
						<td><form:select path="regionId"
										 id="courtName"
										 style="height: 27px;"
										 class="form_view_input combo easyui-combobox"
										 data-options="required: false, editable: false, disabled: false">
							<form:option value="" >--请选择--</form:option>
							<form:options items="${regionList}" itemValue="id" itemLabel="name"/>
						</form:select></td>
						<th class="panel-header">缴存网点</th>
                        <td><form:select path="entity.siteId"
										 style="height: 27px;"
										 class="form_view_input combo easyui-combobox"
										 data-options="editable: false"
										 validType="">
							<form:option value="" >--请选择--</form:option>
							<form:options items="${siteList}" itemValue="id" itemLabel="name"/>
						</form:select></td>
						<th class="panel-header">单位账号</th>
						<td><form:input path="entityPerson.company.gbCompanyAccount" id="CompanyAccount_search"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

						<th class="panel-header">单位名称</th>
						<td><form:input path="entityPerson.company.gbCompanyName" id="gbCompanyName_search"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>

					</tr>
					<tr>
						<th class="panel-header">姓名</th>
						<td><form:input path="entityPerson.name"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
						<th class="panel-header">证件类型</th>
                <td><form:select path="entity.person.gbidCardType"
								 style="height: 27px;"
								 class="form_view_input combo easyui-combobox"
								 data-options="editable: false"
								 validType="">
					<form:option value="" >--请选择--</form:option>
					<form:options items="${legalPersonIdCardTypeIdList}" itemValue="id" itemLabel="name"/>
				</form:select></td>
						<th class="panel-header">证件号码</th>
						<td><form:input path="entityPerson.idCardNumber"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
						<th class="panel-header">个人账号</th>
						<td><form:input path="entityPerson.personalAccount"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
					</tr>
					<tr>
						<th class="panel-header">开户日期</th><%--value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>"--%>
                        <td width="280"><form:input path="startOpenedDate"
										data-options="required: true"
										style="width:100px;height: 27px;" id="startOpenedDate"

										class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"></form:input>至
							<form:input path="endOpenedDate" id="endOpenedDate"

										style="width:100px;height: 27px;"
										data-options=""
										class="Wdate"
										onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true})"
										validType="complexValid['[1-9]+\\\d{3}\\\-(?:0[1-9]|1[0-2]|[1-9]{1})','请输入正确年月！']"></form:input>
                        </td>
						<th class="panel-header">操作员</th>
						<td><form:input path="entity.operatorId"
										class="form_view_input combo easyui-validatebox" style="height: 27px;"/></td>
					</tr>
				</table>
			</form:form>
		</span>
	<table>
		<tr>
			<td height="30px">
			<td>
		</tr>
	</table>
</div>

<table id="person_list_datagrid"></table>
<div id="person_form_dialog">Dialog Content.</div>
<div id="person_confirm_form_dialog">

</div>
<div id="person_account_infomation_chage">

</div>
