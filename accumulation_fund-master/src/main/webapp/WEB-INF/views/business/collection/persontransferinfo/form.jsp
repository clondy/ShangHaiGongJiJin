<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<script>

    //输入框触发事件


    //定义查询返回的json数据
    var personTransferInfoCallBackJson;
    $("#company_search_Info").click(function () {
        var code = $("#turnOutCompanyAccount").val().trim();
        var date = $("#transfromDate").val().trim();
        if (code.length == 0) {
            $.messager.alert("提示", "单位账号不能为空！")
            return false;
        }
        else if (date.length == 0) {
            $.messager.alert("提示", "封存年月不能为空！")
            return false;
        }


        $.ajax({
            url: "${pageContext.request.contextPath }/business/collection/persontransferinfo/get_local_company_information.shtml",
            type: "post",
            data: {companyAccount: code},
            datatype: "json",
            success: function (data) {
                if (!jQuery.isEmptyObject(data)) {
                    data = jQuery.parseJSON(data);
                    personTransferInfoCallBackJson = data;
                    console.log(data);
                    var loadData = {};
                    for (var key in data) {
                        loadData['entity.company.' + key] = data[key];
                    }
                    $('#person_transfer_info_edit_form').form('load', loadData);
                }

            }
        });
    })


    $("#person_search_Info").click(function () {
        var personCode = $("#personalAccount").val().trim();
        if (personCode.length == 0) {
            $.messager.alert("提示", "职工账号不能为空！")
            return false;
        }
        $.ajax({
            url: "${pageContext.request.contextPath}/business/collection/persontransferinfo/get_local_person_information.shtml",
            type: "post",
            data: {personalAccount: personCode},
            datatype: "json",
            success: function (data) {
                if (!jQuery.isEmptyObject(data)) {
                    data = jQuery.parseJSON(data);
                    personTransferInfoCallBackJson = data;
                    console.log(data);
                    var loadData = {};
                    for (var key in data) {
                        loadData['entity.' + key] = data[key];
                    }
                    $('#person_transfer_info_edit_form').form('load', loadData);
                }

            }
        });
    })


</script>
<div class="div_center">
    <form:form name="person_transfer_info_edit_form" id="person_transfer_info_edit_form" method="post"
               style="width:600px"
               action="" onsubmit="return false;">
        <table class="form_view_border" bordercolordark="#45b97c"
               bordercolorlight="#45b97c" border="1px"
               cellpadding="0"
               cellspacing="0">
           <tr>
                <th class="panel-header">单位账号</th>
                <td><form:input path="entity.turnOutCompanyAccount"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="turnOutCompanyAccount"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
                <th class="panel-header">集中封存年月</th>
                <td><form:input path="entity.transfromDate" id="transfromDate"
                                style="width: 100px;height: 27px;"
                                data-options="required: true"
                                class="Wdate"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"></form:input>
                    &nbsp;&nbsp;&nbsp;
                    <a href="#" class="easyui-linkbutton" id="company_search_Info">查询</a>
                </td>
            </tr>
            <tr>
                <th class="panel-header">单位名称</th>
                <td><form:input path="entity.company.gbCompanyName"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="gbCompanyName"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
                <th class="panel-header">缴存网点</th>
                <td><form:input path="entity.company.paymentSite"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="companyName"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">职工账号</th>
                <td colspan="3"><form:input path="entity.personalAccount"
                                            data-options="required: true"
                                            style="width:184px;height: 27px;" id="personalAccount"
                                            class="form_view_input combo easyui-validatebox"
                ></form:input>
                    &nbsp;&nbsp;&nbsp;
                    <a href="#" class="easyui-linkbutton" id="person_search_Info">查询</a>
                </td>
            </tr>
            <tr>
                <th class="panel-header">职工姓名</th>
                <td><form:input path="entity.person.name"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="personname"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
                <th class="panel-header">审核员</th>
                <td><form:input path="entity.operatorId"
                                data-options="required: true"
                                style="width:184px;height: 27px;" id="operatorId"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>

            </tr>

            <th class="panel-header">审核日期</th>
            <td><form:input path="entity.dateTime" id="dateTime"
                            style="width: 184px;height: 27px;"
                            data-options="required: true"
                            class="Wdate"
                            onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"></form:input>
            <th class="panel-header">审核金额</th>
            <td><form:input path="companyAccountFund.companyAccountBalance"
                            data-options="required: true"
                            style="width:184px;height: 27px;" id="companyAccountBalance"
                            class="form_view_input combo easyui-validatebox"
            ></form:input></td>
            </tr>
            <tr>
                <th class="panel-header">证件类型</th>
                <td><form:select path="entity.person.idCardTypeId" id="idCardTypeId"
                                 style="width:186px;height: 27px;"
                                 class="form_view_input combo easyui-combobox"
                                 data-options="required: true, editable: false"
                >
                    <form:option value="">--请选择--</form:option>
                    <form:options items="${idCardTypeIdList}" itemValue="id" itemLabel="name"/>
                </form:select></td>

                <th class="panel-header">证件号码</th>
                <td><form:input path="entity.person.idCardNumber"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="idCardNumber"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
            </tr>

            <tr>
                <th class="panel-header">出生年月</th>
                <td><form:input path="entity.person.birthYearMonth"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="birthYearMonth"
                                class="Wdate"
                                onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',readOnly:true,minDate:'%y-%M-%D'})"
                ></form:input></td>
                <th class="panel-header">性别</th>
                <td><form:input path="entity.person.sex"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="sex"
                                class="form_view_input combo easyui-validatebox"
                ></form:input></td>
            </tr>
            <tr>
                      <th class="panel-header">集中封存原因</th>
                      <td><form:select path="entity.reasonId" id="reasonId"
                                       style="width:186px;height: 27px;"
                                       class="form_view_input combo easyui-combobox"
                                       data-options="required: true, editable: false"
                                      >
                          <form:option value="" >--请选择--</form:option>
                          <form:options items="${reasonIdList}" itemValue="id" itemLabel="name"/>
                      </form:select></td>
            </tr>
            <tr>
                <th class="panel-header">联系电话</th>
                <td><form:input path="entity.contactMobilePhone"
                                data-options="required: true"
                                style="width:184px;height: 27px;"
                                id="contactMobilePhone"
                                class="form_view_input combo easyui-validatebox"></form:input></td>
                <th class="panel-header">联系人</th>
                <td>
                    <form:input path="entity.contactName" id="siteId"
                                data-options="required: true"
                                style="width:182px;height: 27px;"
                                class="form_view_input combo easyui-validatebox"></form:input>
                </td>
            </tr>

        </table>
    </form:form>
</div>
