<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt'%>

<HTML>
<head>
<title>My Project</title>
<style type="text/css">
.bg {
	background-image: url(<c:url value="/images/login-bg02.jpg" />);
	background-repeat: no-repeat;
	background-position: center center;
}

.login {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #000000;
}

.login input {
	font-size: 12px;
}

img {
	border-width: 0px 0px 0px 0px
}
</style>
</head>
<body>
	<table width="1200" height="525" border="0" align="center"
		cellpadding="0" cellspacing="0">
		<tr>
			<td width="1200" height="525" valign="top" class="bg">
				<form name="logon_form" id="logon_form"
					action="<c:url value='/j_spring_security_check'/>" method="POST"
					onsubmit="return checkForm(this);">
					<table border="0" align="left" cellpadding="0" cellspacing="0">
						<tr>
							<td width="880" rowspan="5">&nbsp;</td>
							<td width="280" height="180">&nbsp;</td>
						</tr>
						<tr>
							<td width="280" height="40" align="left" class="login">用户名：
								<label> <input type="text" name="j_username"
									id="j_username">
							</label>
							</td>
						</tr>
						<tr>
							<td width="280" height="40" align="left" class="login">密&nbsp;&nbsp;&nbsp;&nbsp;码：
								<label> <input type="password" name="j_password"
									id="j_password">
							</label>
							</td>
						</tr>
						<tr>
							<td width="280" height="40" align="left" class="login">验证码：
								<label> <input type="text" name="j_validate_code"
									id="validatecode">&nbsp; <img id="validatecode_img_id"
									src="<c:url value="/picCheckCode"/>" width="60" height="20"
									align="absmiddle"
									onclick="javascript:getValidatePic('validatecode_img_id');"
									title="看不清，点击换一张">
							</label>
							</td>
						</tr>
						<tr>
							<td width="280" height="40" align="center" class="login"><input
								type="image" src="<c:url value="/images/but-go.gif"/>"
								width="69" height="22" />&nbsp;&nbsp; <%-- <a href="javascript:void(0)" onclick="javascript:document.logon_form.reset()"><img src="<c:url value="/images/but-exit.gif"/>" width="69" height="22"></a>--%></td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
		<tr>
			<td valign="top" align="center">&nbsp;首都信息发展股份有限公司&nbsp; 联合制作</td>
		</tr>
	</table>

</body>
<script type="text/javascript">
	function checkForm(obj) {
		if (obj.j_username.value == '') {
			alert('请输入用户名！');
			return false;
		}
		if (obj.j_password.value == '') {
			alert('请输入密码！');
			return false;
		}
		if (obj.validatecode.value == '') {
			alert('请输入验证码！');
			return false;
		}
		return true;
	}

	//看不清楚换一个
	function getValidatePic(id) {
		var t = new Date().getTime();
		var url = '<c:url value="/picCheckCode" />?t=' + t;
		document.getElementById(id).src = url;
	}

	<c:if test="${not empty param.login_error}">
	alert("用户名、密码或者验证码错误");
	</c:if>
</script>
</html>
